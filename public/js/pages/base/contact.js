let app_contact = new Vue({
    el: '#page-contact',
    data: {
        url_api_save_contact: url_api_save_contact,
        info: {
            name: '',
            address: '',
            phone: '',
            email: '',
            message: '',
            birthday: '',
            degree: '',
            experience: '',
            type: $('#typeContact').val(),
            object_id: $('#objectIdContact').val(),
            file: null,
            _token: ENV.token,
        },
        isSaving: false,
    },
    methods: {
        uploadFile() {
            this.info.file = this.$refs.file.files[0];
            $('#showFile').html(this.info.file.name)
        },

        saveContact() {
            var data  = new FormData();
            data.append('file', this.info.file );
            data.append('name', this.info.name);
            data.append('address', this.info.address);
            data.append('phone', this.info.phone);
            data.append('email', this.info.email);
            data.append('message', this.info.message);
            data.append('_token', this.info._token);
            data.append('birthday', this.info.birthday);
            data.append('degree', this.info.degree);
            data.append('experience', this.info.experience);
            data.append('type', this.info.type);
            data.append('object_id', this.info.object_id);


            app_contact.isSaving = true;
            $.ajax({
                url: this.url_api_save_contact,
                type: "POST",
                data: data,
                processData: false,
                contentType: false,

                statusCode: {
                    422: function (response) {
                        const bugs = response.responseJSON.errors;
                        let html = '';
                        Object.keys(bugs).forEach(element => {
                            html += bugs[element][0] + '<BR>';
                        });
                        return Swal.fire({
                            title: window.i18n.site.notice,
                            html: html,
                            showCloseButton: true,
                            icon: "warning"
                        });
                    },
                },
                success: function (jsonData) {
                    if (jsonData.success) {
                        app_contact.info = {};
                        return Swal.fire({
                            title: window.i18n.site.thanhcong,
                            html: jsonData.message,
                            showCloseButton: true,
                            icon: "success"
                        }).then(function () {
                            location.reload();
                        });
                    }
                },
            }).always(function () {
                app_contact.isSaving = false;
            }).catch((err) => {
                console.log('errr', err);
            });
        }
    }
});