let app_api_footer_subscriber = new Vue({
    el: '#pageSaveSubscriber',
    data: {
        url_api_page_subscriber: url_api_page_subscriber,
        info: {
            email: ''
        },
        isSaving: false,
    },
    methods: {
        pageSaveSubscriber(evt) {
            app_api_footer_subscriber.isSaving = true;
            $.ajax({
                url: url_api_page_subscriber,
                type: "POST",
                data: app_api_footer_subscriber.info,
                statusCode: {
                    422: function (response) {
                        const bugs = response.responseJSON.errors;
                        let html = '';
                        Object.keys(bugs).forEach(element => {
                            html += bugs[element][0] + '<BR>';
                        });
                        return Swal.fire({
                            title: window.i18n.site.notice,
                            html: html,
                            showCloseButton: true,
                            icon: "warning"
                        });
                    },

                },
                success: function (jsonData) {
                    if (jsonData.success) {
                        app_api_footer_subscriber.info = {};
                        return Swal.fire({
                            title: window.i18n.site.notice,
                            html: jsonData.message,
                            showCloseButton: true,
                            icon: "success"
                        }).then(function(){
                            location.reload();
                        });

                    }
                }
            }).always(function () {
                app_api_footer_subscriber.isSaving = false;
            });
        }
    }
});