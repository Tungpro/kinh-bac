let app_subscriber = new Vue({
    el: '#footer-top',
    data: {
        url_api_store_subscriber: url_api_store_subscriber,
        info: {
            email: ''
        }
    },
    methods: {
        saveSubscriber() {
            console.log('111111',this.info.email);
            let vm = this;
            $.ajax({
                url: url_api_store_subscriber,
                type: "POST",
                data: vm.info,
                statusCode: {
                    422: function (response) {
                        const bugs = response.responseJSON.errors;
                        let html = '';
                        Object.keys(bugs).forEach(element => {
                            html += bugs[element][0] + '<BR>';
                        });
                        return Swal.fire({
                            title: window.i18n.site.notice,
                            html: html,
                            showCloseButton: true,
                            icon: "warning"
                        });
                    },

                },
                success: function (jsonData) {
                    if (jsonData.success) {
                        vm.info = {};
                        return Swal.fire({
                            title: window.i18n.site.notice,
                            html: jsonData.message,
                            showCloseButton: true,
                            icon: "success"
                        }).then(function(){
                            location.reload();
                        });

                    }
                }
            });
        }
    }
});