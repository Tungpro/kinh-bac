const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = merge(common, {
  target: 'browserslist',
  mode: 'development',
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()],
  },
});
