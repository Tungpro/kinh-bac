$(document).ready(function () {
  $('.languages').click(function () {
    $('.language-list').toggleClass('active');
  });

  $(document).mouseup(function (e) {
    let container = $('.languages');
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      container.siblings().removeClass('active');
    }
  });

  if ($(window).innerWidth() <= 1199) {
    $('.sub-menu').slideUp();
    $('.has-sub-menu .mb-click').click(function (e) {
      const parentAddActive = $(this).parent().addClass('active');
      if (parentAddActive.hasClass('active')) {
        $(this).parent().siblings().find('.icon').removeClass('active');
        $(this).parent().siblings().removeClass('active').children('.sub-menu').slideUp();
        $(this).parent().children('.sub-menu').slideToggle();
        $(this).parent().find('.icon').toggleClass('active');
      }
    });

    $('.hambuger').click(function () {
      $('.header__menu, .header__overlay').addClass('active');
    });

    $('.header__overlay').click(function () {
      $('.sub-menu').slideUp();
      $('.header__menu, .header__overlay, .icon').removeClass('active');
    });
  }

  if ($(window).width() >= 1200) {
    function extendWidthProject() {
      let w_container = $('.container').innerWidth();
      let w_dom = $(window).innerWidth();
      $('.outside-container').css('margin-left', (w_dom - w_container) / 2);
    }
    extendWidthProject();

    $(window).resize(function () {
      extendWidthProject();
    });
  }
});

$(document).ready(function () {
  function blogTabActive() {
    $('.blog--tabs-item').slice(0, 1).show();

    $('.js-tab-onClick').click(function () {
      let dataTab = $(this).data('tab');

      $('.js-tab-onClick').removeClass('active');
      $(this).addClass('active');

      $('.blog--tabs-item').fadeOut(0);
      $('#' + dataTab).fadeIn();
    });
  }

  blogTabActive();
});
