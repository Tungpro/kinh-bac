import '@fortawesome/fontawesome-free/js/fontawesome';
import '@fortawesome/fontawesome-free/js/solid';
import '@fortawesome/fontawesome-free/js/regular';
import '@fortawesome/fontawesome-free/js/brands';
import 'bootstrap/dist/css/bootstrap.min.css';
import $ from 'jquery';
import Swiper from 'swiper/bundle';
import { Navigation, Pagination, Autoplay, Grid, FreeMode } from 'swiper';
import 'swiper/css/bundle';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import '../scss/style.scss';
import '../js/main';
import animations from './animations';
import 'tw-elements';

animations();

Swiper.use([Navigation, Pagination, Autoplay, Grid, FreeMode]);
$(document).ready(function () {
  var swiperBanner = new Swiper('.banner__swiper', {
    spaceBetween: 15,
    slidesPerView: 1,
    loop: true,
    speed: 2000,
    autoplay: {
      delay: 5000,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

  var swiperProject = new Swiper('.project__list-swiper', {
    spaceBetween: 15,
    slidesPerView: 1,
    watchSlidesVisibility: true,
    keyboard: true,
    speed: 800,
    preloadImages: false,
    //   centeredSlides: true,
    lazy: {
      loadPrevNext: true,
      loadPrevNextAmount: 2,
      loadOnTransitionStart: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      576: {
        slidesPerView: 2,
      },
      992: {
        slidesPerView: 2.6,
        spaceBetween: 40,
      },
      1680: {
        slidesPerView: 2.6,
        spaceBetween: 90,
      },
    },
  });

  var swiperAward = new Swiper('.award__list-swiper', {
    spaceBetween: 15,
    slidesPerView: 2,
    preloadImages: false,
    watchSlidesVisibility: true,
    lazy: {
      loadPrevNext: true,
      loadPrevNextAmount: 2,
      loadOnTransitionStart: true,
    },
    keyboard: true,
    speed: 800,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      576: {
        slidesPerView: 2.4,
      },
      992: {
        slidesPerView: 4.4,
        spaceBetween: 35,
      },
      1200: {
        slidesPerView: 5.3,
        spaceBetween: 35,
      },
    },
  });

  var swiperDirectory = new Swiper('.directory__swiper', {
    spaceBetween: 20,
    slidesPerView: 1,
    breakpoints: {
      576: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
    },
    navigation: {
      nextEl: '.directory__list .swiper-button-next',
      prevEl: '.directory__list .swiper-button-prev',
    },
  });

  var swiperDirectory = new Swiper('.introduce__history', {
    spaceBetween: 20,
    slidesPerView: 1,
    breakpoints: {
      576: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 30,
      },
      992: {
        slidesPerView: 4,
        spaceBetween: 30,
      },
    },
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
      clickable: true,
    },
    navigation: {
      nextEl: '.introduce__history--list .swiper-button-next',
      prevEl: '.introduce__history--list .swiper-button-prev',
    },
  });

  var swiperCompany = new Swiper('.company__list--swiper', {
    spaceBetween: 22,
    slidesPerView: 1,
    breakpoints: {
      576: {
        slidesPerView: 2,
      },
      768: {
        slidesPerView: 3,
      },
      992: {
        slidesPerView: 4,
      },
    },
    navigation: {
      nextEl: '.company__list .swiper-button-next',
      prevEl: '.company__list .swiper-button-prev',
    },
  });

  var swiperPartnership = new Swiper('.partnership__list--swiper', {
    loop: true,
    freeMode: {
      enabled: true,
      momentum: true,
    },
    speed: 5000,
    spaceBetween: 15,
    grabCursor: false,
    allowTouchMove: false,
    slidesPerView: 4,
    loop: true,
    allowTouchMove: false,
    autoplay: {
      delay: 1,
      disableOnInteraction: true,
    },
    breakpoints: {
      576: {
        slidesPerView: 6,
      },
      768: {
        slidesPerView: 8,
      },
      992: {
        slidesPerView: 10,
      },
    },
  });

  //   var relatedBlog = new Swiper('#relatedBlog .mySwiper', {
  //     breakpoints: {
  //       320: {
  //         slidesPerView: 1.2,
  //         spaceBetween: 15,
  //       },
  //       640: {
  //         slidesPerView: 2,
  //         spaceBetween: 20,
  //       },
  //       992: {
  //         slidesPerView: 3,
  //         spaceBetween: 30,
  //       },
  //       1200: {
  //         slidesPerView: 3,
  //         spaceBetween: 50,
  //       },
  //     },
  //     navigation: {
  //       nextEl: '#relatedBlog .swiper-button-next',
  //       prevEl: '#relatedBlog .swiper-button-prev',
  //     },
  //   });

  var relatedLinhvucSwiper = new Swiper('.news-difference', {
    slidesPerView: 2,
    spaceBetween: 15,
    navigation: {
      nextEl: '#relatedLinhvuc .swiper-button-next',
      prevEl: '#relatedLinhvuc .swiper-button-prev',
    },
    breakpoints: {
      640: {
        slidesPerView: 2,
        spaceBetween: 20,
      },
      992: {
        slidesPerView: 3,
        spaceBetween: 20,
      },
      1200: {
        slidesPerView: 4,
        spaceBetween: 20,
      },
    },
  });

  // sync slide chi tiết lĩnh vực

  var swiperThumbs = new Swiper('.swiper-thumbs .mySwiper', {
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    freeMode: true,
    breakpoints: {
      320: {
        slidesPerView: 3,
        spaceBetween: 11,
      },
      768: {
        slidesPerView: 5,
        spaceBetween: 11,
      },
    },
  });

  var swiperMain = new Swiper('.swiper-main .mySwiper', {
    slideToClickedSlide: true,
    thumbs: {
      swiper: swiperThumbs,
    },
    navigation: {
      nextEl: '.swiper-main .swiper-button-next',
      prevEl: '.swiper-main .swiper-button-prev',
    },
    pagination: {
      el: '.swiper-main .swiper-pagination',
      clickable: true,
    },
  });

  // end sync

  $('.partnership__list--swiper').mouseenter(function () {
    swiperPartnership.autoplay.stop();
  });

  $('.partnership__list--swiper').mouseleave(function () {
    swiperPartnership.autoplay.start();
  });

  var swiperPartner = new Swiper('.partner__list-swiper', {
    direction: 'horizontal',
    slidesPerView: 3,
    spaceBetween: 20,
    grabCursor: true,
    grid: {
      fill: 'row',
      rows: 3,
    },
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
      clickable: true,
    },
  });
});
