import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import { CustomEase } from 'gsap/CustomEase';

gsap.registerPlugin(ScrollTrigger, CustomEase);

CustomEase.create('ease', '0.25, 0.1, 0.25, 0.1');

const animations = (parentSelector) => {
  gsap.config({ nullTargetWarn: false });
  // title split animation
  const baseTitles = gsap.utils.toArray(`[data-title-base-animation]`);
  baseTitles.forEach((baseTitle) => {
    gsap.fromTo(
      baseTitle,
      { opacity: 0 },
      {
        opacity: 1,
        duration: 0.8,
        scrollTrigger: {
          trigger: baseTitle,
        },
      }
    );
  });

  // text opacity

  const opacityContents = gsap.utils.toArray(`[data-opacity-content]`);

  opacityContents.forEach((opacityContent) => {
    gsap.fromTo(
      opacityContent,
      { opacity: 0 },
      {
        opacity: 1,
        delay: 0.2,
        duration: 0.8,
        scrollTrigger: {
          trigger: opacityContent,
        },
      }
    );
  });

  // slider cars animation
  gsap.set('[data-slider-card-up]', { opacity: 0, y: 64 });
  ScrollTrigger.batch('[data-slider-card-up]', {
    once: true,
    onEnter: (batch) =>
      gsap.fromTo(
        batch,
        {
          opacity: 0,
          y: 64,
        },
        {
          opacity: 1,
          y: 0,
          duration: 0.4,
          ease: 'ease',
          stagger: 0.2,
          clearProps: 'transform',
        }
      ),
  });

  // button circle animation

  const fadeInUpItems = gsap.utils.toArray(`[data-fade-in-up]`);

  fadeInUpItems.forEach((fadeInUpItem) => {
    gsap.set(fadeInUpItem, { y: 64, opacity: 0 });

    gsap.fromTo(
      fadeInUpItem,
      { y: 64, opacity: 0 },
      {
        y: 0,
        opacity: 1,
        duration: 0.8,
        scrollTrigger: { trigger: fadeInUpItem },
        ease: 'ease',
      }
    );
  });
};

export default animations;
