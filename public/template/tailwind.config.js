module.exports = {
  content: ['./src/**/*.{html,js}'],
  theme: {
    extend: {},
    container: {
      center: true,
      padding: '15px',
    },
    screens: {
      sm: '576px',
      // => @media (min-width: 640px) { ... }

      md: '768px',
      // => @media (min-width: 768px) { ... }

      lg: '992px',
      // => @media (min-width: 1024px) { ... }

      xl: '1200px',
      // => @media (min-width: 1280px) { ... }
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      white: '#fff',
      gray1: '#4F4F4F',
      gray2: '#828282',
      gray3: '#333333',
      black2: '#060709',
      bd: '#bdbdbd',
      darkBlue: '#001D2F',
      red: '#EC2029',
    },
    fontFamily: {
      utm: ['UTM Bebas'],
      san: ['San Francisco Text'],
      iciel: ['iCiel Rift'],
    },
    fontSize: {
      12: ['12px', '20px'],
      14: ['14px', '20px'],
      16: ['16px', '24px'],
      18: ['18px', '26px'],
      20: ['20px', '28px'],
      22: ['22px', '30px'],
      24: ['24px', '32px'],
      28: ['28px', '42px'],
      32: ['32px', '40px'],
      36: ['36px', '44px'],
      40: ['40px', '48px'],
    },
  },
  plugins: [require('@tailwindcss/forms'), require('@tailwindcss/line-clamp'), require('@tailwindcss/aspect-ratio'), require('tw-elements/dist/plugin')],
};
