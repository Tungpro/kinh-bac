<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',
    'defaultImg' => [
        'max' => ['with' => 1500, 'height' => 1500], //for validate
        'size' => [
            'original' => ['width' => 0, 'height' => 0],
            'small' => ['width' => 80, 'height' => 0],
            'medium' => ['width' => 250, 'height' => 0],
            'large' => ['width' => 800, 'height' => 0],
        ]
    ],
    'data' => [
        'menu' => [
            'max' => ['with' => 845, 'height' => 845], //for validate
            'size' => [
                'original' => ['width' => 0, 'height' => 0],
                'small' => ['width' => 250, 'height' => 0],
                'seo' => ['width' => 800, 'height' => 800],
                'social' => ['width' => 880, 'height' => 0],
            ]
        ],
        'news' => [
            'size' => [
                'medium' => ['width' => 200, 'height' => 0],
                'medium1' => ['width' => 275, 'height' => 140],
                'medium2' => ['width' => 327, 'height' => 0],
                'medium3' => ['width' => 350, 'height' => 0],
                'medium4' => ['width' => 378, 'height' => 0],
                'medium5' => ['width' => 536, 'height' => 0],
                'medium6' => ['width' => 557, 'height' => 0],
                'medium7' => ['width' => 285, 'height' => 0],
                'medium8' => ['width' => 281, 'height' => 0],
                'large' => ['width' => 358, 'height' => 0],
                'large1' => ['width' => 966, 'height' => 0],
                'large2' => ['width' => 719, 'height' => 0],
                'social3' => ['width' => 1422, 'height' => 0],
                'seo' => ['width' => 645, 'height' => 0],
            ]
        ],
        'achievement' => [
            'size' => [
                'medium' => ['width' => 42, 'height' => 0],
                'seo' => ['width' => 645, 'height' => 0],
            ]
        ],
        'history' => [
            'size' => [
                'medium' => ['width' => 379, 'height' => 0],
                'seo' => ['width' => 645, 'height' => 0],
            ]
        ],
        'showroom' => [
            'size' => [
                'small' => ['width' => 110, 'height' => 0],
                'large' => ['width' => 800, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
            ]
        ],
        'staticpage' => [
            'size' => [
                'small' => ['width' => 100, 'height' => 0],
                'medium' => ['width' => 238, 'height' => 0],
                'large' => ['width' => 564, 'height' => 0],
                'large1' => ['width' => 559, 'height' => 0],
                'largex2' => ['width' => 1920, 'height' => 0],
                'detail' => ['width' => 920, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
                'seo' => ['width' => 645, 'height' => 0],
            ]
        ],
        'media' => [
            'size' => [
                'small' => ['width' => 100, 'height' => 0],
                'medium' => ['width' => 135, 'height' => 0],
                'medium2' => ['width' => 719, 'height' => 0],
                'large' => ['width' => 654, 'height' => 319],
            ]
        ],
        'product' => [
            'size' => [
                'small' => ['width' => 80, 'height' => 0],
                'medium' => ['width' => 250, 'height' => 0],
                'medium1' => ['width' => 262, 'height' => 0],
                'mediumx2' => ['width' => 350, 'height' => 0],
                'mediumx3' => ['width' => 300, 'height' => 0],
                'mediumx4' => ['width' => 476, 'height' => 342],
                'large' => ['width' => 630, 'height' => 0],
                'largex2' => ['width' => 1300, 'height' => 0],
                'social' => ['width' => 950, 'height' => 0],
            ]
        ],
        'optionvalue' => [
            'size' => [
                'small' => ['width' => 80, 'height' => 0],
                'medium' => ['width' => 179, 'height' => 0],
                'mediumx2' => ['width' => 350, 'height' => 0],
                'mediumx3' => ['width' => 300, 'height' => 0],
                'mediumx4' => ['width' => 500, 'height' => 0],
                'large' => ['width' => 630, 'height' => 0],
                'largex2' => ['width' => 1300, 'height' => 0],
                'social' => ['width' => 744, 'height' => 0],
            ]
        ],
        'promotioncampaign' => [
            'size' => [
                'small' => ['width' => 80, 'height' => 0],
                'medium' => ['width' => 400, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
                'original' => ['width' => 0, 'height' => 0],
                'large' => ['width' => 1300, 'height' => 0],
                'largex2' => ['width' => 1920, 'height' => 0],
                'largex3' => ['width' => 2300, 'height' => 0],
            ]
        ],
        'attribute' => [
            'size' => [
                'small' => ['width' => 80, 'height' => 0],
                'medium' => ['width' => 350, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
            ]
        ],
        'tags' => [
            'size' => [
                'small' => ['width' => 80, 'height' => 0],
                'medium' => ['width' => 350, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
                'original' => ['width' => 0, 'height' => 0],
            ]
        ],
        'collection' => [
            'size' => [
                'small' => ['width' => 80, 'height' => 0],
                'medium' => ['width' => 350, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
                'original' => ['width' => 0, 'height' => 0],
                'large' => ['width' => 1300, 'height' => 0],
                'largex2' => ['width' => 1600, 'height' => 0],
                'largex3' => ['width' => 2300, 'height' => 0],
                'banner' => ['width' => 800, 'height' => 0],
            ]
        ],
        'feature' => [
            'dir' => 'feature',
            'max' => ['with' => 1500, 'height' => 1500], //for validate
            'size' => [
                'original' => ['width' => 0, 'height' => 0],
                'small' => ['width' => 85, 'height' => 0],
                'medium' => ['width' => 350, 'height' => 0],
                'slide' => ['width' => 600, 'height' => 400],
                'large' => ['width' => 1300, 'height' => 0],
                'largex2' => ['width' => 1920, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
            ]
        ],
        'banner' => [
            'dir' => 'feature',
            'max' => ['with' => 1500, 'height' => 1500], //for validate
            'size' => [
                'original' => ['width' => 0, 'height' => 0],
                'small' => ['width' => 100, 'height' => 0],
                'small2' => ['width' => 300, 'height' => 0],
                'medium' => ['width' => 760, 'height' => 580],
                'medium1' => ['width' => 1170, 'height' => 0],
                'large' => ['width' => 1600, 'height' => 0],
                'super_large' => ['width' => 1920, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
            ]
        ],
        'setting' => [
            'max' => ['with' => 845, 'height' => 845], //for validate
            'size' => [
                'original' => ['width' => 0, 'height' => 0],
                'medium_seo' => ['width' => 250, 'height' => 0],
                'seo' => ['width' => 800, 'height' => 800],
                'social' => ['width' => 800, 'height' => 0],
            ]
        ],
        'category' => [
            'size' => [
                'original' => ['width' => 0, 'height' => 0],
                'icon' => ['width' => 48, 'height' => 0],
                'iconx2' => ['width' => 100, 'height' => 0],
                'banner' => ['width' => 1000, 'height' => 0],
                'avatar' => ['width' => 340, 'height' => 0],
                'medium5' => ['width' => 416, 'height' => 0],
                'social' => ['width' => 675, 'height' => 0],
                'large' => ['width' => 1200, 'height' => 0],
                'large2' => ['width' => 1480, 'height' => 0],
                'seo' => ['width' => 645, 'height' => 0],
                'super_large' => ['width' => 1980, 'height' => 0],
            ]
        ],
        'file' => [],
        'avatar' => [
            'max' => ['with' => 500, 'height' => 500], //for validate
            'size' => [
                'small' => ['width' => 60, 'height' => 0],
                'small2' => ['width' => 40, 'height' => 0],
                'medium' => ['width' => 160, 'height' => 0],
                'large' => ['width' => 200, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
            ]
        ],
        'manufacturer' => [
            'max' => ['with' => 500, 'height' => 500], //for validate
            'size' => [
                'small' => ['width' => 80, 'height' => 0],
                'large' => ['width' => 200, 'height' => 0],
                'largex2' => ['width' => 400, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
            ]
        ],
        'specialoffer' => [
            'dir' => 'specialoffer',
            'max' => ['with' => 1500, 'height' => 1500], //for validate
            'size' => [
                'original' => ['width' => 0, 'height' => 0],
                'small' => ['width' => 100, 'height' => 0],
                'slide_semi_small' => ['width' => 400, 'height' => 0],
                'slide' => ['width' => 600, 'height' => 0],
                'large' => ['width' => 1300, 'height' => 0],
                'super_large' => ['width' => 1920, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
            ]
        ],
        'gallery' => [
            'dir' => 'gallery',
            'size' => [
                'original' => ['width' => 0, 'height' => 0],
                'small' => ['width' => 150, 'height' => 0],
                'slide' => ['width' => 350, 'height' => 0],
                'large' => ['width' => 640, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
            ]
        ],
        'customergroup' => [
            'size' => [
                'original' => ['width' => 0, 'height' => 0],
                'tiny' => ['width' => 13, 'height' => 0],
                'small' => ['width' => 45, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
            ]
        ],
        'customer' => [
            'size' => [
                'original' => ['width' => 0, 'height' => 0],
                'tiny' => ['width' => 13, 'height' => 0],
                'small' => ['width' => 45, 'height' => 0],
                'avatar' => ['width' => 400, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
            ]
        ],
        'order' => [
            'size' => [
                'original' => ['width' => 0, 'height' => 0],
                'address_img' => ['width' => 800, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
            ]
        ],
        'comment' => [
            'dir' => 'comment',
            'max' => ['with' => 500, 'height' => 500], //for validate
            'size' => [
                'small' => ['width' => 60, 'height' => 0],
                'avatar' => ['width' => 100, 'height' => 0],
                'medium' => ['width' => 400, 'height' => 0],
                'large' => ['width' => 800, 'height' => 0],
                'largex2' => ['width' => 1920, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
            ]
        ],
        'partner' => [
            'dir' => 'partners',
            'max' => ['with' => 500, 'height' => 500], //for validate
            'size' => [
                'small' => ['width' => 68, 'height' => 0],
                'small1' => ['width' => 92, 'height' => 0],
                'avatar' => ['width' => 120, 'height' => 0],
                'medium' => ['width' => 234, 'height' => 0],
                'large' => ['width' => 800, 'height' => 0],
                'largex2' => ['width' => 1920, 'height' => 0],
                'social' => ['width' => 800, 'height' => 0],
            ]
        ],
        'speaker' => [
            'dir' => 'speaker',
            'max' => ['with' => 500, 'height' => 500], //for validate
            'size' => [
                'small' => ['width' => 100, 'height' => 0],
                'avatar' => ['width' => 203, 'height' => 0],
                'medium' => ['width' => 384, 'height' => 0],
                'large' => ['width' => 800, 'height' => 0],
                'seo' => ['width' => 645, 'height' => 0],
            ]
        ],
        'testimonial' => [
            'dir' => 'testimonial',
            'max' => ['with' => 500, 'height' => 500], //for validate
            'size' => [
                'small' => ['width' => 68, 'height' => 0],
                'avatar' => ['width' => 149, 'height' => 0],
                'seo' => ['width' => 645, 'height' => 0],
            ]
        ]


    ]

];
