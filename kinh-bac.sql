-- --------------------------------------------------------
-- Host:                         10.3.9.18
-- Server version:               5.7.29 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for centurystone
CREATE DATABASE IF NOT EXISTS `centurystone` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `centurystone`;

-- Dumping structure for table centurystone.address_book
CREATE TABLE IF NOT EXISTS `address_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `ward_id` int(11) DEFAULT NULL,
  `is_main` int(11) NOT NULL COMMENT '1: Main Address\n2: Not Main address',
  `created_at` datetime DEFAULT NULL,
  `type` tinyint(1) DEFAULT '1' COMMENT '1: Owner\n2: Contractor\n',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.address_book: ~0 rows (approximately)
/*!40000 ALTER TABLE `address_book` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_book` ENABLE KEYS */;

-- Dumping structure for table centurystone.api_log
CREATE TABLE IF NOT EXISTS `api_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `end_point` varchar(350) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_object_authen` text COLLATE utf8mb4_unicode_ci,
  `request` text COLLATE utf8mb4_unicode_ci,
  `response` text COLLATE utf8mb4_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.api_log: ~0 rows (approximately)
/*!40000 ALTER TABLE `api_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `api_log` ENABLE KEYS */;

-- Dumping structure for table centurystone.app_space
CREATE TABLE IF NOT EXISTS `app_space` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `image_two` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort_order` int(3) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `created_by` int(10) DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `sort_order` (`sort_order`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.app_space: ~7 rows (approximately)
/*!40000 ALTER TABLE `app_space` DISABLE KEYS */;
INSERT INTO `app_space` (`id`, `image`, `image_two`, `sort_order`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, NULL, NULL, NULL, 1, 1, NULL, '2022-01-07 10:38:41', '2022-01-07 10:38:41'),
	(2, NULL, NULL, NULL, 1, 1, 1, '2022-01-07 10:39:02', '2022-01-07 11:12:31'),
	(3, 'appspace-1641548244.jpg', 'appspace-1641548244.png', NULL, 1, 1, 1, '2022-01-07 11:13:51', '2022-01-07 17:14:29'),
	(4, 'appspace-1641548478.jpg', NULL, NULL, 1, 1, NULL, '2022-01-07 16:41:21', '2022-01-07 16:41:21'),
	(5, 'appspace-1641548521.jpg', NULL, NULL, 1, 1, NULL, '2022-01-07 16:42:04', '2022-01-07 16:42:04'),
	(7, '', NULL, NULL, 1, 1, 1, '2022-01-07 16:48:04', '2022-01-12 11:16:40');
/*!40000 ALTER TABLE `app_space` ENABLE KEYS */;

-- Dumping structure for table centurystone.app_space_description
CREATE TABLE IF NOT EXISTS `app_space_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_space_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `education_program_id` (`app_space_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.app_space_description: ~15 rows (approximately)
/*!40000 ALTER TABLE `app_space_description` DISABLE KEYS */;
INSERT INTO `app_space_description` (`id`, `app_space_id`, `language_id`, `slug`, `title`, `name`, `short_description`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
	(1, 2, 1, 'tieu-de-1', 'Tiêu đề 1', NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 2, 2, 'tieu-de-tieng-anh', 'Tiêu đề tiếng anh', NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 2, 3, 'tieu-de-tieng-trung', 'Tiêu đề tiếng trung', NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 3, 1, 'tieu-de-1', 'Tiêu đề 1 34', NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 3, 2, 'tieu-de-tieng-anh', 'Tiêu đề tiếng anh', NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 3, 3, 'tieu-de-tieng-trung', 'Tiêu đề tiếng trung', NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 4, 1, 'kho-ung-dung-04', 'Kho ứng dụng 04', NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 4, 2, 'tieu-de-tieng-anh', 'Tiêu đề tiếng anh', NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 4, 3, 'tieu-de-tieng-trung', 'Tiêu đề tiếng trung', NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 5, 1, 'kho-ung-dung-04', 'Kho ứng dụng 04', NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 5, 2, 'tieu-de-tieng-anh', 'Tiêu đề tiếng anh', NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 5, 3, 'tieu-de-tieng-trung', 'Tiêu đề tiếng trung', NULL, NULL, NULL, NULL, NULL, NULL),
	(16, 7, 1, 'kho-ung-dung-06', 'Kho ứng dụng 06', 'test 131313', 'mô tả', NULL, NULL, NULL, NULL),
	(17, 7, 2, 'tieu-de-tieng-anh', 'Tiêu đề tiếng anh', 't3wtrst', NULL, NULL, NULL, NULL, NULL),
	(18, 7, 3, 'tieu-de-tieng-trung', 'Tiêu đề tiếng trung', '42424242', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `app_space_description` ENABLE KEYS */;

-- Dumping structure for table centurystone.attribute
CREATE TABLE IF NOT EXISTS `attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- Dumping data for table centurystone.attribute: 0 rows
/*!40000 ALTER TABLE `attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute` ENABLE KEYS */;

-- Dumping structure for table centurystone.attribute_description
CREATE TABLE IF NOT EXISTS `attribute_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table centurystone.attribute_description: 0 rows
/*!40000 ALTER TABLE `attribute_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_description` ENABLE KEYS */;

-- Dumping structure for table centurystone.attribute_group
CREATE TABLE IF NOT EXISTS `attribute_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(3) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- Dumping data for table centurystone.attribute_group: 0 rows
/*!40000 ALTER TABLE `attribute_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_group` ENABLE KEYS */;

-- Dumping structure for table centurystone.attribute_group_description
CREATE TABLE IF NOT EXISTS `attribute_group_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table centurystone.attribute_group_description: 0 rows
/*!40000 ALTER TABLE `attribute_group_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_group_description` ENABLE KEYS */;

-- Dumping structure for table centurystone.bank
CREATE TABLE IF NOT EXISTS `bank` (
  `id` int(11) DEFAULT NULL,
  `bank` varchar(500) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `account` varchar(30) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `lang` varchar(3) DEFAULT NULL,
  `branch` varchar(30) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_mobile` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.bank: ~0 rows (approximately)
/*!40000 ALTER TABLE `bank` DISABLE KEYS */;
/*!40000 ALTER TABLE `bank` ENABLE KEYS */;

-- Dumping structure for table centurystone.banner
CREATE TABLE IF NOT EXISTS `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) DEFAULT NULL,
  `is_mobile` tinyint(1) DEFAULT '0' COMMENT '0: no | 1: yes',
  `status` tinyint(1) DEFAULT '1',
  `position` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `category_id` int(11) DEFAULT '0' COMMENT 'Thuộc danh mục/ngành hàng',
  `publish_at` timestamp NULL DEFAULT NULL,
  `end_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.banner: ~32 rows (approximately)
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` (`id`, `sort_order`, `is_mobile`, `status`, `position`, `category_id`, `publish_at`, `end_at`, `created_at`, `updated_at`, `icon`) VALUES
	(1, NULL, 0, -1, 'bo_suu_tap_top', 0, '2021-12-02 16:16:32', '2022-01-01 16:16:32', '2021-11-16 16:47:56', '2022-01-13 11:18:36', NULL),
	(2, NULL, 0, -1, 'bo_suu_tap_bot', 0, '2021-12-02 18:38:18', '2022-01-01 18:38:18', '2021-11-16 16:49:30', '2022-01-13 11:18:55', NULL),
	(3, 1, 0, -1, 'big_home', 0, '2021-12-02 16:16:21', '2022-01-01 16:16:21', '2021-11-16 17:30:44', '2022-01-13 11:14:20', NULL),
	(5, 2, 0, -1, 'big_home', 0, '2021-12-02 10:21:10', '2022-01-01 10:21:10', '2021-11-22 15:43:46', '2022-01-13 11:18:43', NULL),
	(6, 3, 0, -1, 'big_home', 0, '2021-12-02 10:20:40', '2022-01-01 10:20:40', '2021-11-22 15:57:01', '2022-01-13 11:14:34', NULL),
	(7, 4, 0, -1, 'big_home', 0, '2022-01-06 13:59:37', '2022-02-05 13:59:37', '2021-11-22 15:57:35', '2022-01-13 11:17:30', NULL),
	(8, NULL, 0, -1, 'dieu_can_biet_top', 0, '2021-12-02 14:11:57', '2022-01-01 14:11:57', '2021-11-24 14:28:08', '2022-01-13 11:18:27', NULL),
	(9, 1, 0, -1, 'contact_slide', 0, '2021-11-25 15:37:30', '2021-12-25 15:37:30', '2021-11-25 15:37:30', '2022-01-13 11:18:49', NULL),
	(10, 2, 0, -1, 'contact_slide', 0, '2021-11-25 15:38:09', '2021-12-25 15:38:09', '2021-11-25 15:38:09', '2022-01-13 11:18:19', NULL),
	(11, 1, 0, -1, 'top_text', 0, '2021-12-03 09:58:53', '2022-01-02 09:58:53', '2021-11-25 17:28:28', '2022-01-13 11:18:09', NULL),
	(12, NULL, 0, -1, 'tin_tuc_top', 0, '2021-12-06 09:52:45', '2022-01-05 09:52:45', '2021-11-25 20:21:21', '2022-01-13 11:18:01', NULL),
	(13, NULL, 0, -1, 'gioi_thieu_top', 0, '2021-12-02 09:28:21', '2022-01-01 09:28:21', '2021-11-26 11:02:57', '2022-01-13 11:17:52', NULL),
	(14, NULL, 0, -1, 'gioi_thieu_bot', 0, '2021-12-02 16:21:43', '2022-01-01 16:21:43', '2021-11-26 14:48:50', '2022-01-13 11:17:43', NULL),
	(15, 1, 0, -1, 'search_product', 0, '2021-11-29 10:43:48', '2021-12-29 10:43:48', '2021-11-29 10:43:48', '2022-01-13 11:17:37', NULL),
	(16, 1, 0, -1, 'do_kham_top', 0, '2021-12-02 15:01:17', '2022-01-01 15:01:17', '2021-11-30 09:32:17', '2022-01-13 11:14:13', NULL),
	(17, 1, 0, -1, 'do_kham_bot', 0, '2021-12-02 17:21:43', '2022-01-01 17:21:43', '2021-11-30 10:37:09', '2022-01-13 11:14:06', NULL),
	(18, NULL, 0, -1, 'san_pham', 0, '2022-01-25 09:59:36', '2022-02-24 09:59:36', '2022-01-07 16:42:55', '2022-01-25 10:00:29', NULL),
	(19, 1, 0, 2, 'gioi_thieu', 0, '2022-02-11 13:49:15', '2022-03-13 13:49:15', '2022-01-13 15:02:26', '2022-02-11 13:49:15', NULL),
	(20, NULL, 0, 2, 'san_pham', 0, '2022-01-25 10:00:37', '2022-02-24 10:00:37', '2022-01-14 14:10:45', '2022-02-10 14:48:34', NULL),
	(21, NULL, 0, 2, 'bo_suu_tap', 0, '2022-01-25 09:59:14', '2022-02-24 09:59:14', '2022-01-14 14:12:37', '2022-02-10 14:48:39', NULL),
	(22, 1, 0, 2, 'big_home', 0, '2022-01-26 16:54:04', '2022-02-25 16:54:04', '2022-01-17 10:13:33', '2022-02-10 14:48:43', NULL),
	(23, 2, 0, 2, 'big_home', 0, '2022-01-25 09:59:04', '2022-02-24 09:59:04', '2022-01-17 10:14:25', '2022-02-10 14:48:45', NULL),
	(24, 2, 0, 2, 'gioi_thieu_center', 0, '2022-01-25 09:58:59', '2022-02-24 09:58:59', '2022-01-17 14:51:43', '2022-02-10 18:00:12', NULL),
	(25, NULL, 0, 2, 'tin_tuc', 0, '2022-02-11 13:49:59', '2022-03-13 13:49:59', '2022-01-18 15:04:25', '2022-02-11 13:49:59', NULL),
	(26, NULL, 0, 2, 'ung_dung', 0, '2022-02-25 11:24:11', '2022-03-27 11:24:11', '2022-01-18 17:10:07', '2022-02-25 11:24:11', NULL),
	(27, 1, 0, -1, 'lien_he', 0, '2022-02-09 09:39:36', '2022-03-11 09:39:36', '2022-01-19 10:25:31', '2022-02-10 10:01:14', NULL),
	(28, NULL, 0, -1, 'big_home', 0, '2022-01-25 15:54:02', '2022-02-24 15:54:02', '2022-01-25 15:53:35', '2022-01-25 16:00:01', NULL),
	(29, NULL, 0, -1, 'gioi_thieu,gioi_thieu_center', 0, '2022-01-25 16:18:05', '2022-02-24 16:18:05', '2022-01-25 16:03:11', '2022-01-25 16:33:14', NULL),
	(30, NULL, 0, -1, 'big_home', 0, '2022-02-10 14:45:20', '2022-03-12 14:45:20', '2022-02-10 14:45:20', '2022-02-10 14:48:54', NULL),
	(31, NULL, 0, -1, 'gioi_thieu', 0, '2022-02-10 23:42:04', '2022-03-12 23:42:04', '2022-02-10 23:41:41', '2022-02-10 23:42:17', NULL),
	(32, NULL, 0, -1, 'ung_dung', 0, '2022-02-11 11:43:13', '2022-03-13 11:43:13', '2022-02-11 11:22:33', '2022-02-11 13:49:26', NULL),
	(33, NULL, 0, -1, 'big_home', 0, '2022-02-11 13:58:27', '2022-03-13 13:58:27', '2022-02-11 13:58:27', '2022-02-11 13:58:36', NULL),
	(34, 1, 0, 2, 'lien_he', 0, '2022-02-25 11:20:08', '2022-03-27 11:20:08', '2022-02-24 12:09:27', '2022-02-25 11:20:08', NULL),
	(35, NULL, 0, 2, 'big_home', 0, '2022-03-30 14:41:08', '2022-04-29 14:41:08', '2022-03-30 14:03:49', '2022-03-30 14:41:08', NULL),
	(36, NULL, 0, 2, 'big_home', 0, '2022-04-08 10:45:22', '2022-05-08 10:45:22', '2022-04-08 10:45:22', '2022-04-08 10:45:22', NULL),
	(37, NULL, 0, 2, 'big_home', 0, '2022-04-08 10:46:00', '2022-05-08 10:46:00', '2022-04-08 10:46:00', '2022-04-08 10:46:00', NULL);
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;

-- Dumping structure for table centurystone.banner_description
CREATE TABLE IF NOT EXISTS `banner_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `item` mediumtext COLLATE utf8_unicode_ci,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.banner_description: ~38 rows (approximately)
/*!40000 ALTER TABLE `banner_description` DISABLE KEYS */;
INSERT INTO `banner_description` (`id`, `banner_id`, `language_id`, `name`, `short_description`, `image`, `item`, `link`) VALUES
	(7, 4, 1, 'SẢN PHẨM', 'Việt Vision tin rằng để tầm nhìn rõ, sắc nét thì chất lượng mắt kính là chưa đủ. Đủ là phải kết hợp tư vấn và đo khám chuẩn. Kính mắt còn là phụ kiện thời trang thể hiện sự sang trọng, giàu có. Kính mắt Việt Vision là đại diện một doanh nhân thành đạt, một lãnh đạo tầm vóc, thông minh tinh tế với phong cách thanh lịch tự nhiên.', 'banner-1638497098.png', '[]', NULL),
	(8, 4, 2, 'PRODUCT', 'Viet Vision believes that for clear and sharp vision, the quality of glasses is not enough. It is enough to combine consultation and standard examination. Eyeglasses are also a fashion accessory that shows luxury and wealth. Viet Vision eyewear represents a successful businessman, a leader of stature, smart and sophisticated with natural elegant style.', 'banner-1638497098.png', '[]', NULL),
	(38, 19, 1, 'CENTURY STONE', 'Một trong những công ty sản xuất đá thạch anh nhân tạo mà được trang bị dây chuyền với kỹ thuật tiên tiến, công nghệ mới nhất hiện nay. Được thành lập tháng 12 năm 2020 tại Cụm công nghiệp Bãi Ba, xã Đông Thành, huyện Thanh Ba, tỉnh Phú Thọ.', '', '[]', NULL),
	(39, 19, 2, 'CENTURY STONE', 'One of the companies producing artificial quartz stone that is equipped with advanced technology and latest technology today. Established in December 2020 at Bai Ba Industrial Cluster, Dong Thanh Commune, Thanh Ba District, Phu Tho Province.', '', '[]', NULL),
	(40, 20, 1, 'Sản phẩm', NULL, 'banner-1642144244.png', '[]', NULL),
	(41, 20, 2, NULL, NULL, 'banner-1642144244.png', '[]', NULL),
	(42, 21, 1, 'Bộ sưu tập', NULL, 'banner-1642144355.png', '[]', NULL),
	(43, 21, 2, NULL, NULL, 'banner-1642144355.png', '[]', NULL),
	(44, 22, 1, 'Century Stone', 'many options, one choice', 'banner-1642389212.png', '[]', NULL),
	(45, 22, 2, 'Century Stone', 'many options, one choice', 'banner-1642389264.png', '[]', NULL),
	(46, 23, 1, 'Century Stone', 'many options, one choice', 'banner-1642389264.png', '[]', NULL),
	(47, 23, 2, 'Century Stone', 'many options, one choice', 'banner-1642389264.png', '[]', NULL),
	(48, 24, 1, 'banner quảng cáo', NULL, 'banner-1642405902.png', '[]', NULL),
	(49, 24, 2, NULL, NULL, 'banner-1642405902.png', '[]', NULL),
	(50, 25, 1, 'Century Stone', 'Cập nhật những tin tức mới nhất về thị trường tại Việt Nam và quốc tế, cùng những bài viết chuyên sâu, những lời khuyên từ các chuyên gia tại Century Stone.', '', '[]', NULL),
	(51, 25, 2, 'Century Stone', 'Update the latest news about the market in Vietnam and internationally, with in-depth articles and advice from experts at Century Stone.', '', '[]', NULL),
	(52, 25, 3, NULL, NULL, '', '[]', NULL),
	(53, 26, 1, 'Không gian ứng dụng', 'Hãy để chúng tôi biến ước mơ của bạn thành hiện thực. Chúng tôi lắng nghe bạn và hoan nghênh những ý tưởng của bạn. Hãy đến thăm chúng tôi và bạn sẽ thấy rằng tất cả đều là sự thật.', '', '[]', NULL),
	(54, 26, 2, 'Application space', NULL, '', '[]', NULL),
	(55, 26, 3, '应用空间', NULL, '', '[]', NULL),
	(59, 24, 3, NULL, NULL, 'banner-1642405902.png', '[]', NULL),
	(60, 22, 3, 'Century Stone', '多种选择，一种选择', 'banner-1642389264.png', '[]', NULL),
	(61, 23, 3, NULL, NULL, 'banner-1642389264.png', '[]', NULL),
	(62, 21, 3, NULL, NULL, 'banner-1642144355.png', '[]', NULL),
	(63, 20, 3, NULL, NULL, 'banner-1642144244.png', '[]', NULL),
	(64, 19, 3, 'CENTURY STONE', '生产具有当今先进技术和最新技术的人造石英石的公司之一。于 2020 年 12 月在富寿省清巴区东清公社白巴工业区成立。', '', '[]', NULL),
	(86, 34, 1, 'Thông tin liên hệ', 'Đội ngũ chuyên gia tại Century Stone sẵn lòng tư vấn tất cả các nhu cầu của bạn, chúng tôi cam kết sẽ luôn đồng hành với các bạn bất cứ lúc nào.', 'banner-1645762807.png', '[]', NULL),
	(87, 34, 2, 'Contact information', 'The team of experts at Century Stone are happy to advise all of your needs, we are committed to always accompany you at any time.', 'banner-1645762807.png', '[]', NULL),
	(88, 34, 3, '联系信息', 'Century Stone 的专家团队乐于为您的所有需求提供建议，我们致力于随时陪伴您。', 'banner-1645762807.png', '[]', NULL),
	(89, 35, 1, 'banner trang chủ', NULL, 'banner-1648626065.jpg', '[]', NULL),
	(90, 35, 2, NULL, NULL, 'banner-1648626065.jpg', '[]', NULL),
	(91, 35, 3, NULL, NULL, 'banner-1648626065.jpg', '[]', NULL),
	(92, 36, 1, 'Banner trang chủ', NULL, 'banner-1649389519.jpg', '[]', NULL),
	(93, 36, 2, NULL, NULL, 'banner-1649389519.jpg', '[]', NULL),
	(94, 36, 3, NULL, NULL, 'banner-1649389519.jpg', '[]', NULL),
	(95, 37, 1, 'Banner trang chủ 1', NULL, 'banner-1649389557.jpg', '[]', NULL),
	(96, 37, 2, NULL, NULL, 'banner-1649389557.jpg', '[]', NULL),
	(97, 37, 3, NULL, NULL, 'banner-1649389557.jpg', '[]', NULL);
/*!40000 ALTER TABLE `banner_description` ENABLE KEYS */;

-- Dumping structure for table centurystone.bizfly_chatbot
CREATE TABLE IF NOT EXISTS `bizfly_chatbot` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bizfly_chatbot_id` bigint(20) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `embed_script` varchar(600) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `embed_id` int(11) DEFAULT NULL,
  `is_replace_default_text` tinyint(1) DEFAULT '0',
  `is_renamed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `bizflychat_customer_customer_id_IDX` (`customer_id`,`bizfly_chatbot_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.bizfly_chatbot: ~0 rows (approximately)
/*!40000 ALTER TABLE `bizfly_chatbot` DISABLE KEYS */;
/*!40000 ALTER TABLE `bizfly_chatbot` ENABLE KEYS */;

-- Dumping structure for table centurystone.bizfly_chatbot_user
CREATE TABLE IF NOT EXISTS `bizfly_chatbot_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bot_id` int(11) NOT NULL,
  `bizfly_chat_user_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_setting_email` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `chatbot_user_customer_id_IDX` (`customer_id`,`bot_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.bizfly_chatbot_user: ~0 rows (approximately)
/*!40000 ALTER TABLE `bizfly_chatbot_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `bizfly_chatbot_user` ENABLE KEYS */;

-- Dumping structure for table centurystone.category
CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned DEFAULT '0',
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint(1) DEFAULT '1',
  `top` tinyint(1) DEFAULT '0',
  `hot` tinyint(1) DEFAULT '0',
  `is_good_price` tinyint(1) unsigned DEFAULT '0' COMMENT 'Là danh mục sản phẩm giá tốt ',
  `show_freeship_page` tinyint(1) unsigned DEFAULT '0' COMMENT 'Show trên chuyên trang Freeship',
  `show_sale_page` tinyint(1) unsigned DEFAULT '0' COMMENT 'Show trên chuyên trang Khuyến Mãi',
  `column` int(3) DEFAULT NULL,
  `sort_order` int(3) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `eshop_cate_id` int(11) DEFAULT NULL,
  `children_categories` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `draft_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primary_color` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Gam màu chủ đạo cho ngành hàng',
  `second_color` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Gam màu thứ cấp',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`category_id`) USING BTREE,
  UNIQUE KEY `category_eshop_cate_id_IDX` (`eshop_cate_id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.category: ~58 rows (approximately)
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`category_id`, `parent_id`, `image`, `icon`, `icon2`, `type`, `top`, `hot`, `is_good_price`, `show_freeship_page`, `show_sale_page`, `column`, `sort_order`, `status`, `eshop_cate_id`, `children_categories`, `draft_name`, `primary_color`, `second_color`, `created_at`, `updated_at`, `position`) VALUES
	(1, 0, 'category-1638514568.png', NULL, NULL, 1, 0, 0, 0, 0, 0, NULL, 1, -1, NULL, NULL, NULL, NULL, NULL, '2021-11-23 15:50:28', '2022-01-18 16:45:54', NULL),
	(2, 0, 'category-1642994915.jpg', 'category-1642994899.jpg', NULL, 1, 0, 0, 0, 0, 0, NULL, 3, 2, NULL, NULL, NULL, NULL, NULL, '2021-11-23 15:52:14', '2022-01-24 10:28:35', NULL),
	(3, 0, 'category-1644481801.jpg', 'category-1644481774.jpg', NULL, 1, 0, 0, 0, 0, 0, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, '2021-11-23 15:53:54', '2022-02-10 15:30:01', NULL),
	(4, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, 1, -1, NULL, NULL, NULL, NULL, NULL, '2021-11-24 10:14:35', '2022-01-21 16:49:21', NULL),
	(5, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, 2, -1, NULL, NULL, NULL, NULL, NULL, '2021-11-24 10:15:06', '2022-01-21 16:49:17', 'page_dieu_can_biet'),
	(6, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, 3, -1, NULL, NULL, NULL, NULL, NULL, '2021-11-25 16:58:19', '2022-01-21 16:49:45', 'page_tin_tuc'),
	(7, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, 4, -1, NULL, NULL, NULL, NULL, NULL, '2021-11-25 16:59:09', '2022-01-21 16:49:35', 'page_tin_tuc'),
	(8, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, 5, -1, NULL, NULL, NULL, NULL, NULL, '2021-11-26 10:53:54', '2022-01-21 16:49:27', 'page_gioi_thieu'),
	(11, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, '2021-11-26 16:54:58', '2022-01-21 16:49:32', 'page_do_kham'),
	(12, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, 3, -1, NULL, NULL, NULL, NULL, NULL, '2021-11-30 16:36:25', '2022-01-21 16:49:13', 'page_dieu_can_biet'),
	(13, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, 5, -1, NULL, NULL, NULL, NULL, NULL, '2021-11-30 17:38:32', '2022-01-21 16:49:39', 'page_tin_tuc'),
	(14, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, '2021-12-02 09:40:44', '2021-12-02 09:44:42', 'page_tin_tuc'),
	(15, 0, 'category-1638765148.png', NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, 4, -1, NULL, NULL, NULL, NULL, NULL, '2021-12-06 11:32:29', '2021-12-06 11:33:42', 'page_tin_tuc'),
	(16, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, 5, -1, NULL, NULL, NULL, NULL, NULL, '2021-12-06 11:35:39', '2021-12-06 13:54:51', 'page_dieu_can_biet'),
	(17, 0, 'category-1638773736.jpg', NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, '2021-12-06 13:55:36', '2021-12-06 13:57:39', 'page_dieu_can_biet'),
	(18, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, '2021-12-09 13:59:37', '2021-12-09 14:20:41', NULL),
	(19, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:05:27', '2021-12-09 14:05:27', NULL),
	(20, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:10:33', '2021-12-09 14:10:33', NULL),
	(21, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:11:07', '2021-12-09 14:11:07', NULL),
	(22, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:11:48', '2021-12-09 14:11:48', NULL),
	(23, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:13:41', '2021-12-09 14:13:41', NULL),
	(24, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:14:03', '2021-12-09 14:14:03', NULL),
	(25, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:14:50', '2021-12-09 14:14:50', NULL),
	(26, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:15:02', '2021-12-09 14:15:02', NULL),
	(27, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:15:12', '2021-12-09 14:15:12', NULL),
	(28, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:15:23', '2021-12-09 14:15:23', NULL),
	(29, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:15:35', '2021-12-09 14:15:35', NULL),
	(30, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:15:47', '2021-12-09 14:15:47', NULL),
	(31, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:15:56', '2021-12-09 14:15:56', NULL),
	(32, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:16:15', '2021-12-09 14:16:15', NULL),
	(33, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:16:31', '2021-12-09 14:16:31', NULL),
	(34, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:17:32', '2021-12-09 14:17:32', NULL),
	(35, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:17:45', '2021-12-09 14:17:45', NULL),
	(36, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:18:32', '2021-12-09 14:18:32', NULL),
	(37, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:18:41', '2021-12-09 14:18:41', NULL),
	(38, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:19:22', '2021-12-09 14:19:22', NULL),
	(39, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:19:55', '2021-12-09 14:19:55', NULL),
	(40, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:20:13', '2021-12-09 14:20:13', NULL),
	(41, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:20:24', '2021-12-09 14:20:24', NULL),
	(42, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:20:32', '2021-12-09 15:09:18', NULL),
	(43, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:53:14', '2021-12-09 14:53:14', NULL),
	(44, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:53:39', '2021-12-09 14:53:39', NULL),
	(45, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:54:24', '2021-12-09 14:54:24', NULL),
	(46, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:54:40', '2021-12-09 14:54:40', NULL),
	(47, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:55:02', '2021-12-09 14:55:02', NULL),
	(48, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:55:23', '2021-12-09 14:55:23', NULL),
	(49, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:55:42', '2021-12-09 14:55:42', NULL),
	(50, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:55:54', '2021-12-09 14:55:54', NULL),
	(51, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:56:04', '2021-12-09 14:56:04', NULL),
	(52, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:56:11', '2021-12-09 14:56:11', NULL),
	(53, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:56:26', '2021-12-09 14:56:26', NULL),
	(54, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:57:57', '2021-12-09 14:57:57', NULL),
	(55, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:58:25', '2021-12-09 14:58:25', NULL),
	(56, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:58:43', '2021-12-09 14:58:43', NULL),
	(57, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:58:48', '2021-12-09 14:58:48', NULL),
	(58, 0, 'category-1639036929.jpg', NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, '2021-12-09 14:59:49', '2021-12-09 15:09:09', NULL),
	(59, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, '2021-12-09 15:09:44', '2021-12-09 15:41:30', 'page_dieu_can_biet,page_gioi_thieu,page_tin_tuc,page_do_kham'),
	(60, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, 8, -1, NULL, NULL, NULL, NULL, NULL, '2021-12-09 16:03:19', '2021-12-09 17:34:54', 'page_tin_tuc'),
	(61, 0, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, NULL, 1, -1, NULL, NULL, NULL, NULL, NULL, '2022-01-18 10:35:40', '2022-01-18 16:32:33', NULL),
	(62, 0, 'category-1642499132.png', 'category-1642994360.png', NULL, 1, 0, 0, 0, 0, 0, NULL, 1, 2, NULL, NULL, NULL, NULL, NULL, '2022-01-18 16:45:32', '2022-01-24 10:19:20', NULL),
	(63, 0, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, '2022-01-18 16:48:13', '2022-01-24 10:18:05', NULL),
	(64, 0, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2022-01-21 16:50:24', '2022-01-21 16:50:24', NULL),
	(65, 0, 'category-1644481899.jpg', 'category-1644481868.jpg', NULL, 1, 0, 0, 0, 0, 0, NULL, 4, 2, NULL, NULL, NULL, NULL, NULL, '2022-01-24 10:36:52', '2022-02-10 15:31:39', NULL),
	(66, 0, 'category-1644476974.jpg', 'category-1642995569.jpg', NULL, 1, 0, 0, 0, 0, 0, NULL, 5, 2, NULL, NULL, NULL, NULL, NULL, '2022-01-24 10:39:29', '2022-02-10 14:09:34', NULL),
	(67, 0, 'category-1643083710.png', NULL, NULL, 1, 0, 0, 0, 0, 0, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, '2022-01-25 11:08:00', '2022-01-25 11:08:44', NULL);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Dumping structure for table centurystone.category_description
CREATE TABLE IF NOT EXISTS `category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`category_id`,`language_id`) USING BTREE,
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.category_description: ~66 rows (approximately)
/*!40000 ALTER TABLE `category_description` DISABLE KEYS */;
INSERT INTO `category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`, `slug`, `item`) VALUES
	(1, 1, 'Khu vực nhà bếp', '<p>Sản phẩm Century Stone lu&ocirc;n l&agrave;m nổi bật kh&ocirc;ng gian bếp của bạn với khả năng chống thấm, chống b&aacute;m bẩn, độ bền cao v&agrave; dải m&agrave;u trang nh&atilde; ấn tượng</p>', NULL, NULL, NULL, 'khu-vuc-nha-bep', '[]'),
	(1, 2, 'GLASSES', '<p>Brief description of the frame product. Brief description of the frame product. Brief description of the frame product. Brief description of the frame product. Brief description of the frame product. Brief description of the frame product. Brief description of the frame product</p>', NULL, NULL, NULL, 'glasses-frames', '[]'),
	(2, 1, 'Khu vực phòng khách', '<p>Sản phẩm Century Stone luôn làm nổi bật không gian bếp của bạn với khả năng chống thấm, chống bám bẩn, độ bền cao và dải màu trang nhã ấn tượng</p>', NULL, NULL, NULL, 'khu-vuc-phong-khach', '[]'),
	(2, 2, 'Living room area', '<p>Century Stone products always highlight your kitchen space with waterproofing, anti-fouling, high durability and impressive elegant color range.</p>', NULL, NULL, NULL, 'living', '[]'),
	(2, 3, '客厅区域', '<p>Century Stone 产品始终以防水、防污、高耐用性和令人印象深刻的优雅色彩范围突出您的厨房空间。</p>', NULL, NULL, NULL, 'ke-ting-qu-yu', '[]'),
	(3, 1, 'Khu vực nhà tắm', '<p>Sản phẩm Century Stone luôn làm nổi bật không gian bếp của bạn với khả năng chống thấm, chống bám bẩn, độ bền cao và dải màu trang nhã ấn tượng</p>', NULL, NULL, NULL, 'khu-vuc-nha-tam', '[]'),
	(3, 2, 'Bathroom area', '<p>Century Stone products always highlight your kitchen space with waterproofing, anti-fouling, high durability and impressive elegant color range.</p>', NULL, NULL, NULL, 'lenses', '[]'),
	(3, 3, '浴室区', '<p>Century Stone 产品始终以防水、防污、高耐用性和令人印象深刻的优雅色彩范围突出您的厨房空间。</p>', NULL, NULL, NULL, 'yu-shi-qu', '[]'),
	(4, 1, 'Danh mục tin tức 1', NULL, NULL, NULL, NULL, '', '[{"item_image":null,"item_title":"Vi\\u1ec7t Vision - Th\\u01b0\\u01a1ng hi\\u1ec7u k\\u00ednh m\\u1eaft gi\\u00fap c\\u00e1c doanh nh\\u00e2n","item_iframe":"https:\\/\\/www.youtube.com\\/watch?v=EGxDqbT2Vms"},{"item_image":null,"item_title":"\\u0110eo k\\u00ednh sai s\\u1ed1, l\\u1eddi c\\u1ea3nh b\\u00e1o c\\u1ee7a b\\u00e1c s\\u0129 nh\\u00e3n khoa","item_iframe":"https:\\/\\/www.youtube.com\\/watch?v=EGxDqbT2Vms"},{"item_image":null,"item_title":"Kh\\u00e1m m\\u1eaft khi n\\u00e0o, nh\\u1eefng l\\u01b0u \\u00fd c\\u1ea7n bi\\u1ebft khi \\u0111i kh\\u00e1m m\\u1eaft","item_iframe":"https:\\/\\/www.youtube.com\\/watch?v=EGxDqbT2Vms"},{"item_image":null,"item_title":"C\\u00e1ch ch\\u1ecdn m\\u1eaft k\\u00ednh ph\\u00f9 h\\u1ee3p v\\u1edbi g\\u01b0\\u01a1ng m\\u1eb7t","item_iframe":"https:\\/\\/www.youtube.com\\/watch?v=EGxDqbT2Vms"},{"item_image":null,"item_title":"H\\u01b0\\u1edbng d\\u1eabn \\u0111\\u1ecdc \\u0111\\u01a1n k\\u00ednh m\\u1eaft c\\u1ee7a b\\u1ea1n","item_iframe":"https:\\/\\/www.youtube.com\\/watch?v=EGxDqbT2Vms"},{"item_image":null,"item_title":"C\\u00e1ch b\\u1ea3o qu\\u1ea3n thu\\u1ed1c nh\\u1ecf m\\u1eaft","item_iframe":null}]'),
	(4, 2, 'News Category 1', NULL, NULL, NULL, NULL, '', '[{"item_image":null,"item_title":"C\\u00e1ch b\\u1ea3o qu\\u1ea3n thu\\u1ed1c nh\\u1ecf m\\u1eaft","item_iframe":null}]'),
	(4, 3, '新闻类别 -', NULL, NULL, NULL, NULL, '', '[{"item_image":null,"item_title":"C\\u00e1ch b\\u1ea3o qu\\u1ea3n thu\\u1ed1c nh\\u1ecf m\\u1eaft","item_iframe":null}]'),
	(5, 1, 'Kiến thức về sản phẩm', NULL, NULL, NULL, NULL, 'dieu-can-biet-2', '[{"item_title":"Kh\\u00e1m m\\u1eaft khi n\\u00e0o, nh\\u1eefng \\u0111i\\u1ec1u c\\u1ea7n l\\u01b0u \\u00fd","item_iframe":"https:\\/\\/www.youtube.com\\/watch?v=EGxDqbT2Vms"},{"item_title":"\\u0110o v\\u00e0 l\\u1eafp k\\u00ednh cao c\\u1ea5p, ti\\u1ec7n l\\u1ee3i t\\u1eeb c\\u1eeda h\\u00e0ng k\\u00ednh m\\u1eaft Vi\\u1ec7t Vision","item_iframe":"https:\\/\\/www.youtube.com\\/watch?v=EGxDqbT2Vms"}]'),
	(5, 2, 'Product knowledge', NULL, NULL, NULL, NULL, 'dieu-can-biet-2-ta', '[{"item_title":null,"item_iframe":null}]'),
	(6, 1, 'Tin tức nổi bật', NULL, NULL, NULL, NULL, 'tin-tuc-noi-bat', '[{"item_title":null,"item_iframe":null}]'),
	(6, 2, 'News hot', NULL, NULL, NULL, NULL, 'news-hot', '[{"item_title":null,"item_iframe":null}]'),
	(7, 1, 'Tin từ báo chí', NULL, NULL, NULL, NULL, 'tin-tu-bao-chi', '[]'),
	(7, 2, 'News from the press', NULL, NULL, NULL, NULL, 'news-from-the-press', '[]'),
	(8, 1, 'Danh mục giới thiệu', NULL, NULL, NULL, NULL, 'danh-muc-gioi-thieu', '[{"item_image":"tmpphppjx35y-1639039514.jpg","item_title":"video gioi thieu","item_iframe":"https:\\/\\/www.youtube.com\\/watch?v=EGxDqbT2Vms"}]'),
	(8, 2, 'Introductory list', NULL, NULL, NULL, NULL, 'introductory-list', '[{"item_image":"tmpphpieantd-1639039746.jpg","item_title":"video gioi thieu","item_iframe":"https:\\/\\/www.youtube.com\\/watch?v=EGxDqbT2Vms"}]'),
	(11, 1, 'Danh Mục Đo Khám', NULL, NULL, NULL, NULL, 'danh-muc-do-kham', '[]'),
	(11, 2, 'Checklist', NULL, NULL, NULL, NULL, 'checklist', '[]'),
	(11, 3, '', NULL, NULL, NULL, NULL, '', '[]'),
	(12, 1, 'Mẹo hay/ Cách bảo quản/ Sử dụng?', NULL, NULL, NULL, NULL, 'meo-hay-cach-bao-quan-su-dung', '[{"item_image":null,"item_title":"Kh\\u00e1m m\\u1eaft khi n\\u00e0o, nh\\u1eefng \\u0111i\\u1ec1u c\\u1ea7n l\\u01b0u \\u00fd","item_iframe":"https:\\/\\/www.youtube.com\\/watch?v=EGxDqbT2Vms"}]'),
	(12, 2, 'Good tips / How to store / Use?', NULL, NULL, NULL, NULL, 'huge-tip-how-to-storage-use', '[{"item_image":null,"item_title":"Kh\\u00e1m m\\u1eaft khi n\\u00e0o, nh\\u1eefng \\u0111i\\u1ec1u c\\u1ea7n l\\u01b0u \\u00fd","item_iframe":"https:\\/\\/www.youtube.com\\/watch?v=EGxDqbT2Vms"}]'),
	(13, 1, 'Sự kiện nổi bật', NULL, NULL, NULL, NULL, 'su-kien-noi-bat', '[{"item_title":null,"item_iframe":null}]'),
	(13, 2, 'Big event', '<p>Big event</p>', NULL, NULL, NULL, 'big-event', '[{"item_title":null,"item_iframe":null}]'),
	(14, 1, 'Chăm sóc khách hàng', NULL, NULL, NULL, NULL, 'cham-soc-khach-hang', '[{"item_title":null,"item_iframe":null}]'),
	(14, 2, 'Customer Care', NULL, NULL, NULL, NULL, 'customer-care', '[{"item_title":null,"item_iframe":null}]'),
	(15, 1, 'danh mục khác nữa', NULL, NULL, NULL, NULL, 'danh-muc-khac-nua', '[{"item_title":null,"item_iframe":null}]'),
	(15, 2, 'danh mục khác nữa', NULL, NULL, NULL, NULL, 'danh-muc-khac-nua', '[{"item_title":null,"item_iframe":null}]'),
	(16, 1, 'Có thể bạn chưa biết?', NULL, NULL, NULL, NULL, 'jfdgn', '[{"item_title":"gj","item_iframe":"https:\\/\\/www.youtube.com\\/watch?v=EGxDqbT2Vms"}]'),
	(16, 2, 'Có thể bạn chưa biết?', NULL, NULL, NULL, NULL, 'co-the-ban-chua-biet', '[{"item_title":null,"item_iframe":null}]'),
	(17, 1, 'sdbrg', NULL, NULL, NULL, NULL, 'sdbrg', '[{"item_title":null,"item_iframe":null}]'),
	(17, 2, '', NULL, NULL, NULL, NULL, '', '[{"item_title":null,"item_iframe":null}]'),
	(18, 1, 'test', '', '', '', '', '', '[{"item_image":null,"item_title":{"0":"ttt"},"item_iframe":{"0":null}}]'),
	(18, 2, 'test', '', '', '', '', '', '[{"item_image":null,"item_title":{"0":null},"item_iframe":{"0":null}}]'),
	(42, 1, 'test', NULL, NULL, NULL, NULL, '', '[{"item_image":"cuserstungvappdatalocaltempphpa6bdtmp-1639036257.jpg","item_title":"tests","item_iframe":null}]'),
	(42, 2, 'test', NULL, NULL, NULL, NULL, '', '[{"item_image":null,"item_title":null,"item_iframe":null}]'),
	(54, 1, 'test', NULL, NULL, NULL, NULL, '', '[{"item_image":"cuserstungvappdatalocaltempphpfa1tmp-1639036677.jpg","item_title":null,"item_iframe":null},{"item_image":"cuserstungvappdatalocaltempphpfa2tmp-1639036677.jpg","item_title":null,"item_iframe":null}]'),
	(54, 2, 'ttt', NULL, NULL, NULL, NULL, '', '[{"item_image":"cuserstungvappdatalocaltempphpfb2tmp-1639036677.jpg","item_title":"ttttt","item_iframe":null}]'),
	(57, 1, 'test', NULL, NULL, NULL, NULL, '', '[{"item_image":"cuserstungvappdatalocaltempphpd78etmp-1639036728.jpg","item_title":null,"item_iframe":null},{"item_image":"cuserstungvappdatalocaltempphpd78ftmp-1639036728.jpg","item_title":null,"item_iframe":null}]'),
	(57, 2, 'ttt', NULL, NULL, NULL, NULL, '', '[{"item_image":"cuserstungvappdatalocaltempphpd79ftmp-1639036728.jpg","item_title":"ttttt","item_iframe":null}]'),
	(58, 1, 'ttt', NULL, NULL, NULL, NULL, '', '[]'),
	(58, 2, 'ttt', NULL, NULL, NULL, NULL, '', '[{"item_image":"cuserstungvappdatalocaltempphp27e0tmp-1639036814.jpg","item_title":"tttt","item_iframe":null}]'),
	(59, 1, 'test', NULL, NULL, NULL, NULL, '', '[{"item_image":"cuserstungvappdatalocaltempphpbf2ctmp-1639038099.jpg","item_title":"ttt","item_iframe":"https:\\/\\/www.youtube.com\\/watch?v=EGxDqbT2Vms"}]'),
	(59, 2, 'test', NULL, NULL, NULL, NULL, '', '[{"item_image":"cuserstungvappdatalocaltempphpdab6tmp-1639037384.jpg","item_title":"tttt","item_iframe":null},{"item_image":"cuserstungvappdatalocaltempphpdab7tmp-1639037384.png","item_title":"ccc","item_iframe":null}]'),
	(60, 1, 'dsfh', NULL, NULL, NULL, NULL, '', '[]'),
	(60, 2, '', NULL, NULL, NULL, NULL, '', '[]'),
	(61, 1, 'SẢN PHẨM NỔI BẬT', '<p style="text-align: center;">L&agrave; một trong những doanh nghiệp h&agrave;ng đầu thế giới về sản xuất v&agrave; ph&acirc;n phối đ&aacute; nh&acirc;n tạo gốc thạch anh cao cấp,</p>\r\n<p style="text-align: center;">Century Stone tự h&agrave;o mang đến cho kh&aacute;ch h&agrave;ng c&aacute;c xu hướng thiết kế mới nhất</p>', NULL, NULL, NULL, '', '[]'),
	(61, 2, '', NULL, NULL, NULL, NULL, '', '[]'),
	(61, 3, '', NULL, NULL, NULL, NULL, '', '[]'),
	(62, 1, 'Khu vực nhà bếp', '<p>Sản phẩm Century Stone lu&ocirc;n l&agrave;m nổi bật kh&ocirc;ng gian bếp của bạn với khả năng chống thấm, chống b&aacute;m bẩn, độ bền cao v&agrave; dải m&agrave;u trang nh&atilde; ấn tượng</p>', NULL, NULL, NULL, 'khu-vuc-nha-bep', '[]'),
	(62, 2, 'Kitchen area', '<p>Century Stone products always highlight your kitchen space with waterproofing, anti-fouling, high durability and impressive elegant color range.</p>', NULL, NULL, NULL, 'kitchen-area', '[]'),
	(62, 3, '厨房区', '<p>Century Stone 产品始终以防水、防污、高耐用性和令人印象深刻的优雅色彩范围突出您的厨房空间。</p>', NULL, NULL, NULL, 'chu-fang-qu', '[]'),
	(63, 1, '111', NULL, NULL, NULL, NULL, '', '[]'),
	(63, 2, '', NULL, NULL, NULL, NULL, '', '[]'),
	(63, 3, '', NULL, NULL, NULL, NULL, '', '[]'),
	(64, 1, 'Tin tức', NULL, NULL, NULL, NULL, 'tin-tuc', '[]'),
	(64, 2, 'News', NULL, NULL, NULL, NULL, 'news', '[]'),
	(64, 3, '消息', NULL, NULL, NULL, NULL, 'xiao-xi', '[]'),
	(65, 1, 'Khu vực văn phòng', '<p>Sản phẩm Century Stone luôn làm nổi bật không gian bếp của bạn với khả năng chống thấm, chống bám bẩn, độ bền cao và dải màu trang nhã ấn tượng</p>', NULL, NULL, NULL, 'khu-vuc-van-phong', '[]'),
	(65, 2, 'Office area', '<p>Century Stone products always highlight your kitchen space with waterproofing, anti-fouling, high durability and impressive elegant color range.</p>', NULL, NULL, NULL, 'office-area', '[]'),
	(65, 3, '浴室区', '<p>Century Stone 产品始终以防水、防污、高耐用性和令人印象深刻的优雅色彩范围突出您的厨房空间。</p>', NULL, NULL, NULL, 'yu-shi-qu', '[]'),
	(66, 1, 'Các sản phẩm khác', 'Sản phẩm Century Stone luôn làm nổi bật không gian bếp của bạn với khả năng chống thấm, chống bám bẩn, độ bền cao và dải màu trang nhã ấn tượng', NULL, NULL, NULL, 'cac-san-pham-khac', '[]'),
	(66, 2, 'Other products', 'Century Stone products always highlight your kitchen space with waterproofing, anti-fouling, high durability and impressive elegant color range.', NULL, NULL, NULL, 'other-products', '[]'),
	(66, 3, '其他产品', 'Century Stone 产品始终以防水、防污、高耐用性和令人印象深刻的优雅色彩范围突出您的厨房空间。', NULL, NULL, NULL, 'qi-ta-chan-pin', '[]'),
	(67, 1, '', NULL, NULL, NULL, NULL, '', '[]'),
	(67, 2, '', NULL, NULL, NULL, NULL, '', '[]'),
	(67, 3, '', NULL, NULL, NULL, NULL, '', '[]');
/*!40000 ALTER TABLE `category_description` ENABLE KEYS */;

-- Dumping structure for table centurystone.category_filter
CREATE TABLE IF NOT EXISTS `category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`filter_group_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.category_filter: ~0 rows (approximately)
/*!40000 ALTER TABLE `category_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_filter` ENABLE KEYS */;

-- Dumping structure for table centurystone.category_path
CREATE TABLE IF NOT EXISTS `category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`path_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- Dumping data for table centurystone.category_path: 27 rows
/*!40000 ALTER TABLE `category_path` DISABLE KEYS */;
INSERT INTO `category_path` (`category_id`, `path_id`, `level`) VALUES
	(1, 1, 0),
	(2, 2, 0),
	(3, 3, 0),
	(4, 4, 0),
	(5, 5, 0),
	(6, 6, 0),
	(7, 7, 0),
	(8, 8, 0),
	(11, 11, 0),
	(12, 12, 0),
	(13, 13, 0),
	(14, 14, 0),
	(15, 15, 0),
	(16, 16, 0),
	(17, 17, 0),
	(18, 18, 0),
	(42, 42, 0),
	(58, 58, 0),
	(59, 59, 0),
	(60, 60, 0),
	(61, 61, 0),
	(62, 62, 0),
	(63, 63, 0),
	(64, 64, 0),
	(65, 65, 0),
	(66, 66, 0),
	(67, 67, 0);
/*!40000 ALTER TABLE `category_path` ENABLE KEYS */;

-- Dumping structure for table centurystone.collections
CREATE TABLE IF NOT EXISTS `collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_hover` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_ids` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_order` int(10) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `is_good_price` tinyint(4) DEFAULT '1' COMMENT '0:tắt 1: bật Tùy chọn giá tốt trong ngày',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.collections: ~5 rows (approximately)
/*!40000 ALTER TABLE `collections` DISABLE KEYS */;
INSERT INTO `collections` (`id`, `image`, `image_hover`, `title`, `category_ids`, `sort_order`, `created`, `color`, `status`, `is_good_price`, `updated_at`, `created_at`) VALUES
	(1, 'collection-img-1642669003.png', 'ptg-1645611726.jpg', NULL, NULL, 1, 1642668764, NULL, 1, 0, '2022-03-19 09:11:16', '2022-01-20 15:52:44'),
	(2, 'rectangle-13373-1-1643005349.jpg', 'ptg-1645611726.jpg', NULL, NULL, 2, 1642668822, NULL, 1, 0, '2022-02-23 10:22:32', '2022-01-20 15:53:42'),
	(3, 'rectangle-13370-1643005436.jpg', 'ptg-1645611726.jpg', NULL, NULL, 3, 1642668872, NULL, 1, 0, '2022-02-23 10:22:32', '2022-01-20 15:54:32'),
	(4, 'rectangle-13370-1-1643343408.jpg', 'ptg-1645611726.jpg', NULL, NULL, 5, 1642668913, NULL, 1, 0, '2022-02-23 10:22:32', '2022-01-20 15:55:13'),
	(5, 'rectangle-13373-2-1643005776.jpg', 'ptg-1645611726.jpg', NULL, NULL, 4, 1642757251, NULL, 1, 0, '2022-02-23 17:22:10', '2022-01-21 16:27:31');
/*!40000 ALTER TABLE `collections` ENABLE KEYS */;

-- Dumping structure for table centurystone.collection_description
CREATE TABLE IF NOT EXISTS `collection_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `meta_title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `meta_description` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `meta_keyword` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table centurystone.collection_description: 39 rows
/*!40000 ALTER TABLE `collection_description` DISABLE KEYS */;
INSERT INTO `collection_description` (`id`, `collection_id`, `language_id`, `name`, `short_description`, `description`, `meta_title`, `meta_description`, `meta_keyword`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 'Kiệt tác lớn', 'Với phương châm tạo ra các tác phẩm nghệ thuật trong ngôi nhà của bạn bằng những tấm đá Thạch anh với mầu sắc và đường vân phong phú, mới lạ mô phỏng theo loại đá Calacatta, Carrara nổi tiếng của Italia. Bộ sưu tập Kiệt tác lớn bao gồm các sản phẩm tấm đá Cenvistone được làm từ nguyên liệu Thạch anh chất lượng cao dưới bàn tay của người thợ lành nghề kết hợp với hệ thống máy móc công nghệ hiện đại tạo nên các phiến đá với những đường vân độc đáo, khỏe khoắn đặc trưng của đá Calacatta và Carrara.', NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 1, 2, 'Quartz Crystal', 'Century Stone Crystal Artificial Quartz\n made of 90% - 97% crushed quartz combined with resins, adhesives,\n and coloring agents to form\n quartz. This mixture is produced\n on the most modern production line\n with high durability. Basically, artificial quartz stone\n more affordable and\n used more commonly than natural stones; as well as more aesthetic\n when designing for interior areas, commercial buildings,\n House.', NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 1, 3, '石英晶体', '世纪石水晶人造石英\n 由 90% - 97% 碎石英与树脂、粘合剂、\n 和着色剂形成\n 石英。这种混合物产生\n 在最现代化的生产线上\n 具有高耐久性。基本上，人造石英石\n 更实惠和\n 比天然石头更常用；也更美观\n 在设计室内区域、商业建筑时，\n 房子。', NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 2, 1, 'Thạch anh Natural', 'Đá thạch anh nhân tạo Pha lê Century Stone được làm từ 90% - 97% thạch anh nghiền được kết hợp với nhựa, chất kết dính, và các chất tạo màu để tạo thành đá thạch anh. Hỗn hợp này được sản xuất trên dây chuyền sản xuất hiện đại nhất với độ bền cao. Về cơ bản, đá thạch anh nhân tạo có giá cả phải chăng hơn và được sử dụng thông dụng hơn so với các loại đá tự nhiên; cũng như thẩm mỹ hơn khi thiết kế cho các khu vực nội thất, các tòa nhà thương mại, nhà ở.', NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 2, 2, 'Natural Quartz', 'Artificial Quartz Stone Century Stone Crystal is made of 90% - 97% crushed quartz which is combined with resin, binder, and coloring agents to form quartz stone. This mixture is produced on the most modern production lines with high durability. Essentially, man-made quartz is more affordable and commonly used than natural stones; as well as more aesthetic when designing for interior areas, commercial and residential buildings.', NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 2, 3, '天然石英', '人造石英石世纪石水晶是由90% - 97%的碎石英与树脂、粘合剂、着色剂结合而成的石英石。这种混合物是在最现代化的生产线上生产的，具有很高的耐用性。从本质上讲，人造石英比天然石材更实惠、更常用；并且在设计室内区域、商业和住宅建筑时更具美感。', NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 3, 1, 'Thạch anh Vân', 'Đá thạch anh nhân tạo Pha lê Century Stone được làm từ 90% - 97% thạch anh nghiền được kết hợp với nhựa, chất kết dính, và các chất tạo màu để tạo thành đá thạch anh. Hỗn hợp này được sản xuất trên dây chuyền sản xuất hiện đại nhất với độ bền cao. Về cơ bản, đá thạch anh nhân tạo có giá cả phải chăng hơn và được sử dụng thông dụng hơn so với các loại đá tự nhiên; cũng như thẩm mỹ hơn khi thiết kế cho các khu vực nội thất, các tòa nhà thương mại, nhà ở.', NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 3, 2, 'Quartz Van', 'Artificial Quartz Stone Century Stone Crystal is made of 90% - 97% crushed quartz which is combined with resin, binder, and coloring agents to form quartz stone. This mixture is produced on the most modern production lines with high durability. Essentially, man-made quartz is more affordable and commonly used than natural stones; as well as more aesthetic when designing for interior areas, commercial and residential buildings.', NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 3, 3, '石英车', '人造石英石世纪石水晶是由90% - 97%的碎石英与树脂、粘合剂、着色剂结合而成的石英石。这种混合物是在最现代化的生产线上生产的，具有很高的耐用性。从本质上讲，人造石英比天然石材更实惠、更常用；并且在设计室内区域、商业和住宅建筑时更具美感。', NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 4, 1, 'Thạch anh Nhũ', 'Đá thạch anh nhân tạo Pha lê Century Stone được làm từ 90% - 97% thạch anh nghiền được kết hợp với nhựa, chất kết dính, và các chất tạo màu để tạo thành đá thạch anh. Hỗn hợp này được sản xuất trên dây chuyền sản xuất hiện đại nhất với độ bền cao. Về cơ bản, đá thạch anh nhân tạo có giá cả phải chăng hơn và được sử dụng thông dụng hơn so với các loại đá tự nhiên; cũng như thẩm mỹ hơn khi thiết kế cho các khu vực nội thất, các tòa nhà thương mại, nhà ở.', NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 4, 2, 'Quartz Crystal', 'Artificial Quartz Stone Century Stone Crystal is made of 90% - 97% crushed quartz which is combined with resin, binder, and coloring agents to form quartz stone. This mixture is produced on the most modern production lines with high durability. Essentially, man-made quartz is more affordable and commonly used than natural stones; as well as more aesthetic when designing for interior areas, commercial and residential buildings.', NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 4, 3, '石英晶体', '人造石英石世纪石水晶是由90% - 97%的碎石英与树脂、粘合剂、着色剂结合而成的石英石。这种混合物是在最现代化的生产线上生产的，具有很高的耐用性。从本质上讲，人造石英比天然石材更实惠、更常用；并且在设计室内区域、商业和住宅建筑时更具美感。', NULL, NULL, NULL, NULL, NULL, NULL),
	(37, 13, 1, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(38, 13, 2, 'tét', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(39, 13, 3, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 5, 1, 'Thạch anh Classic', 'Đá thạch anh nhân tạo Pha lê Century Stone\n được làm từ 90% - 97% thạch anh nghiền được kết hợp với nhựa, chất kết dính,\n và các chất tạo màu để tạo thành\n đá thạch anh. Hỗn hợp này được sản xuất\n trên dây chuyền sản xuất hiện đại nhất\n với độ bền cao. Về cơ bản, đá thạch anh nhân tạo\n có giá cả phải chăng hơn và\n được sử dụng thông dụng hơn so với các loại đá tự nhiên; cũng như thẩm mỹ hơn\n khi thiết kế cho các khu vực nội thất, các tòa nhà thương mại,\n nhà ở.', NULL, NULL, NULL, NULL, NULL, NULL),
	(14, 5, 2, 'Classic Quartz', 'Century Stone Crystal Artificial Quartz\n made of 90% - 97% crushed quartz combined with resins, adhesives,\n and coloring agents to form\n quartz. This mixture is produced\n on the most modern production line\n with high durability. Basically, artificial quartz stone\n more affordable and\n used more commonly than natural stones; as well as more aesthetic\n when designing for interior areas, commercial buildings,\n House.', NULL, NULL, NULL, NULL, NULL, NULL),
	(15, 5, 3, '经典石英', '世纪石水晶人造石英\n 由 90% - 97% 碎石英与树脂、粘合剂、\n 和着色剂形成\n 石英。这种混合物产生\n 在最现代化的生产线上\n 具有高耐久性。基本上，人造石英石\n 更实惠和\n 比天然石头更常用；也更美观\n 在设计室内区域、商业建筑时，\n 房子。', NULL, NULL, NULL, NULL, NULL, NULL),
	(19, 7, 1, 'hh', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(20, 7, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(21, 7, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(22, 8, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(23, 8, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(24, 8, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(25, 9, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(26, 9, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(27, 9, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(28, 10, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(29, 10, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(30, 10, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(31, 11, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(32, 11, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(33, 11, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(34, 12, 1, 'sdfsdfds', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(35, 12, 2, 'sdfdsf', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(36, 12, 3, 'sdfdsf', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(16, 6, 1, 'abc êfef', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(17, 6, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(18, 6, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `collection_description` ENABLE KEYS */;

-- Dumping structure for table centurystone.collection_details
CREATE TABLE IF NOT EXISTS `collection_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_id` int(11) DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.collection_details: ~40 rows (approximately)
/*!40000 ALTER TABLE `collection_details` DISABLE KEYS */;
INSERT INTO `collection_details` (`id`, `collection_id`, `object_id`, `sort`) VALUES
	(4, 1, 1, 0),
	(11, 2, 2, 0),
	(16, 3, 4, 0),
	(19, 4, 6, 0),
	(20, 4, 7, 0),
	(21, 4, 8, 0),
	(22, 3, 5, 0),
	(23, 1, 9, 0),
	(24, 1, 10, 0),
	(25, 1, 11, 0),
	(26, 1, 12, 0),
	(27, 1, 13, 0),
	(29, 1, 15, 0),
	(30, 1, 16, 0),
	(31, 1, 17, 0),
	(32, 1, 18, 0),
	(33, 1, 19, 0),
	(34, 1, 20, 0),
	(35, 1, 21, 0),
	(36, 5, 22, 0),
	(38, 2, 23, 0),
	(39, 2, 24, 0),
	(40, 2, 25, 0),
	(41, 2, 26, 0),
	(42, 2, 28, 0),
	(43, 2, 27, 0),
	(44, 2, 29, 0),
	(45, 2, 30, 0),
	(47, 3, 31, 0),
	(48, 5, 32, 0),
	(49, 5, 33, 0),
	(50, 5, 37, 0),
	(51, 5, 38, 0),
	(52, 5, 42, 0),
	(53, 5, 50, 0),
	(54, 5, 52, 0),
	(55, 5, 61, 0),
	(56, 5, 62, 0),
	(57, 5, 63, 0),
	(58, 5, 64, 0),
	(59, 5, 65, 0);
/*!40000 ALTER TABLE `collection_details` ENABLE KEYS */;

-- Dumping structure for table centurystone.collection_img
CREATE TABLE IF NOT EXISTS `collection_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `image_hover` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `object_id` int(11) DEFAULT '0',
  `type` enum('collection') COLLATE utf8mb4_unicode_ci DEFAULT 'collection' COMMENT 'mo ta cho object id',
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.collection_img: ~49 rows (approximately)
/*!40000 ALTER TABLE `collection_img` DISABLE KEYS */;
INSERT INTO `collection_img` (`id`, `image`, `image_hover`, `object_id`, `type`, `link`, `created`, `user_id`, `sort`) VALUES
	(90, 'bst-but-pha-2-1637134790.png', NULL, 1, 'collection', NULL, 1638775617, 0, 0),
	(91, 'bst-but-pha-3-1637134796.png', NULL, 1, 'collection', NULL, 1638775617, 0, 0),
	(92, 'bst-but-pha-4-1637134802.png', NULL, 1, 'collection', NULL, 1638775617, 0, 0),
	(94, 'creativity-1637201045.jpg', NULL, 10, 'collection', NULL, 1638417641, 0, 0),
	(95, 'desk-1637201054.jpg', NULL, 10, 'collection', NULL, 1638417641, 0, 0),
	(96, 'hqdefault-1637201137.jpg', NULL, 10, 'collection', NULL, 1638417641, 0, 0),
	(110, 'tan-son-nhat-gc-clubhouse-at-night-1-3-1638344109.jpg', NULL, 9, 'collection', NULL, 1638523313, 0, 0),
	(118, 'tan-son-nhat-gc-clubhouse-at-night-1-7-1638354462.jpg', NULL, 20, 'collection', NULL, 1638417569, 0, 0),
	(129, 'cuserstungvappdatalocaltempphp6cd4tmp-1637722727-1638418075.png', NULL, 19, 'collection', NULL, 1638763798, 0, 0),
	(131, '1445395397-shutterstock-1445395397-1-22-1638774751.jpg', NULL, NULL, 'collection', NULL, 1638774751, 0, 0),
	(132, '1445395397-shutterstock-1445395397-1-22-1638774756.jpg', NULL, NULL, 'collection', NULL, 1638774756, 0, 0),
	(133, '1445395397-shutterstock-1445395397-1-22-1638774759.jpg', NULL, 22, 'collection', NULL, 1638774784, 0, 0),
	(134, '1445395397-shutterstock-1445395397-1-21-1638774865.jpg', NULL, 23, 'collection', NULL, 1641982343, 0, 0),
	(135, '1445395397-shutterstock-1445395397-1-20-1638774867.jpg', NULL, NULL, 'collection', NULL, 1638774867, 0, 0),
	(136, 'photo-2022-01-12-12-30-19-1641976429.jpg', NULL, NULL, 'collection', NULL, 1641976429, 0, 0),
	(137, 'banner-home-1641982846.png', NULL, NULL, 'collection', NULL, 1641982847, 0, 0),
	(138, 'blog-img-1642045078.png', NULL, NULL, 'collection', NULL, 1642045079, 0, 0),
	(139, 'collection-img-1642669003.png', NULL, NULL, 'collection', NULL, 1642669004, 0, 0),
	(140, 'img-room-area-1642669102.png', NULL, NULL, 'collection', NULL, 1642669102, 0, 0),
	(141, 'img-value-1642669148.png', NULL, NULL, 'collection', NULL, 1642669148, 0, 0),
	(142, 'img-room-area-1642669169.png', NULL, NULL, 'collection', NULL, 1642669170, 0, 0),
	(143, 'rectangle-13373-1642757249.jpg', NULL, NULL, 'collection', NULL, 1642757249, 0, 0),
	(144, 'rectangle-13373-1-1643005349.jpg', NULL, NULL, 'collection', NULL, 1643005349, 0, 0),
	(145, 'choose-icon-3-1643005421.png', NULL, NULL, 'collection', NULL, 1643005421, 0, 0),
	(146, 'rectangle-13370-1643005436.jpg', NULL, NULL, 'collection', NULL, 1643005436, 0, 0),
	(147, 'rectangle-13373-2-1643005776.jpg', NULL, NULL, 'collection', NULL, 1643005776, 0, 0),
	(148, 'application-img-1643082865.png', NULL, NULL, 'collection', NULL, 1643082865, 0, 0),
	(149, 'rectangle-13370-1-1643343408.jpg', NULL, NULL, 'collection', NULL, 1643343408, 0, 0),
	(150, 'application-img-1644565046.png', NULL, NULL, 'collection', NULL, 1644565046, 0, 0),
	(151, 'capture-1644565248.PNG', NULL, NULL, 'collection', NULL, 1644565249, 0, 0),
	(152, 'exam-category-1636536603-1644565445.jpg', NULL, NULL, 'collection', NULL, 1644565445, 0, 0),
	(153, 'ptg-1645603996.jpg', NULL, NULL, 'collection', NULL, 1645603996, 0, 0),
	(154, NULL, 'ptg-1645605576.jpg', NULL, 'collection', NULL, 1645605576, 0, 0),
	(155, NULL, 'ptg-1645607514.jpg', NULL, 'collection', NULL, 1645607514, 0, 0),
	(156, NULL, 'ptg-1645607793.jpg', NULL, 'collection', NULL, 1645607793, 0, 0),
	(157, 'address-company-img-1645607813.png', NULL, NULL, 'collection', NULL, 1645607813, 0, 0),
	(158, NULL, 'ptg-1645607885.jpg', NULL, 'collection', NULL, 1645607885, 0, 0),
	(159, NULL, 'ptg-1645608285.jpg', NULL, 'collection', NULL, 1645608285, 0, 0),
	(160, NULL, 'ptg-1645608741.jpg', NULL, 'collection', NULL, 1645608741, 0, 0),
	(161, NULL, 'ptg-1645609088.jpg', NULL, 'collection', NULL, 1645609088, 0, 0),
	(162, NULL, 'ptg-1645609392.jpg', NULL, 'collection', NULL, 1645609392, 0, 0),
	(163, NULL, 'ptg-1645609524.jpg', NULL, 'collection', NULL, 1645609524, 0, 0),
	(164, NULL, 'ptg-1645609623.jpg', NULL, 'collection', NULL, 1645609623, 0, 0),
	(165, NULL, 'img-value-1645609712.png', NULL, 'collection', NULL, 1645609712, 0, 0),
	(166, NULL, 'ptg-1645609849.jpg', NULL, 'collection', NULL, 1645609849, 0, 0),
	(167, NULL, 'ptg-1645610203.jpg', NULL, 'collection', NULL, 1645610203, 0, 0),
	(168, NULL, 'img-value-1645610274.png', NULL, 'collection', NULL, 1645610275, 0, 0),
	(169, NULL, 'ptg-1645611229.jpg', NULL, 'collection', NULL, 1645611229, 0, 0),
	(170, NULL, 'ptg-1645611458.jpg', NULL, 'collection', NULL, 1645611459, 0, 0),
	(171, NULL, 'ptg-1645611637.jpg', NULL, 'collection', NULL, 1645611637, 0, 0),
	(172, NULL, 'ptg-1645611726.jpg', NULL, 'collection', NULL, 1645611727, 0, 0),
	(173, 'img-new1-1647656464.png', NULL, NULL, 'collection', NULL, 1647656464, 0, 0);
/*!40000 ALTER TABLE `collection_img` ENABLE KEYS */;

-- Dumping structure for table centurystone.comment
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `rating` tinyint(1) DEFAULT '1',
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.comment: ~54 rows (approximately)
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` (`id`, `product_id`, `name`, `email`, `message`, `rating`, `status`, `created_at`, `updated_at`) VALUES
	(1, 7182, 'tùng', 'thanhtungvu0397@gmail.com', NULL, 1, 1, '2022-01-19 11:39:53', '2022-01-19 11:39:53'),
	(2, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'hay', 1, 1, '2022-01-19 11:41:01', '2022-01-19 11:41:01'),
	(3, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'hay', 1, 1, '2022-01-19 11:46:45', '2022-01-19 11:46:45'),
	(4, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'sản phẩm xịn', 1, 1, '2022-01-19 11:47:02', '2022-01-19 11:47:02'),
	(5, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'sản phẩm xịn', 1, 1, '2022-01-19 11:47:38', '2022-01-19 11:47:38'),
	(6, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'sản phẩm xịn', 1, 1, '2022-01-19 11:47:59', '2022-01-19 11:47:59'),
	(7, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'sản phẩm rất hữu ích', 1, 1, '2022-01-19 11:50:35', '2022-01-19 11:50:35'),
	(8, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'hay rất hay', 1, 0, '2022-01-19 11:52:38', '2022-01-19 21:37:40'),
	(9, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'hay rất hay', 1, -1, '2022-01-19 11:53:47', '2022-01-19 21:39:39'),
	(10, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'viết dánh giá', 1, 1, '2022-01-19 11:54:39', '2022-01-19 11:54:39'),
	(11, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'danh giá', 1, 1, '2022-01-19 11:55:28', '2022-01-19 11:55:28'),
	(12, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'test', 1, 1, '2022-01-19 11:56:10', '2022-01-19 11:56:10'),
	(13, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'tesst', 1, 1, '2022-01-19 11:56:32', '2022-01-19 11:56:32'),
	(14, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'tesst', 3, 1, '2022-01-19 11:57:05', '2022-01-19 11:57:05'),
	(15, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'tesstst 1313131', 3, 2, '2022-01-19 11:57:30', '2022-01-19 21:44:20'),
	(16, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'tesst', 1, 2, '2022-01-19 12:00:30', '2022-01-19 21:27:06'),
	(17, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'tesst', 1, 2, '2022-01-19 12:04:37', '2022-01-19 12:04:37'),
	(18, 7182, 'tùng', 'thanhtungvu0397111@gmail.com', 'hay', 1, 2, '2022-01-19 14:18:50', '2022-01-19 21:26:54'),
	(19, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'test', 4, -1, '2022-01-19 14:30:19', '2022-01-19 21:40:58'),
	(20, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'ttttata', 5, 1, '2022-01-20 09:45:13', '2022-01-20 09:45:13'),
	(21, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'test', 5, 1, '2022-01-20 09:49:49', '2022-01-20 09:49:49'),
	(22, 7182, 'tccc', 'cc@gmail.com', 'ggg', 4, 1, '2022-01-20 09:50:46', '2022-01-20 09:50:46'),
	(23, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'tttt', 5, 1, '2022-01-20 09:51:17', '2022-01-20 09:51:17'),
	(24, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'ttttt', 5, 1, '2022-01-20 09:52:11', '2022-01-20 09:52:11'),
	(25, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'ty7', 3, 1, '2022-01-20 09:53:15', '2022-01-20 09:53:15'),
	(26, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'ty7', 3, 1, '2022-01-20 09:53:16', '2022-01-20 09:53:16'),
	(27, 7182, 'tccc', 'cc@gmail.com', 'ỳddd', 5, 1, '2022-01-20 09:53:52', '2022-01-20 09:53:52'),
	(28, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'test', 5, 1, '2022-01-20 09:54:07', '2022-01-20 09:54:07'),
	(29, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'test 1131', 3, 1, '2022-01-20 09:55:12', '2022-01-20 09:55:12'),
	(30, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'tét', 5, 1, '2022-01-20 10:05:02', '2022-01-20 10:05:02'),
	(31, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'ttt', 5, 1, '2022-01-20 10:06:38', '2022-01-20 10:06:38'),
	(32, 7182, 'tùng', 'thanhtungvu0397@gmail.com', 'test', 4, 1, '2022-01-20 10:39:16', '2022-01-20 10:39:16'),
	(33, 7185, 'tùng', 'thanhtungvu0397@gmail.com', 'sản phẩm tốt', 4, 2, '2022-01-20 11:52:37', '2022-01-20 11:56:29'),
	(34, 1, 'tùng', 'thanhtungvu0397@gmail.com', 'Mang đến không san sang trọng cho căn phòng bếp\n\nSản phẩm đá Century Stone Thạch anh mang đến không gian sang trọng, tinh tế cho căn phòng bếp, chất lượng sản phẩm tốt, luôn mới như ngày đầu sử dụng.', 4, 2, '2022-01-20 16:10:17', '2022-01-20 16:10:36'),
	(35, 4, 'Bùi Hương Giang', 'giangbui90@gmail.com', 'Rất tuyệt', 5, 2, '2022-01-21 17:41:25', '2022-01-21 17:42:44'),
	(36, 17, 'Bùi Hương Giang', 'giangbuihuong@vccorp.vn', 'Tôi muốn mua hàng', 5, 2, '2022-01-24 16:46:16', '2022-01-24 16:46:29'),
	(37, 17, 'Nguyễn Phúc Khang', 'admin@admin.com', 'Mang đến không san sang trọng cho căn phòng bếp\n\nSản phẩm đá Century Stone Thạch anh mang đến không gian sang trọng, tinh tế cho căn phòng bếp, chất lượng sản phẩm tốt, luôn mới như ngày đầu sử dụng.', 5, 2, '2022-01-24 16:53:24', '2022-01-24 16:53:37'),
	(38, 4, 'Bùi Hương Giang', 'giangbuihuong@vccorp.vn', 'Tốt', 5, -1, '2022-01-25 08:40:18', '2022-01-25 09:43:09'),
	(39, 9, 'Bùi Hương Giang', 'giangbuihuong@vccorp.vn', 'xin chào', 5, 1, '2022-01-25 16:28:59', '2022-01-25 16:28:59'),
	(40, 17, 'Tùng', 'tungvuthanh@vccorp.vn', 'Sản phẩm tốt', 2, 1, '2022-01-25 17:07:20', '2022-01-25 17:07:20'),
	(41, 17, 'Tùng', 'tungvuthanh@vccorp.vn', 'Sản phẩm tốt', 2, -1, '2022-01-25 17:07:21', '2022-01-26 08:12:24'),
	(42, 17, 'Nguyễn Văn Hiếu', 'giang@gmail.com', 'Rất tốt', 5, 2, '2022-01-26 08:11:45', '2022-01-26 08:11:56'),
	(43, 9, 'f', 'giangbuihuong@vccorp.vn', 'Xin chào', 5, 1, '2022-01-26 08:13:27', '2022-01-26 08:13:27'),
	(44, 1, 'Bùi Hương Giang', 'admin@admin.com', 'Chào', 5, 2, '2022-01-26 19:52:40', '2022-02-07 18:46:41'),
	(45, 14, 'Kem Merino', 'kem11@gmail.com', 'Sản phẩm rất đẹp', 5, 2, '2022-01-27 11:10:34', '2022-01-27 11:10:49'),
	(46, 14, 'Hoa', 'a@gmail.com', 'OK', 4, 2, '2022-01-27 13:54:19', '2022-01-27 13:54:51'),
	(47, 14, 'Mỹ', 'b@gmail.com', 'Mang đến không san sang trọng cho căn phòng bếp\n\nSản phẩm đá Century Stone Thạch anh mang đến không gian sang trọng, tinh tế cho căn phòng bếp, chất lượng sản phẩm tốt, luôn mới như ngày đầu sử dụng.', 5, 2, '2022-01-27 13:54:40', '2022-01-27 13:54:48'),
	(48, 14, 'Tester', 'test@gmail.com', 'Mang đến không san sang trọng cho căn phòng bếp\nSản phẩm đá Century Stone Thạch anh mang đến không gian sang trọng, tinh tế cho căn phòng bếp, chất lượng sản phẩm tốt, luôn mới như ngày đầu sử dụng.', 2, 2, '2022-01-27 13:55:39', '2022-01-27 13:55:44'),
	(49, 14, 'Bùi Hương Giang', 'admin@xaytoam.vn', 'Tốt\nRất tốt \nCực kỳ tốt \nTRên cả tuyệt vời', 5, 2, '2022-02-08 10:01:59', '2022-02-08 10:02:16'),
	(50, 20, 'tùng', 'thanhtungvu0397@gmail.com', 'sản phẩm rất ok', 2, 1, '2022-02-09 10:22:18', '2022-02-09 10:22:18'),
	(51, 16, 'Test', 'test@gmail.com', '产品表面\n抛光', 2, 2, '2022-02-09 14:27:33', '2022-02-09 14:28:38'),
	(52, 16, 'Test', 'hoami@gmail.com', '拍打尺寸\n常规尺寸 3280mm x 1650mm (129” x 65”)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）\n巨型尺寸 3550mm x 2050mm (140” x 80”)', 4, 2, '2022-02-09 14:29:14', '2022-02-09 14:29:18'),
	(53, 16, 'kem2', 'kem12@gmail.com', '委内瑞拉的马拉开波湖被称为“雷霆舞蹈的天堂”，\n每年约有300万次雷击，具有奇特的吸引力。', 5, 2, '2022-02-09 14:29:54', '2022-02-09 14:29:59'),
	(54, 16, 'Tester', 'test@gmail.com', 'Hồ Maracaibo ở Venezuela được mệnh danh là\n “thiên đường của những vũ điệu sấm sét” \nvới xấp xỉ 3 triệu cú sét mỗi năm, có sức cuốn hút lạ kỳ.', 1, 1, '2022-02-09 15:03:21', '2022-02-09 15:03:21'),
	(55, 14, 'Tester', 'test@gmail.com', 'Beautiful', 5, 2, '2022-02-09 16:19:59', '2022-02-09 16:20:12'),
	(56, 61, 'kem2', 'kem12@gmail.com', 'Hồ Maracaibo ở Venezuela được mệnh danh là “thiên đường của những vũ điệu sấm sét” \nVới xấp xỉ 3 triệu cú sét mỗi năm, có sức cuốn hút lạ kỳ.', 4, 2, '2022-02-09 17:24:06', '2022-02-09 17:24:19'),
	(57, 61, 'kem2', 'test@gmail.com', 'Hồ Maracaibo ở Venezuela được mệnh danh là “thiên đường của những vũ điệu sấm sét” \nVới xấp xỉ 3 triệu cú sét mỗi năm, có sức cuốn hút lạ kỳ.', 5, 2, '2022-02-09 17:24:47', '2022-02-09 17:24:52'),
	(58, 22, 'Bb', 'giangbui90@gmail.com', 'Bb', 2, -1, '2022-02-10 09:39:26', '2022-02-10 15:56:12'),
	(59, 18, 'tùng', 'thanhtungvu0397@gmail.com', 'sản phẩm rất ok,\n10 điểm', 4, 2, '2022-02-11 14:02:48', '2022-02-11 14:03:08'),
	(60, 61, 'Cửa hàng hoàng yến', 'admin@admin.com', 'tt', 2, 1, '2022-04-01 12:03:02', '2022-04-01 12:03:02'),
	(61, 19, 'aaaa', 'a@gmail.com', 'asdfasdfasdf', 5, 2, '2022-06-20 21:42:20', '2022-06-20 21:42:56');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;

-- Dumping structure for table centurystone.contact
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `contact_email_IDX` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.contact: ~11 rows (approximately)
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` (`id`, `fullname`, `email`, `phone`, `content`, `message`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Phan Trung Tưởng', 'tuong@gmail.com', '0987654321', 'Test nội dung', NULL, 'contact', '2022-02-08 10:48:23', '2022-02-08 10:48:23', NULL),
	(2, 'abc', 'abc@gmail.com', '0987654125', 'test', NULL, 'contact', '2022-02-08 10:48:51', '2022-02-08 10:48:51', NULL),
	(3, 'Bùi Hương Giang', 'giangbui90@gmail.com', '0989915330', 'Xin chào', NULL, 'home', '2022-02-08 11:22:27', '2022-02-08 11:22:27', NULL),
	(4, 'Bùi Hương Giang', 'giangbui90@gmail.com', '0989915330', 'Xin chào', NULL, 'home', '2022-02-08 14:56:21', '2022-02-08 14:56:21', NULL),
	(5, 'Bùi Hương Giang', 'giangbui90@gmail.com', '0989915330', 'Xin chào các bạn', NULL, 'contact', '2022-02-09 11:19:56', '2022-02-09 11:19:56', NULL),
	(6, 'Bùi Hương Giang', 'giangbui90@gmail.com', '0989915330', 'Xin chào các bạn', NULL, 'contact', '2022-02-09 11:30:23', '2022-02-09 11:30:23', NULL),
	(7, 'tet', 'giangbui90@gmail.com', '0989915330', 'egeg', NULL, 'contact', '2022-02-09 15:24:47', '2022-02-09 15:24:47', NULL),
	(8, 'Kemmerino', 'kem11@gmail.com', '0345555554', 'I don\'t know', NULL, 'contact', '2022-02-09 16:18:13', '2022-02-09 16:18:13', NULL),
	(9, 'Bui Giang', 'Giangbui90@gmail.com', '0989915330', 'Xin chào', NULL, 'contact', '2022-02-10 09:34:31', '2022-02-10 09:34:31', NULL),
	(10, 'Bui Giang', 'Giangbui90@gmail.com', '0989915330', '0989915330', NULL, 'home', '2022-02-10 09:36:22', '2022-02-10 09:36:22', NULL),
	(11, 'Bùi Hương Giang', 'giangbui90@gmail.com', '0989915330', 'xin chào', NULL, 'contact', '2022-02-10 09:53:32', '2022-02-10 23:48:14', '2022-02-10 23:48:14'),
	(12, 'tung', 'tung@gmail.com', '0981561298', 'tttt', NULL, 'home', '2022-04-01 14:24:25', '2022-04-01 14:24:25', NULL);
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;

-- Dumping structure for table centurystone.contacts
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cusid` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `content` text,
  `phone` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `created` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.contacts: ~0 rows (approximately)
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;

-- Dumping structure for table centurystone.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `fullname` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password_encode` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL COMMENT '1:Male;0:Female',
  `avatar` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date_of_birth` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT '1: Not Review\\\\n2: Reviewed\\\\n3: Blocked\\\\n-1: Deleted',
  `register_ip` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `last_login_ip` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_active` datetime DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `is_contractor` tinyint(1) DEFAULT '0' COMMENT '0: No, 1: Yes',
  `otp_method` varchar(45) CHARACTER SET utf8 DEFAULT 'email',
  `social_provider` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `social_nickname` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `social_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `social_token` text CHARACTER SET utf8,
  `social_token_secret` text CHARACTER SET utf8,
  `social_refresh_token` text CHARACTER SET utf8,
  `social_expires_in` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `social_avatar` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `social_avatar_original` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `customers_email_IDX` (`email`) USING BTREE,
  KEY `customers_active_IDX` (`is_active`) USING BTREE,
  KEY `cusstomer_phone_IDX` (`phone`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED;

-- Dumping data for table centurystone.customer: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;

-- Dumping structure for table centurystone.customer_address_book
CREATE TABLE IF NOT EXISTS `customer_address_book` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT '0',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `province_id` int(11) unsigned DEFAULT '0',
  `district_id` int(11) unsigned DEFAULT '0',
  `ward_id` int(11) unsigned DEFAULT '0',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT '2',
  `is_default` tinyint(1) unsigned DEFAULT '0',
  `is_on_working_time` tinyint(1) unsigned DEFAULT '1' COMMENT '1; Trong giờ hành chính; 2; ngoài giờ hành chính',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.customer_address_book: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_address_book` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_address_book` ENABLE KEYS */;

-- Dumping structure for table centurystone.customer_group
CREATE TABLE IF NOT EXISTS `customer_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `percent` int(11) DEFAULT NULL,
  `bonus` int(11) DEFAULT NULL,
  `other` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `reward` int(11) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `color` varchar(10) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `show` tinyint(1) DEFAULT NULL,
  `emails` varchar(1024) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `min_order_value` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `customer_group_status_IDX` (`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.customer_group: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_group` ENABLE KEYS */;

-- Dumping structure for table centurystone.customer_group_connect
CREATE TABLE IF NOT EXISTS `customer_group_connect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `customer_group_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `customer_group_connect_customer_id_IDX` (`customer_id`,`customer_group_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.customer_group_connect: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_group_connect` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_group_connect` ENABLE KEYS */;

-- Dumping structure for table centurystone.customer_group_detail
CREATE TABLE IF NOT EXISTS `customer_group_detail` (
  `id` int(11) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `percent` int(11) DEFAULT NULL,
  `bonus` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `customer_group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.customer_group_detail: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_group_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_group_detail` ENABLE KEYS */;

-- Dumping structure for table centurystone.customer_search
CREATE TABLE IF NOT EXISTS `customer_search` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `key_word` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `comments_customer_id_IDX` (`customer_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.customer_search: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_search` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_search` ENABLE KEYS */;

-- Dumping structure for table centurystone.customer_tokens
CREATE TABLE IF NOT EXISTS `customer_tokens` (
  `email` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.customer_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_tokens` ENABLE KEYS */;

-- Dumping structure for table centurystone.customer_wish_list
CREATE TABLE IF NOT EXISTS `customer_wish_list` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `type` enum('buy_later','reaction','favorite') COLLATE utf8mb4_unicode_ci DEFAULT 'reaction',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`customer_id`,`product_id`) USING BTREE,
  KEY `product_ud_foreign_key` (`product_id`) USING BTREE,
  CONSTRAINT `customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  CONSTRAINT `product_ud_foreign_key` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.customer_wish_list: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_wish_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_wish_list` ENABLE KEYS */;

-- Dumping structure for table centurystone.delivery_type
CREATE TABLE IF NOT EXISTS `delivery_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `shipping_fee` double(15,0) DEFAULT '0',
  `min_order_free_ship` double(15,0) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.delivery_type: ~0 rows (approximately)
/*!40000 ALTER TABLE `delivery_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `delivery_type` ENABLE KEYS */;

-- Dumping structure for table centurystone.delivery_type_description
CREATE TABLE IF NOT EXISTS `delivery_type_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_type_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `link` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.delivery_type_description: ~0 rows (approximately)
/*!40000 ALTER TABLE `delivery_type_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `delivery_type_description` ENABLE KEYS */;

-- Dumping structure for table centurystone.discounts
CREATE TABLE IF NOT EXISTS `discounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.discounts: ~0 rows (approximately)
/*!40000 ALTER TABLE `discounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `discounts` ENABLE KEYS */;

-- Dumping structure for table centurystone.discount_code
CREATE TABLE IF NOT EXISTS `discount_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'percent: Giảm %; amount: Giảm số tiền cụ thể;\rfreeship: Miễn phí giao hàng',
  `discount_type` enum('freeship','percent','amount') CHARACTER SET utf8 DEFAULT 'amount',
  `discount_value` float DEFAULT NULL,
  `is_use_once` tinyint(1) DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `status` int(1) DEFAULT '1' COMMENT '1: Đang hoạt động; 2: Tạm ngưng',
  `lang` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `quantity` int(11) DEFAULT '0',
  `object_id` varchar(255) CHARACTER SET latin1 DEFAULT '' COMMENT 'Là id tùy loại áp dụng của coupon',
  `object_use` varchar(20) CHARACTER SET latin1 DEFAULT NULL COMMENT '*: Bất kỳ ai; groups: Nhóm khách hàng cụ thể; personal: Khách hàng cụ thể',
  `product_use` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '*: Tất cả sản phẩm; collection: Bộ sưu tập sản phẩm; single_product: Chỉ ra Sản phẩm cụ thể',
  `ship_rate` int(11) DEFAULT '0',
  `extra_rule` enum('none','order_minimun','quantity_minimum') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'none',
  `extra_rule_value` int(11) DEFAULT NULL,
  `quanhuyen` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `hasShipRate` tinyint(4) DEFAULT NULL,
  `use_success` int(11) DEFAULT '0',
  `end_date` timestamp NULL DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `code` (`code`) USING BTREE,
  KEY `discount_code_user_id_IDX` (`user_id`) USING BTREE,
  KEY `discount_code_use_success_IDX` (`use_success`) USING BTREE,
  KEY `discount_code_deleted_at_IDX` (`deleted_at`) USING BTREE,
  KEY `discount_code_discount_value_IDX` (`discount_value`,`discount_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.discount_code: ~0 rows (approximately)
/*!40000 ALTER TABLE `discount_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount_code` ENABLE KEYS */;

-- Dumping structure for table centurystone.discount_code_collection
CREATE TABLE IF NOT EXISTS `discount_code_collection` (
  `discount_code_id` int(11) NOT NULL,
  `product_collection_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`product_collection_id`,`discount_code_id`) USING BTREE,
  KEY `discount_code_collection_product_collection_id_IDX` (`product_collection_id`,`discount_code_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.discount_code_collection: ~0 rows (approximately)
/*!40000 ALTER TABLE `discount_code_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount_code_collection` ENABLE KEYS */;

-- Dumping structure for table centurystone.discount_code_customer
CREATE TABLE IF NOT EXISTS `discount_code_customer` (
  `discount_code_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `type` enum('onsave','assign') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`customer_id`,`discount_code_id`) USING BTREE,
  KEY `discount_code_customer_customer_id_IDX` (`customer_id`,`discount_code_id`) USING BTREE,
  KEY `discount_code_customer_type_IDX` (`type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.discount_code_customer: ~0 rows (approximately)
/*!40000 ALTER TABLE `discount_code_customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount_code_customer` ENABLE KEYS */;

-- Dumping structure for table centurystone.discount_code_customer_groups
CREATE TABLE IF NOT EXISTS `discount_code_customer_groups` (
  `customer_group_id` int(11) NOT NULL,
  `discount_code_id` int(11) NOT NULL,
  PRIMARY KEY (`customer_group_id`,`discount_code_id`) USING BTREE,
  KEY `discount_code_customer_groups_customer_group_id_IDX` (`customer_group_id`,`discount_code_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.discount_code_customer_groups: ~0 rows (approximately)
/*!40000 ALTER TABLE `discount_code_customer_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount_code_customer_groups` ENABLE KEYS */;

-- Dumping structure for table centurystone.discount_code_description
CREATE TABLE IF NOT EXISTS `discount_code_description` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `discount_code_id` int(10) unsigned DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.discount_code_description: ~0 rows (approximately)
/*!40000 ALTER TABLE `discount_code_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount_code_description` ENABLE KEYS */;

-- Dumping structure for table centurystone.discount_code_district
CREATE TABLE IF NOT EXISTS `discount_code_district` (
  `discount_code_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`district_id`,`discount_code_id`) USING BTREE,
  KEY `discount_code_district_district_id_IDX` (`district_id`,`discount_code_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.discount_code_district: ~0 rows (approximately)
/*!40000 ALTER TABLE `discount_code_district` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount_code_district` ENABLE KEYS */;

-- Dumping structure for table centurystone.discount_code_product
CREATE TABLE IF NOT EXISTS `discount_code_product` (
  `discount_code_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`product_id`,`discount_code_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.discount_code_product: ~0 rows (approximately)
/*!40000 ALTER TABLE `discount_code_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount_code_product` ENABLE KEYS */;

-- Dumping structure for table centurystone.discount_code_use_log
CREATE TABLE IF NOT EXISTS `discount_code_use_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `discount_code_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `discount_code_use_log_FK` (`discount_code_id`) USING BTREE,
  CONSTRAINT `discount_code_use_log_FK` FOREIGN KEY (`discount_code_id`) REFERENCES `discount_code` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.discount_code_use_log: ~0 rows (approximately)
/*!40000 ALTER TABLE `discount_code_use_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount_code_use_log` ENABLE KEYS */;

-- Dumping structure for table centurystone.eshopbizflies
CREATE TABLE IF NOT EXISTS `eshopbizflies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.eshopbizflies: ~0 rows (approximately)
/*!40000 ALTER TABLE `eshopbizflies` DISABLE KEYS */;
/*!40000 ALTER TABLE `eshopbizflies` ENABLE KEYS */;

-- Dumping structure for table centurystone.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table centurystone.feature
CREATE TABLE IF NOT EXISTS `feature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `positions` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `published` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.feature: ~0 rows (approximately)
/*!40000 ALTER TABLE `feature` DISABLE KEYS */;
/*!40000 ALTER TABLE `feature` ENABLE KEYS */;

-- Dumping structure for table centurystone.feature_description
CREATE TABLE IF NOT EXISTS `feature_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.feature_description: ~0 rows (approximately)
/*!40000 ALTER TABLE `feature_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `feature_description` ENABLE KEYS */;

-- Dumping structure for table centurystone.follow
CREATE TABLE IF NOT EXISTS `follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT '1' COMMENT '1: owner\n2: contractor',
  `customer_id` int(11) DEFAULT NULL COMMENT 'Người được theo dõi',
  `follower_id` int(11) DEFAULT NULL COMMENT 'Người theo dõi',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT COMMENT='	';

-- Dumping data for table centurystone.follow: ~0 rows (approximately)
/*!40000 ALTER TABLE `follow` DISABLE KEYS */;
/*!40000 ALTER TABLE `follow` ENABLE KEYS */;

-- Dumping structure for table centurystone.galleries
CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.galleries: ~0 rows (approximately)
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;

-- Dumping structure for table centurystone.gallery
CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
  `title` varchar(300) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `changed` int(11) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `uname` varchar(60) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `is_cover` tinyint(1) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `lang` varchar(5) DEFAULT NULL,
  `pid` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `gallery_cat_id_IDX` (`cat_id`) USING BTREE,
  KEY `gallery_uid_IDX` (`uid`) USING BTREE,
  KEY `gallery_type_IDX` (`type`) USING BTREE,
  KEY `gallery_pid_IDX` (`pid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.gallery: ~0 rows (approximately)
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;

-- Dumping structure for table centurystone.gallery_cats
CREATE TABLE IF NOT EXISTS `gallery_cats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `total` int(5) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `uname` varchar(60) DEFAULT NULL,
  `safe_title` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.gallery_cats: ~0 rows (approximately)
/*!40000 ALTER TABLE `gallery_cats` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery_cats` ENABLE KEYS */;

-- Dumping structure for table centurystone.geos
CREATE TABLE IF NOT EXISTS `geos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.geos: ~0 rows (approximately)
/*!40000 ALTER TABLE `geos` DISABLE KEYS */;
/*!40000 ALTER TABLE `geos` ENABLE KEYS */;

-- Dumping structure for table centurystone.image
CREATE TABLE IF NOT EXISTS `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.image: ~0 rows (approximately)
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
/*!40000 ALTER TABLE `image` ENABLE KEYS */;

-- Dumping structure for table centurystone.inventories
CREATE TABLE IF NOT EXISTS `inventories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.inventories: ~0 rows (approximately)
/*!40000 ALTER TABLE `inventories` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventories` ENABLE KEYS */;

-- Dumping structure for table centurystone.inventory
CREATE TABLE IF NOT EXISTS `inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `display_name` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact_info` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `inventory_created_by_IDX` (`created_by`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.inventory: ~0 rows (approximately)
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;

-- Dumping structure for table centurystone.inventory_transaction
CREATE TABLE IF NOT EXISTS `inventory_transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `product_option_value_ids` varchar(50) DEFAULT NULL,
  `type` varchar(1) DEFAULT '1' COMMENT '1: Nhập | 2: Xuất',
  `quantity` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `inventory_transaction_product_id_IDX` (`product_id`,`product_option_value_ids`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.inventory_transaction: ~0 rows (approximately)
/*!40000 ALTER TABLE `inventory_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_transaction` ENABLE KEYS */;

-- Dumping structure for table centurystone.jobs
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `jobs_queue_index` (`queue`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for function centurystone.json_extract_c
DELIMITER //
CREATE FUNCTION `json_extract_c`(details TEXT,
  required_field VARCHAR (255)) RETURNS text CHARSET latin1
BEGIN
  /* get key from function passed required field value */
  set @JSON_key = SUBSTRING_INDEX(required_field,'$.', -1); 
  /* get everything to the right of the 'key = <required_field>' */
  set @JSON_entry = SUBSTRING_INDEX(details,CONCAT('"', @JSON_key, '"'), -1 ); 
  /* get everything to the left of the trailing comma */
  set @JSON_entry_no_trailing_comma = SUBSTRING_INDEX(@JSON_entry, ",", 1); 
  /* get everything to the right of the leading colon after trimming trailing and leading whitespace */
  set @JSON_entry_no_leading_colon = TRIM(LEADING ':' FROM TRIM(@JSON_entry_no_trailing_comma)); 
  /* trim off the leading and trailing double quotes after trimming trailing and leading whitespace*/
  set @JSON_extracted_entry = TRIM(BOTH '"' FROM TRIM(@JSON_entry_no_leading_colon));
  RETURN @JSON_extracted_entry;
END//
DELIMITER ;

-- Dumping structure for table centurystone.language
CREATE TABLE IF NOT EXISTS `language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `short_code` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `code` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `locale` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `image` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `directory` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `sort_order` int(3) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`language_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.language: ~3 rows (approximately)
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` (`language_id`, `name`, `short_code`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`, `is_default`) VALUES
	(1, 'Tiếng Việt', 'vi', 'vi-vn', 'vi-VN', 'vn.png', 'vietnamese', 1, 1, 1),
	(2, 'Tiếng Anh', 'en', 'en', 'en', 'en.png', 'english', 2, 1, 0),
	(3, 'Tiếng Trung', 'zh', 'zh', 'zh_CN ', 'cn.png', 'chinese', 3, 1, 0);
/*!40000 ALTER TABLE `language` ENABLE KEYS */;

-- Dumping structure for table centurystone.locations
CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.locations: ~0 rows (approximately)
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;

-- Dumping structure for table centurystone.ltm_translations
CREATE TABLE IF NOT EXISTS `ltm_translations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '0',
  `locale` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `group` varchar(191) COLLATE utf8mb4_bin NOT NULL,
  `key` text COLLATE utf8mb4_bin NOT NULL,
  `value` text COLLATE utf8mb4_bin,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=497 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.ltm_translations: ~466 rows (approximately)
/*!40000 ALTER TABLE `ltm_translations` DISABLE KEYS */;
INSERT INTO `ltm_translations` (`id`, `status`, `locale`, `group`, `key`, `value`, `created_at`, `updated_at`) VALUES
	(1, 0, 'vi', 'site', 'sanphamnoibat', 'Sản phẩm nổi bật', '2022-01-17 15:17:20', '2022-04-01 10:55:02'),
	(2, 0, 'en', 'site', 'sanphamnoibat', 'Featured products', '2022-01-17 15:17:33', '2022-04-01 10:55:02'),
	(3, 0, 'vi', 'site', 'lienhengay', 'Liên hệ ngay', '2022-01-17 15:25:11', '2022-04-01 10:55:02'),
	(4, 0, 'en', 'site', 'lienhengay', 'Contact now', '2022-01-17 15:25:23', '2022-04-01 10:55:02'),
	(5, 0, 'vi', 'site', 'moi', 'Mới', '2022-01-17 15:25:52', '2022-04-01 10:55:02'),
	(6, 0, 'en', 'site', 'moi', 'New', '2022-01-17 15:26:04', '2022-04-01 10:55:02'),
	(7, 0, 'vi', 'site', 'timhieuthem', 'Tìm hiểu thêm', '2022-01-17 15:28:26', '2022-04-01 10:55:02'),
	(8, 0, 'en', 'site', 'timhieuthem', 'More information', '2022-01-17 15:28:52', '2022-04-01 10:55:02'),
	(9, 0, 'vi', 'site', 'khamphathem', 'Khám phá thêm', '2022-01-17 15:29:27', '2022-04-01 10:55:02'),
	(10, 0, 'en', 'site', 'khamphathem', 'Discover more', '2022-01-17 15:29:41', '2022-04-01 10:55:02'),
	(11, 0, 'vi', 'site', 'bosuutap', 'Bộ sưu tập', '2022-01-17 15:30:40', '2022-04-01 10:55:02'),
	(12, 0, 'en', 'site', 'bosuutap', 'Collection', '2022-01-17 15:31:00', '2022-04-01 10:55:02'),
	(13, 0, 'vi', 'site', 'xembosuutap', 'Xem bộ sưu tập', '2022-01-17 15:31:13', '2022-04-01 10:55:02'),
	(14, 0, 'en', 'site', 'xembosuutap', 'View Collection', '2022-01-17 15:31:36', '2022-04-01 10:55:02'),
	(15, 0, 'vi', 'site', 'lienhevoichungtoideduochotrototnhat', 'Liên hệ với chúng tôi để được hỗ trợ tốt nhất', '2022-01-18 10:03:53', '2022-04-01 10:55:02'),
	(16, 0, 'en', 'site', 'lienhevoichungtoideduochotrototnhat', 'Contact us for the best support', '2022-01-18 10:04:14', '2022-04-01 10:55:02'),
	(17, 0, 'vi', 'site', 'dangky', 'Đăng ký', '2022-01-18 10:32:31', '2022-04-01 10:55:02'),
	(18, 0, 'en', 'site', 'dangky', 'Registration', '2022-01-18 10:32:56', '2022-04-01 10:55:02'),
	(19, 0, 'vi', 'site', 'nhantuvan', 'nhận tư vấn', '2022-01-18 10:33:52', '2022-04-01 10:55:02'),
	(20, 0, 'en', 'site', 'nhantuvan', 'get advice', '2022-01-18 10:34:10', '2022-04-01 10:55:02'),
	(21, 0, 'vi', 'site', 'delaithongtinchitietsanpham', 'Để nhận thông tin chi tiết của sản phẩm, quý khách vui lòng để lại thông tin tại đây.', '2022-01-18 10:35:12', '2022-04-01 10:55:02'),
	(22, 0, 'en', 'site', 'delaithongtinchitietsanpham', 'To receive detailed product information, please leave information here.', '2022-01-18 10:35:26', '2022-04-01 10:55:02'),
	(23, 0, 'vi', 'site', 'xinchanthanhcamon', 'Xin chân thành cảm ơn.', '2022-01-18 10:38:37', '2022-04-01 10:55:02'),
	(24, 0, 'en', 'site', 'xinchanthanhcamon', 'Sincerely thank.', '2022-01-18 10:38:54', '2022-04-01 10:55:02'),
	(25, 0, 'vi', 'site', 'tucenturystone', 'Từ century stone', '2022-01-18 10:40:15', '2022-04-01 10:55:02'),
	(26, 0, 'en', 'site', 'tucenturystone', 'From century stone', '2022-01-18 10:40:33', '2022-04-01 10:55:02'),
	(27, 0, 'vi', 'site', 'chungtoiselienhevoiban', 'Chúng tôi sẽ liên hệ với bạn', '2022-01-18 10:41:07', '2022-04-01 10:55:02'),
	(28, 0, 'en', 'site', 'chungtoiselienhevoiban', 'We will contact you', '2022-01-18 10:41:22', '2022-04-01 10:55:02'),
	(29, 0, 'vi', 'site', 'hovaten', 'Họ và tên', '2022-01-18 10:41:45', '2022-04-01 10:55:02'),
	(30, 0, 'en', 'site', 'hovaten', 'Full name', '2022-01-18 10:41:55', '2022-04-01 10:55:02'),
	(31, 0, 'vi', 'site', 'sodienthoai', 'Số điện thoại', '2022-01-18 10:42:27', '2022-04-01 10:55:02'),
	(32, 0, 'en', 'site', 'sodienthoai', 'Phone Number', '2022-01-18 10:42:48', '2022-04-01 10:55:02'),
	(33, 0, 'vi', 'site', 'nhapnoidung', 'Nhập nội dung', '2022-01-18 10:43:19', '2022-04-01 10:55:02'),
	(34, 0, 'en', 'site', 'nhapnoidung', 'Import content', '2022-01-18 10:43:40', '2022-04-01 10:55:02'),
	(35, 0, 'vi', 'site', 'tuvanchotoi', 'TƯ VẤN CHO TÔI', '2022-01-18 10:44:06', '2022-04-01 10:55:02'),
	(36, 0, 'en', 'site', 'tuvanchotoi', 'ADVISE ME', '2022-01-18 10:44:19', '2022-04-01 10:55:02'),
	(37, 0, 'vi', 'site', 'hotenkhongduocdetrong', 'Họ và tên không được để trống', '2022-01-18 11:06:50', '2022-04-01 10:55:02'),
	(38, 0, 'en', 'site', 'hotenkhongduocdetrong', 'Full name is required', '2022-01-18 11:07:54', '2022-04-01 10:55:02'),
	(39, 0, 'vi', 'site', 'emailkhongduocdetrong', 'Email không được để trống', '2022-01-18 11:08:25', '2022-04-01 10:55:02'),
	(40, 0, 'en', 'site', 'emailkhongduocdetrong', 'Email is required', '2022-01-18 11:08:58', '2022-04-01 10:55:02'),
	(41, 0, 'vi', 'site', 'emailkhongdungdinhdang', 'Email không đúng định dạng', '2022-01-18 11:09:19', '2022-04-01 10:55:02'),
	(42, 0, 'en', 'site', 'emailkhongdungdinhdang', 'Email invalidate', '2022-01-18 11:09:44', '2022-04-01 10:55:02'),
	(43, 0, 'vi', 'site', 'sodienthoaikhongduocdetrong', 'Số điện thoại không được để trống', '2022-01-18 11:09:55', '2022-04-01 10:55:02'),
	(44, 0, 'en', 'site', 'sodienthoaikhongduocdetrong', 'Phone number is required', '2022-01-18 11:10:18', '2022-04-01 10:55:02'),
	(45, 0, 'vi', 'site', 'sodienthoaikhonghople', 'Số điện thoai không hợp lệ', '2022-01-18 11:10:31', '2022-04-01 10:55:02'),
	(46, 0, 'en', 'site', 'sodienthoaikhonghople', 'Invalid phone number', '2022-01-18 11:11:11', '2022-04-01 10:55:02'),
	(47, 0, 'vi', 'site', 'sodienthoaikhongdungdinhdang', 'Số điện thoại không đúng định dạng', '2022-01-18 11:11:18', '2022-04-01 10:55:02'),
	(48, 0, 'en', 'site', 'sodienthoaikhongdungdinhdang', 'Phone number invalidate', '2022-01-18 11:11:47', '2022-04-01 10:55:02'),
	(49, 0, 'vi', 'site', 'noidungkhongduocdetrong', 'Nội dung không được để trống', '2022-01-18 11:12:17', '2022-04-01 10:55:02'),
	(50, 0, 'en', 'site', 'noidungkhongduocdetrong', 'Content is required', '2022-01-18 11:12:38', '2022-04-01 10:55:02'),
	(51, 0, 'vi', 'site', 'thanhcong', 'Thành công', '2022-01-18 12:06:07', '2022-04-01 10:55:02'),
	(52, 0, 'en', 'site', 'thanhcong', 'Success', '2022-01-18 12:06:22', '2022-04-01 10:55:02'),
	(53, 0, 'vi', 'site', 'thongtincuabandaduocguitoiadmin', 'Thông tin của bạn đã được gửi tới Admin', '2022-01-18 12:06:31', '2022-04-01 10:55:02'),
	(54, 0, 'en', 'site', 'thongtincuabandaduocguitoiadmin', 'Your information has been sent to Admin', '2022-01-18 12:07:01', '2022-04-01 10:55:02'),
	(55, 0, 'vi', 'site', 'thatbai', 'Thất bại', '2022-01-18 12:07:35', '2022-04-01 10:55:02'),
	(56, 0, 'en', 'site', 'thatbai', 'Faild', '2022-01-18 13:32:26', '2022-04-01 10:55:02'),
	(57, 0, 'vi', 'site', 'dacoloixayravuilongthulai', 'Đã có lỗi xảy ra. Vui lòng thử lại.', '2022-01-18 13:32:52', '2022-04-01 10:55:02'),
	(58, 0, 'en', 'site', 'dacoloixayravuilongthulai', 'An error has occurred. Please try again.', '2022-01-18 13:33:40', '2022-04-01 10:55:02'),
	(59, 0, 'vi', 'site', 'diachinhamay', 'Địa chỉ nhà máy', '2022-01-18 13:42:46', '2022-04-01 10:55:02'),
	(60, 0, 'en', 'site', 'diachinhamay', 'Factory address', '2022-01-18 13:43:03', '2022-04-01 10:55:02'),
	(61, 0, 'vi', 'site', 'chiduong', 'Chỉ đường', '2022-01-18 13:43:27', '2022-04-01 10:55:02'),
	(62, 0, 'en', 'site', 'chiduong', 'Direct', '2022-01-18 13:43:39', '2022-04-01 10:55:02'),
	(63, 0, 'vi', 'site', 'dangkydenhantintucmoinhat', 'Đăng ký theo dõi để nhận tin tức mới nhất từ Century Stone', '2022-01-18 14:15:31', '2022-04-01 10:55:02'),
	(64, 0, 'en', 'site', 'dangkydenhantintucmoinhat', 'Subscribe to receive the latest news from Century Stone', '2022-01-18 14:15:56', '2022-04-01 10:55:02'),
	(67, 0, 'vi', 'site', 'dangnhap', 'Đăng nhập', '2022-01-18 14:17:10', '2022-04-01 10:55:02'),
	(68, 0, 'en', 'site', 'dangnhap', 'Login', '2022-01-18 14:17:23', '2022-04-01 10:55:02'),
	(69, 0, 'vi', 'pagination', 'previous', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(70, 0, 'vi', 'pagination', 'next', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(71, 0, 'vi', 'validation', 'profile.name', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(72, 0, 'vi', 'validation', 'profile.phone', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(73, 0, 'vi', 'validation', 'profile.confirm_password', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(74, 0, 'vi', 'validation', 'profile.password1', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(75, 0, 'vi', 'validation', 'profile.image', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(76, 0, 'vi', 'site', 'name_required', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(77, 0, 'vi', 'site', 'name_min', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(78, 0, 'vi', 'site', 'name_max', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(79, 0, 'vi', 'site', 'email_required', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(80, 0, 'vi', 'site', 'email_email', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(81, 0, 'vi', 'site', 'email_unique', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(82, 0, 'vi', 'site', 'phone_required', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(83, 0, 'vi', 'site', 'phone_regex', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(84, 0, 'vi', 'site', 'phone_unique', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(85, 0, 'vi', 'site', 'message_required', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(86, 0, 'vi', 'site', 'message_max', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(87, 0, 'vi', 'site', 'guithongtinquantamsanpham', NULL, '2022-01-18 14:18:23', '2022-01-18 14:18:23'),
	(88, 0, 'vi', 'site', 'chonphuongthucthanhtoan', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(89, 0, 'vi', 'site', 'chonphuongthucvanchuyen', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(90, 0, 'vi', 'site', 'haynhapdiachi', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(91, 0, 'vi', 'site', 'hientaikhongcosanphamnaotronggio', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(92, 0, 'vi', 'site', 'nhaphotencuaban', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(93, 0, 'vi', 'site', 'haynhapsodienthoai', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(94, 0, 'vi', 'site', 'chontinhthanh', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(95, 0, 'vi', 'site', 'chonquanhuyen', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(96, 0, 'vi', 'site', 'chonxaphuong', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(97, 0, 'vi', 'site', 'chonthoidiemgiao', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(98, 0, 'vi', 'site', 'camonbandadangky', 'Cảm ơn bạn đã đăng ký. Thông tin của bạn đã đươc gửi tới Admin', '2022-01-18 14:18:24', '2022-04-01 10:55:02'),
	(99, 0, 'vi', 'site', 'timhieungay', 'Tìm hiểu ngay', '2022-01-18 14:18:24', '2022-04-01 10:55:02'),
	(100, 0, 'vi', 'site', 'ti', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(101, 0, 'vi', 'site', 'email', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(102, 0, 'vi', 'site', 'noidung', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(103, 0, 'vi', 'site', 'nhanthongtin', 'Để nhận thông tin chi tiết của sản phẩm, quý khách vui lòng để lại thông tin tại đây.', '2022-01-18 14:18:24', '2022-04-01 10:55:02'),
	(104, 0, 'vi', 'site', 'theodoichungtoi', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(105, 0, 'vi', 'site', 'tiengviet', 'Tiếng việt', '2022-01-18 14:18:24', '2022-04-01 10:55:02'),
	(106, 0, 'vi', 'site', 'tienganh', 'Tiếng anh', '2022-01-18 14:18:24', '2022-04-01 10:55:02'),
	(107, 0, 'vi', 'site', 'nhapemaildangky', 'Nhập email đăng ký', '2022-01-18 14:18:24', '2022-04-01 10:55:02'),
	(108, 0, 'vi', 'site', 'ketnoivoichungtoi', 'Kết nối với chúng tôi!', '2022-01-18 14:18:24', '2022-04-01 10:55:02'),
	(109, 0, 'vi', 'site', 'trangchu', 'Trang chủ', '2022-01-18 14:18:24', '2022-04-01 10:55:02'),
	(110, 0, 'vi', 'site', 'khongdusoluonghang', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(111, 0, 'vi', 'site', 'khongtontaisanpham', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(112, 0, 'vi', 'site', 'service', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(113, 0, 'vi', 'site', 'danhgia', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(114, 0, 'vi', 'site', 'thuonghieu', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(115, 0, 'vi', 'site', 'moicapnhat', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(116, 0, 'vi', 'site', 'giathapdencao', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(117, 0, 'vi', 'site', 'giacaodenthap', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(118, 0, 'vi', '_json', 'Đánh dấu đã đọc thông báo thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(119, 0, 'vi', '_json', 'Từ ngày', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(120, 0, 'vi', '_json', 'Đến ngày', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(121, 0, 'vi', '_json', 'Liên hệ', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(122, 0, 'vi', '_json', 'Xóa bản ghi liên hệ', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(123, 0, 'vi', '_json', 'Đánh giá gần đây', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(124, 0, 'vi', '_json', 'Đã duyệt', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(125, 0, 'vi', '_json', 'Đã xóa', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(126, 0, 'vi', '_json', 'Cảm ơn bạn đã để lại bình luận', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(127, 0, 'vi', '_json', 'Sửa từ cấm', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(128, 0, 'vi', '_json', 'Tạo từ cấm', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(129, 0, 'vi', '_json', 'Không có dữ liệu', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(130, 0, 'vi', '_json', 'Quản lý bình luận', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(131, 0, 'vi', '_json', 'Xóa bình luận thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(132, 0, 'vi', '_json', 'Từ cấm', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(133, 0, 'vi', '_json', 'Xóa từ cấm thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(134, 0, 'vi', '_json', 'Hệ thống đã nhận được yêu cầu và sẽ gửi bạn lại kết quả của hành động này qua hệ thống thông báo', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(135, 0, 'vi', '_json', 'Approve Comment Successfully', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(136, 0, 'vi', '_json', 'Duyệt bình luận thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(137, 0, 'vi', '_json', 'Trả lời bình luận thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(138, 0, 'vi', '_json', 'Đánh dấu là đã xử lý bình luận than phiền', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(139, 0, 'vi', '_json', 'Xóa bình luận', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(140, 0, 'vi', '_json', 'Xoá nhãn thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(141, 0, 'vi', '_json', 'Xóa tài liệu thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(142, 0, 'vi', '_json', 'Quản trị đơn hàng', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(143, 0, 'vi', '_json', 'Tiếp nhận thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(144, 0, 'vi', '_json', 'Đơn hàng đã được xuất kho', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(145, 0, 'vi', '_json', 'Đơn hàng đang được giao', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(146, 0, 'vi', '_json', 'Đơn hàng đã được giao', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(147, 0, 'vi', '_json', 'Bỏ tiếp nhận thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(148, 0, 'vi', '_json', 'Đánh dấu là đơn hàng chờ thanh toán thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(149, 0, 'vi', '_json', 'Đánh dấu là đơn hàng đã thanh toán thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(150, 0, 'vi', '_json', 'Đánh dấu là đơn hàng hoàn thành thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(151, 0, 'vi', '_json', 'Đánh dấu là đơn hàng hoàn tiền thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(152, 0, 'vi', '_json', 'Hủy đơn thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(153, 0, 'vi', '_json', 'Gửi thông tin đơn hàng cho khách qua email thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(154, 0, 'vi', '_json', 'Xóa nhóm khách hàng thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(155, 0, 'vi', '_json', 'Xóa khách hàng thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(156, 0, 'vi', '_json', 'Theo dõi thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(157, 0, 'vi', '_json', 'Hủy theo dõi thành công', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(158, 0, 'vi', '_json', 'Xóa bản ghi đăng ký', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(159, 0, 'vi', '_json', 'Thông tin chung', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(160, 0, 'vi', '_json', 'Chi tiết', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(161, 0, 'vi', '_json', 'Hình ảnh', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(162, 0, 'vi', '_json', 'Xóa', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(163, 0, 'vi', '_json', 'Tìm kiếm', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(164, 0, 'vi', '_json', 'Reset', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(165, 0, 'vi', '_json', 'Thêm mới', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(166, 0, 'vi', '_json', 'Danh sách', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(167, 0, 'vi', '_json', 'ID', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(168, 0, 'vi', '_json', 'Tiêu đề', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(169, 0, 'vi', '_json', 'Mô tả', NULL, '2022-01-18 14:18:24', '2022-01-18 14:18:24'),
	(170, 0, 'vi', '_json', 'Sort', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(171, 0, 'vi', '_json', 'Ngày tạo', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(172, 0, 'vi', '_json', 'Lệnh', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(173, 0, 'vi', '_json', 'Tạo mã giảm giá', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(174, 0, 'vi', '_json', 'Tạo mã', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(175, 0, 'vi', '_json', 'Tạo mã giảm giá thành công', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(176, 0, 'vi', '_json', 'Cập nhật mã giảm giá thành công', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(177, 0, 'vi', '_json', 'Xóa mã giảm giá thành công', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(178, 0, 'vi', '_json', 'Cập nhật trạng thái mã thành công', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(179, 0, 'vi', '_json', 'Lấy dữ liệu mã thành công', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(180, 0, 'vi', '_json', 'gioithieuchung', 'Giới thiệu chung', '2022-01-18 14:18:25', '2022-01-25 10:08:35'),
	(181, 0, 'vi', '_json', 'tintuc', 'Tin tức', '2022-01-18 14:18:25', '2022-01-25 10:08:35'),
	(182, 0, 'vi', '_json', 'timhieuthem', 'Tìm hiểu thêm', '2022-01-18 14:18:25', '2022-01-25 10:08:35'),
	(183, 0, 'vi', '_json', 'Cập nhật', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(184, 0, 'vi', '_json', 'Hủy bỏ', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(185, 0, 'vi', '_json', 'Tổng cộng', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(186, 0, 'vi', '_json', 'bản ghi', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(187, 0, 'vi', '_json', 'trang', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(188, 0, 'vi', '_json', 'Tạo biến thể thành công', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(189, 0, 'vi', '_json', 'Tạo biến thể không thành công', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(190, 0, 'vi', '_json', 'Sửa biến thể thành công', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(191, 0, 'vi', '_json', 'Xóa biến thể thành công', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(192, 0, 'vi', '_json', 'Khong co du lieu', NULL, '2022-01-18 14:18:25', '2022-01-18 14:18:25'),
	(193, 0, 'en', 'site', 'ketnoivoichungtoi', 'Connect with us!', '2022-01-18 14:21:43', '2022-04-01 10:55:02'),
	(194, 0, 'vi', 'site', 'tintuc', 'Tin tức', '2022-01-18 14:56:22', '2022-04-01 10:55:02'),
	(195, 0, 'en', 'site', 'tintuc', 'News', '2022-01-18 14:56:45', '2022-04-01 10:55:02'),
	(196, 0, 'vi', 'site', 'hienthi', 'Hiển thị', '2022-01-18 15:17:27', '2022-04-01 10:55:02'),
	(197, 0, 'en', 'site', 'hienthi', 'Show', '2022-01-18 15:17:40', '2022-04-01 10:55:02'),
	(198, 0, 'vi', 'site', 'trongso', 'trong số', '2022-01-18 15:18:05', '2022-04-01 10:55:02'),
	(199, 0, 'en', 'site', 'trongso', 'in', '2022-01-18 15:18:22', '2022-04-01 10:55:02'),
	(200, 0, 'en', 'site', 'trangchu', 'Home', '2022-01-18 15:18:29', '2022-04-01 10:55:02'),
	(201, 0, 'en', 'site', 'timhieungay', 'Research now', '2022-01-18 15:19:10', '2022-04-01 10:55:02'),
	(202, 0, 'en', 'site', 'tiengviet', 'Vietnamese', '2022-01-18 15:19:32', '2022-04-01 10:55:02'),
	(203, 0, 'en', 'site', 'tienganh', 'English', '2022-01-18 15:19:48', '2022-04-01 10:55:02'),
	(204, 0, 'vi', 'site', 'xemthem', 'Xem thêm', '2022-01-18 15:28:48', '2022-04-01 10:55:02'),
	(205, 0, 'en', 'site', 'xemthem', 'See more', '2022-01-18 15:29:13', '2022-04-01 10:55:02'),
	(206, 0, 'vi', 'site', 'xemthemtintuc', 'Xem thêm tin tức', '2022-01-18 15:32:35', '2022-04-01 10:55:02'),
	(207, 0, 'en', 'site', 'xemthemtintuc', 'See more news', '2022-01-18 15:32:50', '2022-04-01 10:55:02'),
	(208, 0, 'vi', 'site', 'sapxep', 'Sắp xếp', '2022-01-18 17:01:39', '2022-04-01 10:55:02'),
	(209, 0, 'en', 'site', 'sapxep', 'Sort', '2022-01-18 17:01:59', '2022-04-01 10:55:02'),
	(210, 0, 'vi', 'site', 'moinhat', 'Mới nhất', '2022-01-18 17:02:14', '2022-04-01 10:55:02'),
	(211, 0, 'en', 'site', 'moinhat', 'Latest', '2022-01-18 17:02:42', '2022-04-01 10:55:02'),
	(212, 0, 'vi', 'site', 'cunhat', 'Cũ nhất', '2022-01-18 17:02:47', '2022-04-01 10:55:02'),
	(213, 0, 'en', 'site', 'cunhat', 'Oldest', '2022-01-18 17:03:27', '2022-04-01 10:55:02'),
	(214, 0, 'vi', 'site', 'tintuckhac', 'Tin tức khác', '2022-01-18 17:58:01', '2022-04-01 10:55:02'),
	(215, 0, 'en', 'site', 'tintuckhac', 'Other news', '2022-01-18 17:58:16', '2022-04-01 10:55:02'),
	(216, 0, 'vi', 'site', 'lienhe', 'Liên hệ', '2022-01-19 09:59:50', '2022-04-01 10:55:02'),
	(217, 0, 'en', 'site', 'lienhe', 'Contact', '2022-01-19 10:00:25', '2022-04-01 10:55:02'),
	(218, 0, 'vi', 'site', 'diachi', 'Địa chỉ', '2022-01-19 10:00:47', '2022-04-01 10:55:02'),
	(219, 0, 'en', 'site', 'diachi', 'address', '2022-01-19 10:01:03', '2022-04-01 10:55:02'),
	(220, 0, 'vi', 'site', 'chamsockhachhang', 'Chăm sóc khách hàng', '2022-01-19 10:01:34', '2022-04-01 10:55:02'),
	(221, 0, 'en', 'site', 'chamsockhachhang', 'Customer care', '2022-01-19 10:01:47', '2022-04-01 10:55:02'),
	(222, 0, 'vi', 'site', 'lienhevoichungtoi', 'Liên hệ với chúng tôi', '2022-01-19 10:02:50', '2022-04-01 10:55:02'),
	(223, 0, 'en', 'site', 'lienhevoichungtoi', 'Contact us', '2022-01-19 10:03:13', '2022-04-01 10:55:02'),
	(224, 0, 'vi', 'site', 'tencuaban', 'Tên của bạn', '2022-01-19 10:09:23', '2022-04-01 10:55:02'),
	(225, 0, 'en', 'site', 'tencuaban', 'Full name', '2022-01-19 10:09:47', '2022-04-01 10:55:02'),
	(226, 0, 'vi', 'site', 'sodienthoaicuaban', 'Số điện thoại của bạn', '2022-01-19 10:10:34', '2022-04-01 10:55:02'),
	(227, 0, 'en', 'site', 'sodienthoaicuaban', 'Phone number', '2022-01-19 10:11:10', '2022-04-01 10:55:02'),
	(228, 0, 'vi', 'site', 'emailcuaban', 'Email của bạn', '2022-01-19 10:12:17', '2022-04-01 10:55:02'),
	(229, 0, 'en', 'site', 'emailcuaban', 'Email', '2022-01-19 10:12:30', '2022-04-01 10:55:02'),
	(230, 0, 'en', 'site', 'nhanthongtin', 'To receive detailed product information, please leave information here.', '2022-01-21 14:38:49', '2022-04-01 10:55:02'),
	(231, 0, 'en', 'site', 'nhapemaildangky', 'Enter registration email', '2022-01-21 14:39:30', '2022-04-01 10:55:02'),
	(232, 0, 'en', '_json', 'tiengviet', 'Vietnamese', '2022-01-25 10:07:19', '2022-01-25 10:08:35'),
	(233, 0, 'en', '_json', 'tienganh', 'English', '2022-01-25 10:07:19', '2022-01-25 10:08:35'),
	(234, 0, 'en', '_json', 'tiengtrung', 'Chinese', '2022-01-25 10:07:19', '2022-01-25 10:08:35'),
	(235, 0, 'en', '_json', 'gioithieuchung', 'General introduction', '2022-01-25 10:07:19', '2022-01-25 10:08:35'),
	(236, 0, 'en', '_json', 'tintuc', 'News', '2022-01-25 10:07:20', '2022-01-25 10:08:35'),
	(237, 0, 'en', '_json', 'timhieuthem', 'See more', '2022-01-25 10:07:20', '2022-01-25 10:08:35'),
	(238, 0, 'en', '_json', 'timhieungay', 'Find out now', '2022-01-25 10:07:20', '2022-01-25 10:08:35'),
	(239, 0, 'en', '_json', 'moi', 'New', '2022-01-25 10:07:21', '2022-01-25 10:08:35'),
	(240, 0, 'en', '_json', 'vanhieudoitackhac', 'And many more partners!', '2022-01-25 10:07:21', '2022-01-25 10:08:35'),
	(241, 0, 'en', '_json', 'lienhengay', 'Contact now', '2022-01-25 10:07:21', '2022-01-25 10:08:35'),
	(242, 0, 'en', '_json', 'khamphathem', 'Discover more', '2022-01-25 10:07:21', '2022-01-25 10:08:35'),
	(243, 0, 'en', '_json', 'lienhevoichungtoideduochotrototnhat', 'Discover more', '2022-01-25 10:07:21', '2022-01-25 10:08:35'),
	(244, 0, 'en', '_json', 'xinchanthanhcamon', 'Sincerely thank', '2022-01-25 10:07:21', '2022-01-25 10:08:35'),
	(245, 0, 'en', '_json', 'nhanthongtin', 'Receive information', '2022-01-25 10:07:22', '2022-01-25 10:08:35'),
	(246, 0, 'en', '_json', 'dangky', 'Registration', '2022-01-25 10:07:22', '2022-01-25 10:08:35'),
	(247, 0, 'en', '_json', 'timkiem', 'Search', '2022-01-25 10:07:22', '2022-01-25 10:08:35'),
	(248, 0, 'en', '_json', 'hienthi', 'Display', '2022-01-25 10:07:22', '2022-01-25 10:08:35'),
	(249, 0, 'en', '_json', 'trongso', 'Among', '2022-01-25 10:07:22', '2022-01-25 10:08:35'),
	(250, 0, 'en', '_json', 'sanphamcuabosuutap', 'products of the collection', '2022-01-25 10:07:22', '2022-01-25 10:08:35'),
	(251, 0, 'en', '_json', 'sapxep', 'Sort', '2022-01-25 10:07:22', '2022-01-25 10:08:35'),
	(252, 0, 'en', '_json', 'moinhat', 'Latest', '2022-01-25 10:07:22', '2022-01-25 10:08:35'),
	(253, 0, 'en', '_json', 'cunhat', 'Oldest', '2022-01-25 10:07:22', '2022-01-25 10:08:35'),
	(254, 0, 'en', '_json', 'dangkydenhantintucmoinhat', 'Sign up to receive the latest news', '2022-01-25 10:07:22', '2022-01-25 10:08:35'),
	(255, 0, 'en', '_json', 'ketnoivoichungtoi', 'Connect with us', '2022-01-25 10:07:23', '2022-01-25 10:08:35'),
	(256, 0, 'en', '_json', 'timkiemsanpham', 'Search product', '2022-01-25 10:07:23', '2022-01-25 10:08:35'),
	(257, 0, 'en', '_json', 'sanpham', 'Product', '2022-01-25 10:07:23', '2022-01-25 10:08:35'),
	(258, 0, 'en', '_json', 'banquyenthuocve', 'Copyright belongs to ECO Products JSC, 2021.', '2022-01-25 10:07:23', '2022-01-25 10:08:35'),
	(259, 0, 'en', '_json', 'motasanphamnoibat', 'As one of the world\'s leading enterprises in the production and distribution of high-quality quartz-based artificial stone, Century Stone is proud to bring customers the latest design trends.', '2022-01-25 10:07:23', '2022-01-25 10:08:35'),
	(260, 0, 'en', '_json', 'bosuutap', 'Collection', '2022-01-25 10:07:23', '2022-01-25 10:08:35'),
	(261, 0, 'en', '_json', 'trangchu', 'Home', '2022-01-25 10:07:23', '2022-01-25 10:08:35'),
	(262, 0, 'en', '_json', 'luachonkhonggian', 'Choice of space', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(263, 0, 'en', '_json', 'nhanxetthanhcong', 'Successfully received lightning', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(264, 0, 'en', '_json', 'dacoloixayravuilongthulai', 'Error!.Please try again', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(265, 0, 'en', '_json', 'xemthem', 'See more', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(266, 0, 'en', '_json', 'timthay', 'Had found', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(267, 0, 'en', '_json', 'ketqua', 'Results matching keyword', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(268, 0, 'en', '_json', 'chinhsachbaohanh', 'Warranty policy', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(269, 0, 'en', '_json', 'nhansetcuaban', 'Get of you', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(270, 0, 'en', '_json', 'danhgiacuaban', 'Your review', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(271, 0, 'en', '_json', 'hethang', 'Out of stock', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(272, 0, 'en', '_json', 'mausac', 'Color', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(273, 0, 'en', '_json', 'tencuaban', 'Your name', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(274, 0, 'en', '_json', 'emailcuaban', 'Your email', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(275, 0, 'en', '_json', 'vietdanhgia', 'Write a review', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(276, 0, 'en', '_json', 'danhgia', 'Evaluate', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(277, 0, 'en', '_json', 'cothebanthich', 'Maybe you like', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(278, 0, 'en', '_json', 'nhapemaildangky', 'Enter registration email', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(279, 0, 'en', '_json', 'email_required', 'Email is not vacant', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(280, 0, 'en', '_json', 'email_unique', 'Email already registered', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(281, 0, 'vi', '_json', 'tiengviet', 'Tiếng Việt', '2022-01-25 10:07:24', '2022-01-25 10:08:35'),
	(282, 0, 'vi', '_json', 'tienganh', 'Tiếng Anh', '2022-01-25 10:07:25', '2022-01-25 10:08:35'),
	(283, 0, 'vi', '_json', 'tiengtrung', 'Tiếng Trung', '2022-01-25 10:07:25', '2022-01-25 10:08:35'),
	(284, 0, 'vi', '_json', 'doitaccuachungtoi', 'Đối tác của chúng tôi', '2022-01-25 10:07:25', '2022-01-25 10:08:35'),
	(285, 0, 'vi', '_json', 'vanhieudoitackhac', 'và nhiều đối tác khác!', '2022-01-25 10:07:25', '2022-01-25 10:08:35'),
	(286, 0, 'vi', '_json', 'timhieungay', 'Tìm hiểu ngay', '2022-01-25 10:07:25', '2022-01-25 10:08:35'),
	(287, 0, 'vi', '_json', 'moi', 'Mới', '2022-01-25 10:07:25', '2022-01-25 10:08:35'),
	(288, 0, 'vi', '_json', 'lienhengay', 'Liên hệ ngay', '2022-01-25 10:07:25', '2022-01-25 10:08:35'),
	(289, 0, 'vi', '_json', 'khamphathem', 'Khám phá thêm', '2022-01-25 10:07:25', '2022-01-25 10:08:35'),
	(290, 0, 'vi', '_json', 'lienhevoichungtoideduochotrototnhat', 'liên hệ với chúng tôi để được hỗ trợ tốt nhất', '2022-01-25 10:07:26', '2022-01-25 10:08:35'),
	(291, 0, 'vi', '_json', 'xinchanthanhcamon', 'Xin chân thành cảm ơn', '2022-01-25 10:07:26', '2022-01-25 10:08:35'),
	(292, 0, 'vi', '_json', 'nhanthongtin', 'Nhận thông tin', '2022-01-25 10:07:26', '2022-01-25 10:08:35'),
	(293, 0, 'vi', '_json', 'dangky', 'Đăng ký', '2022-01-25 10:07:26', '2022-01-25 10:08:35'),
	(294, 0, 'vi', '_json', 'timkiem', 'Tìm kiếm', '2022-01-25 10:07:26', '2022-01-25 10:08:35'),
	(295, 0, 'vi', '_json', 'hienthi', 'Hiển thị', '2022-01-25 10:07:26', '2022-01-25 10:08:35'),
	(296, 0, 'vi', '_json', 'trongso', 'Trong số', '2022-01-25 10:07:26', '2022-01-25 10:08:35'),
	(297, 0, 'vi', '_json', 'sanphamcuabosuutap', 'sản phẩm của bộ sưu tập', '2022-01-25 10:07:27', '2022-01-25 10:08:35'),
	(298, 0, 'vi', '_json', 'sapxep', 'Sắp xếp', '2022-01-25 10:07:27', '2022-01-25 10:08:35'),
	(299, 0, 'vi', '_json', 'moinhat', 'Mới nhất', '2022-01-25 10:07:27', '2022-01-25 10:08:35'),
	(300, 0, 'vi', '_json', 'cunhat', 'Cũ nhất', '2022-01-25 10:07:27', '2022-01-25 10:08:35'),
	(301, 0, 'vi', '_json', 'dangkydenhantintucmoinhat', 'Đăng ký để nhận tin tức mới nhất', '2022-01-25 10:07:27', '2022-01-25 10:08:35'),
	(302, 0, 'vi', '_json', 'ketnoivoichungtoi', 'Kết nối với chúng tôi', '2022-01-25 10:07:28', '2022-01-25 10:08:35'),
	(303, 0, 'vi', '_json', 'timkiemsanpham', 'Tìm kiếm sản phẩm', '2022-01-25 10:07:28', '2022-01-25 10:08:35'),
	(304, 0, 'vi', '_json', 'sanpham', 'Sản phẩm', '2022-01-25 10:07:28', '2022-01-25 10:08:35'),
	(305, 0, 'vi', '_json', 'banquyenthuocve', 'Bản quyền thuộc về ECO Products JSC, 2021.', '2022-01-25 10:07:28', '2022-01-25 10:08:35'),
	(306, 0, 'vi', '_json', 'motasanphamnoibat', 'Là một trong những doanh nghiệp hàng đầu thế giới về sản xuất và phân phối đá nhân tạo gốc thạch anh cao cấp, Century Stone tự hào mang đến cho khách hàng các xu hướng thiết kế mới nhất', '2022-01-25 10:07:28', '2022-01-25 10:08:35'),
	(307, 0, 'vi', '_json', 'trangchu', 'Trang chủ', '2022-01-25 10:07:28', '2022-01-25 10:08:35'),
	(308, 0, 'vi', '_json', 'luachonkhonggian', 'Lựa chọn không gian', '2022-01-25 10:07:28', '2022-01-25 10:08:35'),
	(309, 0, 'vi', '_json', 'danhgia', 'Đánh giá', '2022-01-25 10:07:28', '2022-01-25 10:08:35'),
	(310, 0, 'vi', '_json', 'tinhtrang', 'Tình trạng', '2022-01-25 10:07:28', '2022-01-25 10:08:35'),
	(311, 0, 'vi', '_json', 'conhang', 'Còn hàng', '2022-01-25 10:07:28', '2022-01-25 10:08:35'),
	(312, 0, 'vi', '_json', 'masanpham', 'Mã sản phẩm', '2022-01-25 10:07:28', '2022-01-25 10:08:35'),
	(313, 0, 'vi', '_json', 'lienhededathang', 'Liên hệ đặt hàng', '2022-01-25 10:07:28', '2022-01-25 10:08:35'),
	(314, 0, 'vi', '_json', 'noidemua', 'Nơi để mua', '2022-01-25 10:07:29', '2022-01-25 10:08:35'),
	(315, 0, 'vi', '_json', 'motasanpham', 'Mô tả Sản phẩm', '2022-01-25 10:07:29', '2022-01-25 10:08:35'),
	(316, 0, 'vi', '_json', 'bosutap', 'Bộ sưu tập', '2022-01-25 10:07:29', '2022-01-25 10:08:35'),
	(317, 0, 'vi', '_json', 'tencuaban', 'Tên của bạn', '2022-01-25 10:07:29', '2022-01-25 10:08:35'),
	(318, 0, 'vi', '_json', 'emailcuaban', 'Email của bạn', '2022-01-25 10:07:29', '2022-01-25 10:08:35'),
	(319, 0, 'vi', '_json', 'vietdanhgia', 'Viết đánh giá', '2022-01-25 10:07:29', '2022-01-25 10:08:35'),
	(320, 0, 'vi', '_json', 'cothebanthich', 'Có thể bạn sẽ thích', '2022-01-25 10:07:29', '2022-01-25 10:08:35'),
	(321, 0, 'vi', '_json', 'nhanxetthanhcong', 'Nhận xét thành công', '2022-01-25 10:07:29', '2022-01-25 10:08:35'),
	(322, 0, 'vi', '_json', 'dacoloixayravuilongthulai', 'Đã xảy ra lỗi!. Vui lòng thử lại', '2022-01-25 10:07:29', '2022-01-25 10:08:35'),
	(323, 0, 'vi', '_json', 'xemthem', 'Xemthem', '2022-01-25 10:07:29', '2022-01-25 10:08:35'),
	(324, 0, 'vi', '_json', 'timthay', 'Đã tìm thấy', '2022-01-25 10:07:30', '2022-01-25 10:08:35'),
	(325, 0, 'vi', '_json', 'ketqua', 'kết quả phù hợp với từ khoá', '2022-01-25 10:07:30', '2022-01-25 10:08:35'),
	(326, 0, 'vi', '_json', 'chinhsachbanhang', 'Chính sách bán hàng', '2022-01-25 10:07:30', '2022-01-25 10:08:35'),
	(327, 0, 'vi', '_json', 'nhansetcuaban', 'Nhận xét của bạn', '2022-01-25 10:07:30', '2022-01-25 10:08:35'),
	(328, 0, 'vi', '_json', 'danhgiacuaban', 'Đánh giá của bạn', '2022-01-25 10:07:30', '2022-01-25 10:08:35'),
	(329, 0, 'vi', '_json', 'hethang', 'Hết hàng', '2022-01-25 10:07:30', '2022-01-25 10:08:35'),
	(330, 0, 'vi', '_json', 'bosuutap', 'Bộ sưu tập', '2022-01-25 10:07:30', '2022-01-25 10:08:35'),
	(331, 0, 'vi', '_json', 'mausac', 'Màu sắc', '2022-01-25 10:07:31', '2022-01-25 10:08:35'),
	(332, 0, 'vi', '_json', 'nhapemaildangky', 'Nhập email đăng ký', '2022-01-25 10:07:31', '2022-01-25 10:08:35'),
	(333, 0, 'vi', '_json', 'email_required', 'Email không được bỏ trống', '2022-01-25 10:07:31', '2022-01-25 10:08:35'),
	(334, 0, 'vi', '_json', 'email_unique', 'Email đã đăng ký rồi', '2022-01-25 10:07:31', '2022-01-25 10:08:35'),
	(335, 0, 'zh', '_json', 'tiengviet', '越南语', '2022-01-25 10:07:31', '2022-01-25 10:08:35'),
	(336, 0, 'zh', '_json', 'tienganh', '英语', '2022-01-25 10:07:32', '2022-01-25 10:08:35'),
	(337, 0, 'zh', '_json', 'tiengtrung', '中国人', '2022-01-25 10:07:32', '2022-01-25 10:08:35'),
	(338, 0, 'zh', '_json', 'gioithieuchung', '总体介绍', '2022-01-25 10:07:32', '2022-01-25 10:08:35'),
	(339, 0, 'zh', '_json', 'tintuc', '消息', '2022-01-25 10:07:32', '2022-01-25 10:08:35'),
	(340, 0, 'zh', '_json', 'timhieuthem', '寻找更多信息', '2022-01-25 10:07:32', '2022-01-25 10:08:35'),
	(341, 0, 'zh', '_json', 'doitaccuachungtoi', '我们的合作伙伴', '2022-01-25 10:07:32', '2022-01-25 10:08:35'),
	(342, 0, 'zh', '_json', 'vanhieudoitackhac', '和许多其他合作伙伴!', '2022-01-25 10:07:32', '2022-01-25 10:08:35'),
	(343, 0, 'zh', '_json', 'timhieungay', 'T马上去查', '2022-01-25 10:07:32', '2022-01-25 10:08:35'),
	(344, 0, 'zh', '_json', 'moi', '新的', '2022-01-25 10:07:32', '2022-01-25 10:08:35'),
	(345, 0, 'zh', '_json', 'lienhengay', '现在联系', '2022-01-25 10:07:32', '2022-01-25 10:08:35'),
	(346, 0, 'zh', '_json', 'khamphathem', '发现更多', '2022-01-25 10:07:32', '2022-01-25 10:08:35'),
	(347, 0, 'zh', '_json', 'lienhevoichungtoideduochotrototnhat', '联系我们以获得最佳支持', '2022-01-25 10:07:33', '2022-01-25 10:08:35'),
	(348, 0, 'zh', '_json', 'xinchanthanhcamon', '衷心感谢', '2022-01-25 10:07:33', '2022-01-25 10:08:35'),
	(349, 0, 'zh', '_json', 'nhanthongtin', '接收信息', '2022-01-25 10:07:33', '2022-01-25 10:08:35'),
	(350, 0, 'zh', '_json', 'dangky', '登记', '2022-01-25 10:07:33', '2022-01-25 10:08:35'),
	(351, 0, 'zh', '_json', 'timkiem', '搜索', '2022-01-25 10:07:33', '2022-01-25 10:08:35'),
	(352, 0, 'zh', '_json', 'hienthi', '展示', '2022-01-25 10:07:33', '2022-01-25 10:08:35'),
	(353, 0, 'zh', '_json', 'trongso', '之中', '2022-01-25 10:07:34', '2022-01-25 10:08:35'),
	(354, 0, 'zh', '_json', 'sanphamcuabosuutap', '收藏品', '2022-01-25 10:07:34', '2022-01-25 10:08:35'),
	(355, 0, 'zh', '_json', 'sapxep', '种类', '2022-01-25 10:07:34', '2022-01-25 10:08:35'),
	(356, 0, 'zh', '_json', 'moinhat', '最新的', '2022-01-25 10:07:34', '2022-01-25 10:08:35'),
	(357, 0, 'zh', '_json', 'cunhat', '最老的', '2022-01-25 10:07:34', '2022-01-25 10:08:35'),
	(358, 0, 'zh', '_json', 'dangkydenhantintucmoinhat', '注册以接收最新消息', '2022-01-25 10:07:34', '2022-01-25 10:08:35'),
	(359, 0, 'zh', '_json', 'ketnoivoichungtoi', '联系我们', '2022-01-25 10:07:34', '2022-01-25 10:08:35'),
	(360, 0, 'zh', '_json', 'timkiemsanpham', '搜索产品', '2022-01-25 10:07:34', '2022-01-25 10:08:35'),
	(361, 0, 'zh', '_json', 'sanpham', '产品', '2022-01-25 10:07:35', '2022-01-25 10:08:35'),
	(362, 0, 'zh', '_json', 'banquyenthuocve', '版权属于 ECO Products JSC, 2021。', '2022-01-25 10:07:35', '2022-01-25 10:08:35'),
	(363, 0, 'zh', '_json', 'motasanphamnoibat', '作为世界领先的高品质石英基人造石生产和分销企业之一，世纪石自豪地为客户带来最新的设计趋势。', '2022-01-25 10:07:35', '2022-01-25 10:08:35'),
	(364, 0, 'zh', '_json', 'trangchu', 'T主页', '2022-01-25 10:07:35', '2022-01-25 10:08:35'),
	(365, 0, 'zh', '_json', 'luachonkhonggian', '空间的选择', '2022-01-25 10:07:35', '2022-01-25 10:08:35'),
	(366, 0, 'zh', '_json', 'danhgia', '评价', '2022-01-25 10:07:35', '2022-01-25 10:08:35'),
	(367, 0, 'zh', '_json', 'tinhtrang', '地位', '2022-01-25 10:07:35', '2022-01-25 10:08:35'),
	(368, 0, 'zh', '_json', 'conhang', '袜', '2022-01-25 10:07:35', '2022-01-25 10:08:35'),
	(369, 1, 'zh', '_json', 'masanpham', '产品代码', '2022-01-25 10:07:35', '2022-02-10 10:38:13'),
	(370, 0, 'zh', '_json', 'lienhededathang', '产品代码', '2022-01-25 10:07:35', '2022-01-25 10:08:35'),
	(371, 0, 'zh', '_json', 'noidemua', '去哪买', '2022-01-25 10:07:35', '2022-01-25 10:08:35'),
	(372, 0, 'zh', '_json', 'motasanpham', '产品描述', '2022-01-25 10:07:35', '2022-01-25 10:08:35'),
	(373, 0, 'zh', '_json', 'bosutap', '收藏', '2022-01-25 10:07:36', '2022-01-25 10:08:35'),
	(374, 0, 'zh', '_json', 'tencuaban', '你的名字', '2022-01-25 10:07:36', '2022-01-25 10:08:35'),
	(375, 0, 'zh', '_json', 'emailcuaban', '你的邮件', '2022-01-25 10:07:36', '2022-01-25 10:08:35'),
	(376, 0, 'zh', '_json', 'vietdanhgia', '写评论', '2022-01-25 10:07:36', '2022-01-25 10:08:35'),
	(377, 0, 'zh', '_json', 'cothebanthich', '也许你会喜欢', '2022-01-25 10:07:36', '2022-01-25 10:08:35'),
	(378, 0, 'zh', '_json', 'nhanxetthanhcong', '成功收到闪电', '2022-01-25 10:07:36', '2022-01-25 10:08:35'),
	(379, 0, 'zh', '_json', 'dacoloixayravuilongthulai', '错误！!. 请再试一次', '2022-01-25 10:07:36', '2022-01-25 10:08:35'),
	(380, 0, 'zh', '_json', 'xemthem', '查看更多', '2022-01-25 10:07:36', '2022-01-25 10:08:35'),
	(381, 0, 'zh', '_json', 'timthay', '找到了', '2022-01-25 10:07:37', '2022-01-25 10:08:35'),
	(382, 0, 'zh', '_json', 'ketqua', '结果匹配关键字', '2022-01-25 10:07:37', '2022-01-25 10:08:35'),
	(383, 0, 'zh', '_json', 'chinhsachbanhang', '销售政策', '2022-01-25 10:07:37', '2022-01-25 10:08:35'),
	(384, 0, 'zh', '_json', 'nhansetcuaban', '你的评论', '2022-01-25 10:07:38', '2022-01-25 10:08:35'),
	(385, 0, 'zh', '_json', 'danhgiacuaban', '你的意见', '2022-01-25 10:07:38', '2022-01-25 10:08:35'),
	(386, 0, 'zh', '_json', 'hethang', '缺货', '2022-01-25 10:07:38', '2022-01-25 10:08:35'),
	(387, 0, 'zh', '_json', 'bosuutap', '收藏', '2022-01-25 10:07:38', '2022-01-25 10:08:35'),
	(388, 0, 'zh', '_json', 'mausac', '颜色', '2022-01-25 10:07:38', '2022-01-25 10:08:35'),
	(389, 0, 'zh', '_json', 'nhapemaildangky', '输入注册邮箱', '2022-01-25 10:07:38', '2022-01-25 10:08:35'),
	(390, 0, 'zh', '_json', 'email_required', '邮箱不是空的', '2022-01-25 10:07:38', '2022-01-25 10:08:35'),
	(391, 0, 'zh', '_json', 'email_unique', '邮箱已经注册', '2022-01-25 10:07:38', '2022-01-25 10:08:35'),
	(392, 0, 'en', '_json', 'doitaccuachungtoi', 'Our partner', '2022-01-25 10:08:30', '2022-01-25 10:08:35'),
	(393, 0, 'en', 'site', 'camonbandadangky', 'Thank you for registering. Your information has been sent to Admin', '2022-02-07 09:56:59', '2022-04-01 10:55:02'),
	(394, 0, 'zh', 'site', 'camonbandadangky', '感谢您注册。 您的信息已发送给管理员', '2022-02-07 09:57:27', '2022-04-01 10:55:02'),
	(395, 0, 'zh', 'site', 'bosuutap', '收藏', '2022-02-07 09:57:46', '2022-04-01 10:55:02'),
	(396, 0, 'zh', 'site', 'xembosuutap', '查看收藏', '2022-02-07 10:18:57', '2022-04-01 10:55:02'),
	(397, 0, 'vi', '_json', 'name_required', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(398, 0, 'vi', '_json', 'name_min', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(399, 0, 'vi', '_json', 'name_max', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(400, 0, 'vi', '_json', 'email_email', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(401, 0, 'vi', '_json', 'phone_required', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(402, 0, 'vi', '_json', 'phone_regex', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(403, 0, 'vi', '_json', 'phone_unique', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(404, 0, 'vi', '_json', 'message_required', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(405, 0, 'vi', '_json', 'message_max', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(406, 0, 'vi', '_json', 'datimthay', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(407, 0, 'vi', '_json', 'ketquaphuhopvoitukhoa', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(408, 0, 'vi', '_json', 'khongcoketquanao', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(409, 0, 'vi', '_json', 'banchay', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(410, 0, 'vi', '_json', 'nhantuvan', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(411, 0, 'vi', '_json', 'tu', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(412, 0, 'vi', '_json', 'chungtoiselienhevoiban', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(413, 0, 'vi', '_json', 'hovaten', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(414, 0, 'vi', '_json', 'sodienthoai', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(415, 0, 'vi', '_json', 'email', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(416, 0, 'vi', '_json', 'noidung', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(417, 0, 'vi', '_json', 'tuvanchotoi', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(418, 0, 'vi', '_json', 'tenkhongduocdetrong', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(419, 0, 'vi', '_json', 'emailkhongduocdetrong', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(420, 0, 'vi', '_json', 'emailkhongdungdinhdang', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(421, 0, 'vi', '_json', 'noidungkhongduocbotrong', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(422, 0, 'vi', '_json', 'noidungchuatoido255kytu', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(423, 0, 'vi', '_json', 'tenchuada255kytu', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(424, 0, 'vi', '_json', 'emailchuada255kytu', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(425, 0, 'vi', '_json', 'haydanhgiasao', NULL, '2022-02-08 10:24:00', '2022-02-08 10:24:00'),
	(426, 0, 'vi', 'site', 'doitaccuachungtoi', 'Đối tác của chúng tôi', '2022-02-08 18:23:26', '2022-04-01 10:55:02'),
	(427, 0, 'zh', 'site', 'doitaccuachungtoi', '我们的合作伙伴', '2022-02-08 18:23:52', '2022-04-01 10:55:02'),
	(428, 0, 'en', 'site', 'doitaccuachungtoi', 'Our partner', '2022-02-08 18:24:01', '2022-04-01 10:55:02'),
	(429, 0, 'vi', '_json', 'see more', NULL, '2022-02-09 11:04:50', '2022-02-09 11:04:50'),
	(430, 0, 'zh', 'site', 'xemthem', '查看更多', '2022-02-09 11:06:34', '2022-04-01 10:55:02'),
	(431, 0, 'zh', 'site', 'timhieuthem', '寻找更多信息', '2022-02-09 14:46:28', '2022-04-01 10:55:02'),
	(432, 0, 'zh', 'site', 'tintuc', '消息', '2022-02-09 14:47:08', '2022-04-01 10:55:02'),
	(433, 0, 'zh', 'site', 'sanphamnoibat', '特色产品', '2022-02-09 14:49:50', '2022-04-01 10:55:02'),
	(434, 0, 'zh', 'site', 'lienhe', '接触', '2022-02-09 14:50:40', '2022-04-01 10:55:02'),
	(435, 0, 'zh', 'site', 'lienhevoichungtoi', '联系我们', '2022-02-09 14:59:47', '2022-04-01 10:55:02'),
	(436, 0, 'zh', 'site', 'hotenkhongduocdetrong', '全名是必填项', '2022-02-09 15:14:20', '2022-04-01 10:55:02'),
	(437, 0, 'zh', 'site', 'emailcuaban', '你的邮件', '2022-02-09 15:15:56', '2022-04-01 10:55:02'),
	(438, 0, 'zh', 'site', 'emailkhongdungdinhdang', '电子邮件无效', '2022-02-09 15:16:08', '2022-04-01 10:55:02'),
	(439, 0, 'zh', 'site', 'emailkhongduocdetrong', '电子邮件不能留空', '2022-02-09 15:16:19', '2022-04-01 10:55:02'),
	(440, 0, 'zh', 'site', 'hienthi', '展示', '2022-02-09 15:16:31', '2022-04-01 10:55:02'),
	(441, 0, 'zh', 'site', 'hovaten', '名字和姓氏', '2022-02-09 15:16:42', '2022-04-01 10:55:02'),
	(442, 0, 'zh', 'site', 'ketnoivoichungtoi', '联系我们！', '2022-02-09 15:16:53', '2022-04-01 10:55:02'),
	(443, 0, 'zh', 'site', 'khamphathem', '发现更多', '2022-02-09 15:17:01', '2022-04-01 10:55:02'),
	(444, 0, 'zh', 'site', 'lienhengay', '现在联系', '2022-02-09 15:17:13', '2022-04-01 10:55:02'),
	(445, 0, 'zh', 'site', 'lienhevoichungtoideduochotrototnhat', '联系我们以获得最佳支持', '2022-02-09 15:17:22', '2022-04-01 10:55:02'),
	(446, 0, 'zh', 'site', 'moi', '新的', '2022-02-09 15:17:33', '2022-04-01 10:55:02'),
	(447, 0, 'zh', 'site', 'moinhat', '最新的', '2022-02-09 15:17:45', '2022-04-01 10:55:02'),
	(448, 0, 'zh', 'site', 'nhanthongtin', '信息在这里。要接收详细的产品信息，请在此处留下您的信息。', '2022-02-09 15:17:57', '2022-04-01 10:55:02'),
	(449, 0, 'zh', 'site', 'nhantuvan', '获得建议', '2022-02-09 15:18:06', '2022-04-01 10:55:02'),
	(450, 0, 'zh', 'site', 'nhapemaildangky', '输入注册邮箱', '2022-02-09 15:18:17', '2022-04-01 10:55:02'),
	(451, 0, 'zh', 'site', 'nhapnoidung', '导入内容', '2022-02-09 15:18:26', '2022-04-01 10:55:02'),
	(452, 0, 'zh', 'site', 'noidungkhongduocdetrong', '内容不能为空', '2022-02-09 15:18:38', '2022-04-01 10:55:02'),
	(453, 0, 'zh', 'site', 'sapxep', '种类', '2022-02-09 15:18:54', '2022-04-01 10:55:02'),
	(454, 0, 'zh', 'site', 'sodienthoai', '电话号码', '2022-02-09 15:19:07', '2022-04-01 10:55:02'),
	(455, 0, 'zh', 'site', 'sodienthoaicuaban', '你的电话号码', '2022-02-09 15:19:15', '2022-04-01 10:55:02'),
	(456, 0, 'zh', 'site', 'sodienthoaikhongdungdinhdang', '电话号码格式不正确', '2022-02-09 15:19:26', '2022-04-01 10:55:02'),
	(457, 0, 'zh', 'site', 'sodienthoaikhongduocdetrong', '电话号码不能留空', '2022-02-09 15:19:35', '2022-04-01 10:55:02'),
	(458, 0, 'zh', 'site', 'sodienthoaikhonghople', '无效的电话号码', '2022-02-09 15:19:45', '2022-04-01 10:55:02'),
	(459, 0, 'zh', 'site', 'thongtincuabandaduocguitoiadmin', '您的信息已发送给管理员', '2022-02-09 15:20:02', '2022-04-01 10:55:02'),
	(460, 0, 'zh', 'site', 'timhieungay', '马上去查', '2022-02-09 15:20:57', '2022-04-01 10:55:02'),
	(461, 0, 'zh', 'site', 'tintuckhac', '其他新闻', '2022-02-09 15:21:11', '2022-04-01 10:55:02'),
	(462, 0, 'zh', 'site', 'trangchu', '主页', '2022-02-09 15:21:24', '2022-04-01 10:55:02'),
	(463, 0, 'zh', 'site', 'trongso', '之中', '2022-02-09 15:22:30', '2022-04-01 10:55:02'),
	(464, 0, 'zh', 'site', 'tucenturystone', '来自世纪石', '2022-02-09 15:22:39', '2022-04-01 10:55:02'),
	(465, 0, 'zh', 'site', 'tuvanchotoi', '告诉我', '2022-02-09 15:22:48', '2022-04-01 10:55:02'),
	(466, 0, 'zh', 'site', 'xemthemtintuc', '查看更多新闻', '2022-02-09 15:23:04', '2022-04-01 10:55:02'),
	(467, 0, 'zh', 'site', 'xinchanthanhcamon', '衷心感谢。', '2022-02-09 15:23:12', '2022-04-01 10:55:02'),
	(468, 0, 'zh', 'site', 'chamsockhachhang', '客户关怀', '2022-02-09 15:23:26', '2022-04-01 10:55:02'),
	(469, 1, 'en', '_json', 'bosutap', 'Collection', '2022-02-10 10:36:44', '2022-02-10 10:36:44'),
	(470, 1, 'en', '_json', 'lienhededathang', 'Contact to order', '2022-02-10 10:37:33', '2022-02-10 10:37:33'),
	(471, 1, 'en', '_json', 'masanpham', 'Product code', '2022-02-10 10:37:53', '2022-02-10 10:37:53'),
	(472, 1, 'en', '_json', 'motasanpham', 'Product Description', '2022-02-10 10:39:29', '2022-02-10 10:39:29'),
	(473, 1, 'en', '_json', 'noidemua', 'Where to buy', '2022-02-10 10:39:44', '2022-02-10 10:39:44'),
	(474, 1, 'en', '_json', 'chinhsachbanhang', 'Sales policy', '2022-02-10 10:40:53', '2022-02-10 10:40:53'),
	(475, 1, 'en', '_json', 'conhang', 'Stocking', '2022-02-10 10:41:09', '2022-02-10 10:41:09'),
	(476, 1, 'en', '_json', 'tinhtrang', 'Status', '2022-02-10 10:42:00', '2022-02-10 10:42:00'),
	(477, 0, 'zh', 'site', 'chiduong', '直接的', '2022-02-10 10:42:59', '2022-04-01 10:55:02'),
	(478, 0, 'zh', 'site', 'chungtoiselienhevoiban', '我们将与您联系', '2022-02-10 10:43:42', '2022-04-01 10:55:02'),
	(479, 0, 'zh', 'site', 'cunhat', '最老的', '2022-02-10 10:43:52', '2022-04-01 10:55:02'),
	(480, 0, 'zh', 'site', 'dacoloixayravuilongthulai', '发生了错误。请再试一次。', '2022-02-10 10:44:05', '2022-04-01 10:55:02'),
	(481, 0, 'zh', 'site', 'dangky', '登记', '2022-02-10 10:44:14', '2022-04-01 10:55:02'),
	(482, 0, 'zh', 'site', 'dangkydenhantintucmoinhat', '订阅以接收世纪石的最新消息', '2022-02-10 10:44:27', '2022-04-01 10:55:02'),
	(483, 0, 'zh', 'site', 'dangnhap', '登录', '2022-02-10 10:44:35', '2022-04-01 10:55:02'),
	(484, 0, 'zh', 'site', 'delaithongtinchitietsanpham', '要接收详细的产品信息，请在此处留下您的信息。', '2022-02-10 10:44:49', '2022-04-01 10:55:02'),
	(485, 0, 'zh', 'site', 'diachi', '地址', '2022-02-10 10:44:59', '2022-04-01 10:55:02'),
	(486, 0, 'zh', 'site', 'diachinhamay', '工厂地址', '2022-02-10 10:45:07', '2022-04-01 10:55:02'),
	(487, 0, 'vi', 'site', 'notice', 'Thông báo', '2022-02-10 14:26:01', '2022-04-01 10:55:02'),
	(488, 0, 'vi', 'site', 'dessanphamnoibat', 'Là một trong những doanh nghiệp hàng đầu thế giới về sản xuất và phân phối đá nhân tạo gốc thạch anh cao cấp, Century Stone tự hào mang đến cho khách hàng các xu hướng thiết kế mới nhất', '2022-02-24 09:53:40', '2022-04-01 10:55:02'),
	(489, 0, 'en', 'site', 'dessanphamnoibat', 'As one of the world\'s leading enterprises in the production and distribution of high-quality quartz-based artificial stone, Century Stone is proud to bring customers the latest design trends.', '2022-02-24 09:54:30', '2022-04-01 10:55:02'),
	(490, 0, 'zh', 'site', 'dessanphamnoibat', '作为世界领先的高品质石英基人造石生产和分销企业之一，世纪石自豪地为客户带来最新的设计趋势。', '2022-02-24 09:54:40', '2022-04-01 10:55:02'),
	(491, 0, 'vi', 'site', 'xemchitiet', 'Xem chi tiết', '2022-02-24 10:10:35', '2022-04-01 10:55:02'),
	(492, 0, 'zh', 'site', 'xemchitiet', '查看详细信息', '2022-02-24 10:11:01', '2022-04-01 10:55:02'),
	(493, 0, 'en', 'site', 'xemchitiet', 'See details', '2022-02-24 10:11:10', '2022-04-01 10:55:02'),
	(494, 0, 'vi', 'site', 'nhaptimkiem', 'Nhập tìm kiếm...', '2022-02-25 11:37:29', '2022-04-01 10:55:02'),
	(495, 0, 'en', 'site', 'nhaptimkiem', 'Enter search ...', '2022-02-25 11:37:57', '2022-04-01 10:55:02'),
	(496, 0, 'zh', 'site', 'nhaptimkiem', '输入搜索', '2022-02-25 11:38:09', '2022-04-01 10:55:02');
/*!40000 ALTER TABLE `ltm_translations` ENABLE KEYS */;

-- Dumping structure for table centurystone.mail_cron
CREATE TABLE IF NOT EXISTS `mail_cron` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(191) CHARACTER SET utf8 DEFAULT NULL,
  `receive_id` int(11) DEFAULT NULL,
  `receive_model` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL,
  `object_model` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `is_sent` char(1) CHARACTER SET utf8 DEFAULT '0',
  `subject` varchar(350) COLLATE utf8_unicode_ci DEFAULT NULL,
  `html` mediumtext CHARACTER SET utf8,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `mail_cron_UN` (`receive_id`,`receive_model`,`object_id`,`object_model`) USING BTREE,
  KEY `mail_cron_is_sent_IDX` (`is_sent`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.mail_cron: ~0 rows (approximately)
/*!40000 ALTER TABLE `mail_cron` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_cron` ENABLE KEYS */;

-- Dumping structure for table centurystone.manufacturer
CREATE TABLE IF NOT EXISTS `manufacturer` (
  `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sort_order` int(3) DEFAULT NULL,
  `primary_category_id` int(11) unsigned DEFAULT '0' COMMENT 'Danh mục chính của sản phẩm',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`manufacturer_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.manufacturer: ~8 rows (approximately)
/*!40000 ALTER TABLE `manufacturer` DISABLE KEYS */;
INSERT INTO `manufacturer` (`manufacturer_id`, `name`, `image`, `sort_order`, `primary_category_id`, `created_at`, `updated_at`) VALUES
	(64, 'Michael Kors', NULL, 1, 127, '2020-10-05 16:06:53', '2021-11-30 15:33:44'),
	(74, 'Bvlgari', NULL, 2, 0, '2021-09-28 17:52:45', '2021-11-30 15:33:46'),
	(75, 'Chanel', NULL, 3, 0, '2021-09-28 17:53:17', '2021-11-30 15:33:48'),
	(79, 'Rayban', NULL, 4, 0, '2021-09-28 17:57:05', '2021-11-30 15:33:51'),
	(84, 'Cartier', NULL, 5, 0, '2021-10-26 08:14:09', '2021-11-30 15:33:51'),
	(85, 'Charmaine', NULL, 6, 0, '2021-10-26 08:21:40', '2021-11-30 15:33:52'),
	(88, 'Burberry', NULL, 7, 0, '2021-11-15 09:49:27', '2021-11-30 15:33:54'),
	(89, 'Charriol', NULL, 8, 0, '2021-11-15 09:49:35', '2021-11-30 15:33:55');
/*!40000 ALTER TABLE `manufacturer` ENABLE KEYS */;

-- Dumping structure for table centurystone.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) DEFAULT NULL,
  `extra_link` varchar(500) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `pid` int(11) DEFAULT '0',
  `perm` varchar(255) DEFAULT NULL,
  `type` tinyint(1) DEFAULT '0',
  `no_follow` tinyint(1) DEFAULT NULL,
  `newtab` tinyint(1) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `menu_pid_IDX` (`pid`) USING BTREE,
  KEY `menu_status_IDX` (`status`) USING BTREE,
  KEY `menu_type_IDX` (`type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.menu: ~143 rows (approximately)
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`, `link`, `extra_link`, `sort_order`, `status`, `pid`, `perm`, `type`, `no_follow`, `newtab`, `icon`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, NULL, NULL, 3, 1, 0, NULL, 0, 1, 0, 'fa fa-calculator', '2020-10-05 01:33:36', '2021-10-11 23:25:01', NULL),
	(3, 'admin_news_home_page', NULL, 1, 1, 29, 'news-view', 0, 1, 0, 'fa fa-newspaper-o', '2020-10-05 03:21:10', '2021-10-11 23:31:26', NULL),
	(4, 'admin_banner_home_page', NULL, 3, 1, 29, NULL, 0, 1, 0, 'fa fa-picture-o', '2020-10-05 02:16:38', '2021-10-11 23:31:48', NULL),
	(6, 'admin.category', NULL, 6, -1, 22, 'category-view', 0, 1, 0, 'icon-list', '2020-10-05 01:30:46', '2020-12-02 22:31:17', NULL),
	(15, NULL, NULL, 100, -1, 14, NULL, 4, 1, 0, NULL, '2020-10-05 03:32:41', '2021-01-16 18:00:16', NULL),
	(16, NULL, NULL, 90, -1, 14, NULL, 4, 1, 0, NULL, NULL, '2021-01-16 18:00:16', NULL),
	(18, 'admin.subscriber.index', NULL, 1, 1, 1, 'subscriber-view', 0, 1, 0, 'fa fa-forumbee', '2020-10-05 03:22:12', '2021-10-25 13:42:00', NULL),
	(19, 'home', NULL, 1, -1, 0, NULL, 3, 0, 0, NULL, '2020-10-05 03:24:56', '2021-01-16 17:50:25', NULL),
	(21, 'admin.gallery', NULL, 95, -1, 1, 'gallery-view', 0, 1, 0, 'icon-grid', NULL, NULL, NULL),
	(22, NULL, NULL, 1, 1, 0, NULL, 0, 1, 0, 'fa fa-cube', '2020-10-05 00:54:59', '2021-03-03 23:58:32', NULL),
	(24, 'admin.contact', NULL, 80, -1, 1, NULL, 0, 1, 0, 'fa fa-vcard-o', '2020-10-05 03:24:18', '2020-12-16 01:08:58', NULL),
	(26, 'admin.comment_post', NULL, 95, -1, 29, NULL, 0, 1, 0, 'icon-speech', NULL, NULL, NULL),
	(27, 'admin.question', NULL, 46, -1, 1, NULL, 0, 1, 0, 'fa fa-question', NULL, NULL, NULL),
	(28, 'admin.product', NULL, 2, -1, 22, NULL, 0, 1, 0, 'icon-tag', '2020-10-05 01:31:12', '2020-12-02 23:17:37', NULL),
	(29, NULL, NULL, 2, 1, 0, NULL, 0, 1, 0, 'fa fa-newspaper-o', '2020-10-05 03:20:26', '2021-10-11 23:24:41', NULL),
	(30, 'admin.customergroup', NULL, 0, -1, 8, NULL, 0, 1, 0, 'icon-list', '2020-10-05 03:23:17', '2020-10-23 00:32:38', NULL),
	(32, NULL, NULL, 89, -1, 0, NULL, 3, 1, 0, NULL, NULL, NULL, NULL),
	(33, NULL, NULL, 88, -1, 0, NULL, 3, 1, 0, NULL, NULL, NULL, NULL),
	(34, NULL, NULL, 87, -1, 0, NULL, 3, 1, 0, NULL, NULL, NULL, NULL),
	(35, NULL, NULL, 86, -1, 0, NULL, 3, 1, 0, NULL, NULL, NULL, NULL),
	(36, NULL, NULL, 85, -1, 0, NULL, 3, 1, 0, NULL, NULL, NULL, NULL),
	(37, NULL, NULL, 84, -1, 0, NULL, 3, 1, 0, NULL, NULL, NULL, NULL),
	(38, NULL, NULL, 83, -1, 0, NULL, 3, 1, 0, NULL, NULL, NULL, NULL),
	(39, 'news.list', NULL, 4, -1, 0, NULL, 3, 1, 0, NULL, '2020-10-05 03:28:16', '2021-01-16 17:50:25', NULL),
	(40, NULL, NULL, 3, -1, 0, NULL, 3, 1, 0, NULL, '2020-10-05 03:26:41', '2021-01-16 17:50:25', NULL),
	(41, NULL, NULL, 0, -1, 32, NULL, 3, 1, 0, NULL, NULL, '2021-01-16 17:50:25', NULL),
	(42, NULL, NULL, 0, -1, 33, NULL, 3, 1, 0, NULL, NULL, '2021-01-16 17:50:25', NULL),
	(43, NULL, NULL, 70, -1, 14, NULL, 4, 1, 1, NULL, '2020-10-05 03:32:18', '2021-01-16 18:00:16', NULL),
	(44, NULL, NULL, 65, -1, 14, NULL, 4, 1, 0, NULL, NULL, '2021-01-16 18:00:16', NULL),
	(45, 'admin.comment_post', NULL, 0, -1, 22, NULL, 0, 1, 0, 'icon-speech', NULL, NULL, NULL),
	(46, NULL, NULL, 0, -1, 34, NULL, 3, 1, 0, NULL, NULL, '2021-01-16 17:50:25', NULL),
	(47, NULL, NULL, 0, -1, 35, NULL, 3, 1, 0, NULL, NULL, '2021-01-16 17:50:25', NULL),
	(48, NULL, NULL, 0, -1, 36, NULL, 3, 1, 0, NULL, NULL, '2021-01-16 17:50:25', NULL),
	(49, NULL, NULL, 0, -1, 37, NULL, 3, 1, 0, NULL, NULL, '2021-01-16 17:50:25', NULL),
	(50, NULL, NULL, 0, -1, 38, NULL, 3, 1, 0, NULL, NULL, NULL, NULL),
	(51, 'admin.warehouse', NULL, 2, -1, 1, NULL, 0, 1, 0, 'fa fa-bank', NULL, NULL, NULL),
	(52, 'admin.coupon', NULL, 0, -1, 8, NULL, 0, 1, 0, 'icon-tag', '2020-10-05 03:23:56', '2020-12-14 21:05:25', NULL),
	(53, 'admin.order', NULL, 2, -1, 1, NULL, 0, 1, 0, '', '2020-10-05 03:21:27', '2020-12-14 21:05:38', NULL),
	(54, 'admin.dashboard', NULL, 1, -1, 1, NULL, 0, 1, 0, 'icon-cup', '2020-10-05 03:20:05', '2020-10-09 02:33:30', NULL),
	(55, 'admin.bank', NULL, 520, -1, 1, NULL, 0, 1, 0, 'icon-diamond', '2020-10-05 03:21:56', '2021-01-16 17:48:41', NULL),
	(56, NULL, NULL, 60, -1, 14, NULL, 4, 0, 1, NULL, '2020-10-05 03:31:15', '2021-01-16 18:00:16', NULL),
	(57, 'promotion', NULL, 1, -1, 0, NULL, 3, 1, 0, NULL, '2020-10-05 03:25:48', '2021-01-16 17:50:25', NULL),
	(58, NULL, NULL, 0, -1, 14, NULL, 4, 1, 0, NULL, '2020-10-05 03:30:48', '2021-01-16 18:00:16', NULL),
	(59, 'admin.attribute', NULL, 0, 2, 61, NULL, 0, 1, 0, 'icon-directions', '2020-10-05 01:32:36', '2020-10-05 01:32:36', NULL),
	(60, 'admin.attribute_group', NULL, 0, 2, 61, NULL, 0, 1, 0, 'icon-tag', '2020-10-05 01:33:02', '2020-10-05 01:33:02', NULL),
	(61, NULL, NULL, 3, -1, 22, NULL, 0, 1, 0, 'icon-list', '2020-10-05 01:31:48', '2020-10-12 20:14:18', NULL),
	(62, 'admin_option_home_page', NULL, 3, 1, 22, NULL, 0, 1, 0, 'fa fa-bars', '2020-10-05 01:16:29', '2022-01-12 16:37:15', '2022-01-12 16:37:15'),
	(63, 'admin.statustock', NULL, 0, 2, 69, NULL, 0, 1, 0, 'icon-tag', '2020-10-05 03:18:48', '2020-11-24 17:39:38', NULL),
	(64, NULL, NULL, 5, 1, 0, NULL, 0, 1, 0, 'fa fa-cog', '2020-10-05 03:15:26', '2021-10-11 23:25:21', NULL),
	(65, 'admin_settings_edit_page', NULL, 1, 1, 64, NULL, 0, 1, 0, 'fa fa-cog', '2020-10-05 03:15:51', '2021-03-09 17:51:01', NULL),
	(66, 'admin_menu_index', NULL, 0, 1, 64, NULL, 0, 1, 0, 'fa fa-list', '2020-10-05 03:16:15', '2021-07-07 18:11:29', NULL),
	(67, NULL, NULL, 4, 1, 0, NULL, 0, 1, 0, 'fa fa-user-circle', '2020-10-05 03:16:40', '2021-03-09 17:45:22', NULL),
	(68, 'admin_authorization_get_all_roles', NULL, 1, 1, 67, NULL, 0, 1, 0, 'fa fa-terminal', '2020-10-05 03:17:07', '2021-07-08 10:09:35', NULL),
	(69, NULL, NULL, 0, -1, 64, NULL, 0, 1, 0, 'icon-diamond', '2020-10-05 03:18:08', '2020-12-14 21:06:12', NULL),
	(70, 'admin.orderstatus', NULL, 0, -1, 69, NULL, 0, 1, 0, 'icon-diamond', '2020-10-05 03:19:11', '2020-11-09 21:35:59', NULL),
	(71, 'admin_filter_home_page', NULL, 2, 1, 22, NULL, 0, 1, 0, 'fa fa-filter', '2020-10-05 01:12:21', '2022-01-12 16:37:06', '2022-01-12 16:37:06'),
	(73, 'admin_manufacturer_home_page', NULL, 4, 1, 22, NULL, 0, 1, 0, 'fa fa-tags', '2020-10-05 01:11:46', '2022-01-12 16:36:10', '2022-01-12 16:36:10'),
	(74, '', NULL, 40, -1, 0, NULL, 3, 0, 0, NULL, '2020-10-05 04:26:00', '2021-01-16 17:50:25', NULL),
	(75, '/admin/translations', NULL, 2, 1, 64, NULL, 0, 0, 0, 'fa fa-file', '2020-10-05 07:50:40', '2021-01-25 16:55:25', NULL),
	(76, '', NULL, 0, -1, 57, NULL, 3, 0, 0, NULL, '2020-10-06 17:12:57', '2020-10-08 15:40:15', NULL),
	(77, '', NULL, 50, -1, 0, NULL, 3, 0, 0, NULL, '2020-10-06 18:25:13', '2021-01-16 17:50:25', NULL),
	(78, '', NULL, 60, -1, 0, NULL, 3, 0, 0, NULL, '2020-10-06 18:59:32', '2021-01-16 17:50:25', NULL),
	(81, NULL, '/lien-he', 5, 1, 203, NULL, 4, 0, 0, NULL, '2020-10-06 17:58:29', '2022-02-10 11:38:35', NULL),
	(82, '', NULL, 2, -1, 81, NULL, 4, 0, 0, NULL, '2020-10-06 18:00:16', '2021-01-16 17:59:52', NULL),
	(83, '', NULL, 10, -1, 81, NULL, 4, 0, 0, NULL, '2020-10-06 18:02:05', '2021-01-16 17:59:52', NULL),
	(85, '', NULL, 0, -1, 84, NULL, 4, 0, 0, NULL, '2020-10-06 18:04:56', '2021-01-16 17:59:31', NULL),
	(87, '', NULL, 91, -1, 1, NULL, 0, 0, 0, 'icon-cup', '2020-10-07 21:47:14', '2020-10-07 23:00:28', NULL),
	(88, '', NULL, 100, -1, 29, NULL, 0, 0, 0, 'icon-camrecorder', '2020-10-07 16:22:34', '2021-01-26 21:21:54', NULL),
	(90, '', NULL, 5, -1, 57, NULL, 3, 0, 0, NULL, '2020-10-09 02:53:08', '2021-01-16 17:50:25', NULL),
	(91, '', NULL, 10, -1, 57, NULL, 3, 0, 0, NULL, '2020-10-09 02:54:04', '2021-01-16 17:50:25', NULL),
	(92, '', NULL, 5, -1, 40, NULL, 3, 0, 0, NULL, '2020-10-09 02:55:38', '2021-01-16 17:50:25', NULL),
	(93, '', NULL, 10, -1, 40, NULL, 3, 0, 0, NULL, '2020-10-09 02:56:05', '2021-01-16 17:50:25', NULL),
	(94, 'admin.contact.index', NULL, 0, 1, 1, 'contact-view', 0, 0, 0, 'fa fa-question-circle-o', '2020-10-09 07:15:09', '2021-07-26 03:50:24', NULL),
	(95, NULL, '/bo-su-tap/thach-anh-van-3', 3, 1, 168, NULL, 3, 0, 0, NULL, '2020-10-09 17:23:40', '2022-01-21 16:43:45', NULL),
	(96, NULL, '/tin-tuc/tin-tu-bao-chi-7', 0, 1, 166, NULL, 3, 0, 0, NULL, '2020-10-09 17:24:18', '2022-01-18 14:30:54', '2022-01-18 14:30:54'),
	(98, NULL, '/san-pham', 3, 1, 202, NULL, 4, 0, 0, NULL, '2020-10-09 17:26:20', '2022-02-10 11:37:45', NULL),
	(101, NULL, '/tin-tuc/tin-tuc-noi-bat-6', 1, 1, 166, NULL, 3, 0, 0, NULL, '2020-10-15 15:20:16', '2022-01-18 14:31:03', '2022-01-18 14:31:03'),
	(102, NULL, '/trang/chinh-sach-bao-hanh/13', 0, 1, 181, NULL, 4, 0, 0, NULL, '2020-10-15 15:21:00', '2022-02-09 17:49:21', NULL),
	(103, NULL, '/bo-su-tap/thach-anh-natural-2', 2, 1, 168, NULL, 3, 0, 0, NULL, '2020-10-15 15:22:11', '2022-01-21 16:40:12', NULL),
	(104, '', NULL, 11, -1, 81, NULL, 4, 0, 0, NULL, '2020-10-19 23:19:36', '2021-01-16 17:59:52', NULL),
	(105, '', NULL, 12, -1, 81, NULL, 4, 0, 0, NULL, '2020-10-19 23:21:04', '2021-01-16 17:59:52', NULL),
	(106, '', NULL, 13, -1, 81, NULL, 4, 0, 0, NULL, '2020-10-19 23:21:34', '2021-01-16 17:59:52', NULL),
	(107, '', NULL, 14, -1, 81, NULL, 4, 0, 0, NULL, '2020-10-19 23:22:47', '2021-01-16 17:59:52', NULL),
	(108, '', NULL, 15, -1, 84, NULL, 4, 0, 0, NULL, '2020-10-19 23:24:39', '2021-01-16 17:59:31', NULL),
	(109, '', NULL, 16, -1, 84, NULL, 4, 0, 0, NULL, '2020-10-19 23:25:22', '2021-01-16 17:59:31', NULL),
	(110, '', NULL, 17, -1, 84, NULL, 4, 0, 0, NULL, '2020-10-19 23:25:51', '2021-01-16 17:59:31', NULL),
	(111, '', NULL, 18, -1, 84, NULL, 4, 0, 0, NULL, '2020-10-19 23:26:27', '2021-01-16 17:59:31', NULL),
	(112, '', NULL, 19, -1, 84, NULL, 4, 0, 0, NULL, '2020-10-19 23:26:55', '2021-01-16 17:59:32', NULL),
	(113, '', NULL, 20, -1, 84, NULL, 4, 0, 0, NULL, '2020-10-19 23:27:27', '2021-01-16 17:59:32', NULL),
	(114, '', NULL, 21, -1, 84, NULL, 4, 0, 0, NULL, '2020-10-19 23:27:58', '2021-01-16 17:59:32', NULL),
	(116, '', NULL, 0, -1, 8, NULL, 0, 0, 0, NULL, '2020-10-24 18:46:27', '2020-10-24 18:46:42', NULL),
	(117, '', NULL, 0, -1, 69, NULL, 0, 0, 0, 'icon-directions', '2020-11-09 21:36:42', '2020-11-24 17:39:38', NULL),
	(118, '', NULL, 11, -1, 57, NULL, 3, 0, 0, NULL, '2020-11-09 22:05:12', '2021-01-16 17:50:25', NULL),
	(119, '', NULL, 12, -1, 57, NULL, 3, 0, 0, NULL, '2020-11-09 22:06:11', '2021-01-16 17:50:25', NULL),
	(120, '', NULL, 11, -1, 40, NULL, 3, 0, 0, NULL, '2020-11-09 22:07:17', '2021-01-16 17:50:25', NULL),
	(121, '', NULL, 12, -1, 40, NULL, 3, 0, 0, NULL, '2020-11-09 22:07:49', '2021-01-16 17:50:25', NULL),
	(122, '', NULL, 1, -1, 81, NULL, 4, 0, 0, NULL, '2020-11-18 20:50:05', '2021-01-16 17:59:52', NULL),
	(125, 'admin_category_home_page', NULL, 0, 1, 29, NULL, 0, 0, 0, 'fa fa-cubes', '2021-01-13 21:25:30', '2021-03-09 21:46:48', NULL),
	(126, NULL, '/bo-suu-tap', 1, 1, 178, NULL, 4, 0, 0, NULL, '2021-01-19 16:56:02', '2022-02-09 14:49:25', '2022-02-09 14:49:25'),
	(130, '', NULL, 0, -1, 89, NULL, 0, 0, 0, NULL, '2021-01-25 20:42:51', '2021-01-25 20:43:27', NULL),
	(140, NULL, NULL, 1, 1, 61, NULL, 0, 1, 1, NULL, '2021-01-30 12:14:32', '2021-01-30 12:14:32', NULL),
	(141, NULL, NULL, 1, 1, 61, NULL, 0, 1, 1, NULL, '2021-01-30 12:15:09', '2021-01-30 12:15:09', NULL),
	(142, NULL, NULL, 1, 1, 61, NULL, 0, 1, 1, NULL, '2021-01-30 12:18:12', '2021-01-30 12:18:12', NULL),
	(156, 'get_admin_dashboard_page', NULL, 0, 1, 0, NULL, 0, 0, 0, 'fa fa-tachometer', '2021-03-17 16:34:19', '2021-03-17 16:35:25', NULL),
	(157, 'admin_product_home_page', NULL, 0, 1, 22, NULL, 0, NULL, NULL, 'fa fa-cubes', '2021-07-03 10:36:53', '2021-07-03 10:36:53', NULL),
	(159, 'admin_authorization_get_all_perms', NULL, 2, 1, 67, NULL, 0, NULL, NULL, 'fa fa-cubes', '2021-07-08 10:10:54', '2021-07-08 10:10:54', NULL),
	(160, 'get_user_home_page', NULL, 0, 1, 67, NULL, 0, 0, 0, 'fa fa-list', '2021-07-08 10:11:56', '2021-07-08 10:13:32', NULL),
	(164, 'admin_collection_home_page', NULL, 1, 1, 22, NULL, 0, 0, 0, 'fa fa-cube', '2021-07-21 18:40:17', '2021-07-21 18:46:53', NULL),
	(165, NULL, '/', 0, 1, 0, NULL, 3, 0, 0, 'icons icon-truck', '2021-07-25 17:28:46', '2021-11-26 14:51:25', NULL),
	(166, NULL, '/tin-tuc', 6, 1, 0, NULL, 3, 0, 0, 'icons icon-ticket', '2021-07-25 17:29:31', '2022-01-20 11:42:36', '2022-01-20 11:42:36'),
	(167, NULL, '/bo-suu-tap', 3, 1, 0, NULL, 3, 0, 0, 'icons icon-flash', '2021-07-25 17:29:54', '2022-01-20 11:42:26', '2022-01-20 11:42:26'),
	(168, NULL, '/san-pham', 2, 1, 0, NULL, 3, 0, 0, 'icons icon-shipper', '2021-07-25 17:30:08', '2021-11-26 10:08:38', NULL),
	(169, 'web.do-kham.index', NULL, 2, 1, 0, NULL, 3, 0, 0, 'icons icon-gift', '2021-07-25 17:30:25', '2022-01-20 11:42:31', '2022-01-20 11:42:31'),
	(170, NULL, '/ung-dung', 2, 1, 202, NULL, 4, 0, 0, NULL, '2021-07-26 02:32:58', '2022-02-10 11:36:51', NULL),
	(171, NULL, NULL, 1, -1, 64, NULL, 0, 0, 0, 'fa fa-map-marker', '2021-07-26 03:29:01', '2021-07-26 03:29:55', NULL),
	(172, 'location.city_list', NULL, 1, 1, 171, NULL, 0, NULL, NULL, 'fa fa-check-square-o', '2021-07-26 03:31:49', '2021-07-26 03:31:49', NULL),
	(173, 'location.district_list', NULL, 0, 1, 171, NULL, 0, NULL, NULL, 'fa fa-check-square-o', '2021-07-26 03:32:42', '2021-07-26 03:32:42', NULL),
	(174, 'location.ward_list', NULL, 2, 1, 171, NULL, 0, NULL, NULL, 'fa fa-check-square-o', '2021-07-26 03:33:13', '2021-07-26 03:33:13', NULL),
	(175, 'admin_staticpage_home_page', NULL, 2, 1, 29, NULL, 0, NULL, NULL, 'fa fa-book', '2021-07-26 03:56:19', '2021-07-26 03:56:19', NULL),
	(176, NULL, '/bo-su-tap/thach-anh-pha-le-1', 1, 1, 168, NULL, 3, 0, 0, 'icons icon-rocket', '2021-07-29 23:59:13', '2022-01-21 16:39:39', NULL),
	(177, NULL, '/lien-he', 5, 1, 0, NULL, 3, 0, 0, 'icons icon-circle', '2021-07-29 23:59:54', '2021-11-26 10:12:02', NULL),
	(178, NULL, NULL, 0, 1, 0, NULL, 4, 0, 0, NULL, '2021-07-31 18:24:34', '2021-10-29 11:34:22', NULL),
	(179, NULL, '/ve-chung-toi', 0, 1, 178, NULL, 4, 0, 0, NULL, '2021-07-31 18:25:39', '2022-02-10 11:25:08', NULL),
	(180, NULL, '/tin-tuc', 4, 1, 178, NULL, 4, 0, 0, NULL, '2021-07-31 18:25:59', '2022-01-24 13:57:00', '2022-01-24 13:57:00'),
	(181, NULL, NULL, 1, 1, 0, NULL, 4, 0, 0, NULL, '2021-07-31 18:26:54', '2021-10-29 11:33:33', NULL),
	(182, NULL, '/trang/chinh-sach-giao-hang/11', 1, 1, 181, NULL, 4, 0, 0, NULL, '2021-07-31 18:28:20', '2022-02-09 17:49:35', NULL),
	(183, NULL, '/trang/trung-tam-tro-giup-10', 2, 1, 181, NULL, 4, 0, 0, NULL, '2021-07-31 18:28:40', '2022-02-09 15:37:08', '2022-02-09 15:37:08'),
	(184, NULL, '/trang/ho-tro-mua-hang-22', 3, 1, 181, NULL, 4, 0, 0, NULL, '2021-07-31 18:29:03', '2022-02-09 15:37:12', '2022-02-09 15:37:12'),
	(191, NULL, '/tin-tuc/su-kien-noi-bat-13', 0, 1, 166, NULL, 3, 0, 0, 'fa fa-check-square-o', '2021-09-20 01:11:10', '2022-01-18 14:30:59', '2022-01-18 14:30:59'),
	(195, 'web.Staticpage.index', '/ve-chung-toi', 1, 1, 0, NULL, 3, 0, 0, 'icons icon-fire-small', '2021-10-06 11:24:36', '2022-01-20 11:41:39', NULL),
	(199, 'web.news.index', NULL, 4, 1, 0, NULL, 3, 0, 0, NULL, '2021-10-26 09:39:39', '2022-01-20 11:51:05', NULL),
	(200, 'admin_partners_home_page', NULL, 4, 1, 29, NULL, 0, NULL, NULL, 'fa fa-plane', '2021-11-29 15:10:34', '2021-11-29 15:10:34', NULL),
	(201, 'admin.appspace.index', NULL, 0, 1, 22, NULL, 0, NULL, NULL, NULL, '2022-01-10 10:16:23', '2022-01-10 10:18:36', '2022-01-10 10:18:36'),
	(202, NULL, NULL, 2, 1, 0, NULL, 4, 0, 0, NULL, '2022-01-12 14:53:54', '2022-01-20 15:35:19', NULL),
	(203, NULL, NULL, 4, 1, 0, NULL, 4, 0, 0, NULL, '2022-01-12 14:54:33', '2022-01-12 15:00:35', NULL),
	(204, 'web.Apication.index', NULL, 3, 1, 0, NULL, 3, 0, 0, NULL, '2022-01-20 11:44:14', '2022-01-20 15:27:47', NULL),
	(205, 'admin_comment_home_page', NULL, NULL, 1, 22, NULL, 0, 0, 0, NULL, '2022-01-20 11:54:34', '2022-01-20 11:55:49', NULL),
	(206, NULL, NULL, NULL, 1, 0, NULL, 3, NULL, NULL, NULL, '2022-01-21 16:32:55', '2022-01-21 16:36:57', '2022-01-21 16:36:57'),
	(207, NULL, '/bo-su-tap/thach-anh-classic-5', 4, 1, 168, NULL, 3, 0, 0, NULL, '2022-01-21 16:35:28', '2022-01-21 16:58:42', NULL),
	(208, NULL, NULL, NULL, 1, 168, NULL, 3, NULL, NULL, NULL, '2022-01-21 16:36:34', '2022-01-21 16:37:05', '2022-01-21 16:37:05'),
	(209, NULL, NULL, 5, 1, 209, NULL, 3, 0, 0, NULL, '2022-01-21 16:38:27', '2022-02-10 17:17:46', NULL),
	(210, NULL, '/ung-dung/62/khu-vuc-nha-bep', NULL, 1, 204, NULL, 3, 0, 0, NULL, '2022-01-26 19:38:06', '2022-02-10 13:54:59', NULL),
	(211, NULL, '/ung-dung/3/khu-vuc-nha-tam', 2, 1, 204, NULL, 3, 0, 0, NULL, '2022-01-26 19:39:23', '2022-02-10 11:08:57', NULL),
	(212, NULL, '/ung-dung/2/khu-vuc-phong-khach', 3, 1, 204, NULL, 3, 0, 0, NULL, '2022-01-26 19:40:16', '2022-02-10 11:06:11', NULL),
	(213, NULL, '/ung-dung/65/khu-vuc-van-phong', 4, 1, 204, NULL, 3, 0, 0, NULL, '2022-01-26 19:41:28', '2022-02-10 13:55:51', NULL),
	(214, NULL, '/ung-dung/66/cac-san-pham-khac', 5, 1, 204, NULL, 3, 0, 0, NULL, '2022-01-26 19:42:11', '2022-02-10 13:56:11', NULL),
	(215, NULL, '/trang/chinh-sach-bao-mat/37', NULL, 1, 0, NULL, 0, NULL, NULL, NULL, '2022-02-10 14:38:57', '2022-02-10 15:39:55', '2022-02-10 15:39:55'),
	(216, NULL, '/trang/chinh-sach-bao-mat/37', NULL, 1, 181, NULL, 4, NULL, NULL, NULL, '2022-02-10 14:40:11', '2022-02-10 14:41:16', '2022-02-10 14:41:16'),
	(217, NULL, NULL, NULL, 1, 0, NULL, 0, 0, 0, NULL, '2022-02-11 00:04:24', '2022-02-11 00:04:52', '2022-02-11 00:04:52');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- Dumping structure for table centurystone.menu_description
CREATE TABLE IF NOT EXISTS `menu_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `link` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(600) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `menu_description_menu_id_IDX` (`menu_id`) USING BTREE,
  KEY `menu_description_language_id_IDX` (`language_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=388 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.menu_description: ~278 rows (approximately)
/*!40000 ALTER TABLE `menu_description` DISABLE KEYS */;
INSERT INTO `menu_description` (`id`, `menu_id`, `language_id`, `name`, `short_description`, `image`, `link`, `description`, `meta_title`, `meta_description`, `meta_keyword`, `created_at`, `updated_at`) VALUES
	(1, 22, 1, 'Sản phẩm', NULL, NULL, 'admin.product', NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 22, 2, 'Products', NULL, NULL, 'admin.product', NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 73, 1, 'Thương hiệu', NULL, NULL, 'admin.manufacturer', NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 73, 2, 'Manufacturer', NULL, NULL, 'admin.manufacturer', NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 71, 1, 'Bộ lọc', NULL, NULL, 'admin.filter_group', NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 71, 2, 'Filters', NULL, NULL, 'admin.filter_group', NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 62, 1, 'Tùy chọn', NULL, NULL, 'admin.option', NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 62, 2, 'Options', NULL, NULL, 'admin.option', NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 6, 1, 'Danh Mục', NULL, NULL, 'admin.category', NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 6, 2, 'Categories', NULL, NULL, 'admin.category', NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 28, 1, 'Sản phẩm', 'áđâsd', NULL, 'admin.product', NULL, NULL, NULL, NULL, NULL, NULL),
	(12, 28, 2, 'Products', NULL, NULL, 'admin.product', NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 61, 1, 'Thuộc tính', NULL, NULL, 'admin.attribute', NULL, NULL, NULL, NULL, NULL, NULL),
	(14, 61, 2, 'Attributes', NULL, NULL, 'admin.attribute', NULL, NULL, NULL, NULL, NULL, NULL),
	(15, 59, 1, 'Thuộc tính sản phẩm', NULL, NULL, 'admin.attribute', NULL, NULL, NULL, NULL, NULL, NULL),
	(16, 59, 2, 'Attributes', NULL, NULL, 'admin.attribute', NULL, NULL, NULL, NULL, NULL, NULL),
	(17, 60, 1, 'Nhóm thuộc tính', NULL, NULL, 'admin.attribute_group', NULL, NULL, NULL, NULL, NULL, NULL),
	(18, 60, 2, 'Attribute Groups', NULL, NULL, 'admin.attribute_group', NULL, NULL, NULL, NULL, NULL, NULL),
	(19, 1, 1, 'Quản trị', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(20, 1, 2, 'Management', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(21, 4, 1, 'Banner', NULL, NULL, 'admin.banner', NULL, NULL, NULL, NULL, NULL, NULL),
	(22, 4, 2, 'Banner', NULL, NULL, 'admin.banner', NULL, NULL, NULL, NULL, NULL, NULL),
	(23, 64, 1, 'Hệ thống', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(24, 64, 2, 'System', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(25, 65, 1, 'Cấu hình chung', NULL, NULL, 'admin.config', NULL, NULL, NULL, NULL, NULL, NULL),
	(26, 65, 2, 'General Settings', NULL, NULL, 'admin.config', NULL, NULL, NULL, NULL, NULL, NULL),
	(27, 66, 1, 'Menu', NULL, NULL, 'admin.menu', NULL, NULL, NULL, NULL, NULL, NULL),
	(28, 66, 2, 'Menu', NULL, NULL, 'admin.menu', NULL, NULL, NULL, NULL, NULL, NULL),
	(29, 67, 1, 'Quản trị viên', NULL, NULL, 'admin.user', NULL, NULL, NULL, NULL, NULL, NULL),
	(30, 67, 2, 'Quản trị viên', NULL, NULL, 'admin.user', NULL, NULL, NULL, NULL, NULL, NULL),
	(31, 68, 1, 'Nhóm quyền', NULL, NULL, 'admin.role', NULL, NULL, NULL, NULL, NULL, NULL),
	(32, 68, 2, 'Roles', NULL, NULL, 'admin.role', NULL, NULL, NULL, NULL, NULL, NULL),
	(33, 69, 1, 'Thiết lập', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(34, 69, 2, 'Localisation', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(35, 63, 1, 'Trạng thái tồn kho', NULL, NULL, 'admin.statustock', NULL, NULL, NULL, NULL, NULL, NULL),
	(36, 63, 2, 'Stock statuses', NULL, NULL, 'admin.statustock', NULL, NULL, NULL, NULL, NULL, NULL),
	(37, 70, 1, 'Trạng thái đơn hàng', NULL, NULL, 'admin.orderstatus', NULL, NULL, NULL, NULL, NULL, NULL),
	(38, 70, 2, 'Order Statuses', NULL, NULL, 'admin.orderstatus', NULL, NULL, NULL, NULL, NULL, NULL),
	(41, 54, 1, 'Dashboard', NULL, NULL, 'admin.dashboard', NULL, NULL, NULL, NULL, NULL, NULL),
	(42, 54, 2, 'Dashboard', NULL, NULL, 'admin.dashboard', NULL, NULL, NULL, NULL, NULL, NULL),
	(43, 29, 1, 'Nội dung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(44, 29, 2, 'Content', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(47, 3, 1, 'Tin tức', NULL, NULL, 'admin.news', NULL, NULL, NULL, NULL, NULL, NULL),
	(48, 3, 2, 'News', NULL, NULL, 'admin.news', NULL, NULL, NULL, NULL, NULL, NULL),
	(49, 53, 1, 'Đơn hàng', NULL, NULL, 'admin.order', NULL, NULL, NULL, NULL, NULL, NULL),
	(50, 53, 2, 'Orders', NULL, NULL, 'admin.order', NULL, NULL, NULL, NULL, NULL, NULL),
	(51, 55, 1, 'Ngân hàng', NULL, NULL, 'admin.bank', NULL, NULL, NULL, NULL, NULL, NULL),
	(52, 55, 2, 'Banks', NULL, NULL, 'admin.bank', NULL, NULL, NULL, NULL, NULL, NULL),
	(53, 18, 1, 'Subscriber', NULL, NULL, 'admin.subscriber', NULL, NULL, NULL, NULL, NULL, NULL),
	(54, 18, 2, 'Subscriber', NULL, NULL, 'admin.subscriber', NULL, NULL, NULL, NULL, NULL, NULL),
	(57, 30, 1, 'Nhóm Khách hàng', NULL, NULL, 'admin.customergroup', NULL, NULL, NULL, NULL, NULL, NULL),
	(58, 30, 2, 'Customer Groups', NULL, NULL, 'admin.customergroup', NULL, NULL, NULL, NULL, NULL, NULL),
	(61, 52, 1, 'Mã Giảm giá', NULL, NULL, 'admin.coupon', NULL, NULL, NULL, NULL, NULL, NULL),
	(62, 52, 2, 'Coupons', NULL, NULL, 'admin.coupon', NULL, NULL, NULL, NULL, NULL, NULL),
	(63, 24, 1, 'Liên hệ', NULL, NULL, 'admin.contact', NULL, NULL, NULL, NULL, NULL, NULL),
	(64, 24, 2, 'Contacts', NULL, NULL, 'admin.contact', NULL, NULL, NULL, NULL, NULL, NULL),
	(65, 19, 1, 'Giới thiệu', NULL, NULL, '/gioi-thieu-st8', NULL, NULL, NULL, NULL, NULL, NULL),
	(66, 19, 2, 'Introduction', NULL, NULL, '/gioi-thieu-st8', NULL, NULL, NULL, NULL, NULL, NULL),
	(67, 57, 1, 'SNIMAY CABINET', NULL, NULL, '/snimay-cabinet-c91', NULL, NULL, NULL, NULL, NULL, NULL),
	(68, 57, 2, 'SNIMAY CABINET', NULL, NULL, '/snimay-cabinet-c91', NULL, NULL, NULL, NULL, NULL, NULL),
	(69, 40, 1, 'Nội thất The One', NULL, NULL, '/noi-that-the-one-c93', NULL, NULL, NULL, NULL, NULL, NULL),
	(70, 40, 2, 'The One Interior', NULL, NULL, '/noi-that-the-one-c93', NULL, NULL, NULL, NULL, NULL, NULL),
	(71, 39, 1, 'Trang Trí & Gia dụng', NULL, NULL, '/trang-tri-va-do-gia-dung-c109', NULL, NULL, NULL, NULL, NULL, NULL),
	(72, 39, 2, 'Decor & Home appliances', NULL, NULL, '/trang-tri-va-do-gia-dung-c109', NULL, NULL, NULL, NULL, NULL, NULL),
	(75, 58, 1, 'Giới thiệu', NULL, NULL, '/gioi-thieu-st8', NULL, NULL, NULL, NULL, NULL, NULL),
	(76, 58, 2, 'Guarantee Policy', NULL, NULL, '/chinh-sach', NULL, NULL, NULL, NULL, NULL, NULL),
	(77, 56, 1, 'Thông tin liên hệ', NULL, NULL, '/contact', NULL, NULL, NULL, NULL, NULL, NULL),
	(78, 56, 2, 'Care and incentives', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(79, 43, 1, 'Hướng dẫn mua hàng', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(80, 43, 2, 'Privacy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(81, 15, 1, 'Chương trình khuyến mãi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(82, 15, 2, 'Services', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(83, 74, 1, 'Chính sách', NULL, NULL, '/chinh-sach', NULL, NULL, NULL, NULL, NULL, NULL),
	(84, 74, 2, 'Policy', NULL, NULL, '/chinh-sach', NULL, NULL, NULL, NULL, NULL, NULL),
	(85, 75, 1, 'Dịch thuật', NULL, NULL, '/admin/translations', NULL, NULL, NULL, NULL, NULL, NULL),
	(86, 75, 2, 'Dịch thuật', NULL, NULL, '/admin/translations', NULL, NULL, NULL, NULL, NULL, NULL),
	(87, 76, 1, 'HIện đại', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(88, 76, 2, 'Modern', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(89, 77, 1, 'Liên hệ', NULL, NULL, 'contact', NULL, NULL, NULL, NULL, NULL, NULL),
	(90, 77, 2, 'Contact us', NULL, NULL, 'contact', NULL, NULL, NULL, NULL, NULL, NULL),
	(91, 78, 1, 'Tạp chí', NULL, NULL, 'news.list', NULL, NULL, NULL, NULL, NULL, NULL),
	(92, 78, 2, 'Magazine', NULL, NULL, 'news.list', NULL, NULL, NULL, NULL, NULL, NULL),
	(93, 79, 1, 'Features', 'Features', NULL, 'admin.feature', NULL, NULL, NULL, NULL, NULL, NULL),
	(94, 79, 2, 'Features', 'Features', NULL, 'admin.feature', NULL, NULL, NULL, NULL, NULL, NULL),
	(97, 81, 1, 'Liên hệ', NULL, NULL, 'product', NULL, NULL, NULL, NULL, NULL, NULL),
	(98, 81, 2, 'Contact Us', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(99, 82, 1, 'Chính sách thanh toán', NULL, NULL, '/chinh-sach-thanh-toan-st9', NULL, NULL, NULL, NULL, NULL, NULL),
	(100, 82, 2, 'Contact Infomartion', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(101, 83, 1, 'Chính sách bán hàng', NULL, NULL, '/chinh-sach-ban-hang-st10', NULL, NULL, NULL, NULL, NULL, NULL),
	(102, 83, 2, 'Shopping Guide', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(105, 85, 1, 'Trang chủ', NULL, NULL, '/', NULL, NULL, NULL, NULL, NULL, NULL),
	(106, 85, 2, 'Home', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(109, 87, 1, 'Video', NULL, NULL, 'admin.video', NULL, NULL, NULL, NULL, NULL, NULL),
	(110, 87, 2, 'Video', NULL, NULL, 'admin.video', NULL, NULL, NULL, NULL, NULL, NULL),
	(111, 88, 1, 'Video', NULL, NULL, 'admin.video', NULL, NULL, NULL, NULL, NULL, NULL),
	(112, 88, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(115, 90, 1, 'Luxury', NULL, NULL, '/snimay-cabinet-c91?type=1', NULL, NULL, NULL, NULL, NULL, NULL),
	(116, 90, 2, 'Luxury', NULL, NULL, '/snimay-cabinet-c91?type=1', NULL, NULL, NULL, NULL, NULL, NULL),
	(117, 91, 1, 'Hiện đại', NULL, NULL, '/snimay-cabinet-c91?type=0', NULL, NULL, NULL, NULL, NULL, NULL),
	(118, 91, 2, 'Modern', NULL, NULL, '/snimay-cabinet-c91?type=0', NULL, NULL, NULL, NULL, NULL, NULL),
	(119, 92, 1, 'Luxury', NULL, NULL, '/noi-that-the-one-c93?type=1', NULL, NULL, NULL, NULL, NULL, NULL),
	(120, 92, 2, 'Luxury', NULL, NULL, '/noi-that-the-one-c93?type=1', NULL, NULL, NULL, NULL, NULL, NULL),
	(121, 93, 1, 'Hiện đại', NULL, NULL, '/noi-that-the-one-c93?type=0', NULL, NULL, NULL, NULL, NULL, NULL),
	(122, 93, 2, 'Modern', NULL, NULL, '/noi-that-the-one-c93?type=0', NULL, NULL, NULL, NULL, NULL, NULL),
	(123, 94, 1, 'Liên hệ', NULL, NULL, 'admin.contact', NULL, NULL, NULL, NULL, NULL, NULL),
	(124, 94, 2, 'Contacts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(125, 95, 1, 'Bộ sưu tập thạch anh Vân', NULL, NULL, '/shopping-guide-st5', NULL, NULL, NULL, NULL, NULL, NULL),
	(126, 95, 2, 'Quartz collection', NULL, NULL, '/shopping-guide-st5', NULL, NULL, NULL, NULL, NULL, NULL),
	(127, 96, 1, 'Tin từ báo chí', NULL, NULL, '/shopping-guide-st9', NULL, NULL, NULL, NULL, NULL, NULL),
	(128, 96, 2, 'News from press', NULL, NULL, '/shopping-guide-st9', NULL, NULL, NULL, NULL, NULL, NULL),
	(131, 98, 1, 'Sản phẩm', NULL, NULL, '/shopping-guide-st2', NULL, NULL, NULL, NULL, NULL, NULL),
	(132, 98, 2, 'Product', NULL, NULL, '/shopping-guide-st2', NULL, NULL, NULL, NULL, NULL, NULL),
	(137, 101, 1, 'Tin tức nổi bật', NULL, NULL, '/chinh-sach-bao-hanh-st18', NULL, NULL, NULL, NULL, NULL, NULL),
	(138, 101, 2, 'Featured news', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(139, 102, 1, 'Chính sách bảo hành', NULL, NULL, 'chinh-sach-va-dieu-khoan-st4', NULL, NULL, NULL, NULL, NULL, NULL),
	(140, 102, 2, 'Warranty Policy', NULL, NULL, '/chinh-sach-bao-mat-st19', NULL, NULL, NULL, NULL, NULL, NULL),
	(141, 103, 1, 'Bộ sưu tập thạch anh Natural', NULL, NULL, '/dieu-khoan-dich-vu-st22', NULL, NULL, NULL, NULL, NULL, NULL),
	(142, 103, 2, 'Natural Quartz Collection', NULL, NULL, '/dieu-khoan-dich-vu-st22', NULL, NULL, NULL, NULL, NULL, NULL),
	(143, 104, 1, 'Chính sách giao hàng', NULL, NULL, '/chinh-sach-giao-hang-st2', NULL, NULL, NULL, NULL, NULL, NULL),
	(144, 104, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(145, 105, 1, 'Chính sách đổi trả', NULL, NULL, '/chinh-sach-doi-tra-st17', NULL, NULL, NULL, NULL, NULL, NULL),
	(146, 105, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(147, 106, 1, 'Chính sách bảo hành', NULL, NULL, '/chinh-sach-bao-hanh-st18', NULL, NULL, NULL, NULL, NULL, NULL),
	(148, 106, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(149, 107, 1, 'Chính sách bảo mật', NULL, NULL, '/chinh-sach-bao-mat-st19', NULL, NULL, NULL, NULL, NULL, NULL),
	(150, 107, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(151, 108, 1, 'Giới thiệu', NULL, NULL, '/gioi-thieu-st8', NULL, NULL, NULL, NULL, NULL, NULL),
	(152, 108, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(153, 109, 1, 'SNIMAY CABINE', NULL, NULL, '/snimay-cabinet-c91', NULL, NULL, NULL, NULL, NULL, NULL),
	(154, 109, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(155, 110, 1, 'NỘI THẤT THE ONE', NULL, NULL, '/noi-that-the-one-c93', NULL, NULL, NULL, NULL, NULL, NULL),
	(156, 110, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(157, 111, 1, 'TRANG TRÍ VÀ GIA DỤNG', NULL, NULL, '/trang-tri-va-do-gia-dung-c109', NULL, NULL, NULL, NULL, NULL, NULL),
	(158, 111, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(159, 112, 1, 'Chính sách', NULL, NULL, '/chinh-sach', NULL, NULL, NULL, NULL, NULL, NULL),
	(160, 112, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(161, 113, 1, 'Liên hệ', NULL, NULL, '/contact', NULL, NULL, NULL, NULL, NULL, NULL),
	(162, 113, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(163, 114, 1, 'Tạp chí', NULL, NULL, '/tin-tuc', NULL, NULL, NULL, NULL, NULL, NULL),
	(164, 114, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(167, 116, 1, 'Vận chuyển lắp đặt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(168, 116, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(169, 117, 1, 'Phân loại sản phẩm', 'Phân loại sản phẩm', NULL, 'admin.producttype', NULL, NULL, NULL, NULL, NULL, NULL),
	(170, 117, 2, 'Product Type', 'Product Type', NULL, 'admin.producttype', NULL, NULL, NULL, NULL, NULL, NULL),
	(171, 118, 1, 'Classical', NULL, NULL, '/snimay-cabinet-c91?type=2', NULL, NULL, NULL, NULL, NULL, NULL),
	(172, 118, 2, 'Classical', NULL, NULL, '/snimay-cabinet-c91?type=2', NULL, NULL, NULL, NULL, NULL, NULL),
	(173, 119, 1, 'Châu Âu tối giản', NULL, NULL, '/snimay-cabinet-c91?type=3', NULL, NULL, NULL, NULL, NULL, NULL),
	(174, 119, 2, 'Châu Âu tối giản', NULL, NULL, '/snimay-cabinet-c91?type=3', NULL, NULL, NULL, NULL, NULL, NULL),
	(175, 120, 1, 'Classical', NULL, NULL, '/snimay-cabinet-c91?type=2', NULL, NULL, NULL, NULL, NULL, NULL),
	(176, 120, 2, 'Classical', NULL, NULL, '/snimay-cabinet-c91?type=2', NULL, NULL, NULL, NULL, NULL, NULL),
	(177, 121, 1, 'Châu Âu tối giản', NULL, NULL, '/snimay-cabinet-c91?type=3', NULL, NULL, NULL, NULL, NULL, NULL),
	(178, 121, 2, 'Châu Âu tối giản', NULL, NULL, '/snimay-cabinet-c91?type=3', NULL, NULL, NULL, NULL, NULL, NULL),
	(179, 122, 1, 'Điều khoản dịch vụ', NULL, NULL, 'dieu-khoan-dich-vu-st22', NULL, NULL, NULL, NULL, NULL, NULL),
	(180, 122, 2, 'Điều khoản dịch vụ', NULL, NULL, 'dieu-khoan-dich-vu-st22', NULL, NULL, NULL, NULL, NULL, NULL),
	(187, 125, 1, 'Danh mục', NULL, NULL, 'admin.category', NULL, NULL, NULL, NULL, NULL, NULL),
	(188, 126, 1, 'Bộ sưu tập', NULL, NULL, '/', NULL, NULL, NULL, NULL, NULL, NULL),
	(191, 129, 1, NULL, NULL, '-1611281008.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(192, 130, 1, 'menu tét', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(194, 141, 1, 'Menu ABC', NULL, NULL, NULL, '212', '1212', 'ÁDASD', 'ÁDASD', '2021-01-30 05:15:09', '2021-01-30 05:15:09'),
	(195, 141, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-30 05:15:09', '2021-01-30 05:15:09'),
	(196, 142, 1, 'Menu ABC', NULL, NULL, NULL, '212', '1212', 'ÁDASD', 'ÁDASD', '2021-01-30 05:18:12', '2021-01-30 05:18:12'),
	(197, 142, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-30 05:18:12', '2021-01-30 05:18:12'),
	(226, 156, 1, 'Bảng điều khiển', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 09:34:20', '2021-03-17 09:34:20'),
	(227, 156, 2, 'Dashboard', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 09:34:20', '2021-03-17 09:34:20'),
	(228, 157, 1, 'Danh sách', NULL, NULL, NULL, 'Danh sách', 'Danh sách', 'Danh sách', NULL, '2021-07-03 10:36:53', '2021-07-03 10:36:53'),
	(229, 157, 2, 'Danh sách', NULL, NULL, NULL, 'Danh sách', 'Danh sách', 'Danh sách', NULL, '2021-07-03 10:36:53', '2021-07-03 10:36:53'),
	(232, 159, 1, 'Quyền', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-08 10:10:54', '2021-07-08 10:10:54'),
	(233, 159, 2, 'Permissions', NULL, NULL, NULL, 'Permissions', NULL, NULL, NULL, '2021-07-08 10:10:54', '2021-07-08 10:10:54'),
	(234, 160, 1, 'Danh sách', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-08 10:11:56', '2021-07-08 10:11:56'),
	(235, 160, 2, 'List Customers', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-08 10:11:56', '2021-07-08 10:11:56'),
	(241, 163, 2, 'Tag', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-12 14:35:35', '2021-07-12 14:35:35'),
	(242, 164, 1, 'Bộ sưu tập', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-21 18:40:17', '2021-07-21 18:40:17'),
	(243, 164, 2, 'Collection', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-21 18:40:17', '2021-07-21 18:40:17'),
	(244, 165, 1, 'Trang chủ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-25 17:28:46', '2021-07-25 17:28:46'),
	(245, 165, 2, 'Home', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-25 17:28:46', '2021-07-25 17:28:46'),
	(246, 166, 1, 'Tin tức', NULL, NULL, NULL, 'TIN TỨC', NULL, NULL, NULL, '2021-07-25 17:29:31', '2021-07-25 17:29:31'),
	(247, 166, 2, 'News', NULL, NULL, NULL, 'News', NULL, NULL, NULL, '2021-07-25 17:29:31', '2021-07-25 17:29:31'),
	(248, 167, 1, 'Bộ sưu tập', NULL, NULL, NULL, 'Bộ sưu tập', NULL, NULL, NULL, '2021-07-25 17:29:54', '2021-07-25 17:29:54'),
	(249, 167, 2, 'Collection', NULL, NULL, NULL, 'Collection', NULL, NULL, NULL, '2021-07-25 17:29:54', '2021-07-25 17:29:54'),
	(250, 168, 1, 'Sản phẩm', NULL, NULL, NULL, 'Sản phẩm', NULL, NULL, NULL, '2021-07-25 17:30:09', '2021-07-25 17:30:09'),
	(251, 168, 2, 'Product', NULL, NULL, NULL, 'Product', NULL, NULL, NULL, '2021-07-25 17:30:09', '2021-07-25 17:30:09'),
	(252, 169, 1, 'Đo khám', NULL, NULL, NULL, 'Đo khám', NULL, NULL, NULL, '2021-07-25 17:30:25', '2021-07-25 17:30:25'),
	(253, 169, 2, 'Examination', NULL, NULL, NULL, 'Examination', NULL, NULL, NULL, '2021-07-25 17:30:25', '2021-07-25 17:30:25'),
	(254, 170, 1, 'Ảnh Ứng dụng', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-26 02:32:58', '2021-07-26 02:32:58'),
	(255, 170, 2, 'Photos Apps', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-26 02:32:58', '2021-07-26 02:32:58'),
	(256, 126, 2, 'Collection', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(257, 171, 1, 'Location', NULL, NULL, NULL, 'Location', NULL, NULL, NULL, '2021-07-26 03:29:01', '2021-07-26 03:29:01'),
	(258, 171, 2, 'Location', NULL, NULL, NULL, 'Location', NULL, NULL, NULL, '2021-07-26 03:29:01', '2021-07-26 03:29:01'),
	(259, 172, 1, 'Tỉnh/thành', NULL, NULL, NULL, 'Tỉnh/thành', NULL, NULL, NULL, '2021-07-26 03:31:49', '2021-07-26 03:31:49'),
	(260, 172, 2, 'Tỉnh/thành', NULL, NULL, NULL, 'Tỉnh/thành', NULL, NULL, NULL, '2021-07-26 03:31:49', '2021-07-26 03:31:49'),
	(261, 173, 1, 'Quận/huyện', NULL, NULL, NULL, 'Quận/huyện', NULL, NULL, NULL, '2021-07-26 03:32:42', '2021-07-26 03:32:42'),
	(262, 173, 2, 'Quận/huyện', NULL, NULL, NULL, 'Quận/huyện', NULL, NULL, NULL, '2021-07-26 03:32:42', '2021-07-26 03:32:42'),
	(263, 174, 1, 'Xã/phường', NULL, NULL, NULL, 'Xã/phường', NULL, NULL, NULL, '2021-07-26 03:33:13', '2021-07-26 03:33:13'),
	(264, 174, 2, 'Xã/phường', NULL, NULL, NULL, 'Xã/phường', NULL, NULL, NULL, '2021-07-26 03:33:13', '2021-07-26 03:33:13'),
	(265, 175, 1, 'Trang tĩnh', NULL, NULL, NULL, 'Static Page', NULL, NULL, NULL, '2021-07-26 03:56:19', '2021-07-26 03:56:19'),
	(266, 175, 2, 'Static Page', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-26 03:56:19', '2021-07-26 03:56:19'),
	(267, 176, 1, 'Bộ sưu tập thạch anh Pha lê', NULL, NULL, NULL, 'Bộ sưu tập thạch anh pha lê', NULL, NULL, NULL, '2021-07-29 23:59:13', '2021-07-29 23:59:13'),
	(268, 176, 2, 'Quartz Crystal Collection', NULL, NULL, NULL, 'Quartz Crystal Collection', NULL, NULL, NULL, '2021-07-29 23:59:13', '2021-07-29 23:59:13'),
	(269, 177, 1, 'Liên hệ', NULL, NULL, NULL, 'Liên hệ', NULL, NULL, NULL, '2021-07-29 23:59:54', '2021-07-29 23:59:54'),
	(270, 177, 2, 'Contact', NULL, NULL, NULL, 'Contact', NULL, NULL, NULL, '2021-07-29 23:59:54', '2021-07-29 23:59:54'),
	(271, 178, 1, 'VỀ CENTURY STONE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 18:24:34', '2021-07-31 18:24:34'),
	(272, 178, 2, 'ABOUT CENTURY STONE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 18:24:34', '2021-07-31 18:24:34'),
	(273, 179, 1, 'Giới thiệu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 18:25:39', '2021-07-31 18:25:39'),
	(274, 179, 2, 'Introduce', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 18:25:39', '2021-07-31 18:25:39'),
	(275, 180, 1, 'Blog', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 18:25:59', '2021-07-31 18:25:59'),
	(276, 180, 2, 'Blog', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 18:25:59', '2021-07-31 18:25:59'),
	(277, 181, 1, 'HƯỚNG DẪN', NULL, NULL, NULL, 'HƯỚNG DẪN', NULL, NULL, NULL, '2021-07-31 18:26:54', '2021-07-31 18:26:54'),
	(278, 181, 2, 'TUTORIAL', NULL, NULL, NULL, 'TUTORIAL', NULL, NULL, NULL, '2021-07-31 18:26:54', '2021-07-31 18:26:54'),
	(279, 182, 1, 'Chính sách giao hàng', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 18:28:20', '2021-07-31 18:28:20'),
	(280, 182, 2, 'Delivery policy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 18:28:20', '2021-07-31 18:28:20'),
	(281, 183, 1, 'Trung tâm trợ giúp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 18:28:40', '2021-07-31 18:28:40'),
	(282, 183, 2, 'Help Center', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 18:28:40', '2021-07-31 18:28:40'),
	(283, 184, 1, 'Hỗ trợ mua hàng', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 18:29:03', '2021-07-31 18:29:03'),
	(284, 184, 2, 'Purchase support', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 18:29:03', '2021-07-31 18:29:03'),
	(297, 191, 1, 'Sự kiện nổi bật', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-20 01:11:10', '2021-09-20 01:11:10'),
	(298, 191, 2, 'Featured Events', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-20 01:11:10', '2021-09-20 01:11:10'),
	(305, 195, 1, 'Về Century Stone', NULL, NULL, NULL, 'Century Stone', NULL, NULL, NULL, '2021-10-06 11:24:36', '2021-10-06 11:24:36'),
	(306, 195, 2, 'About Century Stone', NULL, NULL, NULL, 'About Century Stone', NULL, NULL, NULL, '2021-10-06 11:24:37', '2021-10-06 11:24:37'),
	(313, 199, 1, 'Tin tức', NULL, NULL, NULL, 'App photo', NULL, NULL, NULL, '2021-10-26 09:39:39', '2021-10-26 09:39:39'),
	(314, 199, 2, 'News', NULL, NULL, NULL, 'News', NULL, NULL, NULL, '2021-10-26 09:39:39', '2021-10-26 09:39:39'),
	(315, 200, 1, 'Đối tác', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-29 15:10:35', '2021-11-29 15:10:35'),
	(316, 200, 2, 'Partner', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-29 15:10:35', '2021-11-29 15:10:35'),
	(317, 201, 1, 'Kho ứng dụng', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-10 10:16:23', '2022-01-10 10:16:23'),
	(318, 201, 2, 'Kho ứng dụng', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-10 10:16:23', '2022-01-10 10:16:23'),
	(319, 201, 3, 'Kho ứng dụng', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-10 10:16:23', '2022-01-10 10:16:23'),
	(320, 202, 1, 'SẢN PHẨM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-12 14:53:54', '2022-01-12 14:53:54'),
	(321, 202, 2, 'PRODUCT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-12 14:53:54', '2022-01-12 14:53:54'),
	(322, 203, 1, 'HỖ TRỢ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-12 14:54:33', '2022-01-12 14:54:33'),
	(323, 203, 2, 'SUPORT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-12 14:54:33', '2022-01-12 14:54:33'),
	(324, 195, 3, '关于世纪石', NULL, NULL, NULL, '关于世纪石', NULL, NULL, NULL, NULL, NULL),
	(325, 204, 1, 'Ảnh ứng dụng', NULL, NULL, NULL, 'Ảnh ứng dụng', NULL, NULL, NULL, '2022-01-20 11:44:14', '2022-01-20 11:44:14'),
	(326, 204, 2, 'App photo', NULL, NULL, NULL, 'App photo', NULL, NULL, NULL, '2022-01-20 11:44:14', '2022-01-20 11:44:14'),
	(327, 204, 3, '应用照片', NULL, NULL, NULL, '应用照片', NULL, NULL, NULL, '2022-01-20 11:44:14', '2022-01-20 11:44:14'),
	(328, 199, 3, '消息', NULL, NULL, NULL, '消息', NULL, NULL, NULL, NULL, NULL),
	(329, 205, 1, 'Đánh giá', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-20 11:54:34', '2022-01-20 11:54:34'),
	(330, 205, 2, 'Comment', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-20 11:54:34', '2022-01-20 11:54:34'),
	(331, 205, 3, '评论', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-20 11:54:34', '2022-01-20 11:54:34'),
	(332, 165, 3, '主页', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(333, 168, 3, '产品', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(334, 176, 3, '石英水晶系列', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(335, 103, 3, '天然石英系列', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(336, 95, 3, '石英系列', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(337, 177, 3, '接触', NULL, NULL, NULL, '接触', NULL, NULL, NULL, NULL, NULL),
	(338, 178, 3, '关于世纪石', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(339, 179, 3, '介绍', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(340, 126, 3, '收藏', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(341, 170, 3, '照片应用', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(342, 98, 3, '产品', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(343, 180, 3, '消息', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(344, 81, 3, '接触', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(345, 181, 3, '教程', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(346, 102, 3, '保修政策', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(347, 182, 3, '交货政策', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(348, 183, 3, '帮助中心', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(349, 184, 3, '购买支持', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(350, 202, 3, '产品', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(351, 203, 3, '支持', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(352, 206, 1, 'Bộ sưu tập thạch anh Classic', NULL, NULL, NULL, 'Bộ sưu tập thạch anh Classic', NULL, NULL, NULL, '2022-01-21 16:32:55', '2022-01-21 16:32:55'),
	(353, 206, 2, 'Bộ sưu tập thạch anh Classic', NULL, NULL, NULL, 'Classic Quartz Collection', NULL, NULL, NULL, '2022-01-21 16:32:55', '2022-01-21 16:32:55'),
	(354, 206, 3, '经典石英系列', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 16:32:55', '2022-01-21 16:32:55'),
	(355, 207, 1, 'Bộ sưu tập thạch anh Classic', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 16:35:28', '2022-01-21 16:35:28'),
	(356, 207, 2, 'Classic Quartz Collection', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 16:35:28', '2022-01-21 16:35:28'),
	(357, 207, 3, '经典石英系列', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 16:35:28', '2022-01-21 16:35:28'),
	(358, 208, 1, 'Bộ sưu tập thạch anh Classic', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 16:36:34', '2022-01-21 16:36:34'),
	(359, 208, 2, 'Bộ sưu tập thạch anh Classic', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 16:36:34', '2022-01-21 16:36:34'),
	(360, 208, 3, 'Bộ sưu tập thạch anh Classic', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 16:36:34', '2022-01-21 16:36:34'),
	(361, 209, 1, 'Bộ sưu tập thạch anh Nhũ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 16:38:27', '2022-01-21 16:38:27'),
	(362, 209, 2, 'Quartz Collection', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 16:38:27', '2022-01-21 16:38:27'),
	(363, 209, 3, '石英系列', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-21 16:38:27', '2022-01-21 16:38:27'),
	(364, 210, 1, 'Khu vực nhà bếp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-26 19:38:07', '2022-01-26 19:38:07'),
	(365, 210, 2, 'Kitchen area', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-26 19:38:07', '2022-01-26 19:38:07'),
	(366, 210, 3, '厨房区', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-26 19:38:07', '2022-01-26 19:38:07'),
	(367, 211, 1, 'Khu vực nhà tắm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-26 19:39:23', '2022-01-26 19:39:23'),
	(368, 211, 2, 'Bathroom area', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-26 19:39:23', '2022-01-26 19:39:23'),
	(369, 211, 3, '浴室区', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-26 19:39:23', '2022-01-26 19:39:23'),
	(370, 212, 1, 'Khu vực phòng khách', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-26 19:40:16', '2022-01-26 19:40:16'),
	(371, 212, 2, 'Living room area', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-26 19:40:16', '2022-01-26 19:40:16'),
	(372, 212, 3, '客厅区域', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-26 19:40:16', '2022-01-26 19:40:16'),
	(373, 213, 1, 'Khu vực văn phòng', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-26 19:41:28', '2022-01-26 19:41:28'),
	(374, 213, 2, 'Office area', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-26 19:41:28', '2022-01-26 19:41:28'),
	(375, 213, 3, '办公区', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-26 19:41:28', '2022-01-26 19:41:28'),
	(376, 214, 1, 'Các sản phẩm khác', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-26 19:42:11', '2022-01-26 19:42:11'),
	(377, 214, 2, 'Other products', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-26 19:42:11', '2022-01-26 19:42:11'),
	(378, 214, 3, '其他产品', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-01-26 19:42:11', '2022-01-26 19:42:11'),
	(379, 215, 1, 'Chính sách', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-10 14:38:57', '2022-02-10 14:38:57'),
	(380, 215, 2, 'Chính sách', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-10 14:38:57', '2022-02-10 14:38:57'),
	(381, 215, 3, 'Chính sách', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-10 14:38:57', '2022-02-10 14:38:57'),
	(382, 216, 1, 'Chính sách', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-10 14:40:11', '2022-02-10 14:40:11'),
	(383, 216, 2, 'Chính sách', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-10 14:40:11', '2022-02-10 14:40:11'),
	(384, 216, 3, 'Chính sách', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-10 14:40:11', '2022-02-10 14:40:11'),
	(385, 217, 1, '12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-11 00:04:24', '2022-02-11 00:04:24'),
	(386, 217, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-11 00:04:24', '2022-02-11 00:04:24'),
	(387, 217, 3, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-11 00:04:24', '2022-02-11 00:04:24');
/*!40000 ALTER TABLE `menu_description` ENABLE KEYS */;

-- Dumping structure for table centurystone.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.migrations: ~27 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2000_01_01_000001_create_users_table', 1),
	(2, '2000_01_01_000002_create_password_resets_table', 1),
	(3, '2000_01_02_000001_create_payment_accounts_table', 1),
	(4, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
	(5, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
	(6, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
	(7, '2016_06_01_000004_create_oauth_clients_table', 1),
	(8, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
	(9, '2016_07_08_110947_create_stripe_accounts_table', 1),
	(10, '2016_12_29_201047_create_permission_tables', 1),
	(11, '2016_20_09_030201_create_settings_table', 1),
	(12, '2017_03_28_203558_create_jobs_table', 1),
	(13, '2017_03_28_203559_create_failed_jobs_table', 1),
	(14, '2017_09_12_174826_create_notifications_table', 1),
	(15, '2017_10_26_214459_update_user_table_with_socialauth_columns', 1),
	(16, '2017_10_27_091547_add_level_column_to_roles_table', 1),
	(17, '2018_01_01_000001_create_payment_transactions_table', 1),
	(18, '2021_01_27_102644_create_menu_tables', 2),
	(19, '2021_02_08_095956_create_gallery_tables', 3),
	(20, '2021_03_03_171352_create_discount_tables', 4),
	(21, '2021_03_05_100911_create_geo_tables', 4),
	(22, '2021_03_22_135627_create_inventory_tables', 4),
	(23, '2021_04_13_115419_create_eshopbizfly_tables', 4),
	(24, '2021_04_15_130033_create_jobs_table', 4),
	(25, '2021_07_14_183215_create_sessions_table', 5),
	(26, '2014_04_02_193005_create_translations_table', 6),
	(27, '2021_06_25_182655_create_location_tables', 6);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table centurystone.model_has_permissions
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`model_type`,`model_id`,`permission_id`) USING BTREE,
  KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`) USING BTREE,
  KEY `model_has_permissions_permission_id_foreign` (`permission_id`) USING BTREE,
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.model_has_permissions: ~44 rows (approximately)
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
INSERT INTO `model_has_permissions` (`permission_id`, `model_type`, `model_id`) VALUES
	(1, 'App\\Containers\\User\\Models\\User', 42),
	(3, 'App\\Containers\\User\\Models\\User', 41),
	(3, 'App\\Containers\\User\\Models\\User', 42),
	(4, 'App\\Containers\\User\\Models\\User', 42),
	(6, 'App\\Containers\\User\\Models\\User', 1),
	(6, 'App\\Containers\\User\\Models\\User', 41),
	(6, 'App\\Containers\\User\\Models\\User', 42),
	(7, 'App\\Containers\\User\\Models\\User', 1),
	(7, 'App\\Containers\\User\\Models\\User', 41),
	(7, 'App\\Containers\\User\\Models\\User', 42),
	(8, 'App\\Containers\\User\\Models\\User', 41),
	(8, 'App\\Containers\\User\\Models\\User', 42),
	(15, 'App\\Containers\\User\\Models\\User', 42),
	(16, 'App\\Containers\\User\\Models\\User', 42),
	(30, 'App\\Containers\\User\\Models\\User', 42),
	(31, 'App\\Containers\\Customer\\Models\\Customer', 81),
	(31, 'App\\Containers\\Customer\\Models\\Customer', 83),
	(31, 'App\\Containers\\Customer\\Models\\Customer', 148),
	(31, 'App\\Containers\\Customer\\Models\\Customer', 149),
	(31, 'App\\Containers\\Customer\\Models\\Customer', 158),
	(31, 'App\\Containers\\Customer\\Models\\Customer', 159),
	(32, 'App\\Containers\\User\\Models\\User', 42),
	(33, 'App\\Containers\\User\\Models\\User', 42),
	(44, 'App\\Containers\\User\\Models\\User', 42),
	(47, 'App\\Containers\\User\\Models\\User', 42),
	(48, 'App\\Containers\\User\\Models\\User', 42),
	(49, 'App\\Containers\\User\\Models\\User', 42),
	(50, 'App\\Containers\\User\\Models\\User', 42),
	(51, 'App\\Containers\\User\\Models\\User', 42),
	(52, 'App\\Containers\\User\\Models\\User', 42),
	(55, 'App\\Containers\\User\\Models\\User', 42),
	(56, 'App\\Containers\\User\\Models\\User', 42),
	(57, 'App\\Containers\\User\\Models\\User', 42),
	(58, 'App\\Containers\\User\\Models\\User', 42),
	(59, 'App\\Containers\\User\\Models\\User', 42),
	(60, 'App\\Containers\\User\\Models\\User', 42),
	(67, 'App\\Containers\\User\\Models\\User', 42),
	(69, 'App\\Containers\\User\\Models\\User', 42),
	(71, 'App\\Containers\\User\\Models\\User', 42),
	(72, 'App\\Containers\\User\\Models\\User', 42),
	(82, 'App\\Containers\\User\\Models\\User', 42),
	(84, 'App\\Containers\\User\\Models\\User', 42),
	(86, 'App\\Containers\\User\\Models\\User', 42),
	(87, 'App\\Containers\\User\\Models\\User', 42);
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;

-- Dumping structure for table centurystone.model_has_roles
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`model_id`,`role_id`,`model_type`) USING BTREE,
  KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`) USING BTREE,
  KEY `model_has_roles_role_id_foreign` (`role_id`) USING BTREE,
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.model_has_roles: ~12 rows (approximately)
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
	(1, 'App\\Containers\\User\\Models\\User', 1),
	(1, 'App\\Containers\\User\\Models\\User', 2),
	(1, 'App\\Containers\\User\\Models\\User', 37),
	(1, 'App\\Containers\\User\\Models\\User', 38),
	(14, 'App\\Containers\\User\\Models\\User', 39),
	(14, 'App\\Containers\\User\\Models\\User', 41),
	(15, 'App\\Containers\\User\\Models\\User', 40),
	(15, 'App\\Containers\\User\\Models\\User', 42),
	(15, 'App\\Containers\\User\\Models\\User', 45),
	(19, 'App\\Containers\\User\\Models\\User', 43),
	(19, 'App\\Containers\\User\\Models\\User', 44),
	(19, 'App\\Containers\\User\\Models\\User', 46);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;

-- Dumping structure for table centurystone.news
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT '',
  `status` tinyint(1) DEFAULT '1',
  `sort_order` int(11) unsigned DEFAULT '0',
  `published` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `views` int(11) DEFAULT '0',
  `is_hot` tinyint(1) unsigned DEFAULT '0',
  `is_home` tinyint(1) unsigned DEFAULT '0',
  `author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author_updated` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('normal','event','promotion') DEFAULT 'normal',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.news: ~16 rows (approximately)
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` (`id`, `image`, `status`, `sort_order`, `published`, `created_at`, `updated_at`, `views`, `is_hot`, `is_home`, `author`, `author_updated`, `type`) VALUES
	(1, 'news-1642758513.png', 2, NULL, NULL, '2022-01-21 16:48:33', '2022-01-21 16:48:33', 0, 0, 2, 'Tester', NULL, 'normal'),
	(2, 'news-1642759026.jpg', 2, NULL, NULL, '2022-01-21 16:57:06', '2022-01-21 16:57:06', 0, 0, 2, 'Tester', NULL, 'normal'),
	(3, 'news-1642759411.jpg', 2, NULL, NULL, '2022-01-21 17:03:31', '2022-01-21 17:03:31', 0, 0, 2, 'Tester', NULL, 'normal'),
	(4, 'news-1642759501.jpg', 2, NULL, NULL, '2022-01-21 17:05:01', '2022-01-21 17:05:01', 0, 0, 2, 'Tester', NULL, 'normal'),
	(5, 'news-1642759619.jpg', 2, NULL, NULL, '2022-01-21 17:06:59', '2022-01-21 17:06:59', 0, 0, 2, 'Tester', NULL, 'normal'),
	(6, 'news-1642759710.jpg', 2, NULL, NULL, '2022-01-21 17:08:30', '2022-01-21 17:08:30', 0, 0, 2, 'Tester', NULL, 'normal'),
	(7, 'news-1642759858.jpg', 2, 2, NULL, '2022-01-21 17:10:58', '2022-02-07 16:59:09', 0, 0, 2, 'Tester', 'Tester', 'normal'),
	(8, 'news-1642759998.jpg', 2, NULL, NULL, '2022-01-21 17:13:18', '2022-01-21 17:13:18', 0, 0, 2, 'Tester', NULL, 'normal'),
	(9, 'news-1642760164.jpg', 2, NULL, NULL, '2022-01-21 17:16:04', '2022-01-21 17:16:04', 0, 0, 2, 'Tester', NULL, 'normal'),
	(10, 'news-1642760413.jpg', 2, NULL, NULL, '2022-01-21 17:20:13', '2022-01-21 17:20:13', 0, 0, 2, 'Tester', NULL, 'normal'),
	(11, 'news-1642760594.jpg', 2, NULL, NULL, '2022-01-21 17:23:14', '2022-01-21 17:23:14', 0, 0, 2, 'Tester', NULL, 'normal'),
	(12, 'news-1642760696.jpg', 2, 1, NULL, '2022-01-21 17:24:56', '2022-02-07 16:52:19', 0, 0, 2, 'Tester', 'Tester', 'normal'),
	(13, 'news-1643098076.jpg', 1, NULL, NULL, '2022-01-25 15:07:56', '2022-01-28 12:08:58', 0, 0, 1, 'Tester', NULL, 'normal'),
	(14, '', -1, NULL, NULL, '2022-02-08 11:26:00', '2022-02-08 11:26:06', 0, 0, 0, 'Phan Trung Tưởng', NULL, 'normal'),
	(15, '', -1, NULL, NULL, '2022-02-09 11:11:58', '2022-02-09 17:45:49', 0, 0, 0, 'Tester', NULL, 'normal'),
	(16, '', -1, NULL, NULL, '2022-02-10 17:43:04', '2022-02-10 17:45:33', 0, 0, 0, 'Tester', NULL, 'normal'),
	(17, 'news-1644554673.png', 1, NULL, NULL, '2022-02-11 11:44:33', '2022-02-11 11:50:05', 0, 0, 0, 'Phan Trung Tưởng', 'Phan Trung Tưởng', 'normal');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;

-- Dumping structure for table centurystone.news_category
CREATE TABLE IF NOT EXISTS `news_category` (
  `news_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`news_id`,`category_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.news_category: ~16 rows (approximately)
/*!40000 ALTER TABLE `news_category` DISABLE KEYS */;
INSERT INTO `news_category` (`news_id`, `category_id`) VALUES
	(1, 8),
	(2, 64),
	(3, 64),
	(4, 64),
	(5, 64),
	(6, 64),
	(7, 64),
	(8, 64),
	(9, 64),
	(10, 64),
	(11, 64),
	(12, 64),
	(13, 64),
	(14, 64),
	(15, 64),
	(16, 64);
/*!40000 ALTER TABLE `news_category` ENABLE KEYS */;

-- Dumping structure for table centurystone.news_description
CREATE TABLE IF NOT EXISTS `news_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` text CHARACTER SET utf8,
  `meta_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.news_description: ~45 rows (approximately)
/*!40000 ALTER TABLE `news_description` DISABLE KEYS */;
INSERT INTO `news_description` (`id`, `news_id`, `language_id`, `name`, `short_description`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`, `slug`) VALUES
	(1, 1, 1, 'CENTURYSTONE TIẾP TỤC ĐƯỢC VINH DANH CƠ SỞ SỬ DỤNG NĂNG LƯỢNG XANH (TIÊU CHUẨN 5 SAO)', 'Ngày 11/12/2021, Century Stone tự hào được công nhận là sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất công nghiệp (thứ hạng 5 sao) sau khi đã xuất sắc vượt qua 90 đơn vị trên địa bàn thành phố Hà Nội. Đây là giải thưởng vinh danh các cơ sở, công trình xây dựng sử dụng năng lượng xanh năm 2021 do Sở Công Thương Hà Nội tổ chức, công bố và trao tặng.', '<p>Ng&agrave;y 11/12/2021, Century Stone tự h&agrave;o được c&ocirc;ng nhận l&agrave; sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất c&ocirc;ng nghiệp (thứ hạng 5 sao) sau khi đ&atilde; xuất sắc vượt qua 90 đơn vị tr&ecirc;n địa b&agrave;n th&agrave;nh phố H&agrave; Nội. Đ&acirc;y l&agrave; giải thưởng vinh danh c&aacute;c cơ sở, c&ocirc;ng tr&igrave;nh x&acirc;y dựng sử dụng năng lượng xanh năm 2021 do Sở C&ocirc;ng Thương H&agrave; Nội tổ chức, c&ocirc;ng bố v&agrave; trao tặng.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Trong những năm qua, Century Stone lu&ocirc;n ch&uacute; trọng v&agrave; đồng bộ triển khai c&aacute;c giải ph&aacute;p trong việc tiết kiệm năng lượng hiệu quả, &iacute;t ph&aacute;t thải ra m&ocirc;i trường, t&aacute;i sử dụng ở mức đối đa trong c&aacute;c hoạt động sản xuất, kinh doanh của doanh nghiệp.</p>\r\n<p>Với việc sử dụng d&acirc;y chuyền sản xuất hiện đại bậc nhất được chuyển giao từ Breton (&Yacute;), c&ugrave;ng sự cải tiến đột ph&aacute; của đội ngũ kỹ sư c&ocirc;ng nghệ, to&agrave;n bộ qu&aacute; tr&igrave;nh sản xuất của Vicostone ho&agrave;n to&agrave;n kh&eacute;p k&iacute;n v&agrave; th&acirc;n thiện với m&ocirc;i trường, kh&ocirc;ng c&oacute; c&ocirc;ng đoạn nung n&ecirc;n đạt hiệu quả tiết kiệm năng lượng, kh&ocirc;ng c&oacute; kh&iacute; thải từ qu&aacute; tr&igrave;nh dưỡng hộ như c&aacute;c loại vật liệu ốp l&aacute;t được sản xuất theo c&ocirc;ng nghệ th&ocirc;ng thường</p>\r\n<p>B&ecirc;n cạnh đ&oacute;, Century Stone đ&atilde; triển khai v&agrave; li&ecirc;n tục mở rộng/ cải tiến kỹ thuật nhiều biện ph&aacute;p tiết kiệm năng lượng hiệu quả như:</p>\r\n<p>Thực hiện triển khai hệ thống điện năng lượng mặt trời tại 2 t&ograve;a nh&agrave; điều h&agrave;nh A1 v&agrave; A2 của C&ocirc;ng ty từ năm 2017 với 400 tấm pin quang điện được lắp đặt, c&oacute; thể cung cấp tới 90% điện năng cho hoạt động của hơn 200 c&aacute;n bộ, c&ocirc;ng nh&acirc;n vi&ecirc;n đang l&agrave;m việc tại 2 t&ograve;a nh&agrave;. Việc đưa tấm pin năng lượng mặt trời v&agrave;o hoạt động đ&atilde; gi&uacute;p sản lượng điện mỗi năm của Vicostone tiết kiệm l&ecirc;n đến tr&ecirc;n 500.000 kWh, tương đương với giảm ph&aacute;t thải tr&ecirc;n 200 tấn CO2 mỗi năm.<br />Thực hiện nhiều giải ph&aacute;p tiết kiệm năng lượng trong qu&aacute; tr&igrave;nh sản xuất,- ti&ecirc;u biểu như: Giải ph&aacute;p cho hệ thống chiếu s&aacute;ng; Giải ph&aacute;p cho hệ thống m&aacute;y n&eacute;n kh&iacute;; Giải ph&aacute;p lắp biến tần cho hệ thống bơm nước thải, bơm cấp nước c&ocirc;ng nghệ, giải ph&aacute;p cải tạo hệ thống sấy khu&ocirc;n, giải ph&aacute;p cải tạo hầm l&agrave;m m&aacute;t c&oacute; lắp biến tần điều khiển động cơ&hellip;<br />&Aacute;p dụng Hệ thống quản l&yacute; năng lượng theo ti&ecirc;u chuẩn ISO 50001: To&agrave;n bộ c&aacute;c c&ocirc;ng đoạn sử dụng năng lượng điện, gas, dầu diesel, nước sạch đều c&oacute; định mức sử dụng r&otilde; r&agrave;ng trong quy định, quy tr&igrave;nh ban h&agrave;nh; c&oacute; bộ phận chuy&ecirc;n m&ocirc;n theo d&otilde;i, gi&aacute;m s&aacute;t, ph&acirc;n t&iacute;ch dữ liệu năng lượng để cảnh b&aacute;o v&agrave; đưa ra c&aacute;c giải ph&aacute;p kịp thời về việc sử dụng năng lượng, tr&aacute;nh l&atilde;ng ph&iacute; m&agrave; vẫn đảm bảo sản xuất.<br />Qua những giải ph&aacute;p thiết thực tr&ecirc;n, Vicostone tự h&agrave;o g&oacute;p phần phần th&uacute;c đẩy c&ocirc;ng t&aacute;c quản l&yacute;, sử dụng năng lượng hiệu quả, n&acirc;ng cao hiệu suất sử dụng năng lượng của Th&agrave;nh phố H&agrave; Nội, tối đa sử dụng nguồn năng lượng xanh, hiện thực h&oacute;a cam kết ph&aacute;t triển bền vững v&agrave; tr&aacute;ch nhiệm v&igrave; cộng đồng x&atilde; hội.</p>\r\n<p>Vicostone thuộc Tập đo&agrave;n Phenikaa, một trong c&aacute;c nh&agrave; sản xuất h&agrave;ng đầu thế giới về đ&aacute; nh&acirc;n tạo gốc thạch anh cao cấp. Sản phẩm đ&atilde; c&oacute; mặt tại hơn 50 quốc gia, chinh phục c&aacute;c thị trường khắt khe như Mỹ, Canada, Australia, ch&acirc;u &Acirc;u...</p>\r\n<p>Với hơn 130 mẫu m&atilde;, m&agrave;u sắc, được thiết kế lấy &yacute; tưởng từ vẻ đẹp của thi&ecirc;n nhi&ecirc;n, mặt đ&aacute; truyền cảm hứng s&aacute;ng tạo cho kh&ocirc;ng gian sống, l&agrave; lựa chọn ph&ugrave; hợp để l&agrave;m b&agrave;n bếp đ&aacute; nh&acirc;n tạo, b&agrave;n tr&agrave;, b&agrave;n ăn, nh&agrave; tắm, decor nội thất để tạo điểm nhấn. Century Stone bảo h&agrave;nh 15 năm cho sản phẩm ch&iacute;nh h&atilde;ng với c&ocirc;ng tr&igrave;nh nh&agrave; ri&ecirc;ng, tham khảo tại: www.Century Stone.com/vi-vn</p>', NULL, '', '', '', 'centurystone-tiep-tuc-duoc-vinh-danh-co-so-su-dung-nang-luong-xanh-tieu-chuan-5-sao'),
	(2, 1, 2, 'CENTURYSTONE CONTINUES TO BE HONORED A GREEN ENERGY USE FACILITY (5 STAR STANDARD)', 'On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.', '<p>On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Over the years, Century Stone has always focused and synchronously deployed solutions to save energy effectively, with low emissions to the environment, and reuse to the maximum extent in production and business activities. of the enterprise.</p>\r\n<p>With the use of the most modern production line transferred from Breton (Italy), along with the breakthrough improvement of a team of technology engineers, Vicostone\'s entire production process is completely self-contained and user-friendly. Environmentally friendly, there is no calcination process, so it is effective in saving energy, and there is no emission from the curing process like other types of paving materials produced by conventional technology.</p>\r\n<p>In addition, Century Stone has deployed and continuously expanded/improved many effective energy saving measures such as:</p>\r\n<p>Deploying the solar power system at the Company\'s 2 operating buildings A1 and A2 from 2017 with 400 installed photovoltaic panels, which can provide up to 90% of electricity for the operation of more than 200 officers and employees are working in 2 buildings. Putting solar panels into operation has helped Vicostone\'s annual electricity output save up to over 500,000 kWh, equivalent to reducing emissions of over 200 tons of CO2 per year.<br />Implement many solutions to save energy in the production process,- typically: Solutions for lighting system; Solutions for air compressor systems; Solutions to install inverters for wastewater pumping systems, technology water supply pumps, solutions to renovate mold drying systems, solutions to renovate cooling tunnels with motor control inverters...<br />Applying the Energy Management System according to ISO 50001: All stages of using electric energy, gas, diesel oil, clean water have clear usage norms in the promulgated regulations and procedures; has a specialized department to monitor, monitor and analyze energy data to warn and provide timely solutions on energy use, avoid waste while ensuring production.<br />Through these practical solutions, Vicostone is proud to contribute to promoting the management and efficient use of energy, improving the energy efficiency of Hanoi City, and maximizing the use of green energy sources. , realizing the commitment to sustainable development and responsibility for the social community.</p>\r\n<p>Vicostone is part of Phenikaa Group, one of the world\'s leading manufacturers of premium quartz-based artificial stone. Products have been present in more than 50 countries, conquering demanding markets such as the US, Canada, Australia, Europe...</p>\r\n<p>With more than 130 designs and colors, designed with inspiration from the beauty of nature, stone countertops inspire creativity for living space, is the right choice for making artificial stone kitchen table, tea table, etc. dining table, bathroom, interior decoration to create accents. Century Stone has a 15-year warranty for genuine products with private buildings, refer to: www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'centurystone-continues-to-be-honored-a-green-energy-use-facility-5-star-standard'),
	(3, 1, 3, 'CENTURYSTONE 继续被授予绿色能源使用设施（5 星级标准）', '2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。', '<p>2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>多年来，世纪石始终专注并同步部署解决方案，以有效节约能源，对环境低排放，并在生产经营活动中最大程度地重复利用。的企业。</p>\r\n<p>使用从意大利布列塔尼转来的最现代化的生产线，再加上技术工程师团队的突破性改进，Vicostone的整个生产过程完全独立且人性化。环保，无需煅烧工序，节能效果显着，不会像传统工艺生产的其他类型铺路材料一样，在固化过程中产生废气。</p>\r\n<p>此外，世纪石还部署并不断扩展/改进了许多有效的节能措施，例如：</p>\r\n<p>从 2017 年开始，在公司的 2 栋运营大楼 A1 和 A2 部署太阳能发电系统，安装了 400 块光伏板，可为 2 栋大楼内 200 多名员工和员工的运营提供高达 90% 的电力。太阳能电池板投产后，Vicostone 的年发电量可节省超过 500,000 千瓦时，相当于每年减少 200 多吨二氧化碳的排放。<br />在生产过程中实施许多节能解决方案，通常是：照明系统解决方案；空压机系统解决方案；废水泵系统逆变器安装解决方案、技术供水泵、模具干燥系统改造解决方案、使用电机控制逆变器改造冷却隧道的解决方案......<br />应用符合ISO 50001的能源管理体系：在已颁布的法规和程序中，使用电能、燃气、柴油、清洁水的各个阶段都有明确的使用规范；设有专门部门对能源数据进行监测、监测和分析，对能源使用情况进行预警并及时提供解决方案，在确保生产的同时避免浪费。<br />通过这些切实可行的解决方案，Vicostone 很自豪能够为促进能源的管理和高效利用、提高河内市的能源效率以及最大限度地利用绿色能源做出贡献。 ，实现对可持续发展的承诺和对社会社区的责任。</p>\r\n<p>Vicostone 隶属于 Phenikaa 集团，该集团是世界领先的优质石英人造石制造商之一。产品已在 50 多个国家出现，征服了美国、加拿大、澳大利亚、欧洲等要求苛刻的市场&hellip;&hellip;</p>\r\n<p>130多种设计和颜色，灵感来自大自然之美，石材台面激发生活空间的创造力，是制作人造石厨房桌子、茶几等餐桌、浴室、室内装饰创造的正确选择口音。 Century Stone对私人建筑的正品提供15年保修，请参阅：www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'centurystone-ji-xu-bei-shou-yu-lu-se-neng-yuan-shi-yong-she-shi-5-xing-ji-biao-zhun'),
	(4, 2, 1, 'MUA SẮM AN TOÀN VỚI CHƯƠNG TRÌNH ‘’CHỐT ĐƠN ONLINE- ƯU ĐÃI X2’’ TỪ CENTURYSTONE', 'Ngày 11/12/2021, Century Stone tự hào được công nhận là sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất công nghiệp (thứ hạng 5 sao) sau khi đã xuất sắc vượt qua 90 đơn vị trên địa bàn thành phố Hà Nội.', '<p>Ng&agrave;y 11/12/2021, Century Stone tự h&agrave;o được c&ocirc;ng nhận l&agrave; sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất c&ocirc;ng nghiệp (thứ hạng 5 sao) sau khi đ&atilde; xuất sắc vượt qua 90 đơn vị tr&ecirc;n địa b&agrave;n th&agrave;nh phố H&agrave; Nội. Đ&acirc;y l&agrave; giải thưởng vinh danh c&aacute;c cơ sở, c&ocirc;ng tr&igrave;nh x&acirc;y dựng sử dụng năng lượng xanh năm 2021 do Sở C&ocirc;ng Thương H&agrave; Nội tổ chức, c&ocirc;ng bố v&agrave; trao tặng.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Trong những năm qua, Century Stone lu&ocirc;n ch&uacute; trọng v&agrave; đồng bộ triển khai c&aacute;c giải ph&aacute;p trong việc tiết kiệm năng lượng hiệu quả, &iacute;t ph&aacute;t thải ra m&ocirc;i trường, t&aacute;i sử dụng ở mức đối đa trong c&aacute;c hoạt động sản xuất, kinh doanh của doanh nghiệp.</p>\r\n<p>Với việc sử dụng d&acirc;y chuyền sản xuất hiện đại bậc nhất được chuyển giao từ Breton (&Yacute;), c&ugrave;ng sự cải tiến đột ph&aacute; của đội ngũ kỹ sư c&ocirc;ng nghệ, to&agrave;n bộ qu&aacute; tr&igrave;nh sản xuất của Vicostone ho&agrave;n to&agrave;n kh&eacute;p k&iacute;n v&agrave; th&acirc;n thiện với m&ocirc;i trường, kh&ocirc;ng c&oacute; c&ocirc;ng đoạn nung n&ecirc;n đạt hiệu quả tiết kiệm năng lượng, kh&ocirc;ng c&oacute; kh&iacute; thải từ qu&aacute; tr&igrave;nh dưỡng hộ như c&aacute;c loại vật liệu ốp l&aacute;t được sản xuất theo c&ocirc;ng nghệ th&ocirc;ng thường</p>\r\n<p>B&ecirc;n cạnh đ&oacute;, Century Stone đ&atilde; triển khai v&agrave; li&ecirc;n tục mở rộng/ cải tiến kỹ thuật nhiều biện ph&aacute;p tiết kiệm năng lượng hiệu quả như:</p>\r\n<p>Thực hiện triển khai hệ thống điện năng lượng mặt trời tại 2 t&ograve;a nh&agrave; điều h&agrave;nh A1 v&agrave; A2 của C&ocirc;ng ty từ năm 2017 với 400 tấm pin quang điện được lắp đặt, c&oacute; thể cung cấp tới 90% điện năng cho hoạt động của hơn 200 c&aacute;n bộ, c&ocirc;ng nh&acirc;n vi&ecirc;n đang l&agrave;m việc tại 2 t&ograve;a nh&agrave;. Việc đưa tấm pin năng lượng mặt trời v&agrave;o hoạt động đ&atilde; gi&uacute;p sản lượng điện mỗi năm của Vicostone tiết kiệm l&ecirc;n đến tr&ecirc;n 500.000 kWh, tương đương với giảm ph&aacute;t thải tr&ecirc;n 200 tấn CO2 mỗi năm.<br />Thực hiện nhiều giải ph&aacute;p tiết kiệm năng lượng trong qu&aacute; tr&igrave;nh sản xuất,- ti&ecirc;u biểu như: Giải ph&aacute;p cho hệ thống chiếu s&aacute;ng; Giải ph&aacute;p cho hệ thống m&aacute;y n&eacute;n kh&iacute;; Giải ph&aacute;p lắp biến tần cho hệ thống bơm nước thải, bơm cấp nước c&ocirc;ng nghệ, giải ph&aacute;p cải tạo hệ thống sấy khu&ocirc;n, giải ph&aacute;p cải tạo hầm l&agrave;m m&aacute;t c&oacute; lắp biến tần điều khiển động cơ&hellip;<br />&Aacute;p dụng Hệ thống quản l&yacute; năng lượng theo ti&ecirc;u chuẩn ISO 50001: To&agrave;n bộ c&aacute;c c&ocirc;ng đoạn sử dụng năng lượng điện, gas, dầu diesel, nước sạch đều c&oacute; định mức sử dụng r&otilde; r&agrave;ng trong quy định, quy tr&igrave;nh ban h&agrave;nh; c&oacute; bộ phận chuy&ecirc;n m&ocirc;n theo d&otilde;i, gi&aacute;m s&aacute;t, ph&acirc;n t&iacute;ch dữ liệu năng lượng để cảnh b&aacute;o v&agrave; đưa ra c&aacute;c giải ph&aacute;p kịp thời về việc sử dụng năng lượng, tr&aacute;nh l&atilde;ng ph&iacute; m&agrave; vẫn đảm bảo sản xuất.<br />Qua những giải ph&aacute;p thiết thực tr&ecirc;n, Vicostone tự h&agrave;o g&oacute;p phần phần th&uacute;c đẩy c&ocirc;ng t&aacute;c quản l&yacute;, sử dụng năng lượng hiệu quả, n&acirc;ng cao hiệu suất sử dụng năng lượng của Th&agrave;nh phố H&agrave; Nội, tối đa sử dụng nguồn năng lượng xanh, hiện thực h&oacute;a cam kết ph&aacute;t triển bền vững v&agrave; tr&aacute;ch nhiệm v&igrave; cộng đồng x&atilde; hội.</p>\r\n<p>Vicostone thuộc Tập đo&agrave;n Phenikaa, một trong c&aacute;c nh&agrave; sản xuất h&agrave;ng đầu thế giới về đ&aacute; nh&acirc;n tạo gốc thạch anh cao cấp. Sản phẩm đ&atilde; c&oacute; mặt tại hơn 50 quốc gia, chinh phục c&aacute;c thị trường khắt khe như Mỹ, Canada, Australia, ch&acirc;u &Acirc;u...</p>\r\n<p>Với hơn 130 mẫu m&atilde;, m&agrave;u sắc, được thiết kế lấy &yacute; tưởng từ vẻ đẹp của thi&ecirc;n nhi&ecirc;n, mặt đ&aacute; truyền cảm hứng s&aacute;ng tạo cho kh&ocirc;ng gian sống, l&agrave; lựa chọn ph&ugrave; hợp để l&agrave;m b&agrave;n bếp đ&aacute; nh&acirc;n tạo, b&agrave;n tr&agrave;, b&agrave;n ăn, nh&agrave; tắm, decor nội thất để tạo điểm nhấn. Century Stone bảo h&agrave;nh 15 năm cho sản phẩm ch&iacute;nh h&atilde;ng với c&ocirc;ng tr&igrave;nh nh&agrave; ri&ecirc;ng, tham khảo tại: www.Century Stone.com/vi-vn</p>', NULL, '', '', '', 'mua-sam-an-toan-voi-chuong-trinh-chot-don-online-uu-dai-x2-tu-centurystone'),
	(5, 2, 2, 'SAFE SHOPPING WITH THE PROGRAM \'\'ONLINE FINDING - X2 OFFER\'\' FROM CENTURYSTONE', 'On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city', '<p>On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Over the years, Century Stone has always focused and synchronously deployed solutions to save energy effectively, with low emissions to the environment, and reuse to the maximum extent in production and business activities. of the enterprise.</p>\r\n<p>With the use of the most modern production line transferred from Breton (Italy), along with the breakthrough improvement of a team of technology engineers, Vicostone\'s entire production process is completely self-contained and user-friendly. Environmentally friendly, there is no calcination process, so it is effective in saving energy, and there is no emission from the curing process like other types of paving materials produced by conventional technology.</p>\r\n<p>In addition, Century Stone has deployed and continuously expanded/improved many effective energy saving measures such as:</p>\r\n<p>Deploying the solar power system at the Company\'s 2 operating buildings A1 and A2 from 2017 with 400 installed photovoltaic panels, which can provide up to 90% of electricity for the operation of more than 200 officers and employees are working in 2 buildings. Putting solar panels into operation has helped Vicostone\'s annual electricity output save up to over 500,000 kWh, equivalent to reducing emissions of over 200 tons of CO2 per year.<br />Implement many solutions to save energy in the production process,- typically: Solutions for lighting system; Solutions for air compressor systems; Solutions to install inverters for wastewater pumping systems, technology water supply pumps, solutions to renovate mold drying systems, solutions to renovate cooling tunnels with motor control inverters...<br />Applying the Energy Management System according to ISO 50001: All stages of using electric energy, gas, diesel oil, clean water have clear usage norms in the promulgated regulations and procedures; has a specialized department to monitor, monitor and analyze energy data to warn and provide timely solutions on energy use, avoid waste while ensuring production.<br />Through these practical solutions, Vicostone is proud to contribute to promoting the management and efficient use of energy, improving the energy efficiency of Hanoi City, and maximizing the use of green energy sources. , realizing the commitment to sustainable development and responsibility for the social community.</p>\r\n<p>Vicostone is part of Phenikaa Group, one of the world\'s leading manufacturers of premium quartz-based artificial stone. Products have been present in more than 50 countries, conquering demanding markets such as the US, Canada, Australia, Europe...</p>\r\n<p>With more than 130 designs and colors, designed with inspiration from the beauty of nature, stone countertops inspire creativity for living space, is the right choice for making artificial stone kitchen table, tea table, etc. dining table, bathroom, interior decoration to create accents. Century Stone has a 15-year warranty for genuine products with private buildings, refer to: www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'safe-shopping-with-the-program-online-finding-x2-offer-from-centurystone'),
	(6, 2, 3, '使用 CENTURYSTONE 的“在线查找 - X2 优惠”计划进行安全购物', '2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）', '<p>2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>多年来，世纪石始终专注并同步部署解决方案，以有效节约能源，对环境低排放，并在生产经营活动中最大程度地重复利用。的企业。</p>\r\n<p>使用从意大利布列塔尼转来的最现代化的生产线，再加上技术工程师团队的突破性改进，Vicostone的整个生产过程完全独立且人性化。环保，无需煅烧工序，节能效果显着，不会像传统工艺生产的其他类型铺路材料一样，在固化过程中产生废气。</p>\r\n<p>此外，世纪石还部署并不断扩展/改进了许多有效的节能措施，例如：</p>\r\n<p>从 2017 年开始，在公司的 2 栋运营大楼 A1 和 A2 部署太阳能发电系统，安装了 400 块光伏板，可为 2 栋大楼内 200 多名员工和员工的运营提供高达 90% 的电力。太阳能电池板投产后，Vicostone 的年发电量可节省超过 500,000 千瓦时，相当于每年减少 200 多吨二氧化碳的排放。<br />在生产过程中实施许多节能解决方案，通常是：照明系统解决方案；空压机系统解决方案；废水泵系统逆变器安装解决方案、技术供水泵、模具干燥系统改造解决方案、使用电机控制逆变器改造冷却隧道的解决方案......<br />应用符合ISO 50001的能源管理体系：在已颁布的法规和程序中，使用电能、燃气、柴油、清洁水的各个阶段都有明确的使用规范；设有专门部门对能源数据进行监测、监测和分析，对能源使用情况进行预警并及时提供解决方案，在确保生产的同时避免浪费。<br />通过这些切实可行的解决方案，Vicostone 很自豪能够为促进能源的管理和高效利用、提高河内市的能源效率以及最大限度地利用绿色能源做出贡献。 ，实现对可持续发展的承诺和对社会社区的责任。</p>\r\n<p>Vicostone 隶属于 Phenikaa 集团，该集团是世界领先的优质石英人造石制造商之一。产品已在 50 多个国家出现，征服了美国、加拿大、澳大利亚、欧洲等要求苛刻的市场&hellip;&hellip;</p>\r\n<p>130多种设计和颜色，灵感来自大自然之美，石材台面激发生活空间的创造力，是制作人造石厨房桌子、茶几等餐桌、浴室、室内装饰创造的正确选择口音。 Century Stone对私人建筑的正品提供15年保修，请参阅：www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'shi-yong-centurystone-de-zai-xian-cha-zhao-x2-you-hui-ji-hua-jin-xing-an-quan-gou-wu'),
	(7, 3, 1, '3 Công nghệ sản xuất đá thạch anh nhân tạo tốt nhất hiện nay', 'Ngày 11/12/2021, Century Stone tự hào được công nhận là sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất công nghiệp (thứ hạng 5 sao) sau khi đã xuất sắc vượt qua 90 đơn vị trên địa bàn thành phố Hà Nội. Đây là giải thưởng vinh danh các cơ sở, công trình xây dựng sử dụng năng lượng xanh năm 2021 do Sở Công Thương Hà Nội tổ chức, công bố và trao tặng.', '<p>Ng&agrave;y 11/12/2021, Century Stone tự h&agrave;o được c&ocirc;ng nhận l&agrave; sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất c&ocirc;ng nghiệp (thứ hạng 5 sao) sau khi đ&atilde; xuất sắc vượt qua 90 đơn vị tr&ecirc;n địa b&agrave;n th&agrave;nh phố H&agrave; Nội. Đ&acirc;y l&agrave; giải thưởng vinh danh c&aacute;c cơ sở, c&ocirc;ng tr&igrave;nh x&acirc;y dựng sử dụng năng lượng xanh năm 2021 do Sở C&ocirc;ng Thương H&agrave; Nội tổ chức, c&ocirc;ng bố v&agrave; trao tặng.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Trong những năm qua, Century Stone lu&ocirc;n ch&uacute; trọng v&agrave; đồng bộ triển khai c&aacute;c giải ph&aacute;p trong việc tiết kiệm năng lượng hiệu quả, &iacute;t ph&aacute;t thải ra m&ocirc;i trường, t&aacute;i sử dụng ở mức đối đa trong c&aacute;c hoạt động sản xuất, kinh doanh của doanh nghiệp.</p>\r\n<p>Với việc sử dụng d&acirc;y chuyền sản xuất hiện đại bậc nhất được chuyển giao từ Breton (&Yacute;), c&ugrave;ng sự cải tiến đột ph&aacute; của đội ngũ kỹ sư c&ocirc;ng nghệ, to&agrave;n bộ qu&aacute; tr&igrave;nh sản xuất của Vicostone ho&agrave;n to&agrave;n kh&eacute;p k&iacute;n v&agrave; th&acirc;n thiện với m&ocirc;i trường, kh&ocirc;ng c&oacute; c&ocirc;ng đoạn nung n&ecirc;n đạt hiệu quả tiết kiệm năng lượng, kh&ocirc;ng c&oacute; kh&iacute; thải từ qu&aacute; tr&igrave;nh dưỡng hộ như c&aacute;c loại vật liệu ốp l&aacute;t được sản xuất theo c&ocirc;ng nghệ th&ocirc;ng thường</p>\r\n<p>B&ecirc;n cạnh đ&oacute;, Century Stone đ&atilde; triển khai v&agrave; li&ecirc;n tục mở rộng/ cải tiến kỹ thuật nhiều biện ph&aacute;p tiết kiệm năng lượng hiệu quả như:</p>\r\n<p>Thực hiện triển khai hệ thống điện năng lượng mặt trời tại 2 t&ograve;a nh&agrave; điều h&agrave;nh A1 v&agrave; A2 của C&ocirc;ng ty từ năm 2017 với 400 tấm pin quang điện được lắp đặt, c&oacute; thể cung cấp tới 90% điện năng cho hoạt động của hơn 200 c&aacute;n bộ, c&ocirc;ng nh&acirc;n vi&ecirc;n đang l&agrave;m việc tại 2 t&ograve;a nh&agrave;. Việc đưa tấm pin năng lượng mặt trời v&agrave;o hoạt động đ&atilde; gi&uacute;p sản lượng điện mỗi năm của Vicostone tiết kiệm l&ecirc;n đến tr&ecirc;n 500.000 kWh, tương đương với giảm ph&aacute;t thải tr&ecirc;n 200 tấn CO2 mỗi năm.<br />Thực hiện nhiều giải ph&aacute;p tiết kiệm năng lượng trong qu&aacute; tr&igrave;nh sản xuất,- ti&ecirc;u biểu như: Giải ph&aacute;p cho hệ thống chiếu s&aacute;ng; Giải ph&aacute;p cho hệ thống m&aacute;y n&eacute;n kh&iacute;; Giải ph&aacute;p lắp biến tần cho hệ thống bơm nước thải, bơm cấp nước c&ocirc;ng nghệ, giải ph&aacute;p cải tạo hệ thống sấy khu&ocirc;n, giải ph&aacute;p cải tạo hầm l&agrave;m m&aacute;t c&oacute; lắp biến tần điều khiển động cơ&hellip;<br />&Aacute;p dụng Hệ thống quản l&yacute; năng lượng theo ti&ecirc;u chuẩn ISO 50001: To&agrave;n bộ c&aacute;c c&ocirc;ng đoạn sử dụng năng lượng điện, gas, dầu diesel, nước sạch đều c&oacute; định mức sử dụng r&otilde; r&agrave;ng trong quy định, quy tr&igrave;nh ban h&agrave;nh; c&oacute; bộ phận chuy&ecirc;n m&ocirc;n theo d&otilde;i, gi&aacute;m s&aacute;t, ph&acirc;n t&iacute;ch dữ liệu năng lượng để cảnh b&aacute;o v&agrave; đưa ra c&aacute;c giải ph&aacute;p kịp thời về việc sử dụng năng lượng, tr&aacute;nh l&atilde;ng ph&iacute; m&agrave; vẫn đảm bảo sản xuất.<br />Qua những giải ph&aacute;p thiết thực tr&ecirc;n, Vicostone tự h&agrave;o g&oacute;p phần phần th&uacute;c đẩy c&ocirc;ng t&aacute;c quản l&yacute;, sử dụng năng lượng hiệu quả, n&acirc;ng cao hiệu suất sử dụng năng lượng của Th&agrave;nh phố H&agrave; Nội, tối đa sử dụng nguồn năng lượng xanh, hiện thực h&oacute;a cam kết ph&aacute;t triển bền vững v&agrave; tr&aacute;ch nhiệm v&igrave; cộng đồng x&atilde; hội.</p>\r\n<p>Vicostone thuộc Tập đo&agrave;n Phenikaa, một trong c&aacute;c nh&agrave; sản xuất h&agrave;ng đầu thế giới về đ&aacute; nh&acirc;n tạo gốc thạch anh cao cấp. Sản phẩm đ&atilde; c&oacute; mặt tại hơn 50 quốc gia, chinh phục c&aacute;c thị trường khắt khe như Mỹ, Canada, Australia, ch&acirc;u &Acirc;u...</p>\r\n<p>Với hơn 130 mẫu m&atilde;, m&agrave;u sắc, được thiết kế lấy &yacute; tưởng từ vẻ đẹp của thi&ecirc;n nhi&ecirc;n, mặt đ&aacute; truyền cảm hứng s&aacute;ng tạo cho kh&ocirc;ng gian sống, l&agrave; lựa chọn ph&ugrave; hợp để l&agrave;m b&agrave;n bếp đ&aacute; nh&acirc;n tạo, b&agrave;n tr&agrave;, b&agrave;n ăn, nh&agrave; tắm, decor nội thất để tạo điểm nhấn. Century Stone bảo h&agrave;nh 15 năm cho sản phẩm ch&iacute;nh h&atilde;ng với c&ocirc;ng tr&igrave;nh nh&agrave; ri&ecirc;ng, tham khảo tại: www.Century Stone.com/vi-vn</p>', NULL, '', '', '', '3-cong-nghe-san-xuat-da-thach-anh-nhan-tao-tot-nhat-hien-nay'),
	(8, 3, 2, '3 The best technology for producing artificial quartz stone today', 'On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.', '<p>On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Over the years, Century Stone has always focused and synchronously deployed solutions to save energy effectively, with low emissions to the environment, and reuse to the maximum extent in production and business activities. of the enterprise.</p>\r\n<p>With the use of the most modern production line transferred from Breton (Italy), along with the breakthrough improvement of a team of technology engineers, Vicostone\'s entire production process is completely self-contained and user-friendly. Environmentally friendly, there is no calcination process, so it is effective in saving energy, and there is no emission from the curing process like other types of paving materials produced by conventional technology.</p>\r\n<p>In addition, Century Stone has deployed and continuously expanded/improved many effective energy saving measures such as:</p>\r\n<p>Deploying the solar power system at the Company\'s 2 operating buildings A1 and A2 from 2017 with 400 installed photovoltaic panels, which can provide up to 90% of electricity for the operation of more than 200 officers and employees are working in 2 buildings. Putting solar panels into operation has helped Vicostone\'s annual electricity output save up to over 500,000 kWh, equivalent to reducing emissions of over 200 tons of CO2 per year.<br />Implement many solutions to save energy in the production process,- typically: Solutions for lighting system; Solutions for air compressor systems; Solutions to install inverters for wastewater pumping systems, technology water supply pumps, solutions to renovate mold drying systems, solutions to renovate cooling tunnels with motor control inverters...<br />Applying the Energy Management System according to ISO 50001: All stages of using electric energy, gas, diesel oil, clean water have clear usage norms in the promulgated regulations and procedures; has a specialized department to monitor, monitor and analyze energy data to warn and provide timely solutions on energy use, avoid waste while ensuring production.<br />Through these practical solutions, Vicostone is proud to contribute to promoting the management and efficient use of energy, improving the energy efficiency of Hanoi City, and maximizing the use of green energy sources. , realizing the commitment to sustainable development and responsibility for the social community.</p>\r\n<p>Vicostone is part of Phenikaa Group, one of the world\'s leading manufacturers of premium quartz-based artificial stone. Products have been present in more than 50 countries, conquering demanding markets such as the US, Canada, Australia, Europe...</p>\r\n<p>With more than 130 designs and colors, designed with inspiration from the beauty of nature, stone countertops inspire creativity for living space, is the right choice for making artificial stone kitchen table, tea table, etc. dining table, bathroom, interior decoration to create accents. Century Stone has a 15-year warranty for genuine products with private buildings, refer to: www.Century Stone.com/en-vn</p>', NULL, '', '', '', '3-the-best-technology-for-producing-artificial-quartz-stone-today'),
	(9, 3, 3, '3 当今生产人造石英石的最佳技术', '2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。', '<p>2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>多年来，世纪石始终专注并同步部署解决方案，以有效节约能源，对环境低排放，并在生产经营活动中最大程度地重复利用。的企业。</p>\r\n<p>使用从意大利布列塔尼转来的最现代化的生产线，再加上技术工程师团队的突破性改进，Vicostone的整个生产过程完全独立且人性化。环保，无需煅烧工序，节能效果显着，不会像传统工艺生产的其他类型铺路材料一样，在固化过程中产生废气。</p>\r\n<p>此外，世纪石还部署并不断扩展/改进了许多有效的节能措施，例如：</p>\r\n<p>从 2017 年开始，在公司的 2 栋运营大楼 A1 和 A2 部署太阳能发电系统，安装了 400 块光伏板，可为 2 栋大楼内 200 多名员工和员工的运营提供高达 90% 的电力。太阳能电池板投产后，Vicostone 的年发电量可节省超过 500,000 千瓦时，相当于每年减少 200 多吨二氧化碳的排放。<br />在生产过程中实施许多节能解决方案，通常是：照明系统解决方案；空压机系统解决方案；废水泵系统逆变器安装解决方案、技术供水泵、模具干燥系统改造解决方案、使用电机控制逆变器改造冷却隧道的解决方案......<br />应用符合ISO 50001的能源管理体系：在已颁布的法规和程序中，使用电能、燃气、柴油、清洁水的各个阶段都有明确的使用规范；设有专门部门对能源数据进行监测、监测和分析，对能源使用情况进行预警并及时提供解决方案，在确保生产的同时避免浪费。<br />通过这些切实可行的解决方案，Vicostone 很自豪能够为促进能源的管理和高效利用、提高河内市的能源效率以及最大限度地利用绿色能源做出贡献。 ，实现对可持续发展的承诺和对社会社区的责任。</p>\r\n<p>Vicostone 隶属于 Phenikaa 集团，该集团是世界领先的优质石英人造石制造商之一。产品已在 50 多个国家出现，征服了美国、加拿大、澳大利亚、欧洲等要求苛刻的市场&hellip;&hellip;</p>\r\n<p>130多种设计和颜色，灵感来自大自然之美，石材台面激发生活空间的创造力，是制作人造石厨房桌子、茶几等餐桌、浴室、室内装饰创造的正确选择口音。 Century Stone对私人建筑的正品提供15年保修，请参阅：www.Century Stone.com/en-vn</p>', NULL, '', '', '', '3-dang-jin-sheng-chan-ren-zao-shi-ying-shi-de-zui-jia-ji-zhu'),
	(10, 4, 1, 'Ưu điểm của đá thạch anh nhân tạo', 'Ngày 11/12/2021, Century Stone tự hào được công nhận là sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất công nghiệp (thứ hạng 5 sao) sau khi đã xuất sắc vượt qua 90 đơn vị trên địa bàn thành phố Hà Nội.', '<p>Ng&agrave;y 11/12/2021, Century Stone tự h&agrave;o được c&ocirc;ng nhận l&agrave; sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất c&ocirc;ng nghiệp (thứ hạng 5 sao) sau khi đ&atilde; xuất sắc vượt qua 90 đơn vị tr&ecirc;n địa b&agrave;n th&agrave;nh phố H&agrave; Nội. Đ&acirc;y l&agrave; giải thưởng vinh danh c&aacute;c cơ sở, c&ocirc;ng tr&igrave;nh x&acirc;y dựng sử dụng năng lượng xanh năm 2021 do Sở C&ocirc;ng Thương H&agrave; Nội tổ chức, c&ocirc;ng bố v&agrave; trao tặng.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Trong những năm qua, Century Stone lu&ocirc;n ch&uacute; trọng v&agrave; đồng bộ triển khai c&aacute;c giải ph&aacute;p trong việc tiết kiệm năng lượng hiệu quả, &iacute;t ph&aacute;t thải ra m&ocirc;i trường, t&aacute;i sử dụng ở mức đối đa trong c&aacute;c hoạt động sản xuất, kinh doanh của doanh nghiệp.</p>\r\n<p>Với việc sử dụng d&acirc;y chuyền sản xuất hiện đại bậc nhất được chuyển giao từ Breton (&Yacute;), c&ugrave;ng sự cải tiến đột ph&aacute; của đội ngũ kỹ sư c&ocirc;ng nghệ, to&agrave;n bộ qu&aacute; tr&igrave;nh sản xuất của Vicostone ho&agrave;n to&agrave;n kh&eacute;p k&iacute;n v&agrave; th&acirc;n thiện với m&ocirc;i trường, kh&ocirc;ng c&oacute; c&ocirc;ng đoạn nung n&ecirc;n đạt hiệu quả tiết kiệm năng lượng, kh&ocirc;ng c&oacute; kh&iacute; thải từ qu&aacute; tr&igrave;nh dưỡng hộ như c&aacute;c loại vật liệu ốp l&aacute;t được sản xuất theo c&ocirc;ng nghệ th&ocirc;ng thường</p>\r\n<p>B&ecirc;n cạnh đ&oacute;, Century Stone đ&atilde; triển khai v&agrave; li&ecirc;n tục mở rộng/ cải tiến kỹ thuật nhiều biện ph&aacute;p tiết kiệm năng lượng hiệu quả như:</p>\r\n<p>Thực hiện triển khai hệ thống điện năng lượng mặt trời tại 2 t&ograve;a nh&agrave; điều h&agrave;nh A1 v&agrave; A2 của C&ocirc;ng ty từ năm 2017 với 400 tấm pin quang điện được lắp đặt, c&oacute; thể cung cấp tới 90% điện năng cho hoạt động của hơn 200 c&aacute;n bộ, c&ocirc;ng nh&acirc;n vi&ecirc;n đang l&agrave;m việc tại 2 t&ograve;a nh&agrave;. Việc đưa tấm pin năng lượng mặt trời v&agrave;o hoạt động đ&atilde; gi&uacute;p sản lượng điện mỗi năm của Vicostone tiết kiệm l&ecirc;n đến tr&ecirc;n 500.000 kWh, tương đương với giảm ph&aacute;t thải tr&ecirc;n 200 tấn CO2 mỗi năm.<br />Thực hiện nhiều giải ph&aacute;p tiết kiệm năng lượng trong qu&aacute; tr&igrave;nh sản xuất,- ti&ecirc;u biểu như: Giải ph&aacute;p cho hệ thống chiếu s&aacute;ng; Giải ph&aacute;p cho hệ thống m&aacute;y n&eacute;n kh&iacute;; Giải ph&aacute;p lắp biến tần cho hệ thống bơm nước thải, bơm cấp nước c&ocirc;ng nghệ, giải ph&aacute;p cải tạo hệ thống sấy khu&ocirc;n, giải ph&aacute;p cải tạo hầm l&agrave;m m&aacute;t c&oacute; lắp biến tần điều khiển động cơ&hellip;<br />&Aacute;p dụng Hệ thống quản l&yacute; năng lượng theo ti&ecirc;u chuẩn ISO 50001: To&agrave;n bộ c&aacute;c c&ocirc;ng đoạn sử dụng năng lượng điện, gas, dầu diesel, nước sạch đều c&oacute; định mức sử dụng r&otilde; r&agrave;ng trong quy định, quy tr&igrave;nh ban h&agrave;nh; c&oacute; bộ phận chuy&ecirc;n m&ocirc;n theo d&otilde;i, gi&aacute;m s&aacute;t, ph&acirc;n t&iacute;ch dữ liệu năng lượng để cảnh b&aacute;o v&agrave; đưa ra c&aacute;c giải ph&aacute;p kịp thời về việc sử dụng năng lượng, tr&aacute;nh l&atilde;ng ph&iacute; m&agrave; vẫn đảm bảo sản xuất.<br />Qua những giải ph&aacute;p thiết thực tr&ecirc;n, Vicostone tự h&agrave;o g&oacute;p phần phần th&uacute;c đẩy c&ocirc;ng t&aacute;c quản l&yacute;, sử dụng năng lượng hiệu quả, n&acirc;ng cao hiệu suất sử dụng năng lượng của Th&agrave;nh phố H&agrave; Nội, tối đa sử dụng nguồn năng lượng xanh, hiện thực h&oacute;a cam kết ph&aacute;t triển bền vững v&agrave; tr&aacute;ch nhiệm v&igrave; cộng đồng x&atilde; hội.</p>\r\n<p>Vicostone thuộc Tập đo&agrave;n Phenikaa, một trong c&aacute;c nh&agrave; sản xuất h&agrave;ng đầu thế giới về đ&aacute; nh&acirc;n tạo gốc thạch anh cao cấp. Sản phẩm đ&atilde; c&oacute; mặt tại hơn 50 quốc gia, chinh phục c&aacute;c thị trường khắt khe như Mỹ, Canada, Australia, ch&acirc;u &Acirc;u...</p>\r\n<p>Với hơn 130 mẫu m&atilde;, m&agrave;u sắc, được thiết kế lấy &yacute; tưởng từ vẻ đẹp của thi&ecirc;n nhi&ecirc;n, mặt đ&aacute; truyền cảm hứng s&aacute;ng tạo cho kh&ocirc;ng gian sống, l&agrave; lựa chọn ph&ugrave; hợp để l&agrave;m b&agrave;n bếp đ&aacute; nh&acirc;n tạo, b&agrave;n tr&agrave;, b&agrave;n ăn, nh&agrave; tắm, decor nội thất để tạo điểm nhấn. Century Stone bảo h&agrave;nh 15 năm cho sản phẩm ch&iacute;nh h&atilde;ng với c&ocirc;ng tr&igrave;nh nh&agrave; ri&ecirc;ng, tham khảo tại: www.Century Stone.com/vi-vn</p>', NULL, '', '', '', 'uu-diem-cua-da-thach-anh-nhan-tao'),
	(11, 4, 2, 'Advantages of artificial quartz stone', 'On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city', '<p>On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Over the years, Century Stone has always focused and synchronously deployed solutions to save energy effectively, with low emissions to the environment, and reuse to the maximum extent in production and business activities. of the enterprise.</p>\r\n<p>With the use of the most modern production line transferred from Breton (Italy), along with the breakthrough improvement of a team of technology engineers, Vicostone\'s entire production process is completely self-contained and user-friendly. Environmentally friendly, there is no calcination process, so it is effective in saving energy, and there is no emission from the curing process like other types of paving materials produced by conventional technology.</p>\r\n<p>In addition, Century Stone has deployed and continuously expanded/improved many effective energy saving measures such as:</p>\r\n<p>Deploying the solar power system at the Company\'s 2 operating buildings A1 and A2 from 2017 with 400 installed photovoltaic panels, which can provide up to 90% of electricity for the operation of more than 200 officers and employees are working in 2 buildings. Putting solar panels into operation has helped Vicostone\'s annual electricity output save up to over 500,000 kWh, equivalent to reducing emissions of over 200 tons of CO2 per year.<br />Implement many solutions to save energy in the production process,- typically: Solutions for lighting system; Solutions for air compressor systems; Solutions to install inverters for wastewater pumping systems, technology water supply pumps, solutions to renovate mold drying systems, solutions to renovate cooling tunnels with motor control inverters...<br />Applying the Energy Management System according to ISO 50001: All stages of using electric energy, gas, diesel oil, clean water have clear usage norms in the promulgated regulations and procedures; has a specialized department to monitor, monitor and analyze energy data to warn and provide timely solutions on energy use, avoid waste while ensuring production.<br />Through these practical solutions, Vicostone is proud to contribute to promoting the management and efficient use of energy, improving the energy efficiency of Hanoi City, and maximizing the use of green energy sources. , realizing the commitment to sustainable development and responsibility for the social community.</p>\r\n<p>Vicostone is part of Phenikaa Group, one of the world\'s leading manufacturers of premium quartz-based artificial stone. Products have been present in more than 50 countries, conquering demanding markets such as the US, Canada, Australia, Europe...</p>\r\n<p>With more than 130 designs and colors, designed with inspiration from the beauty of nature, stone countertops inspire creativity for living space, is the right choice for making artificial stone kitchen table, tea table, etc. dining table, bathroom, interior decoration to create accents. Century Stone has a 15-year warranty for genuine products with private buildings, refer to: www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'advantages-of-artificial-quartz-stone'),
	(12, 4, 3, '人造石英石的优点', '2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星', '<p>2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>多年来，世纪石始终专注并同步部署解决方案，以有效节约能源，对环境低排放，并在生产经营活动中最大程度地重复利用。的企业。</p>\r\n<p>使用从意大利布列塔尼转来的最现代化的生产线，再加上技术工程师团队的突破性改进，Vicostone的整个生产过程完全独立且人性化。环保，无需煅烧工序，节能效果显着，不会像传统工艺生产的其他类型铺路材料一样，在固化过程中产生废气。</p>\r\n<p>此外，世纪石还部署并不断扩展/改进了许多有效的节能措施，例如：</p>\r\n<p>从 2017 年开始，在公司的 2 栋运营大楼 A1 和 A2 部署太阳能发电系统，安装了 400 块光伏板，可为 2 栋大楼内 200 多名员工和员工的运营提供高达 90% 的电力。太阳能电池板投产后，Vicostone 的年发电量可节省超过 500,000 千瓦时，相当于每年减少 200 多吨二氧化碳的排放。<br />在生产过程中实施许多节能解决方案，通常是：照明系统解决方案；空压机系统解决方案；废水泵系统逆变器安装解决方案、技术供水泵、模具干燥系统改造解决方案、使用电机控制逆变器改造冷却隧道的解决方案......<br />应用符合ISO 50001的能源管理体系：在已颁布的法规和程序中，使用电能、燃气、柴油、清洁水的各个阶段都有明确的使用规范；设有专门部门对能源数据进行监测、监测和分析，对能源使用情况进行预警并及时提供解决方案，在确保生产的同时避免浪费。<br />通过这些切实可行的解决方案，Vicostone 很自豪能够为促进能源的管理和高效利用、提高河内市的能源效率以及最大限度地利用绿色能源做出贡献。 ，实现对可持续发展的承诺和对社会社区的责任。</p>\r\n<p>Vicostone 隶属于 Phenikaa 集团，该集团是世界领先的优质石英人造石制造商之一。产品已在 50 多个国家出现，征服了美国、加拿大、澳大利亚、欧洲等要求苛刻的市场&hellip;&hellip;</p>\r\n<p>130多种设计和颜色，灵感来自大自然之美，石材台面激发生活空间的创造力，是制作人造石厨房桌子、茶几等餐桌、浴室、室内装饰创造的正确选择口音。 Century Stone对私人建筑的正品提供15年保修，请参阅：www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'ren-zao-shi-ying-shi-de-you-dian'),
	(13, 5, 1, 'Quy trình sản xuất đá thạch anh nhân tạo', 'Ngày 11/12/2021, Century Stone tự hào được công nhận là sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất công nghiệp (thứ hạng 5 sao) sau khi đã xuất sắc vượt qua 90 đơn vị trên địa bàn thành phố Hà Nội.', '<p>Ng&agrave;y 11/12/2021, Century Stone tự h&agrave;o được c&ocirc;ng nhận l&agrave; sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất c&ocirc;ng nghiệp (thứ hạng 5 sao) sau khi đ&atilde; xuất sắc vượt qua 90 đơn vị tr&ecirc;n địa b&agrave;n th&agrave;nh phố H&agrave; Nội. Đ&acirc;y l&agrave; giải thưởng vinh danh c&aacute;c cơ sở, c&ocirc;ng tr&igrave;nh x&acirc;y dựng sử dụng năng lượng xanh năm 2021 do Sở C&ocirc;ng Thương H&agrave; Nội tổ chức, c&ocirc;ng bố v&agrave; trao tặng.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Trong những năm qua, Century Stone lu&ocirc;n ch&uacute; trọng v&agrave; đồng bộ triển khai c&aacute;c giải ph&aacute;p trong việc tiết kiệm năng lượng hiệu quả, &iacute;t ph&aacute;t thải ra m&ocirc;i trường, t&aacute;i sử dụng ở mức đối đa trong c&aacute;c hoạt động sản xuất, kinh doanh của doanh nghiệp.</p>\r\n<p>Với việc sử dụng d&acirc;y chuyền sản xuất hiện đại bậc nhất được chuyển giao từ Breton (&Yacute;), c&ugrave;ng sự cải tiến đột ph&aacute; của đội ngũ kỹ sư c&ocirc;ng nghệ, to&agrave;n bộ qu&aacute; tr&igrave;nh sản xuất của Vicostone ho&agrave;n to&agrave;n kh&eacute;p k&iacute;n v&agrave; th&acirc;n thiện với m&ocirc;i trường, kh&ocirc;ng c&oacute; c&ocirc;ng đoạn nung n&ecirc;n đạt hiệu quả tiết kiệm năng lượng, kh&ocirc;ng c&oacute; kh&iacute; thải từ qu&aacute; tr&igrave;nh dưỡng hộ như c&aacute;c loại vật liệu ốp l&aacute;t được sản xuất theo c&ocirc;ng nghệ th&ocirc;ng thường</p>\r\n<p>B&ecirc;n cạnh đ&oacute;, Century Stone đ&atilde; triển khai v&agrave; li&ecirc;n tục mở rộng/ cải tiến kỹ thuật nhiều biện ph&aacute;p tiết kiệm năng lượng hiệu quả như:</p>\r\n<p>Thực hiện triển khai hệ thống điện năng lượng mặt trời tại 2 t&ograve;a nh&agrave; điều h&agrave;nh A1 v&agrave; A2 của C&ocirc;ng ty từ năm 2017 với 400 tấm pin quang điện được lắp đặt, c&oacute; thể cung cấp tới 90% điện năng cho hoạt động của hơn 200 c&aacute;n bộ, c&ocirc;ng nh&acirc;n vi&ecirc;n đang l&agrave;m việc tại 2 t&ograve;a nh&agrave;. Việc đưa tấm pin năng lượng mặt trời v&agrave;o hoạt động đ&atilde; gi&uacute;p sản lượng điện mỗi năm của Vicostone tiết kiệm l&ecirc;n đến tr&ecirc;n 500.000 kWh, tương đương với giảm ph&aacute;t thải tr&ecirc;n 200 tấn CO2 mỗi năm.<br />Thực hiện nhiều giải ph&aacute;p tiết kiệm năng lượng trong qu&aacute; tr&igrave;nh sản xuất,- ti&ecirc;u biểu như: Giải ph&aacute;p cho hệ thống chiếu s&aacute;ng; Giải ph&aacute;p cho hệ thống m&aacute;y n&eacute;n kh&iacute;; Giải ph&aacute;p lắp biến tần cho hệ thống bơm nước thải, bơm cấp nước c&ocirc;ng nghệ, giải ph&aacute;p cải tạo hệ thống sấy khu&ocirc;n, giải ph&aacute;p cải tạo hầm l&agrave;m m&aacute;t c&oacute; lắp biến tần điều khiển động cơ&hellip;<br />&Aacute;p dụng Hệ thống quản l&yacute; năng lượng theo ti&ecirc;u chuẩn ISO 50001: To&agrave;n bộ c&aacute;c c&ocirc;ng đoạn sử dụng năng lượng điện, gas, dầu diesel, nước sạch đều c&oacute; định mức sử dụng r&otilde; r&agrave;ng trong quy định, quy tr&igrave;nh ban h&agrave;nh; c&oacute; bộ phận chuy&ecirc;n m&ocirc;n theo d&otilde;i, gi&aacute;m s&aacute;t, ph&acirc;n t&iacute;ch dữ liệu năng lượng để cảnh b&aacute;o v&agrave; đưa ra c&aacute;c giải ph&aacute;p kịp thời về việc sử dụng năng lượng, tr&aacute;nh l&atilde;ng ph&iacute; m&agrave; vẫn đảm bảo sản xuất.<br />Qua những giải ph&aacute;p thiết thực tr&ecirc;n, Vicostone tự h&agrave;o g&oacute;p phần phần th&uacute;c đẩy c&ocirc;ng t&aacute;c quản l&yacute;, sử dụng năng lượng hiệu quả, n&acirc;ng cao hiệu suất sử dụng năng lượng của Th&agrave;nh phố H&agrave; Nội, tối đa sử dụng nguồn năng lượng xanh, hiện thực h&oacute;a cam kết ph&aacute;t triển bền vững v&agrave; tr&aacute;ch nhiệm v&igrave; cộng đồng x&atilde; hội.</p>\r\n<p>Vicostone thuộc Tập đo&agrave;n Phenikaa, một trong c&aacute;c nh&agrave; sản xuất h&agrave;ng đầu thế giới về đ&aacute; nh&acirc;n tạo gốc thạch anh cao cấp. Sản phẩm đ&atilde; c&oacute; mặt tại hơn 50 quốc gia, chinh phục c&aacute;c thị trường khắt khe như Mỹ, Canada, Australia, ch&acirc;u &Acirc;u...</p>\r\n<p>Với hơn 130 mẫu m&atilde;, m&agrave;u sắc, được thiết kế lấy &yacute; tưởng từ vẻ đẹp của thi&ecirc;n nhi&ecirc;n, mặt đ&aacute; truyền cảm hứng s&aacute;ng tạo cho kh&ocirc;ng gian sống, l&agrave; lựa chọn ph&ugrave; hợp để l&agrave;m b&agrave;n bếp đ&aacute; nh&acirc;n tạo, b&agrave;n tr&agrave;, b&agrave;n ăn, nh&agrave; tắm, decor nội thất để tạo điểm nhấn. Century Stone bảo h&agrave;nh 15 năm cho sản phẩm ch&iacute;nh h&atilde;ng với c&ocirc;ng tr&igrave;nh nh&agrave; ri&ecirc;ng, tham khảo tại: www.Century Stone.com/vi-vn</p>', NULL, '', '', '', 'quy-trinh-san-xuat-da-thach-anh-nhan-tao'),
	(14, 5, 2, 'Advantages of artificial quartz stone', 'On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.', '<p>On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Over the years, Century Stone has always focused and synchronously deployed solutions to save energy effectively, with low emissions to the environment, and reuse to the maximum extent in production and business activities. of the enterprise.</p>\r\n<p>With the use of the most modern production line transferred from Breton (Italy), along with the breakthrough improvement of a team of technology engineers, Vicostone\'s entire production process is completely self-contained and user-friendly. Environmentally friendly, there is no calcination process, so it is effective in saving energy, and there is no emission from the curing process like other types of paving materials produced by conventional technology.</p>\r\n<p>In addition, Century Stone has deployed and continuously expanded/improved many effective energy saving measures such as:</p>\r\n<p>Deploying the solar power system at the Company\'s 2 operating buildings A1 and A2 from 2017 with 400 installed photovoltaic panels, which can provide up to 90% of electricity for the operation of more than 200 officers and employees are working in 2 buildings. Putting solar panels into operation has helped Vicostone\'s annual electricity output save up to over 500,000 kWh, equivalent to reducing emissions of over 200 tons of CO2 per year.<br />Implement many solutions to save energy in the production process,- typically: Solutions for lighting system; Solutions for air compressor systems; Solutions to install inverters for wastewater pumping systems, technology water supply pumps, solutions to renovate mold drying systems, solutions to renovate cooling tunnels with motor control inverters...<br />Applying the Energy Management System according to ISO 50001: All stages of using electric energy, gas, diesel oil, clean water have clear usage norms in the promulgated regulations and procedures; has a specialized department to monitor, monitor and analyze energy data to warn and provide timely solutions on energy use, avoid waste while ensuring production.<br />Through these practical solutions, Vicostone is proud to contribute to promoting the management and efficient use of energy, improving the energy efficiency of Hanoi City, and maximizing the use of green energy sources. , realizing the commitment to sustainable development and responsibility for the social community.</p>\r\n<p>Vicostone is part of Phenikaa Group, one of the world\'s leading manufacturers of premium quartz-based artificial stone. Products have been present in more than 50 countries, conquering demanding markets such as the US, Canada, Australia, Europe...</p>\r\n<p>With more than 130 designs and colors, designed with inspiration from the beauty of nature, stone countertops inspire creativity for living space, is the right choice for making artificial stone kitchen table, tea table, etc. dining table, bathroom, interior decoration to create accents. Century Stone has a 15-year warranty for genuine products with private buildings, refer to: www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'advantages-of-artificial-quartz-stone'),
	(15, 5, 3, '人造石英石的优点', '', '<p>2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>多年来，世纪石始终专注并同步部署解决方案，以有效节约能源，对环境低排放，并在生产经营活动中最大程度地重复利用。的企业。</p>\r\n<p>使用从意大利布列塔尼转来的最现代化的生产线，再加上技术工程师团队的突破性改进，Vicostone的整个生产过程完全独立且人性化。环保，无需煅烧工序，节能效果显着，不会像传统工艺生产的其他类型铺路材料一样，在固化过程中产生废气。</p>\r\n<p>此外，世纪石还部署并不断扩展/改进了许多有效的节能措施，例如：</p>\r\n<p>从 2017 年开始，在公司的 2 栋运营大楼 A1 和 A2 部署太阳能发电系统，安装了 400 块光伏板，可为 2 栋大楼内 200 多名员工和员工的运营提供高达 90% 的电力。太阳能电池板投产后，Vicostone 的年发电量可节省超过 500,000 千瓦时，相当于每年减少 200 多吨二氧化碳的排放。<br />在生产过程中实施许多节能解决方案，通常是：照明系统解决方案；空压机系统解决方案；废水泵系统逆变器安装解决方案、技术供水泵、模具干燥系统改造解决方案、使用电机控制逆变器改造冷却隧道的解决方案......<br />应用符合ISO 50001的能源管理体系：在已颁布的法规和程序中，使用电能、燃气、柴油、清洁水的各个阶段都有明确的使用规范；设有专门部门对能源数据进行监测、监测和分析，对能源使用情况进行预警并及时提供解决方案，在确保生产的同时避免浪费。<br />通过这些切实可行的解决方案，Vicostone 很自豪能够为促进能源的管理和高效利用、提高河内市的能源效率以及最大限度地利用绿色能源做出贡献。 ，实现对可持续发展的承诺和对社会社区的责任。</p>\r\n<p>Vicostone 隶属于 Phenikaa 集团，该集团是世界领先的优质石英人造石制造商之一。产品已在 50 多个国家出现，征服了美国、加拿大、澳大利亚、欧洲等要求苛刻的市场&hellip;&hellip;</p>\r\n<p>130多种设计和颜色，灵感来自大自然之美，石材台面激发生活空间的创造力，是制作人造石厨房桌子、茶几等餐桌、浴室、室内装饰创造的正确选择口音。 Century Stone对私人建筑的正品提供15年保修，请参阅：www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'ren-zao-shi-ying-shi-de-you-dian'),
	(16, 6, 1, 'Tìm hiểu công nghệ, quy trình sản xuất đá nhân tạo gốc thạch anh', 'Ngày 11/12/2021, Century Stone tự hào được công nhận là sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất công nghiệp (thứ hạng 5 sao) sau khi đã xuất sắc vượt qua 90 đơn vị trên địa bàn thành phố Hà Nội.', '<p>Ng&agrave;y 11/12/2021, Century Stone tự h&agrave;o được c&ocirc;ng nhận l&agrave; sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất c&ocirc;ng nghiệp (thứ hạng 5 sao) sau khi đ&atilde; xuất sắc vượt qua 90 đơn vị tr&ecirc;n địa b&agrave;n th&agrave;nh phố H&agrave; Nội. Đ&acirc;y l&agrave; giải thưởng vinh danh c&aacute;c cơ sở, c&ocirc;ng tr&igrave;nh x&acirc;y dựng sử dụng năng lượng xanh năm 2021 do Sở C&ocirc;ng Thương H&agrave; Nội tổ chức, c&ocirc;ng bố v&agrave; trao tặng.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Trong những năm qua, Century Stone lu&ocirc;n ch&uacute; trọng v&agrave; đồng bộ triển khai c&aacute;c giải ph&aacute;p trong việc tiết kiệm năng lượng hiệu quả, &iacute;t ph&aacute;t thải ra m&ocirc;i trường, t&aacute;i sử dụng ở mức đối đa trong c&aacute;c hoạt động sản xuất, kinh doanh của doanh nghiệp.</p>\r\n<p>Với việc sử dụng d&acirc;y chuyền sản xuất hiện đại bậc nhất được chuyển giao từ Breton (&Yacute;), c&ugrave;ng sự cải tiến đột ph&aacute; của đội ngũ kỹ sư c&ocirc;ng nghệ, to&agrave;n bộ qu&aacute; tr&igrave;nh sản xuất của Vicostone ho&agrave;n to&agrave;n kh&eacute;p k&iacute;n v&agrave; th&acirc;n thiện với m&ocirc;i trường, kh&ocirc;ng c&oacute; c&ocirc;ng đoạn nung n&ecirc;n đạt hiệu quả tiết kiệm năng lượng, kh&ocirc;ng c&oacute; kh&iacute; thải từ qu&aacute; tr&igrave;nh dưỡng hộ như c&aacute;c loại vật liệu ốp l&aacute;t được sản xuất theo c&ocirc;ng nghệ th&ocirc;ng thường</p>\r\n<p>B&ecirc;n cạnh đ&oacute;, Century Stone đ&atilde; triển khai v&agrave; li&ecirc;n tục mở rộng/ cải tiến kỹ thuật nhiều biện ph&aacute;p tiết kiệm năng lượng hiệu quả như:</p>\r\n<p>Thực hiện triển khai hệ thống điện năng lượng mặt trời tại 2 t&ograve;a nh&agrave; điều h&agrave;nh A1 v&agrave; A2 của C&ocirc;ng ty từ năm 2017 với 400 tấm pin quang điện được lắp đặt, c&oacute; thể cung cấp tới 90% điện năng cho hoạt động của hơn 200 c&aacute;n bộ, c&ocirc;ng nh&acirc;n vi&ecirc;n đang l&agrave;m việc tại 2 t&ograve;a nh&agrave;. Việc đưa tấm pin năng lượng mặt trời v&agrave;o hoạt động đ&atilde; gi&uacute;p sản lượng điện mỗi năm của Vicostone tiết kiệm l&ecirc;n đến tr&ecirc;n 500.000 kWh, tương đương với giảm ph&aacute;t thải tr&ecirc;n 200 tấn CO2 mỗi năm.<br />Thực hiện nhiều giải ph&aacute;p tiết kiệm năng lượng trong qu&aacute; tr&igrave;nh sản xuất,- ti&ecirc;u biểu như: Giải ph&aacute;p cho hệ thống chiếu s&aacute;ng; Giải ph&aacute;p cho hệ thống m&aacute;y n&eacute;n kh&iacute;; Giải ph&aacute;p lắp biến tần cho hệ thống bơm nước thải, bơm cấp nước c&ocirc;ng nghệ, giải ph&aacute;p cải tạo hệ thống sấy khu&ocirc;n, giải ph&aacute;p cải tạo hầm l&agrave;m m&aacute;t c&oacute; lắp biến tần điều khiển động cơ&hellip;<br />&Aacute;p dụng Hệ thống quản l&yacute; năng lượng theo ti&ecirc;u chuẩn ISO 50001: To&agrave;n bộ c&aacute;c c&ocirc;ng đoạn sử dụng năng lượng điện, gas, dầu diesel, nước sạch đều c&oacute; định mức sử dụng r&otilde; r&agrave;ng trong quy định, quy tr&igrave;nh ban h&agrave;nh; c&oacute; bộ phận chuy&ecirc;n m&ocirc;n theo d&otilde;i, gi&aacute;m s&aacute;t, ph&acirc;n t&iacute;ch dữ liệu năng lượng để cảnh b&aacute;o v&agrave; đưa ra c&aacute;c giải ph&aacute;p kịp thời về việc sử dụng năng lượng, tr&aacute;nh l&atilde;ng ph&iacute; m&agrave; vẫn đảm bảo sản xuất.<br />Qua những giải ph&aacute;p thiết thực tr&ecirc;n, Vicostone tự h&agrave;o g&oacute;p phần phần th&uacute;c đẩy c&ocirc;ng t&aacute;c quản l&yacute;, sử dụng năng lượng hiệu quả, n&acirc;ng cao hiệu suất sử dụng năng lượng của Th&agrave;nh phố H&agrave; Nội, tối đa sử dụng nguồn năng lượng xanh, hiện thực h&oacute;a cam kết ph&aacute;t triển bền vững v&agrave; tr&aacute;ch nhiệm v&igrave; cộng đồng x&atilde; hội.</p>\r\n<p>Vicostone thuộc Tập đo&agrave;n Phenikaa, một trong c&aacute;c nh&agrave; sản xuất h&agrave;ng đầu thế giới về đ&aacute; nh&acirc;n tạo gốc thạch anh cao cấp. Sản phẩm đ&atilde; c&oacute; mặt tại hơn 50 quốc gia, chinh phục c&aacute;c thị trường khắt khe như Mỹ, Canada, Australia, ch&acirc;u &Acirc;u...</p>\r\n<p>Với hơn 130 mẫu m&atilde;, m&agrave;u sắc, được thiết kế lấy &yacute; tưởng từ vẻ đẹp của thi&ecirc;n nhi&ecirc;n, mặt đ&aacute; truyền cảm hứng s&aacute;ng tạo cho kh&ocirc;ng gian sống, l&agrave; lựa chọn ph&ugrave; hợp để l&agrave;m b&agrave;n bếp đ&aacute; nh&acirc;n tạo, b&agrave;n tr&agrave;, b&agrave;n ăn, nh&agrave; tắm, decor nội thất để tạo điểm nhấn. Century Stone bảo h&agrave;nh 15 năm cho sản phẩm ch&iacute;nh h&atilde;ng với c&ocirc;ng tr&igrave;nh nh&agrave; ri&ecirc;ng, tham khảo tại: www.Century Stone.com/vi-vn</p>', NULL, '', '', '', 'tim-hieu-cong-nghe-quy-trinh-san-xuat-da-nhan-tao-goc-thach-anh'),
	(17, 6, 2, 'Learn the technology and production process of quartz-based artificial stone', 'On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.', '<p>On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Over the years, Century Stone has always focused and synchronously deployed solutions to save energy effectively, with low emissions to the environment, and reuse to the maximum extent in production and business activities. of the enterprise.</p>\r\n<p>With the use of the most modern production line transferred from Breton (Italy), along with the breakthrough improvement of a team of technology engineers, Vicostone\'s entire production process is completely self-contained and user-friendly. Environmentally friendly, there is no calcination process, so it is effective in saving energy, and there is no emission from the curing process like other types of paving materials produced by conventional technology.</p>\r\n<p>In addition, Century Stone has deployed and continuously expanded/improved many effective energy saving measures such as:</p>\r\n<p>Deploying the solar power system at the Company\'s 2 operating buildings A1 and A2 from 2017 with 400 installed photovoltaic panels, which can provide up to 90% of electricity for the operation of more than 200 officers and employees are working in 2 buildings. Putting solar panels into operation has helped Vicostone\'s annual electricity output save up to over 500,000 kWh, equivalent to reducing emissions of over 200 tons of CO2 per year.<br />Implement many solutions to save energy in the production process,- typically: Solutions for lighting system; Solutions for air compressor systems; Solutions to install inverters for wastewater pumping systems, technology water supply pumps, solutions to renovate mold drying systems, solutions to renovate cooling tunnels with motor control inverters...<br />Applying the Energy Management System according to ISO 50001: All stages of using electric energy, gas, diesel oil, clean water have clear usage norms in the promulgated regulations and procedures; has a specialized department to monitor, monitor and analyze energy data to warn and provide timely solutions on energy use, avoid waste while ensuring production.<br />Through these practical solutions, Vicostone is proud to contribute to promoting the management and efficient use of energy, improving the energy efficiency of Hanoi City, and maximizing the use of green energy sources. , realizing the commitment to sustainable development and responsibility for the social community.</p>\r\n<p>Vicostone is part of Phenikaa Group, one of the world\'s leading manufacturers of premium quartz-based artificial stone. Products have been present in more than 50 countries, conquering demanding markets such as the US, Canada, Australia, Europe...</p>\r\n<p>With more than 130 designs and colors, designed with inspiration from the beauty of nature, stone countertops inspire creativity for living space, is the right choice for making artificial stone kitchen table, tea table, etc. dining table, bathroom, interior decoration to create accents. Century Stone has a 15-year warranty for genuine products with private buildings, refer to: www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'learn-the-technology-and-production-process-of-quartz-based-artificial-stone'),
	(18, 6, 3, '了解石英基人造石的技术和生产工艺', '2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。', '<p>2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>多年来，世纪石始终专注并同步部署解决方案，以有效节约能源，对环境低排放，并在生产经营活动中最大程度地重复利用。的企业。</p>\r\n<p>使用从意大利布列塔尼转来的最现代化的生产线，再加上技术工程师团队的突破性改进，Vicostone的整个生产过程完全独立且人性化。环保，无需煅烧工序，节能效果显着，不会像传统工艺生产的其他类型铺路材料一样，在固化过程中产生废气。</p>\r\n<p>此外，世纪石还部署并不断扩展/改进了许多有效的节能措施，例如：</p>\r\n<p>从 2017 年开始，在公司的 2 栋运营大楼 A1 和 A2 部署太阳能发电系统，安装了 400 块光伏板，可为 2 栋大楼内 200 多名员工和员工的运营提供高达 90% 的电力。太阳能电池板投产后，Vicostone 的年发电量可节省超过 500,000 千瓦时，相当于每年减少 200 多吨二氧化碳的排放。<br />在生产过程中实施许多节能解决方案，通常是：照明系统解决方案；空压机系统解决方案；废水泵系统逆变器安装解决方案、技术供水泵、模具干燥系统改造解决方案、使用电机控制逆变器改造冷却隧道的解决方案......<br />应用符合ISO 50001的能源管理体系：在已颁布的法规和程序中，使用电能、燃气、柴油、清洁水的各个阶段都有明确的使用规范；设有专门部门对能源数据进行监测、监测和分析，对能源使用情况进行预警并及时提供解决方案，在确保生产的同时避免浪费。<br />通过这些切实可行的解决方案，Vicostone 很自豪能够为促进能源的管理和高效利用、提高河内市的能源效率以及最大限度地利用绿色能源做出贡献。 ，实现对可持续发展的承诺和对社会社区的责任。</p>\r\n<p>Vicostone 隶属于 Phenikaa 集团，该集团是世界领先的优质石英人造石制造商之一。产品已在 50 多个国家出现，征服了美国、加拿大、澳大利亚、欧洲等要求苛刻的市场&hellip;&hellip;</p>\r\n<p>130多种设计和颜色，灵感来自大自然之美，石材台面激发生活空间的创造力，是制作人造石厨房桌子、茶几等餐桌、浴室、室内装饰创造的正确选择口音。 Century Stone对私人建筑的正品提供15年保修，请参阅：www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'liao-jie-shi-ying-ji-ren-zao-shi-de-ji-zhu-he-sheng-chan-gong-yi'),
	(19, 7, 1, 'Cách sử dụng máy đánh bóng đá cẩm thạch', 'Ngày 11/12/2021, Century Stone tự hào được công nhận là sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất công nghiệp (thứ hạng 5 sao) sau khi đã xuất sắc vượt qua 90 đơn vị trên địa bàn thành phố Hà Nội. Đây là giải thưởng vinh danh các cơ sở, công trình xây dựng sử dụng năng lượng xanh năm 2021 do Sở Công Thương Hà Nội tổ chức, công bố và trao tặng.', '<p>Ng&agrave;y 11/12/2021, Century Stone tự h&agrave;o được c&ocirc;ng nhận l&agrave; sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất c&ocirc;ng nghiệp (thứ hạng 5 sao) sau khi đ&atilde; xuất sắc vượt qua 90 đơn vị tr&ecirc;n địa b&agrave;n th&agrave;nh phố H&agrave; Nội. Đ&acirc;y l&agrave; giải thưởng vinh danh c&aacute;c cơ sở, c&ocirc;ng tr&igrave;nh x&acirc;y dựng sử dụng năng lượng xanh năm 2021 do Sở C&ocirc;ng Thương H&agrave; Nội tổ chức, c&ocirc;ng bố v&agrave; trao tặng.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Trong những năm qua, Century Stone lu&ocirc;n ch&uacute; trọng v&agrave; đồng bộ triển khai c&aacute;c giải ph&aacute;p trong việc tiết kiệm năng lượng hiệu quả, &iacute;t ph&aacute;t thải ra m&ocirc;i trường, t&aacute;i sử dụng ở mức đối đa trong c&aacute;c hoạt động sản xuất, kinh doanh của doanh nghiệp.</p>\r\n<p>Với việc sử dụng d&acirc;y chuyền sản xuất hiện đại bậc nhất được chuyển giao từ Breton (&Yacute;), c&ugrave;ng sự cải tiến đột ph&aacute; của đội ngũ kỹ sư c&ocirc;ng nghệ, to&agrave;n bộ qu&aacute; tr&igrave;nh sản xuất của Vicostone ho&agrave;n to&agrave;n kh&eacute;p k&iacute;n v&agrave; th&acirc;n thiện với m&ocirc;i trường, kh&ocirc;ng c&oacute; c&ocirc;ng đoạn nung n&ecirc;n đạt hiệu quả tiết kiệm năng lượng, kh&ocirc;ng c&oacute; kh&iacute; thải từ qu&aacute; tr&igrave;nh dưỡng hộ như c&aacute;c loại vật liệu ốp l&aacute;t được sản xuất theo c&ocirc;ng nghệ th&ocirc;ng thường</p>\r\n<p>B&ecirc;n cạnh đ&oacute;, Century Stone đ&atilde; triển khai v&agrave; li&ecirc;n tục mở rộng/ cải tiến kỹ thuật nhiều biện ph&aacute;p tiết kiệm năng lượng hiệu quả như:</p>\r\n<p>Thực hiện triển khai hệ thống điện năng lượng mặt trời tại 2 t&ograve;a nh&agrave; điều h&agrave;nh A1 v&agrave; A2 của C&ocirc;ng ty từ năm 2017 với 400 tấm pin quang điện được lắp đặt, c&oacute; thể cung cấp tới 90% điện năng cho hoạt động của hơn 200 c&aacute;n bộ, c&ocirc;ng nh&acirc;n vi&ecirc;n đang l&agrave;m việc tại 2 t&ograve;a nh&agrave;. Việc đưa tấm pin năng lượng mặt trời v&agrave;o hoạt động đ&atilde; gi&uacute;p sản lượng điện mỗi năm của Vicostone tiết kiệm l&ecirc;n đến tr&ecirc;n 500.000 kWh, tương đương với giảm ph&aacute;t thải tr&ecirc;n 200 tấn CO2 mỗi năm.<br />Thực hiện nhiều giải ph&aacute;p tiết kiệm năng lượng trong qu&aacute; tr&igrave;nh sản xuất,- ti&ecirc;u biểu như: Giải ph&aacute;p cho hệ thống chiếu s&aacute;ng; Giải ph&aacute;p cho hệ thống m&aacute;y n&eacute;n kh&iacute;; Giải ph&aacute;p lắp biến tần cho hệ thống bơm nước thải, bơm cấp nước c&ocirc;ng nghệ, giải ph&aacute;p cải tạo hệ thống sấy khu&ocirc;n, giải ph&aacute;p cải tạo hầm l&agrave;m m&aacute;t c&oacute; lắp biến tần điều khiển động cơ&hellip;<br />&Aacute;p dụng Hệ thống quản l&yacute; năng lượng theo ti&ecirc;u chuẩn ISO 50001: To&agrave;n bộ c&aacute;c c&ocirc;ng đoạn sử dụng năng lượng điện, gas, dầu diesel, nước sạch đều c&oacute; định mức sử dụng r&otilde; r&agrave;ng trong quy định, quy tr&igrave;nh ban h&agrave;nh; c&oacute; bộ phận chuy&ecirc;n m&ocirc;n theo d&otilde;i, gi&aacute;m s&aacute;t, ph&acirc;n t&iacute;ch dữ liệu năng lượng để cảnh b&aacute;o v&agrave; đưa ra c&aacute;c giải ph&aacute;p kịp thời về việc sử dụng năng lượng, tr&aacute;nh l&atilde;ng ph&iacute; m&agrave; vẫn đảm bảo sản xuất.<br />Qua những giải ph&aacute;p thiết thực tr&ecirc;n, Vicostone tự h&agrave;o g&oacute;p phần phần th&uacute;c đẩy c&ocirc;ng t&aacute;c quản l&yacute;, sử dụng năng lượng hiệu quả, n&acirc;ng cao hiệu suất sử dụng năng lượng của Th&agrave;nh phố H&agrave; Nội, tối đa sử dụng nguồn năng lượng xanh, hiện thực h&oacute;a cam kết ph&aacute;t triển bền vững v&agrave; tr&aacute;ch nhiệm v&igrave; cộng đồng x&atilde; hội.</p>\r\n<p>Vicostone thuộc Tập đo&agrave;n Phenikaa, một trong c&aacute;c nh&agrave; sản xuất h&agrave;ng đầu thế giới về đ&aacute; nh&acirc;n tạo gốc thạch anh cao cấp. Sản phẩm đ&atilde; c&oacute; mặt tại hơn 50 quốc gia, chinh phục c&aacute;c thị trường khắt khe như Mỹ, Canada, Australia, ch&acirc;u &Acirc;u...</p>\r\n<p>Với hơn 130 mẫu m&atilde;, m&agrave;u sắc, được thiết kế lấy &yacute; tưởng từ vẻ đẹp của thi&ecirc;n nhi&ecirc;n, mặt đ&aacute; truyền cảm hứng s&aacute;ng tạo cho kh&ocirc;ng gian sống, l&agrave; lựa chọn ph&ugrave; hợp để l&agrave;m b&agrave;n bếp đ&aacute; nh&acirc;n tạo, b&agrave;n tr&agrave;, b&agrave;n ăn, nh&agrave; tắm, decor nội thất để tạo điểm nhấn. Century Stone bảo h&agrave;nh 15 năm cho sản phẩm ch&iacute;nh h&atilde;ng với c&ocirc;ng tr&igrave;nh nh&agrave; ri&ecirc;ng, tham khảo tại: www.Century Stone.com/vi-vn</p>', NULL, '', '', '', 'cach-su-dung-may-danh-bong-da-cam-thach'),
	(20, 7, 2, 'How to use marble polishing machine', 'On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.', '<p>On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Over the years, Century Stone has always focused and synchronously deployed solutions to save energy effectively, with low emissions to the environment, and reuse to the maximum extent in production and business activities. of the enterprise.</p>\r\n<p>With the use of the most modern production line transferred from Breton (Italy), along with the breakthrough improvement of a team of technology engineers, Vicostone\'s entire production process is completely self-contained and user-friendly. Environmentally friendly, there is no calcination process, so it is effective in saving energy, and there is no emission from the curing process like other types of paving materials produced by conventional technology.</p>\r\n<p>In addition, Century Stone has deployed and continuously expanded/improved many effective energy saving measures such as:</p>\r\n<p>Deploying the solar power system at the Company\'s 2 operating buildings A1 and A2 from 2017 with 400 installed photovoltaic panels, which can provide up to 90% of electricity for the operation of more than 200 officers and employees are working in 2 buildings. Putting solar panels into operation has helped Vicostone\'s annual electricity output save up to over 500,000 kWh, equivalent to reducing emissions of over 200 tons of CO2 per year.<br />Implement many solutions to save energy in the production process,- typically: Solutions for lighting system; Solutions for air compressor systems; Solutions to install inverters for wastewater pumping systems, technology water supply pumps, solutions to renovate mold drying systems, solutions to renovate cooling tunnels with motor control inverters...<br />Applying the Energy Management System according to ISO 50001: All stages of using electric energy, gas, diesel oil, clean water have clear usage norms in the promulgated regulations and procedures; has a specialized department to monitor, monitor and analyze energy data to warn and provide timely solutions on energy use, avoid waste while ensuring production.<br />Through these practical solutions, Vicostone is proud to contribute to promoting the management and efficient use of energy, improving the energy efficiency of Hanoi City, and maximizing the use of green energy sources. , realizing the commitment to sustainable development and responsibility for the social community.</p>\r\n<p>Vicostone is part of Phenikaa Group, one of the world\'s leading manufacturers of premium quartz-based artificial stone. Products have been present in more than 50 countries, conquering demanding markets such as the US, Canada, Australia, Europe...</p>\r\n<p>With more than 130 designs and colors, designed with inspiration from the beauty of nature, stone countertops inspire creativity for living space, is the right choice for making artificial stone kitchen table, tea table, etc. dining table, bathroom, interior decoration to create accents. Century Stone has a 15-year warranty for genuine products with private buildings, refer to: www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'how-to-use-marble-polishing-machine'),
	(21, 7, 3, '大理石抛光机的使用方法', '', '<p>2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>多年来，世纪石始终专注并同步部署解决方案，以有效节约能源，对环境低排放，并在生产经营活动中最大程度地重复利用。的企业。</p>\r\n<p>使用从意大利布列塔尼转来的最现代化的生产线，再加上技术工程师团队的突破性改进，Vicostone的整个生产过程完全独立且人性化。环保，无需煅烧工序，节能效果显着，不会像传统工艺生产的其他类型铺路材料一样，在固化过程中产生废气。</p>\r\n<p>此外，世纪石还部署并不断扩展/改进了许多有效的节能措施，例如：</p>\r\n<p>从 2017 年开始，在公司的 2 栋运营大楼 A1 和 A2 部署太阳能发电系统，安装了 400 块光伏板，可为 2 栋大楼内 200 多名员工和员工的运营提供高达 90% 的电力。太阳能电池板投产后，Vicostone 的年发电量可节省超过 500,000 千瓦时，相当于每年减少 200 多吨二氧化碳的排放。<br />在生产过程中实施许多节能解决方案，通常是：照明系统解决方案；空压机系统解决方案；废水泵系统逆变器安装解决方案、技术供水泵、模具干燥系统改造解决方案、使用电机控制逆变器改造冷却隧道的解决方案......<br />应用符合ISO 50001的能源管理体系：在已颁布的法规和程序中，使用电能、燃气、柴油、清洁水的各个阶段都有明确的使用规范；设有专门部门对能源数据进行监测、监测和分析，对能源使用情况进行预警并及时提供解决方案，在确保生产的同时避免浪费。<br />通过这些切实可行的解决方案，Vicostone 很自豪能够为促进能源的管理和高效利用、提高河内市的能源效率以及最大限度地利用绿色能源做出贡献。 ，实现对可持续发展的承诺和对社会社区的责任。</p>\r\n<p>Vicostone 隶属于 Phenikaa 集团，该集团是世界领先的优质石英人造石制造商之一。产品已在 50 多个国家出现，征服了美国、加拿大、澳大利亚、欧洲等要求苛刻的市场&hellip;&hellip;</p>\r\n<p>130多种设计和颜色，灵感来自大自然之美，石材台面激发生活空间的创造力，是制作人造石厨房桌子、茶几等餐桌、浴室、室内装饰创造的正确选择口音。 Century Stone对私人建筑的正品提供15年保修，请参阅：www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'da-li-shi-pao-guang-ji-de-shi-yong-fang-fa'),
	(22, 8, 1, 'Những vấn đề cần chú ý trong dây chuyền sản xuất đá cẩm thạch nhân tạo', 'Ngày 11/12/2021, Century Stone tự hào được công nhận là sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất công nghiệp (thứ hạng 5 sao) sau khi đã xuất sắc vượt qua 90 đơn vị trên địa bàn thành phố Hà Nội. Đây là giải thưởng vinh danh các cơ sở, công trình xây dựng sử dụng năng lượng xanh năm 2021 do Sở Công Thương Hà Nội tổ chức, công bố và trao tặng.', '<p>Ng&agrave;y 11/12/2021, Century Stone tự h&agrave;o được c&ocirc;ng nhận l&agrave; sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất c&ocirc;ng nghiệp (thứ hạng 5 sao) sau khi đ&atilde; xuất sắc vượt qua 90 đơn vị tr&ecirc;n địa b&agrave;n th&agrave;nh phố H&agrave; Nội. Đ&acirc;y l&agrave; giải thưởng vinh danh c&aacute;c cơ sở, c&ocirc;ng tr&igrave;nh x&acirc;y dựng sử dụng năng lượng xanh năm 2021 do Sở C&ocirc;ng Thương H&agrave; Nội tổ chức, c&ocirc;ng bố v&agrave; trao tặng.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Trong những năm qua, Century Stone lu&ocirc;n ch&uacute; trọng v&agrave; đồng bộ triển khai c&aacute;c giải ph&aacute;p trong việc tiết kiệm năng lượng hiệu quả, &iacute;t ph&aacute;t thải ra m&ocirc;i trường, t&aacute;i sử dụng ở mức đối đa trong c&aacute;c hoạt động sản xuất, kinh doanh của doanh nghiệp.</p>\r\n<p>Với việc sử dụng d&acirc;y chuyền sản xuất hiện đại bậc nhất được chuyển giao từ Breton (&Yacute;), c&ugrave;ng sự cải tiến đột ph&aacute; của đội ngũ kỹ sư c&ocirc;ng nghệ, to&agrave;n bộ qu&aacute; tr&igrave;nh sản xuất của Vicostone ho&agrave;n to&agrave;n kh&eacute;p k&iacute;n v&agrave; th&acirc;n thiện với m&ocirc;i trường, kh&ocirc;ng c&oacute; c&ocirc;ng đoạn nung n&ecirc;n đạt hiệu quả tiết kiệm năng lượng, kh&ocirc;ng c&oacute; kh&iacute; thải từ qu&aacute; tr&igrave;nh dưỡng hộ như c&aacute;c loại vật liệu ốp l&aacute;t được sản xuất theo c&ocirc;ng nghệ th&ocirc;ng thường</p>\r\n<p>B&ecirc;n cạnh đ&oacute;, Century Stone đ&atilde; triển khai v&agrave; li&ecirc;n tục mở rộng/ cải tiến kỹ thuật nhiều biện ph&aacute;p tiết kiệm năng lượng hiệu quả như:</p>\r\n<p>Thực hiện triển khai hệ thống điện năng lượng mặt trời tại 2 t&ograve;a nh&agrave; điều h&agrave;nh A1 v&agrave; A2 của C&ocirc;ng ty từ năm 2017 với 400 tấm pin quang điện được lắp đặt, c&oacute; thể cung cấp tới 90% điện năng cho hoạt động của hơn 200 c&aacute;n bộ, c&ocirc;ng nh&acirc;n vi&ecirc;n đang l&agrave;m việc tại 2 t&ograve;a nh&agrave;. Việc đưa tấm pin năng lượng mặt trời v&agrave;o hoạt động đ&atilde; gi&uacute;p sản lượng điện mỗi năm của Vicostone tiết kiệm l&ecirc;n đến tr&ecirc;n 500.000 kWh, tương đương với giảm ph&aacute;t thải tr&ecirc;n 200 tấn CO2 mỗi năm.<br />Thực hiện nhiều giải ph&aacute;p tiết kiệm năng lượng trong qu&aacute; tr&igrave;nh sản xuất,- ti&ecirc;u biểu như: Giải ph&aacute;p cho hệ thống chiếu s&aacute;ng; Giải ph&aacute;p cho hệ thống m&aacute;y n&eacute;n kh&iacute;; Giải ph&aacute;p lắp biến tần cho hệ thống bơm nước thải, bơm cấp nước c&ocirc;ng nghệ, giải ph&aacute;p cải tạo hệ thống sấy khu&ocirc;n, giải ph&aacute;p cải tạo hầm l&agrave;m m&aacute;t c&oacute; lắp biến tần điều khiển động cơ&hellip;<br />&Aacute;p dụng Hệ thống quản l&yacute; năng lượng theo ti&ecirc;u chuẩn ISO 50001: To&agrave;n bộ c&aacute;c c&ocirc;ng đoạn sử dụng năng lượng điện, gas, dầu diesel, nước sạch đều c&oacute; định mức sử dụng r&otilde; r&agrave;ng trong quy định, quy tr&igrave;nh ban h&agrave;nh; c&oacute; bộ phận chuy&ecirc;n m&ocirc;n theo d&otilde;i, gi&aacute;m s&aacute;t, ph&acirc;n t&iacute;ch dữ liệu năng lượng để cảnh b&aacute;o v&agrave; đưa ra c&aacute;c giải ph&aacute;p kịp thời về việc sử dụng năng lượng, tr&aacute;nh l&atilde;ng ph&iacute; m&agrave; vẫn đảm bảo sản xuất.<br />Qua những giải ph&aacute;p thiết thực tr&ecirc;n, Vicostone tự h&agrave;o g&oacute;p phần phần th&uacute;c đẩy c&ocirc;ng t&aacute;c quản l&yacute;, sử dụng năng lượng hiệu quả, n&acirc;ng cao hiệu suất sử dụng năng lượng của Th&agrave;nh phố H&agrave; Nội, tối đa sử dụng nguồn năng lượng xanh, hiện thực h&oacute;a cam kết ph&aacute;t triển bền vững v&agrave; tr&aacute;ch nhiệm v&igrave; cộng đồng x&atilde; hội.</p>\r\n<p>Vicostone thuộc Tập đo&agrave;n Phenikaa, một trong c&aacute;c nh&agrave; sản xuất h&agrave;ng đầu thế giới về đ&aacute; nh&acirc;n tạo gốc thạch anh cao cấp. Sản phẩm đ&atilde; c&oacute; mặt tại hơn 50 quốc gia, chinh phục c&aacute;c thị trường khắt khe như Mỹ, Canada, Australia, ch&acirc;u &Acirc;u...</p>\r\n<p>Với hơn 130 mẫu m&atilde;, m&agrave;u sắc, được thiết kế lấy &yacute; tưởng từ vẻ đẹp của thi&ecirc;n nhi&ecirc;n, mặt đ&aacute; truyền cảm hứng s&aacute;ng tạo cho kh&ocirc;ng gian sống, l&agrave; lựa chọn ph&ugrave; hợp để l&agrave;m b&agrave;n bếp đ&aacute; nh&acirc;n tạo, b&agrave;n tr&agrave;, b&agrave;n ăn, nh&agrave; tắm, decor nội thất để tạo điểm nhấn. Century Stone bảo h&agrave;nh 15 năm cho sản phẩm ch&iacute;nh h&atilde;ng với c&ocirc;ng tr&igrave;nh nh&agrave; ri&ecirc;ng, tham khảo tại: www.Century Stone.com/vi-vn</p>', NULL, '', '', '', 'nhung-van-de-can-chu-y-trong-day-chuyen-san-xuat-da-cam-thach-nhan-tao'),
	(23, 8, 2, 'Matters needing attention in the artificial marble production line', 'On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.', '<p>On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Over the years, Century Stone has always focused and synchronously deployed solutions to save energy effectively, with low emissions to the environment, and reuse to the maximum extent in production and business activities. of the enterprise.</p>\r\n<p>With the use of the most modern production line transferred from Breton (Italy), along with the breakthrough improvement of a team of technology engineers, Vicostone\'s entire production process is completely self-contained and user-friendly. Environmentally friendly, there is no calcination process, so it is effective in saving energy, and there is no emission from the curing process like other types of paving materials produced by conventional technology.</p>\r\n<p>In addition, Century Stone has deployed and continuously expanded/improved many effective energy saving measures such as:</p>\r\n<p>Deploying the solar power system at the Company\'s 2 operating buildings A1 and A2 from 2017 with 400 installed photovoltaic panels, which can provide up to 90% of electricity for the operation of more than 200 officers and employees are working in 2 buildings. Putting solar panels into operation has helped Vicostone\'s annual electricity output save up to over 500,000 kWh, equivalent to reducing emissions of over 200 tons of CO2 per year.<br />Implement many solutions to save energy in the production process,- typically: Solutions for lighting system; Solutions for air compressor systems; Solutions to install inverters for wastewater pumping systems, technology water supply pumps, solutions to renovate mold drying systems, solutions to renovate cooling tunnels with motor control inverters...<br />Applying the Energy Management System according to ISO 50001: All stages of using electric energy, gas, diesel oil, clean water have clear usage norms in the promulgated regulations and procedures; has a specialized department to monitor, monitor and analyze energy data to warn and provide timely solutions on energy use, avoid waste while ensuring production.<br />Through these practical solutions, Vicostone is proud to contribute to promoting the management and efficient use of energy, improving the energy efficiency of Hanoi City, and maximizing the use of green energy sources. , realizing the commitment to sustainable development and responsibility for the social community.</p>\r\n<p>Vicostone is part of Phenikaa Group, one of the world\'s leading manufacturers of premium quartz-based artificial stone. Products have been present in more than 50 countries, conquering demanding markets such as the US, Canada, Australia, Europe...</p>\r\n<p>With more than 130 designs and colors, designed with inspiration from the beauty of nature, stone countertops inspire creativity for living space, is the right choice for making artificial stone kitchen table, tea table, etc. dining table, bathroom, interior decoration to create accents. Century Stone has a 15-year warranty for genuine products with private buildings, refer to: www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'matters-needing-attention-in-the-artificial-marble-production-line'),
	(24, 8, 3, '人造大理石生产线的注意事项', '2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。', '<p>2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>多年来，世纪石始终专注并同步部署解决方案，以有效节约能源，对环境低排放，并在生产经营活动中最大程度地重复利用。的企业。</p>\r\n<p>使用从意大利布列塔尼转来的最现代化的生产线，再加上技术工程师团队的突破性改进，Vicostone的整个生产过程完全独立且人性化。环保，无需煅烧工序，节能效果显着，不会像传统工艺生产的其他类型铺路材料一样，在固化过程中产生废气。</p>\r\n<p>此外，世纪石还部署并不断扩展/改进了许多有效的节能措施，例如：</p>\r\n<p>从 2017 年开始，在公司的 2 栋运营大楼 A1 和 A2 部署太阳能发电系统，安装了 400 块光伏板，可为 2 栋大楼内 200 多名员工和员工的运营提供高达 90% 的电力。太阳能电池板投产后，Vicostone 的年发电量可节省超过 500,000 千瓦时，相当于每年减少 200 多吨二氧化碳的排放。<br />在生产过程中实施许多节能解决方案，通常是：照明系统解决方案；空压机系统解决方案；废水泵系统逆变器安装解决方案、技术供水泵、模具干燥系统改造解决方案、使用电机控制逆变器改造冷却隧道的解决方案......<br />应用符合ISO 50001的能源管理体系：在已颁布的法规和程序中，使用电能、燃气、柴油、清洁水的各个阶段都有明确的使用规范；设有专门部门对能源数据进行监测、监测和分析，对能源使用情况进行预警并及时提供解决方案，在确保生产的同时避免浪费。<br />通过这些切实可行的解决方案，Vicostone 很自豪能够为促进能源的管理和高效利用、提高河内市的能源效率以及最大限度地利用绿色能源做出贡献。 ，实现对可持续发展的承诺和对社会社区的责任。</p>\r\n<p>Vicostone 隶属于 Phenikaa 集团，该集团是世界领先的优质石英人造石制造商之一。产品已在 50 多个国家出现，征服了美国、加拿大、澳大利亚、欧洲等要求苛刻的市场&hellip;&hellip;</p>\r\n<p>130多种设计和颜色，灵感来自大自然之美，石材台面激发生活空间的创造力，是制作人造石厨房桌子、茶几等餐桌、浴室、室内装饰创造的正确选择口音。 Century Stone对私人建筑的正品提供15年保修，请参阅：www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'ren-zao-da-li-shi-sheng-chan-xian-de-zhu-yi-shi-xiang'),
	(25, 9, 1, 'Đầu tư vào dây chuyền sản xuất đá nhân tạo, lợi ích là nhanh chóng', 'Ngày 11/12/2021, Century Stone tự hào được công nhận là Cơ sở sử dụng Năng lượng Xanh trong sản xuất công nghiệp', '<p>Ng&agrave;y 11/12/2021, Century Stone tự h&agrave;o được c&ocirc;ng nhận l&agrave; sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất c&ocirc;ng nghiệp (thứ hạng 5 sao) sau khi đ&atilde; xuất sắc vượt qua 90 đơn vị tr&ecirc;n địa b&agrave;n th&agrave;nh phố H&agrave; Nội. Đ&acirc;y l&agrave; giải thưởng vinh danh c&aacute;c cơ sở, c&ocirc;ng tr&igrave;nh x&acirc;y dựng sử dụng năng lượng xanh năm 2021 do Sở C&ocirc;ng Thương H&agrave; Nội tổ chức, c&ocirc;ng bố v&agrave; trao tặng.</p>\r\n<p>Trong những năm qua, Century Stone lu&ocirc;n ch&uacute; trọng v&agrave; đồng bộ triển khai c&aacute;c giải ph&aacute;p trong việc tiết kiệm năng lượng hiệu quả, &iacute;t ph&aacute;t thải ra m&ocirc;i trường, t&aacute;i sử dụng ở mức đối đa trong c&aacute;c hoạt động sản xuất, kinh doanh của doanh nghiệp.</p>\r\n<p>Với việc sử dụng d&acirc;y chuyền sản xuất hiện đại bậc nhất được chuyển giao từ Breton (&Yacute;), c&ugrave;ng sự cải tiến đột ph&aacute; của đội ngũ kỹ sư c&ocirc;ng nghệ, to&agrave;n bộ qu&aacute; tr&igrave;nh sản xuất của Vicostone ho&agrave;n to&agrave;n kh&eacute;p k&iacute;n v&agrave; th&acirc;n thiện với m&ocirc;i trường, kh&ocirc;ng c&oacute; c&ocirc;ng đoạn nung n&ecirc;n đạt hiệu quả tiết kiệm năng lượng, kh&ocirc;ng c&oacute; kh&iacute; thải từ qu&aacute; tr&igrave;nh dưỡng hộ như c&aacute;c loại vật liệu ốp l&aacute;t được sản xuất theo c&ocirc;ng nghệ th&ocirc;ng thường</p>\r\n<p>B&ecirc;n cạnh đ&oacute;, Century Stone đ&atilde; triển khai v&agrave; li&ecirc;n tục mở rộng/ cải tiến kỹ thuật nhiều biện ph&aacute;p tiết kiệm năng lượng hiệu quả như:</p>\r\n<p>Thực hiện triển khai hệ thống điện năng lượng mặt trời tại 2 t&ograve;a nh&agrave; điều h&agrave;nh A1 v&agrave; A2 của C&ocirc;ng ty từ năm 2017 với 400 tấm pin quang điện được lắp đặt, c&oacute; thể cung cấp tới 90% điện năng cho hoạt động của hơn 200 c&aacute;n bộ, c&ocirc;ng nh&acirc;n vi&ecirc;n đang l&agrave;m việc tại 2 t&ograve;a nh&agrave;. Việc đưa tấm pin năng lượng mặt trời v&agrave;o hoạt động đ&atilde; gi&uacute;p sản lượng điện mỗi năm của Vicostone tiết kiệm l&ecirc;n đến tr&ecirc;n 500.000 kWh, tương đương với giảm ph&aacute;t thải tr&ecirc;n 200 tấn CO2 mỗi năm.<br />Thực hiện nhiều giải ph&aacute;p tiết kiệm năng lượng trong qu&aacute; tr&igrave;nh sản xuất,- ti&ecirc;u biểu như: Giải ph&aacute;p cho hệ thống chiếu s&aacute;ng; Giải ph&aacute;p cho hệ thống m&aacute;y n&eacute;n kh&iacute;; Giải ph&aacute;p lắp biến tần cho hệ thống bơm nước thải, bơm cấp nước c&ocirc;ng nghệ, giải ph&aacute;p cải tạo hệ thống sấy khu&ocirc;n, giải ph&aacute;p cải tạo hầm l&agrave;m m&aacute;t c&oacute; lắp biến tần điều khiển động cơ&hellip;<br />&Aacute;p dụng Hệ thống quản l&yacute; năng lượng theo ti&ecirc;u chuẩn ISO 50001: To&agrave;n bộ c&aacute;c c&ocirc;ng đoạn sử dụng năng lượng điện, gas, dầu diesel, nước sạch đều c&oacute; định mức sử dụng r&otilde; r&agrave;ng trong quy định, quy tr&igrave;nh ban h&agrave;nh; c&oacute; bộ phận chuy&ecirc;n m&ocirc;n theo d&otilde;i, gi&aacute;m s&aacute;t, ph&acirc;n t&iacute;ch dữ liệu năng lượng để cảnh b&aacute;o v&agrave; đưa ra c&aacute;c giải ph&aacute;p kịp thời về việc sử dụng năng lượng, tr&aacute;nh l&atilde;ng ph&iacute; m&agrave; vẫn đảm bảo sản xuất.<br />Qua những giải ph&aacute;p thiết thực tr&ecirc;n, Vicostone tự h&agrave;o g&oacute;p phần phần th&uacute;c đẩy c&ocirc;ng t&aacute;c quản l&yacute;, sử dụng năng lượng hiệu quả, n&acirc;ng cao hiệu suất sử dụng năng lượng của Th&agrave;nh phố H&agrave; Nội, tối đa sử dụng nguồn năng lượng xanh, hiện thực h&oacute;a cam kết ph&aacute;t triển bền vững v&agrave; tr&aacute;ch nhiệm v&igrave; cộng đồng x&atilde; hội.</p>\r\n<p>Vicostone thuộc Tập đo&agrave;n Phenikaa, một trong c&aacute;c nh&agrave; sản xuất h&agrave;ng đầu thế giới về đ&aacute; nh&acirc;n tạo gốc thạch anh cao cấp. Sản phẩm đ&atilde; c&oacute; mặt tại hơn 50 quốc gia, chinh phục c&aacute;c thị trường khắt khe như Mỹ, Canada, Australia, ch&acirc;u &Acirc;u...</p>\r\n<p>Với hơn 130 mẫu m&atilde;, m&agrave;u sắc, được thiết kế lấy &yacute; tưởng từ vẻ đẹp của thi&ecirc;n nhi&ecirc;n, mặt đ&aacute; truyền cảm hứng s&aacute;ng tạo cho kh&ocirc;ng gian sống, l&agrave; lựa chọn ph&ugrave; hợp để l&agrave;m b&agrave;n bếp đ&aacute; nh&acirc;n tạo, b&agrave;n tr&agrave;, b&agrave;n ăn, nh&agrave; tắm, decor nội thất để tạo điểm nhấn. Century Stone bảo h&agrave;nh 15 năm cho sản phẩm ch&iacute;nh h&atilde;ng với c&ocirc;ng tr&igrave;nh nh&agrave; ri&ecirc;ng, tham khảo tại: www.Century Stone.com/vi-vn</p>', NULL, '', '', '', 'dau-tu-vao-day-chuyen-san-xuat-da-nhan-tao-loi-ich-la-nhanh-chong'),
	(26, 9, 2, 'Invest in artificial stone production line, the benefits are quick', 'On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.', '<p>On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.</p>\r\n<p>Over the years, Century Stone has always focused and synchronously deployed solutions to save energy effectively, with low emissions to the environment, and reuse to the maximum extent in production and business activities. of the enterprise.</p>\r\n<p>With the use of the most modern production line transferred from Breton (Italy), along with the breakthrough improvement of a team of technology engineers, Vicostone\'s entire production process is completely self-contained and user-friendly. Environmentally friendly, there is no calcination process, so it is effective in saving energy, and there is no emission from the curing process like other types of paving materials produced by conventional technology.</p>\r\n<p>In addition, Century Stone has deployed and continuously expanded/improved many effective energy saving measures such as:</p>\r\n<p>Deploying the solar power system at the Company\'s 2 operating buildings A1 and A2 from 2017 with 400 installed photovoltaic panels, which can provide up to 90% of electricity for the operation of more than 200 officers and employees are working in 2 buildings. Putting solar panels into operation has helped Vicostone\'s annual electricity output save up to over 500,000 kWh, equivalent to reducing emissions of over 200 tons of CO2 per year.<br />Implement many solutions to save energy in the production process,- typically: Solutions for lighting system; Solutions for air compressor systems; Solutions to install inverters for wastewater pumping systems, technology water supply pumps, solutions to renovate mold drying systems, solutions to renovate cooling tunnels with motor control inverters...<br />Applying the Energy Management System according to ISO 50001: All stages of using electric energy, gas, diesel oil, clean water have clear usage norms in the promulgated regulations and procedures; has a specialized department to monitor, monitor and analyze energy data to warn and provide timely solutions on energy use, avoid waste while ensuring production.<br />Through these practical solutions, Vicostone is proud to contribute to promoting the management and efficient use of energy, improving the energy efficiency of Hanoi City, and maximizing the use of green energy sources. , realizing the commitment to sustainable development and responsibility for the social community.</p>\r\n<p>Vicostone is part of Phenikaa Group, one of the world\'s leading manufacturers of premium quartz-based artificial stone. Products have been present in more than 50 countries, conquering demanding markets such as the US, Canada, Australia, Europe...</p>\r\n<p>With more than 130 designs and colors, designed with inspiration from the beauty of nature, stone countertops inspire creativity for living space, is the right choice for making artificial stone kitchen table, tea table, etc. dining table, bathroom, interior decoration to create accents. Century Stone has a 15-year warranty for genuine products with private buildings, refer to: www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'invest-in-artificial-stone-production-line-the-benefits-are-quick'),
	(27, 9, 3, '投资人造石生产线，见效快', '2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。', '<p>2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>多年来，世纪石始终专注并同步部署解决方案，以有效节约能源，对环境低排放，并在生产经营活动中最大程度地重复利用。的企业。</p>\r\n<p>使用从意大利布列塔尼转来的最现代化的生产线，再加上技术工程师团队的突破性改进，Vicostone的整个生产过程完全独立且人性化。环保，无需煅烧工序，节能效果显着，不会像传统工艺生产的其他类型铺路材料一样，在固化过程中产生废气。</p>\r\n<p>此外，世纪石还部署并不断扩展/改进了许多有效的节能措施，例如：</p>\r\n<p>从 2017 年开始，在公司的 2 栋运营大楼 A1 和 A2 部署太阳能发电系统，安装了 400 块光伏板，可为 2 栋大楼内 200 多名员工和员工的运营提供高达 90% 的电力。太阳能电池板投产后，Vicostone 的年发电量可节省超过 500,000 千瓦时，相当于每年减少 200 多吨二氧化碳的排放。<br />在生产过程中实施许多节能解决方案，通常是：照明系统解决方案；空压机系统解决方案；废水泵系统逆变器安装解决方案、技术供水泵、模具干燥系统改造解决方案、使用电机控制逆变器改造冷却隧道的解决方案......<br />应用符合ISO 50001的能源管理体系：在已颁布的法规和程序中，使用电能、燃气、柴油、清洁水的各个阶段都有明确的使用规范；设有专门部门对能源数据进行监测、监测和分析，对能源使用情况进行预警并及时提供解决方案，在确保生产的同时避免浪费。<br />通过这些切实可行的解决方案，Vicostone 很自豪能够为促进能源的管理和高效利用、提高河内市的能源效率以及最大限度地利用绿色能源做出贡献。 ，实现对可持续发展的承诺和对社会社区的责任。</p>\r\n<p>Vicostone 隶属于 Phenikaa 集团，该集团是世界领先的优质石英人造石制造商之一。产品已在 50 多个国家出现，征服了美国、加拿大、澳大利亚、欧洲等要求苛刻的市场&hellip;&hellip;</p>\r\n<p>130多种设计和颜色，灵感来自大自然之美，石材台面激发生活空间的创造力，是制作人造石厨房桌子、茶几等餐桌、浴室、室内装饰创造的正确选择口音。 Century Stone对私人建筑的正品提供15年保修，请参阅：www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'tou-zi-ren-zao-shi-sheng-chan-xian-jian-xiao-kuai'),
	(28, 10, 1, 'Các biện pháp phòng ngừa an toàn và các biện pháp bảo vệ cho máy nghiền di động ở nhiệt độ cao trong mùa hè', 'Ngày 11/12/2021, Century Stone tự hào được công nhận là sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất công nghiệp (thứ hạng 5 sao) sau khi đã xuất sắc vượt qua 90 đơn vị trên địa bàn thành phố Hà Nội. Đây là giải thưởng vinh danh các cơ sở, công trình xây dựng sử dụng năng lượng xanh năm 2021 do Sở Công Thương Hà Nội tổ chức, công bố và trao tặng.', '<p>Ng&agrave;y 11/12/2021, Century Stone tự h&agrave;o được c&ocirc;ng nhận l&agrave; sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất c&ocirc;ng nghiệp (thứ hạng 5 sao) sau khi đ&atilde; xuất sắc vượt qua 90 đơn vị tr&ecirc;n địa b&agrave;n th&agrave;nh phố H&agrave; Nội. Đ&acirc;y l&agrave; giải thưởng vinh danh c&aacute;c cơ sở, c&ocirc;ng tr&igrave;nh x&acirc;y dựng sử dụng năng lượng xanh năm 2021 do Sở C&ocirc;ng Thương H&agrave; Nội tổ chức, c&ocirc;ng bố v&agrave; trao tặng.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Trong những năm qua, Century Stone lu&ocirc;n ch&uacute; trọng v&agrave; đồng bộ triển khai c&aacute;c giải ph&aacute;p trong việc tiết kiệm năng lượng hiệu quả, &iacute;t ph&aacute;t thải ra m&ocirc;i trường, t&aacute;i sử dụng ở mức đối đa trong c&aacute;c hoạt động sản xuất, kinh doanh của doanh nghiệp.</p>\r\n<p>Với việc sử dụng d&acirc;y chuyền sản xuất hiện đại bậc nhất được chuyển giao từ Breton (&Yacute;), c&ugrave;ng sự cải tiến đột ph&aacute; của đội ngũ kỹ sư c&ocirc;ng nghệ, to&agrave;n bộ qu&aacute; tr&igrave;nh sản xuất của Vicostone ho&agrave;n to&agrave;n kh&eacute;p k&iacute;n v&agrave; th&acirc;n thiện với m&ocirc;i trường, kh&ocirc;ng c&oacute; c&ocirc;ng đoạn nung n&ecirc;n đạt hiệu quả tiết kiệm năng lượng, kh&ocirc;ng c&oacute; kh&iacute; thải từ qu&aacute; tr&igrave;nh dưỡng hộ như c&aacute;c loại vật liệu ốp l&aacute;t được sản xuất theo c&ocirc;ng nghệ th&ocirc;ng thường</p>\r\n<p>B&ecirc;n cạnh đ&oacute;, Century Stone đ&atilde; triển khai v&agrave; li&ecirc;n tục mở rộng/ cải tiến kỹ thuật nhiều biện ph&aacute;p tiết kiệm năng lượng hiệu quả như:</p>\r\n<p>Thực hiện triển khai hệ thống điện năng lượng mặt trời tại 2 t&ograve;a nh&agrave; điều h&agrave;nh A1 v&agrave; A2 của C&ocirc;ng ty từ năm 2017 với 400 tấm pin quang điện được lắp đặt, c&oacute; thể cung cấp tới 90% điện năng cho hoạt động của hơn 200 c&aacute;n bộ, c&ocirc;ng nh&acirc;n vi&ecirc;n đang l&agrave;m việc tại 2 t&ograve;a nh&agrave;. Việc đưa tấm pin năng lượng mặt trời v&agrave;o hoạt động đ&atilde; gi&uacute;p sản lượng điện mỗi năm của Vicostone tiết kiệm l&ecirc;n đến tr&ecirc;n 500.000 kWh, tương đương với giảm ph&aacute;t thải tr&ecirc;n 200 tấn CO2 mỗi năm.<br />Thực hiện nhiều giải ph&aacute;p tiết kiệm năng lượng trong qu&aacute; tr&igrave;nh sản xuất,- ti&ecirc;u biểu như: Giải ph&aacute;p cho hệ thống chiếu s&aacute;ng; Giải ph&aacute;p cho hệ thống m&aacute;y n&eacute;n kh&iacute;; Giải ph&aacute;p lắp biến tần cho hệ thống bơm nước thải, bơm cấp nước c&ocirc;ng nghệ, giải ph&aacute;p cải tạo hệ thống sấy khu&ocirc;n, giải ph&aacute;p cải tạo hầm l&agrave;m m&aacute;t c&oacute; lắp biến tần điều khiển động cơ&hellip;<br />&Aacute;p dụng Hệ thống quản l&yacute; năng lượng theo ti&ecirc;u chuẩn ISO 50001: To&agrave;n bộ c&aacute;c c&ocirc;ng đoạn sử dụng năng lượng điện, gas, dầu diesel, nước sạch đều c&oacute; định mức sử dụng r&otilde; r&agrave;ng trong quy định, quy tr&igrave;nh ban h&agrave;nh; c&oacute; bộ phận chuy&ecirc;n m&ocirc;n theo d&otilde;i, gi&aacute;m s&aacute;t, ph&acirc;n t&iacute;ch dữ liệu năng lượng để cảnh b&aacute;o v&agrave; đưa ra c&aacute;c giải ph&aacute;p kịp thời về việc sử dụng năng lượng, tr&aacute;nh l&atilde;ng ph&iacute; m&agrave; vẫn đảm bảo sản xuất.<br />Qua những giải ph&aacute;p thiết thực tr&ecirc;n, Vicostone tự h&agrave;o g&oacute;p phần phần th&uacute;c đẩy c&ocirc;ng t&aacute;c quản l&yacute;, sử dụng năng lượng hiệu quả, n&acirc;ng cao hiệu suất sử dụng năng lượng của Th&agrave;nh phố H&agrave; Nội, tối đa sử dụng nguồn năng lượng xanh, hiện thực h&oacute;a cam kết ph&aacute;t triển bền vững v&agrave; tr&aacute;ch nhiệm v&igrave; cộng đồng x&atilde; hội.</p>\r\n<p>Vicostone thuộc Tập đo&agrave;n Phenikaa, một trong c&aacute;c nh&agrave; sản xuất h&agrave;ng đầu thế giới về đ&aacute; nh&acirc;n tạo gốc thạch anh cao cấp. Sản phẩm đ&atilde; c&oacute; mặt tại hơn 50 quốc gia, chinh phục c&aacute;c thị trường khắt khe như Mỹ, Canada, Australia, ch&acirc;u &Acirc;u...</p>\r\n<p>Với hơn 130 mẫu m&atilde;, m&agrave;u sắc, được thiết kế lấy &yacute; tưởng từ vẻ đẹp của thi&ecirc;n nhi&ecirc;n, mặt đ&aacute; truyền cảm hứng s&aacute;ng tạo cho kh&ocirc;ng gian sống, l&agrave; lựa chọn ph&ugrave; hợp để l&agrave;m b&agrave;n bếp đ&aacute; nh&acirc;n tạo, b&agrave;n tr&agrave;, b&agrave;n ăn, nh&agrave; tắm, decor nội thất để tạo điểm nhấn. Century Stone bảo h&agrave;nh 15 năm cho sản phẩm ch&iacute;nh h&atilde;ng với c&ocirc;ng tr&igrave;nh nh&agrave; ri&ecirc;ng, tham khảo tại: www.Century Stone.com/vi-vn</p>', NULL, '', '', '', 'cac-bien-phap-phong-ngua-an-toan-va-cac-bien-phap-bao-ve-cho-may-nghien-di-dong-o-nhiet-do-cao-trong-mua-he'),
	(29, 10, 2, 'Safety precautions and protective measures for high temperature mobile crusher in summer', 'On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.', '<p>On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Over the years, Century Stone has always focused and synchronously deployed solutions to save energy effectively, with low emissions to the environment, and reuse to the maximum extent in production and business activities. of the enterprise.</p>\r\n<p>With the use of the most modern production line transferred from Breton (Italy), along with the breakthrough improvement of a team of technology engineers, Vicostone\'s entire production process is completely self-contained and user-friendly. Environmentally friendly, there is no calcination process, so it is effective in saving energy, and there is no emission from the curing process like other types of paving materials produced by conventional technology.</p>\r\n<p>In addition, Century Stone has deployed and continuously expanded/improved many effective energy saving measures such as:</p>\r\n<p>Deploying the solar power system at the Company\'s 2 operating buildings A1 and A2 from 2017 with 400 installed photovoltaic panels, which can provide up to 90% of electricity for the operation of more than 200 officers and employees are working in 2 buildings. Putting solar panels into operation has helped Vicostone\'s annual electricity output save up to over 500,000 kWh, equivalent to reducing emissions of over 200 tons of CO2 per year.<br />Implement many solutions to save energy in the production process,- typically: Solutions for lighting system; Solutions for air compressor systems; Solutions to install inverters for wastewater pumping systems, technology water supply pumps, solutions to renovate mold drying systems, solutions to renovate cooling tunnels with motor control inverters...<br />Applying the Energy Management System according to ISO 50001: All stages of using electric energy, gas, diesel oil, clean water have clear usage norms in the promulgated regulations and procedures; has a specialized department to monitor, monitor and analyze energy data to warn and provide timely solutions on energy use, avoid waste while ensuring production.<br />Through these practical solutions, Vicostone is proud to contribute to promoting the management and efficient use of energy, improving the energy efficiency of Hanoi City, and maximizing the use of green energy sources. , realizing the commitment to sustainable development and responsibility for the social community.</p>\r\n<p>Vicostone is part of Phenikaa Group, one of the world\'s leading manufacturers of premium quartz-based artificial stone. Products have been present in more than 50 countries, conquering demanding markets such as the US, Canada, Australia, Europe...</p>\r\n<p>With more than 130 designs and colors, designed with inspiration from the beauty of nature, stone countertops inspire creativity for living space, is the right choice for making artificial stone kitchen table, tea table, etc. dining table, bathroom, interior decoration to create accents. Century Stone has a 15-year warranty for genuine products with private buildings, refer to: www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'safety-precautions-and-protective-measures-for-high-temperature-mobile-crusher-in-summer'),
	(30, 10, 3, '夏季高温移动式破碎机的安全注意事项及防护措施', '2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。', '<p>2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>多年来，世纪石始终专注并同步部署解决方案，以有效节约能源，对环境低排放，并在生产经营活动中最大程度地重复利用。的企业。</p>\r\n<p>使用从意大利布列塔尼转来的最现代化的生产线，再加上技术工程师团队的突破性改进，Vicostone的整个生产过程完全独立且人性化。环保，无需煅烧工序，节能效果显着，不会像传统工艺生产的其他类型铺路材料一样，在固化过程中产生废气。</p>\r\n<p>此外，世纪石还部署并不断扩展/改进了许多有效的节能措施，例如：</p>\r\n<p>从 2017 年开始，在公司的 2 栋运营大楼 A1 和 A2 部署太阳能发电系统，安装了 400 块光伏板，可为 2 栋大楼内 200 多名员工和员工的运营提供高达 90% 的电力。太阳能电池板投产后，Vicostone 的年发电量可节省超过 500,000 千瓦时，相当于每年减少 200 多吨二氧化碳的排放。<br />在生产过程中实施许多节能解决方案，通常是：照明系统解决方案；空压机系统解决方案；废水泵系统逆变器安装解决方案、技术供水泵、模具干燥系统改造解决方案、使用电机控制逆变器改造冷却隧道的解决方案......<br />应用符合ISO 50001的能源管理体系：在已颁布的法规和程序中，使用电能、燃气、柴油、清洁水的各个阶段都有明确的使用规范；设有专门部门对能源数据进行监测、监测和分析，对能源使用情况进行预警并及时提供解决方案，在确保生产的同时避免浪费。<br />通过这些切实可行的解决方案，Vicostone 很自豪能够为促进能源的管理和高效利用、提高河内市的能源效率以及最大限度地利用绿色能源做出贡献。 ，实现对可持续发展的承诺和对社会社区的责任。</p>\r\n<p>Vicostone 隶属于 Phenikaa 集团，该集团是世界领先的优质石英人造石制造商之一。产品已在 50 多个国家出现，征服了美国、加拿大、澳大利亚、欧洲等要求苛刻的市场&hellip;&hellip;</p>\r\n<p>130多种设计和颜色，灵感来自大自然之美，石材台面激发生活空间的创造力，是制作人造石厨房桌子、茶几等餐桌、浴室、室内装饰创造的正确选择口音。 Century Stone对私人建筑的正品提供15年保修，请参阅：www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'xia-ji-gao-wen-yi-dong-shi-po-sui-ji-de-an-quan-zhu-yi-shi-xiang-ji-fang-hu-cuo-shi'),
	(31, 11, 1, 'Sự phát triển tương lai của đá thạch anh là gì?', 'Ngày 11/12/2021, Century Stone tự hào được công nhận là sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất công nghiệp', '<p>Ng&agrave;y 11/12/2021, Century Stone tự h&agrave;o được c&ocirc;ng nhận l&agrave; sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất c&ocirc;ng nghiệp (thứ hạng 5 sao) sau khi đ&atilde; xuất sắc vượt qua 90 đơn vị tr&ecirc;n địa b&agrave;n th&agrave;nh phố H&agrave; Nội. Đ&acirc;y l&agrave; giải thưởng vinh danh c&aacute;c cơ sở, c&ocirc;ng tr&igrave;nh x&acirc;y dựng sử dụng năng lượng xanh năm 2021 do Sở C&ocirc;ng Thương H&agrave; Nội tổ chức, c&ocirc;ng bố v&agrave; trao tặng.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Trong những năm qua, Century Stone lu&ocirc;n ch&uacute; trọng v&agrave; đồng bộ triển khai c&aacute;c giải ph&aacute;p trong việc tiết kiệm năng lượng hiệu quả, &iacute;t ph&aacute;t thải ra m&ocirc;i trường, t&aacute;i sử dụng ở mức đối đa trong c&aacute;c hoạt động sản xuất, kinh doanh của doanh nghiệp.</p>\r\n<p>Với việc sử dụng d&acirc;y chuyền sản xuất hiện đại bậc nhất được chuyển giao từ Breton (&Yacute;), c&ugrave;ng sự cải tiến đột ph&aacute; của đội ngũ kỹ sư c&ocirc;ng nghệ, to&agrave;n bộ qu&aacute; tr&igrave;nh sản xuất của Vicostone ho&agrave;n to&agrave;n kh&eacute;p k&iacute;n v&agrave; th&acirc;n thiện với m&ocirc;i trường, kh&ocirc;ng c&oacute; c&ocirc;ng đoạn nung n&ecirc;n đạt hiệu quả tiết kiệm năng lượng, kh&ocirc;ng c&oacute; kh&iacute; thải từ qu&aacute; tr&igrave;nh dưỡng hộ như c&aacute;c loại vật liệu ốp l&aacute;t được sản xuất theo c&ocirc;ng nghệ th&ocirc;ng thường</p>\r\n<p>B&ecirc;n cạnh đ&oacute;, Century Stone đ&atilde; triển khai v&agrave; li&ecirc;n tục mở rộng/ cải tiến kỹ thuật nhiều biện ph&aacute;p tiết kiệm năng lượng hiệu quả như:</p>\r\n<p>Thực hiện triển khai hệ thống điện năng lượng mặt trời tại 2 t&ograve;a nh&agrave; điều h&agrave;nh A1 v&agrave; A2 của C&ocirc;ng ty từ năm 2017 với 400 tấm pin quang điện được lắp đặt, c&oacute; thể cung cấp tới 90% điện năng cho hoạt động của hơn 200 c&aacute;n bộ, c&ocirc;ng nh&acirc;n vi&ecirc;n đang l&agrave;m việc tại 2 t&ograve;a nh&agrave;. Việc đưa tấm pin năng lượng mặt trời v&agrave;o hoạt động đ&atilde; gi&uacute;p sản lượng điện mỗi năm của Vicostone tiết kiệm l&ecirc;n đến tr&ecirc;n 500.000 kWh, tương đương với giảm ph&aacute;t thải tr&ecirc;n 200 tấn CO2 mỗi năm.<br />Thực hiện nhiều giải ph&aacute;p tiết kiệm năng lượng trong qu&aacute; tr&igrave;nh sản xuất,- ti&ecirc;u biểu như: Giải ph&aacute;p cho hệ thống chiếu s&aacute;ng; Giải ph&aacute;p cho hệ thống m&aacute;y n&eacute;n kh&iacute;; Giải ph&aacute;p lắp biến tần cho hệ thống bơm nước thải, bơm cấp nước c&ocirc;ng nghệ, giải ph&aacute;p cải tạo hệ thống sấy khu&ocirc;n, giải ph&aacute;p cải tạo hầm l&agrave;m m&aacute;t c&oacute; lắp biến tần điều khiển động cơ&hellip;<br />&Aacute;p dụng Hệ thống quản l&yacute; năng lượng theo ti&ecirc;u chuẩn ISO 50001: To&agrave;n bộ c&aacute;c c&ocirc;ng đoạn sử dụng năng lượng điện, gas, dầu diesel, nước sạch đều c&oacute; định mức sử dụng r&otilde; r&agrave;ng trong quy định, quy tr&igrave;nh ban h&agrave;nh; c&oacute; bộ phận chuy&ecirc;n m&ocirc;n theo d&otilde;i, gi&aacute;m s&aacute;t, ph&acirc;n t&iacute;ch dữ liệu năng lượng để cảnh b&aacute;o v&agrave; đưa ra c&aacute;c giải ph&aacute;p kịp thời về việc sử dụng năng lượng, tr&aacute;nh l&atilde;ng ph&iacute; m&agrave; vẫn đảm bảo sản xuất.<br />Qua những giải ph&aacute;p thiết thực tr&ecirc;n, Vicostone tự h&agrave;o g&oacute;p phần phần th&uacute;c đẩy c&ocirc;ng t&aacute;c quản l&yacute;, sử dụng năng lượng hiệu quả, n&acirc;ng cao hiệu suất sử dụng năng lượng của Th&agrave;nh phố H&agrave; Nội, tối đa sử dụng nguồn năng lượng xanh, hiện thực h&oacute;a cam kết ph&aacute;t triển bền vững v&agrave; tr&aacute;ch nhiệm v&igrave; cộng đồng x&atilde; hội.</p>\r\n<p>Vicostone thuộc Tập đo&agrave;n Phenikaa, một trong c&aacute;c nh&agrave; sản xuất h&agrave;ng đầu thế giới về đ&aacute; nh&acirc;n tạo gốc thạch anh cao cấp. Sản phẩm đ&atilde; c&oacute; mặt tại hơn 50 quốc gia, chinh phục c&aacute;c thị trường khắt khe như Mỹ, Canada, Australia, ch&acirc;u &Acirc;u...</p>\r\n<p>Với hơn 130 mẫu m&atilde;, m&agrave;u sắc, được thiết kế lấy &yacute; tưởng từ vẻ đẹp của thi&ecirc;n nhi&ecirc;n, mặt đ&aacute; truyền cảm hứng s&aacute;ng tạo cho kh&ocirc;ng gian sống, l&agrave; lựa chọn ph&ugrave; hợp để l&agrave;m b&agrave;n bếp đ&aacute; nh&acirc;n tạo, b&agrave;n tr&agrave;, b&agrave;n ăn, nh&agrave; tắm, decor nội thất để tạo điểm nhấn. Century Stone bảo h&agrave;nh 15 năm cho sản phẩm ch&iacute;nh h&atilde;ng với c&ocirc;ng tr&igrave;nh nh&agrave; ri&ecirc;ng, tham khảo tại: www.Century Stone.com/vi-vn</p>', NULL, '', '', '', 'su-phat-trien-tuong-lai-cua-da-thach-anh-la-gi'),
	(32, 11, 2, 'What is the future development of quartz stone?', 'On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production', '<p>On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Over the years, Century Stone has always focused and synchronously deployed solutions to save energy effectively, with low emissions to the environment, and reuse to the maximum extent in production and business activities. of the enterprise.</p>\r\n<p>With the use of the most modern production line transferred from Breton (Italy), along with the breakthrough improvement of a team of technology engineers, Vicostone\'s entire production process is completely self-contained and user-friendly. Environmentally friendly, there is no calcination process, so it is effective in saving energy, and there is no emission from the curing process like other types of paving materials produced by conventional technology.</p>\r\n<p>In addition, Century Stone has deployed and continuously expanded/improved many effective energy saving measures such as:</p>\r\n<p>Deploying the solar power system at the Company\'s 2 operating buildings A1 and A2 from 2017 with 400 installed photovoltaic panels, which can provide up to 90% of electricity for the operation of more than 200 officers and employees are working in 2 buildings. Putting solar panels into operation has helped Vicostone\'s annual electricity output save up to over 500,000 kWh, equivalent to reducing emissions of over 200 tons of CO2 per year.<br />Implement many solutions to save energy in the production process,- typically: Solutions for lighting system; Solutions for air compressor systems; Solutions to install inverters for wastewater pumping systems, technology water supply pumps, solutions to renovate mold drying systems, solutions to renovate cooling tunnels with motor control inverters...<br />Applying the Energy Management System according to ISO 50001: All stages of using electric energy, gas, diesel oil, clean water have clear usage norms in the promulgated regulations and procedures; has a specialized department to monitor, monitor and analyze energy data to warn and provide timely solutions on energy use, avoid waste while ensuring production.<br />Through these practical solutions, Vicostone is proud to contribute to promoting the management and efficient use of energy, improving the energy efficiency of Hanoi City, and maximizing the use of green energy sources. , realizing the commitment to sustainable development and responsibility for the social community.</p>\r\n<p>Vicostone is part of Phenikaa Group, one of the world\'s leading manufacturers of premium quartz-based artificial stone. Products have been present in more than 50 countries, conquering demanding markets such as the US, Canada, Australia, Europe...</p>\r\n<p>With more than 130 designs and colors, designed with inspiration from the beauty of nature, stone countertops inspire creativity for living space, is the right choice for making artificial stone kitchen table, tea table, etc. dining table, bathroom, interior decoration to create accents. Century Stone has a 15-year warranty for genuine products with private buildings, refer to: www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'what-is-the-future-development-of-quartz-stone'),
	(33, 11, 3, '石英石未来的发展方向是什么？', '2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。', '<p>2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>多年来，世纪石始终专注并同步部署解决方案，以有效节约能源，对环境低排放，并在生产经营活动中最大程度地重复利用。的企业。</p>\r\n<p>使用从意大利布列塔尼转来的最现代化的生产线，再加上技术工程师团队的突破性改进，Vicostone的整个生产过程完全独立且人性化。环保，无需煅烧工序，节能效果显着，不会像传统工艺生产的其他类型铺路材料一样，在固化过程中产生废气。</p>\r\n<p>此外，世纪石还部署并不断扩展/改进了许多有效的节能措施，例如：</p>\r\n<p>从 2017 年开始，在公司的 2 栋运营大楼 A1 和 A2 部署太阳能发电系统，安装了 400 块光伏板，可为 2 栋大楼内 200 多名员工和员工的运营提供高达 90% 的电力。太阳能电池板投产后，Vicostone 的年发电量可节省超过 500,000 千瓦时，相当于每年减少 200 多吨二氧化碳的排放。<br />在生产过程中实施许多节能解决方案，通常是：照明系统解决方案；空压机系统解决方案；废水泵系统逆变器安装解决方案、技术供水泵、模具干燥系统改造解决方案、使用电机控制逆变器改造冷却隧道的解决方案......<br />应用符合ISO 50001的能源管理体系：在已颁布的法规和程序中，使用电能、燃气、柴油、清洁水的各个阶段都有明确的使用规范；设有专门部门对能源数据进行监测、监测和分析，对能源使用情况进行预警并及时提供解决方案，在确保生产的同时避免浪费。<br />通过这些切实可行的解决方案，Vicostone 很自豪能够为促进能源的管理和高效利用、提高河内市的能源效率以及最大限度地利用绿色能源做出贡献。 ，实现对可持续发展的承诺和对社会社区的责任。</p>\r\n<p>Vicostone 隶属于 Phenikaa 集团，该集团是世界领先的优质石英人造石制造商之一。产品已在 50 多个国家出现，征服了美国、加拿大、澳大利亚、欧洲等要求苛刻的市场&hellip;&hellip;</p>\r\n<p>130多种设计和颜色，灵感来自大自然之美，石材台面激发生活空间的创造力，是制作人造石厨房桌子、茶几等餐桌、浴室、室内装饰创造的正确选择口音。 Century Stone对私人建筑的正品提供15年保修，请参阅：www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'shi-ying-shi-wei-lai-de-fa-zhan-fang-xiang-shi-shi-yao'),
	(34, 12, 1, 'Những vấn đề cần chú ý trong dây chuyền sản xuất đá cẩm thạch nhân tạo', 'Ngày 11/12/2021, Century Stone tự hào được công nhận là sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất công nghiệp', '<p>Ng&agrave;y 11/12/2021, Century Stone tự h&agrave;o được c&ocirc;ng nhận l&agrave; sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất c&ocirc;ng nghiệp (thứ hạng 5 sao) sau khi đ&atilde; xuất sắc vượt qua 90 đơn vị tr&ecirc;n địa b&agrave;n th&agrave;nh phố H&agrave; Nội. Đ&acirc;y l&agrave; giải thưởng vinh danh c&aacute;c cơ sở, c&ocirc;ng tr&igrave;nh x&acirc;y dựng sử dụng năng lượng xanh năm 2021 do Sở C&ocirc;ng Thương H&agrave; Nội tổ chức, c&ocirc;ng bố v&agrave; trao tặng.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Trong những năm qua, Century Stone lu&ocirc;n ch&uacute; trọng v&agrave; đồng bộ triển khai c&aacute;c giải ph&aacute;p trong việc tiết kiệm năng lượng hiệu quả, &iacute;t ph&aacute;t thải ra m&ocirc;i trường, t&aacute;i sử dụng ở mức đối đa trong c&aacute;c hoạt động sản xuất, kinh doanh của doanh nghiệp.</p>\r\n<p>Với việc sử dụng d&acirc;y chuyền sản xuất hiện đại bậc nhất được chuyển giao từ Breton (&Yacute;), c&ugrave;ng sự cải tiến đột ph&aacute; của đội ngũ kỹ sư c&ocirc;ng nghệ, to&agrave;n bộ qu&aacute; tr&igrave;nh sản xuất của Vicostone ho&agrave;n to&agrave;n kh&eacute;p k&iacute;n v&agrave; th&acirc;n thiện với m&ocirc;i trường, kh&ocirc;ng c&oacute; c&ocirc;ng đoạn nung n&ecirc;n đạt hiệu quả tiết kiệm năng lượng, kh&ocirc;ng c&oacute; kh&iacute; thải từ qu&aacute; tr&igrave;nh dưỡng hộ như c&aacute;c loại vật liệu ốp l&aacute;t được sản xuất theo c&ocirc;ng nghệ th&ocirc;ng thường</p>\r\n<p>B&ecirc;n cạnh đ&oacute;, Century Stone đ&atilde; triển khai v&agrave; li&ecirc;n tục mở rộng/ cải tiến kỹ thuật nhiều biện ph&aacute;p tiết kiệm năng lượng hiệu quả như:</p>\r\n<p>Thực hiện triển khai hệ thống điện năng lượng mặt trời tại 2 t&ograve;a nh&agrave; điều h&agrave;nh A1 v&agrave; A2 của C&ocirc;ng ty từ năm 2017 với 400 tấm pin quang điện được lắp đặt, c&oacute; thể cung cấp tới 90% điện năng cho hoạt động của hơn 200 c&aacute;n bộ, c&ocirc;ng nh&acirc;n vi&ecirc;n đang l&agrave;m việc tại 2 t&ograve;a nh&agrave;. Việc đưa tấm pin năng lượng mặt trời v&agrave;o hoạt động đ&atilde; gi&uacute;p sản lượng điện mỗi năm của Vicostone tiết kiệm l&ecirc;n đến tr&ecirc;n 500.000 kWh, tương đương với giảm ph&aacute;t thải tr&ecirc;n 200 tấn CO2 mỗi năm.<br />Thực hiện nhiều giải ph&aacute;p tiết kiệm năng lượng trong qu&aacute; tr&igrave;nh sản xuất,- ti&ecirc;u biểu như: Giải ph&aacute;p cho hệ thống chiếu s&aacute;ng; Giải ph&aacute;p cho hệ thống m&aacute;y n&eacute;n kh&iacute;; Giải ph&aacute;p lắp biến tần cho hệ thống bơm nước thải, bơm cấp nước c&ocirc;ng nghệ, giải ph&aacute;p cải tạo hệ thống sấy khu&ocirc;n, giải ph&aacute;p cải tạo hầm l&agrave;m m&aacute;t c&oacute; lắp biến tần điều khiển động cơ&hellip;<br />&Aacute;p dụng Hệ thống quản l&yacute; năng lượng theo ti&ecirc;u chuẩn ISO 50001: To&agrave;n bộ c&aacute;c c&ocirc;ng đoạn sử dụng năng lượng điện, gas, dầu diesel, nước sạch đều c&oacute; định mức sử dụng r&otilde; r&agrave;ng trong quy định, quy tr&igrave;nh ban h&agrave;nh; c&oacute; bộ phận chuy&ecirc;n m&ocirc;n theo d&otilde;i, gi&aacute;m s&aacute;t, ph&acirc;n t&iacute;ch dữ liệu năng lượng để cảnh b&aacute;o v&agrave; đưa ra c&aacute;c giải ph&aacute;p kịp thời về việc sử dụng năng lượng, tr&aacute;nh l&atilde;ng ph&iacute; m&agrave; vẫn đảm bảo sản xuất.<br />Qua những giải ph&aacute;p thiết thực tr&ecirc;n, Vicostone tự h&agrave;o g&oacute;p phần phần th&uacute;c đẩy c&ocirc;ng t&aacute;c quản l&yacute;, sử dụng năng lượng hiệu quả, n&acirc;ng cao hiệu suất sử dụng năng lượng của Th&agrave;nh phố H&agrave; Nội, tối đa sử dụng nguồn năng lượng xanh, hiện thực h&oacute;a cam kết ph&aacute;t triển bền vững v&agrave; tr&aacute;ch nhiệm v&igrave; cộng đồng x&atilde; hội.</p>\r\n<p>Vicostone thuộc Tập đo&agrave;n Phenikaa, một trong c&aacute;c nh&agrave; sản xuất h&agrave;ng đầu thế giới về đ&aacute; nh&acirc;n tạo gốc thạch anh cao cấp. Sản phẩm đ&atilde; c&oacute; mặt tại hơn 50 quốc gia, chinh phục c&aacute;c thị trường khắt khe như Mỹ, Canada, Australia, ch&acirc;u &Acirc;u...</p>\r\n<p>Với hơn 130 mẫu m&atilde;, m&agrave;u sắc, được thiết kế lấy &yacute; tưởng từ vẻ đẹp của thi&ecirc;n nhi&ecirc;n, mặt đ&aacute; truyền cảm hứng s&aacute;ng tạo cho kh&ocirc;ng gian sống, l&agrave; lựa chọn ph&ugrave; hợp để l&agrave;m b&agrave;n bếp đ&aacute; nh&acirc;n tạo, b&agrave;n tr&agrave;, b&agrave;n ăn, nh&agrave; tắm, decor nội thất để tạo điểm nhấn. Century Stone bảo h&agrave;nh 15 năm cho sản phẩm ch&iacute;nh h&atilde;ng với c&ocirc;ng tr&igrave;nh nh&agrave; ri&ecirc;ng, tham khảo tại: www.Century Stone.com/vi-vn</p>', NULL, '', '', '', 'nhung-van-de-can-chu-y-trong-day-chuyen-san-xuat-da-cam-thach-nhan-tao'),
	(35, 12, 2, 'Matters needing attention in the artificial marble production line', 'On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.', '<p>On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Over the years, Century Stone has always focused and synchronously deployed solutions to save energy effectively, with low emissions to the environment, and reuse to the maximum extent in production and business activities. of the enterprise.</p>\r\n<p>With the use of the most modern production line transferred from Breton (Italy), along with the breakthrough improvement of a team of technology engineers, Vicostone\'s entire production process is completely self-contained and user-friendly. Environmentally friendly, there is no calcination process, so it is effective in saving energy, and there is no emission from the curing process like other types of paving materials produced by conventional technology.</p>\r\n<p>In addition, Century Stone has deployed and continuously expanded/improved many effective energy saving measures such as:</p>\r\n<p>Deploying the solar power system at the Company\'s 2 operating buildings A1 and A2 from 2017 with 400 installed photovoltaic panels, which can provide up to 90% of electricity for the operation of more than 200 officers and employees are working in 2 buildings. Putting solar panels into operation has helped Vicostone\'s annual electricity output save up to over 500,000 kWh, equivalent to reducing emissions of over 200 tons of CO2 per year.<br />Implement many solutions to save energy in the production process,- typically: Solutions for lighting system; Solutions for air compressor systems; Solutions to install inverters for wastewater pumping systems, technology water supply pumps, solutions to renovate mold drying systems, solutions to renovate cooling tunnels with motor control inverters...<br />Applying the Energy Management System according to ISO 50001: All stages of using electric energy, gas, diesel oil, clean water have clear usage norms in the promulgated regulations and procedures; has a specialized department to monitor, monitor and analyze energy data to warn and provide timely solutions on energy use, avoid waste while ensuring production.<br />Through these practical solutions, Vicostone is proud to contribute to promoting the management and efficient use of energy, improving the energy efficiency of Hanoi City, and maximizing the use of green energy sources. , realizing the commitment to sustainable development and responsibility for the social community.</p>\r\n<p>Vicostone is part of Phenikaa Group, one of the world\'s leading manufacturers of premium quartz-based artificial stone. Products have been present in more than 50 countries, conquering demanding markets such as the US, Canada, Australia, Europe...</p>\r\n<p>With more than 130 designs and colors, designed with inspiration from the beauty of nature, stone countertops inspire creativity for living space, is the right choice for making artificial stone kitchen table, tea table, etc. dining table, bathroom, interior decoration to create accents. Century Stone has a 15-year warranty for genuine products with private buildings, refer to: www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'matters-needing-attention-in-the-artificial-marble-production-line'),
	(36, 12, 3, '人造大理石生产线的注意事项', '2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。', '<p>2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>多年来，世纪石始终专注并同步部署解决方案，以有效节约能源，对环境低排放，并在生产经营活动中最大程度地重复利用。的企业。</p>\r\n<p>使用从意大利布列塔尼转来的最现代化的生产线，再加上技术工程师团队的突破性改进，Vicostone的整个生产过程完全独立且人性化。环保，无需煅烧工序，节能效果显着，不会像传统工艺生产的其他类型铺路材料一样，在固化过程中产生废气。</p>\r\n<p>此外，世纪石还部署并不断扩展/改进了许多有效的节能措施，例如：</p>\r\n<p>从 2017 年开始，在公司的 2 栋运营大楼 A1 和 A2 部署太阳能发电系统，安装了 400 块光伏板，可为 2 栋大楼内 200 多名员工和员工的运营提供高达 90% 的电力。太阳能电池板投产后，Vicostone 的年发电量可节省超过 500,000 千瓦时，相当于每年减少 200 多吨二氧化碳的排放。<br />在生产过程中实施许多节能解决方案，通常是：照明系统解决方案；空压机系统解决方案；废水泵系统逆变器安装解决方案、技术供水泵、模具干燥系统改造解决方案、使用电机控制逆变器改造冷却隧道的解决方案......<br />应用符合ISO 50001的能源管理体系：在已颁布的法规和程序中，使用电能、燃气、柴油、清洁水的各个阶段都有明确的使用规范；设有专门部门对能源数据进行监测、监测和分析，对能源使用情况进行预警并及时提供解决方案，在确保生产的同时避免浪费。<br />通过这些切实可行的解决方案，Vicostone 很自豪能够为促进能源的管理和高效利用、提高河内市的能源效率以及最大限度地利用绿色能源做出贡献。 ，实现对可持续发展的承诺和对社会社区的责任。</p>\r\n<p>Vicostone 隶属于 Phenikaa 集团，该集团是世界领先的优质石英人造石制造商之一。产品已在 50 多个国家出现，征服了美国、加拿大、澳大利亚、欧洲等要求苛刻的市场&hellip;&hellip;</p>\r\n<p>130多种设计和颜色，灵感来自大自然之美，石材台面激发生活空间的创造力，是制作人造石厨房桌子、茶几等餐桌、浴室、室内装饰创造的正确选择口音。 Century Stone对私人建筑的正品提供15年保修，请参阅：www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'ren-zao-da-li-shi-sheng-chan-xian-de-zhu-yi-shi-xiang'),
	(37, 13, 1, 'CENTURYSTONE tiếp tục được vinh danh', 'Ngày 11/12/2021, Century Stone tự hào được công nhận là sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất công nghiệp', '<p>Ng&agrave;y 11/12/2021, Century Stone tự h&agrave;o được c&ocirc;ng nhận l&agrave; sở Cơ sở sử dụng Năng lượng Xanh trong sản xuất c&ocirc;ng nghiệp (thứ hạng 5 sao) sau khi đ&atilde; xuất sắc vượt qua 90 đơn vị tr&ecirc;n địa b&agrave;n th&agrave;nh phố H&agrave; Nội. Đ&acirc;y l&agrave; giải thưởng vinh danh c&aacute;c cơ sở, c&ocirc;ng tr&igrave;nh x&acirc;y dựng sử dụng năng lượng xanh năm 2021 do Sở C&ocirc;ng Thương H&agrave; Nội tổ chức, c&ocirc;ng bố v&agrave; trao tặng.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Trong những năm qua, Century Stone lu&ocirc;n ch&uacute; trọng v&agrave; đồng bộ triển khai c&aacute;c giải ph&aacute;p trong việc tiết kiệm năng lượng hiệu quả, &iacute;t ph&aacute;t thải ra m&ocirc;i trường, t&aacute;i sử dụng ở mức đối đa trong c&aacute;c hoạt động sản xuất, kinh doanh của doanh nghiệp.</p>\r\n<p>Với việc sử dụng d&acirc;y chuyền sản xuất hiện đại bậc nhất được chuyển giao từ Breton (&Yacute;), c&ugrave;ng sự cải tiến đột ph&aacute; của đội ngũ kỹ sư c&ocirc;ng nghệ, to&agrave;n bộ qu&aacute; tr&igrave;nh sản xuất của Vicostone ho&agrave;n to&agrave;n kh&eacute;p k&iacute;n v&agrave; th&acirc;n thiện với m&ocirc;i trường, kh&ocirc;ng c&oacute; c&ocirc;ng đoạn nung n&ecirc;n đạt hiệu quả tiết kiệm năng lượng, kh&ocirc;ng c&oacute; kh&iacute; thải từ qu&aacute; tr&igrave;nh dưỡng hộ như c&aacute;c loại vật liệu ốp l&aacute;t được sản xuất theo c&ocirc;ng nghệ th&ocirc;ng thường</p>\r\n<p>B&ecirc;n cạnh đ&oacute;, Century Stone đ&atilde; triển khai v&agrave; li&ecirc;n tục mở rộng/ cải tiến kỹ thuật nhiều biện ph&aacute;p tiết kiệm năng lượng hiệu quả như:</p>\r\n<p>Thực hiện triển khai hệ thống điện năng lượng mặt trời tại 2 t&ograve;a nh&agrave; điều h&agrave;nh A1 v&agrave; A2 của C&ocirc;ng ty từ năm 2017 với 400 tấm pin quang điện được lắp đặt, c&oacute; thể cung cấp tới 90% điện năng cho hoạt động của hơn 200 c&aacute;n bộ, c&ocirc;ng nh&acirc;n vi&ecirc;n đang l&agrave;m việc tại 2 t&ograve;a nh&agrave;. Việc đưa tấm pin năng lượng mặt trời v&agrave;o hoạt động đ&atilde; gi&uacute;p sản lượng điện mỗi năm của Vicostone tiết kiệm l&ecirc;n đến tr&ecirc;n 500.000 kWh, tương đương với giảm ph&aacute;t thải tr&ecirc;n 200 tấn CO2 mỗi năm.<br />Thực hiện nhiều giải ph&aacute;p tiết kiệm năng lượng trong qu&aacute; tr&igrave;nh sản xuất,- ti&ecirc;u biểu như: Giải ph&aacute;p cho hệ thống chiếu s&aacute;ng; Giải ph&aacute;p cho hệ thống m&aacute;y n&eacute;n kh&iacute;; Giải ph&aacute;p lắp biến tần cho hệ thống bơm nước thải, bơm cấp nước c&ocirc;ng nghệ, giải ph&aacute;p cải tạo hệ thống sấy khu&ocirc;n, giải ph&aacute;p cải tạo hầm l&agrave;m m&aacute;t c&oacute; lắp biến tần điều khiển động cơ&hellip;<br />&Aacute;p dụng Hệ thống quản l&yacute; năng lượng theo ti&ecirc;u chuẩn ISO 50001: To&agrave;n bộ c&aacute;c c&ocirc;ng đoạn sử dụng năng lượng điện, gas, dầu diesel, nước sạch đều c&oacute; định mức sử dụng r&otilde; r&agrave;ng trong quy định, quy tr&igrave;nh ban h&agrave;nh; c&oacute; bộ phận chuy&ecirc;n m&ocirc;n theo d&otilde;i, gi&aacute;m s&aacute;t, ph&acirc;n t&iacute;ch dữ liệu năng lượng để cảnh b&aacute;o v&agrave; đưa ra c&aacute;c giải ph&aacute;p kịp thời về việc sử dụng năng lượng, tr&aacute;nh l&atilde;ng ph&iacute; m&agrave; vẫn đảm bảo sản xuất.<br />Qua những giải ph&aacute;p thiết thực tr&ecirc;n, Vicostone tự h&agrave;o g&oacute;p phần phần th&uacute;c đẩy c&ocirc;ng t&aacute;c quản l&yacute;, sử dụng năng lượng hiệu quả, n&acirc;ng cao hiệu suất sử dụng năng lượng của Th&agrave;nh phố H&agrave; Nội, tối đa sử dụng nguồn năng lượng xanh, hiện thực h&oacute;a cam kết ph&aacute;t triển bền vững v&agrave; tr&aacute;ch nhiệm v&igrave; cộng đồng x&atilde; hội.</p>\r\n<p>Vicostone thuộc Tập đo&agrave;n Phenikaa, một trong c&aacute;c nh&agrave; sản xuất h&agrave;ng đầu thế giới về đ&aacute; nh&acirc;n tạo gốc thạch anh cao cấp. Sản phẩm đ&atilde; c&oacute; mặt tại hơn 50 quốc gia, chinh phục c&aacute;c thị trường khắt khe như Mỹ, Canada, Australia, ch&acirc;u &Acirc;u...</p>\r\n<p>Với hơn 130 mẫu m&atilde;, m&agrave;u sắc, được thiết kế lấy &yacute; tưởng từ vẻ đẹp của thi&ecirc;n nhi&ecirc;n, mặt đ&aacute; truyền cảm hứng s&aacute;ng tạo cho kh&ocirc;ng gian sống, l&agrave; lựa chọn ph&ugrave; hợp để l&agrave;m b&agrave;n bếp đ&aacute; nh&acirc;n tạo, b&agrave;n tr&agrave;, b&agrave;n ăn, nh&agrave; tắm, decor nội thất để tạo điểm nhấn. Century Stone bảo h&agrave;nh 15 năm cho sản phẩm ch&iacute;nh h&atilde;ng với c&ocirc;ng tr&igrave;nh nh&agrave; ri&ecirc;ng, tham khảo tại: www.Century Stone.com/vi-vn</p>', NULL, '', '', '', 'centurystone-tiep-tuc-duoc-vinh-danh'),
	(38, 13, 2, 'CENTURYSTONE continues to be honored', 'On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.', '<p>On December 11, 2021, Century Stone was proudly recognized as a facility using Green Energy in industrial production (5 stars) after successfully surpassing 90 units in Ha Noi city. This is an award honoring facilities and construction works using green energy in 2021 organized, announced and awarded by Hanoi Department of Industry and Trade.</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>Over the years, Century Stone has always focused and synchronously deployed solutions to save energy effectively, with low emissions to the environment, and reuse to the maximum extent in production and business activities. of the enterprise.</p>\r\n<p>With the use of the most modern production line transferred from Breton (Italy), along with the breakthrough improvement of a team of technology engineers, Vicostone\'s entire production process is completely self-contained and user-friendly. Environmentally friendly, there is no calcination process, so it is effective in saving energy, and there is no emission from the curing process like other types of paving materials produced by conventional technology.</p>\r\n<p>In addition, Century Stone has deployed and continuously expanded/improved many effective energy saving measures such as:</p>\r\n<p>Deploying the solar power system at the Company\'s 2 operating buildings A1 and A2 from 2017 with 400 installed photovoltaic panels, which can provide up to 90% of electricity for the operation of more than 200 officers and employees are working in 2 buildings. Putting solar panels into operation has helped Vicostone\'s annual electricity output save up to over 500,000 kWh, equivalent to reducing emissions of over 200 tons of CO2 per year.<br />Implement many solutions to save energy in the production process,- typically: Solutions for lighting system; Solutions for air compressor systems; Solutions to install inverters for wastewater pumping systems, technology water supply pumps, solutions to renovate mold drying systems, solutions to renovate cooling tunnels with motor control inverters...<br />Applying the Energy Management System according to ISO 50001: All stages of using electric energy, gas, diesel oil, clean water have clear usage norms in the promulgated regulations and procedures; has a specialized department to monitor, monitor and analyze energy data to warn and provide timely solutions on energy use, avoid waste while ensuring production.<br />Through these practical solutions, Vicostone is proud to contribute to promoting the management and efficient use of energy, improving the energy efficiency of Hanoi City, and maximizing the use of green energy sources. , realizing the commitment to sustainable development and responsibility for the social community.</p>\r\n<p>Vicostone is part of Phenikaa Group, one of the world\'s leading manufacturers of premium quartz-based artificial stone. Products have been present in more than 50 countries, conquering demanding markets such as the US, Canada, Australia, Europe...</p>\r\n<p>With more than 130 designs and colors, designed with inspiration from the beauty of nature, stone countertops inspire creativity for living space, is the right choice for making artificial stone kitchen table, tea table, etc. dining table, bathroom, interior decoration to create accents. Century Stone has a 15-year warranty for genuine products with private buildings, refer to: www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'centurystone-continues-to-be-honored'),
	(39, 13, 3, 'CENTURYSTONE 继续获得荣誉', '2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。', '<p>2021 年 12 月 11 日，Century Stone 在河内市成功超过 90 个单位后，自豪地被公认为在工业生产中使用绿色能源的设施（5 星）。这是河内工业和贸易部组织、宣布和颁发的2021年使用绿色能源的设施和建筑工程的奖项。</p>\r\n<p><img src="/upload/photos/shares/61e6a0d8ebb65.png" alt="" width="771" height="515" /></p>\r\n<p>多年来，世纪石始终专注并同步部署解决方案，以有效节约能源，对环境低排放，并在生产经营活动中最大程度地重复利用。的企业。</p>\r\n<p>使用从意大利布列塔尼转来的最现代化的生产线，再加上技术工程师团队的突破性改进，Vicostone的整个生产过程完全独立且人性化。环保，无需煅烧工序，节能效果显着，不会像传统工艺生产的其他类型铺路材料一样，在固化过程中产生废气。</p>\r\n<p>此外，世纪石还部署并不断扩展/改进了许多有效的节能措施，例如：</p>\r\n<p>从 2017 年开始，在公司的 2 栋运营大楼 A1 和 A2 部署太阳能发电系统，安装了 400 块光伏板，可为 2 栋大楼内 200 多名员工和员工的运营提供高达 90% 的电力。太阳能电池板投产后，Vicostone 的年发电量可节省超过 500,000 千瓦时，相当于每年减少 200 多吨二氧化碳的排放。<br />在生产过程中实施许多节能解决方案，通常是：照明系统解决方案；空压机系统解决方案；废水泵系统逆变器安装解决方案、技术供水泵、模具干燥系统改造解决方案、使用电机控制逆变器改造冷却隧道的解决方案......<br />应用符合ISO 50001的能源管理体系：在已颁布的法规和程序中，使用电能、燃气、柴油、清洁水的各个阶段都有明确的使用规范；设有专门部门对能源数据进行监测、监测和分析，对能源使用情况进行预警并及时提供解决方案，在确保生产的同时避免浪费。<br />通过这些切实可行的解决方案，Vicostone 很自豪能够为促进能源的管理和高效利用、提高河内市的能源效率以及最大限度地利用绿色能源做出贡献。 ，实现对可持续发展的承诺和对社会社区的责任。</p>\r\n<p>Vicostone 隶属于 Phenikaa 集团，该集团是世界领先的优质石英人造石制造商之一。产品已在 50 多个国家出现，征服了美国、加拿大、澳大利亚、欧洲等要求苛刻的市场&hellip;&hellip;</p>\r\n<p>130多种设计和颜色，灵感来自大自然之美，石材台面激发生活空间的创造力，是制作人造石厨房桌子、茶几等餐桌、浴室、室内装饰创造的正确选择口音。 Century Stone对私人建筑的正品提供15年保修，请参阅：www.Century Stone.com/en-vn</p>', NULL, '', '', '', 'centurystone-ji-xu-huo-de-rong-yu'),
	(40, 14, 1, 'dfsdfsd', '', '<p>&aacute;dfdsfsdaf</p>', NULL, '', '', '', 'dfsdfsd'),
	(41, 14, 2, '', '', '<p>sdfsdf</p>', NULL, '', '', '', ''),
	(42, 14, 3, '', '', '', NULL, '', '', '', ''),
	(43, 15, 1, '', '', '', NULL, '', '', '', ''),
	(44, 15, 2, '', '', '', NULL, '', '', '', ''),
	(45, 15, 3, '', '', '', NULL, '', '', '', ''),
	(46, 16, 1, 'wf', '', '', NULL, '', '', '', 'wf'),
	(47, 16, 2, 'wf', '', '', NULL, '', '', '', 'wf'),
	(48, 16, 3, 'wf', '', '', NULL, '', '', '', 'wf'),
	(49, 17, 1, '', '', '', NULL, '', '', '', ''),
	(50, 17, 2, '', '', '', NULL, '', '', '', ''),
	(51, 17, 3, '', '', '', NULL, '', '', '', '');
/*!40000 ALTER TABLE `news_description` ENABLE KEYS */;

-- Dumping structure for table centurystone.notifications
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) unsigned NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.notifications: ~0 rows (approximately)
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;

-- Dumping structure for table centurystone.oauth_access_tokens
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `oauth_access_tokens_user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.oauth_access_tokens: ~60 rows (approximately)
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
	('062368b02826374602791fd8a46f13115da1e94a43b344786e1fa7ca9e7d4f92c35ef109335ac2b7', 2, 22, NULL, '[]', 0, '2022-07-07 15:57:41', '2022-07-07 15:57:41', '2022-07-21 15:57:41'),
	('0aa7f9858768c73b8cbf52c785e883f2d1125092c61c2b7574cb300bba97041ff1758492828dd89c', 43, 3, NULL, '[]', 0, '2022-02-07 10:45:11', '2022-02-07 10:45:11', '2022-02-21 10:45:11'),
	('133ea57a8971d9cf495cbf32209d45429a2234b1ec2e343d8c54145ac6750d3ceabeb70a354bd315', 2, 22, NULL, '[]', 0, '2022-06-20 21:42:42', '2022-06-20 21:42:42', '2022-07-04 21:42:42'),
	('14ca99ed21fb018e37cdaf4024db9768eed5934fcecbae96397325fdc126317ce6afbc94d3fd5f55', 1, 7, NULL, '[]', 0, '2021-12-02 18:16:40', '2021-12-02 18:16:40', '2021-12-16 18:16:40'),
	('1a502c90f83be58a21b7c7056b2bdd5366a9a55ddde7471df34e3cb289f3010918cfae22a7a9250d', 38, 7, NULL, '[]', 0, '2021-12-15 14:52:42', '2021-12-15 14:52:42', '2021-12-29 14:52:42'),
	('1af951feb7de86e01603b5f3cfc7d296a295b95352e56c6a0a46057e4162aa841998701b83e99a3f', 1, 7, NULL, '[]', 0, '2021-12-02 15:11:55', '2021-12-02 15:11:55', '2021-12-16 15:11:55'),
	('1b15313fdf83eb95405cfa2bdaa758238ded8ee6d2d3c0a41757bace770f4283bcb39f75e0f4fe4c', 38, 7, NULL, '[]', 0, '2022-01-04 13:58:56', '2022-01-04 13:58:56', '2022-01-18 13:58:56'),
	('1f3e3940225cce203f8ca28c51e554b41c818f6ea2c77d79cf63422f437b5c347a845fcdc9587633', 1, 22, NULL, '[]', 0, '2022-04-01 10:13:11', '2022-04-01 10:13:11', '2022-04-15 10:13:11'),
	('20cfa4848a1e62cd7019d140961082e341a709eac5eeb3097d4efef144233aa47cfa0b5c05fd6c44', 1, 3, NULL, '[]', 0, '2022-02-07 10:05:33', '2022-02-07 10:05:33', '2022-02-21 10:05:33'),
	('27f7d5b909776046499d3e176490cfa47cc32b4457278d3173f7a3e1fd7b06b5e9e69b4c7842304d', 40, 7, NULL, '[]', 0, '2021-12-07 15:02:41', '2021-12-07 15:02:41', '2021-12-21 15:02:41'),
	('282c5bb128bf889682bdf00a2ac6f17250483d0696c601abd21c89f950f88154288dcfe109c10ca5', 1, 22, NULL, '[]', 0, '2022-05-13 10:50:07', '2022-05-13 10:50:07', '2022-05-27 10:50:07'),
	('2efd85e71494ca751246b3921504fb5a97b7b596c2d71d8752327a334ef20fbbe9a9fe045bca612f', 43, 22, NULL, '[]', 0, '2022-02-08 14:30:10', '2022-02-08 14:30:10', '2022-02-22 14:30:10'),
	('30e62d6191aafb926f709786df5c416315bbefd085ca13bdf3e02ab79185ede083fb495e9b9fe392', 42, 22, NULL, '[]', 0, '2022-02-09 08:56:03', '2022-02-09 08:56:03', '2022-02-23 08:56:03'),
	('31a6cd3775751268154b6f5966fd36a13b98468a5ad3f874152c5730b3c57b048c82396b63262800', 45, 22, NULL, '[]', 0, '2022-03-19 09:46:04', '2022-03-19 09:46:04', '2022-04-02 09:46:04'),
	('34159482904f4ab19d4eb175ecc37fea4e7fdefc6973a0fc1b605a445af4668f662cee6590c8c9d0', 1, 22, NULL, '[]', 0, '2022-02-10 15:09:37', '2022-02-10 15:09:37', '2022-02-24 15:09:37'),
	('38e0911c23d7f44b018fbf2272b31bcd7279e0bc79c027e7f7bbeca1148c4b22b5ed3ba9f7ec2c10', 1, 22, NULL, '[]', 0, '2022-02-25 11:33:54', '2022-02-25 11:33:54', '2022-03-11 11:33:54'),
	('3a0914169bceb21a5eadf841aa97b218eaeb9d4056e3cc88a44659d461d24a8cf65ac187b52f0e44', 1, 7, NULL, '[]', 0, '2022-01-04 14:00:41', '2022-01-04 14:00:41', '2022-01-18 14:00:41'),
	('3a2d967c3cefad91b4ce248e0faa7c12b774512131afccb7e9d893636feb34b383b9916c14af1cbf', 1, 22, NULL, '[]', 0, '2022-01-21 14:35:51', '2022-01-21 14:35:51', '2022-02-04 14:35:51'),
	('3e26a42558a1a67ec8177c11e6862b3f1a4c0d48b0e41b30ca2f305559d8fe32e00d935880baf173', 45, 22, NULL, '[]', 0, '2022-02-10 15:38:45', '2022-02-10 15:38:45', '2022-02-24 15:38:45'),
	('42fca84e858fda4977bfed180df2c92e02679f701accc8b886d26b3ab65b10a42d2bd762ad2eff12', 38, 7, NULL, '[]', 0, '2021-11-30 08:47:11', '2021-11-30 08:47:11', '2021-12-14 08:47:11'),
	('43deb2ec6f2feca1cff14085c3de5e8148572dbaceb88724a7ee1506c7895728702eb286d8d750fb', 38, 7, NULL, '[]', 0, '2021-12-02 14:19:17', '2021-12-02 14:19:17', '2021-12-16 14:19:17'),
	('4811fc7ea134d5a51078bfec72e805dc42f03cada1559bface4df55ee061cb7f6d2ef5e780709c97', 45, 22, NULL, '[]', 0, '2022-03-25 09:47:11', '2022-03-25 09:47:11', '2022-04-08 09:47:11'),
	('48ef672b113879e32e19a7b74b57b350fda7470b7917632691994d15bd78e7594bb01aac684c4e6b', 45, 22, NULL, '[]', 0, '2022-02-10 21:02:29', '2022-02-10 21:02:29', '2022-02-24 21:02:29'),
	('4aaedd216e76fe48dfa5365885d0d01de3d4a543a8d401569247042cbc5eae5a8646e9a7043bcce0', 42, 22, NULL, '[]', 0, '2022-02-11 14:14:00', '2022-02-11 14:14:00', '2022-02-25 14:14:00'),
	('4bbeb0b0493c500336bbefa968ce761378ef5585a18afdbe8bf99597aec4148bb4114c12eb585458', 43, 22, NULL, '[]', 0, '2022-01-25 17:10:03', '2022-01-25 17:10:03', '2022-02-08 17:10:03'),
	('4c006c05314faed317f75b7bd547ed957475eb3bcd859649c92b68729062708b1e0937d8c930eebc', 42, 22, NULL, '[]', 0, '2022-01-21 16:13:40', '2022-01-21 16:13:40', '2022-02-04 16:13:40'),
	('4c4444184b8530e8cd9c4dd51823258154d0a31638da435bbe3ec2d7b0953d0e51612e50985cd588', 39, 7, NULL, '[]', 0, '2021-12-08 13:45:24', '2021-12-08 13:45:24', '2021-12-22 13:45:24'),
	('4f023ba8d6b4193f70cc19cdd8f3332b85a305685f0ffcb4a37d6045d6ee56c2a7d6cd916baf60a5', 1, 22, NULL, '[]', 0, '2022-01-24 17:45:06', '2022-01-24 17:45:06', '2022-02-07 17:45:06'),
	('5062f2662fd0db803aee86e46e069d3f59b05553d1a65f511e4b90dba8eaf8a0dc2970b117b7eb42', 43, 22, NULL, '[]', 0, '2022-02-10 08:53:06', '2022-02-10 08:53:06', '2022-02-24 08:53:06'),
	('511e14340f8100551f875cbd66015e8689095e0fe206796821522fe718a6ef7cb8218a89109846f9', 43, 22, NULL, '[]', 0, '2022-02-08 14:51:15', '2022-02-08 14:51:15', '2022-02-22 14:51:15'),
	('5404088c2011473612f24fa922070fa82c4dc090f13ceead5643080ae1d9b231125a985a462d03c4', 1, 7, NULL, '[]', 0, '2021-12-03 10:11:36', '2021-12-03 10:11:36', '2021-12-17 10:11:36'),
	('545f1b63a241d62eb9935c1150bf74fba0eb0e560ee6423c05abfd01f9ca3589a95a0776c62e769a', 1, 22, NULL, '[]', 0, '2022-02-28 16:21:32', '2022-02-28 16:21:32', '2022-03-14 16:21:32'),
	('5595b24a821baab0883df0fc4ede78f701360099a2c5af4830d41fe7a9b94e54478535c690e67383', 1, 22, NULL, '[]', 0, '2022-02-08 11:55:28', '2022-02-08 11:55:28', '2022-02-22 11:55:28'),
	('55a69c426585a14023b793abc584798e2690338a4bd63c1226c6e3c49ec1ee66a3684425077816b7', 43, 22, NULL, '[]', 0, '2022-02-11 00:08:03', '2022-02-11 00:08:03', '2022-02-25 00:08:03'),
	('5901ab5f9d0b8f4c89b3620bb4a9e800dfe9644d7f71c04bb59007978763f7ad9c372e260889c35b', 1, 22, NULL, '[]', 0, '2022-01-21 16:56:00', '2022-01-21 16:56:00', '2022-02-04 16:56:00'),
	('5959efd6ac469e73a8dcb12644cfcdef6886d9e77368e231ab4ac3476f4e943872f51bc40a514cff', 42, 22, NULL, '[]', 0, '2022-01-26 08:47:06', '2022-01-26 08:47:06', '2022-02-09 08:47:06'),
	('5b3c992846a31bbd0d59aa786d33692b019f16144d2d5aaf84327f08fa8545cc08be1df814b6cdd4', 1, 22, NULL, '[]', 0, '2022-01-18 14:03:08', '2022-01-18 14:03:08', '2022-02-01 14:03:08'),
	('63a9c4180c1f4771f3394c6e121810f17ace7f4839a261c4a13e2a92b7bf333bf3f511a766676123', 1, 22, NULL, '[]', 0, '2022-01-19 22:57:16', '2022-01-19 22:57:16', '2022-02-02 22:57:16'),
	('63c5373fee4848b5e6a0eee74218e48e1a4309230c42990066e203986a1e19f39ceb78990e1e3367', 38, 7, NULL, '[]', 0, '2021-12-02 17:01:13', '2021-12-02 17:01:13', '2021-12-16 17:01:13'),
	('647e8db193d149e7c653b9aa2e311d02536deb212424cb5f09cb32f86f463332806386805feeb85d', 40, 7, NULL, '[]', 0, '2021-12-08 13:53:00', '2021-12-08 13:53:00', '2021-12-22 13:53:00'),
	('649a38119f89043ee5f10c94719ad078d78b37ca7ad1e575846ccc8b06337d9134c544c7b7bd4970', 40, 7, NULL, '[]', 0, '2021-12-06 10:56:16', '2021-12-06 10:56:16', '2021-12-20 10:56:16'),
	('6750490bea20803029f0de16ab21e43f1b05c1f705aeec53c4a1d99875791c3d1590348d6bbe2c8f', 38, 7, NULL, '[]', 0, '2021-12-02 16:59:11', '2021-12-02 16:59:11', '2021-12-16 16:59:11'),
	('6825de8187a76bc044e0d48d2bfb6e1a56eb063b04a0d034d8d9d783ecdf07becd02d473c6992da7', 1, 22, NULL, '[]', 0, '2022-04-26 17:08:58', '2022-04-26 17:08:58', '2022-05-10 17:08:58'),
	('6d1a27fa928028f8d0d2dece2c1e659a0cb604b8a5b0194814085552bd079fcd0d1f9065b1ff8051', 45, 22, NULL, '[]', 0, '2022-03-23 13:10:49', '2022-03-23 13:10:49', '2022-04-06 13:10:49'),
	('70be05621c3bcdededd6d6fffc7e515564d050d69c4afda29316a863e185e2c798edf45560ab85e0', 45, 22, NULL, '[]', 0, '2022-03-18 15:30:59', '2022-03-18 15:30:59', '2022-04-01 15:30:59'),
	('76915b54dd888aaf6edb4d7b4478d579e85280c2dcdb923c0333892f9491f5a9bdd9f6d01a6c6d68', 43, 3, NULL, '[]', 0, '2022-02-08 14:41:22', '2022-02-08 14:41:22', '2022-02-22 14:41:22'),
	('7a115f8c3a85b78987758870a77ae8b943e1553dc0c8266bcbecd63476c28b5052bb6c4795fc8935', 45, 22, NULL, '[]', 0, '2022-02-14 10:26:36', '2022-02-14 10:26:36', '2022-02-28 10:26:36'),
	('7a15949122bdec8b9f3e51caf97cd05a50d9188179760c6548f29b34fa3723dd52c92058f92d9bdb', 1, 22, NULL, '[]', 0, '2022-03-25 09:49:32', '2022-03-25 09:49:32', '2022-04-08 09:49:32'),
	('80312eca208644eaf43563a105c7c3a4942c8d53fd12874c4077f7c8b972108b6a71f12934edaa8a', 1, 3, NULL, '[]', 0, '2022-01-07 16:57:08', '2022-01-07 16:57:08', '2022-01-21 16:57:08'),
	('81eba060aeb915d26a275ec6b4d8224099f5f3515fdc2c9e011f8d27843207b0fd1c1dbca91568b3', 38, 7, NULL, '[]', 0, '2021-12-04 09:43:01', '2021-12-04 09:43:01', '2021-12-18 09:43:01'),
	('82f2ea2bb09827e57761fb8badc673f895d1d84fbbec7db7ef50af99cbdacc71d13a1f05ea8c1f99', 1, 22, NULL, '[]', 0, '2022-01-21 14:36:26', '2022-01-21 14:36:26', '2022-02-04 14:36:26'),
	('85c6b92fd9d680be7c25b519656975596126adf82013f34c16338407512fddd9c383e12e32ff084d', 1, 22, NULL, '[]', 0, '2022-02-10 10:19:24', '2022-02-10 10:19:24', '2022-02-24 10:19:24'),
	('86ab8b3bfadace30505d668f2da1f9b4eb12ffefdf3386897111bd2fcd22862d40da9f1b9ae60d33', 1, 22, NULL, '[]', 0, '2022-02-08 10:37:40', '2022-02-08 10:37:40', '2022-02-22 10:37:40'),
	('88473a28696d38f8d5f9ce564e50341daf949a212297f8df07516d5bef7cf24c3f3ada943723505c', 1, 22, NULL, '[]', 0, '2022-01-26 10:52:36', '2022-01-26 10:52:36', '2022-02-09 10:52:36'),
	('88e5e71981f7dfd9bb83d03b59598977fc625e13885af3328661383467f3ed1bbd2c2f55c44424ee', 2, 22, NULL, '[]', 0, '2022-06-16 16:23:44', '2022-06-16 16:23:44', '2022-06-30 16:23:44'),
	('8b4ab8f12d8101502b7f8009bc8ebe034b51b03b0de151f629c5002ae004af60412c90857f4f1153', 1, 22, NULL, '[]', 0, '2022-04-01 17:38:09', '2022-04-01 17:38:09', '2022-04-15 17:38:09'),
	('8d1dadac398bc710f99d54519897c239e6992480707cf29fe670a2d56522525aa1323c4b9e658184', 2, 22, NULL, '[]', 0, '2022-01-21 14:37:59', '2022-01-21 14:37:59', '2022-02-04 14:37:59'),
	('8de6cefbd38d27fed0eb4b608a964d0c0340ef6db4c5df705950902129e977a26fd02f5855a3e5fb', 39, 7, NULL, '[]', 0, '2021-12-04 09:43:15', '2021-12-04 09:43:15', '2021-12-18 09:43:15'),
	('8f8ab648cba69eadef3c9090110021362c6bb0343ff141ca1ef8bb452ea07a7f1e5cd7d16cb190e9', 1, 7, NULL, '[]', 0, '2021-12-06 10:40:28', '2021-12-06 10:40:28', '2021-12-20 10:40:28'),
	('920f817ea3bde10d3060c012fe5d0897a733c2d3de97cdebab137b2730136475707b510a8ee745cf', 1, 7, NULL, '[]', 0, '2022-01-04 13:58:31', '2022-01-04 13:58:31', '2022-01-18 13:58:31'),
	('9211032d8c427dec1f37e5bbd9774ff3e8118debfe21e9484278eb5e24ba80737f17572f04317b85', 42, 22, NULL, '[]', 0, '2022-04-01 10:21:55', '2022-04-01 10:21:55', '2022-04-15 10:21:55'),
	('9676f88ad2534fc6a4c308b199a6f5242422e8c72ba5f885a8bd032cf223291d7c14710b2fbbcd9a', 1, 22, NULL, '[]', 0, '2022-02-16 14:45:51', '2022-02-16 14:45:51', '2022-03-02 14:45:51'),
	('9dbd81c39d6bc850c4a1384c7d35a3648a379e957182377c8b5756a7a8e7ea182a9846f738fd4882', 37, 3, NULL, '[]', 0, '2021-12-29 09:44:09', '2021-12-29 09:44:09', '2022-01-12 09:44:09'),
	('a096e201ada1934189fa446dc596e5f1050399df4ee6584b92f2f4af71775d978810d8a245a0ef4b', 1, 3, NULL, '[]', 0, '2022-01-12 16:30:29', '2022-01-12 16:30:29', '2022-01-26 16:30:29'),
	('a2ae2ddf9b2cbd7b71b0d41aa56772996b99d210f81aefc91e8c8fd1cdf3971d5a137ece9393b412', 37, 7, NULL, '[]', 0, '2021-12-01 09:11:43', '2021-12-01 09:11:43', '2021-12-15 09:11:43'),
	('a523bada25bd41abd9d56ee97d1a2ad4f686f1e04ee36d51e531c79edf601e63e993ffd165aae2b8', 45, 22, NULL, '[]', 0, '2022-03-19 07:53:22', '2022-03-19 07:53:22', '2022-04-02 07:53:22'),
	('a8518ff6b44e1cc08726490a1ed0001c911b926a5c52429514e677c278de35f8401516951dc33a71', 41, 7, NULL, '[]', 0, '2021-12-08 13:47:03', '2021-12-08 13:47:03', '2021-12-22 13:47:03'),
	('a92039571397a9ff6fa562706556302d14d2b0a85681d616adfeb4f5df3123e007512d55fadfefd7', 37, 7, NULL, '[]', 0, '2021-11-30 10:36:58', '2021-11-30 10:36:58', '2021-12-14 10:36:58'),
	('a92eb1d164ff6c3ee8332904dac31e316049b8e8232207dd565fb1237ae3452df4caf16e986c59c3', 38, 7, NULL, '[]', 0, '2021-12-07 14:59:10', '2021-12-07 14:59:10', '2021-12-21 14:59:10'),
	('ab6f507443b97795a257b1b7795dca5a9322048f3123d9455648f9faf92273746b8713904654dab0', 2, 22, NULL, '[]', 0, '2022-02-07 09:49:06', '2022-02-07 09:49:06', '2022-02-21 09:49:06'),
	('ac0fa6f7f54ebd199a7abef65c82d9ca8e3675e3b122d1cf0ef62613b20fe66d183f1d838dc8aec4', 38, 7, NULL, '[]', 0, '2021-12-08 10:58:33', '2021-12-08 10:58:33', '2021-12-22 10:58:33'),
	('ac4735c1046173d0783868719a3bbe20873e6f9a7b7c63778188f4906a8c5f3ed9bbe783aaa9394a', 42, 22, NULL, '[]', 0, '2022-01-27 12:38:16', '2022-01-27 12:38:16', '2022-02-10 12:38:16'),
	('acfdb6be895bcee87fd29a062cd0adaf801780f0df38d9887724bc6b1eed9b956192f637e8a36d7c', 1, 22, NULL, '[]', 0, '2022-02-09 10:37:36', '2022-02-09 10:37:36', '2022-02-23 10:37:36'),
	('ae2f309499dc81ba1b9bcebc8d6e40ee3fee58015e0bb18931957481a62ef4599a712e29d97d4511', 38, 7, NULL, '[]', 0, '2021-12-01 21:28:28', '2021-12-01 21:28:28', '2021-12-15 21:28:28'),
	('ba01e00db82b80167d28b0c157af30dbb4ed184e328e09354e9af004a741d641c40007be1b9855a6', 45, 22, NULL, '[]', 0, '2022-03-19 09:26:24', '2022-03-19 09:26:24', '2022-04-02 09:26:24'),
	('bb703dc6d9eb27690940f9f3ae046caceb14e579195b48d2221f8960ae1812bde5ddd4b1fc1149c8', 1, 3, NULL, '[]', 0, '2022-02-07 09:31:20', '2022-02-07 09:31:20', '2022-02-21 09:31:20'),
	('bc7e34ae778e69d796ed85e6c56b6cb4376db24930315ac6b5a3fd62879968eec88d8144e3879831', 1, 7, NULL, '[]', 0, '2022-01-04 13:56:26', '2022-01-04 13:56:26', '2022-01-18 13:56:26'),
	('bd93a6ac850b20b58d0753e220f3aaaa278f4afa7a202e89def759318760c3c3852ea46d9537552a', 1, 22, NULL, '[]', 0, '2022-03-19 09:20:24', '2022-03-19 09:20:24', '2022-04-02 09:20:24'),
	('c2332cfcdae8fdfe92a57a9ddcf30e6aff6386cd8e27019a621e638ceb8ad049e338cfcb18127061', 38, 7, NULL, '[]', 0, '2021-12-06 10:53:03', '2021-12-06 10:53:03', '2021-12-20 10:53:03'),
	('c2aae4bba651ed1ecd87131f2adf0055cfc861b27e685bf30d2c9e4d1bb2400647677aef6c6c594f', 42, 22, NULL, '[]', 0, '2022-05-19 14:32:38', '2022-05-19 14:32:38', '2022-06-02 14:32:38'),
	('c423192f29188b3f346878441cd0d39e4291cbc25b09ccad3031437c3eb48da93dee573f04826be0', 1, 22, NULL, '[]', 0, '2022-04-01 10:13:37', '2022-04-01 10:13:37', '2022-04-15 10:13:37'),
	('ce1f7c9698f889803bd9d32c21a7500cfb88908e128dfcaca8ffdb7e8e2a75a966028ebab78ed090', 2, 22, NULL, '[]', 0, '2022-02-09 15:39:46', '2022-02-09 15:39:46', '2022-02-23 15:39:46'),
	('d0b3f898047acf701a3727ffd4b029d1903c79238feb738df660f3eb1056396d8a1bff060da344b3', 42, 22, NULL, '[]', 0, '2022-05-19 14:40:49', '2022-05-19 14:40:49', '2022-06-02 14:40:49'),
	('d207121f2ce71752e3ab0ea91174cca8df4d44618921b10fce5672f99d85dc80fdf308a2bcac51e7', 43, 22, NULL, '[]', 0, '2022-01-26 08:53:49', '2022-01-26 08:53:49', '2022-02-09 08:53:49'),
	('d2bccd9390efff36051c6703475f655eb97f0143743d267f6fdb3d13ee0e694fc0a881f129c1b98e', 1, 22, NULL, '[]', 0, '2022-03-30 14:50:08', '2022-03-30 14:50:08', '2022-04-13 14:50:08'),
	('d51d74677aded9eace36db0f72da3d20c1acf7934b5122a9f19b2815a9ddaaa02dc7fd4c15ce5997', 38, 7, NULL, '[]', 0, '2021-11-29 15:00:13', '2021-11-29 15:00:13', '2021-12-13 15:00:13'),
	('dc8bd74df5e4ba225fe8845c1fdf9bf8798e981a427de808b977c6736e2e8904e78c898aeee7db83', 38, 7, NULL, '[]', 0, '2021-12-29 09:50:12', '2021-12-29 09:50:12', '2022-01-12 09:50:12'),
	('ddfdf880c0b88cd05af3859a5c140f859eacdb99c4004d39dde623fc360c31c83c031ad18b161af8', 45, 22, NULL, '[]', 0, '2022-02-10 15:47:51', '2022-02-10 15:47:51', '2022-02-24 15:47:51'),
	('df88b4208f7a8774e251355181a75fb55b5cf401e4d20974e09433faf38a0408c00d5fefc7d30529', 42, 22, NULL, '[]', 0, '2022-05-19 14:32:09', '2022-05-19 14:32:09', '2022-06-02 14:32:09'),
	('e02224aee8a36c1903f319bb2408660d3fc4020ff4bbbb5150cbd5d3f51565d216089efc350e2e2a', 43, 22, NULL, '[]', 0, '2022-02-09 09:24:16', '2022-02-09 09:24:16', '2022-02-23 09:24:16'),
	('e112b57c64c11d35317e4f75456fdc5d014b1208441b64e07c1a3016d3b5cf9cc75ed338f64081b3', 45, 22, NULL, '[]', 0, '2022-02-14 09:56:04', '2022-02-14 09:56:04', '2022-02-28 09:56:04'),
	('e173258ebf6d3333852739b3dda0ecdba7a9f54b3db7aa290a102142573f3b6cc76921fb78712be1', 1, 7, NULL, '[]', 0, '2021-12-02 15:13:15', '2021-12-02 15:13:15', '2021-12-16 15:13:15'),
	('e219bcc9ae1bd724fc639e95b9fbd13acaac8d133ac213a3d25ee2095149835d85653870444ad26d', 1, 3, NULL, '[]', 0, '2021-11-29 14:17:54', '2021-11-29 14:17:54', '2021-12-13 14:17:54'),
	('e58ab41ecab75b179e07464598ab43d20e5b8c05fa8101d834988626153ebb51b390d7ca6ad51b4c', 1, 22, NULL, '[]', 0, '2022-02-10 14:15:05', '2022-02-10 14:15:05', '2022-02-24 14:15:05'),
	('e9594c46c907139e4f06b3a8b789c0291f7d5f2fc7edfe1335d325a58cd77c2e941f5578b7a5f816', 42, 22, NULL, '[]', 0, '2022-02-12 10:47:20', '2022-02-12 10:47:20', '2022-02-26 10:47:20'),
	('edc1d2d1f495feeb94bce7920a88e82529d1c2f0eb2ab90c9d32f7fff4a04b68ed2a21bf1db07c2e', 37, 7, NULL, '[]', 0, '2021-12-04 10:55:53', '2021-12-04 10:55:53', '2021-12-18 10:55:53'),
	('edc5b08c627f1986856cb06af32a66c71414a4f1f58b3560da4ce4aab2628d2c5c197873876d9ea4', 1, 22, NULL, '[]', 0, '2022-04-01 11:27:34', '2022-04-01 11:27:34', '2022-04-15 11:27:34'),
	('eeb4dfaa94d35aa62e46fe468719c7f463a78931cb4171ff17023e9939930996f3e30576c192a98a', 2, 22, NULL, '[]', 0, '2022-01-18 14:17:39', '2022-01-18 14:17:39', '2022-02-01 14:17:39'),
	('f3c087914dc41e06630485c7d1cb06b329e53da93b09d436cecaf34d3346ed10ff17f4ff55874b1b', 45, 22, NULL, '[]', 0, '2022-02-11 07:33:12', '2022-02-11 07:33:12', '2022-02-25 07:33:12'),
	('f61e5b09c7ff64cae2ce980b7e1fe74f62a81dc915ff8d185e52cb96cd61bf28b82c84cb8a87c3c4', 38, 7, NULL, '[]', 0, '2021-12-08 13:45:07', '2021-12-08 13:45:07', '2021-12-22 13:45:07'),
	('f6e187239f3ba0f02edf831b7d9ae10f4b38c328091d62494706f9c9249a922ac94fdd29fe361782', 1, 22, NULL, '[]', 0, '2022-01-18 14:03:01', '2022-01-18 14:03:01', '2022-02-01 14:03:01'),
	('f753ffc45428a29c81fdd5a1093ce6ebd6811392ff4a6735e5bb60ccbe9c2422afca983f525e41f7', 40, 7, NULL, '[]', 0, '2021-12-06 10:47:38', '2021-12-06 10:47:38', '2021-12-20 10:47:38'),
	('f7f9899e7deee2cea5476527ca09e758a9c424ee7626dd748e170ed9e960fc2f2d965d19c061dde6', 42, 22, NULL, '[]', 0, '2022-01-21 16:01:53', '2022-01-21 16:01:53', '2022-02-04 16:01:53'),
	('f975efddba952edf9a1dbb00449e405b58bd46f9b78be293532805c3acbaa1ebc115c09db27edb2c', 42, 22, NULL, '[]', 0, '2022-01-21 15:50:38', '2022-01-21 15:50:38', '2022-02-04 15:50:38');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Dumping structure for table centurystone.oauth_auth_codes
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `oauth_auth_codes_user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.oauth_auth_codes: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Dumping structure for table centurystone.oauth_clients
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `oauth_clients_user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.oauth_clients: ~23 rows (approximately)
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'Viet Vision Personal Access Client', 'BLQrVhmSQsQqRCdg5w46yPx4s1NyPPkwfjITBoO3', NULL, 'http://localhost', 1, 0, 0, '2021-11-29 14:15:53', '2021-11-29 14:15:53'),
	(2, NULL, 'Viet Vision Password Grant Client', 'pGK2oCq83wt2eykNzIzzRAiwyv6cjKpkxaOQBCZ7', 'users', 'http://localhost', 0, 1, 0, '2021-11-29 14:15:53', '2021-11-29 14:15:53'),
	(3, NULL, 'admin', 'hfGHtZwZW2wDRWFMDQdfdFPdZys2NP74d8LgSznk', 'users', 'http://localhost', 0, 1, 0, '2021-11-29 14:17:23', '2021-11-29 14:17:23'),
	(4, NULL, 'frontend', 'yuaQfDHL80300BfJHNUoAGuXrYYWH9TdJx7rTygN', 'customers', 'http://localhost', 0, 1, 0, '2021-11-29 14:17:31', '2021-11-29 14:17:31'),
	(5, NULL, 'Viet Vision Personal Access Client', 'yhgtty71emWGY2voSIcmrDDCZDPmsGNeso6rJaCE', NULL, 'http://localhost', 1, 0, 0, '2021-11-29 14:20:33', '2021-11-29 14:20:33'),
	(6, NULL, 'Viet Vision Password Grant Client', 'RVJfCh7A9j7rokkeNt7TcuINkBUHiPy98CTVzRqy', 'users', 'http://localhost', 0, 1, 0, '2021-11-29 14:20:33', '2021-11-29 14:20:33'),
	(7, NULL, 'admin', 'NbMIMuyCMFfjRYHqk81nJjV3cAmoYEF9W1SKCqQn', 'users', 'http://localhost', 0, 1, 0, '2021-11-29 14:22:29', '2021-11-29 14:22:29'),
	(8, NULL, 'frontend', 'FD25ecE8NelEtQG1mUXttUmufiminYGf4YKgZMzo', 'customers', 'http://localhost', 0, 1, 0, '2021-11-29 14:22:54', '2021-11-29 14:22:54'),
	(9, NULL, 'Viet Vision Personal Access Client', '6mv1CZEb6MhmjUhQUO78J3vPAfVY6vrzxVL5YGUr', NULL, 'http://localhost', 1, 0, 0, '2021-11-29 17:06:05', '2021-11-29 17:06:05'),
	(10, NULL, 'Viet Vision Password Grant Client', 'MmSsL2OihjtEwFGvfONI9Acj1z7Hxt4RmsIUgOuX', 'users', 'http://localhost', 0, 1, 0, '2021-11-29 17:06:05', '2021-11-29 17:06:05'),
	(11, NULL, 'admin', 'tBPHpAgOlnZGyTUDo5xVgVgQodj1Vk2j38l1GZVA', 'users', 'http://localhost', 0, 1, 0, '2021-11-29 17:06:19', '2021-11-29 17:06:19'),
	(12, NULL, 'frontend', 'jgVK9exuZp672IBQ76NUCorP4Cn32h9CWh5bH6EN', 'customers', 'http://localhost', 0, 1, 0, '2021-11-29 17:06:24', '2021-11-29 17:06:24'),
	(13, NULL, 'Viet Vision Personal Access Client', '2KTi5zxYOiHGp960taxYOa5CxZq7PvscdyNz1VaD', NULL, 'http://localhost', 1, 0, 0, '2021-11-30 09:44:47', '2021-11-30 09:44:47'),
	(14, NULL, 'Viet Vision Password Grant Client', 'FqxuB3kEjGvslqocUZ53dHVVzqLz8rk6jhNHWiAB', 'users', 'http://localhost', 0, 1, 0, '2021-11-30 09:44:48', '2021-11-30 09:44:48'),
	(15, NULL, 'admin', 'mEmY5rOJ06fn3tusuj9ONhZHo3i1hc8dDCC8es2e', 'users', 'http://localhost', 0, 1, 0, '2021-11-30 09:44:57', '2021-11-30 09:44:57'),
	(16, NULL, 'frontend', 'bKNYL3D5KrlzZ0kBTEkSQGBaCiyLzWdCCPLQlRaq', 'customers', 'http://localhost', 0, 1, 0, '2021-11-30 09:45:04', '2021-11-30 09:45:04'),
	(17, NULL, 'Century stone Personal Access Client', '1H6fpmiGiPxj1hIGzXOxAfxYskBANEk8V9n5MDp9', NULL, 'http://localhost', 1, 0, 0, '2022-01-07 16:54:46', '2022-01-07 16:54:46'),
	(18, NULL, 'Century stone Password Grant Client', 'YnzGaP8KVOFMSjFxC2esjb2O2uNuWb80Xy8CZAFA', 'users', 'http://localhost', 0, 1, 0, '2022-01-07 16:54:47', '2022-01-07 16:54:47'),
	(19, NULL, 'Century stone Personal Access Client', 'iPMrfhwgdFQnlML9Hl0dztRWIvvKWVIuFW2hQLxD', NULL, 'http://localhost', 1, 0, 0, '2022-01-12 16:22:37', '2022-01-12 16:22:37'),
	(20, NULL, 'Century stone Password Grant Client', 'k320AKNgkBoQHahc5SsWvJZp7AsIFhlwxtcaOLke', 'users', 'http://localhost', 0, 1, 0, '2022-01-12 16:22:37', '2022-01-12 16:22:37'),
	(21, NULL, 'Century Stone Personal Access Client', 'lI7jyjz63INdzTuGrk25mJFWQfVDAQbC5gHiMCNs', NULL, 'http://localhost', 1, 0, 0, '2022-01-18 13:59:22', '2022-01-18 13:59:22'),
	(22, NULL, 'Century Stone Password Grant Client', '3ILqaQau7Y2hoX7QL84HqXQ6AlGQDUbq6ICmnoDp', 'users', 'http://localhost', 0, 1, 0, '2022-01-18 13:59:23', '2022-01-18 13:59:23'),
	(23, NULL, 'FE DEV', 'Ry7WeZ1DkixuQRqUPscLV9TtMNdtfq5hwZRAPm9P', 'customers', 'http://localhost', 0, 1, 0, '2022-01-18 14:00:01', '2022-01-18 14:00:01'),
	(24, NULL, 'Century Stone Personal Access Client', 'n4azchgeqwkyQ7YE4tAZSsKVORZPIsS389LitaEz', NULL, 'http://localhost', 1, 0, 0, '2022-03-17 18:05:45', '2022-03-17 18:05:45'),
	(25, NULL, 'Century Stone Password Grant Client', 'eMRr44trtsUJjx803tzTSOFkxfYpavFj4WTV3H0h', 'users', 'http://localhost', 0, 1, 0, '2022-03-17 18:05:46', '2022-03-17 18:05:46'),
	(26, NULL, 'FE LIVE', 'hUhoKFxmuVOLuNYPblzePzQa6maYTpl8Lof5V2hD', 'customers', 'http://localhost', 0, 1, 0, '2022-03-17 18:06:15', '2022-03-17 18:06:15'),
	(27, NULL, 'Century Stone Personal Access Client', 'df34DIYruMAs8MnuxfF5klhYygJjem8tZdIjzn69', NULL, 'http://localhost', 1, 0, 0, '2022-03-23 15:20:03', '2022-03-23 15:20:03'),
	(28, NULL, 'Century Stone Password Grant Client', 'SHdTuWmMTz8yHFTfWiSMleiIVeyVGRSqtnABGay1', 'users', 'http://localhost', 0, 1, 0, '2022-03-23 15:20:04', '2022-03-23 15:20:04'),
	(29, NULL, 'FE DEV', 'kAPdmMaMkevCbRT7twHqrSaqbhziho5YOG88rvzs', 'customers', 'http://localhost', 0, 1, 0, '2022-03-23 15:21:16', '2022-03-23 15:21:16');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Dumping structure for table centurystone.oauth_personal_access_clients
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.oauth_personal_access_clients: ~4 rows (approximately)
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
	(1, 1, '2021-11-29 14:15:53', '2021-11-29 14:15:53'),
	(2, 5, '2021-11-29 14:20:33', '2021-11-29 14:20:33'),
	(3, 9, '2021-11-29 17:06:05', '2021-11-29 17:06:05'),
	(4, 13, '2021-11-30 09:44:48', '2021-11-30 09:44:48'),
	(5, 17, '2022-01-07 16:54:46', '2022-01-07 16:54:46'),
	(6, 19, '2022-01-12 16:22:37', '2022-01-12 16:22:37'),
	(7, 21, '2022-01-18 13:59:23', '2022-01-18 13:59:23'),
	(8, 24, '2022-03-17 18:05:45', '2022-03-17 18:05:45'),
	(9, 27, '2022-03-23 15:20:03', '2022-03-23 15:20:03');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Dumping structure for table centurystone.oauth_refresh_tokens
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.oauth_refresh_tokens: ~60 rows (approximately)
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
	('03c8fb6f34dd1490665f2e8550f622ee3cfea990de82cef55815b8b81a2fd4e3dfe42a9bf92b0bdb', '062368b02826374602791fd8a46f13115da1e94a43b344786e1fa7ca9e7d4f92c35ef109335ac2b7', 0, '2022-08-06 15:57:42'),
	('057fef71417dd45b6c42b2e7e63121adefa4618c6e134317de04405f4eb2ab73d477aa84524e4a25', '133ea57a8971d9cf495cbf32209d45429a2234b1ec2e343d8c54145ac6750d3ceabeb70a354bd315', 0, '2022-07-20 21:42:43'),
	('07a520995ce0ddf0740542fb55fdc21cc3f81b3ab08cd953daeedad0a128d94f5f9ce48bca0c9142', '1af951feb7de86e01603b5f3cfc7d296a295b95352e56c6a0a46057e4162aa841998701b83e99a3f', 0, '2022-01-01 15:11:55'),
	('094c2a78c9e3c8534a0f4c322ceb302e9c2edbaf3cbe33785d56120f5fe586a30d977ce40240c7a2', '88473a28696d38f8d5f9ce564e50341daf949a212297f8df07516d5bef7cf24c3f3ada943723505c', 0, '2022-02-25 10:52:36'),
	('0ad3594a84109935429ecbca12d521b653fd78b85f676dd6cbd2fc46bd2d4a16146459e5acc4e12c', 'd207121f2ce71752e3ab0ea91174cca8df4d44618921b10fce5672f99d85dc80fdf308a2bcac51e7', 0, '2022-02-25 08:53:49'),
	('0f5b828e6bbd2d8e4810688c04ff894703cc006b821ee64a6c1c92405e56acf000911860e9bb5ee3', '5901ab5f9d0b8f4c89b3620bb4a9e800dfe9644d7f71c04bb59007978763f7ad9c372e260889c35b', 0, '2022-02-20 16:56:00'),
	('1330442bb776e87fe95e8c946aca4081873b65f696379ad42cbb8295a5f837e05849eceaf010c7f3', '7a115f8c3a85b78987758870a77ae8b943e1553dc0c8266bcbecd63476c28b5052bb6c4795fc8935', 0, '2022-03-16 10:26:36'),
	('14cbc983a92b17e56edaf2e19992007e4eab9e94e41c40b87de7ea05d4fe4f8b3e8757c195499215', '88e5e71981f7dfd9bb83d03b59598977fc625e13885af3328661383467f3ed1bbd2c2f55c44424ee', 0, '2022-07-16 16:23:44'),
	('16523f89b615696042b50bb3b69c44f2e7afe6d279c1b7e33b30b1d4dfa590eab47e45f545cf3758', 'a92eb1d164ff6c3ee8332904dac31e316049b8e8232207dd565fb1237ae3452df4caf16e986c59c3', 0, '2022-01-06 14:59:10'),
	('183597cf147a09321fdd7a01f8ccf07e453712edb358efe9995ff438a85ae8fa708bdc63947caeaf', '6d1a27fa928028f8d0d2dece2c1e659a0cb604b8a5b0194814085552bd079fcd0d1f9065b1ff8051', 0, '2022-04-22 13:10:50'),
	('1ac432cf464cd438cd8d6823bf2268fe9384fec65c3055c00b8753c441ef457eaefc8f90e3004a3c', '282c5bb128bf889682bdf00a2ac6f17250483d0696c601abd21c89f950f88154288dcfe109c10ca5', 0, '2022-06-12 10:50:07'),
	('1dd54d4ac06fcece26790f2d53e0859910a6da7ddde730ed646b35f9085d45f8525f582558f06c63', 'edc1d2d1f495feeb94bce7920a88e82529d1c2f0eb2ab90c9d32f7fff4a04b68ed2a21bf1db07c2e', 0, '2022-01-03 10:55:53'),
	('1e9840697360d9f12a767ae61af506e8b4bdda9d1176d6d370e9c168ca0a4a9eb480b45353f1eacb', 'f61e5b09c7ff64cae2ce980b7e1fe74f62a81dc915ff8d185e52cb96cd61bf28b82c84cb8a87c3c4', 0, '2022-01-07 13:45:07'),
	('20161a545437f4aeac5f130ff39218154a8a2cb43dda5eed9488395e7a7c94c678ec3030f56dfeb9', '1b15313fdf83eb95405cfa2bdaa758238ded8ee6d2d3c0a41757bace770f4283bcb39f75e0f4fe4c', 0, '2022-02-03 13:58:56'),
	('21d0e4eb9eb542ceb689681584a2458945847e5f0395b4630e33ce231b1fd89960e5a23d4849fe73', 'e02224aee8a36c1903f319bb2408660d3fc4020ff4bbbb5150cbd5d3f51565d216089efc350e2e2a', 0, '2022-03-11 09:24:16'),
	('23df2b47b10bb4ed4f2b80e3dfa0c46f7f1a19b56f08a7ab3b9ddf0353d3097fa08649691d1742f5', '5404088c2011473612f24fa922070fa82c4dc090f13ceead5643080ae1d9b231125a985a462d03c4', 0, '2022-01-02 10:11:36'),
	('24ad297f41048d4da78817b874bf1ba6328779f459e16bd02ff5f1bf36303fa37308b4e0a911899b', 'ae2f309499dc81ba1b9bcebc8d6e40ee3fee58015e0bb18931957481a62ef4599a712e29d97d4511', 0, '2021-12-31 21:28:28'),
	('27908331e22c362132a0929beed7061496b903a0df3bfb32c7bc3f47ae474492f186822d9802ac10', '2efd85e71494ca751246b3921504fb5a97b7b596c2d71d8752327a334ef20fbbe9a9fe045bca612f', 0, '2022-03-10 14:30:10'),
	('283a6eaa7148d23a7b2393ac06f0b666b989d7eea1cdcc2419d4d76469bb67e0708141a74c149f38', '920f817ea3bde10d3060c012fe5d0897a733c2d3de97cdebab137b2730136475707b510a8ee745cf', 0, '2022-02-03 13:58:31'),
	('2e734c3439e4a160a7722dac02ff48907ed7b4a8596f1f24d66aa299c898c0fb622f28446ed95767', 'c423192f29188b3f346878441cd0d39e4291cbc25b09ccad3031437c3eb48da93dee573f04826be0', 0, '2022-05-01 10:13:37'),
	('2ece992246c8f29fc38cf6af9851f6c37ae27ae44f7ec4f7d0fab19e3462cb6d95f9caf66d637868', '6825de8187a76bc044e0d48d2bfb6e1a56eb063b04a0d034d8d9d783ecdf07becd02d473c6992da7', 0, '2022-05-26 17:08:58'),
	('2ed78daedf36d528e830aa7bc491d6c7c8d813b9216d63edac348535d176019d66fb4f5ade533e16', '8de6cefbd38d27fed0eb4b608a964d0c0340ef6db4c5df705950902129e977a26fd02f5855a3e5fb', 0, '2022-01-03 09:43:15'),
	('359da1ad4d9f186bdf6aa5657bcf45a326312b4eb694710c5191962e1bcdfa45f039759363d3a0c7', '30e62d6191aafb926f709786df5c416315bbefd085ca13bdf3e02ab79185ede083fb495e9b9fe392', 0, '2022-03-11 08:56:03'),
	('3a44587141441ebf0b4449dcd3cf4c1994afeb6cd6ca4f59478cb357f632757f89751dc9ac2dcf4b', 'f975efddba952edf9a1dbb00449e405b58bd46f9b78be293532805c3acbaa1ebc115c09db27edb2c', 0, '2022-02-20 15:50:38'),
	('3c28da2d8ce3d2304e6bdd180eb47e7b9938ae330f149eea96ebfbaf78fd015483bab9b280e7032f', 'c2aae4bba651ed1ecd87131f2adf0055cfc861b27e685bf30d2c9e4d1bb2400647677aef6c6c594f', 0, '2022-06-18 14:32:38'),
	('401dd5542738b39dd8a8656431e7eea3881b20c932b4c0d1540f04c3132b9f68ec6aa087b7cbae20', '4f023ba8d6b4193f70cc19cdd8f3332b85a305685f0ffcb4a37d6045d6ee56c2a7d6cd916baf60a5', 0, '2022-02-23 17:45:06'),
	('414b55ea1db5e2947d34e5f9a63315c872abbee3c65a19ff9ad9270197877f0b5a6f6e2cb340e667', '3a2d967c3cefad91b4ce248e0faa7c12b774512131afccb7e9d893636feb34b383b9916c14af1cbf', 0, '2022-02-20 14:35:51'),
	('44747c3ac74559af70098f1983fe313be4a931d25ec054ce476f0896f44517189a3b5bce9c164c05', '3e26a42558a1a67ec8177c11e6862b3f1a4c0d48b0e41b30ca2f305559d8fe32e00d935880baf173', 0, '2022-03-12 15:38:45'),
	('46915a7e8b9c1946177072918206d40f2ccdaeec6533cf2cb78ac72847020495befb69c56a29588c', '7a15949122bdec8b9f3e51caf97cd05a50d9188179760c6548f29b34fa3723dd52c92058f92d9bdb', 0, '2022-04-24 09:49:32'),
	('48dc3bb3c75f718e3a274d091b508e53ea64a87b711b32cb5457139f75b39fb6b36a58fb9304737d', 'a2ae2ddf9b2cbd7b71b0d41aa56772996b99d210f81aefc91e8c8fd1cdf3971d5a137ece9393b412', 0, '2021-12-31 09:11:43'),
	('496de33f82bf8b191af751af04e521b9beb944c9db6eee687ff40a5617036c0e84501f29e417881f', '4c006c05314faed317f75b7bd547ed957475eb3bcd859649c92b68729062708b1e0937d8c930eebc', 0, '2022-02-20 16:13:40'),
	('4db17f339a4574fedfdb7bcab1804101e5112589e11839677ba6e1ddd5a48f78fc449d968ed04f44', 'e9594c46c907139e4f06b3a8b789c0291f7d5f2fc7edfe1335d325a58cd77c2e941f5578b7a5f816', 0, '2022-03-14 10:47:20'),
	('4ffd9bc1452d534b27ab5923533e8c8677b8bc9bca9cfa8eeb20976f805eeb34e3a082b92d1c1dbe', '42fca84e858fda4977bfed180df2c92e02679f701accc8b886d26b3ab65b10a42d2bd762ad2eff12', 0, '2021-12-30 08:47:11'),
	('5318c3c4bed33be47a13ab60c581b4113e6a6e3d6e330e2e96abaca0ba8f10b259dfa2ad93679a5a', 'a92039571397a9ff6fa562706556302d14d2b0a85681d616adfeb4f5df3123e007512d55fadfefd7', 0, '2021-12-30 10:36:58'),
	('56edd46f6645a17d543e8812c89a3c32bccc2aec69f8aa1e2b9f4eeee0e0a6417ab65c6da52a72b5', 'f753ffc45428a29c81fdd5a1093ce6ebd6811392ff4a6735e5bb60ccbe9c2422afca983f525e41f7', 0, '2022-01-05 10:47:38'),
	('582a862db7052337d88adb79afa4f1cf3b372eaed765df777ff5feba6d641dab9fc0cf93696e6446', 'df88b4208f7a8774e251355181a75fb55b5cf401e4d20974e09433faf38a0408c00d5fefc7d30529', 0, '2022-06-18 14:32:09'),
	('58499beeab92c4b5a1f66f3a6c4e31392798a6f5856cebdb4c7498082b717e39002199a67a61995d', 'bb703dc6d9eb27690940f9f3ae046caceb14e579195b48d2221f8960ae1812bde5ddd4b1fc1149c8', 0, '2022-03-09 09:31:21'),
	('5c56a7141caa9413c5253fe58c21c5d51fd5d9a2f34e4e3119fc4801bae9e2d8b3384dfa95f098f3', 'a8518ff6b44e1cc08726490a1ed0001c911b926a5c52429514e677c278de35f8401516951dc33a71', 0, '2022-01-07 13:47:03'),
	('5e2626d5799d2e07a102996a598f1b5b751973a2f8845a03de89f3e06bfde20c7148bb9ab4ada3e5', '4c4444184b8530e8cd9c4dd51823258154d0a31638da435bbe3ec2d7b0953d0e51612e50985cd588', 0, '2022-01-07 13:45:24'),
	('5ec77f6960353e5d808d8e55042c33c99b84d94144fa5f130a2517489ebf69aa0078f27dc8ae3f92', '6750490bea20803029f0de16ab21e43f1b05c1f705aeec53c4a1d99875791c3d1590348d6bbe2c8f', 0, '2022-01-01 16:59:11'),
	('5ee4f12b80a2acd6e764588ce9ecd225fd77eaad59fdb98035bd4dbebdfc4a3b2e0457ee1579bff8', 'bc7e34ae778e69d796ed85e6c56b6cb4376db24930315ac6b5a3fd62879968eec88d8144e3879831', 0, '2022-02-03 13:56:26'),
	('647d39518dea735f7443b077f183e73048fd52968401505847b419c425553b906a3b7707294c2ea6', 'bd93a6ac850b20b58d0753e220f3aaaa278f4afa7a202e89def759318760c3c3852ea46d9537552a', 0, '2022-04-18 09:20:24'),
	('6680c34452dd76ea87e8c6181ac4989006f0e68c5e0270c4dfbe83d98d46dbd456505691cc53d08e', 'a096e201ada1934189fa446dc596e5f1050399df4ee6584b92f2f4af71775d978810d8a245a0ef4b', 0, '2022-02-11 16:30:29'),
	('66852b2187e0812fc8be037b18896cb89f6e3f26308756dc76c88945beff0801260441baee790622', 'edc5b08c627f1986856cb06af32a66c71414a4f1f58b3560da4ce4aab2628d2c5c197873876d9ea4', 0, '2022-05-01 11:27:34'),
	('6b5c7e916f7da14be0a485526fa431bbdff7d99ab6660a403a822d058ab7b019f7f82ed10d97fdc8', '4aaedd216e76fe48dfa5365885d0d01de3d4a543a8d401569247042cbc5eae5a8646e9a7043bcce0', 0, '2022-03-13 14:14:00'),
	('6d723c1c52428dad44ed036cf4a0267465f6f91b3fff1895c7d33a6b49cc375be115eb738054a1a0', 'f6e187239f3ba0f02edf831b7d9ae10f4b38c328091d62494706f9c9249a922ac94fdd29fe361782', 0, '2022-02-17 14:03:01'),
	('703bbc9a33111ae5db2c3ad05efb9550f8ab45ea714508c2992768e63e7f70b43d4a152bcaa2421f', '511e14340f8100551f875cbd66015e8689095e0fe206796821522fe718a6ef7cb8218a89109846f9', 0, '2022-03-10 14:51:15'),
	('73a759fafd83bd1b780c35fd6f93c8b76ed32866e602b836d50233f01a0abb19c6092a2c32726e6a', 'd2bccd9390efff36051c6703475f655eb97f0143743d267f6fdb3d13ee0e694fc0a881f129c1b98e', 0, '2022-04-29 14:50:09'),
	('74646b235280a13c56289a57a466af94bd0c6c535318ea52a173cefe64ce1c312dbce72190621d0d', 'ac0fa6f7f54ebd199a7abef65c82d9ca8e3675e3b122d1cf0ef62613b20fe66d183f1d838dc8aec4', 0, '2022-01-07 10:58:33'),
	('778af5958324bfa879494b372552ae39bc18945a823ef8aebd335da18bf630903e59c99d1c0825d2', 'acfdb6be895bcee87fd29a062cd0adaf801780f0df38d9887724bc6b1eed9b956192f637e8a36d7c', 0, '2022-03-11 10:37:36'),
	('791f8443de7f3f9990839a45eb2becda6efac2adf6f40851a1ab04826b10d1ebb1eb6d4bdfeaf3a9', '649a38119f89043ee5f10c94719ad078d78b37ca7ad1e575846ccc8b06337d9134c544c7b7bd4970', 0, '2022-01-05 10:56:16'),
	('7abec9877ca438920bf8fd03a7eb3b827c3cdec2d834b3471d1ab51c76ea109f895bdd387f7ed4a4', '48ef672b113879e32e19a7b74b57b350fda7470b7917632691994d15bd78e7594bb01aac684c4e6b', 0, '2022-03-12 21:02:29'),
	('7e43c9242de44cdc165a8f3952ed76ef60afa22be071ee2a4d81d145aa89f744584896309f8558ad', 'dc8bd74df5e4ba225fe8845c1fdf9bf8798e981a427de808b977c6736e2e8904e78c898aeee7db83', 0, '2022-01-28 09:50:12'),
	('7f5c9511aeb11d1b08420e6f0ef2772328f0aab43c0d1d4d61908680837f2674fb50f1f132e3da39', 'd0b3f898047acf701a3727ffd4b029d1903c79238feb738df660f3eb1056396d8a1bff060da344b3', 0, '2022-06-18 14:40:49'),
	('847a1e6bcf7fbc45d3f47eabe439d1f3cec078a32b68817689cbe1bc645fb3253ed875b402d7b99a', 'e58ab41ecab75b179e07464598ab43d20e5b8c05fa8101d834988626153ebb51b390d7ca6ad51b4c', 0, '2022-03-12 14:15:05'),
	('8787c154e9acfa9c3860eab649a1bfa6d6fc54759f234e00c03a7b146f27518ea0f4787da028760e', 'c2332cfcdae8fdfe92a57a9ddcf30e6aff6386cd8e27019a621e638ceb8ad049e338cfcb18127061', 0, '2022-01-05 10:53:03'),
	('8a1a03005f25c7eb5ac49c03cd5f8a6ee172ff49e42f14ea53831d567ab0f33245de9e0dcce50dba', '9211032d8c427dec1f37e5bbd9774ff3e8118debfe21e9484278eb5e24ba80737f17572f04317b85', 0, '2022-05-01 10:21:55'),
	('8cccb1ef2ce6646f676bca1752d2f73f1418e3d78adce6d4420ca8b960b939330743a988650aa757', '31a6cd3775751268154b6f5966fd36a13b98468a5ad3f874152c5730b3c57b048c82396b63262800', 0, '2022-04-18 09:46:05'),
	('8eb27152cd9df8b09db4f8cbdbe3dd57eccac492dc2f142ed275ad968e0c4276ba73af2960476ec0', '3a0914169bceb21a5eadf841aa97b218eaeb9d4056e3cc88a44659d461d24a8cf65ac187b52f0e44', 0, '2022-02-03 14:00:41'),
	('8faba5799b2629812eeed4a04c7da94bc310abcc31fa8a8580898e1ac1db1ac8759fbea1b063276a', 'ab6f507443b97795a257b1b7795dca5a9322048f3123d9455648f9faf92273746b8713904654dab0', 0, '2022-03-09 09:49:06'),
	('9118eab6d5c7fc3e38de7c7e32688ae5edbdef30ae88fe676957d8ccc1d053104c5736a016d4e4b1', '545f1b63a241d62eb9935c1150bf74fba0eb0e560ee6423c05abfd01f9ca3589a95a0776c62e769a', 0, '2022-03-30 16:21:32'),
	('92c7c86f04087d9d346e5397f3b09e38962f2117d5479540941d4b4080d2dadff85c82db7f79662e', '80312eca208644eaf43563a105c7c3a4942c8d53fd12874c4077f7c8b972108b6a71f12934edaa8a', 0, '2022-02-06 16:57:08'),
	('945fd81339bf9aa2d22f8315bbd2215ba4ee3c0b81e923b75d08da17d49eb474ecc6ae6f56a743b6', '8b4ab8f12d8101502b7f8009bc8ebe034b51b03b0de151f629c5002ae004af60412c90857f4f1153', 0, '2022-05-01 17:38:09'),
	('94cbc313909fd8c4c13396921e86a395c27f2a6ecbd8a1ca70aeba7e7eb3c88f885dc0fc51195756', 'e219bcc9ae1bd724fc639e95b9fbd13acaac8d133ac213a3d25ee2095149835d85653870444ad26d', 0, '2021-12-29 14:17:54'),
	('96b3de13e10acc7581931b22ca998c49a02e1946ad4d22660e10b665fbb5102a7199147e85171e48', '5595b24a821baab0883df0fc4ede78f701360099a2c5af4830d41fe7a9b94e54478535c690e67383', 0, '2022-03-10 11:55:28'),
	('98b5f037ccc114803795efcab0e0941972b5bf7c03df2d34a13ba2c9056f1e463f039c7059560bec', '82f2ea2bb09827e57761fb8badc673f895d1d84fbbec7db7ef50af99cbdacc71d13a1f05ea8c1f99', 0, '2022-02-20 14:36:26'),
	('9b48fb52104d13441105574b0922b8509069892ce2644e0e67eaa8e97050f27a4c7c84a40d26784b', '9676f88ad2534fc6a4c308b199a6f5242422e8c72ba5f885a8bd032cf223291d7c14710b2fbbcd9a', 0, '2022-03-18 14:45:51'),
	('9b746425f13574c443fbc585df447f634da792ad967e282134ec9dde909b64c483fae5a6e583d60e', '5b3c992846a31bbd0d59aa786d33692b019f16144d2d5aaf84327f08fa8545cc08be1df814b6cdd4', 0, '2022-02-17 14:03:08'),
	('a09e741b7a807090b507d7b0569636235c1e2eb3f574b465ed77fb6eedcaaeb3009958f02fa8926d', '20cfa4848a1e62cd7019d140961082e341a709eac5eeb3097d4efef144233aa47cfa0b5c05fd6c44', 0, '2022-03-09 10:05:33'),
	('a6618c3927047d6bd0e18b5b10c1e66089ae944e5b2c26b368e2df208cb366a13f7db6c0d3475e65', 'ac4735c1046173d0783868719a3bbe20873e6f9a7b7c63778188f4906a8c5f3ed9bbe783aaa9394a', 0, '2022-02-26 12:38:16'),
	('a81eaac78418201820d9b6aa1c52844d2fc3b369df0703f2acff6bb71b9b48374b0dfc4b127a05f9', 'ddfdf880c0b88cd05af3859a5c140f859eacdb99c4004d39dde623fc360c31c83c031ad18b161af8', 0, '2022-03-12 15:47:51'),
	('ae755138ab309a66f2f0b68cd12248f139305d7884395754557fbe4c94be3093ae2b37a21ed8a25a', 'f7f9899e7deee2cea5476527ca09e758a9c424ee7626dd748e170ed9e960fc2f2d965d19c061dde6', 0, '2022-02-20 16:01:53'),
	('bb0736ad60956078c8ce1354e9b0129964a7c9246c474526cae478888e040f5a32664d8bfe610a62', '27f7d5b909776046499d3e176490cfa47cc32b4457278d3173f7a3e1fd7b06b5e9e69b4c7842304d', 0, '2022-01-06 15:02:41'),
	('bd6c2961b74741b337a4235058f64900b2fe003ec8ce612a32c05004a35619d2aa008993d2e2475f', '4811fc7ea134d5a51078bfec72e805dc42f03cada1559bface4df55ee061cb7f6d2ef5e780709c97', 0, '2022-04-24 09:47:12'),
	('bf15b8dbbcde0e4c939b3942deec47f28e92617abee6b647ac8c3c1d644140917b22d2c622b218c6', '14ca99ed21fb018e37cdaf4024db9768eed5934fcecbae96397325fdc126317ce6afbc94d3fd5f55', 0, '2022-01-01 18:16:40'),
	('bfa8045b3ef964520a224785bd2cbdbc1e040f648745e468ba606ade0e89f7a16d2e9d639939e858', '38e0911c23d7f44b018fbf2272b31bcd7279e0bc79c027e7f7bbeca1148c4b22b5ed3ba9f7ec2c10', 0, '2022-03-27 11:33:54'),
	('bfefec71d8ce58739acca0e5b350d854f6ad89ec4de3820505393cf88206c0cc96ebccd91b83277a', '4bbeb0b0493c500336bbefa968ce761378ef5585a18afdbe8bf99597aec4148bb4114c12eb585458', 0, '2022-02-24 17:10:03'),
	('c399a1e9aa797a77fdfe7a93456431d610c9bec12a179c5df919f912f58a4fe3ddbb85feda7134ab', '55a69c426585a14023b793abc584798e2690338a4bd63c1226c6e3c49ec1ee66a3684425077816b7', 0, '2022-03-13 00:08:04'),
	('c4bb6e8ae77caa2eb3947c3aaf5238b711ff27ec25b207b94c191f5b16cad08e9e208c576d314d68', '34159482904f4ab19d4eb175ecc37fea4e7fdefc6973a0fc1b605a445af4668f662cee6590c8c9d0', 0, '2022-03-12 15:09:37'),
	('c4ee1c752fce7cb07451d77a8672d7cc57f1deb8eee2b710e33f20e163bbe01e37101d09927cf0b9', '63c5373fee4848b5e6a0eee74218e48e1a4309230c42990066e203986a1e19f39ceb78990e1e3367', 0, '2022-01-01 17:01:13'),
	('cac4b1a3cf5360cd22b867d1dac0b407bf380bda16e4713e852a01e7109d93a37b56db083c88975b', '0aa7f9858768c73b8cbf52c785e883f2d1125092c61c2b7574cb300bba97041ff1758492828dd89c', 0, '2022-03-09 10:45:11'),
	('cd4834f74929bf1b8b3c5a273f24431689080b5e3913e6435d8f939ec989f36a893555fd68c2990d', 'e173258ebf6d3333852739b3dda0ecdba7a9f54b3db7aa290a102142573f3b6cc76921fb78712be1', 0, '2022-01-01 15:13:15'),
	('d63f115208f557e2caec9f3ece3fcbe2cac8d2309e1b9eb5b497ffa6d1908a5dc7d97c82a505db45', '86ab8b3bfadace30505d668f2da1f9b4eb12ffefdf3386897111bd2fcd22862d40da9f1b9ae60d33', 0, '2022-03-10 10:37:40'),
	('d83c4e215ec9bb8e7ce1c2dd75c96f3b422c94b348739c32063b2a4023f4f7e487318f38b9c4c6ad', 'ba01e00db82b80167d28b0c157af30dbb4ed184e328e09354e9af004a741d641c40007be1b9855a6', 0, '2022-04-18 09:26:24'),
	('d99ae1a16a749f7de73117c414757ce7ae9202ccae8a821549cb04ee6c618912047fa111f2803b1b', '43deb2ec6f2feca1cff14085c3de5e8148572dbaceb88724a7ee1506c7895728702eb286d8d750fb', 0, '2022-01-01 14:19:17'),
	('db121daf8d705ac31e084591713a7689cfa8526767e2777f2173d412a43140c4bb5422f773d1e131', '647e8db193d149e7c653b9aa2e311d02536deb212424cb5f09cb32f86f463332806386805feeb85d', 0, '2022-01-07 13:53:00'),
	('dcf212fb518d45fe68f0c71a3937d5ce7d0941be32dcf87300ce79bfa8b38c67a5446420b83a8abb', '5959efd6ac469e73a8dcb12644cfcdef6886d9e77368e231ab4ac3476f4e943872f51bc40a514cff', 0, '2022-02-25 08:47:06'),
	('dd4f25b8fa3b8c9c5a1e5c13af1c286658de7f0779211f704981806aa179fbff862de3b012a0c050', '8f8ab648cba69eadef3c9090110021362c6bb0343ff141ca1ef8bb452ea07a7f1e5cd7d16cb190e9', 0, '2022-01-05 10:40:28'),
	('de78c4cae7cf1635966d62fb33133b6c247c2d6197dfc4501a5cbc2916107e6c0d6327efadb7d91c', '63a9c4180c1f4771f3394c6e121810f17ace7f4839a261c4a13e2a92b7bf333bf3f511a766676123', 0, '2022-02-18 22:57:16'),
	('dfe68c2ed5258e0854157a52d7c3bbc776fd9bd259175f9c818083e8573fafa9f77de78a435794c3', '1f3e3940225cce203f8ca28c51e554b41c818f6ea2c77d79cf63422f437b5c347a845fcdc9587633', 0, '2022-05-01 10:13:11'),
	('e0bf0df66bdc6338ebcc46a9998c532d8dc141606074d4ba47cf4e154be24e5222ca2a497671bcf9', 'f3c087914dc41e06630485c7d1cb06b329e53da93b09d436cecaf34d3346ed10ff17f4ff55874b1b', 0, '2022-03-13 07:33:12'),
	('e5b4e24513979f8a6dae2b85bf010b1b270fb77fc8ffa38c357b72b602a9680c38679cbfbc528655', '8d1dadac398bc710f99d54519897c239e6992480707cf29fe670a2d56522525aa1323c4b9e658184', 0, '2022-02-20 14:37:59'),
	('e6956818df1e58d85fa5daf5fe0adeda76aa726703e2ba0a821f3a37b20c1151f8d0b268d824c1e0', '9dbd81c39d6bc850c4a1384c7d35a3648a379e957182377c8b5756a7a8e7ea182a9846f738fd4882', 0, '2022-01-28 09:44:09'),
	('e6b22769ca128e6f51b187ccbe9f3ca5e00746958554b661ed11b98ef0d6c68a1c194ed6055548fb', '81eba060aeb915d26a275ec6b4d8224099f5f3515fdc2c9e011f8d27843207b0fd1c1dbca91568b3', 0, '2022-01-03 09:43:01'),
	('e79386ddf6a84dc337514d08183f43c0b562fa434d839541bbba96933f664ee1bae20c6ccef03195', '5062f2662fd0db803aee86e46e069d3f59b05553d1a65f511e4b90dba8eaf8a0dc2970b117b7eb42', 0, '2022-03-12 08:53:06'),
	('e8c306b1de8b37feb48d3b1adffafd3a6b2da3fc19726e56fbae2755065d9173b50a15d9ce316b1a', '1a502c90f83be58a21b7c7056b2bdd5366a9a55ddde7471df34e3cb289f3010918cfae22a7a9250d', 0, '2022-01-14 14:52:42'),
	('e950760acf59586933fb48dcbe9a89159cf886e461903d8a5e17fef9ba1441bec0f6eaeff9163138', '76915b54dd888aaf6edb4d7b4478d579e85280c2dcdb923c0333892f9491f5a9bdd9f6d01a6c6d68', 0, '2022-03-10 14:41:22'),
	('eb433d08037e81da6d3d5c776b758501f083d18da7d757192375c76a3073d883c0c7aed9518036ce', '70be05621c3bcdededd6d6fffc7e515564d050d69c4afda29316a863e185e2c798edf45560ab85e0', 0, '2022-04-17 15:31:00'),
	('eb947257718a239dbe62e985837229dcaea346943e96317191201564c5bf2e2b3a739aadcab594bd', 'ce1f7c9698f889803bd9d32c21a7500cfb88908e128dfcaca8ffdb7e8e2a75a966028ebab78ed090', 0, '2022-03-11 15:39:46'),
	('f1a544d73a90fbd6b3f773dfd6f08ced88ee2005bce109e020824f01181e19928b441f6741a4d155', 'd51d74677aded9eace36db0f72da3d20c1acf7934b5122a9f19b2815a9ddaaa02dc7fd4c15ce5997', 0, '2021-12-29 15:00:13'),
	('f21d2c56ae48ee2bf3934c0b669c26b8a524232fe305af82d4be4f42299b8f9cc5eb28cdfbd06e66', 'eeb4dfaa94d35aa62e46fe468719c7f463a78931cb4171ff17023e9939930996f3e30576c192a98a', 0, '2022-02-17 14:17:39'),
	('f3fe2a19acaf18d25ece6f0979a7bdbc042406cc4041a425450dea51b48fe553e296974f12d47d58', 'e112b57c64c11d35317e4f75456fdc5d014b1208441b64e07c1a3016d3b5cf9cc75ed338f64081b3', 0, '2022-03-16 09:56:04'),
	('fa8b96a938360ef1ad73b430049f001c2a06f6c481b141bdbbe22cbc9da8d34ddfbecd72fe9fe44e', '85c6b92fd9d680be7c25b519656975596126adf82013f34c16338407512fddd9c383e12e32ff084d', 0, '2022-03-12 10:19:24'),
	('fdfb46b5a7ab1c6fee6f68f26656bd2e7d37cabbd402ade0211fbf71ec026863349676cdf8b3deb8', 'a523bada25bd41abd9d56ee97d1a2ad4f686f1e04ee36d51e531c79edf601e63e993ffd165aae2b8', 0, '2022-04-18 07:53:22');
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

-- Dumping structure for table centurystone.option
CREATE TABLE IF NOT EXISTS `option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eshop_attr_id` int(11) DEFAULT NULL,
  `type` varchar(32) DEFAULT 'select',
  `sort_order` int(3) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `show_image` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `draft_name` varchar(150) DEFAULT NULL,
  `eshop_attr_value_ids` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `option_eshop_attr_id_IDX` (`eshop_attr_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=767 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.option: ~0 rows (approximately)
/*!40000 ALTER TABLE `option` DISABLE KEYS */;
INSERT INTO `option` (`id`, `eshop_attr_id`, `type`, `sort_order`, `status`, `show_image`, `created_at`, `updated_at`, `draft_name`, `eshop_attr_value_ids`) VALUES
	(766, 1688, 'select', 1, 1, 1, '2021-05-21 13:59:06', '2021-10-18 17:22:50', 'Màu sắc', NULL);
/*!40000 ALTER TABLE `option` ENABLE KEYS */;

-- Dumping structure for table centurystone.option_description
CREATE TABLE IF NOT EXISTS `option_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=901 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table centurystone.option_description: 2 rows
/*!40000 ALTER TABLE `option_description` DISABLE KEYS */;
INSERT INTO `option_description` (`id`, `option_id`, `language_id`, `name`) VALUES
	(861, 766, 1, 'Màu sắc'),
	(862, 766, 2, 'Color');
/*!40000 ALTER TABLE `option_description` ENABLE KEYS */;

-- Dumping structure for table centurystone.option_value
CREATE TABLE IF NOT EXISTS `option_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `eshop_attr_value_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `draft_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `option_value_eshop_attr_value_id_IDX` (`eshop_attr_value_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=847 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.option_value: ~4 rows (approximately)
/*!40000 ALTER TABLE `option_value` DISABLE KEYS */;
INSERT INTO `option_value` (`id`, `option_id`, `eshop_attr_value_id`, `image`, `sort_order`, `status`, `created_at`, `updated_at`, `draft_name`) VALUES
	(820, 766, 247, '#ff0000', 1, 1, '2021-05-21 13:59:07', '2021-11-23 17:49:38', 'Đỏ'),
	(844, 766, NULL, '#00ff40', 0, 1, NULL, '2021-11-23 17:49:55', NULL),
	(845, 766, NULL, '#924b9b', NULL, 1, NULL, NULL, NULL),
	(846, 766, NULL, '#e4ff14', NULL, 1, NULL, '2021-12-01 15:36:28', NULL);
/*!40000 ALTER TABLE `option_value` ENABLE KEYS */;

-- Dumping structure for table centurystone.option_value_description
CREATE TABLE IF NOT EXISTS `option_value_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=1491 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table centurystone.option_value_description: 8 rows
/*!40000 ALTER TABLE `option_value_description` DISABLE KEYS */;
INSERT INTO `option_value_description` (`id`, `option_value_id`, `language_id`, `option_id`, `name`) VALUES
	(1489, 846, 1, 766, 'Vàng'),
	(1488, 845, 2, 766, 'Tím'),
	(1437, 820, 1, 766, 'Đỏ'),
	(1438, 820, 2, 766, 'Đỏ'),
	(1485, 844, 1, 766, 'Xanh'),
	(1486, 844, 2, 766, 'Xanh'),
	(1487, 845, 1, 766, 'Tím'),
	(1490, 846, 2, 766, 'Vàng');
/*!40000 ALTER TABLE `option_value_description` ENABLE KEYS */;

-- Dumping structure for table centurystone.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '0',
  `fullname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `note` varchar(1024) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `type` enum('order','table') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'order',
  `status` int(3) DEFAULT '1',
  `code` varchar(125) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `total_price` decimal(10,0) DEFAULT '0',
  `fee_shipping` decimal(10,0) DEFAULT '0',
  `extra_fee` int(11) DEFAULT '0',
  `payment_type` int(11) unsigned DEFAULT '0',
  `token_tracking` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT '0' COMMENT 'nguoi xu ly',
  `admin_note` varchar(2024) DEFAULT '',
  `created_by_admin` int(11) DEFAULT '0' COMMENT 'Đơn được bởi admin',
  `order_from` enum('web','chat') CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'web' COMMENT 'Đơn xuất phát từ đâu',
  `province_id` int(11) DEFAULT '0',
  `district_id` int(11) DEFAULT '0',
  `ward_id` int(11) DEFAULT '0',
  `address` varchar(1024) DEFAULT '',
  `address_id` int(11) DEFAULT '0',
  `payment_time` timestamp NULL DEFAULT NULL COMMENT 'timestamp dạng integer',
  `payment_status` tinyint(1) DEFAULT '2' COMMENT '2 - chua thanh toan tien |  1 - da thanh toan',
  `coupon_value` int(11) DEFAULT '0',
  `coupon_code` varchar(255) DEFAULT '',
  `pay_gate_token` varchar(255) DEFAULT '',
  `pay_gate_note` varchar(1000) DEFAULT '',
  `pay_gate_type` varchar(255) DEFAULT NULL,
  `member_discount` int(11) DEFAULT '0' COMMENT 'Gias trij KM cua tung bac thanh vien',
  `delivery_type` int(11) unsigned DEFAULT '0',
  `delivery_status` int(11) DEFAULT '0',
  `language_id` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `customer_id` (`customer_id`) USING BTREE,
  KEY `orders_user_id_IDX` (`user_id`) USING BTREE,
  KEY `orders_status_IDX` (`status`) USING BTREE,
  KEY `orders_deleted_at_IDX` (`deleted_at`) USING BTREE,
  KEY `orders_payment_status_IDX` (`payment_status`) USING BTREE,
  KEY `orders_created_at_IDX` (`created_at`) USING BTREE,
  KEY `orders_total_price_IDX` (`total_price`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;

-- Dumping data for table centurystone.orders: ~0 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table centurystone.order_items
CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT '0',
  `variant_id` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `img` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `note` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `opts` text CHARACTER SET utf8 COMMENT 'Size, màu, cân nặng, v.v..',
  `special_offers` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `order_items_order_id_IDX` (`order_id`,`product_id`) USING BTREE,
  KEY `variant_id_FK` (`variant_id`) USING BTREE,
  CONSTRAINT `variant_id_FK` FOREIGN KEY (`variant_id`) REFERENCES `product_variants` (`product_variant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.order_items: ~0 rows (approximately)
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;

-- Dumping structure for table centurystone.order_logs
CREATE TABLE IF NOT EXISTS `order_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT '0',
  `action_key` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `created` int(11) DEFAULT '0',
  `ip` varchar(50) DEFAULT NULL,
  `note` text,
  `object_model` varchar(191) DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `order_logs_order_id_IDX` (`order_id`,`action_key`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.order_logs: ~0 rows (approximately)
/*!40000 ALTER TABLE `order_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_logs` ENABLE KEYS */;

-- Dumping structure for table centurystone.order_note
CREATE TABLE IF NOT EXISTS `order_note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_action` varchar(100) DEFAULT NULL,
  `reason_key` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  `message` text,
  `color` varchar(25) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `object_model` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `order_note_order_id_IDX` (`order_id`,`order_action`) USING BTREE,
  KEY `order_note_uid_IDX` (`user_id`) USING BTREE,
  KEY `order_note_deleted_at_IDX` (`deleted_at`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.order_note: ~0 rows (approximately)
/*!40000 ALTER TABLE `order_note` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_note` ENABLE KEYS */;

-- Dumping structure for table centurystone.order_refund
CREATE TABLE IF NOT EXISTS `order_refund` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `refund_fee` decimal(11,0) DEFAULT NULL,
  `refund_time` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `process_user` int(11) DEFAULT NULL,
  `process_time` int(11) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.order_refund: ~0 rows (approximately)
/*!40000 ALTER TABLE `order_refund` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_refund` ENABLE KEYS */;

-- Dumping structure for table centurystone.partners
CREATE TABLE IF NOT EXISTS `partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `status` tinyint(1) DEFAULT '2',
  `sort_order` int(11) DEFAULT '0',
  `published` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `url` varchar(191) CHARACTER SET utf8 DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `object_type` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `object_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `departments_link_IDX` (`url`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

-- Dumping data for table centurystone.partners: ~16 rows (approximately)
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
INSERT INTO `partners` (`id`, `image`, `status`, `sort_order`, `published`, `created_at`, `updated_at`, `url`, `created_by`, `position`, `object_type`, `object_id`) VALUES
	(39, '', 1, 1, NULL, '2022-01-24 14:39:49', '2022-01-24 14:39:49', NULL, NULL, 'partner_home', NULL, NULL),
	(40, '', 1, 2, NULL, '2022-01-24 14:40:32', '2022-01-24 14:40:32', NULL, NULL, 'partner_home', NULL, NULL),
	(41, '', 1, 3, NULL, '2022-01-24 14:41:56', '2022-01-24 14:41:56', NULL, NULL, 'partner_home', NULL, NULL),
	(42, '', 1, 4, NULL, '2022-01-24 14:42:20', '2022-01-24 14:42:20', NULL, NULL, 'partner_home', NULL, NULL),
	(43, '', 1, 5, NULL, '2022-01-24 14:51:59', '2022-01-24 14:51:59', NULL, NULL, 'partner_home', NULL, NULL),
	(44, '', 1, 6, NULL, '2022-01-24 14:52:38', '2022-01-24 14:52:38', NULL, NULL, 'partner_home', NULL, NULL),
	(45, '', 1, 7, NULL, '2022-01-24 14:53:07', '2022-01-24 14:53:07', NULL, NULL, 'partner_home', NULL, NULL),
	(46, '', 1, 8, NULL, '2022-01-24 14:53:30', '2022-01-25 16:52:49', NULL, NULL, 'partner_home', NULL, NULL),
	(53, '', 1, 9, NULL, '2022-01-24 14:39:49', '2022-02-24 04:50:20', NULL, NULL, 'partner_home', NULL, NULL),
	(54, '', 1, 10, NULL, '2022-01-24 14:40:32', '2022-02-24 04:50:21', NULL, NULL, 'partner_home', NULL, NULL),
	(55, '', 1, 11, NULL, '2022-01-24 14:41:56', '2022-02-24 04:50:23', NULL, NULL, 'partner_home', NULL, NULL),
	(56, '', 1, 12, NULL, '2022-01-24 14:42:20', '2022-02-24 04:50:25', NULL, NULL, 'partner_home', NULL, NULL),
	(57, '', 1, 13, NULL, '2022-01-24 14:51:59', '2022-02-24 04:50:26', NULL, NULL, 'partner_home', NULL, NULL),
	(58, '', 1, 14, NULL, '2022-01-24 14:52:38', '2022-02-24 04:50:28', NULL, NULL, 'partner_home', NULL, NULL),
	(59, '', 1, 15, NULL, '2022-01-24 14:53:07', '2022-02-24 04:50:31', NULL, NULL, 'partner_home', NULL, NULL),
	(60, '', 1, 16, NULL, '2022-01-24 14:53:30', '2022-02-24 04:50:33', NULL, NULL, 'partner_home', NULL, NULL);
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;

-- Dumping structure for table centurystone.partners_description
CREATE TABLE IF NOT EXISTS `partners_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partners_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `short_description` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `description` mediumtext CHARACTER SET utf8,
  `tag` mediumtext CHARACTER SET utf8,
  `meta_title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `meta_description` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `meta_keyword` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `link` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `partners_id` (`partners_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=latin1;

-- Dumping data for table centurystone.partners_description: ~48 rows (approximately)
/*!40000 ALTER TABLE `partners_description` DISABLE KEYS */;
INSERT INTO `partners_description` (`id`, `partners_id`, `language_id`, `name`, `short_description`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`, `image`, `link`) VALUES
	(74, 39, 1, 'Đối tác 1', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643009989.png', 'https://kenh14.vn/'),
	(75, 39, 2, 'Đối tác 1', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643009989.png', ''),
	(76, 39, 3, 'Đối tác 1', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643009989.png', ''),
	(77, 40, 1, '2', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010032.png', NULL),
	(78, 40, 2, '2', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010032.png', NULL),
	(79, 40, 3, '2', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010032.png', NULL),
	(80, 41, 1, '3', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010116.png', NULL),
	(81, 41, 2, '3', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010116.png', NULL),
	(82, 41, 3, '3', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010116.png', NULL),
	(83, 42, 1, '4', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010140.png', NULL),
	(84, 42, 2, '4', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010140.png', NULL),
	(85, 42, 3, '4', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010140.png', NULL),
	(86, 43, 1, '5', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010719.jpg', NULL),
	(87, 43, 2, '5', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010719.jpg', NULL),
	(88, 43, 3, '5', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010719.jpg', NULL),
	(89, 44, 1, '6', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010758.jpg', NULL),
	(90, 44, 2, '6', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010758.jpg', NULL),
	(91, 44, 3, '6', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010758.jpg', NULL),
	(92, 45, 1, '7', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010787.jpg', NULL),
	(93, 45, 2, '7', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010787.jpg', NULL),
	(94, 45, 3, '7', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010787.jpg', NULL),
	(95, 46, 1, '8', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010810.jpg', 'https://www.youtube.com/watch?v=tbnAAxjvd6U'),
	(96, 46, 2, '8', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010810.jpg', ''),
	(97, 46, 3, '8', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010810.jpg', ''),
	(116, 53, 1, 'Đối tác 1', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643009989.png', 'https://kenh14.vn/'),
	(117, 53, 2, 'Đối tác 1', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643009989.png', ''),
	(118, 53, 3, 'Đối tác 1', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643009989.png', ''),
	(119, 54, 1, '9', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010032.png', NULL),
	(120, 54, 2, '9', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010032.png', NULL),
	(121, 54, 3, '9', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010032.png', NULL),
	(122, 55, 1, '10', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010116.png', NULL),
	(123, 55, 2, '10', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010116.png', NULL),
	(124, 55, 3, '10', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010116.png', NULL),
	(125, 56, 1, '11', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010140.png', NULL),
	(126, 56, 2, '11', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010140.png', NULL),
	(127, 56, 3, '11', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010140.png', NULL),
	(128, 57, 1, '12', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010719.jpg', NULL),
	(129, 57, 2, '12', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010719.jpg', NULL),
	(130, 57, 3, '12', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010719.jpg', NULL),
	(131, 58, 1, '13', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010758.jpg', NULL),
	(132, 58, 2, '13', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010758.jpg', NULL),
	(133, 58, 3, '13', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010758.jpg', NULL),
	(134, 59, 1, '14', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010787.jpg', NULL),
	(135, 59, 2, '14', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010787.jpg', NULL),
	(136, 59, 3, '14', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010787.jpg', NULL),
	(137, 60, 1, '15', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010810.jpg', 'https://www.youtube.com/watch?v=tbnAAxjvd6U'),
	(138, 60, 2, '15', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010810.jpg', ''),
	(139, 60, 3, '15', '', NULL, NULL, NULL, NULL, NULL, 'partners-1643010810.jpg', '');
/*!40000 ALTER TABLE `partners_description` ENABLE KEYS */;

-- Dumping structure for table centurystone.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `source` tinyint(1) NOT NULL,
  `pass_reset` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `password_resets_email_index` (`email`) USING BTREE,
  KEY `password_resets_token_index` (`token`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table centurystone.payment_accounts
CREATE TABLE IF NOT EXISTS `payment_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accountable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `accountable_id` bigint(20) unsigned NOT NULL,
  `customer_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `payment_accounts_accountable_type_accountable_id_index` (`accountable_type`,`accountable_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.payment_accounts: ~0 rows (approximately)
/*!40000 ALTER TABLE `payment_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_accounts` ENABLE KEYS */;

-- Dumping structure for table centurystone.payment_transactions
CREATE TABLE IF NOT EXISTS `payment_transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `gateway` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_successful` tinyint(1) NOT NULL DEFAULT '0',
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8mb4_unicode_ci,
  `custom` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `payment_transactions_user_id_foreign` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.payment_transactions: ~0 rows (approximately)
/*!40000 ALTER TABLE `payment_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_transactions` ENABLE KEYS */;

-- Dumping structure for table centurystone.payment_type
CREATE TABLE IF NOT EXISTS `payment_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `online_method` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.payment_type: ~0 rows (approximately)
/*!40000 ALTER TABLE `payment_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_type` ENABLE KEYS */;

-- Dumping structure for table centurystone.payment_type_description
CREATE TABLE IF NOT EXISTS `payment_type_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_type_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `link` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table centurystone.payment_type_description: 0 rows
/*!40000 ALTER TABLE `payment_type_description` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_type_description` ENABLE KEYS */;

-- Dumping structure for table centurystone.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `containers` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.permissions: ~36 rows (approximately)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `name`, `guard_name`, `display_name`, `description`, `containers`, `created_at`, `updated_at`) VALUES
	(1, 'manage-roles', 'admin', 'manage roles', 'Create, Update, Delete, Get All, Attach/detach permissions to Roles and Get All Permissions.', 'Authorization', '2022-01-26 15:50:48', '2022-02-11 00:02:09'),
	(3, 'manage-admins-access', 'admin', NULL, 'Assign users to Roles.', 'User', '2021-01-15 10:06:21', '2021-01-15 10:06:21'),
	(4, 'access-dashboard', 'admin', NULL, 'Access the admins dashboard. aaaa', 'DashBoard', '2021-01-15 10:06:21', '2021-01-29 18:35:08'),
	(6, 'list-users', 'admin', NULL, 'Get All Users.', 'User', '2021-01-15 10:06:21', '2021-01-15 10:06:21'),
	(7, 'update-users', 'admin', NULL, 'Create - Update a User.', 'User', '2021-01-15 10:06:21', '2021-01-15 10:06:21'),
	(8, 'delete-users', 'admin', NULL, 'Delete a User.', 'User', '2021-01-15 10:06:21', '2021-01-15 10:06:21'),
	(15, 'edit-news', 'admin', NULL, 'Cập nhật tin tức', 'News', '2021-01-29 16:11:20', '2021-01-29 16:11:20'),
	(16, 'delete-news', 'admin', NULL, 'Xóa tin tức', 'News', '2021-01-29 16:11:20', '2021-01-29 16:11:20'),
	(30, 'list-products', 'admin', NULL, 'List all product to management', 'Product', '2021-02-01 00:49:01', '2021-02-01 00:49:01'),
	(31, 'view-order', 'customers', NULL, 'Can view order', 'Product', '2021-02-01 00:49:01', '2021-02-01 00:49:01'),
	(32, 'list-categories', 'admin', NULL, 'DS Danh mục', 'Category', '2021-02-08 04:09:27', '2021-02-08 04:09:27'),
	(33, 'update-categories', 'admin', NULL, 'Cập nhật danh mục', 'Category', '2021-02-08 07:38:38', '2021-02-08 07:38:38'),
	(44, 'list-news', 'admin', NULL, 'Xem danh sách tin tức', 'News', '2021-02-12 19:29:44', '2021-02-12 19:29:44'),
	(47, 'view-settings', 'admin', NULL, 'Xem cấu hình', 'Settings', '2021-02-16 00:12:03', '2021-02-16 00:12:03'),
	(48, 'edit-settings', 'admin', NULL, 'Chỉnh sửa cấu hình', 'Settings', '2021-02-16 01:00:42', '2021-02-16 01:00:42'),
	(49, 'delete-products', 'admin', NULL, 'Xóa sản phẩm', 'Product', '2021-02-16 04:47:19', '2021-02-16 04:47:19'),
	(50, 'update-products', 'admin', NULL, 'Thêm - Sửa sản phẩm', 'Product', '2021-02-19 01:13:33', '2021-02-19 01:13:33'),
	(51, 'banner-manager', 'admin', NULL, 'Cho phép các quyền Thêm, Sửa, Xóa, Show', 'Banner', '2021-02-20 00:32:02', '2021-02-20 00:32:02'),
	(52, 'list-user-logs', 'admin', NULL, 'Xem Log hoạt động user', 'Log', '2021-07-07 17:56:57', '2021-07-07 17:56:57'),
	(55, 'delete-categories', 'admin', NULL, 'delete-categories', 'Category', '2021-07-25 14:55:17', '2021-07-25 14:55:17'),
	(56, 'manage-contacts', 'admin', NULL, 'manage-contacts', 'Contact', '2021-07-26 03:36:37', '2021-07-26 03:36:37'),
	(57, 'static-page-list', 'admin', NULL, 'Xem danh sách trang tĩnh', 'StaticPage', '2021-07-26 03:52:13', '2021-07-26 03:52:13'),
	(58, 'static-page-create', 'admin', NULL, 'Thêm mới trang tĩnh', 'StaticPage', '2021-07-26 03:52:48', '2021-07-26 03:52:48'),
	(59, 'static-page-delete', 'admin', NULL, 'Xóa trang tĩnh', 'StaticPage', '2021-07-26 03:53:32', '2021-07-26 03:53:32'),
	(60, 'static-page-update', 'admin', NULL, 'Sửa trang tĩnh', 'StaticPage', '2021-07-26 03:54:17', '2021-07-26 03:54:17'),
	(67, 'view-menu', 'admin', 'Xem Menu', 'Xem Menu', 'Menu', '2021-10-07 16:49:40', '2021-10-07 16:49:40'),
	(69, 'update-menu', 'admin', 'Chỉnh sửa menu', 'Chỉnh sửa menu', 'Menu', '2021-10-07 16:51:04', '2021-10-07 16:51:04'),
	(71, 'manage-subscriber', 'admin', 'Quản lý subscriber', 'Quản lý subscriber', 'Subscriber', '2021-10-25 16:41:51', '2021-10-25 16:41:51'),
	(72, 'partners-manager', 'admin', 'Thêm - sửa - xóa đối tác', 'Thêm sửa xóa Đối tác', 'Partners', '2021-11-17 16:43:23', '2021-11-17 16:43:23'),
	(82, 'comment-manager', 'admin', 'Xem - sửa - xóa bình luận', 'Quản lý đánh gía', 'Comment', '2022-01-19 20:48:39', '2022-01-19 20:48:39'),
	(84, 'collection-manager', 'admin', 'Thêm - sửa - xóa  bộ sưu tập', 'Cho phép thêm - sửa - xóa Bộ sưu tập', 'Collection', '2022-02-07 09:56:36', '2022-02-07 09:56:36'),
	(86, 'create-news', 'admin', 'Thêm mới bài viết', 'Thêm mới bài viết', 'News', '2022-02-08 16:31:03', '2022-02-08 16:31:03'),
	(87, 'create-categories', 'admin', 'create-categories', 'create-categories', 'Category', '2022-02-08 17:16:46', '2022-02-08 17:16:46');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for table centurystone.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) DEFAULT '1',
  `image` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `hot` tinyint(1) DEFAULT '0' COMMENT '0: bt | 1: hot',
  `points` int(8) DEFAULT '0',
  `date_available` timestamp NULL DEFAULT NULL,
  `stock` int(15) DEFAULT '0',
  `length` int(15) DEFAULT '0',
  `subtract_stock` tinyint(1) DEFAULT '1' COMMENT 'Cho phép trừ tồn kho hay không',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  `type` tinyint(1) DEFAULT '0' COMMENT '0: bt | 1: luxury',
  `deleted_at` datetime DEFAULT NULL,
  `is_home` tinyint(1) unsigned DEFAULT '0' COMMENT 'Cho phép hiển thị ở trang. chủ hay không',
  `data_status` tinyint(1) DEFAULT '1',
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_old` tinyint(1) DEFAULT '2',
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_home` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- Dumping data for table centurystone.products: ~66 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `status`, `image`, `hot`, `points`, `date_available`, `stock`, `length`, `subtract_stock`, `created_at`, `updated_at`, `sort_order`, `type`, `deleted_at`, `is_home`, `data_status`, `color`, `new_old`, `code`, `image_home`) VALUES
	(1, 2, 'box-product-1642735878.png', 1, 0, NULL, 0, 0, 1, '2022-01-20 16:07:11', '2022-03-23 14:51:22', 0, 0, NULL, 1, 2, 'rgba(204, 32, 32, 1)', 1, 'CSC22001', NULL),
	(2, -1, 'banner-home-1642735986.png', 0, 0, NULL, 0, 0, 1, '2022-01-20 17:09:05', '2022-01-24 10:45:46', 0, 0, NULL, 1, 2, 'rgba(30, 144, 255, 1)', 1, 'GBHJK-TB', NULL),
	(3, 2, NULL, 0, 0, NULL, 0, 0, 1, '2022-01-21 16:46:53', '2022-01-21 16:46:53', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(4, 2, 'product-hot-img-1642758761.png', 0, 0, NULL, 0, 0, 1, '2022-01-21 16:53:15', '2022-03-23 14:37:59', 0, 0, NULL, 1, 2, 'rgba(237, 244, 244, 1)', 1, 'CSA52001', NULL),
	(5, 2, 'product-image-1642762239.png', 0, 0, NULL, 0, 0, 1, '2022-01-21 17:50:43', '2022-03-23 14:49:11', 0, 0, NULL, 1, 1, 'rgba(199, 21, 133, 1)', 0, 'CSA62001', NULL),
	(6, 2, 'rectangle-13385-1642821617.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-21 17:51:09', '2022-01-22 10:20:19', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(7, 2, 'rectangle-13384-1642821622.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-21 17:51:11', '2022-01-22 10:20:24', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(8, 2, 'rectangle-13487-1642821630.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-21 17:51:15', '2022-01-22 10:20:44', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(9, 2, 'rectangle-13385-1642820110.jpg', 1, 0, NULL, 0, 0, 1, '2022-01-22 09:54:38', '2022-02-09 09:54:45', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(10, 2, 'rectangle-13384-1642820075.jpg', 1, 0, NULL, 0, 0, 1, '2022-01-22 09:54:38', '2022-02-09 09:55:15', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(11, 2, 'rectangle-13487-1642820410.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-22 09:54:39', '2022-01-22 10:00:11', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(12, 2, 'rectangle-13384-1642820075.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-22 09:54:39', '2022-01-22 09:54:39', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(13, 2, 'rectangle-13487-1642820308.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-22 09:54:39', '2022-01-22 09:58:34', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(14, 2, 'rectangle-13385-1642820275.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-22 09:54:39', '2022-01-22 09:57:56', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(15, 2, 'rectangle-13385-1642820221.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-22 09:54:39', '2022-04-01 16:59:01', 0, 0, NULL, 0, 2, NULL, 1, NULL, 'collection-img-1648807141.png'),
	(16, 2, 'rectangle-13487-1642820152.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-22 09:54:39', '2022-01-22 09:56:00', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(17, 2, 'rectangle-13384-1642820075.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-22 09:54:40', '2022-01-24 16:48:18', 0, 0, NULL, 0, 2, 'rgba(255, 69, 0, 1)', 1, 'CT56', NULL),
	(18, 2, 'rectangle-13384-1642820075.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-22 09:54:40', '2022-02-08 15:24:48', 0, 0, NULL, 0, 2, 'rgba(199, 21, 133, 0.46)', 1, NULL, NULL),
	(19, 2, 'rectangle-13384-1642821113.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-22 09:54:40', '2022-01-22 10:11:54', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(20, 2, 'rectangle-13384-1642820075.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-22 09:54:40', '2022-01-23 22:34:33', 0, 0, NULL, 0, 2, 'rgba(255, 215, 0, 1)', 1, NULL, NULL),
	(21, 2, 'rectangle-13385-1642820450.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-22 09:54:40', '2022-02-08 11:38:09', 1, 0, NULL, 1, 2, 'rgba(144, 238, 144, 1)', 1, NULL, NULL),
	(22, 2, 'rectangle-13384-1642821919.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-22 10:25:25', '2022-01-22 10:25:25', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(23, 2, 'rectangle-13458-1642996477.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-24 10:54:40', '2022-01-24 10:54:40', 0, 0, NULL, 0, 2, 'rgba(129, 22, 22, 1)', 1, '1', NULL),
	(24, 2, 'rectangle-13373-1642996817.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-24 11:00:20', '2022-01-24 11:00:20', 0, 0, NULL, 0, 2, 'rgba(197, 132, 132, 1)', 0, '2', NULL),
	(25, -1, 'rectangle-13489-1642996990.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-24 11:03:20', '2022-01-24 11:15:40', 0, 0, NULL, 0, 2, 'rgba(255, 140, 0, 1)', 1, 'CSC3', NULL),
	(26, -1, 'rectangle-13485-1642997703.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-24 11:03:20', '2022-01-24 11:16:53', 0, 0, NULL, 0, 2, 'rgba(255, 140, 0, 1)', 1, 'CSC35', NULL),
	(27, -1, 'rectangle-13489-1642996990.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-24 11:03:21', '2022-01-24 11:15:44', 0, 0, NULL, 0, 2, 'rgba(255, 140, 0, 1)', 1, 'CSC3', NULL),
	(28, -1, 'rectangle-13489-1642996990.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-24 11:03:21', '2022-01-24 11:15:47', 0, 0, NULL, 0, 2, 'rgba(255, 140, 0, 1)', 1, 'CSC3', NULL),
	(29, -1, 'rectangle-13489-1642996990.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-24 11:03:21', '2022-01-24 11:15:51', 0, 0, NULL, 0, 2, 'rgba(255, 140, 0, 1)', 1, 'CSC3', NULL),
	(30, 2, 'rectangle-13489-1642996990.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-24 11:03:21', '2022-01-24 11:03:21', 0, 0, NULL, 0, 2, 'rgba(255, 140, 0, 1)', 1, 'CSC3', NULL),
	(31, 2, 'rectangle-13458-1643000423.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-24 12:00:02', '2022-01-24 14:53:56', 0, 0, NULL, 1, 2, 'rgba(199, 21, 133, 1)', 1, 'TAV 1', NULL),
	(32, 2, 'rectangle-13458-1643004612.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-24 13:10:14', '2022-01-24 13:10:14', 0, 0, NULL, 0, 2, 'rgba(255, 69, 0, 0.68)', 1, 'C7', NULL),
	(33, 2, 'rectangle-13487-1-1644462655.jpg', 0, 0, NULL, 0, 0, 1, '2022-01-24 13:14:29', '2022-04-01 16:37:34', 0, 0, NULL, 1, 1, 'rgba(0, 186, 189, 1)', 0, 'C8', 'blog-img-1648805852.png'),
	(34, 2, NULL, 0, 0, NULL, 0, 0, 1, '2022-01-24 15:41:15', '2022-01-24 15:41:15', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(35, 2, NULL, 0, 0, NULL, 0, 0, 1, '2022-01-24 15:41:33', '2022-01-24 15:41:33', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(36, 2, NULL, 0, 0, NULL, 0, 0, 1, '2022-01-24 15:41:47', '2022-01-24 15:41:47', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(37, -1, 'address-company-img-1643014398.png', 0, 0, NULL, 0, 0, 1, '2022-01-24 15:49:37', '2022-02-08 15:12:35', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(38, -1, NULL, 1, 0, NULL, 0, 0, 1, '2022-01-24 15:49:58', '2022-02-08 15:13:27', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(39, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-01-24 15:50:35', '2022-01-25 09:49:01', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(40, 2, NULL, 0, 0, NULL, 0, 0, 1, '2022-01-24 15:52:14', '2022-01-24 15:52:14', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(41, -1, 'address-company-img-1643014787.png', 0, 0, NULL, 0, 0, 1, '2022-01-24 15:52:31', '2022-02-08 15:13:22', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(42, -1, 'address-company-img-1643014417.png', 1, 0, NULL, 0, 0, 1, '2022-01-24 15:52:51', '2022-02-08 15:13:18', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(43, 2, NULL, 0, 0, NULL, 0, 0, 1, '2022-01-25 08:41:30', '2022-01-25 08:41:30', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(44, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-01-25 08:41:44', '2022-01-25 09:48:43', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(45, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-01-27 11:44:58', '2022-02-08 15:13:13', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(46, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-01-27 11:45:35', '2022-02-08 15:13:09', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(47, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-01-27 11:45:57', '2022-02-08 15:13:05', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(48, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-01-27 11:47:15', '2022-02-08 15:13:00', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(49, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-01-27 11:49:33', '2022-02-08 15:12:55', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(50, -1, NULL, 1, 0, NULL, 0, 0, 1, '2022-01-27 11:56:33', '2022-02-08 15:12:45', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(51, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-02-08 09:42:11', '2022-02-08 15:12:41', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(52, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-02-09 10:21:11', '2022-02-09 17:45:22', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(53, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-02-09 10:59:56', '2022-02-09 17:45:18', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(54, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-02-09 11:00:36', '2022-02-09 17:45:12', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(55, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-02-09 11:00:59', '2022-02-09 17:45:08', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(56, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-02-09 11:01:33', '2022-02-09 17:45:01', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(57, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-02-09 11:01:53', '2022-02-09 17:44:53', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(58, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-02-09 11:02:42', '2022-02-09 17:44:49', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(59, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-02-09 11:02:58', '2022-02-09 17:42:40', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(60, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-02-09 11:09:47', '2022-02-09 17:42:34', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(61, 2, 'rectangle-13448-1644401418.jpg', 0, 0, NULL, 0, 0, 1, '2022-02-09 17:10:23', '2022-04-01 16:37:13', 0, 0, NULL, 0, 2, 'rgba(255, 120, 0, 1)', 0, NULL, 'img-value-1648805830.png'),
	(62, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-02-09 17:24:13', '2022-02-09 17:42:29', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(63, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-02-10 10:21:19', '2022-02-10 15:21:04', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(64, -1, 'rectangle-13448-1644463996.jpg', 0, 0, NULL, 0, 0, 1, '2022-02-10 10:33:24', '2022-02-10 15:21:08', 0, 0, NULL, 0, 2, 'rgba(0, 206, 209, 1)', 1, NULL, NULL),
	(65, -1, NULL, 1, 0, NULL, 0, 0, 1, '2022-02-10 17:16:55', '2022-02-10 17:21:05', 10, 0, NULL, 0, 2, NULL, 0, NULL, NULL),
	(66, -1, NULL, 0, 0, NULL, 0, 0, 1, '2022-02-10 17:22:21', '2022-02-11 13:52:21', 0, 0, NULL, 0, 2, NULL, 1, NULL, NULL),
	(67, 1, 'blog-img-1648809848.png', 0, 0, NULL, 0, 0, 1, '2022-04-01 16:22:25', '2022-04-01 17:44:15', 0, 0, NULL, 0, 2, NULL, 1, NULL, 'collection-img-1648809851.png');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table centurystone.product_attribute
CREATE TABLE IF NOT EXISTS `product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`product_id`,`attribute_id`,`language_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table centurystone.product_attribute: 0 rows
/*!40000 ALTER TABLE `product_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_attribute` ENABLE KEYS */;

-- Dumping structure for table centurystone.product_category
CREATE TABLE IF NOT EXISTS `product_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`) USING BTREE,
  KEY `category_id` (`category_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- Dumping data for table centurystone.product_category: 138 rows
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
INSERT INTO `product_category` (`product_id`, `category_id`) VALUES
	(1, 2),
	(1, 62),
	(14, 2),
	(14, 62),
	(15, 62),
	(16, 62),
	(17, 62),
	(18, 62),
	(19, 62),
	(20, 62),
	(21, 62),
	(22, 62),
	(23, 62),
	(23, 65),
	(24, 3),
	(24, 62),
	(25, 2),
	(26, 2),
	(26, 3),
	(26, 62),
	(26, 65),
	(26, 66),
	(27, 2),
	(28, 2),
	(29, 2),
	(30, 2),
	(30, 62),
	(31, 2),
	(31, 62),
	(31, 65),
	(32, 2),
	(32, 62),
	(33, 2),
	(33, 62),
	(33, 65),
	(35, 2),
	(37, 2),
	(38, 2),
	(39, 2),
	(40, 2),
	(41, 2),
	(42, 65),
	(45, 2),
	(46, 2),
	(48, 2),
	(49, 2),
	(53, 2),
	(54, 2),
	(55, 2),
	(56, 2),
	(57, 2),
	(58, 2),
	(59, 2),
	(61, 62),
	(64, 66),
	(7176, 1),
	(7176, 2),
	(7177, 2),
	(7178, 3),
	(7179, 1),
	(7179, 2),
	(7180, 1),
	(7181, 2),
	(7182, 1),
	(7183, 1),
	(7183, 2),
	(7184, 1),
	(7185, 1),
	(7186, 3),
	(7187, 2),
	(7188, 1),
	(7189, 2),
	(7190, 1),
	(7191, 1),
	(7191, 2),
	(7203, 3),
	(7270, 1),
	(7271, 1),
	(7271, 2),
	(7273, 1),
	(7273, 2),
	(7274, 1),
	(7274, 2),
	(7275, 1),
	(7275, 2),
	(7276, 3),
	(7277, 3),
	(7278, 3),
	(7279, 2),
	(7280, 1),
	(7281, 3),
	(7282, 3),
	(7283, 1),
	(7283, 2),
	(7284, 1),
	(7284, 2),
	(7285, 1),
	(7285, 2),
	(7285, 61),
	(7286, 1),
	(7287, 3),
	(7288, 3),
	(7289, 1),
	(7294, 3),
	(7296, 61),
	(7297, 1),
	(7297, 61),
	(7298, 61),
	(7299, 61),
	(7300, 3),
	(7301, 2),
	(7302, 2),
	(7304, 2),
	(7305, 1),
	(7319, 1),
	(7320, 2),
	(7321, 2),
	(7322, 2),
	(7323, 2),
	(7324, 1),
	(7325, 3),
	(7326, 1),
	(7327, 1),
	(7329, 1),
	(7329, 2),
	(7330, 1),
	(7330, 2),
	(7331, 2),
	(7346, 1),
	(7346, 2),
	(7346, 61),
	(7348, 1),
	(7348, 9),
	(7348, 10),
	(7348, 19),
	(7348, 23),
	(7356, 1),
	(7356, 2);
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;

-- Dumping structure for table centurystone.product_description
CREATE TABLE IF NOT EXISTS `product_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mô tả ngắn',
  `description` text COLLATE utf8mb4_unicode_ci COMMENT 'Nội dung chi tiết',
  `product_description` text COLLATE utf8mb4_unicode_ci COMMENT 'Mô tả sản phẩm',
  `tag` text COLLATE utf8mb4_unicode_ci,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=202 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table centurystone.product_description: 201 rows
/*!40000 ALTER TABLE `product_description` DISABLE KEYS */;
INSERT INTO `product_description` (`id`, `product_id`, `language_id`, `name`, `slug`, `short_description`, `description`, `product_description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`, `policy`) VALUES
	(1, 1, 1, 'CSC22001 - Stary Sky', 'starry-sky', NULL, '<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>', '<p>Lấy cảm hứng từ bầu trời đ&ecirc;m với mu&ocirc;n ng&agrave;n v&igrave; sao, Starry Sky l&agrave; sự kết hợp của những mảnh k&iacute;nh v&agrave; gương tr&ecirc;n nền đen tuyền. Sự kết hợp của n&oacute; tạo n&ecirc;n một nguồn tuyệt vời cho tr&iacute; tưởng tượng của người xem.</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh 2 năm</p>'),
	(2, 1, 2, 'Starry Sky', 'starry-sky', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(3, 1, 3, 'Starry Sky', 'starry-sky', NULL, '<div class="row">\n<div class="col-md-6">\n<h4>M&ocirc; tả Sản phẩm</h4>\n<div class="list ml-md-3">\n<h5>K&iacute;ch thước Slap</h5>\n<ul class="disc mb-0">\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<h5>Độ d&agrave;y</h5>\n<ul class="disc mb-0">\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n</div>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="666" height="438" /></p>\n</div>\n<div class="col-md-6">\n<h4>M&ocirc; tả Sản phẩm</h4>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<h4 class="text-uppercase">CH&Iacute;NH S&Aacute;CH B&Aacute;N H&Agrave;NG V&Agrave; CHẤT LƯỢNG H&Agrave;NG HO&Aacute;</h4>\n<p>L&agrave; một trong những doanh nghiệp hoạt động l&acirc;u năm tr&ecirc;n lĩnh vực Kinh doanh Đ&aacute; thạch anh tại Việt Nam, C&Ocirc;NG TY CỔ PHẦN THƯƠNG MẠI CENTURY STONE xin cam kết ch&iacute;nh s&aacute;ch b&aacute;n h&agrave;ng v&agrave; ch&iacute;nh s&aacute;ch chất lượng với sản phẩm được cung cấp như sau:</p>\n<ul class="disc">Tất cả h&agrave;ng ho&aacute; đều c&oacute; nguồn gốc xuất xứ r&otilde; r&agrave;ng, minh bạch, ch&iacute;nh h&atilde;ng từ c&aacute;c nh&agrave; sản xuất v&agrave; c&aacute;c nh&agrave; nhập khẩu. Tuyệt đối kh&ocirc;ng b&aacute;n h&agrave;ng giả, h&agrave;ng nh&aacute;i thương hiệu, h&agrave;ng k&eacute;m chất lượng. Mọi sản phẩm c&oacute; gi&aacute; ni&ecirc;m yết đều đ&atilde; bao gồm thuế gi&aacute; trị gia tăng (VAT). Hoạt động n&agrave;y nhằm bảo vệ quyền lợi kh&aacute;ch h&agrave;ng v&agrave; ho&agrave;n th&agrave;nh tr&aacute;ch nhiệm đ&oacute;ng thuế theo ph&aacute;p luật Việt Nam. Ch&uacute;ng t&ocirc;i khuyến kh&iacute;ch mọi tổ chức, c&aacute; nh&acirc;n y&ecirc;u cầu ho&aacute; đơn VAT khi mua h&agrave;ng. Qu&yacute; kh&aacute;ch muốn viết h&oacute;a đơn t&agrave;i ch&iacute;nh, vui l&ograve;ng cung cấp th&ocirc;ng tin ngay để bộ phận Kế to&aacute;n xuất h&oacute;a đơn kịp thời. H&oacute;a đơn VAT chỉ xuất trong ng&agrave;y theo quy định của Tổng Cục Thuế v&agrave; Bộ T&agrave;i Ch&iacute;nh. Trường hợp kh&aacute;ch h&agrave;ng từ chối mua h&agrave;ng nhưng h&oacute;a đơn đ&atilde; xuất; Qu&yacute; kh&aacute;ch vui l&ograve;ng k&yacute; v&agrave; đ&oacute;ng dấu v&agrave;o Bi&ecirc;n bản Hủy h&oacute;a đơn. Ho&agrave;n tiền 100% gi&aacute; trị h&agrave;ng ho&aacute; b&aacute;n ra nếu h&agrave;ng ho&aacute; b&aacute;n ra kh&ocirc;ng đ&uacute;ng cam kết, sai nguồn gốc (kh&ocirc;ng bao gồm chi ph&iacute; vận chuyển, chuyển ho&agrave;n).</ul>\n<p>&nbsp;</p>'),
	(4, 2, 1, 'csh 22001-starry sky', 'csh-22001-starry-sky', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(5, 2, 2, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(6, 2, 3, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(7, 3, 1, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(8, 3, 2, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(9, 3, 3, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(10, 4, 1, 'CSA 52001 - White Pearl', 'csa-52001-white-pearl', NULL, '<div class="row">\n<div class="col-md-12">\n<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p>Với bề mặt m&agrave;u trắng ngọc, xen kẽ l&agrave; những đường v&acirc;n đen x&aacute;m, White Pearl mang lại một vẻ đẹp nhẹ nh&agrave;ng, tao nh&atilde; nhưng cũng kh&ocirc;ng k&eacute;m phần sang trọng, rất th&iacute;ch hợp trong bối cảnh ph&ograve;ng kh&aacute;ch v&agrave; ph&ograve;ng bếp.</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh 2 năm</p>'),
	(11, 4, 2, 'CSA 52001 - White Pearl', 'csa-52001-white-pearl', 'Inspired by the flowing lava flowing like a stream...', '<div class="row">\n<div class="col-md-6">\n<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(12, 4, 3, 'CSA 52001 - 白珍珠', 'csa-52001-bai-zhen-zhu', '灵感来自像溪流一样流动的熔岩......', '<div class="row">\n<div class="col-md-6">\n<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(13, 5, 1, 'CSA62001 - Iridescent Cloud', 'csa62001-iridescent-cloud', NULL, '<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>', '<p>M&acirc;y trời vẫn lu&ocirc;n l&agrave; nguồn cảm hứng s&aacute;ng tạo v&ocirc; tận cho nghệ thuật, l&uacute;c trong trẻo nhẹ nh&agrave;ng như những đ&aacute;m m&acirc;y trắng, khi th&igrave; mạnh mẽ dữ dội trong cơn b&atilde;o d&ocirc;ng, hay b&iacute; ẩn cuốn h&uacute;t tr&ecirc;n bầu trời đ&ecirc;m. Tất cả những h&igrave;nh tượng đ&oacute; đ&atilde; được phối hợp một c&aacute;ch h&agrave;i h&ograve;a, tạo n&ecirc;n một bức tranh đồng nhất tr&ecirc;n sản phẩm Iridescent Cloud của Century Stone.&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh 2 năm.&nbsp;</p>'),
	(14, 5, 2, 'CSA62001 - White Cloud', 'csa62001-white-cloud', 'Inspired by the flowing lava flowing like a stream...', '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(15, 5, 3, 'CSA62001 - 白云', 'csa62001-bai-yun', '灵感来自像溪流一样流动的熔岩......', '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(16, 6, 1, 'QWE 22001-Starry Sky', 'acb-22001-starry-sky', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>Th&ocirc;ng số kỹ thuật</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong></p>\n<ul>\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<p><strong>Độ d&agrave;y</strong></p>\n<ul>\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n<p><strong>Bề mặt sản phẩm</strong></p>\n<ul>\n<li>Đ&aacute;nh b&oacute;ng</li>\n</ul>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm&nbsp;</strong></p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(17, 6, 2, 'QWE 22001-Starry Sky', 'acb-22001-starry-sky', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<div class="row">\n<div class="col-md-6">\n<p>Slap Size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>Thickness<br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p>Product surface<br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></strong></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(18, 6, 3, 'QWE 22001-星空', 'acb-22001-xing-kong', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<div class="row">\n<div class="col-md-6">\n<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>厚度<br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p>产品表面<br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(19, 7, 1, 'CSA62002 - Sparkling Terrazzo', 'csa-62002-sparkling-terrazzo', NULL, '<p><strong>Th&ocirc;ng số kỹ thuật</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong></p>\n<ul>\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<p><strong>Độ d&agrave;y</strong></p>\n<ul>\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n<p><strong>Bề mặt sản phẩm</strong></p>\n<ul>\n<li>Đ&aacute;nh b&oacute;ng</li>\n</ul>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<p>Lấy cảm hứng từ vẻ đẹp cổ điển của phong c&aacute;ch Terrazzo, kết hợp với những mảnh/cục s&aacute;ng v&agrave; tối tạo n&ecirc;n sự tương phản h&agrave;i h&ograve;a trong sản phẩm Sparkling Terrazzo của ch&uacute;ng t&ocirc;i.&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh 2 năm</p>'),
	(199, 67, 1, 'test', 'test', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(200, 67, 2, 'ttt', 'ttt', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(201, 67, 3, 'ttt', 'ttt', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(20, 7, 2, 'ACB Starry', 'acb-22001-starry-sky', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<div class="row">\n<div class="col-md-6">\n<p>Slap Size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>Thickness<br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p>Product surface<br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></strong></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(21, 7, 3, 'ACB 星空', 'acb-22001-xing-kong', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<div class="row">\n<div class="col-md-6">\n<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>厚度<br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p>产品表面<br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(22, 8, 1, 'ASD 2200 Sky', 'acb-22001-starry-sky', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>Th&ocirc;ng số kỹ thuật</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong></p>\n<ul>\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<p><strong>Độ d&agrave;y</strong></p>\n<ul>\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n<p><strong>Bề mặt sản phẩm</strong></p>\n<ul>\n<li>Đ&aacute;nh b&oacute;ng</li>\n</ul>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm&nbsp;</strong></p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(23, 8, 2, 'ASD 220 Sky', 'acb-22001-starry-sky', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<div class="row">\n<div class="col-md-6">\n<p>Slap Size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>Thickness<br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p>Product surface<br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></strong></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(24, 8, 3, 'ASD 22 星空', 'acb-22001-xing-kong', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<div class="row">\n<div class="col-md-6">\n<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>厚度<br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p>产品表面<br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(25, 9, 1, 'Pearl', '123-white-pearl', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>Th&ocirc;ng số kỹ thuật</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong></p>\n<ul>\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<p><strong>Độ d&agrave;y</strong></p>\n<ul>\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n<p><strong>Bề mặt sản phẩm</strong></p>\n<ul>\n<li>Đ&aacute;nh b&oacute;ng</li>\n</ul>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm&nbsp;</strong></p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(26, 9, 2, 'Pearl', '123-white-pearl', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<div class="row">\n<div class="col-md-6">\n<p>Slap Size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>Thickness<br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p>Product surface<br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></strong></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(27, 9, 3, '白珍珠', '123-bai-zhen-zhu', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<div class="row">\n<div class="col-md-6">\n<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>厚度<br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p>产品表面<br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(28, 10, 1, '123 - White Pearl', '123-white-pearl', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>Th&ocirc;ng số kỹ thuật</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong></p>\n<ul>\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<p><strong>Độ d&agrave;y</strong></p>\n<ul>\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n<p><strong>Bề mặt sản phẩm</strong></p>\n<ul>\n<li>Đ&aacute;nh b&oacute;ng</li>\n</ul>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm&nbsp;</strong></p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(29, 10, 2, '123 - White Pearl', '123-white-pearl', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<div class="row">\n<div class="col-md-6">\n<p>Slap Size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>Thickness<br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p>Product surface<br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></strong></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(30, 10, 3, '123 - 白珍珠', '123-bai-zhen-zhu', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<div class="row">\n<div class="col-md-6">\n<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>厚度<br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p>产品表面<br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(31, 11, 1, 'PIS12 - Black Crystal', '123-white-pearl', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>Th&ocirc;ng số kỹ thuật</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong></p>\n<ul>\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<p><strong>Độ d&agrave;y</strong></p>\n<ul>\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n<p><strong>Bề mặt sản phẩm</strong></p>\n<ul>\n<li>Đ&aacute;nh b&oacute;ng</li>\n</ul>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm&nbsp;</strong></p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(32, 11, 2, 'PIS12 - Black Crystal', '123-white-pearl', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<div class="row">\n<div class="col-md-6">\n<p>Slap Size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>Thickness<br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p>Product surface<br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></strong></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(33, 11, 3, 'PIS12 - 白珍珠', '123-bai-zhen-zhu', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<div class="row">\n<div class="col-md-6">\n<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>厚度<br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p>产品表面<br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(34, 12, 1, '896 - White Pearl', '123-white-pearl', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>Th&ocirc;ng số kỹ thuật</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong></p>\n<ul>\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<p><strong>Độ d&agrave;y</strong></p>\n<ul>\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n<p><strong>Bề mặt sản phẩm</strong></p>\n<ul>\n<li>Đ&aacute;nh b&oacute;ng</li>\n</ul>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm&nbsp;</strong></p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(35, 12, 2, '896 - White Pearl', '123-white-pearl', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<div class="row">\n<div class="col-md-6">\n<p>Slap Size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>Thickness<br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p>Product surface<br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></strong></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(36, 12, 3, '896 - 白珍珠', '123-bai-zhen-zhu', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<div class="row">\n<div class="col-md-6">\n<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>厚度<br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p>产品表面<br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(37, 13, 1, 'UI12 White', '123-white-pearl', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>Th&ocirc;ng số kỹ thuật</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong></p>\n<ul>\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<p><strong>Độ d&agrave;y</strong></p>\n<ul>\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n<p><strong>Bề mặt sản phẩm</strong></p>\n<ul>\n<li>Đ&aacute;nh b&oacute;ng</li>\n</ul>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm&nbsp;</strong></p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(38, 13, 2, 'UI12 White', '123-white-pearl', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<div class="row">\n<div class="col-md-6">\n<p>Slap Size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>Thickness<br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p>Product surface<br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></strong></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(39, 13, 3, 'UI12 - 白珍珠', '123-bai-zhen-zhu', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<div class="row">\n<div class="col-md-6">\n<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>厚度<br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p>产品表面<br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(40, 14, 1, 'PO121 White', '123-white-pearl', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>Th&ocirc;ng số kỹ thuật</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong></p>\n<ul>\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<p><strong>Độ d&agrave;y</strong></p>\n<ul>\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n<p><strong>Bề mặt sản phẩm</strong></p>\n<ul>\n<li>Đ&aacute;nh b&oacute;ng</li>\n</ul>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm&nbsp;</strong></p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(41, 14, 2, 'PO121 White', '123-white-pearl', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<div class="row">\n<div class="col-md-6">\n<p>Slap Size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>Thickness<br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p>Product surface<br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></strong></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(42, 14, 3, 'PO121 - 白珍珠', '123-bai-zhen-zhu', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<div class="row">\n<div class="col-md-6">\n<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>厚度<br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p>产品表面<br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(43, 15, 1, 'QD12 - White Pearl', '123-white-pearl', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>Th&ocirc;ng số kỹ thuật</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong></p>\n<ul>\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<p><strong>Độ d&agrave;y</strong></p>\n<ul>\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n<p><strong>Bề mặt sản phẩm</strong></p>\n<ul>\n<li>Đ&aacute;nh b&oacute;ng</li>\n</ul>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm&nbsp;</strong></p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(44, 15, 2, 'QD12 - White Pearl', '123-white-pearl', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<div class="row">\n<div class="col-md-6">\n<p>Slap Size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>Thickness<br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p>Product surface<br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></strong></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(45, 15, 3, 'QD12 - 白珍珠', '123-bai-zhen-zhu', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<div class="row">\n<div class="col-md-6">\n<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>厚度<br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p>产品表面<br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(46, 16, 1, '785 - Black Crystal', '123-white-pearl', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>Th&ocirc;ng số kỹ thuật</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong></p>\n<ul>\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<p><strong>Độ d&agrave;y</strong></p>\n<ul>\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n<p><strong>Bề mặt sản phẩm</strong></p>\n<ul>\n<li>Đ&aacute;nh b&oacute;ng</li>\n</ul>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm&nbsp;</strong></p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(47, 16, 2, '785 - White Pearl', '123-white-pearl', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<div class="row">\n<div class="col-md-6">\n<p>Slap Size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>Thickness<br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p>Product surface<br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></strong></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(48, 16, 3, '785 - 白珍珠', '123-bai-zhen-zhu', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<div class="row">\n<div class="col-md-6">\n<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>厚度<br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p>产品表面<br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(49, 17, 1, 'HND12 - White Pearl', '123-white-pearl', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>Th&ocirc;ng số kỹ thuật</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong></p>\n<ul>\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<p><strong>Độ d&agrave;y</strong></p>\n<ul>\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n<p><strong>Bề mặt sản phẩm</strong></p>\n<ul>\n<li>Đ&aacute;nh b&oacute;ng</li>\n</ul>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm&nbsp;</strong></p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<div>\n<div>\n<div><strong>CH&Iacute;NH S&Aacute;CH B&Aacute;N H&Agrave;NG V&Agrave; CHẤT LƯỢNG H&Agrave;NG H&Oacute;A&nbsp;</strong></div>\n<div>L&agrave; một trong những doanh nghiệp hoạt động l&acirc;u năm tr&ecirc;n lĩnh vực Kinh doanh đ&aacute; thạch anh</div>\n<ul>\n<li>Tất cả h&agrave;ng h&oacute;a c&oacute; nguồn gốc r&otilde; r&agrave;ng&nbsp;</li>\n<li>Gi&aacute; ni&ecirc;m yết đ&atilde; bao gồm VAT</li>\n</ul>\n</div>\n</div>'),
	(50, 17, 2, 'HND12 - White Pearl', '123-white-pearl', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<div class="row">\n<div class="col-md-6">\n<p>Slap Size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>Thickness<br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p>Product surface<br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></strong></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(51, 17, 3, 'HND12 - 白珍珠', '123-bai-zhen-zhu', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<div class="row">\n<div class="col-md-6">\n<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>厚度<br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p>产品表面<br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(52, 18, 1, 'DHJ32 - White Pearl', '123-white-pearl', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>Th&ocirc;ng số kỹ thuật</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong></p>\n<ul>\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<p><strong>Độ d&agrave;y</strong></p>\n<ul>\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n<p><strong>Bề mặt sản phẩm</strong></p>\n<ul>\n<li>Đ&aacute;nh b&oacute;ng</li>\n</ul>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm&nbsp;</strong></p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(53, 18, 2, 'DHJ32 - White Pearl', '123-white-pearl', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<div class="row">\n<div class="col-md-6">\n<p>Slap Size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>Thickness<br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p>Product surface<br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></strong></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(54, 18, 3, 'DHJ32 - 白珍珠', '123-bai-zhen-zhu', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<div class="row">\n<div class="col-md-6">\n<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>厚度<br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p>产品表面<br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(55, 19, 1, 'AJA12 - Black Crystal', '123-white-pearl', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>Th&ocirc;ng số kỹ thuật</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong></p>\n<ul>\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<p><strong>Độ d&agrave;y</strong></p>\n<ul>\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n<p><strong>Bề mặt sản phẩm</strong></p>\n<ul>\n<li>Đ&aacute;nh b&oacute;ng</li>\n</ul>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm&nbsp;</strong></p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(56, 19, 2, 'AJA12 - Black Crystal', '123-white-pearl', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<div class="row">\n<div class="col-md-6">\n<p>Slap Size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>Thickness<br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p>Product surface<br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></strong></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(57, 19, 3, 'AJA12 - 白珍珠', '123-bai-zhen-zhu', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<div class="row">\n<div class="col-md-6">\n<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>厚度<br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p>产品表面<br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(58, 20, 1, 'USH12 - White Cloud', '123-white-pearl', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>Th&ocirc;ng số kỹ thuật</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong></p>\n<ul>\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<p><strong>Độ d&agrave;y</strong></p>\n<ul>\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n<p><strong>Bề mặt sản phẩm</strong></p>\n<ul>\n<li>Đ&aacute;nh b&oacute;ng</li>\n</ul>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm&nbsp;</strong></p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(59, 20, 2, 'USH12 - White Cloud', '123-white-pearl', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<div class="row">\n<div class="col-md-6">\n<p>Slap Size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>Thickness<br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p>Product surface<br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></strong></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(60, 20, 3, 'USH12 - 白珍珠', '123-bai-zhen-zhu', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<div class="row">\n<div class="col-md-6">\n<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>厚度<br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p>产品表面<br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(61, 21, 1, 'ELK15 Crystal', '123-white-pearl', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>Th&ocirc;ng số kỹ thuật</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong></p>\n<ul>\n<li>K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</li>\n<li>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</li>\n</ul>\n<p><strong>Độ d&agrave;y</strong></p>\n<ul>\n<li>15mm (3/5 &rdquo;)</li>\n<li>20mm (3/4 &rdquo;)</li>\n<li>30mm (1 1/6 &rdquo;)</li>\n</ul>\n<p><strong>Bề mặt sản phẩm</strong></p>\n<ul>\n<li>Đ&aacute;nh b&oacute;ng</li>\n</ul>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm&nbsp;</strong></p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(62, 21, 2, 'ELK15 Crystal', '123-white-pearl', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<div class="row">\n<div class="col-md-6">\n<p>Slap Size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>Thickness<br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p>Product surface<br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></strong></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(63, 21, 3, 'ELK15 - 白珍珠', '123-bai-zhen-zhu', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<div class="row">\n<div class="col-md-6">\n<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>厚度<br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p>产品表面<br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(64, 22, 1, 'NDH12 - White Cloud', 'ndh12-white-cloud', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<p></p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm&nbsp;</strong></p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(65, 22, 2, 'NDH12 - White Cloud', 'ndh12-white-cloud', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of shards of glass and mirrors against a jet-black background. Its combination creates a great source of viewer\'s imagination.', '<p></p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n<div class="col-md-6">\n<p>Product Description</p>\n<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(66, 22, 3, 'NDH12 - 白云', 'ndh12-bai-yun', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。它的组合为观众的想象力提供了很好的来源。', '<p></p>', '<div class="row">\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="635" height="418" /></p>\n</div>\n<div class="col-md-6">\n<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(67, 23, 1, 'CSA1 - White Cloud', 'csa1-white-cloud', 'Lấy cảm hứng từ dòng nham thạch đang cuồn cuộn tuôn trào như dòng ...', '<div class="row">\n<div class="col-md-6">\n<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(68, 23, 2, 'CSA1 - White Cloud', 'csa1-white-cloud', 'Inspired by the flowing lava flowing like a stream...', '<div class="row">\n<div class="col-md-6">\n<p><strong>Slap Size</strong><br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p><strong>Thickness</strong><br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p><strong>Product surface</strong><br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\nLake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(69, 23, 3, 'CSA1 - 白云', 'csa1-bai-yun', '灵感来自像溪流一样流动的熔岩......', '<div class="row">\n<div class="col-md-6">\n<p><strong>拍打尺寸</strong><br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p><strong>厚度</strong><br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p><strong>产品表面</strong><br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能看清，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(70, 24, 1, 'CSA2 - White Cloud', 'csa2-white-cloud', 'Lấy cảm hứng từ dòng nham thạch đang cuồn cuộn tuôn trào như dòng ...', '<div class="row">\n<div class="col-md-6">\n<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(71, 24, 2, 'CSA2 - White Cloud', 'csa2-white-cloud', 'Inspired by the flowing lava flowing like a stream...', '<div class="row">\n<div class="col-md-6">\n<p><strong>Slap Size</strong><br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p><strong>Thickness</strong><br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p><strong>Product surface</strong><br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\nLake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(72, 24, 3, 'CSA2 - 白云', 'csa2-bai-yun', '灵感来自像溪流一样流动的熔岩......', '<div class="row">\n<div class="col-md-6">\n<p><strong>拍打尺寸</strong><br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p><strong>厚度</strong><br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p><strong>产品表面</strong><br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能看清，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(73, 25, 1, 'CSC3 - Thạch anh Natural', 'csc3-thach-anh-natural', 'Lấy cảm hứng từ dòng nham thạch đang cuồn cuộn tuôn trào như dòng ...', '<div class="row">\n<div class="col-md-6">\n<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(74, 25, 2, 'CSC3 - Natural Quartz', 'csc3-natural-quartz', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(75, 25, 3, 'CSC3 - 天然石英', 'csc3-tian-ran-shi-ying', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(76, 26, 1, 'CSC5 - Thạch anh Natural', 'csc3-thach-anh-natural', 'Lấy cảm hứng từ dòng nham thạch đang cuồn cuộn tuôn trào như dòng ...', '<div class="row">\n<div class="col-md-6">\n<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(77, 26, 2, 'CSC5 - Natural Quartz', 'csc3-natural-quartz', 'Inspired by the flowing lava flowing like a stream...', '<div class="row">\n<div class="col-md-6">\n<p><strong>Slap Size</strong><br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p><strong>Thickness</strong><br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p><strong>Product surface</strong><br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\nLake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(78, 26, 3, 'CSC5 - 天然石英', 'csc3-tian-ran-shi-ying', '灵感来自像溪流一样流动的熔岩......', '<div class="row">\n<div class="col-md-6">\n<p><strong>拍打尺寸</strong><br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p><strong>厚度</strong><br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p><strong>产品表面</strong><br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能看清，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(79, 27, 1, 'CSC3 - Thạch anh Natural', 'csc3-thach-anh-natural', 'Lấy cảm hứng từ dòng nham thạch đang cuồn cuộn tuôn trào như dòng ...', '<div class="row">\n<div class="col-md-6">\n<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(80, 27, 2, 'CSC3 - Natural Quartz', 'csc3-natural-quartz', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(81, 27, 3, 'CSC3 - 天然石英', 'csc3-tian-ran-shi-ying', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(82, 28, 1, 'CSC3 - Thạch anh Natural', 'csc3-thach-anh-natural', 'Lấy cảm hứng từ dòng nham thạch đang cuồn cuộn tuôn trào như dòng ...', '<div class="row">\n<div class="col-md-6">\n<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(83, 28, 2, 'CSC3 - Natural Quartz', 'csc3-natural-quartz', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(84, 28, 3, 'CSC3 - 天然石英', 'csc3-tian-ran-shi-ying', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(85, 29, 1, 'CSC3 - Thạch anh Natural', 'csc3-thach-anh-natural', 'Lấy cảm hứng từ dòng nham thạch đang cuồn cuộn tuôn trào như dòng ...', '<div class="row">\n<div class="col-md-6">\n<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(86, 29, 2, 'CSC3 - Natural Quartz', 'csc3-natural-quartz', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(87, 29, 3, 'CSC3 - 天然石英', 'csc3-tian-ran-shi-ying', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(88, 30, 1, 'CSC3 - Thạch anh Natural', 'csc3-thach-anh-natural', 'Lấy cảm hứng từ dòng nham thạch đang cuồn cuộn tuôn trào như dòng ...', '<div class="row">\n<div class="col-md-6">\n<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(89, 30, 2, 'CSC3 - Natural Quartz', 'csc3-natural-quartz', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(90, 30, 3, 'CSC3 - 天然石英', 'csc3-tian-ran-shi-ying', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(91, 31, 1, 'TAV 1 White Pearl', 'tav-1-white-pearl', 'Lấy cảm hứng từ dòng nham thạch đang cuồn cuộn tuôn trào như dòng ...', '<div class="row">\n<div class="col-md-6">\n<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(92, 31, 2, 'TAV 1 White Pearl', 'tav-1-white-pearl', 'Inspired by the flowing lava flowing like a stream...', '<div class="row">\n<div class="col-md-6">\n<p><strong>Slap Size</strong><br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p><strong>Thickness</strong><br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p><strong>Product surface</strong><br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\nLake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(93, 31, 3, 'TAV 1 白珍珠', 'tav-1-bai-zhen-zhu', '灵感来自像溪流一样流动的熔岩......', '<div class="row">\n<div class="col-md-6">\n<p><strong>拍打尺寸</strong><br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p><strong>厚度</strong><br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p><strong>产品表面</strong><br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能看清，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(94, 32, 1, 'C7 - White Cloud', 'c7-white-cloud', 'Lấy cảm hứng từ dòng nham thạch đang cuồn cuộn tuôn trào như dòng ...', '<div class="row">\n<div class="col-md-6">\n<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Bảo h&agrave;nh trong v&ograve;ng 2 năm</p>'),
	(95, 32, 2, 'C7 - White Cloud', 'c7-white-cloud', 'Inspired by the flowing lava flowing like a stream...', '<div class="row">\n<div class="col-md-6">\n<p><strong>Slap Size</strong><br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p><strong>Thickness</strong><br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p><strong>Product surface</strong><br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\nLake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Warranty for 2 years</p>'),
	(96, 32, 3, 'C7 - 白云', 'c7-bai-yun', '灵感来自像溪流一样流动的熔岩......', '<div class="row">\n<div class="col-md-6">\n<p><strong>拍打尺寸</strong><br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p><strong>厚度</strong><br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p><strong>产品表面</strong><br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能看清，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>质保2年</p>'),
	(97, 33, 1, 'C8 White Pearl', 'c8-white-pearl', 'Lấy cảm hứng từ dòng nham thạch đang cuồn cuộn tuôn trào như dòng ...', '<div class="row">\n<div class="col-md-6">\n<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p><strong>CH&Iacute;NH S&Aacute;CH B&Aacute;N H&Agrave;NG V&Agrave; CHẤT LƯỢNG H&Agrave;NG HO&Aacute;</strong></p>\n<ul>\n<li>L&agrave; một trong những doanh nghiệp hoạt động l&acirc;u năm tr&ecirc;n lĩnh vực Kinh doanh Đ&aacute; thạch anh tại Việt Nam, C&Ocirc;NG TY CỔ PHẦN THƯƠNG MẠI CENTURY STONE xin cam kết ch&iacute;nh s&aacute;ch b&aacute;n h&agrave;ng v&agrave; ch&iacute;nh s&aacute;ch chất lượng với sản phẩm được cung cấp như sau:</li>\n<li>Tất cả h&agrave;ng ho&aacute; đều c&oacute; nguồn gốc xuất xứ r&otilde; r&agrave;ng, minh bạch, ch&iacute;nh h&atilde;ng từ c&aacute;c nh&agrave; sản xuất v&agrave; c&aacute;c nh&agrave; nhập khẩu.<br />Tuyệt đối kh&ocirc;ng b&aacute;n h&agrave;ng giả, h&agrave;ng nh&aacute;i thương hiệu, h&agrave;ng k&eacute;m chất lượng.</li>\n<li>Mọi sản phẩm c&oacute; gi&aacute; ni&ecirc;m yết đều đ&atilde; bao gồm thuế gi&aacute; trị gia tăng (VAT). Hoạt động n&agrave;y nhằm bảo vệ quyền lợi kh&aacute;ch h&agrave;ng v&agrave; ho&agrave;n th&agrave;nh tr&aacute;ch nhiệm đ&oacute;ng thuế theo ph&aacute;p luật Việt Nam. Ch&uacute;ng t&ocirc;i khuyến kh&iacute;ch mọi tổ chức, c&aacute; nh&acirc;n y&ecirc;u cầu ho&aacute; đơn VAT khi mua h&agrave;ng. Qu&yacute; kh&aacute;ch muốn viết h&oacute;a đơn t&agrave;i ch&iacute;nh, vui l&ograve;ng cung cấp th&ocirc;ng tin ngay để bộ phận Kế to&aacute;n xuất h&oacute;a đơn kịp thời. H&oacute;a đơn VAT chỉ xuất trong ng&agrave;y theo quy định của Tổng Cục Thuế v&agrave; Bộ T&agrave;i Ch&iacute;nh. Trường hợp kh&aacute;ch h&agrave;ng từ chối mua h&agrave;ng nhưng h&oacute;a đơn đ&atilde; xuất; Qu&yacute; kh&aacute;ch vui l&ograve;ng k&yacute; v&agrave; đ&oacute;ng dấu v&agrave;o Bi&ecirc;n bản Hủy h&oacute;a đơn.</li>\n<li>Ho&agrave;n tiền 100% gi&aacute; trị h&agrave;ng ho&aacute; b&aacute;n ra nếu h&agrave;ng ho&aacute; b&aacute;n ra kh&ocirc;ng đ&uacute;ng cam kết, sai nguồn gốc (kh&ocirc;ng bao gồm chi ph&iacute; vận chuyển, chuyển ho&agrave;n).&nbsp;</li>\n</ul>'),
	(98, 33, 2, 'C8 White Pearl', 'c8-white-pearl', 'Inspired by the flowing lava flowing like a stream...', '<div class="row">\n<div class="col-md-6">\n<p><strong>Slap Size</strong><br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p><strong>Thickness</strong><br />15mm (3/5&rdquo;)<br />20mm (3/4&rdquo;)<br />30mm (1 1/6&rdquo;)</p>\n<p><strong>Product surface</strong><br />Polish</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\nLake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>SALE POLICY AND QUALITY OF GOODS</p>\n<p>As one of the long-standing enterprises operating in the field of Quartz Stone Business in Vietnam, CENTURY STONE TRADING JOINT STOCK COMPANY would like to commit to sales policy and quality policy with products provided as follows: :<br />All goods have clear, transparent, genuine origin from manufacturers and importers.<br />Absolutely do not sell fake goods, imitation brand goods, poor quality goods.</p>\n<p>All listed products are inclusive of value added tax (VAT). This activity is to protect the interests of customers and fulfill their tax liability under Vietnamese law. We encourage all organizations and individuals to request VAT invoices when making purchases. If you want to write a financial invoice, please provide the information immediately so that the Accounting department can issue the invoice in time. VAT invoices are only issued within the day according to the regulations of the General Department of Taxation and the Ministry of Finance. In case the customer refuses to buy the goods but the invoice has been issued; Please sign and stamp the Invoice Cancellation Minute.<br />Refund 100% of the value of sold goods if the goods sold are not in accordance with commitments, from the wrong origin (excluding shipping and return costs).</p>'),
	(99, 33, 3, 'C8白珍珠', 'c8bai-zhen-zhu', '灵感来自像溪流一样流动的熔岩......', '<div class="row">\n<div class="col-md-6">\n<p><strong>拍打尺寸</strong><br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p><strong>厚度</strong><br />15 毫米（3/5 英寸）<br />20 毫米（3/4 英寸）<br />30 毫米（1 1/6 英寸）</p>\n<p><strong>产品表面</strong><br />抛光</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能看清，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>销售政策和商品质量</p>\n<p>作为在越南石英石业务领域长期经营的企业之一，CENTURY STONE TRADING JOINT STOCK COMPANY 愿致力于销售政策和质量政策，提供的产品如下：<br />所有商品均来自制造商和进口商的清晰、透明、真实的来源。<br />绝对不卖假货、仿牌货、劣质货。</p>\n<p>所有列出的产品都包含增值税 (VAT)。这项活动是为了保护客户的利益并履行越南法律规定的纳税义务。我们鼓励所有组织和个人在购买时索取增值税发票。如需开具财务发票，请立即提供资料，以便会计部门及时开具发票。根据税务总局和财政部的规定，增值税发票只能在当日内开具。客户拒绝购买商品但已开具发票的；请在发票取消记录上签名并盖章。<br />如果所售商品不符合承诺，来自错误的原产地（不包括运费和退货费用），则退还所售商品价值的 100%。</p>'),
	(100, 34, 1, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(101, 34, 2, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(102, 34, 3, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(103, 35, 1, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(104, 35, 2, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(105, 35, 3, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(106, 36, 1, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(107, 36, 2, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(108, 36, 3, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(109, 37, 1, 'ff', 'ff', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(110, 37, 2, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(111, 37, 3, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(112, 38, 1, 'fwf', 'fwf', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(113, 38, 2, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(114, 38, 3, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(115, 39, 1, 'gdg', 'gdg', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(116, 39, 2, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(117, 39, 3, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(118, 40, 1, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(119, 40, 2, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(120, 40, 3, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(121, 41, 1, '3', '3', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(122, 41, 2, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(123, 41, 3, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(124, 42, 1, '4', '4', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(125, 42, 2, '4', '4', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(126, 42, 3, '5', '4', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(127, 43, 1, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(128, 43, 2, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(129, 43, 3, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(130, 44, 1, 'ttt', 'ttt', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(131, 44, 2, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(132, 44, 3, '', '', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(133, 45, 1, 'sdf', 'sdf', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(134, 45, 2, 'sfsf', 'sfsf', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(135, 45, 3, 'ff', 'ff', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(136, 46, 1, '7', '7', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(137, 46, 2, '7', '7', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(138, 46, 3, '7', '7', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(139, 47, 1, '8', '8', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(140, 47, 2, '8', '8', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(141, 47, 3, '8', '8', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(142, 48, 1, '9', '9', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(143, 48, 2, '9', '9', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(144, 48, 3, '9', '9', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(145, 49, 1, '10', '10', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(146, 49, 2, '10', '10', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(147, 49, 3, '10', '10', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(148, 50, 1, '1', '1', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(149, 50, 2, '1', '1', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(150, 50, 3, '1', '1', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(151, 51, 1, 'aa', 'aa', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(152, 51, 2, 'ccc', 'ccc', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(153, 51, 3, 'ccc', 'ccc', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(154, 52, 1, 'kjkk', 'kjkk', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(155, 52, 2, 'hj', 'hj', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(156, 52, 3, 'jj', 'jj', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(157, 53, 1, 'kukuy', 'kukuy', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(158, 53, 2, 'kukuy', 'kukuy', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(159, 53, 3, 'kukuy', 'kukuy', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(160, 54, 1, 'eyweywey', 'eyweywey', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(161, 54, 2, 'eyweywey', 'eyweywey', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(162, 54, 3, 'eyweywey', 'eyweywey', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(163, 55, 1, 'gưgqwg', 'gugqwg', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(164, 55, 2, 'gưgqwg', 'gugqwg', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(165, 55, 3, 'gưgqwg', 'gugqwg', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(166, 56, 1, 'egegeg', 'egegeg', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(167, 56, 2, 'egegeg', 'egegeg', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(168, 56, 3, 'egegeg', 'egegeg', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(169, 57, 1, 'ẻyey', 'eyey', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(170, 57, 2, 'ẻyey', 'eyey', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(171, 57, 3, 'ẻyey', 'eyey', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(172, 58, 1, 'u6uu56u6', 'u6uu56u6', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(173, 58, 2, 'u6uu56u6', 'u6uu56u6', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(174, 58, 3, 'u6uu56u6', 'u6uu56u6', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(175, 59, 1, '32323', '32323', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(176, 59, 2, '32323', '32323', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(177, 59, 3, '32323', '32323', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(178, 60, 1, 'gdgdg', 'gdgdg', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(179, 60, 2, 'gdg', 'gdg', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(180, 60, 3, 'dgd', 'dgd', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(181, 61, 1, 'CSH0000 - Starry Sky', 'csh0000-starry-sky', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p>K&iacute;ch thước Slap<br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)</p>\n<p>K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="953" height="627" /></p>\n</div>\n</div>\n<p>&nbsp;</p>', '<p>&nbsp;</p>\n<p><span style="font-size: 24px;"><span style="font-size: 14px;">Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry S</span>ky</span></p>', NULL, NULL, NULL, NULL, '<p>Đổi trả h&agrave;ng&nbsp;</p>\n<p>Bảo h&agrave;nh</p>'),
	(182, 61, 2, 'CSH22001 - Starry Sky', 'csh22001-starry-sky', 'Inspired by the night sky with thousands of stars, Starry Sky is a combination of glass and mirror pieces on a jet-black background. Its combination makes a great source for the viewer\'s imagination.', '<p>Slap size<br />Regular size 3280mm x 1650mm (129&rdquo; x 65&rdquo;); 3280mm x 1860mm (129&rdquo; x 73&rdquo;)<br />Jumbo Dimensions 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>', '<p>Lake Maracaibo in Venezuela is known as the "paradise of thunder dances" with approximately 3 million lightning strikes per year, with a strange attraction. Hundreds of thousands of famous photographers around the world gather here to enjoy the exciting feelings of hunting for those impressive moments. The special thing is that the lightning at Lake Maracaibo is so bright that it can be seen clearly from a distance of 400 km, helping sailors navigate in the dark in the middle of the vast sea... Inspired by the phenomenon of nature full That\'s exciting, we\'ve infused the power of bright, energetic lightning into the CSH22001 - Starry Sky kit</p>', NULL, NULL, NULL, NULL, '<p>Return goods</p>\n<p>Insurance</p>'),
	(183, 61, 3, 'CSH22001 - Starry Sky', 'csh22001-starry-sky', 'Starry Sky 的灵感来自拥有数千颗星星的夜空，是在漆黑的背景上结合玻璃和镜子碎片。 它的组合为观众的想象力提供了很好的来源。', '<p>拍打尺寸<br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>', '<p>委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。 全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。 特别的是，马拉开波湖的闪电如此明亮，在400公里外都能清晰看到，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>', NULL, NULL, NULL, NULL, '<p>退货</p>\n<p>保险</p>'),
	(184, 62, 1, 'ggg', 'ggg', 'ggg', '<p>ggg</p>', '<p>gggg</p>', NULL, NULL, NULL, NULL, '<p>Ch&iacute;nh s&aacute;ch bảo h&agrave;nh</p>'),
	(185, 62, 2, 'ggg', 'ggg', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(186, 62, 3, 'ggg', 'ggg', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(187, 63, 1, 'Test', 'test', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>Th&ocirc;ng số kỹ thuật&nbsp;</strong></p>\n<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p><strong>M&ocirc; tả sản phẩm</strong>&nbsp;</p>\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Ch&iacute;nh s&aacute;ch bảo h&agrave;nh</p>'),
	(188, 63, 2, 'Test', 'test', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>K&iacute;ch thước Slap</strong><br />K&iacute;ch thước th&ocirc;ng thường 3280mm x 1650mm (129 &rdquo;x 65&rdquo;); 3280mm x 1860mm (129 &rdquo;x 73&rdquo;)<br />K&iacute;ch thước Jumbo 3550mm x 2050mm (140 &rdquo;x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p>Hồ Maracaibo ở Venezuela được mệnh danh l&agrave; &ldquo;thi&ecirc;n đường của những vũ điệu sấm s&eacute;t&rdquo; với xấp xỉ 3 triệu c&uacute; s&eacute;t mỗi năm, c&oacute; sức cuốn h&uacute;t lạ kỳ. H&agrave;ng trăm ngh&igrave;n c&aacute;c nhiếp ảnh gia nổi tiếng tr&ecirc;n thế giới tụ hội về đ&acirc;y để tận hưởng những cảm gi&aacute;c th&uacute; vị khi săn l&ugrave;ng những khoảnh khắc ấn tượng ấy. Điều đặc biệt l&agrave; c&aacute;c tia s&eacute;t ở hồ Maracaibo rất s&aacute;ng, tới mức c&oacute; thể nh&igrave;n r&otilde; từ khoảng c&aacute;ch 400 km gi&uacute;p c&aacute;c thủy thủ định hướng trong đ&ecirc;m tối giữa biển lớn m&ecirc;nh m&ocirc;ng... Lấy cảm hứng từ hiện tượng thi&ecirc;n nhi&ecirc;n đầy th&uacute; vị đ&oacute;, ch&uacute;ng t&ocirc;i đ&atilde; truyền tải sức mạnh của những tia s&eacute;t rực s&aacute;ng tr&agrave;n đầy năng lượng v&agrave;o bộ sản phẩm CSH22001 - Starry Sky</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(189, 63, 3, 'Test', 'test', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Starry Sky là sự kết hợp của những mảnh kính và gương trên nền đen tuyền. Sự kết hợp của nó tạo nên một nguồn tuyệt vời cho trí tưởng tượng của người xem.', '<div class="row">\n<div class="col-md-6">\n<p><strong>规格</strong></p>\n<p><strong>拍打尺寸</strong><br />常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p><strong>Độ d&agrave;y</strong><br />15mm (3/5 &rdquo;)<br />20mm (3/4 &rdquo;)<br />30mm (1 1/6 &rdquo;)</p>\n<p><strong>Bề mặt sản phẩm</strong><br />Đ&aacute;nh b&oacute;ng</p>\n</div>\n<div class="col-md-6">\n<p><img src="/upload/photos/shares/61ea8d146982f.jpg" alt="" width="483" height="318" /></p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', '<div class="row">\n<div class="col-md-6">\n<p><strong><img src="/upload/photos/shares/61e6a112c5e1c.png" alt="" width="388" height="255" /></strong></p>\n</div>\n<div class="col-md-6">\n<p><strong>产品描述</strong></p>\n<p>Hồ 委内瑞拉的马拉开波湖被称为&ldquo;雷霆舞蹈的天堂&rdquo;，每年约有300万次雷击，具有奇特的吸引力。全球数以十万计的著名摄影师齐聚于此，享受追寻那些令人印象深刻的瞬间的激动人心的感受。特别的是，马拉开波湖的闪电如此明亮，在400公里外都能看清，帮助水手们在茫茫大海中的黑暗中航行&hellip;&hellip;灵感来自于充满大自然的现象令人兴奋的是，我们将明亮、充满活力的闪电的力量注入了 CSH22001 - Starry Sky 套件</p>\n</div>\n</div>\n<p>&nbsp;</p>\n<p>常规尺寸 3280mm x 1650mm (129&rdquo; x 65&rdquo;)； 3280 毫米 x 1860 毫米（129 英寸 x 73 英寸）<br />巨型尺寸 3550mm x 2050mm (140&rdquo; x 80&rdquo;)</p>\n<p>egewgwegegeg</p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(190, 64, 1, 'Tin test', 'tin-test', NULL, '<p>Lấy cảm hứng từ bầu trời đ&ecirc;m với mu&ocirc;n ng&agrave;n v&igrave; sao, Star l&agrave; sự kết hợp của những mảnh k&iacute;nh v&agrave; gương tr&ecirc;n nền đen tuyền.</p>', '<p>Lấy cảm hứng từ bầu trời đ&ecirc;m với mu&ocirc;n ng&agrave;n v&igrave; sao, Star l&agrave; sự kết hợp của những mảnh k&iacute;nh v&agrave; gương tr&ecirc;n nền đen tuyền.</p>', NULL, NULL, NULL, NULL, '<p>Lấy cảm hứng từ bầu trời đ&ecirc;m với mu&ocirc;n ng&agrave;n v&igrave; sao, Star l&agrave; sự kết hợp của những mảnh k&iacute;nh v&agrave; gương tr&ecirc;n nền đen tuyền.</p>'),
	(191, 64, 2, 'Tin test', 'tin-test', 'Lấy cảm hứng từ bầu trời đêm với muôn ngàn vì sao, Star là sự kết hợp của những mảnh kính và gương trên nền đen tuyền.', '<p>Lấy cảm hứng từ bầu trời đ&ecirc;m với mu&ocirc;n ng&agrave;n v&igrave; sao, Star l&agrave; sự kết hợp của những mảnh k&iacute;nh v&agrave; gương tr&ecirc;n nền đen tuyền.</p>', '<p>Lấy cảm hứng từ bầu trời đ&ecirc;m với mu&ocirc;n ng&agrave;n v&igrave; sao, Star l&agrave; sự kết hợp của những mảnh k&iacute;nh v&agrave; gương tr&ecirc;n nền đen tuyền.</p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(192, 64, 3, 'Tin test', 'tin-test', NULL, '<p>Lấy cảm hứng từ bầu trời đ&ecirc;m với mu&ocirc;n ng&agrave;n v&igrave; sao, Star l&agrave; sự kết hợp của những mảnh k&iacute;nh v&agrave; gương tr&ecirc;n nền đen tuyền.</p>', '<p>Lấy cảm hứng từ bầu trời đ&ecirc;m với mu&ocirc;n ng&agrave;n v&igrave; sao, Star l&agrave; sự kết hợp của những mảnh k&iacute;nh v&agrave; gương tr&ecirc;n nền đen tuyền.</p>\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '<p>Lấy cảm hứng từ bầu trời đ&ecirc;m với mu&ocirc;n ng&agrave;n v&igrave; sao, Star l&agrave; sự kết hợp của những mảnh k&iacute;nh v&agrave; gương tr&ecirc;n nền đen tuyền.</p>'),
	(193, 65, 1, 'test ụ', 'test', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(194, 65, 2, 'test', 'test', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(195, 65, 3, 'test', 'test', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(196, 66, 1, 'gg', 'gg', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(197, 66, 2, 'gg', 'gg', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>'),
	(198, 66, 3, 'gg', 'gg', NULL, '<p></p>', '<p></p>', NULL, NULL, NULL, NULL, '<p></p>');
/*!40000 ALTER TABLE `product_description` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
