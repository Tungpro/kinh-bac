export default {
    namespaced: true,
    state: {
        content: {
            image: '',
            image_url: '',
            image_home: '',
            image_home_url: '',
            matter: '',
            size: '',
            sort_order: 0,

            draft_name: '',
            global_price: '',
            price: '',
            video_link: '',
            status: 2,
            stockStatus: '',
            stock_status_id: '',
            weight: '',
            primary_category_id: '',
            manufacturer_id: '',
            shipping_required: 1,
        },
        arrID_variants_add: {},
    },
    mutations: {
        contentData(state, payloadData) {
            state.content = payloadData;
        },
        deleteArrIDVariantAdd(state, value) {
            delete state.arrID_variants_add[value];
            state.arrID_variants_add = Object.assign({}, state.arrID_variants_add, state.arrID_variants_add);
        },
        addVariantID(state, value) {
            // state.arrID_variants_add.push(value.product_variant_id);
            const keyArr = value.product_variant_id;
            const productOptionInsert = value.productOptionInsert;
            state.arrID_variants_add[keyArr] = productOptionInsert;

            // console.log(productOptions);
        },
        updateMatter(state, value) {
            state.content.matter = value;
        },
        setSort_order(state, value) {
            state.content.sort_order = value;
        },
        updateSize(state, value) {
            state.content.size = value;
        },
        updateVideoLink(state, value) {
            state.content.video_link = value;
        },
        setFreeship(state, value) {
            state.content.shipping_required = value;
        },
        updateManufacturer(state, value) {
            state.content.manufacturer_id = value;
        },
        updateGlobal_price(state, value) {
            state.content.global_price = value;
        },
        updatePrice(state, value) {
            state.content.price = value;
        },
        updateStock_status_id(state, value) {
            state.content.stock_status_id = value;
        },
        updateStatus(state, value) {
            state.content.status = value;
        },
        updateWeight(state, value) {
            state.content.weight = value;
        },
        setImg(state, value) {
            state.content.image = value;
        },
        setImgUrl(state, value) {
            state.content.image_url = value;
        },
        setImgHome(state, value) {
            state.content.image_home = value;
        },
        setImgHomeUrl(state, value) {
            state.content.image_home_url = value;
        },
        setDataPrdOrigin(state, value) {
            state.content.stockStatus = value;
            state.content = Object.assign({}, state.content, state.content);
        },
        setPrimaryCategoryId(state, value) {
            state.content.primary_category_id = value;
        },
    },
    actions: {
        contentData({commit}, content) {
            commit('contentData', content);
        },
        updateMatter({commit}, content) {
            commit('updateMatter', content);
        },
        setSort_order({ commit }, content) {
            commit('setSort_order', content);
        },
        updateSize({commit}, content) {
            commit('updateSize', content);
        },
        updateVideoLink({commit}, content) {
            commit('updateVideoLink', content);
        },
        updateManufacturer({commit}, content) {
            commit('updateManufacturer', content);
        },
        updateGlobal_price({commit}, content) {
            commit('updateGlobal_price', content);
        },
        updatePrice({commit}, content) {
            commit('updatePrice', content);
        },
        updateStock_status_id({commit}, content) {
            commit('updateStock_status_id', content);
        },
        updateStatus({commit}, content) {
            commit('updateStatus', content);
        },
        updateWeight({commit}, content) {
            commit('updateWeight', content);
        },
        setImg({commit}, content) {
            commit('setImg', content);
        },
        setImgUrl({commit}, content) {
            commit('setImgUrl', content);
        },
        setImgHome({commit}, content) {
            commit('setImgHome', content);
        },
        setImgHomeUrl({commit}, content) {
            commit('setImgHomeUrl', content);
        },
        setDataPrdOrigin({commit}, content) {
            commit('setDataPrdOrigin', content);
        },
        setPrimaryCategoryId({commit}, content) {
            commit('setPrimaryCategoryId', content);
        },
        setFreeship({commit}, content) {
            commit('setFreeship', content);
        },
        addVariantID({commit}, content) {
            commit('addVariantID', content);
        },
        deleteArrIDVariantAdd({commit}, content) {
            commit('deleteArrIDVariantAdd', content);
        },
    },
    getters: {},
};
