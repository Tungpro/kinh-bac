<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-26 16:13:56
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-07-27 11:07:27
 * @ Description: Happy Coding!
 */

namespace App\Ship\Parents\Components;

use Apiato\Core\Abstracts\Components\Component as AbtractComponent;

abstract class Component extends AbtractComponent
{
    public $user;
}
