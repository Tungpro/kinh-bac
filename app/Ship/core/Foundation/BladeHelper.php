<?php

namespace App\Ship\core\Foundation;

class BladeHelper
{
    const HIEN_THI = 1;
    const AN = 2;
    const XOA = -1;

    const YES = 1 ;
    const  NO = 0 ;

    const STATUS_TEXT = [
        self::HIEN_THI => 'Hiển thị',
        self::AN => 'Ẩn',
    ];

    const STATUS_YES_NO = [
        self::YES => 'Có',
        self::NO => 'Không',
    ];

}