<?php

namespace App\Ship\Criterias\Eloquent;

use App\Ship\Parents\Criterias\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

/**
 * Class ByIdCriteria.
 *
 */
class ThisNotAllCriteria extends Criteria
{
    private $params = [];

    public function __construct(array $params = [])
    {
        $this->params = $params;
    }

    /**
     * @param                                                   $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        foreach ( $this->params as $key => $item){
            $model->where($key,'!=', $item);
        }

        return $model;
    }
}
