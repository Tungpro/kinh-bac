<?php

namespace App\Containers\File\Tasks;

use Apiato\Core\Foundation\ImageURL;
use App\Ship\Parents\Tasks\Task;

/**
 * Class UploadImageTask.
 */
class UploadFileTask extends Task
{
    public function run($request, $fileField, $title, $folder_upload)
    {

        $errorMsg = null;
        $fname = null;
        if ($request->hasFile($fileField)) {
            $file = $request->file($fileField);

            $allowedFileExtension = ['doc', 'docx', 'pdf','zip','rar'];
            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedFileExtension) && $file->isValid()) {
                $fname = ImageURL::makeFileName(!empty($prefix) ? $prefix : $file->getClientOriginalName(), $extension);

                $storeResult = $file->storeAs($folder_upload, $fname, 'upload');

                if (!$storeResult) {
                    $errorMsg = 'Upload file lên server thất bại!';
                }
            } else {
                $errorMsg = 'Upload file lên server sai định dạng!';
            }
        }

        return ['error' => !empty($errorMsg), 'msg' => @$errorMsg ?: 'Success', 'fileName' => $fname];
    }
}
