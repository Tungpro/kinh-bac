<?php

namespace App\Containers\Order\Models;

use App\Containers\Order\Traits\OrderItemTrait;
use App\Containers\Product\Models\Product;
use App\Ship\Parents\Models\Model;

class OrderItem extends Model
{
    use OrderItemTrait;

    protected $table = 'order_items';

    protected $appends = [
      'price_currency',
      'total_price_product_currency'
    ];

    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'orderitems';

    public function product() {
      return $this->belongsTo(Product::class, 'product_id', 'id');
    }
} // End class
