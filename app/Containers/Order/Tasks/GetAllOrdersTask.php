<?php

namespace App\Containers\Order\Tasks;

use App\Containers\Order\Data\Criterias\FilterOrdersCriteria;
use App\Containers\Order\Data\Repositories\OrderRepository;
use App\Ship\Criterias\Eloquent\OrderByFieldsCriteria;
use App\Ship\Criterias\Eloquent\SelectFieldsCriteria;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Ship\Parents\Tasks\Task;

class GetAllOrdersTask extends Task
{

  protected $isSimplePagin = false;
  protected $repository;

  public function __construct(OrderRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run($limit = 20)
  {
    return $this->isSimplePagin ? $this->repository->simplePaginate($limit) : $this->repository->paginate($limit);
  }

  public function simplePagin(): self
  {
    $this->isSimplePagin = true;
    return $this;
  }

  public function ordersFilter(array $filters): self
  {
    $this->repository->pushCriteria(new FilterOrdersCriteria($filters));
    return $this;
  }

  // public function with(array $with = []): self
  // {
  //   $this->repository->pushCriteria(new WithCriteria($with));
  //   return $this;
  // }

  public function selectFields(array $column = ['*']): self
  {
    $this->repository->pushCriteria(new SelectFieldsCriteria($column));
    return $this;
  }

  public function orderBy(array $orderBy): self
  {
    $this->repository->pushCriteria(new OrderByFieldsCriteria($orderBy));
    return $this;
  }
} // End class
