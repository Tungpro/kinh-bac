<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-21 14:44:04
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-24 16:38:44
 * @ Description: Happy Coding!
 */

namespace App\Containers\Order\Events\Admin\Handlers;

use App\Containers\BaseContainer\Events\Handlers\BaseFrontEventHandler;
use App\Containers\Product\Actions\ProductVariant\UpdateStockByVariantIdAction;

class UpdateStockProductByOrderHandler extends BaseFrontEventHandler
{
    public function __construct()
    {
        parent::__construct();
    }

    public function handle($event)
    {
        try{
            // dd($event->order->orderItems);
            if (isset($event->order->orderItems) && !empty($event->order->orderItems)) {
                $orderItems = $event->order->orderItems;
                $arrVariant = [];
                foreach ($orderItems as $item) {
                    $arrVariant[$item->variant_id] = $item->quantity;
                }
    // dd($arrVariant);
                $action = app(UpdateStockByVariantIdAction::class)->setVariantStock($arrVariant);
    
                isset($event->increase) ? ($event->increase ? $action->increase() : $action->decrease()) : false;
    
                $action->run();
            }
        }catch(\Exception $e) {
            throw $e;
        }
    }
}
