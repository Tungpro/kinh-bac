<tr class="font-sm {{ $order->isCancelOrder() ? 'table-danger' : '' }}"
    ondblclick="return loadIframe(this, '{{ route('admin.orders.show', ['id' => $order->id]) }}')">
    <td class="py-0">
        <p class="mb-1">
            ID:
            <a href="javascript:void(0)" class="font-italic"
                onclick="return loadIframe(this, '{{ route('admin.orders.show', ['id' => $order->id]) }}')">
                #{{ $order->id }}
            </a>
        </p>
        <p class="mb-1">Mã đơn: <em><b>{{ $order->code }}</b></em></p>
        <p class="mb-1">
            @if ($order->logs->isNotEmpty())
                <img src="{{ asset('admin/img/discount/email.png') }}" title="Đã gửi mail cho khách">
            @endif

            @if (!empty($order->coupon_code))
                <img src="{{ asset('admin/img/discount/discount-money.png') }}" data-toggle="tooltip" data-placement="bottom" title="Sử dụng mã giảm giá: {{$order->coupon_code}}">
            @endif
        </p>
    </td>
    <td class="py-1">
        <p class="mb-1"><i class="fa fa-user"></i> &nbsp;{{ ucwords($order->fullname) }}</p>

        @if (!empty($order->email))
            <p class="mb-1"><b>@</b> <a href="mailto:{{ $order->email }}">{{ $order->email }}</a></p>
        @endif

        @if (!empty($order->phone))
            <p class="mb-1"><i class="fa fa-mobile"></i> &nbsp;&nbsp;{{ $order['phone'] }}</p>
        @endif
    </td>
    <td class="py-0">
        <p class="mb-1"><i class="fa fa-truck" aria-hidden="true"></i> Phí ship: {{ $order->fee_ship_currency }}</p>
        <p class="mb-1"><i class="fa fa-money" aria-hidden="true"></i> Giảm giá: - {{ \FunctionLib::priceFormat($order->coupon_value) }}
        <p class="mb-1"><i class="fa fa-money" aria-hidden="true"></i> Tổng tiền: {{ $order->total_price_currency }}
        </p>
    </td>
    <td>
        <p class="mb-1"><i class="fa fa-money" aria-hidden="true"></i> {{ \FunctionLib::priceFormat($order->fee_shipping + $order->total_price)}}</p>
    </td>
    <td class="text-center py-0">{{ $order->created_at }}</td>
    <td class="py-0 mb-1">
        <p class="mb-1"><i class="fa fa-credit-card" title="Hình thức thanh toán" aria-hidden="true"></i>
            {{ $order->getPaymentText() }}</p>
        <p class="mb-1">
            @if ($order->isPaymentStatusSuccess())
                <strong class="text-success">
                    <i class="fa fa-opencart" aria-hidden="true"></i>
                    {{ $order->getPaymentStatusText() }}
                </strong>
            @else
                <strong class="text-dark">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> {{ $order->getPaymentStatusText() }}
                </strong>
            @endif

        </p>
    </td>
    <td class="py-0 mb-1">
        <p class="mb-1"><i class="fa fa-truck" aria-hidden="true"></i> {{ $order->getDeliveryText() }}</p>

        <p class="mb-1">
            {{ $order->getOrderStatusText() }}
        </p>
        @if ($order->user)
            <p class="mb-1">Người xử lý: {{ $order->user->name ?? $order->user->email }}</p>
        @endif
    </td>
    <td class="py-0 text-center">
        <div class="btn-group">
            @dropdown('Chọn')
            <div class="dropdown-menu">
                <input type="hidden" class="customer-hidden" value='@bladeJson($order)'>

                <a class="dropdown-item" href="javascript:void(0)"
                    onclick="return loadIframe(this, '{{ route('admin.orders.show', ['id' => $order->id]) }}')">
                    Xử lý đơn hàng
                </a>
                <a class="dropdown-item" href="javascript:void(0)"
                    onclick="return loadIframe(this, '{{ route('admin.orders.logs', ['id' => $order->id]) }}')">
                    Lịch sử xử lý đơn hàng
                </a>

                <a class="dropdown-item" href="javascript:void(0)"
                    onclick="return loadIframe(this, '{{ route('admin.orders.note.index', ['orderId' => $order->id]) }}')">
                    Ghi chú
                </a>
            </div>
        </div>
    </td>
</tr>
