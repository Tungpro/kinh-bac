<?php

namespace App\Containers\Order\UI\WEB\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;
use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Containers\BaseContainer\Actions\CreateBreadcrumbAction;
use App\Containers\Order\Actions\CreateOrderAction;
use App\Containers\Order\Actions\GetAllOrdersAction;
use App\Containers\Order\Enums\OrderStatus;
use App\Ship\Parents\Controllers\WebController;
use App\Ship\Parents\Controllers\AdminController;
use App\Containers\Order\UI\WEB\Requests\EditOrderRequest;
use App\Containers\Order\UI\WEB\Requests\StoreOrderRequest;
use App\Containers\Order\UI\WEB\Requests\CreateOrderRequest;
use App\Containers\Order\UI\WEB\Requests\DeleteOrderRequest;
use App\Containers\Order\UI\WEB\Requests\UpdateOrderRequest;
use App\Containers\Order\UI\WEB\Requests\GetAllOrdersRequest;
use App\Containers\Order\UI\WEB\Requests\FindOrderByIdRequest;
use App\Containers\Settings\Enums\PaymentStatus;
use Illuminate\Support\Facades\Auth;

/**
 * Class Controller
 *
 * @package App\Containers\Order\UI\WEB\Controllers
 */
class OrderController extends AdminController
{
  public function __construct()
  {
    $this->title = 'Đơn hàng';
    $method = request()->route()->getActionMethod();
    if (in_array($method, ['edit', 'show', 'update', 'delete'])) {
      $this->dontUseShareData = true;
    }

    parent::__construct();
  }

  /**
   * Show all entities
   *
   * @param GetAllOrdersRequest $request
   */
  public function index(GetAllOrdersRequest $request)
  {
    $filters = $request->all();
    app(CreateBreadcrumbAction::class)->run('list', $this->title, 'admin.orders.index');

    $orders = app(GetAllOrdersAction::class)->skipCache()->run(
      $filters,
      [
        'user:id,name,email',
        'logs' => function ($query) {
          $query->where('action_key', 'email_admin')->select('id', 'order_id');
        },
        'paymentType.desc:payment_type_id,name',
        'deliveryType.desc:delivery_type_id,name'
      ],

      [
        'id',
        'customer_id',
        'fullname',
        'email',
        'phone',
        'code',
        'total_price',
        'fee_shipping',
        'created_at',
        'status',
        'user_id',
        'payment_status',
        'payment_type',
        'delivery_type',
        'coupon_code',
        'coupon_value'
      ],20,
      $request->page ?? 1
    );

    // dd($orders);

    $users = collect([]);
    if (isset($filters['user_id']) && $filters['user_id']) {
      $users = Apiato::call('User@GetAllUserNoLimitAction', [
        [
          ['id', '=', $filters['user_id']]
        ],

        ['id', 'name', 'email']
      ]);
    }

    return view('order::index', [
      'orders' => $orders,
      'input' => $filters,
      'ordersType' => OrderStatus::TEXT,
      'orderPaymmentStatus' => PaymentStatus::TEXT,
      'users' => $users
    ]);
  }

  /**
   * Show one entity
   *
   * @param FindOrderByIdRequest $request
   */
  public function show(FindOrderByIdRequest $request)
  {
    $order = Apiato::call('Order@FindOrderByIdAction', [
      $request->id,
      ['orderItems', 'user:id,name,email', 'notes', 'province:id,name', 'district:id,name', 'ward:id,name']
    ]);

    return view('order::show', [
      'order' => $order,
      'input' => $request->all()
    ]);
  }

  /**
   * Create entity (show UI)
   *
   * @param CreateOrderRequest $request
   */
  public function create(CreateOrderRequest $request)
  {
    // ..
  }

  /**
   * Add a new entity
   *
   * @param StoreOrderRequest $request
   */
  public function store(StoreOrderRequest $request)
  {
    $data = $request->sanitizeInput([
      // add your request data here
    ]);

    $order = app(CreateOrderAction::class)->run($data);

    // ..
  }

  /**
   * Edit entity (show UI)
   *
   * @param EditOrderRequest $request
   */
  public function edit(EditOrderRequest $request)
  {
    $order = Apiato::call('Order@GetOrderByIdAction', [$request]);

    // ..
  }

  /**
   * Update a given entity
   *
   * @param UpdateOrderRequest $request
   */
  public function update(UpdateOrderRequest $request)
  {
    $order = Apiato::call('Order@UpdateOrderAction', [$request]);

    // ..
  }

  /**
   * Delete a given entity
   *
   * @param DeleteOrderRequest $request
   */
  public function delete(DeleteOrderRequest $request)
  {
    $result = Apiato::call('Order@DeleteOrderAction', [$request]);

    // ..
  }
}
