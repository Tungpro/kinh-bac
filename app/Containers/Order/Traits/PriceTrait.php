<?php

namespace App\Containers\Order\Traits;
use Apiato\Core\Foundation\FunctionLib;

trait PriceTrait
{
  public function getTotalPriceCurrencyAttribute() {
    return FunctionLib::priceFormat($this->total_price);
  }

  public function getFeeShipCurrencyAttribute() {
    return FunctionLib::priceFormat($this->fee_shipping);
  }

  public function getPriceCurrencyAttribute() {
    return FunctionLib::priceFormat($this->price);
  }
} // End class

