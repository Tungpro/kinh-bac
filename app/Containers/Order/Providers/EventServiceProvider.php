<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-21 14:45:44
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 11:10:42
 * @ Description: Happy Coding!
 */

namespace App\Containers\Order\Providers;

use App\Containers\Order\Events\FrontEnd\Events\OrderSuccessEvent;
use App\Containers\Order\Events\Admin\Events\RollbackProductStockEvent;
use App\Containers\Order\Events\Admin\Events\SubtractionProductStockEvent;
use App\Containers\Order\Events\Admin\Events\OrderResendMailEvent;
use App\Containers\Order\Events\Admin\Handlers\IncreaseProductPurchaseCountHanlder;
use App\Containers\Order\Events\FrontEnd\Handlers\OrderSendMailSuccessHandler;
use App\Containers\Order\Events\FrontEnd\Handlers\OrderSuccessPushNotiEventHandler;
use App\Containers\Order\Events\Admin\Handlers\UpdateStockProductByOrderHandler;
use App\Containers\Order\Events\FrontEnd\Events\OrderCancelEvent;
use App\Containers\Order\Events\FrontEnd\Handlers\OrderCancelPushNotiEventHandler;
use App\Containers\Order\Events\FrontEnd\Handlers\OrderSendMailCancelHandler;
use App\Containers\Order\Events\Admin\Handlers\OrderResendMailHandler;
use App\Containers\Order\Events\Admin\Handlers\OrderResendPushNotiEventHandler;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        // Admin
        SubtractionProductStockEvent::class => [
            UpdateStockProductByOrderHandler::class,
            IncreaseProductPurchaseCountHanlder::class
        ],
        RollbackProductStockEvent::class => [
            UpdateStockProductByOrderHandler::class
        ],
        OrderResendMailEvent::class => [
            OrderResendMailHandler::class,
            OrderResendPushNotiEventHandler::class
        ],

        // FrontEnd
        OrderSuccessEvent::class => [
            OrderSuccessPushNotiEventHandler::class,
            OrderSendMailSuccessHandler::class
        ],
        OrderCancelEvent::class => [
            OrderCancelPushNotiEventHandler::class,
            OrderSendMailCancelHandler::class
        ],
    ];

    public function boot() {
        parent::boot();
    }
}
