<?php

namespace App\Containers\Location\Models;

use App\Ship\Parents\Models\Model;

class District extends Model
{
    protected $table = '_geovndistrict';

    protected $fillable = [
        'name',
        'province_id'
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    // protected $dates = [
    //     'created_at',
    //     'updated_at',
    // ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = '_geovndistrict';

    public function city(){
        return $this->hasOne(City::class, 'id', 'province_id');
    }
}
