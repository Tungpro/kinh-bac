<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-15 16:20:37
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 16:30:02
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\Actions\Setters;

use App\Containers\Discount\Models\DiscountCode;
use App\Containers\Discount\Tasks\Setters\CustomerSaveCodeTask;
use App\Ship\Parents\Actions\Action;

class CustomerSaveCodeAction extends Action
{
    /**
     * [
     *   [
     *      'discount_code_id' => xxx,
     *      'customer_id' => xxx,
     *      'created_at' => xxx,
     *      'type' => xxx,
     *   ]
     * ]
     */
    public function run(DiscountCode $model, array $data)
    {
        $result = app(CustomerSaveCodeTask::class)->run($model, $data);

        return $result;
    }
}
