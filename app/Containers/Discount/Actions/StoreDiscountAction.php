<?php

namespace App\Containers\Discount\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class StoreDiscountAction extends Action
{
    public function run($transporter)
    {
      DB::beginTransaction();
      try {
        $data = $transporter->toArray();
        $discountCodeData = Arr::only($data['discount'], [
          'code',
          'discount_type',
          'discount_value',
          'is_use_once',
          'user_id',
          'status',
          'lang',
          'quantity',
          'object_id',
          'start_date',
          'end_date',
          'object_use',
          'discount_value',
          'product_use',
          'ship_rate',
          'extra_rule',
          'extra_rule_value',
          'quanhuyen',
          'hasShipRate'
        ]);

        $discountCodeData['user_id'] = auth()->id();
        $discountCodeData['hasShipRate'] = (int)($discountCodeData['hasShipRate']);

        $discountCode = Apiato::call('Discount@StoreDiscountCodeTask', [$discountCodeData]);
        if (isset($data['description'])) {
          foreach ($data['description'] as $key => $val){
            $dataDescription[$key] =  Arr::only($data['description'][$key], [
              'name',
              'short_description',
              'discount_value',
              'description',
              'image',
            ]);
            $dataDescription[$key]['discount_code_id'] = $discountCode->id;
            $dataDescription[$key]['language_id'] = $key;
          }
        }
        $discountDes = Apiato::call('Discount@StoreDiscountDescriptionTask', [$dataDescription]);

        // Xử lý thông tin liên quan của mã giảm giá

        if (isset($data['meta'])) {
          $discountCodeMeta = Apiato::call('Discount@StoreDiscountCodeMetaTask', [$discountCode, $data['meta']]);
        }

        $version = Apiato::call('Discount@CreateVersionDiscountCodeTask', [$discountCode]);
        DB::commit();
        return $discountCode;
      }catch(\Exception $e) {
        DB::rollBack();
        dd($e->getMessage());
      }
    }
} // E
