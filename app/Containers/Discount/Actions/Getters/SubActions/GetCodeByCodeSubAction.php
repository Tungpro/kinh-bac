<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-15 16:20:37
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 15:18:44
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\Actions\Getters\SubActions;

use App\Containers\Discount\Tasks\Getters\GetDiscountCodeByTask;
use App\Ship\Parents\Actions\SubAction;

class GetCodeByCodeSubAction extends SubAction
{
    public function run(string $code)
    {
        $coupons = app(GetDiscountCodeByTask::class)
            ->byCode($code)
            ->isActive()
            ->orderBy([
                ['end_date','desc']
            ])
            ->run();

        return $coupons;
    }
}
