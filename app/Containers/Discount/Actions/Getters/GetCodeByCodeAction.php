<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-15 16:20:37
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 15:44:52
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\Actions\Getters;

use App\Containers\Discount\Actions\Getters\SubActions\GetCodeByCodeSubAction;
use App\Containers\Discount\Models\DiscountCode;
use App\Ship\Parents\Actions\Action;

class GetCodeByCodeAction extends Action
{
    public function run(string $code): ?DiscountCode
    {
        $code = app(GetCodeByCodeSubAction::class)->run($code);

        return $code;
    }
}
