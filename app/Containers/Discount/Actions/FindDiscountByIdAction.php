<?php

namespace App\Containers\Discount\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindDiscountByIdAction extends Action
{
    public function run($transporter)
    {
        $discount = Apiato::call('Discount@FindDiscountByIdTask', [$transporter->id]);
        if ($discount->isApplyForCollection()) {
          $discount->load('productCollections.desc');
        }

        if ($discount->isApplyForProducts()) {
          $discount->load([
            'products' => function ($query) {
              $query->selectRaw("products.id, products.image");
            },

            'products.desc:product_id,name'
          ]);
        }

        if ($discount->isUseForCustomerGroups()) {
          $discount->load('customerGroups');
        }

        if ($discount->isUseForCustomers()) {
          $discount->load('customers:id,fullname,email,phone');
        }

        if ($discount->isDiscountFreeShip()) {
          $discount->load('districts.province');
        }

        $discount->load('user:id,name,email');
        return $discount;
    }
}
