<?php

namespace App\Containers\Discount\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DiscountDescriptionAction extends Action
{
  public function run($id)
  {
    // $data = $transporter->toArray();
    $discounts = Apiato::call('Discount@DiscountDescriptionTask', [$id]);
    return $discounts;
  }
} // End class
