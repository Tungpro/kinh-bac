<?php

namespace App\Containers\Discount\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class ToggleDiscountCodeStatusAction extends Action
{
  public function run(int $discountCodeId, array $data = [])
  {
    $discountcode = Apiato::call('Discount@UpdateDiscountCodeByIdTask', [$discountCodeId, $data]);
    return $discountcode;
  }
}
