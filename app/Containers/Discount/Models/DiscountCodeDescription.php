<?php

namespace App\Containers\Discount\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class DiscountCodeDescription extends Model
{
    protected $table = 'discount_code_description';

    protected $guarded = [];

    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    public $timestamps = false;
    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'discountcodedescription';

    public function language()
    {
        return $this->hasOne(Language::class,'language_id','language_id');
    }
}
