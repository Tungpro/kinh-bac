<?php

namespace App\Containers\Discount\Models;

use Apiato\Core\Traits\HasCompositePrimaryKey;
use App\Ship\Parents\Models\Model;

class DiscountCodeCustomer extends Model
{
    use HasCompositePrimaryKey;

    protected $table = 'discount_code_customer';

    protected $primaryKey = ['discount_code_id', 'customer_id'];

    protected $guarded = [];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'discountcodecustomers';
}
