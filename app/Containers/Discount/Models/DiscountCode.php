<?php

namespace App\Containers\Discount\Models;

use App\Ship\Parents\Models\Model;
use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Product\Models\Product;
use App\Containers\Customer\Models\Customer;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Containers\Customer\Models\CustomerGroup;
use App\Containers\Product\Models\ProductCollection;
use App\Containers\Discount\Traits\DiscountCodeTrait;
use App\Ship\core\Traits\HelpersTraits\VersionableTrait;
use App\Containers\Discount\Traits\DiscountCodeAttributeTrait;
use App\Containers\Discount\Traits\DiscountCodeExtraRuleTrait;
use App\Containers\Location\Models\District;
use App\Containers\User\Models\User;

class DiscountCode extends Model
{
  use DiscountCodeTrait;
  use DiscountCodeAttributeTrait;
  use DiscountCodeExtraRuleTrait;
  use SoftDeletes;
  use VersionableTrait;

  protected $table = 'discount_code';

  protected $guarded = [];

  protected $appends = ['discount_value_text'];

  protected $attributes = [];

  protected $hidden = [];

  protected $casts = [];

  protected $dates = [
    'created_at',
    'updated_at',
    'start_date',
    'end_date',
  ];


  /**
   * A resource key to be used by the the JSON API Serializer responses.
   */
  protected $resourceKey = 'discounts';

  public function desc()
  {
    return $this->hasOne(DiscountCodeDescription::class, 'discount_code_id', 'id');
  }

  public function all_desc()
  {
    return $this->hasManyKeyBy('language_id', DiscountCodeDescription::class, 'discount_code_id', 'id');
  }

  public function productCollections()
  {
    return $this->belongsToMany(ProductCollection::class, DiscountCodeProductCollection::getTableName(), 'discount_code_id', 'product_collection_id');
  }

  public function products()
  {
    return $this->belongsToMany(Product::class, DiscountCodeProduct::getTableName(), 'discount_code_id', 'product_id');
  }

  public function districts()
  {
    return $this->belongsToMany(District::class, DiscountCodeDistrict::getTableName(), 'discount_code_id', 'district_id');
  }

  public function customers()
  {
    return $this->belongsToMany(Customer::class, DiscountCodeCustomer::getTableName(), 'discount_code_id', 'customer_id');
  }

  public function customerGroups()
  {
    return $this->belongsToMany(CustomerGroup::class, DiscountCodeCustomerGroup::getTableName(), 'discount_code_id', 'customer_group_id');
  }

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function getStartDateAttribute($value)
  {
    return FunctionLib::dateFormat($value, 'Y-m-d\TH:i');
  }

  public function getEndDateAttribute($value)
  {
    return FunctionLib::dateFormat($value, 'Y-m-d\TH:i');
  }
} // End class
