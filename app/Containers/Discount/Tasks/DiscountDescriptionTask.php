<?php

namespace App\Containers\Discount\Tasks;

use Exception;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Discount\Data\Repositories\DiscountCodeDescriptionRepository;

class DiscountDescriptionTask extends Task
{

    protected $repository;

    public function __construct(DiscountCodeDescriptionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->where('discount_code_id', $id)->get();
        }
        catch (Exception $exception) {
            throw $exception;
        }
    }
}
