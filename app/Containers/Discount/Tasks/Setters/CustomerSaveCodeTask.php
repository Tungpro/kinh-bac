<?php

namespace App\Containers\Discount\Tasks\Setters;

use Exception;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Discount\Data\Repositories\DiscountCodeRepository;
use App\Containers\Discount\Models\DiscountCode;

class CustomerSaveCodeTask extends Task
{
    protected $repository;

    public function __construct(DiscountCodeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * [
     *   [
     *      'discount_code_id' => xxx,
     *      'customer_id' => xxx,
     *      'created_at' => xxx,
     *      'type' => xxx,
     *   ]
     * ]
     */
    public function run(DiscountCode $model, array $data)
    {
        try {
            return $model->customers()->sync($data);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
