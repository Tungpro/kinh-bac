<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-15 15:44:49
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 15:16:26
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\Tasks\Getters;

use App\Containers\Customer\Models\Customer;
use Exception;
use App\Containers\Discount\Data\Repositories\DiscountCodeRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisInThatCriteria;

class GetDiscountCodeByTask extends BaseGetCodeTask
{
    protected $repository;

    public function __construct(DiscountCodeRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    public function run()
    {
        try {
            // DB::enableQueryLog();
            return $this->repository->first();
            // dd(DB::getQueryLog());
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    public function byCode(string $code): self
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('code', $code));
        return $this;
    }
}
