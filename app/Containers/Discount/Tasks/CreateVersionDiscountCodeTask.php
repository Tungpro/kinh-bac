<?php

namespace App\Containers\Discount\Tasks;

use App\Containers\Discount\Data\Repositories\DiscountCodeRepository;
use App\Containers\Discount\Models\DiscountCode;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateVersionDiscountCodeTask extends Task
{

  protected $repository;

  public function __construct(DiscountCodeRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run(DiscountCode $discount)
  {
    try {
      if ($discount->isApplyForCollection()) {
        $discount->load('productCollections.desc');
      }

      if ($discount->isApplyForProducts()) {
        $discount->load([
          'products' => function ($query) {
            $query->selectRaw("products.id, products.image");
          },

          'products.desc:product_id,name'
        ]);
      }

      if ($discount->isUseForCustomerGroups()) {
        $discount->load('customerGroups');
      }

      if ($discount->isUseForCustomers()) {
        $discount->load('customers:id,fullname,email,phone');
      }

      if ($discount->isDiscountFreeShip()) {
        $discount->load('districts.province');
      }

      $discount->load('user');

      $discount->versionablePostSave();
    } catch (Exception $exception) {
      throw $exception;
      // dd($exception->getMessage());
    }
  }
} // End class
