<?php

namespace App\Containers\Discount\Tasks;

use Exception;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Containers\Discount\Data\Repositories\DiscountCodeDescriptionRepository;

class StoreDiscountDescriptionTask extends Task
{

    protected $repository;

    public function __construct(DiscountCodeDescriptionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            foreach ($data as $key => $value) {
                $this->repository->create($value);
            }
            return true;
        }
        catch (Exception $exception) {
            throw $exception;
        }
    }
}
