<?php

namespace App\Containers\Discount\Tasks;

use Exception;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Containers\Discount\Data\Repositories\DiscountCodeRepository;
use App\Containers\Discount\Data\Criterias\FilterDiscountCodeCriteria;

class GetAllDiscountsCodeTask extends Task
{

    protected $repository;

    public function __construct(DiscountCodeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        try {
            return $this->repository->paginate(15);
        }
        catch (Exception $exception) {
            throw $exception;
        }
    }

    public function filterDiscountCode($transporter) {
      $this->repository->pushCriteria(new FilterDiscountCodeCriteria($transporter));
    }
}
