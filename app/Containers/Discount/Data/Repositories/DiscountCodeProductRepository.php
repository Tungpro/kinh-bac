<?php

namespace App\Containers\Discount\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class DiscountCodeProductRepository
 */
class DiscountCodeProductRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
