<?php

namespace App\Containers\Discount\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class DiscountCodeDistrictRepository
 */
class DiscountCodeDistrictRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
