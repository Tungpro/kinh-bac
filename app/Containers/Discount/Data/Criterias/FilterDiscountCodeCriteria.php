<?php

namespace App\Containers\Discount\Data\Criterias;

use Apiato\Core\Foundation\FunctionLib;
use App\Ship\Parents\Criterias\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;
use Carbon\Carbon;

class FilterDiscountCodeCriteria extends Criteria
{
  private $transporter;

  public function __construct($transporter)
  {
    $this->transporter = $transporter;
  }

  public function apply($model, PrettusRepositoryInterface $repository) {
    if ($this->transporter->discount_type) {
      $model = $model->where('discount_type', $this->transporter->discount_type);
    }

    if ($this->transporter->code) {
      $model = $model->where('code', 'REGEXP', $this->transporter->code);
    }

    if (isset($this->transporter->status) && $this->transporter->status != '') {
      if ($this->transporter->status == 1) {
        $model = $model->where('status', '=', $this->transporter->status);
      }else {
        $model = $model->where('status', '!=', 1);
      }
    }

    if ($this->transporter->conlai) {
      switch ($this->transporter->conlai) {
        case 1:
          $model = $model->whereRaw("(1-use_success/quantity) > 0.7");
          break;

        case 2:
          $model = $model->whereRaw("( (1-use_success/quantity) > 0 AND (1-use_success/quantity) < 0.3 )");
          break;

        case 3:
          $model = $model->whereRaw("(1 - (use_success/quantity)) = 0");
          break;

        default:
          // code
          break;
      }
    }

    if ($this->transporter->object_use) {
      $model = $model->where('object_use', $this->transporter->object_use);
    }

    if ($this->transporter->product_use) {
      $model = $model->where('product_use', $this->transporter->product_use);
    }

    if ($this->transporter->start_date) {
      // $date = FunctionLib::getCarbonFromVNDate($this->transporter->start_date)->toDateTimeString();
      $date = Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($this->transporter->start_date));
      $model = $model->whereDate('start_date', '>=', $date);
    }

    if ($this->transporter->orderByType || $this->transporter->orderByQty || $this->transporter->orderByCode) {
      if ($this->transporter->orderByType) {
        $model = $model->orderByRaw($this->transporter->orderByType);
      }

      if ($this->transporter->orderByQty) {
        $model = $model->orderByRaw($this->transporter->orderByQty);
      }

      if ($this->transporter->orderByCode) {
        $model = $model->orderByRaw($this->transporter->orderByCode);
      }
    }else {
      $model = $model->latest();
    }
    return $model;
  }
} // End class
