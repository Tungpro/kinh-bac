<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-18 14:37:58
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class DiscountCodeExtraRule extends BaseEnum
{
    /**
     * Không có điều kiện
     */
    const NONE = 'none';

    /**
     * Giá trị đơn hàng tối thiếu
     */
    const ORDER_MINIMUM = 'order_minimun';

    /**
     * Số lượng sản phẩm tối thiểu trong đơn
     */
    const QUANTITY_MINIMUM = 'quantity_minimum';

    const TEXT = [
        self::NONE => 'Không có điều kiện',
        self::ORDER_MINIMUM => 'Giá trị đơn hàng tối thiếu',
        self::QUANTITY_MINIMUM => 'Số lượng sản phẩm tối thiểu trong đơn'
    ];
}
