<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 12:09:33
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class DiscountCodeCustomerType extends BaseEnum
{
    /**
     * Customer tự lưu mã vào danh sách
     */
    const ONSAVE = "onsave";

    /**
     * Được chỉ định trong quản trị
     */
    const ASSIGN = 'assign';

    const TEXT = [
        self::ONSAVE => 'Tự lưu',
        self::ASSIGN => 'Chỉ định',
    ];
}
