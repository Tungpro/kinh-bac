<?php

namespace App\Containers\Discount\Traits;

trait DiscountCodeTrait
{
  // Loại giảm giá
  public function isDiscountPercent(): bool
  {
    return $this->discount_type == 'percent';
  }

  public function isDiscountAmount(): bool
  {
    return $this->discount_type == 'amount';
  }

  public function isDiscountFreeShip(): bool
  {
    return $this->discount_type == 'freeship';
  }

  public function isDiscountGetA2B(): bool
  {
    return $this->discount_type == 'geta2b';
  }

  // Áp dụng cho sản phẩm, nhóm sản phẩm
  public function isApplyForAllProduct(): bool
  {
    return $this->product_use == '*';
  }

  public function isApplyForCollection(): bool
  {
    return $this->product_use == 'collection';
  }

  public function isApplyForProducts(): bool
  {
    return $this->product_use == 'single_product';
  }

  // Đối tượng sử dụng
  public function isUseForAnyOne(): bool
  {
    return $this->object_use == '*';
  }

  public function isUseForCustomerGroups(): bool
  {
    return $this->object_use == 'groups';
  }

  public function isUseForCustomers(): bool
  {
    return $this->object_use == 'personal';
  }

  // 1 khách hàng chỉ được sử dụng mã này 1 lần
  public function isUseOncePerCustomer(): bool
  {
    return $this->is_use_once == 1;
  }

  // Check mã đang hoạt động
  public function isActive(): bool {
    return $this->status == 1;
  }

  // Ship
  public function isFreeShipAllPlace() {
    return $this->isDiscountFreeShip() && $this->quanhuyen = '*';
  }

  public function isFreeShipSpecialLocation() {
    return $this->isDiscountFreeShip() && $this->quanhuyen == 'single_quanhuyen';
  }
} // End class
