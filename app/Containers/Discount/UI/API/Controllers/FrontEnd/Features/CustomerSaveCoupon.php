<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 16:36:10
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Discount\Actions\Getters\GetCodeByCodeAction;
use App\Containers\Discount\Actions\Setters\CustomerSaveCodeAction;
use App\Containers\Discount\Enums\DiscountCodeCustomerType;
use App\Containers\Discount\Models\DiscountCode;
use App\Containers\Discount\UI\API\Requests\FrontEnd\CustomerSaveCouponsRequest;
use Carbon\Carbon;

trait CustomerSaveCoupon
{
    public function customerSaveCoupon(CustomerSaveCouponsRequest $request)
    {
        $code = app(GetCodeByCodeAction::class)->run($request->code);
        // dd($this->user->id);
        $result = null;
        if ($code) {
            $result = app(CustomerSaveCodeAction::class)->run($code, [[
                'discount_code_id' => $code->id,
                'customer_id' => $this->user->id,
                'created_at' => Carbon::now(),
                'type' => DiscountCodeCustomerType::ONSAVE
            ]]);
        }

        return $this->sendResponse([],'Ok');
    }
}
