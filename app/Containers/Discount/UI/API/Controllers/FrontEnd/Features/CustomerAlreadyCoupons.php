<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 15:19:05
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Discount\Actions\Getters\GetAlreadyCodeForCustomerAction;
use App\Containers\Discount\UI\API\Requests\FrontEnd\CustomerAlreadyCouponsRequest;
use App\Containers\Discount\UI\API\Transformers\FrontEnd\ListCouponCodeTransfomer;

trait CustomerAlreadyCoupons
{
    public function customerAlreadyCoupons(CustomerAlreadyCouponsRequest $request)
    {
        $coupons = app(GetAlreadyCodeForCustomerAction::class)
            ->descDiscountCode($this->currentLang)
            ->run($this->user->id, 24, $this->currentLang, $request->page ?? 1);
        // dd($coupons);

        return $this->transform($coupons, new ListCouponCodeTransfomer, [], [], 'coupons');
    }
}
