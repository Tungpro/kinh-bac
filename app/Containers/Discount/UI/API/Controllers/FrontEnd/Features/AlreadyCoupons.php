<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 15:19:21
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Discount\Actions\Getters\GetAlreadyCodeForAllAction;
use App\Containers\Discount\Actions\Getters\GetAlreadyCodeForCustomerAction;
use App\Containers\Discount\UI\API\Requests\FrontEnd\AlreadyCouponsRequest;
use App\Containers\Discount\UI\API\Transformers\FrontEnd\ListCouponCodeTransfomer;

trait AlreadyCoupons
{
    public function alreadyCoupons(AlreadyCouponsRequest $request)
    {
        $coupons = app(GetAlreadyCodeForAllAction::class)
            ->descDiscountCode($this->currentLang)
            ->run(24,$this->currentLang,$request->page ?? 1);

        // return $coupons;

        return $this->transform($coupons,new ListCouponCodeTransfomer,[],[],'coupons');
    }
}
