<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-04 14:54:56
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-20 16:35:02
 * @ Description: Happy Coding!
 */

namespace App\Containers\Discount\UI\API\Transformers\FrontEnd;

use App\Containers\Discount\Models\DiscountCode;
use App\Ship\Parents\Transformers\Transformer;
use Carbon\Carbon;

class ListCouponCodeTransfomer extends Transformer
{
    protected array $defaultIncludes = [];
    protected array $availableIncludes = [];

    public function transform(DiscountCode $entity)
    {
        $data =[
            'id' => $entity->id,
            'code' => $entity->code,
            'end_date' => Carbon::createFromDate($entity->end_date)->format('d/m/Y H:i:s'),
            'name' => $entity->desc->name,
            'short_description' => $entity->desc->short_description,
            'description' => $entity->desc->description
        ];

        return $data;
    }
}
