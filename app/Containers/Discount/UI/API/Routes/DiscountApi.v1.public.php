<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-04 16:55:35
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 15:10:25
 * @ Description: Happy Coding!
 */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;
// dd(app(CheckSegmentLanguageAction::class)->run());
Route::group(
    [
        'middleware' => [
            // 'api',
        ],
        'prefix' => app(CheckSegmentLanguageAction::class)->run() . '/discount',
    ],
    function () use ($router) {
        $router->any('/alreadyCoupons', [
            'as' => 'api_discount_already_coupons',
            'uses'       => 'FrontEnd\Controller@alreadyCoupons'
        ]);

        $router->group(
            [
                'middleware' => [
                    'auth:api-customer'
                ],
                'prefix' => 'customer'
            ],
            function () use ($router) {
                $router->any('/alreadyCoupons', [
                    'as' => 'api_discount_cus_already_coupons',
                    'uses'       => 'FrontEnd\Controller@customerAlreadyCoupons'
                ]);

                $router->put('/saveCoupons/{code?}', [
                    'as' => 'api_discount_cus_save_coupons',
                    'uses'       => 'FrontEnd\Controller@customerSaveCoupon'
                ])->where([
                    'code' => '[a-zA-Z0-9]+'
                ]);
            }
        );
    }
);
