<?php

$router->group(['prefix' => 'discount'], function () use ($router) {
  $router->get('code', [
    'as' => 'admin.discount.code.index',
    'uses'  => 'DiscountCodeController@index',
    'middleware' => [
      'auth:admin',
    ],
  ]);

  $router->get('code/create', [
    'as' => 'admin.discount.code.create',
    'uses'  => 'DiscountCodeController@create',
    'middleware' => [
      'auth:admin',
    ],
  ]);

  $router->post('code/store', [
    'as' => 'admin.discount.code.store',
    'uses'  => 'DiscountCodeController@store',
    'middleware' => [
      'auth:admin',
    ],
  ]);

  $router->get('code/{id}', [
    'as' => 'admin.discount.code.show',
    'uses'  => 'DiscountCodeController@show',
    'middleware' => [
      'auth:admin',
    ],
  ]);

  $router->get('code/{id}/edit', [
    'as' => 'admin.discount.code.edit',
    'uses'  => 'DiscountCodeController@edit',
    'middleware' => [
      'auth:admin',
    ],
  ]);

  $router->post('code/update/{id}', [
    'as' => 'admin.discount.code.update',
    'uses'  => 'DiscountCodeController@update',
    'middleware' => [
      'auth:admin',
    ],
  ]);

  $router->put('code/toggle/{id}', [
    'as' => 'admin.discount.code.toggle-status',
    'uses'  => 'DiscountCodeController@toggleStatus',
    'middleware' => [
      'auth:admin',
    ],
  ]);

  $router->delete('code/{id}', [
    'as' => 'admin.discount.code.delete',
    'uses'  => 'DiscountCodeController@delete',
    'middleware' => [
      'auth:admin',
    ],
  ]);

  $router->get('code/history/{id}', [
    'as' => 'admin.discount.code.history',
    'uses'  => 'DiscountCodeHistoryController@all',
    'middleware' => [
      'auth:admin',
    ],
  ]);
  $router->get('code/ajaxDescription/{id}', [
    'as' => 'admin.discount.code.ajaxDescription',
    'uses'  => 'DiscountCodeController@ajaxDescription',
    'middleware' => [
      'auth:admin',
    ],
  ]);

  $router->get('code/history/{version_id}/{id}', [
    'as' => 'admin.discount.code.history-detail',
    'uses'  => 'DiscountCodeHistoryController@getDiscountCodeVersion',
    'middleware' => [
      'auth:admin',
    ],
  ])->where(['version_id' => '[0-9]+', 'id' => '[0-9]+']);
});

