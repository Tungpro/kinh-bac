@extends('basecontainer::admin.layouts.default_for_iframe')
@section('content')
<div class="row" id="sectionContent">
  <div class="col-12">
    <div class="card mb-0">
      <div class="card-header d-flex">
        <button class="btn btn-link">Lịch sử thay đổi mã code: <strong>{{ $discountCode->code }}</strong></button>
        <button type="button" class="btn btn-secondary ml-auto mr-2" onclick='return closeFrame()'>Đóng lại</button>

      </div>

      <div class="card-body">
        @php($versions = $discountCode->versions)
        @if ($discountCode->versions)
        <div class="py-2">
          @foreach($versions as $version)
            @include('discount::code.inc.history-item', [
              'version' => $version,
              'loop' => $loop
            ])
          @endforeach
        </div><!--container-->
        @else
          <div class="d-flex justify-content-center mt-4">Không có lịch sử</div>
        @endif
      </div><!-- /.Card-body -->
    </div><!-- /.End card -->
  </div>
</div>
@endsection
