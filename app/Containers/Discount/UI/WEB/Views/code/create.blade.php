@extends('basecontainer::admin.layouts.default')

@section('right-breads')

@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12 position-relative">
            <div class="card"  id="commentCreateApp">
              <div class="card-header d-flex">
                <button class="btn btn-link">{{ __('Tạo mã giảm giá') }}</button>
                <button type="button"
                        class="btn btn-primary ml-auto"
                        :disabled="loading"
                        @click="saveDiscountCode('{{ route('admin.discount.code.store') }}')">
                        <i class="fa fa-spinner fa-pulse" v-show="loading"></i> {{ __('Tạo mã') }}
                </button>
              </div>
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" href="#detail" role="tab" data-toggle="tab" aria-selected="true">Chi tiết</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#info" role="tab" data-toggle="tab" aria-selected="true">Thông tin</a>
                </li>
              </ul>
              <form action="{{ route('admin.discount.code.store') }}" method="POST" id="formCreateMenu" v-cloak>
                @csrf
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id='detail'>
                    <div class="card-body">
                      <div class="form-group">
                        <div class="d-flex">
                          <label for="" class="fa-13e">Mã giảm giá</label>
                          <a href="javascript:void(0)" class="ml-auto" @click="generateDiscountCode()">Sinh mã ngẫu nhiên</a>
                        </div>
                        <input type="text" class="form-control" id="discount_code" v-model="discount.code">
                        <small class="help-block text-info"><i class="fa fa-info-circle"></i> KH dùng mã này tại bước thanh toán</small>
                      </div>

                      @include('discount::code.inc.cach-thuc-hoat-dong')


                      <hr class="row">
                      <div class="form-group">
                        <label for="" class="fa-13e">Đối tượng sử dụng mã</label>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="object_use" id="objectUse1" value="*" v-model="discount.object_use">
                          <label class="form-check-label" for="objectUse1">
                            Bất kỳ ai
                          </label>
                        </div>
                        {{-- <div class="form-check">
                          <input  class="form-check-input"
                                  type="radio"
                                  name="object_use"
                                  id="objectUse2"
                                  value="groups"
                                  v-model="discount.object_use" />
                          <label class="form-check-label" for="objectUse2">
                            Nhóm khách hàng cụ thể
                          </label>
                        </div>

                        <div class="form-check">
                          <input  class="form-check-input"
                                  type="radio"
                                  name="object_use"
                                  id="objectUse3"
                                  value="personal"
                                  v-model="discount.object_use" />
                          <label class="form-check-label" for="objectUse3">
                            Khách hàng được chỉ định
                          </label>
                        </div> --}}

                        @include('discount::code.inc.modal-customers-group')


                        @include('discount::code.inc.modal-customers')
                      </div> <!-- /.Đối tượng dùng mã -->

                      <hr class="row">
                      <div class="form-group">
                        <label for="" class="fa-13e">Lượt sử dụng</label>

                        <div class="form-check pl-0 mb-2">
                          <div class="input-group">
                            <input  type="number"
                                    name="number_use"
                                    class="form-control"
                                    placeholder="Nhập số lượt sử dụng mã giảm giá"
                                    v-model="discount.quantity" 
                                    onkeypress="return shop.numberOnly()" min="0" onfocus="this.select()" onchange="checkNumber(this, true)"
                            />

                            <div class="input-group-prepend">
                              <span class="input-group-text" id=""><i class="fa fa-ticket mr-2" aria-hidden="true"></i> <b>Số lượt sử dụng</b></span>
                            </div>
                          </div>
                        </div>

                        {{-- <div class="form-check">
                          <input class="form-check-input" type="checkbox" name="use_time" id="useTime1" value="increment">
                          <label class="form-check-label" for="useTime1">
                            Sử dụng cho đến khi hết số lượng mã thì ngừng giảm giá
                          </label>
                        </div> --}}
                        <div class="form-check">
                          <input  class="form-check-input"
                                  type="checkbox"
                                  name="use_time"
                                  id="useTime2"
                                  v-model="discount.is_use_once"
                                  true-value="1"
                                  false-value="0">
                          <label class="form-check-label" for="useTime2">
                            Mỗi khách chỉ được dùng mã 1 lần duy nhất
                          </label>
                        </div>
                      </div>

                      <hr class="row">
                      <div class="form-group">
                        <label for="" class="fa-13e">Khoảng thời gian sử dụng</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="">Từ ngày</span>
                          </div>
                          <input  type="datetime-local"
                                  name="from_date"
                                  class="form-control"
                                  v-model="discount.start_date"
                          />

                          <div class="input-group-prepend">
                            <span class="input-group-text" id="">đến</span>
                          </div>

                          <input  type="datetime-local"
                                  name="to_date"
                                  class="form-control"
                                  v-model="discount.end_date"
                          />
                        </div>
                      </div>
                    </div><!-- /.card-body -->
                  </div>
                  @include('discount::code.inc.tabDescription')
                </div>
              </form>
            </div>
        </div>
  </div>
@endsection

@section('js_top')
{!! \FunctionLib::addMedia('admin/js/library/tinymce/tinymce.min.js') !!}
  <script src="{{ asset('admin/js/vue_dev.js')}}"></script>
  <script>
      Vue.component('tinymce', {
      props: ['value'],
      template: '<div style="width:2000px; height:800px"><textarea rows="5" v-bind:value="value"></textarea></div>',
      methods: {
      updateValue: function(value) {
      this.$emit('input', value.trim());
      },
      },
          mounted: function() {
            const component = this;
            tinymce.init({
              height: 500,
              target: this.$el.children[0],
              setup: function(editor) {
                editor.on('Change', function(e) {
                  component.updateValue(editor.getContent());
                });
              },
            });
          },
      });
      function checkNumber(element, isInt = false){
          element.value = isInt ? ( Number.isInteger(+element.value) ? element.value  : '' ) : ( isNaN(element.value) ? '' : element.value);
      }
  </script>
@endsection


