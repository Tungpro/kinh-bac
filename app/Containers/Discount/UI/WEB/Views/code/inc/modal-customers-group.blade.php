<div class="mt-3" v-show="discount.object_use == 'groups'">
  <div class="d-flex">
    <input  type="text"
            class="form-control"
            placeholder="Tìm kiếm nhóm khách hàng"
            v-model="lists.customerGroups.keyword"
            @input="showModalCustomerGroup('#modalHandleCustomerGroup')">

    <input type="hidden" name="customer_groups_ids" id="customer_groups_ids">

    <button type="button" class="btn btn-primary px-4 ml-2" @click="showModalCustomerGroup('#modalHandleCustomerGroup')">
      Duyệt
    </button>

    <!-- Modal -->
    <div class="modal fade" id="modalHandleCustomerGroup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Chọn nhóm khách hàng được sử dụng mã giảm giá</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-header">
            <input  type="text"
                    class="form-control"
                    name="filter-groups"
                    placeholder="&#xF002; Lọc nhóm khách hàng"
                    style="font-family: Helvetica, FontAwesome;"
                    v-model="lists.customerGroups.keyword">
          </div>

          <div class="modal-header" v-show="lists.customerGroups.choice.length > 0">
            <div class="d-flex align-content-around flex-wrap">
              <div class="btn-group mr-2 mb-1" v-for="(group, index) in lists.customerGroups.choice" :key="group.id">
                <button type="button" class="btn btn-outline-info btn-sm">@{{ group.title || group.display_name }}</button>
                <button type="button" class="btn btn-outline-info" @click="removeCustomerGroup(index)">x</button>
              </div>
            </div>
          </div>

          <div class="modal-body">
            <ul class="list-group">
              <li class="list-group-item" v-for="group in filteredCustomerGroups" :key="group.id">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" :value="group" :id="'group'+group.id" v-model="lists.customerGroups.choice">
                  <label class="form-check-label" :for="'group'+group.id">
                    @{{ group.title || group.display_name }}
                  </label>
                </div>
              </li>
            </ul>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng lại</button>
          </div>
        </div>
      </div>
    </div>

  </div>
  <small>Chọn nhóm KH có thể sử dụng mã giảm giá này</small>

  <ul class="list-group">
    <li class="list-group-item" v-for="(group, index) in lists.customerGroups.choice"
                                :key="group.id">
      <div class="form-check d-flex">
        <label class="form-check-label" :for="'group'+group.id">
          <i class="fa fa-users"></i> @{{ group.title || group.display_name }}
        </label>
        <a href="javascript:void(0)" class="ml-auto" @click="removeCustomerGroup(index)">x</a>
      </div>
    </li>
  </ul>
</div>




