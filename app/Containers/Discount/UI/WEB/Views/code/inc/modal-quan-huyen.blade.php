<div id="quanhuyen" v-show="discount.discount_type == 'freeship'">
  <label for="" class="fa-13e mt-2 text-success">#4. Quận/Huyện</label>
  <div class="form-check">
    <input class="form-check-input" type="radio" name="quanhuyen" id="freeShip1" value="*" v-model="discount.quanhuyen">
    <label class="form-check-label" for="freeShip1">
      Tất cả quận/huyện
    </label>
  </div>

  {{-- <div class="form-check">
    <input class="form-check-input" type="radio" name="quanhuyen" id="freeShip2" value="single_quanhuyen" v-model="discount.quanhuyen">
    <label class="form-check-label" for="freeShip2">
      Quận huyện cụ thể
    </label>

    <div class="d-flex mt-2" v-if="discount.quanhuyen == 'single_quanhuyen'">
      <input  type="text"
              class="form-control"
              placeholder="&#xF002; Tìm kiếm địa điểm muốn miễn phí giao hàng"
              style="font-family: Helvetica, FontAwesome;"
              v-model="lists.provinces.keyword"
              @input="showModalQuanHuyen('#modalQuanHuyen')" />

      <button type="button" class="btn btn-primary px-4 ml-2" @click="showModalQuanHuyen('#modalQuanHuyen')">
        Duyệt
      </button>

      <!-- Modal -->
      <div class="modal fade" id="modalQuanHuyen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Thông tin địa chính</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-header" v-show="lists.districts.choice.length > 0">
              <div class="d-flex align-content-around flex-wrap">
                <div class="btn-group mr-2 mb-1" v-for="(district, index) in lists.districts.choice" :key="district.id">
                  <button type="button" class="btn btn-outline-info btn-sm">@{{ district.district_tag_name }}</button>
                  <button type="button" class="btn btn-outline-info" @click="removeDistrictChosen(district, index)">x</button>
                </div>
              </div>
            </div>

            <div class="modal-body">
                           <div class="row">
                <div class="col">
                  <label for="">Tỉnh/Thành</label>
                  <ul class="list-group">
                    <li class="list-group-item active">
                      <input  type="text"
                              class="form-control"
                              name="filter-groups"
                              placeholder="&#xF002; Lọc tỉnh thành"
                              style="font-family: Helvetica, FontAwesome;"
                              v-model="lists.provinces.keyword">
                    </li>

                    <div style="height: 400px; overflow-y: auto;">
                      <li class="list-group-item" v-for="province in filteredProvinces" :key="province.id">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="province_id" :value="province.id" :id="'province-'+province.id" v-model="lists.provinces.choice">
                          <label class="form-check-label" :for="'province-'+province.id">
                            @{{ province.Name_VI }}
                          </label>
                        </div>
                      </li>
                    </div>
                  </ul>
                </div>

                <div class="col">
                  <label for="">Quận/Huyện</label>
                  <ul class="list-group" v-show="lists.districts.data">
                    <li class="list-group-item active">
                      <input  type="text"
                              class="form-control"
                              name="filter-groups"
                              placeholder="&#xF002; Lọc quận huyện"
                              style="font-family: Helvetica, FontAwesome;"
                              v-model="lists.districts.keyword">
                    </li>

                    <div style="height: 400px; overflow-y: auto;">
                      <li class="list-group-item" v-for="district in filteredDistricts" :key="district.id">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" :value="district" :id="'district-'+district.id" v-model="lists.districts.choice">
                          <label class="form-check-label" :for="'district-'+district.id">
                            @{{ district.Name_VI }}
                          </label>
                        </div>
                      </li>
                    </div>
                  </ul>
                </div>
              </div>
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng lại</button>
            </div>
          </div>
        </div>
      </div><!-- /.End modal -->
    </div><!-- /.End v-if -->

    <ul class="list-group  mt-2">
      <li class="list-group-item"
          v-for="(district, index) in lists.districts.choice" :key="district.id">
        <div class="form-check d-flex">
          <label class="form-check-label" :for="'district'+district.id">
            <i class="fa fa-map-marker" aria-hidden="true"></i> @{{ district.district_tag_name }}
          </label>

          <a href="javascript:void(0)" class="ml-auto" @click="removeDistrictChosen(district, index)">x</a>
        </div>
      </li>
    </ul>
  </div> --}}


  <!-- Gioi han so tien co the freeship -->
  <div class="form-check mt-3">
    <input class="form-check-input" type="checkbox" name="ship-rate" id="shipRate" value="1" v-model="discount.hasShipRate">
    <label class="form-check-label" for="shipRate">
      Giới hạn số tiền có thể hỗ trợ miễn phí ship
    </label>

    <div class="row col-3 mt-1" v-show="discount.hasShipRate">
      <input type="text" v-model="discount.ship_rate" class="form-control" placeholder="đ 0"
      onkeypress="return shop.numberOnly()" onfocus="this.select()" onchange="checkNumber(this, true)"
      >
    </div>
  </div>
</div><!-- /.End v-if quan huyen -->

