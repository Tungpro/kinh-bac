<div class="mt-3" v-if="discount.product_use == 'single_product'">
  <div class="d-flex">
    <input  type="text"
            class="form-control"
            placeholder="&#xF002; Tìm kiếm sản phẩm"
            style="font-family: Helvetica, FontAwesome;">
    <button type="button" class="btn btn-primary px-4 ml-2" @click="showModalProduct('#modalProducts')">
      Duyệt
    </button>

    <!-- Modal -->
    <div class="modal fade" id="modalProducts" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Chọn sản phẩm áp dụng mã giảm giá</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-header">
            <input  type="text"
                    class="form-control"
                    name="filter-groups"
                    placeholder="&#xF002; Lọc sản phẩm"
                    style="font-family: Helvetica, FontAwesome;"
                    v-model="lists.products.filter.keyword">
          </div>

          <div class="modal-header" v-if="lists.products.choice.length > 0">
            <div class="d-flex align-content-around flex-wrap">
              <div class="btn-group mr-2 mb-1" v-for="(product, index) in lists.products.choice" :key="product.id">
                <button type="button" class="btn btn-outline-info btn-sm">@{{ product.name }}</button>
                <button type="button" class="btn btn-outline-info" @click="removeProduct(index)">x</button>
              </div>
            </div>
          </div>

          <div class="modal-body">
            {{-- <nav aria-label="Page navigation example" v-show="lists.products.data.data">
              <ul class="pagination d-flex">
                <li class="page-item" :class="{'disabled': lists.products.data.current_page == 1}">
                  <button type="button" class="page-link" @click="changeFilterWithPagiProduct(--lists.products.data.current_page)"> < Trước</button>
                </li>
                <li class="page-item" :class="{'disabled': lists.products.data.current_page == lists.products.data.last_page}">
                  <button type="button" class="page-link" @click="changeFilterWithPagiProduct(++lists.products.data.current_page)"> Sau > </button>
                </li>

                <li class="page-item disabled ml-auto d-flex">
                  <span class="mt-2"><b>@{{ lists.products.data.from }}</b> - <b>@{{ lists.products.data.to }}</b> trong tổng số <b>@{{ lists.products.data.total }}</b></span>
                </li>
              </ul>
            </nav> --}}

            <ul class="list-group" v-if="lists.products.data">
              <li class="list-group-item" v-for="product in lists.products.data" :key="product.id">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" :value="product" :id="'product-'+product.id" v-model="lists.products.choice">
                  <label class="form-check-label" :for="'product-'+product.id">
                    @{{ product.name }}
                  </label>
                </div>
              </li>
            </ul>

            <ul class="list-group" v-else>
              <li class="list-group-item">Không có dữ liệu</li>
            </ul>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng lại</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <ul class="list-group  mt-2">
    <li class="list-group-item"
        v-for="(product, index) in lists.products.choice"
        :key="product.id">
      <div class="form-check d-flex">
        <label class="form-check-label" :for="'product'+product.id">
          <i class="fa fa-cubes" aria-hidden="true"></i> @{{ product.desc.name }}
        </label>
        <a href="javascript:void(0)" class="ml-auto" @click="removeProduct(index)">x</a>
      </div>
    </li>
  </ul>
</div>
