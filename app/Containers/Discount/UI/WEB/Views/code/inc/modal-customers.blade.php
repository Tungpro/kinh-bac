<div class="mt-3" v-if="discount.object_use == 'personal'">
  <div class="d-flex">
    <input  type="text"
            class="form-control"
            placeholder="Tìm kiếm khách hàng"
            v-model="lists.customers.filter.keyword">

    <input type="hidden" name="customers_ids" id="customers_ids">

    <button type="button" class="btn btn-primary px-4 ml-2" @click="showModalKhachHang('#modalKhachHang')">
      Duyệt
    </button>

    <!-- Modal -->
    <div class="modal fade" id="modalKhachHang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Chọn khách hàng sử dụng mã giảm giá</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-header">
            <input  type="text"
                    class="form-control"
                    name="filter-groups"
                    placeholder="&#xF002; Lọc khách hàng (tên, SĐT, email)"
                    style="font-family: Helvetica, FontAwesome;"
                    v-model="lists.customers.filter.keyword">
          </div>

          <div class="modal-header" v-if="lists.customers.choice.length > 0">
            <div class="d-flex align-content-around flex-wrap">
              <div class="btn-group mr-2 mb-1" v-for="(customer, index) in lists.customers.choice" :key="customer.id">
                <button type="button" class="btn btn-outline-info btn-sm">@{{ customer.fullname || customer.email }}</button>
                <button type="button" class="btn btn-outline-info" @click="removeCustomer(customer)">x</button>
              </div>
            </div>
          </div>

          <div class="modal-body">
            <nav aria-label="Page navigation example" v-show="lists.customers.data.data">
              <ul class="pagination d-flex">
                <li class="page-item" :class="{'disabled': lists.customers.data.current_page == 1}">
                  <button type="button" class="page-link" @click="changeFilterWithPagi(--lists.customers.data.current_page)"> < Trước</button>
                </li>
                <li class="page-item" :class="{'disabled': lists.customers.data.current_page == lists.customers.data.last_page}">
                  <button type="button" class="page-link" @click="changeFilterWithPagi(++lists.customers.data.current_page)"> Sau > </button>
                </li>

                <li class="page-item disabled ml-auto d-flex">
                  <span class="mt-2"><b>@{{ lists.customers.data.from }}</b> - <b>@{{ lists.customers.data.to }}</b> trong tổng số <b>@{{ lists.customers.data.total }}</b></span>
                </li>
              </ul>
            </nav>

            <ul class="list-group" v-if="lists.customers.data.data">
              <li class="list-group-item" v-for="customer in lists.customers.data.data" :key="customer.id">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" :value="customer" :id="'customer-'+customer.id" v-model="lists.customers.choice">
                  <label class="form-check-label" :for="'customer-'+customer.id">
                    <span v-if="customer.fullname && customer.fullname.length > 1">
                      @{{ customer.fullname }}
                      <a :href="'mailto:'+ customer.email">(@{{ customer.email }})</a>
                    </span>

                    <span v-else><a :href="'mailto:'+ customer.email">@{{ customer.email }}</a></span>
                  </label>
                </div>
              </li>
            </ul>

            <ul class="list-group" v-else>
              <li class="list-group-item">Không có dữ liệu</li>
            </ul>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng lại</button>
            {{-- <button type="button"
                    class="btn btn-primary"
                    :disabled="lists.customers.choice.length == 0"
                    @click="saveCustomerToInput('#modalKhachHang')"
                    >
                    Lưu thông tin
            </button> --}}
          </div>
        </div>
      </div>
    </div>
  </div>

  <ul class="list-group  mt-2">
    <li class="list-group-item"
        v-for="customer in lists.customers.choice"
        :key="customer.id">
      <div class="form-check d-flex">
        <label class="form-check-label" :for="'customer'+customer.id">
          <i class="fa fa-user"></i>
          <span v-if="customer.fullname && customer.fullname.length > 1">
            @{{ customer.fullname }}
            <a :href="'mailto:'+ customer.email">(@{{ customer.email }})</a>
          </span>

          <span v-else><a :href="'mailto:'+ customer.email">@{{ customer.email }}</a></span>
        </label>

        <a href="javascript:void(0)" class="ml-auto" @click="removeCustomer(customer.id)">x</a>
      </div>
    </li>
  </ul>
</div>

