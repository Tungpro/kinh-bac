<?php

namespace App\Containers\Discount\UI\WEB\Controllers;


use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Discount\UI\WEB\Requests\GetAllHistoryDiscountCodeByIdRequest;
use App\Containers\Discount\UI\WEB\Requests\GetDiscountCodeVersionByIdRequest;
use App\Ship\Parents\Controllers\AdminController;

/**
 * Class DiscountCodeHistoryController
 *
 * @package App\Containers\Discount\UI\WEB\Controllers
 */
class DiscountCodeHistoryController extends AdminController
{
  public function __construct()
  {
    if (FunctionLib::isDontUseShareData(['all'])) {
      $this->dontUseShareData = true;
    }

    parent::__construct();
  }
  /**
   * Show all history change of code
   *
   * @param GetAllDiscountsRequest $request
   */
  public function all(GetAllHistoryDiscountCodeByIdRequest $request)
  {
    $transporter = $request->toTransporter();
    $discountCode = Apiato::call('Discount@FindDiscountByIdAction', [
      $transporter,
    ]);

    return view('discount::code.history', [
      'discountCode' => $discountCode
    ]);
  }

  public function getDiscountCodeVersion(GetDiscountCodeVersionByIdRequest $request) {
    $transporter = $request->toTransporter();
    $discountCode = Apiato::call('Discount@FindDiscountByIdAction', [
      $transporter,
    ]);

    $discountCodeVersion = $discountCode->getVersionModel($request->version_id);
    $discountCode->load(['versions' => function ($query) use ($request) {
      $query->where('version_id', $request->version_id);
    }]);

    return view('discount::code.history-detail', [
      'discountCode' => $discountCodeVersion,
      'input' => $request->all(),
      'versions' => $discountCode->versions
    ]);
  }
} // End class
