<?php

namespace App\Containers\Discount\UI\WEB\Controllers;

use Carbon\Carbon;
use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\WebController;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Containers\Discount\UI\WEB\Requests\EditDiscountRequest;
use App\Containers\Discount\UI\WEB\Requests\StoreDiscountRequest;
use App\Containers\Discount\UI\WEB\Requests\CreateDiscountRequest;
use App\Containers\Discount\UI\WEB\Requests\DeleteDiscountRequest;
use App\Containers\Discount\UI\WEB\Requests\UpdateDiscountRequest;
use App\Containers\Discount\UI\WEB\Requests\GetAllDiscountsRequest;
use App\Containers\Discount\UI\WEB\Requests\FindDiscountByIdRequest;
use App\Containers\Discount\UI\WEB\Requests\ToggleDiscountCodeStatusRequest;
use App\Ship\Transporters\DataTransporter;

/**
 * Class Controller
 *
 * @package App\Containers\Discount\UI\WEB\Controllers
 */
class DiscountCodeController extends AdminController
{
  use ApiResTrait;

  public function __construct()
  {
    if (FunctionLib::isDontUseShareData(['show'])) {
      $this->dontUseShareData = true;
    }

    parent::__construct();
  }

  /**
   * Show all entities
   *
   * @param GetAllDiscountsRequest $request
   */
  public function index(GetAllDiscountsRequest $request)
  {
    $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Mã giảm giá', route('admin.customers.index'));
    \View::share('breadcrumb', $this->breadcrumb);

    $transporter = $request->toTransporter();
    $discountsCode = Apiato::call('Discount@GetAllDiscountsCodeAction', [$transporter]);
    return view('discount::code.index', [
      'discountsCode' => $discountsCode,
      'input' => $request->all(),
      'config' => config('discount-container')
    ]);
  }

  /**
   * Show one entity
   *
   * @param FindDiscountByIdRequest $request
   */
  public function show(FindDiscountByIdRequest $request)
  {
    $transporter = $request->toTransporter();
    $discountCode = Apiato::call('Discount@FindDiscountByIdAction', [
      $transporter,
    ]);
    return view('discount::code.show', [
      'discountCode' => $discountCode,
      'input' => $request->all()
    ]);
  }

  /**
   * Create entity (show UI)
   *
   * @param CreateDiscountRequest $request
   */
  public function create(CreateDiscountRequest $request)
  {
    $this->breadcrumb[] = FunctionLib::addBreadcrumb('Giảm giá', route('admin.discount.code.index'));
    $this->breadcrumb[] = FunctionLib::addBreadcrumb('Tạo mã giảm giá');
    \View::share('breadcrumb', $this->breadcrumb);
    return view('discount::code.create');
  }

  /**
   * Add a new entity
   *
   * @param StoreDiscountRequest $request
   */
  public function store(StoreDiscountRequest $request)
  {
    $transporter = $request->toTransporter();
    $discountCode = Apiato::call('Discount@StoreDiscountAction', [$transporter]);
    return $this->sendResponse($discountCode, __('Tạo mã giảm giá thành công'));
  }

  /**
   * Edit entity (show UI)
   *
   * @param EditDiscountRequest $request
   */
  public function edit(EditDiscountRequest $request)
  {
    $this->breadcrumb[] = FunctionLib::addBreadcrumb('Giảm giá', route('admin.discount.code.index'));
    $this->breadcrumb[] = FunctionLib::addBreadcrumb('Sửa mã giảm giá');
    \View::share('breadcrumb', $this->breadcrumb);

    $transporter = $request->toTransporter();
    $discountCode = Apiato::call('Discount@FindDiscountByIdAction', [
      $transporter,
    ]);

    return view('discount::code.edit', [
      'discountCode' => $discountCode,
      'input' => $request->all(),
    ]);
  }

  /**
   * Update a given entity
   *
   * @param UpdateDiscountRequest $request
   */
  public function update(UpdateDiscountRequest $request)
  {
    $transporter = $request->toTransporter();
    $transporter->discount->user_id = auth()->id();
    $discountCode = Apiato::call('Discount@UpdateDiscountAction', [$transporter]);
    return $this->sendResponse($discountCode, __('Cập nhật mã giảm giá thành công'));
  }

  /**
   * Delete a given entity
   *
   * @param DeleteDiscountRequest $request
   */
  public function delete(DeleteDiscountRequest $request)
  {
    $result = Apiato::call('Discount@DeleteDiscountCodeAction', [$request]);

    return $this->sendResponse($request, __('Xóa mã giảm giá thành công'));
  }

  public function toggleStatus(ToggleDiscountCodeStatusRequest $request) {
    $discountCode = Apiato::call('Discount@ToggleDiscountCodeStatusAction', [$request->id, ['status' => (int) !$request->status]]);
    $html = view('discount::code.inc.item', ['code' => $discountCode])->render();
    return $this->sendResponse($html, __('Cập nhật trạng thái mã thành công'));
  }

  public function ajaxDescription(ToggleDiscountCodeStatusRequest $request){
    $discountDescription = Apiato::call('Discount@DiscountDescriptionAction', [$request->id]);
    return $this->sendResponse($discountDescription, __('Lấy dữ liệu mã thành công'));
  }
} // End class
