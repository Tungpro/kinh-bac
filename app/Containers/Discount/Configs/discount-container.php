<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Discount Container
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'discount_type' => [
      'freeship' => 'Miễn phí giao hàng',
      'percent' => 'Giảm %',
      'amount' => 'Giảm tiền'
    ],
    'object_use' => [
      '*' => 'Bất kỳ ai',
      // 'group' => 'Nhóm khách hàng',
      // 'personal' => 'Khách hàng riêng lẻ'
    ],
    'product_use' => [
      '*' => 'Sản phẩm bất kỳ',
      // 'collection' => 'Bộ sưu tập',
      // 'single_product' => 'Sản phẩm cụ thể'
    ],
];
