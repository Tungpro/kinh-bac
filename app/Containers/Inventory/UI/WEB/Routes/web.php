<?php

/** @var Route $router */
$router->get('inventories', [
    'as' => 'admin.inventory.index',
    'uses'  => 'InventoryController@index',
    'middleware' => [
      'auth:admin',
    ],
]);
