<?php

namespace App\Containers\Inventory\Tasks;

use App\Containers\Inventory\Data\Repositories\InventoryRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateInventoryTask extends Task
{

    protected $repository;

    public function __construct(InventoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
