<?php

namespace App\Containers\Inventory\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllInventoriesAction extends Action
{
    public function run(Request $request)
    {
        $inventories = Apiato::call('Inventory@GetAllInventoriesTask', [], ['addRequestCriteria']);

        return $inventories;
    }
}
