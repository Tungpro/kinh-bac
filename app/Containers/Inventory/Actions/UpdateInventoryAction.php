<?php

namespace App\Containers\Inventory\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateInventoryAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $inventory = Apiato::call('Inventory@UpdateInventoryTask', [$request->id, $data]);

        return $inventory;
    }
}
