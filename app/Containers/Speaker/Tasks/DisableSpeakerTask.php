<?php

namespace App\Containers\Speaker\Tasks;

use App\Containers\Speaker\Data\Repositories\SpeakerRepository;
use App\Ship\core\Foundation\BladeHelper;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DisableSpeakerTask extends Task
{
    protected $repository;

    public function __construct(SpeakerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->getModel()->where('id', $id)->update(['status' => BladeHelper::AN]);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
