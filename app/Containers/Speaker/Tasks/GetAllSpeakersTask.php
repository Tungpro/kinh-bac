<?php

namespace App\Containers\Speaker\Tasks;

use App\Containers\BaseContainer\Traits\ScopeAdminBaseSearchTrait;
use App\Containers\BaseContainer\Traits\ScopeFEAvailableDataTrait;
use App\Containers\Localization\Models\Language;
use App\Containers\Speaker\Data\Repositories\SpeakerRepository;
use App\Ship\core\Foundation\BladeHelper;
use App\Ship\Parents\Tasks\Task;

class GetAllSpeakersTask extends Task
{
    use ScopeAdminBaseSearchTrait;
    use ScopeFEAvailableDataTrait;

    protected $repository;

    public function __construct(SpeakerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($request, $limit = 20, Language $currentLang = null, $with = [], $limitNews = 10)
    {
        $language_id = $currentLang ? $currentLang->language_id : 1;

        if(array_search('news',$with)){
            $this->with([
                'news' => function ($query) use ($limitNews) {
                    $query->where('status',BladeHelper::YES)->limit($limitNews);
                },
                'news.desc' => function ($query) use ($language_id) {
                    $query->where('language_id', $language_id);
                }
             ]);
        }

        return $this->with([
            'desc' => function ($query) use ($language_id) {
                $query->where('language_id', $language_id);
            },
        ])->returnDataByLimit($limit);

    }

}
