<?php

namespace App\Containers\Speaker\Tasks;

use App\Containers\Speaker\Data\Repositories\SpeakerRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteSpeakerTask extends Task
{
    protected $repository;

    public function __construct(SpeakerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            $examCategory = $this->repository->find($id);

            return $examCategory->delete();
        } catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
