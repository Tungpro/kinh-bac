<?php

namespace App\Containers\Speaker\Tasks;

use App\Containers\Speaker\Data\Repositories\SpeakerRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindSpeakerByIdTask extends Task
{
    protected $repository;

    public function __construct(SpeakerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        } catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
