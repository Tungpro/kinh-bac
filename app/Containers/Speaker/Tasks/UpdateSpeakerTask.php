<?php

namespace App\Containers\Speaker\Tasks;

use App\Containers\Speaker\Data\Repositories\SpeakerRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateSpeakerTask extends Task
{
    protected $repository;

    public function __construct(SpeakerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        } catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
