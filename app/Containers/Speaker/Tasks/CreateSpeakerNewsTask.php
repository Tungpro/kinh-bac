<?php

namespace App\Containers\Speaker\Tasks;

use App\Containers\Speaker\Data\Repositories\SpeakerRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateSpeakerNewsTask extends Task
{
    protected $repository;

    public function __construct(SpeakerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data,$speaker)
    {
        try {
            $speaker_news = $data['speaker_news'] ?? [];
            if (!empty($speaker_news)) {
                $speaker->news()->sync($speaker_news);
            }else {
                $speaker->news()->sync([]);
            }

        } catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
