<?php

namespace App\Containers\Speaker\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\StringLib;
use App\Containers\BaseContainer\Traits\AdminBaseCRUDTrait;
use App\Containers\File\Actions\UploadImageAction;
use App\Containers\Media\Models\Media;
use App\Containers\Speaker\UI\WEB\Requests\CreateSpeakerRequest;
use App\Containers\Speaker\UI\WEB\Requests\DeleteSpeakerRequest;
use App\Containers\Speaker\UI\WEB\Requests\GetAllSpeakersRequest;
use App\Containers\Speaker\UI\WEB\Requests\UpdateSpeakerRequest;
use App\Containers\Speaker\UI\WEB\Requests\StoreSpeakerRequest;
use App\Containers\Speaker\UI\WEB\Requests\EditSpeakerRequest;
use App\Containers\Speaker\UI\WEB\Requests\UpdateStatusSpeakerRequest;
use App\Ship\Parents\Controllers\AdminController;
use Apiato\Core\Foundation\Facades\Apiato;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class Controller extends AdminController
{
    use AdminBaseCRUDTrait;
    protected $containerName = 'speaker';

    public function __construct()
    {
        $this->title = 'Ban lãnh đạo';
        $this->imageKey = 'speaker';
        $this->actions = [
            'list' => 'Speaker@GetAllSpeakersAction',
            'create' => 'Speaker@CreateSpeakerAction',
            'edit' => 'Speaker@FindSpeakerByIdAction',
            'update' => 'Speaker@UpdateSpeakerAction',
            'delete' => 'Speaker@DeleteSpeakerAction',

        ];
        $this->routes = [
            'list' => 'admin.speaker.index',
            'create' => 'admin.speaker.create',
            'store' => 'admin.speaker.store',
            'edit' => 'admin.speaker.edit',
            'update' => 'admin.speaker.update',
            'destroy' => 'admin.speaker.destroy',
            'disable' => "admin.$this->containerName.disable",
            'enable' => "admin.$this->containerName.enable",
        ];
        View::share('routes', $this->routes);
        View::share('containerName', $this->containerName);

        parent::__construct();
    }

    public function index(GetAllSpeakersRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);

        $request->merge(['scopeAdminBaseSearch' => true]);
        $data = Apiato::call($this->actions['list'], [$request, $this->perPage, null, ['desc']]);

        return view("$this->containerName::Admin.index", [
            'request' => $request,
            'data' => $data,
        ]);
    }

    public function create(CreateSpeakerRequest $request)
    {
        $this->showAddForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['add', $this->title, $this->routes['list']]);

        return view("$this->containerName::Admin.form", []);
    }

    public function store(StoreSpeakerRequest $request)
    {
        $this->editMode = false;
        return $this->save($request);
    }

    public function edit($id, EditSpeakerRequest $request)
    {
        $this->showEditForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['edit', $this->title, $this->routes['list']]);

        $editedObj = Apiato::call($this->actions['edit'], [$id, ['all_desc','news','news.desc']]);

        if ($editedObj) {

            return view("$this->containerName::Admin.form", [
                'data' => $editedObj,
            ]);
        }

        return redirect()->route($this->routes['list']);
    }

    public function update(UpdateSpeakerRequest $request)
    {
        $this->editMode = true;
        return $this->save($request);
    }

    public function save($request)
    {
        try {
            $tranporter = $request->all();

            $this->beforeSave($request, $tranporter);

            $object = Apiato::call($this->actions[$this->editMode ? 'update' : 'create'], [$tranporter]);

            if ($object) {
                $this->afterSave($request);

                $url = $this->editMode ? route($this->routes['edit'] ?? $this->routes['update'], [$object->id]) : route($this->routes['list']);
                $msg = $this->editMode ? 'Cập nhật bản ghi thành công!' : 'Thêm mới bản ghi thành công!';

                return redirect()->to($url)->with('status', $msg);
            }
        } catch (Exception $e) {

            return redirect()->back()->withInput()->withErrors(['error:' => 'Đã xảy ra lỗi! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function destroy($id, DeleteSpeakerRequest $request)
    {
        try {

            Apiato::call($this->actions['delete'], [$id]);
            return FunctionLib::ajaxRespondV2(true, 'Xóa bản ghi thành công!');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình xóa dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function enable(UpdateStatusSpeakerRequest $request)
    {
        try {
            Apiato::call('Speaker@EnableSpeakerAction', [$request]);

            return FunctionLib::ajaxRespondV2(true, 'Hiện bản ghi thành công!');
        } catch (Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình sửa dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function disable(UpdateStatusSpeakerRequest $request)
    {
        try {
            Apiato::call('Speaker@DisableSpeakerAction', [$request]);

            return FunctionLib::ajaxRespondV2(true, 'Ẩn bản ghi thành công!');
        } catch (Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình sửa dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }
    /*override*/
    public function beforeSave($request, &$transporter)
    {
        parent::beforeSave($request, $transporter);
        $this->uploadImageByField($request,$transporter,'about_image');
    }

    public function uploadImageByField($request, &$transporter, $field)
    {
        if (!empty($transporter["delete_{$field}"]) && $transporter["delete_{$field}"] === '1') {
            $transporter[$field] = null;
        }
        if (isset($request->$field)) {
            $image = app(UploadImageAction::class)->run($request, $field, $this->imageKey.$field, $this->imageKey);

            if (!empty($image) && isset($image['error']) && $image['error']) {
                return redirect()->back()->withInput()->withErrors(['error:' => $image['msg']]);
            }
            $transporter[$field] = $image['fileName'];
        }
    }
}
