<?php

/** @var Route $router */
$router->group([
    'middleware' => ['auth:admin'],
    'namespace' => '\App\Containers\Speaker\UI\WEB\Controllers\Admin',
    'domain' => sprintf('admin.%s', parse_url(config('app.url'))['host'])
], function () use ($router) {
    $router->resource('dien-gia', 'Controller')
        ->names([
            'index' => 'admin.speaker.index',
            'create' => 'admin.speaker.create',
            'store' => 'admin.speaker.store',
            'edit' => 'admin.speaker.edit',
            'update' => 'admin.speaker.update',
            'destroy' => 'admin.speaker.destroy',
        ])
        ->except(['show']);

    $router->post('/dien-gia/enable/{id}', ['as' => 'admin.speaker.enable', 'uses' => 'Controller@enable',]);
    $router->post('/dien-gia/disable/{id}', ['as' => 'admin.speaker.disable', 'uses' => 'Controller@disable',]);

});
