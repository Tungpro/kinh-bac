@extends('basecontainer::admin.layouts.default')

@section('content')
    @if (isset($editMode) && $editMode)
        {!! Form::open(['url' => route($routes['update'], $data->id), 'files' => true]) !!}
        <input type="hidden" name="id" value="{{$data->id}}"/>
        @method('PUT')
    @else
        {!! Form::open(['url' => route($routes['store']), 'files' => true]) !!}
    @endif

    <div class="row">
        <div class="col-12">
            @include('basecontainer::admin.inc.alert')
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card card-accent-primary">
                <div class="card-header">
                    <i class="fa fa-pencil"></i>THÔNG TIN
                </div>
                <div class="card-body">
                    <div class="tabbable boxed parentTabs">
                        <ul class="nav nav-tabs nav-underline nav-underline-primary mb-3">
                            <li class="nav-item">
                                <a class="nav-link active" href="#general"><i class="fa fa-empire"></i> Chung</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#data"><i class="fa fa-deviantart"></i> Thông tin chi tiết</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#links"><i class="fa fa-link"></i> Liên kết</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#image"><i class="fa fa-image"></i> Hình ảnh</a>
                            </li>
                        </ul>

                        <div class="tab-content p-0">

                            @include('speaker::Admin.edit_tabs.general')


                            @include('speaker::Admin.edit_tabs.detail')

                            @include('speaker::Admin.edit_tabs.links')

                            @include('speaker::Admin.edit_tabs.image')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('basecontainer::admin.inc.form-btn-submit')
    {!! Form::close() !!}
@stop

@section('js_bot')
    <script type="text/javascript">
        $("ul.nav-tabs a").click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

    </script>
@stop
@push('js_bot_all')
    {!! \FunctionLib::addMedia('admin/js/library/tinymce/tinymce.min.js') !!}

    <script type="text/javascript">
        admin.system.tinyMCE('.__editor', plugins = '', menubar = '', toolbar = '', height = 600);
    </script>
@endpush
