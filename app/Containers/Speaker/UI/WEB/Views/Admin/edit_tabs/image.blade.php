<div class="tab-pane" id="image">
    <div class="tabbable">
        @include("basecontainer::admin.inc.edit_tabs.image-single", ['imageSingleColClass' => 'col-lg-6', 'imageSingleTittle' => 'Ảnh đại diện', 'imageSingleKey' => 'image' ])

        @include("basecontainer::admin.inc.edit_tabs.image-single", ['imageSingleColClass' => 'col-lg-6', 'imageSingleTittle' => 'Ảnh giới thiệu', 'imageSingleKey' => 'about_image' ])

    </div>
</div>

