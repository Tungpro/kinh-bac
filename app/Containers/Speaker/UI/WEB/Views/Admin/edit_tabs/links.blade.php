
<div class="tab-pane" id="links">
    <div class="tabbable">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="sort_order">Bài viết</label>
                    <div class="input-group">
                        <input type="text" placeholder="Nhập để tìm bài viết" id="input-news" class="form-control" />
                    </div>
                </div>


                <div class="form-group">
                    <div id="speaker_news" class="row mx-0 list-group flex-row">
                        @if(isset($data->news))
                            @foreach($data->news as $item)
                                <a href="#" id="category-{{$item->category_id}}" class="list-group-item list-group-item-action">
                                    <i class="fa fa-close"></i> {!! @$item->desc->name !!}
                                    <input type="hidden" name="speaker_news[]" value="{{$item->id}}" />
                                </a>
                                @endforeach
                                @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    @push('js_bot_all')
        <script>
            $('input#input-news').autocomplete({
                classes: {
                    "ui-autocomplete": "dropdown-menu"
                },
                'source': function(request, response) {
                    $.ajax({
                        global: false,
                        url: "{{route('admin_api_get_news')}}",
                        dataType: 'json',
                        headers: ENV.headerParams,
                        data: {
                            type: 'select2',
                            name: request.term,
                            _token: ENV.token
                        },
                        success: function(json) {
                            response($.map(json.data.data, function(item) {
                                return {
                                    label: item.desc.name,
                                    value: item.desc.name,
                                    id: item.id
                                }
                            }));
                        }
                    });
                },
                'select': function(event, ui) {
                    var item = ui.item;

                    $('#category-' + item.id).remove();

                    var html = '<a href="#" id="category-' + item.id + '" class="list-group-item list-group-item-action">\n' +
                        '                        <i class="fa fa-close"></i> ' + item.label + '\n' +
                        '                        <input type="hidden" name="speaker_news[]" value="' + item.id + '" />\n' +
                        '                    </a>';

                    $('#speaker_news').append(html);

                    $('input#input-news').val('');
                    return false;
                }
            });

            $('#speaker_news').delegate('.fa-close', 'click', function() {
                $(this).parent().remove();
            });
        </script>
@endpush