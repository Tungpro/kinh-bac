<div class="tab-pane active" id="general">
    <div class="tabbable">
        <ul class="nav nav-tabs nav-underline nav-underline-primary mb-3" role="tablist">
            @foreach($langs as $it_lang)
                <li class="nav-item">
                    <a class="nav-link {{$loop->first ? 'active' : ''}}" href="#lang_{{$it_lang['language_id']}}">
                        <img src="{{ asset('admin/img/lang/'.$it_lang['image']) }}"
                             title="{{$it_lang['name']}}"> {{$it_lang['name']}}
                    </a>
                </li>
            @endforeach
        </ul>

        <div class="tab-content p-0">
            @foreach($langs as $it_lang)
                <div class="tab-pane {{$loop->first ? 'active' : ''}}" id="lang_{{$it_lang['language_id']}}">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name_{{$it_lang['language_id']}}">Tên Ban lãnh đạo <span
                                            class="small text-danger">({{$it_lang['name']}})</span> <span
                                            class="text-danger">*</span></label>
                                <div class="input-group">
                                    @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
                                    <input type="text"
                                           class="form-control {{ $errors->has("description.{$it_lang['language_id']}.name") ? 'is-invalid' : '' }}"
                                           name="description[{{$it_lang['language_id']}}][name]"
                                           id="name_{{$it_lang['language_id']}}"
                                           placeholder="Tên Ban lãnh đạo"
                                           value="{{ old("description.{$it_lang['language_id']}.name", @$data['all_desc'][$it_lang['language_id']]['name']) }}"
                                    >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name_{{$it_lang['language_id']}}">Tiêu đề giới thiệu<span
                                        class="small text-danger">({{$it_lang['name']}})</span> <span
                                        class="text-danger">*</span></label>
                                <div class="input-group">
                                    @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
                                    <input type="text"
                                           class="form-control {{ $errors->has("description.{$it_lang['language_id']}.name_two") ? 'is-invalid' : '' }}"
                                           name="description[{{$it_lang['language_id']}}][name_two]"
                                           id="name_{{$it_lang['language_id']}}"
                                           placeholder="Tiêu đề giới thiệu"
                                           value="{{ old("description.{$it_lang['language_id']}.name_two", @$data['all_desc'][$it_lang['language_id']]['name_two']) }}"
                                    >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="description_{{ $it_lang['language_id'] }}">Chức vụ<span
                                            class="small text-danger">({{ $it_lang['name'] }})</span> <span></span></label>
                                <div class="input-group">
                                    @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
                                    <input type="text"
                                           class="form-control {{ $errors->has("description.{$it_lang['language_id']}.position") ? 'is-invalid' : '' }}"
                                           name="description[{{$it_lang['language_id']}}][position]"
                                           id="position_{{$it_lang['language_id']}}"
                                           placeholder="Chức vụ"
                                           value="{{ old("description.{$it_lang['language_id']}.position", @$data['all_desc'][$it_lang['language_id']]['position']) }}"></input>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-8">
                            <div class="form-group">
                                <label for="description_{{ $it_lang['language_id'] }}">Mô tả <span
                                        class="small text-danger">({{ $it_lang['name'] }})</span></label>
                                <div class="input-group">
                                    @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
                                    <textarea name="description[{{ $it_lang['language_id'] }}][short_description]"
                                              class="editor_desc form-control {{ $errors->has("description.{$it_lang['language_id']}.short_description") ? 'is-invalid' : '' }}"
                                              id="short_description_{{ $it_lang['language_id'] }}"
                                              placeholder='Mô tả' rows="10"
                                    >{!! old("description.{$it_lang['language_id']}.short_description", @$data['all_desc'][$it_lang['language_id']]['short_description']) !!}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="description_{{ $it_lang['language_id'] }}">Nội dung <span
                                        class="small text-danger">({{ $it_lang['name'] }})</span></label>
                                <div class="input-group">
                                    @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
                                    <textarea name="description[{{ $it_lang['language_id'] }}][description]"
                                              class="__editor form-control {{ $errors->has("description.{$it_lang['language_id']}.description") ? 'is-invalid' : '' }}"
                                              id="description_{{ $it_lang['language_id'] }}"
                                              rows="7"
                                    >{!! old("description.{$it_lang['language_id']}.description", @$data['all_desc'][$it_lang['language_id']]['description']) !!}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--@include('basecontainer::admin.inc.edit_tabs.meta-tag')--}}
                </div>

            @endforeach
        </div>
    </div>
</div>