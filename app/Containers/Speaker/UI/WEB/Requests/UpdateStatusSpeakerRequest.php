<?php

namespace App\Containers\Speaker\UI\WEB\Requests;

use App\Containers\Speaker\Models\Speaker;
use App\Ship\Parents\Requests\Request;

class UpdateStatusSpeakerRequest extends Request
{
    protected $access = [
        'permissions' => 'speaker-edit',
        'roles' => 'admin',
    ];

    protected $decode = [];

    protected $urlParameters = [
        'id',
    ];

    public function rules()
    {
        return [
            'id' => ['required', sprintf('exists:%s,id', Speaker::TABLE_NAME)],

        ];
    }

    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
