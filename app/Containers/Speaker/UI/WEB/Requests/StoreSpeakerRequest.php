<?php

namespace App\Containers\Speaker\UI\WEB\Requests;

use App\Containers\BaseContainer\Traits\RequestBaseLanguage;
use App\Ship\Parents\Requests\Request;

class StoreSpeakerRequest extends Request
{
    use RequestBaseLanguage ;
    protected $access = [
        'permissions' => 'speaker-create',
        'roles' => 'admin',
    ];

    protected $decode = [
        // 'id',
    ];

    protected $urlParameters = [
        // 'id',
    ];

    public function rules()
    {
        return [
//            'description.*.name' => 'bail|required|string',
            'description.*.description' => 'bail|nullable|string',

            'status' => 'bail|nullable|numeric|in:1,2',
            'sort_order' => 'bail|nullable|numeric|min:0',

            'birth_year' => 'bail|nullable|numeric|min:0',
            'email' => 'bail|nullable|email:rfc,filter|min:0|max:100',
            'image' => 'bail|nullable|mimes:jpeg,jpg,png|max:15360',
        ];
    }

    public function attributes()
    {
        return [
            'status' => 'Trạng thái',
            'sort_order' => 'Sắp xếp',

            'birth_year' => 'Năm sinh',
            'email' => 'Email',
            'image' => 'Hình ảnh'
        ] + $this->messagesLang([
//            'description.*.name' => 'Tên Ban lãnh đạo language',
            'description.*.description' => 'Các chức danh language',
        ]);
    }

    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
