<?php

namespace App\Containers\Speaker\UI\WEB\Requests;

use App\Containers\BaseContainer\Traits\RequestBaseLanguage;
use App\Containers\Customer\Models\Customer;
use App\Containers\Speaker\Models\Speaker;
use App\Ship\Parents\Requests\Request;

/**
 * Class UpdateSpeakerRequest.
 */
class UpdateSpeakerRequest extends Request
{
    use RequestBaseLanguage ;
    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    // protected $transporter = \App\Ship\Transporters\DataTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => 'speaker-edit',
        'roles' => 'admin',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        // 'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'id' => ['required', sprintf('exists:%s,id', Speaker::TABLE_NAME)],

//            'description.*.name' => 'bail|required|string',
            'description.*.description' => 'bail|nullable|string',

            'status' => 'bail|nullable|numeric|in:1,2',
            'sort_order' => 'bail|nullable|numeric|min:0',

            'birth_year' => 'bail|nullable|numeric|min:0',
            'email' => 'bail|nullable|email:rfc,filter|min:0|max:100',
            'image' => 'bail|nullable|mimes:jpeg,jpg,png|max:15360',
        ];
    }

    public function attributes()
    {
        return [
            'status' => 'Trạng thái',
            'sort_order' => 'Sắp xếp',

            'birth_year' => 'Năm sinh',
            'email' => 'Email',
            'image' => 'Hình ảnh'
        ] + $this->messagesLang([
//            'description.*.name' => 'Tên Ban lãnh đạo language',
            'description.*.description' => 'Câu trả lời language',
        ]);
    }

    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
