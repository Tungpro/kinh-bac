<?php

namespace App\Containers\Speaker\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class SpeakerRepository
 */
class SpeakerRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
