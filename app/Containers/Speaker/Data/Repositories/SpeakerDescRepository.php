<?php

namespace App\Containers\Speaker\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class SpeakerDescRepository
 */
class SpeakerDescRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
