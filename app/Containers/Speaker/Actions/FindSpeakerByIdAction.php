<?php

namespace App\Containers\Speaker\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class FindSpeakerByIdAction extends Action
{
    public function run($id, array $withData = [], array $conditions = [], $selectFields = ['*'])
    {
        return Apiato::call('Speaker@FindSpeakerByIdTask', [$id], [
            ['with' => [$withData]],
            ['where' => [$conditions]],
            ['selectFields' => [$selectFields]]
        ]);
    }
}
