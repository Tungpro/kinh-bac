<?php

namespace App\Containers\Speaker\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class FindSpeakerByConditionsAction extends Action
{
    public function run(array $conditions = [], array $withData = [], $selectFields = ['*'])
    {
        return Apiato::call('Speaker@FindSpeakerByConditionsTask', [], [
            ['where' => [$conditions]],
            ['with' => [$withData]],
            ['selectFields' => [$selectFields]]
        ]);
    }
}
