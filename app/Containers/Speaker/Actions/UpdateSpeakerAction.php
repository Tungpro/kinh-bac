<?php

namespace App\Containers\Speaker\Actions;

use App\Containers\Speaker\Data\Repositories\SpeakerDescRepository;
use App\Containers\Speaker\Models\Speaker;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;

class UpdateSpeakerAction extends Action
{
    public function run($request)
    {
        $data = Arr::only($request, [
            'id', 'description', 'status', 'sort_order', 'birth_year', 'email', 'image', 'speaker_news', 'is_hot', 'position', 'about_image'
        ]);
        $data['updated_by'] = auth()->id();

        $objBefore = Apiato::call('Speaker@FindSpeakerByIdTask', [$data['id']], [['with' => [['all_desc']]]]);
        $objUpdated = Apiato::call('Speaker@UpdateSpeakerTask', [$data['id'], Arr::except($data, ['description'])]);

        if ($objUpdated) {
            $originalDesc = Apiato::call('BaseContainer@Admin\GetAllModelDescTask', [app(SpeakerDescRepository::class), $objUpdated->id, 'speaker_id']);
            //news
            Apiato::call('Speaker@CreateSpeakerNewsTask', [$data,$objUpdated]);

            Apiato::call('BaseContainer@Admin\SaveModelDescTask', [app(SpeakerDescRepository::class), $data, $originalDesc, $objUpdated->id, 'speaker_id']);

            // Create Log
            Apiato::call('User@CreateUserLogSubAction', [
                $objUpdated->id,
                $objBefore->toArray(),
                $objUpdated->load('all_desc')->toArray(),
                'Sửa Speaker',
                Speaker::class
            ]);
        }

        $this->clearCache();

        return $objUpdated;
    }
}
