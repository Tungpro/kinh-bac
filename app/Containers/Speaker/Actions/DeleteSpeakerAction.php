<?php

namespace App\Containers\Speaker\Actions;

use App\Containers\Speaker\Data\Repositories\SpeakerDescRepository;
use App\Containers\Speaker\Models\Speaker;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteSpeakerAction extends Action
{
    public function run($id)
    {
        $deleted = Apiato::call('Speaker@DeleteSpeakerTask', [$id]);

        if ($deleted) {
            Apiato::call('BaseContainer@Admin\DeleteModelDescTask', [app(SpeakerDescRepository::class), $id, 'speaker_id']);

            Apiato::call('User@CreateUserLogSubAction', [
                $id,
                [],
                [],
                'Xóa Ban lãnh đạo',
                Speaker::class
            ]);
            $this->clearCache();

            return $deleted;
        }

        return false;
    }
}
