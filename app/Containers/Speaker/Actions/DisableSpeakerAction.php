<?php

namespace App\Containers\Speaker\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Speaker\Models\Speaker;
use App\Ship\Parents\Actions\Action;

class DisableSpeakerAction extends Action
{
    public function run($data)
    {
        $object = Apiato::call('Speaker@DisableSpeakerTask', [$data->id]);

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            [],
            [],
            'Ẩn Ban lãnh đạo',
            Speaker::class
        ]);

        $this->clearCache();

        return $object;
    }
}
