<?php

namespace App\Containers\Speaker\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

class GetAllSpeakersAction extends Action
{
    public function run($filters,$limit = 10,array $with = ['desc'],$skipPagination = false,$currentPage = 1)
    {
             $data = Apiato::call('Speaker@GetAllSpeakersTask', [$filters, $limit],[
                 ['with' => [$with]],
             ]);
            return $data;
    }
}
