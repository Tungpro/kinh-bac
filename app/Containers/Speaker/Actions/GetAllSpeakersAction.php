<?php

namespace App\Containers\Speaker\Actions;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllSpeakersAction extends Action
{
    public function run($request, $limit = 20,
                        ?Language $currentLang = null,
                        $with = [], $conditions = [], $withNews = 10, $selectFields = ['*'], $orderBy = ['sort_order' => 'ASC', 'id' => 'DESC']
    )
    {
        return $this->remember(function () use ($request, $limit, $currentLang, $with, $conditions, $selectFields, $orderBy, $withNews) {

            return Apiato::call('Speaker@GetAllSpeakersTask', [$request, $limit, $currentLang, $with, $withNews], [
                ['currentLang' => [$currentLang]],

                ['with' => [$with]],
                ['where' => [$conditions]],
                ['selectFields' => [$selectFields]],
                ['orderBy' => [$orderBy]],

                ['scopeAdminBaseSearch' => [$request]],
                ['scopeFEAvailableData' => [$request]],
            ]);
        }, null, [], 0, $request->skipCache ?? true);
    }
}
