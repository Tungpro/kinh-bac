<?php

namespace App\Containers\Speaker\Actions;

use App\Containers\Speaker\Data\Repositories\SpeakerDescRepository;
use App\Containers\Speaker\Models\Speaker;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;

class CreateSpeakerAction extends Action
{
    public function run($request)
    {
        $data = Arr::only($request, [
            'description', 'status', 'sort_order', 'birth_year', 'email', 'image', 'speaker_news' ,'is_hot', 'position', 'about_image'
        ]);
        $data['created_by'] = auth()->id();

        $objCreated = Apiato::call('Speaker@CreateSpeakerTask', [Arr::except($data, ['description'])]);


        if ($objCreated) {
            // Create Desc
            Apiato::call('BaseContainer@Admin\SaveModelDescTask', [app(SpeakerDescRepository::class), $data, [], $objCreated->id, 'speaker_id']);
            //news
            Apiato::call('Speaker@CreateSpeakerNewsTask', [$data,$objCreated]);
            // Create Log
            Apiato::call('User@CreateUserLogSubAction', [
                $objCreated->id,
                [],
                $objCreated->load('all_desc')->toArray(),
                'Tạo Ban lãnh đạo',
                Speaker::class
            ]);
        }

        $this->clearCache();

        return $objCreated;
    }
}
