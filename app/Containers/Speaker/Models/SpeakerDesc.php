<?php

namespace App\Containers\Speaker\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class SpeakerDesc extends Model
{
    const TABLE_NAME = 'speaker_desc';
    protected $table = self::TABLE_NAME;

    protected $fillable = [
        'speaker_id', 'language_id', 'name', 'description','short_description', 'name_two'
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'speaker_desc';

    public function language()
    {
        return $this->hasOne(Language::class, 'language_id', 'language_id');
    }

}
