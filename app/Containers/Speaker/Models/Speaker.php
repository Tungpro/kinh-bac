<?php

namespace App\Containers\Speaker\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Containers\BaseContainer\Traits\BaseLanguage;
use App\Containers\News\Models\News;
use App\Ship\core\Traits\HelpersTraits\LangTrait;
use App\Ship\Parents\Models\Model;
use Illuminate\Support\Str;

class Speaker extends Model
{
    use LangTrait;
    use BaseLanguage;

    const TABLE_NAME = 'speaker';
    protected $table = self::TABLE_NAME;

    protected $fillable = [
        'status', 'sort_order', 'birth_year', 'email', 'image', 'created_by', 'updated_by', 'is_hot' ,'position' , 'about_image'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $resourceKey = 'speaker';

    /*relationship*/
    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', SpeakerDesc::class, 'speaker_id', 'id');
    }

    public function desc()
    {
        return $this->hasOne(SpeakerDesc::class, 'speaker_id', 'id');
    }

    public function news(){
        return $this->belongsToMany(News::class, SpeakerNews::getTableName(), 'speaker_id', 'news_id')->orderBy('sort_order','ASC');
    }


    /*function */
    public function getImageUrl($size = 'small',$column = 'image')
    {
        return ImageURL::getImageUrl($this->$column, 'speaker', $size);
    }


    public function getPositionArr()
    {
        if (!empty($this->desc->description)) {
            return explode('|', $this->desc->description);
        }
        return [];
    }

    public function link(){
            $slug = Str::slug($this->desc->name);
            return !empty($slug) ? route('web.about.speaker', ['slug' => $this->slug(null,$this->desc->name,$this->desc->language_id) , 'id' => $this->id]) : false ;
    }
}
