<?php

namespace App\Containers\Speaker\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Containers\News\Models\News;
use App\Ship\core\Traits\HelpersTraits\LangTrait;
use App\Ship\Parents\Models\Model;

class SpeakerNews extends Model
{
    use LangTrait;

    const TABLE_NAME = 'speaker_news';
    protected $table = self::TABLE_NAME;

    protected $primaryKey = ['news_id', 'speaker_id'];

}
