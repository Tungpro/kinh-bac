<?php


namespace App\Containers\Media\Models;


use Apiato\Core\Foundation\ImageURL;
use Apiato\Core\Foundation\StringLib;
use App\Ship\Parents\Models\Model;

class Media  extends Model
{
    protected $table = 'images';
    protected $primaryKey = 'id';


    protected $fillable = [
        'id',
        'image',
        'type',
        'model',
        'sort_order',
        'user_id'
    ];

    public function getImageUrl($size = 'small')
    {
        return ImageURL::getImageUrl($this->image, StringLib::getClassNameFromString(Media::class), $size);
    }
}