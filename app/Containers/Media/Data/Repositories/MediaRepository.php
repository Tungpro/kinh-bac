<?php

namespace App\Containers\Media\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class NewsRepository.
 */
class MediaRepository extends Repository
{
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}

