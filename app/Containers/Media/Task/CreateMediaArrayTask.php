<?php


namespace App\Containers\Media\Task;


use App\Containers\Media\Data\Repositories\MediaRepository;
use App\Containers\Media\Models\Media;
use App\Containers\ReProject\Data\Repositories\ReProjectMediaRepository;
use App\Containers\ReProject\Models\ReProject;
use App\Ship\Exceptions\UpdateResourceFailedException;

class CreateMediaArrayTask
{

    private $mediaRepository;

    public function __construct(MediaRepository $mediaRepository)
    {
        $this->mediaRepository = $mediaRepository;
    }


    public function run($object, array $oldImageNames = [], array $uploadImageNames = [], int $mediaType = 1, $model = 'Model')
    {
        try {
            $sortOrder = 0;
            $insertNewMedias = [];
            $updateOrderOldMedias = [];
            $media = $object->medias;
            $existsMedias = $media->pluck('id', 'image')->toArray();

            if (!empty($oldImageNames) && is_array($oldImageNames)) {
                foreach ($oldImageNames as $key => $oldImageName) {
                    if (!empty($oldImageName)) {
                        // update order old media
                        $updateOrderOldMedias[$existsMedias[$oldImageName]] = [
                            'sort_order' => $sortOrder,
                            'updated_at' => now(),
                        ];

                    } else {
                        // insert new media
                        $insertNewMedias[] = [
                            'type' => $mediaType,
                            'model' => $model,
                            'object_id' => $object->id,
                            'image' => array_shift($uploadImageNames),
                            'sort_order' => $sortOrder,
                            'created_at' => now(),
                        ];
                        unset($oldImageNames[$key]);
                    }
                    $sortOrder++;
                }
                unset($oldImageName);
            }

            if (!empty($oldImageNames)) {
                $object->medias()->whereNotIn('image', $oldImageNames)->delete();
            } else {
                $object->medias()->delete();
            }

            if (!empty($updateOrderOldMedias)) {
                $this->mediaRepository->updateMultiple($updateOrderOldMedias);
            }

            if (!empty($insertNewMedias)) {
                return $object->medias()->insert($insertNewMedias);
            }

            return true;
        } catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}