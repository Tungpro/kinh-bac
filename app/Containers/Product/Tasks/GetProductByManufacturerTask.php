<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-03 13:48:24
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-12 15:17:38
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Tasks;

use App\Containers\Localization\Models\Language;
use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Containers\Product\Models\Product;
use App\Ship\Criterias\Eloquent\MustHaveDescCriteria;
use App\Ship\Parents\Tasks\Task;

class GetProductByManufacturerTask extends Task
{

    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    public function run(int $manufacturer_id, $limit)
    {
        $language_id = $this->currentLang ? $this->currentLang->language_id : 1;

        // dd( $language_id);

        $this->repository->with([
            'desc'  => function ($query) use ($language_id) {
                $query->activeLang($language_id);
            },
            'images',
            'specialOffers.desc' => function ($query) use ($language_id) {
                $query->select('id', 'special_offer_id', 'name');
                $query->whereHas('language', function ($q) use ($language_id) {
                    $q->where('language_id', $language_id);
                });
            },
            'specialTags' => function($q) use($language_id){
                $q->mustHaveDesc($language_id);
            },
            'specialTags.desc' => function($q) use($language_id){
                $q->activeLang($language_id);
            },
            'manufacturer'
        ]);
        $this->repository->where('manufacturer_id', $manufacturer_id);
        return $this->repository->paginate($limit);
    }
}
