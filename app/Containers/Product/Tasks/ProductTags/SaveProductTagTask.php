<?php

namespace App\Containers\Product\Tasks\ProductTags;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Tags\Data\Repositories\TagDetailsRepository;
/**
 * Class SaveProductCategoryTask.
 */
class SaveProductTagTask extends Task
{
    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function __construct(TagDetailsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($product_tags,$prd_id)
    {
        return $this->repository->saveAndAddTagLabel($product_tags,$prd_id);
    }
}
