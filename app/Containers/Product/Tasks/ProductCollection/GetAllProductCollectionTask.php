<?php

namespace App\Containers\Product\Tasks\ProductCollection;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Product\Data\Criterias\LikeNameCriteria;
use App\Containers\Product\Data\Repositories\ProductCollectionRepository;

class GetAllProductCollectionTask extends Task
{

  protected $repository;

  public function __construct(ProductCollectionRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run()
  {
    return $this->repository->scopeQuery(function ($query) {
      return $query->orderBy('id', 'DESC');
    })->paginate();
  }

  public function filterByName($transporter) {
    if (!empty($transporter->keyword)) {
     $this->repository->pushCriteria(new LikeNameCriteria($transporter->keyword));
    }
  }
} // End class

