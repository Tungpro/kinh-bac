<?php

namespace App\Containers\Product\Tasks\Admin;

use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class GetProductByIdTask.
 */
class GetProductByIdTask extends Task
{

    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($product_id, $defaultLanguage = null, $external_data = ['with_relationship' => ['all_desc', 'manufacturer']])
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;

        $data = $this->repository->with(array_merge($external_data['with_relationship'],[ 'categories', 'categories.desc' => function ($query) use($language_id) {
            $query->activeLang($language_id);
        }]))->find($product_id);

        return $data;
    }
}
