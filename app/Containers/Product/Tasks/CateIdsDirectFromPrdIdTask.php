<?php

namespace App\Containers\Product\Tasks;

use App\Containers\Product\Data\Repositories\ProductCategoryRepository;
use App\Ship\Criterias\Eloquent\SelectFieldsCriteria;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisInThatCriteria;
use App\Ship\Parents\Tasks\Task;

/**
 * Class CateIdsDirectFromPrdIdTask.
 */
class CateIdsDirectFromPrdIdTask extends Task
{

    protected $repository;

    public function __construct(ProductCategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return  mixed
     */
    public function run($product_id)
    {
        if(is_array($product_id)) {
            $this->repository->pushCriteria(new ThisInThatCriteria('product_id',$product_id));
        }else {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('product_id',$product_id));
        }

        $this->repository->pushCriteria(new SelectFieldsCriteria(['category_id']));

        $result = $this->repository->all();

        if($result && !$result->isEmpty()) {
            return $result->pluck('category_id')->toArray();
        }

        return [];
    }
}
