<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-23 21:17:58
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-24 16:43:19
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Tasks\Product;

use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class GetLPrdsNullEshopIdTasks extends Task
{
    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($notPaginate = false, $limit = 10)
    {
        try {
        $data =  $this->repository->whereNull('eshop_product_id')->with([
        'all_desc' => function ($queryDesc){
          $queryDesc->select('id','product_id','language_id','name','slug','policy','tag');
        },
        'product_variants' => function ($queryVariants) {
          $queryVariants->with([
            'product_option_values',
            'product_option_values.option.desc',
            'product_option_values.option_value.desc',
          ]);
        }]);
          return $notPaginate == true ?  $data->limit($limit) :  $data->paginate($limit);
        } catch (Exception $e) {
            // throw $e;
            throw new UpdateResourceFailedException();
        }
    }

}
