<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-23 21:17:58
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-24 16:43:19
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Tasks\Product;

use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdatePurchaseCountTask extends Task
{
    protected $repository;
    protected $productPurchased = [];
    protected $increase = true;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        try {
            foreach ($this->productPurchased as $k => $v) {
                $updates[$k] = [
                    'purchased_count' => $this->increase ? '(purchased_count + ' . $v . ')' : '(purchased_count - ' . $v . ')',
                ];
            }

            if (isset($updates) && !empty($updates)) {
                $this->repository->updateMultiple($updates, 'id', false, true);
            }
        } catch (Exception $e) {
            // throw $e;
            throw new UpdateResourceFailedException();
        }
    }

    /**
     * @var productPurchased [productId => quantity]
     */
    public function setProductPurchased(array $productPurchased): self
    {
        $this->productPurchased = $productPurchased;
        return $this;
    }

    public function increase(): self
    {
        $this->increase = true;
        return $this;
    }

    public function decrease(): self
    {
        $this->increase = false;
        return $this;
    }
}
