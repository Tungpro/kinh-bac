<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-03 13:48:24
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-06 11:13:23
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Tasks;

use App\Containers\Localization\Models\Language;
use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Containers\Product\Models\Product;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetSuggestFromProductTask extends Task
{

    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Product $product, Language $currentLang): ?iterable
    {
        $language_id = $currentLang ? $currentLang->language_id : 1;

        
    }
}
