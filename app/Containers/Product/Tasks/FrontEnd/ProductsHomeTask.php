<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-18 10:22:36
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-17 10:03:27
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Tasks\FrontEnd;


use App\Containers\Localization\Models\Language;
use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Ship\Parents\Tasks\Task;


/**
 * Class GetAllProductssTask.
 */
class ProductsHomeTask extends Task
{

    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run(string $select = '*', Language $currentLang, $orderBy = ['created_at' => 'desc','sort_order' => 'asc'],array $where = [], $limit = false)
    {
        $language_id = $currentLang ? $currentLang->language_id : 1;

        if (!empty($orderBy)) {
            foreach ($orderBy as $columm => $item) {
                if (!empty($item) && $item != 'null') {
                    $this->repository->orderBy($columm, $item);
                }
            }
        } else {
            foreach ($orderBy as $columm => $item) {
                $this->repository->orderBy($columm, $item);
            }
        }

        if (!empty($where)) {
            foreach ($where as $columm => $item) {
                if (!empty($item) && $item != 'null' && $columm != 'name') {
                    $this->repository->where($columm, $item);
                }
            }
        }

        $this->repository->whereHas('desc', function ($query) use ($where) {
            if (!empty($where['name'])) {
                $query->where('name', 'like', '%' . $where['name'] . '%')
                    ->orWhere('short_description', 'like', '%' . $where['name'] . '%');
            }
        });

        $data = $this->repository->select($select)->where('status',2)->with([
            'desc' => function ($query) use ($language_id) {
                $query->activeLang($language_id);
            },
//            'images' => function($query){
//                $query->orderBy('sort','desc')->limit(1);
//            }
        ]);

        return !empty($limit) ? $data->paginate($limit) : $data->get();
    }


}
