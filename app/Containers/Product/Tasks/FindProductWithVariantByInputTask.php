<?php

namespace App\Containers\Product\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Ship\Criterias\Eloquent\SelectFieldsCriteria;
use App\Containers\Product\Data\Repositories\ProductDescRepository;

class FindProductWithVariantByInputTask extends Task
{

  protected $repository;

  public function __construct(ProductDescRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run(string $input, int $limit)
    {
      try {
        return $this->repository->join('product_variants', 'product_description.product_id', '=', 'product_variants.product_id')->with(['product'=> function($query)
        {
          # code...
          $query->select('id', 'price', 'type', 'status', 'image');
        }])->where('language_id',1)->where('name','like', '%'.$input.'%')->limit($limit)->get();
    } catch (\Exception $th) {
        throw $th;
    }
  }
}
