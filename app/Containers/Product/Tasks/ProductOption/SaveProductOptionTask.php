<?php

namespace App\Containers\Product\Tasks\ProductOption;

use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Facades\DB;
use App\Containers\Product\Models\ProductOption;
use App\Containers\Option\Data\Repositories\OptionRepository;
use App\Containers\Option\Data\Repositories\OptionDescRepository;
use App\Containers\Option\Data\Repositories\OptionValueRepository;
use App\Containers\Product\Data\Repositories\ProductOptionRepository;
use App\Containers\Product\Data\Repositories\ProductOptionValueRepository;
use Carbon\Carbon;

/**
 * Class SaveProductOptionTask.
 */
class SaveProductOptionTask extends Task
{
    protected $productOptionRepository,
              $productOptionValueRepository,
              $optionRepository,
              $optionDescRepository,
              $optionValueRepository;

    public function __construct(
        ProductOptionRepository $productOptionRepository,
        ProductOptionValueRepository $productOptionValueRepository,
        OptionRepository $optionRepository,
        OptionDescRepository $optionDescRepository,
        OptionValueRepository $optionValueRepository
    ) {
        $this->productOptionRepository = $productOptionRepository;
        $this->productOptionValueRepository = $productOptionValueRepository;
        $this->optionRepository = $optionRepository;
        $this->optionDescRepository = $optionDescRepository;
        $this->optionValueRepository = $optionValueRepository;
    }

    public function run(array $prd_options, int $prd_id) : Void
    {
        try {
            if (!empty($prd_options)) {
                // $this->productOptionRepository->getModel()->where('product_id', $prd_id)->delete();
                // $this->productOptionValueRepository->getModel()->where('product_id', $prd_id)->delete();

                $arr_values = $arr_prd_opt = [];

                $productOptionInsertArray = $productOptionUpdateArray = [];
                $productOptionValueInsertArrayInput = $productOptionValueUpdateArrayInput = [];


                foreach ($prd_options as $product_option) {
                    if ($product_option['type'] == 'select' || $product_option['type'] == 'radio'
                                                            || $product_option['type'] == 'checkbox'
                                                            || $product_option['type'] == 'image') {
                        if (isset($product_option['product_option_value'])) {
                            if ($product_option['product_option_id']) {
                                $productOptionUpdateArray[] = [
                                    'product_option_id' => (int)@$product_option['product_option_id'],
                                    'product_id' => (int)@$prd_id,
                                    'option_id' => (int)@$product_option['option_id'],
                                    'value' => '112', // thêm vào để tránh lỗi "Insert value list does not match column list"
                                    'required' => (int)@$product_option['required'],
                                ];
                            } else {
                                $productOptionInsertArray[] = [
                                  'product_id' => (int)@$prd_id,
                                  'option_id' => (int)@$product_option['option_id'],
                                  'value' => '2222333',
                                  'required' => (int)@$product_option['required']
                                ];
                            }

                            foreach ($product_option['product_option_value'] as $product_option_value) {
                                $productOptionValueInsertArrayInput[] = [
                                    'product_option_id' => isset($prd_opt->product_option_id) ? $prd_opt->product_option_id : (int)$product_option['product_option_id'], //(int)$prd_option_obj->product_option_id,
                                    'product_id' => $prd_id,
                                    'option_id' => (int)@$product_option['option_id'],
                                    'option_value_id' => (int)@$product_option_value['option_value_id'],
                                    // 'price' => (float)intval(str_replace([',', '.'], '', @$product_option_value['price'])),
                                    // 'price_prefix' => @$product_option_value['price_prefix'] ?? '+',
                                    'images' => @$product_option_value['imgs']
                                ];
                            }
                        }
                    } else {
                        // Không thuộc tất cả các type trên thì tính là insert
                        $productOptionInsertArray[] = [
                            'product_option_id' => (int)@$product_option['product_option_id'],
                            'product_id' => (int)@$prd_id,
                            'option_id' => (int)@$product_option['option_id'],
                            'value' => @$product_option['value'],
                            'required' => (int)@$product_option['required'],
                        ];
                    }
                } // End foreach


                if (!empty($productOptionInsertArray)) {
                    $this->productOptionRepository->getModel()->insert($productOptionInsertArray);
                }

                if ( !empty($productOptionUpdateArray) ) {
                  $this->updateProductOption($productOptionUpdateArray);
                }

                if (!empty($productOptionValueInsertArrayInput)) {
                    $this->productOptionValueRepository->getModel()->insert($productOptionValueInsertArrayInput);
                }
            }
        }catch(\Exception $e) {
            throw $e;
        }
    } // End method

    public function updateProductOption(array $productOptionUpdateArray=[]) {
      // Vì mảng truyền lên lúc nào cũng có "product_option_id" vậy nên sẽ luôn vào $productOptionUpdateArray
      // Sử dụng insert update duplicate để tránh trùng
      return $this->productOptionRepository->updateOnDuplicateInsert($productOptionUpdateArray);
    }
} // End class
