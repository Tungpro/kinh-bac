<?php

namespace App\Containers\Product\Tasks\ProductOption;

use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Collection as SupportCollection;
use App\Containers\Product\Data\Repositories\ProductOptionRepository;

/**
 * Class SaveProductOptionTask.
 */
class StoreProductOptionByProductIdTask extends Task
{
    protected $productOptionRepository;

    public function __construct(ProductOptionRepository $productOptionRepository) {
        $this->productOptionRepository = $productOptionRepository;
    }

    public function run(int $productId, array $optionIds=[]): SupportCollection
    {
      $productOption = collect([]);
      foreach ($optionIds as $optionId) {
        $productOptionItem = $this->productOptionRepository->firstOrCreate([
          'option_id' => $optionId,
          'product_id' => $productId
        ]);
        $productOption->push($productOptionItem);
      }
      return $productOption;
    } // End method
} // End class
