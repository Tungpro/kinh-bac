<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-06 17:04:24
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Tasks\ProductOption;

use App\Containers\Option\Data\Repositories\OptionDescRepository;
use App\Containers\Option\Data\Repositories\OptionRepository;
use App\Containers\Option\Data\Repositories\OptionValueDescRepository;
use App\Containers\Option\Data\Repositories\OptionValueRepository;
use App\Containers\Product\Data\Repositories\ProductOptionRepository;
use App\Containers\Product\Data\Repositories\ProductOptionValueRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Facades\DB;

class GetProductOptionsTask extends Task
{

    protected $repository, $productOptionValueRepository, $optionRepository, $optionDescRepository, $optionValueRepository, $optionValueDescRepository;

    public function __construct(
        ProductOptionRepository $repository,
        ProductOptionValueRepository $productOptionValueRepository,
        OptionRepository $optionRepository,
        OptionDescRepository $optionDescRepository,
        OptionValueRepository $optionValueRepository,
        OptionValueDescRepository $optionValueDescRepository
    )
    {
        $this->repository = $repository;
        $this->productOptionValueRepository = $productOptionValueRepository;
        $this->optionRepository = $optionRepository;
        $this->optionDescRepository = $optionDescRepository;
        $this->optionValueRepository = $optionValueRepository;
        $this->optionValueDescRepository = $optionValueDescRepository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($product_id, $defaultLanguage = null)
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;
        $return_data = [];
        $prd_options = DB::table($this->repository->getModel()->getTable() . ' as po')
            ->leftJoin($this->optionRepository->getModel()->getTable() . ' as o', 'po.option_id', '=', 'o.id')
            ->leftJoin($this->optionDescRepository->getModel()->getTable() . ' as od', 'o.id', '=', 'od.option_id')
            ->where('po.product_id', $product_id)->where('od.language_id', $language_id)
            ->orderBy('o.sort_order')
            ->get()->keyBy('product_option_id')->toArray();
        if (!empty($prd_options)) {
            $arr_prd_opt_ids = array_keys($prd_options);
            $prd_opt_values = DB::table($this->productOptionValueRepository->getModel()->getTable() . ' as pov')
                ->leftJoin($this->optionValueRepository->getModel()->getTable() . ' as ov', 'pov.option_value_id', '=', 'ov.id')
                ->leftJoin($this->optionValueDescRepository->getModel()->getTable() . ' as ovd', 'ov.id', '=', 'ovd.option_value_id')
                ->whereIn('pov.product_option_id', $arr_prd_opt_ids)
                ->where('product_id', $product_id)->whereNull('deleted_at')->where('ovd.language_id', $language_id)
                ->orderBy('ov.sort_order', 'ASC')->get()->toArray();

            //    dd($prd_options,$prd_opt_values);

            if (!empty($prd_opt_values)) {
                foreach ($prd_opt_values as $product_option_value) {
                    //                    dd($prd_options[$product_option_value->product_option_id]);
                    $prd_options[$product_option_value->product_option_id]->option_value_data[] = [
                        'option_id' => $product_option_value->option_id,
                        'product_option_value_id' => $product_option_value->product_option_value_id,
                        'option_value_id' => $product_option_value->option_value_id,
                        'product_variant_id' => $product_option_value->product_variant_id,
                        'name' => $product_option_value->name,

                        // Bỏ không dùng nữa, tạm thời để lại để lấy tư liệu tham khảo :")))~
                        // 'quantity'                => $product_option_value->quantity,
                        // 'subtract'                => $product_option_value->subtract,
                        // 'price'                   => $product_option_value->price,
                        // 'price_prefix'            => $product_option_value->price_prefix,
                        // 'points'                  => $product_option_value->points,
                        // 'points_prefix'           => $product_option_value->points_prefix,
                        // 'weight'                  => $product_option_value->weight,
                        // 'weight_prefix'           => $product_option_value->weight_prefix,
                        'images' => $product_option_value->images,
                        'color' => $product_option_value->image,
                    ];
                }
            }
            //    dd($prd_options);

            foreach ($prd_options as $k => $product_option) {
                if (isset($product_option->option_value_data) && !empty($product_option->option_value_data)) {
                    $return_data[] = [
                        'product_option_id' => $product_option->product_option_id,
                        'product_option_value' => isset($product_option->option_value_data) ? $product_option->option_value_data : [],
                        'option_id' => $product_option->option_id,
                        'name' => $product_option->name,
                        'type' => $product_option->type,
                        'value' => $product_option->value,
                        'required' => $product_option->required,
                        'show_image' => $product_option->show_image === 1,
                    ];
                }
            }
        }

        return $return_data;
    }
}
