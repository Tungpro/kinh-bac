<?php

namespace App\Containers\Product\Tasks;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Containers\Product\Data\Repositories\ProductVariantRepository;
use App\Ship\Criterias\Eloquent\SelectFieldsCriteria;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;
use App\Containers\Product\Enums\ProductSync;

/**
 * Class SaveProductTask.
 */
class SaveProductTask extends Task
{

    protected $repository;

    public function __construct(ProductRepository $repository, ProductVariantRepository $repositoryVariantRepository)
    {
        $this->repository = $repository;
        $this->repositoryVariantRepository = $repositoryVariantRepository;
    }

    /**
     * @return  mixed
     */
    public function run(array $data, $product_id)
    {
        try {
            $dataUpdate = Arr::except($data, ['product_description', 'img_upload_for_add', 'product_category', 'product_filter', 'product_attribute', 'product_option', '_token', '_headers']);
            $dataUpdate['price'] = intval(str_replace([',', '.'], '', $dataUpdate['price']));
            $dataUpdate['global_price'] = intval(str_replace([',', '.'], '', $dataUpdate['global_price']));
            $dataUpdate['stock_status_id'] = $dataUpdate['stock_status_id'];
            $dataUpdate['quantity'] = intval(str_replace([',', '.'], '', @$dataUpdate['quantity']));
            $dataUpdate['length'] = intval(str_replace([',', '.'], '', @$dataUpdate['length']));
            $dataUpdate['weight'] = intval(str_replace([',', '.'], '', @$dataUpdate['weight']));
            $dataUpdate['height'] = intval(str_replace([',', '.'], '', @$dataUpdate['height']));
            $dataUpdate['sort_order'] = intval(str_replace([',', '.'], '', @$dataUpdate['sort_order']));
            $dataUpdate['shipping_required'] = intval(str_replace([',', '.'], '', @$dataUpdate['shipping_required']));
            $dataUpdate['status'] = intval(str_replace([',', '.'], '', empty($dataUpdate['status']) && $dataUpdate['status']==null? 1 : $dataUpdate['status'] ));

            // $dataUpdate['date_available'] = FunctionLib::getCarbonFromVNDate($dataUpdate['date_available']);
            if(isset($product_id) && !empty($product_id) && $product_id!= 'NaN'){
                $product = $this->repository->update($dataUpdate, $product_id);
                $arrayPrd = $product->toArray();
                if(ProductSync::SYNC){
                  if( array_key_exists(ProductSync::KEY_ISSET_SYNC,$arrayPrd) && empty($arrayPrd[ProductSync::KEY_ISSET_SYNC]))
                    $this->repositoryVariantRepository->where('product_id', $product_id)->where('is_root', 1)->update(['price' => $dataUpdate['price'],'global_price' => $dataUpdate['global_price']]);
                }else{
                    $this->repositoryVariantRepository->where('product_id', $product_id)->where('is_root', 1)->update(['price' => $dataUpdate['price'],'global_price' => $dataUpdate['global_price']]);
                }
            }else{
                $product = $this->repository->create($dataUpdate);
                // create root variant cho trường hợp những sản phẩm thuần, ko có thuộc tính
                $this->repositoryVariantRepository->create(
                   [
                    'product_id' => $product->id,
                    'is_root' => 1,
                    'price' => $dataUpdate['price'],
                    'global_price' => $dataUpdate['global_price'],
                   ]
                );
            }
            return $product;

        }catch(Exception $e) {
            throw $e;
        }
    }
}
