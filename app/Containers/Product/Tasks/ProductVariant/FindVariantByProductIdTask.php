<?php

/**
* @ Created by: VSCode
* @ Author: Oops!Memory - OopsMemory.com
* @ Create Time: 2021-10-13 10:15:44
* @ Modified by: Oops!Memory - OopsMemory.com
* @ Modified time: 2021-10-13 15:15:52
* @ Description: Happy Coding!
*/

namespace App\Containers\Product\Tasks\ProductVariant;

use App\Containers\Product\Data\Repositories\ProductVariantRepository;
use App\Ship\Criterias\Eloquent\OrderByFieldCriteria;
use App\Ship\Parents\Tasks\Task;

class FindVariantByProductIdTask extends Task
{
  protected $repository;

  public function __construct(ProductVariantRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run(int $productId = 0, int $isset_root = 0)
  {
    $productsVariant = $this->repository->pushCriteria(new OrderByFieldCriteria('product_variant_id', 'asc'));

    $productsVariant->where([
      ['product_id', '=', $productId]
    ]);
    if ($isset_root == 0) {
      $productsVariant->where([
        ['is_root', '=', 0]
      ]);
    }
    return $productsVariant->get();
  }
} // End class
