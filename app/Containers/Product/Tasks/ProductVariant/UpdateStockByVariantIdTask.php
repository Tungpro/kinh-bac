<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-23 21:17:58
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-24 13:45:26
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Tasks\ProductVariant;

use App\Containers\Product\Data\Repositories\ProductVariantRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateStockByVariantIdTask extends Task
{
    protected $repository;
    protected $variantStock = [];
    protected $increase = true;

    public function __construct(ProductVariantRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        try {
            foreach ($this->variantStock as $k => $v) {
                $updates[$k] = [
                    'stock' => $this->increase ? '(stock + ' . $v . ')' : '(stock - ' . $v . ')',
                ];
            }

            if (isset($updates) && !empty($updates)) {
                $this->repository->updateMultiple($updates, 'product_variant_id', false, true);
            }
        } catch (Exception $e) {
            // throw $e;
            throw new UpdateResourceFailedException();
        }
    }

    /**
     * @var variantStock [variantId => quantity]
     */
    public function setVariantStock(array $variantStock): self
    {
        $this->variantStock = $variantStock;
        return $this;
    }

    public function increase(): self
    {
        $this->increase = true;
        return $this;
    }

    public function decrease(): self
    {
        $this->increase = false;
        return $this;
    }
}
