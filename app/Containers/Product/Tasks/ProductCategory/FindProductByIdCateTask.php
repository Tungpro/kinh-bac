<?php

namespace App\Containers\Product\Tasks\ProductCategory;

use App\Containers\Product\Data\Repositories\ProductCategoryRepository;
use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Ship\Parents\Tasks\Task;
use Apiato\Core\Foundation\Facades\Apiato;


/**
 * Class GetProductByIdTask.
 */
class FindProductByIdCateTask extends Task
{

    protected $repository;

    public function __construct(ProductCategoryRepository $repository, ProductRepository $repositoryPrd)
    {
        $this->repository = $repository;
        $this->repositoryPrd = $repositoryPrd;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run(int $cate_id,$defaultLanguage = null,int $limit, $offset, $inputName)
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;
        $arr_cate_ids = [$cate_id];
        // dd($cate_id);
        Apiato::call('Category@GetAllCateChildsTask', [[$cate_id], &$arr_cate_ids]);
        $arr_cate_ids = array_unique($arr_cate_ids);
        $data = $this->repository->getModel()->with(['category','product', 'product.categories', 'product.desc' => function ($query) use($language_id) {
        $query->activeLang($language_id);
        }])->has('product')->whereIn('category_id',$arr_cate_ids);
        if(isset($inputName) && !empty($inputName)){
          $data->whereHas('product.desc', function ($query) use($inputName) {
              $query->where('name', 'like', '%' . $inputName . '%');
          });
        }
        if(isset($offset) && !empty($offset)){
          return  $data->groupBy('product_id')->orderBy('product_id', 'DESC')->offset($offset)->limit($limit)->get();
        }else{
          return  $data->groupBy('product_id')->orderBy('product_id', 'DESC')->limit($limit)->get();
        }
    }
}
