<?php

namespace App\Containers\Product\Tasks\ProductCategory;

use App\Containers\Product\Models\Product;
use App\Ship\Parents\Tasks\Task;

/**
 * Class SaveProductCategoryTask.
 */
class SaveProductCategoryTask extends Task
{
    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($product_category,Product $current_product_model)
    {
        if (!empty($product_category)) {
            if (is_array($product_category)) {
                $current_product_model->categories()->sync($product_category);
            } else {
                $current_product_model->categories()->attach($product_category);
            }
        }else {
            $current_product_model->categories()->sync([]);
        }
    }
}
