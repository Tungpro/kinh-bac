<?php

namespace App\Containers\Product\Tasks\ProductDesc;

use App\Ship\Parents\Tasks\Task;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Ship\Criterias\Eloquent\SelectFieldsCriteria;
use App\Containers\Product\Data\Repositories\ProductDescRepository;

class FindProducDescByProductIdTask extends Task
{

  protected $repository;

  public function __construct(ProductDescRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run(array $where=[])
  {
    return $this->repository->findWhere($where);
  }
}
