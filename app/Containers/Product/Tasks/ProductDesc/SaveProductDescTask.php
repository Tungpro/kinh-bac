<?php

namespace App\Containers\Product\Tasks\ProductDesc;

use App\Containers\Product\Data\Repositories\ProductDescRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Str;

/**
 * Class SaveProductDescTask.
 */
class SaveProductDescTask extends Task
{
    protected $repository;

    public function __construct(ProductDescRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data, $original_desc, $id, $edit_id = null)
    {
        try {
            $description = $data['product_description'];

            if (is_array($description) && !empty($description)) {
                $updates = [];
                $inserts = [];
                foreach ($description as $k => $v) {
                    if (isset($original_desc[$k])) {
                        $updates[$original_desc[$k]['id']] = [
                            'name' => $v['name'],
                            'slug' => $v['slug'] ? Str::slug($v['slug']) : Str::slug($v['name']),
                            'short_description' => $v['short_description'],
                            'description' => $v['description'] ?? '<p></p>',
                            'product_description' => $v['product_description'] ?? '<p></p>',
                            'policy' => $v['policy'] ?? '<p></p>',
                            'privilege' => $v['privilege'] ?? '<p></p>',
                            'meta_title' => $v['meta_title'],
                            'meta_description' => $v['meta_description'],
                            'meta_keyword' => $v['meta_keyword'],
                        ];
                    } else {
                        $inserts[] = [
                            'product_id' => $id,
                            'language_id' => $k,
                            'name' => $v['name'],
                            'slug' => $v['slug'] ? Str::slug($v['slug']) : Str::slug($v['name']),
                            'short_description' => $v['short_description'],
                            'description' => $v['description'] ?? '<p></p>',
                            'product_description' => $v['product_description'] ?? '<p></p>',
                            'policy' => $v['policy'] ?? '<p></p>',
                            'privilege' => $v['privilege'] ?? '<p></p>',
                            'meta_title' => $v['meta_title'],
                            'meta_description' => $v['meta_description'],
                            'meta_keyword' => $v['meta_keyword'],
                        ];
                    }
                }

                if (!empty($updates)) {
                    $this->repository->updateMultiple($updates);
                }

                if (!empty($inserts)) {
                    $this->repository->getModel()->insert($inserts);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
}
