<?php

Route::group(
[
    'middleware' => [
        'api',
    ],
],
function () use ($router) {
    $router->get('product/loadall', [
        'as' => 'api_product_load_all',
        'uses'       => 'Admin\ProductController@loadAll'
    ]);
});