<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-04 16:55:35
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-30 20:11:50
 * @ Description: Happy Coding!
 */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

Route::group(
[
    'middleware' => [
        // 'api',
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run().'/product',
],
function () use ($router) {
    $router->get('/getNewProductsHome', [
        'as' => 'api_product_get_new_products_home',
        'uses'       => 'FrontEnd\Controller@getNewProductsHome'
    ]);

    $router->get('/getProductByCate', [
        'as' => 'api_product_get_by_cate',
        'uses'       => 'FrontEnd\Controller@getProductByCate'
    ]);

    $router->get('/getProductByCollection', [
        'as' => 'api_product_get_by_collection',
        'uses'       => 'FrontEnd\Controller@getProductByCollection'
    ]);

    $router->get('/getQuickProductsHome', [
        'as' => 'api_product_get_quick_products_home',
        'uses'       => 'FrontEnd\Controller@getQuickProductsHome'
    ]);

    $router->get('/getSuggestProducts', [
        'as' => 'api_product_get_suggest_products',
        'uses'       => 'FrontEnd\Controller@getSuggestProducts'
    ]);

    $router->get('/getTopProductSearch', [
        'as' => 'api_product_get_top_product_search_home',
        'uses'       => 'FrontEnd\Controller@getTopProductSearch'
    ]);

    $router->get('/getAllProductByCollection', [
        'as' => 'api_product_get_all_by_collection',
        'uses'       => 'FrontEnd\Controller@getAllProductByCollection'
    ]);

    $router->get('/GetProductToCollectionByTag', [
        'as' => 'api_product_to_collection_by_Tag',
        'uses'       => 'FrontEnd\Controller@getProductToCollectionByTag'
    ]);

    $router->get('/GetProductByManufacturer', [
        'as' => 'api_product_by_manufacturer',
        'uses'       => 'FrontEnd\Controller@getProductByManufacturer'
    ]);

   
});