<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-04 16:55:35
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-13 23:46:25
 * @ Description: Happy Coding!
 */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

Route::group(
[
    'middleware' => [
        // 'api',
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run().'/product/detail',
],
function () use ($router) {
    $router->any('/{id}', [
        'as' => 'api_product_detail',
        'uses'       => 'FrontEnd\Controller@getProductInfor'
    ])->where([
        'id' => '[0-9]+'
    ]);

    $router->any('/getAllOptions', [
        'as' => 'api_product_detail_get_all_options',
        'uses'       => 'FrontEnd\Controller@getAllOptions'
    ]);

    $router->any('/getAllVariants', [
        'as' => 'api_product_detail_get_all_variants',
        'uses'       => 'FrontEnd\Controller@getAllVariants'
    ]);

   
});