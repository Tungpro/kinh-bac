<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-05 10:47:07
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Controllers\FrontEnd\Features\Detail;

use App\Containers\Product\Actions\ProductVariant\FindVariantByProductIdAction;
use App\Containers\Product\UI\API\Requests\FrontEnd\Detail\GetAllVariantsRequest;
use App\Containers\Product\UI\API\Transformers\FrontEnd\Detail\ProductVariantsTransformer;

trait GetAllVariants
{
    public function getAllVariants(GetAllVariantsRequest $request)
    {
        $productId = $request->productId;
        $variants = app(FindVariantByProductIdAction::class)->run($productId, [
            // 'product_option_values',
            // 'product_option_values.option.desc',
            'product_option_values.option_value.desc'
        ]);
        // dd($variants);
        // return $variants;
        return $this->transform($variants, ProductVariantsTransformer::class, [], [
            'message' => 'Success',
            'status' => true
        ], 'product_options');
    }
}
