<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-12 15:20:47
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Collection\Actions\FrontEnd\GetCollectionByTagAction;
use App\Containers\Product\UI\API\Requests\FrontEnd\GetProductByTagRequest;
use App\Containers\Product\UI\API\Transformers\FrontEnd\Detail\ProductCollectionTransformer;
use App\Containers\PromotionCampaign\Actions\FrontEnd\ApplyCampaignForProductListAction;


trait GetProductToCollectionByTag
{
    public $data = [];
    public function getProductToCollectionByTag(
        GetProductByTagRequest $request,
        ApplyCampaignForProductListAction $applyCampaignForProductListAction
    ) {
        $products = app(GetCollectionByTagAction::class)
            ->descSpecialOfffer($this->currentLang)
            ->descTags($this->currentLang)
            ->run($request->tag, [
                'product.manufacturer',
                'product.specialOffers',
                'product.specialOffers.desc',
                'product.specialTags',
                'product.specialTags.desc'
            ], $this->currentLang, 20);
        
        //  dd($products);
        $list_product=$products->pluck('product');
        $applyCampaignForProductListAction->run($list_product);
        $this->data['list_product'] = $this->transform($products, ProductCollectionTransformer::class, [], []);
     
        $this->data['pagination'] = isset($this->data['list_product']['meta']['pagination']) ?$this->data['list_product']['meta']['pagination'] : [];
        $this->data['list_product']= isset($this->data['list_product']['data']) ? $this->data['list_product']['data'] : [];
        return $this->data;
    }
}
