<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-06 17:04:33
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Controllers\FrontEnd\Features\Detail;

use App\Containers\Product\Actions\ProductOption\GetProductOptionsAction;
use App\Containers\Product\UI\API\Requests\FrontEnd\Detail\GetAllOptionsRequest;
use App\Containers\Product\UI\API\Transformers\FrontEnd\Detail\ProductOptionsTransfomer;

trait GetAllOptions
{
    public function getAllOptions(GetAllOptionsRequest $request)
    {
        $productId = $request->productId;
        $options = app(GetProductOptionsAction::class)->run($productId, $this->currentLang);
        // dd($options);
        // return $options;
        return $this->transform($options, ProductOptionsTransfomer::class, [], [
            'message' => 'Success',
            'status' => true
        ], 'product_options');
    }
}
