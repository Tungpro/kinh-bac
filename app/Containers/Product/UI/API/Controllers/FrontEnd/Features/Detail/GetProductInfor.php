<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-16 18:06:51
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Controllers\FrontEnd\Features\Detail;

use App\Containers\Product\Actions\Api\GetProductByIdForApiAction;
use App\Containers\Product\UI\API\Requests\FrontEnd\Detail\GetProductInforRequest;
use App\Containers\Product\UI\API\Transformers\FrontEnd\Detail\ProductInforTransformer;

trait GetProductInfor
{
    public function getProductInfor(GetProductInforRequest $request)
    {
        $variantId = $request->piid;
        $product = app(GetProductByIdForApiAction::class)->skipCache()->run($request->id,(int)$variantId,$this->currentLang);

        // return $product;
        
        return $this->transform($product, ProductInforTransformer::class, [], [
            'message' => 'Success',
            'status' => true
        ], 'product_infor');
    }
}
