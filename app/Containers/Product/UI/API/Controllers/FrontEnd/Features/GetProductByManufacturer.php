<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-28 18:10:50
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Product\UI\API\Requests\FrontEnd\GetProductByManufacturerRequest;
use App\Containers\Product\UI\API\Transformers\FrontEnd\ProductListTransformer;
use App\Containers\PromotionCampaign\Actions\FrontEnd\ApplyCampaignForProductListAction;
use App\Containers\Product\Actions\Api\GetProductByManufacturerAction;

trait GetProductByManufacturer
{
    public $data = [];
    public function getProductByManufacturer(
        GetProductByManufacturerRequest $request,
        ApplyCampaignForProductListAction $applyCampaignForProductListAction
    ) {
        $products = app(GetProductByManufacturerAction::class)
            ->run($request->manufacturer_id,$this->currentLang, 20);
        
        //  dd($products);
        $applyCampaignForProductListAction->run($products);
        $this->data['list_product'] = $this->transform($products, ProductListTransformer::class, [], [],'list_product');
     
        $this->data['pagination'] = isset($this->data['list_product']['meta']['pagination']) ?$this->data['list_product']['meta']['pagination'] : [];
        $this->data['list_product']= isset($this->data['list_product']['data']) ? $this->data['list_product']['data'] : [];
        return $this->data;
    }
}
