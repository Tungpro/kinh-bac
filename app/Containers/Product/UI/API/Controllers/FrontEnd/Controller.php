<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-30 20:13:15
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\UI\API\Controllers\FrontEnd;

use App\Containers\BaseContainer\UI\WEB\Controllers\BaseApiFrontController;
use App\Containers\Category\Actions\FrontEnd\GetCategoryByIdAction;
use App\Containers\Product\Actions\FrontEnd\ProductListingAction;
use App\Containers\Product\UI\API\Controllers\FrontEnd\Features\Detail\GetAllOptions;
use App\Containers\Product\UI\API\Controllers\FrontEnd\Features\Detail\GetAllVariants;
use App\Containers\Product\UI\API\Controllers\FrontEnd\Features\Detail\GetProductInfor;
use App\Containers\Product\UI\API\Controllers\FrontEnd\Features\GetNewProductsHome;
use App\Containers\Product\UI\API\Controllers\FrontEnd\Features\GetProductByCate;
use App\Containers\Product\UI\API\Controllers\FrontEnd\Features\GetProductByCollection;
use App\Containers\Product\UI\API\Controllers\FrontEnd\Features\GetQuickProductsHome;
use App\Containers\Product\UI\API\Controllers\FrontEnd\Features\GetSuggestProducts;
use App\Containers\Product\UI\API\Controllers\FrontEnd\Features\GetTopProductSearch;
use App\Containers\Product\UI\API\Controllers\FrontEnd\Features\GetAllProductCollection;
use App\Containers\Product\UI\API\Controllers\FrontEnd\Features\GetProductByManufacturer;
use App\Containers\Product\UI\API\Controllers\FrontEnd\Features\GetProductToCollectionByTag;

class Controller extends BaseApiFrontController
{
    use GetProductByCate,
        GetSuggestProducts,
        GetQuickProductsHome,
        GetNewProductsHome,
        GetTopProductSearch,
        GetAllOptions,
        GetAllVariants,
        GetProductInfor,
        GetProductByCollection,
        GetAllProductCollection,
        GetProductToCollectionByTag,
        GetProductByManufacturer;

    public $getCategoryByIdAction, $productListingAction;

    public function __construct(
        GetCategoryByIdAction $getCategoryByIdAction,
        ProductListingAction $productListingAction
    ) {
        parent::__construct();
        $this->getCategoryByIdAction = $getCategoryByIdAction;
        $this->productListingAction = $productListingAction;
    }
}
