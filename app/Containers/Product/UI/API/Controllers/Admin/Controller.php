<?php

namespace App\Containers\Product\UI\API\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\StringLib;
use App\Containers\Product\Models\Product;
use App\Containers\Product\Models\ProductImage;
use App\Containers\Product\UI\API\Requests\LoadImgGalleryRequest;
use App\Containers\Product\UI\API\Requests\UploadImgPrdGalleryRequest;
use App\Ship\Parents\Controllers\ApiController;
use App\Ship\Transporters\DataTransporter;

/**
 * Class Controller.
 */
class Controller extends ApiController
{
    public function loadImgGallery(LoadImgGalleryRequest $request) {
        return FunctionLib::ajaxRespond(true, 'ok', ['images' => Apiato::call('Product@Admin\GetProductGalleryAction', [$request->object_id])]);;
    }

    public function uploadImgGallery(UploadImgPrdGalleryRequest $request)
    {
        if ($request->hasFile('Filedata')) {
            $image = $request->file('Filedata');
            if ($image->isValid()) {
                $title = basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension());
                $fname = Apiato::call('File@UploadImageAction', [$request, 'Filedata', $title, StringLib::getClassNameFromString(Product::class)]);
                if (!empty($fname) && !$fname['error']) {
                    $imgGallery = new ProductImage();
                    $imgGallery->object_id = $request->object_id;
                    $imgGallery->image = $fname['fileName'];
                    //                    $imgGallery->size = $image->getClientSize();
                    //                    $imgGallery->type = $image->getClientMimeType();
                    $imgGallery->type = $request->type;
                    //                    $imgGallery->changed = time();
                    // $imgGallery->user_id = \Auth::id();
                    //                    $imgGallery->uname = \Auth::user()->user_name;
                    //                    $imgGallery->lang = $request->lang;
                    $imgGallery->sort = ProductImage::getSortInsert($request->lang);
                    $imgGallery->save();

                    if (empty($imgGallery->object_id)) {
                        return \Lib::ajaxRespond(true, 'ok', ['id' => $imgGallery->id]);
                    } else {
                        return \Lib::ajaxRespond(true, 'ok', ['images' => Apiato::call('Product@Admin\GetProductGalleryAction', [$request->object_id])]);
                    }
                }
                return \Lib::ajaxRespond(false, 'Upload ảnh thất bại!');
            }
            return \Lib::ajaxRespond(false, 'File không hợp lệ!');
        }
        return Lib::ajaxRespond(false, 'Không tìm thấy ảnh!');
    }
}
