@if($product_option['type'] == 'textarea' )
    <div class="form-group row align-items-center">
        <label class="col-sm-2 control-label text-right mb-0" for="input-value{{ $k_prd_opt }}">Nội dung tùy chọn</label>
        <div class="col-sm-10">
            <textarea name="product_option[{{ $k_prd_opt }}][value]" rows="5" placeholder="Nội dung tùy chọn" id="input-value{{ $k_prd_opt }}" class="form-control">{{ $product_option['value'] }}</textarea>
        </div>
    </div>
@endif