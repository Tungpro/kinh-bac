@if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image')
    <div class="table-responsive table-opts">
        <table id="option-value{{ $k_prd_opt }}"
            class="table table-striped table-bordered table-hover table-options">
            <thead>
                <tr>
                    <td class="text-left">Option Value</td>
                    {{-- <td class="text-right">Quantity</td>
            <td class="text-left">Trừ tồn kho</td> --}}
                    <td class="text-right">Giá trị</td>
                    {{-- <td class="text-right">Tích điểm</td>
            <td class="text-right">Trọng lượng</td> --}}
                    <td class="">Ảnh</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                @foreach ($product_option['product_option_value'] as $product_option_value)
                    <tr id="option-value-row-{{ $k_prd_opt }}-{{ $option_value_row }}">
                        <td class="text-left">
                            <select
                                name="product_option[{{ $k_prd_opt }}][product_option_value][{{ $option_value_row }}][option_value_id]"
                                class="form-control">
                                @if (isset($option_values[$product_option['option_id']]))
                                    @foreach ($option_values[$product_option['option_id']] as $option_value)
                                        <option value="{{ $option_value['option_value_id'] }}"
                                            {{ $option_value['option_value_id'] == $product_option_value['option_value_id'] ? 'selected="selected"' : '' }}>
                                            {{ $option_value['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                            <input type="hidden"
                                name="product_option[{{ $k_prd_opt }}][product_option_value][{{ $option_value_row }}][product_option_value_id]"
                                value="{{ $product_option_value['product_option_value_id'] }}" />
                        </td>
                        {{-- <td class="text-right">
                    <input type="text" name="product_option[{{ $k_prd_opt }}][product_option_value][{{ $option_value_row }}][quantity]" value="{{ $product_option_value['quantity'] }}" placeholder="Quantity" class="form-control"/>
                </td> --}}
                        {{-- <td class="text-left">
                    <select name="product_option[{{ $k_prd_opt }}][product_option_value][{{ $option_value_row }}][subtract]" class="form-control">
                        <option value="1" {{$product_option_value['subtract'] ? 'selected="selected"' : ''}}>Yes</option>
                        <option value="0" {{$product_option_value['subtract'] ? '' : 'selected="selected"'}}>No</option>
                    </select>
                </td> --}}
                        <td class="text-right">
                            <select
                                name="product_option[{{ $k_prd_opt }}][product_option_value][{{ $option_value_row }}][price_prefix]"
                                class="form-control">
                                <option value="+"
                                    {{ $product_option_value['price_prefix'] == '+' ? 'selected="selected"' : '' }}>+
                                </option>
                                <option value="-"
                                    {{ $product_option_value['price_prefix'] == '-' ? 'selected="selected"' : '' }}>-
                                </option>
                            </select>
                            <input type="text"
                                name="product_option[{{ $k_prd_opt }}][product_option_value][{{ $option_value_row }}][price]"
                                value="{{ \FunctionLib::numberFormat($product_option_value['price'], '') }}"
                                placeholder="Giá trị" class="form-control" onkeypress="return shop.numberOnly()"
                                onkeyup="shop.mixMoney(this)" onfocus="this.select()" />
                        </td>
                        {{-- <td class="text-right">
                    <select name="product_option[{{ $k_prd_opt }}][product_option_value][{{ $option_value_row }}][points_prefix]" class="form-control">
                        <option value="+" {{$product_option_value['points_prefix'] == '+' ? 'selected="selected"' : ''}}>+</option>
                        <option value="-" {{$product_option_value['points_prefix'] == '-' ? 'selected="selected"' : ''}}>-</option>
                    </select>
                    <input type="text" name="product_option[{{ $k_prd_opt }}][product_option_value][{{ $option_value_row }}][points]" value="{{ $product_option_value['points'] }}" placeholder="Điểm" class="form-control"/></td>
                <td class="text-right">
                    <select name="product_option[{{ $k_prd_opt }}][product_option_value][{{ $option_value_row }}][weight_prefix]" class="form-control">
                        <option value="+" {{$product_option_value['weight_prefix'] == '+' ? 'selected="selected"' : ''}}>+</option>
                        <option value="-" {{$product_option_value['weight_prefix'] == '-' ? 'selected="selected"' : ''}}>-</option>
                    </select>
                    <input type="text" name="product_option[{{ $k_prd_opt }}][product_option_value][{{ $option_value_row }}][weight]" value="{{ $product_option_value['weight'] }}" placeholder="Trọng lượng" class="form-control"/></td> --}}
                        <td>
                            <input id="input-imgs-{{ $k_prd_opt }}-{{ $option_value_row }}" type="hidden"
                                name="product_option[{{ $k_prd_opt }}][product_option_value][{{ $option_value_row }}][imgs]"
                                value="{{ $product_option_value['images'] }}" />
                            <a href="javascript:void(0);"
                                data-input="input-imgs-{{ $k_prd_opt }}-{{ $option_value_row }}"
                                data-preview="preview-imgs-{{ $k_prd_opt }}-{{ $option_value_row }}"
                                class="btn btn-primary lfm"><i class="fa  fa-picture-o"></i> Chọn ảnh</a>
                            <div id="preview-imgs-{{ $k_prd_opt }}-{{ $option_value_row }}"
                                class="preview-imgs mt-3">
                                @php
                                    $images = json_decode($product_option_value['images']) ?? [];
                                @endphp

                                @foreach ($images as $img)
                                    <div class="wrap-img-opt"><img src="{{ $img->thumb_nail }}" alt=""> <i
                                            data-input-imgs="input-imgs-{{ $k_prd_opt }}-{{ $option_value_row }}"
                                            data-src="{{ $img->thumb_nail }}" class="fa fa-close"></i></div>
                                @endforeach
                            </div>
                        </td>
                        <td class="text-right"><button type="button"
                                onclick="$('#option-value-row-{{ $k_prd_opt }}-{{ $option_value_row }}').remove();"
                                data-toggle="tooltip" title="Xóa" class="btn btn-danger"><i
                                    class="fa fa-minus-circle"></i></button></td>
                    </tr>
                    @php($option_value_row = $option_value_row + 1)
                    @endforeach
                </tbody>

                <tfoot>
                    <tr>
                        <td colspan="6"></td>
                        <td class="text-left"><button data-current-row="{{ $option_value_row }}" type="button"
                                onclick="addOptionValue('{{ $k_prd_opt }}',this);" data-toggle="tooltip" title="Thêm"
                                class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <select id="option-values{{ $k_prd_opt }}" style="display: none;">
            @if (isset($option_values[$product_option['option_id']]))
                @foreach ($option_values[$product_option['option_id']] as $option_value)
                    <option value="{{ $option_value['option_value_id'] }}">{{ $option_value['name'] }}</option>
                @endforeach
            @endif
        </select>
    @endif
