@include('product::admin.components.option_types.text')

@include('product::admin.components.option_types.textarea')

@include('product::admin.components.option_types.file')

@include('product::admin.components.option_types.date')

@include('product::admin.components.option_types.time')

@include('product::admin.components.option_types.datetime')

@include('product::admin.components.option_types.select_radio_checkbox',['option_value_row' => $option_value_row])