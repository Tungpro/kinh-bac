@extends('basecontainer::admin.layouts.default')

@section('right-breads')
    {!! $data->appends($search_data->toArray())->links('basecontainer::admin.inc.paginator') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-5">
                <h1>Quản trị Sản phẩm</h1>
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif

            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            {!! Form::open(['url' => route('admin_product_home_page'), 'method' => 'get', 'id' => 'searchForm']) !!}
            <div class="card card-accent-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                                class="fa fa-hashtag"></i></span></div>
                                <input type="text" name="id" class="form-control" placeholder="ID"
                                       value="{{ $search_data->id }}">
                            </div>
                        </div>
                        {{--<div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                                class="fa fa-hashtag"></i></span></div>
                                <input type="text" name="sync_id" class="form-control" placeholder="sync id"
                                       value="{{ $search_data->eshop_product_id }}">
                            </div>
                        </div>--}}
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                                class="fa fa-bookmark-o"></i></span></div>
                                <input type="text" name="name" class="form-control" placeholder="Tiêu đề"
                                       value="{{ $search_data->name }}">
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                                class="fa fa-calendar"></i></span></div>
                                <input type="text" name="time_from" class="datepicker form-control"
                                       placeholder="Ngày từ"
                                       autocomplete="off" value="{{ $search_data->time_from }}">
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                                class="fa fa-calendar"></i></span></div>
                                <input type="text" name="time_to" class="datepicker form-control" placeholder="Ngày đến"
                                       autocomplete="off" value="{{ $search_data->time_to }}">
                            </div>
                        </div>

                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <select class="form-control{{ $errors->has('cate_ids') ? ' is-invalid' : '' }}"
                                        name="cate_ids" id="cate_ids">
                                    <option value="">-- Chọn danh mục --</option>
                                    @if($categories && $categories->isNotEmpty())
                                        @foreach($categories as $category)
                                            <option value="{{$category->category_id}}" {{$category->category_id == $search_data->cate_ids ? 'selected' : ''}}>{{$category->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                                class="fa fa-list"></i></span></div>
                                <select id="status" name="status" class="form-control">
                                    <option value="">-- Chọn trạng thái --</option>
                                    <option value="2" {{ $search_data->status == 2 ? ' selected="selected"' : '' }}>Đang
                                        hiển thị
                                    </option>
                                    <option value="1" {{ $search_data->status == 1 ? ' selected="selected"' : '' }}>Đang
                                        ẩn
                                    </option>
                                    <option value="-1" {{ $search_data->status == -1 ? ' selected="selected"' : '' }}>Đã
                                        xóa
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                    <a href="{{ route('admin_product_home_page') }}" class="btn btn-sm btn-primary"><i
                                class="fa fa-refresh"></i> Reset</a>
                    @if($user->can('update-products'))
                    <a class="btn btn-sm btn-primary" style='color:white' href="{{ route('admin_product_add') }}"><i
                                class="fa fa-plus"></i>Thêm mới</a>
                    @endif
                </div>
            </div>
            {!! Form::close() !!}

            <div class="card card-accent-primary">
                <table class="table table-bordered table-hover mb-0">
                    <thead>
                    <tr>
                        <th width="55">ID</th>
                        <th style="min-width:400px;">Tiêu đề</th>
                        <!-- <th width="100">Mã sản phẩm</th> -->
                        {{-- <th>Danh mục</th> --}}
                        <!-- <th width="150">Giá</th> -->
                        {{-- <th width="100">Ngôn ngữ</th> --}}
                        <th width="125">Ảnh</th>
                        <th width="200">Ngày tạo</th>
                        {{-- @if (\FunctionLib::can($permission, 'edit') || \FunctionLib::can($permission, 'delete')) --}}
                        {{-- <th width="110">Phân loại</th> --}}
                        <th width="120">Trạng thái</th>
                        {{--<th width="55">Sync</th>--}}
                        @if($user->can('update-products'))
                        <th width="55">Lệnh</th>
                        @endif
                        {{-- @endif --}}
                    </tr>
                    </thead>
                    <tbody>
                    @if (isset($data) && !empty($data) && !$data->isEmpty())
                        @foreach ($data as $key => $item)
                            <tr>
                                <td align="center">
                                    <p>{{ $item->id }}</p>
                                    {{--<p>EID:{{ $item->eshop_product_id }}</p>--}}
                                </td>
                                <td>
                                    <b>{{ @$item->desc->name }}</b>
                                    <div class="mt-2 font-sm"><i>{{ @$item->desc->short_description }}</i></div>
                                    @if (!$item->categories->isEmpty())
                                        <div class="mt-2 font-sm">
                                            Danh mục:
                                            @foreach ($item->categories as $c)
                                                @if (!empty($c->desc))
                                                    <b>{{ $c->desc->name }}</b> {{ $loop->last ? '' : ' | ' }}
                                                @endif
                                            @endforeach
                                        </div>
                                    @endif
                                </td>
                                <!-- <td>
                                    {{ !empty($item->sku) ? $item->sku : '---' }}
                                </td> -->
                                {{-- <td> --}}
                                {{-- {{ !empty($item->category) ? $item->category->title : '---' }} --}}
                                {{-- </td> --}}
                                <!-- <td align="right">
                                    <b>{{ $item->price_format() }}</b><br>
                                    <s>{{ $item->price_format(true) }}</s>
                                </td> -->
                                {{-- <td>{{ $item->lang() }}</td> --}}
                                <td align="center">
                                    @if ($item->image != '')
                                        <img src="{{ $item->getImageUrl('small') }}" width="100"/>
                                    @endif
                                </td>
                                <td align="center">{!! $item->created_at ? $item->created_at->format('d/m/Y H:i:s') : '---' !!} </td>

                                {{-- <td align="center">
                                    {{ isset($producttype[$item->type]['name']) ? $producttype[$item->type]['name'] : '' }}
                                </td> --}}
                                @if($user->can('update-products'))
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-light dropdown-toggle" type="button"
                                                id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
                                            <i
                                                    class="fa fa-home {{ $item->is_home ? 'text-danger' : 'text-secondary' }}"></i>
                                            <!-- <i
                                                    class="fa fa-bolt {{ $item->is_quick ? 'text-danger' : 'text-secondary' }}"></i>
                                            <i
                                                    class="fa fa-star-o {{ $item->is_new ? 'text-danger' : 'text-secondary' }}"></i>
                                            <i
                                                    class="fa fa-fire {{ $item->hot ? 'text-danger' : 'text-secondary' }}"></i>
                                            <i
                                                    class="fa fa-search-plus {{ $item->is_top_searching ? 'text-danger' : 'text-secondary' }}"></i>
                                            <i
                                                    class="fa fa-first-order {{ $item->is_sale ? 'text-danger' : 'text-secondary' }}"></i>
                                            <i
                                                    class="fa fa-truck {{ !$item->shipping_required ? 'text-danger' : 'text-secondary' }}"></i>
                                            <i
                                                    class="fa fa-paper-plane {{ $item->is_promotion ? 'text-danger' : 'text-secondary' }}"></i> -->
                                        </button>

                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <div class="mb-2 dropdown-item">
                                                <a href="javascript:void(0)"
                                                   class="{{ $item->is_home ? 'text-danger' : 'text-secondary' }}"
                                                   data-route="{{ route('admin_product_change_status', ['field' => 'is_home']) }}"
                                                   onclick="admin.updateStatus(this,{{ $item->id }}, {{ $item->is_home ? 0 : 1 }})"
                                                   data-toggle="tooltip" data-placement="top"
                                                   data-original-title="{{ $item->is_home ? 'Đang đánh dấu hiện trang chủ, bấm để tắt' : 'Đang bình thường, Click để đánh dấu Hiện trang chủ' }}">
                                                    <i class="fa fa-home"></i>
                                                    Hiện trang chủ
                                                </a>
                                            </div>
                                            <!-- <div class="mb-2 dropdown-item">
                                                <a href="javascript:void(0)"
                                                   class="{{ $item->is_quick ? 'text-danger' : 'text-secondary' }}"
                                                   data-route="{{ route('admin_product_change_status', ['field' => 'is_quick']) }}"
                                                   onclick="admin.updateStatus(this,{{ $item->id }}, {{ $item->is_quick ? 0 : 1 }})"
                                                   data-toggle="tooltip" data-placement="top"
                                                   data-original-title="{{ $item->is_quick ? 'Đang đánh dấu Quick, bấm để tắt' : 'Đang bình thường, Click để đánh dấu Quick' }}">
                                                    <i class="fa fa-bolt"></i>
                                                    Quick
                                                </a>
                                            </div>
                                            <div class="mb-2 dropdown-item">
                                                <a href="javascript:void(0)"
                                                   class="{{ $item->is_new ? 'text-danger' : 'text-secondary' }}"
                                                   data-route="{{ route('admin_product_change_status', ['field' => 'is_new']) }}"
                                                   onclick="admin.updateStatus(this,{{ $item->id }}, {{ $item->is_new ? 0 : 1 }})"
                                                   data-toggle="tooltip" data-placement="top"
                                                   data-original-title="{{ $item->is_new ? 'Đang đánh dấu là Sản phẩm MỚI, bấm để tắt' : 'Đang bình thường, Click để đánh dấu Sản phẩm MỚI' }}">
                                                    <i class="fa fa-star-o"></i>
                                                    Mới
                                                </a>
                                            </div>
                                            <div class="mb-2 dropdown-item">
                                                <a href="javascript:void(0)"
                                                   class="{{ $item->hot ? 'text-danger' : 'text-secondary' }}"
                                                   data-route="{{ route('admin_product_change_status', ['field' => 'hot']) }}"
                                                   onclick="admin.updateStatus(this,{{ $item->id }}, {{ $item->hot ? 0 : 1 }})"
                                                   data-toggle="tooltip" data-placement="top"
                                                   data-original-title="{{ $item->hot ? 'Đang đánh dấu là HOT, bấm để tắt' : 'Đang bình thường, Click để đánh dấu HOT' }}">
                                                    <i class="fa fa-fire"></i>
                                                    HOT
                                                </a>
                                            </div>
                                            <div class="mb-2 dropdown-item">
                                                <a href="javascript:void(0)"
                                                   class="{{ $item->is_top_searching ? 'text-danger' : 'text-secondary' }}"
                                                   data-route="{{ route('admin_product_change_status', ['field' => 'is_top_searching']) }}"
                                                   onclick="admin.updateStatus(this,{{ $item->id }}, {{ $item->is_top_searching ? 0 : 1 }})"
                                                   data-toggle="tooltip" data-placement="top"
                                                   data-original-title="{{ $item->is_top_searching ? 'Đang đánh dấu là Top Search, bấm để tắt' : 'Đang bình thường, Click để đánh dấu Top Search' }}">
                                                    <i class="fa fa-search-plus"></i>
                                                    Top search
                                                </a>
                                            </div>
                                            <div class="mb-2 dropdown-item">
                                                <a href="javascript:void(0)"
                                                   class="{{ $item->is_sale ? 'text-danger' : 'text-secondary' }}"
                                                   data-route="{{ route('admin_product_change_status', ['field' => 'is_sale']) }}"
                                                   onclick="admin.updateStatus(this,{{ $item->id }}, {{ $item->is_sale ? 0 : 1 }})"
                                                   data-toggle="tooltip" data-placement="top"
                                                   data-original-title="{{ $item->is_sale ? 'Đang đánh dấu là Bán chạy, bấm để tắt' : 'Đang bình thường, Click để đánh dấu Bán chạy' }}">
                                                    <i class="fa fa-first-order"></i>
                                                    Bán chạy
                                                </a>
                                            </div>

                                            <div class="mb-2 dropdown-item">
                                                <a href="javascript:void(0)"
                                                   class="{{ !$item->shipping_required ? 'text-danger' : 'text-secondary' }}"
                                                   data-route="{{ route('admin_product_change_status', ['field' => 'shipping_required']) }}"
                                                   onclick="admin.updateStatus(this,{{ $item->id }}, {{ $item->shipping_required ? 0 : 1 }})"
                                                   data-toggle="tooltip" data-placement="top"
                                                   data-original-title="{{ !$item->shipping_required ? 'Đang đánh dấu là FreeShip, bấm để tắt' : 'Đang bình thường, Click để đánh dấu FreeShip' }}">
                                                    <i class="fa fa-truck"></i>
                                                    FreeShip
                                                </a>
                                            </div>

                                            <div class="mb-2 dropdown-item">
                                                <a href="javascript:void(0)"
                                                   class="{{ $item->is_promotion ? 'text-danger' : 'text-secondary' }}"
                                                   data-route="{{ route('admin_product_change_status', ['field' => 'is_promotion']) }}"
                                                   onclick="admin.updateStatus(this,{{ $item->id }}, {{ $item->is_promotion ? 0 : 1 }})"
                                                   data-toggle="tooltip" data-placement="top"
                                                   data-original-title="{{ $item->is_promotion ? 'Đang đánh dấu là KM, bấm để tắt' : 'Đang bình thường, Click để đánh dấu KM' }}">
                                                    <i class="fa fa-paper-plane"></i>
                                                    Khuyến Mãi
                                                </a>
                                            </div> -->
                                        </div>

                                    </div>
                                </td>
                                @endif
                                {{--<td align="center">
                                    <div class="mb-2">
                                        <i class="fa fa-exchange   @if(empty($item->eshop_product_id)) text-secondary @else text-danger  @endif"></i>
                                    </div>
                                </td>--}}
                                <td align="center">
                                    @if($user->can('update-products'))
                                    <div class="mb-2">
                                        <a href="javascript:void(0)"
                                           class="{{ $item->status == 2 ? 'text-success' : 'text-secondary' }}"
                                           data-route="{{ route('admin_product_change_status', ['field' => 'status']) }}"
                                           onclick="admin.updateStatus(this,{{ $item->id }}, {{ $item->status == 2 ? 1 : 2 }})"
                                           data-toggle="tooltip" data-placement="top"
                                           data-original-title="{{ $item->status == 2 ? 'Đang hiển thị, Click để ẩn' : 'Đang ẩn, Click để hiển thị' }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </div>

                                    <div data-toggle="tooltip" data-placement="top" data-original-title="Chỉnh sửa">
                                        <a href="{{ route('admin_product_edit_page', $item->id) }}"
                                           class="text-primary"><i class="fa fa-pencil"></i></a>
                                    </div>
                                    @endif
                                    @if($user->can('delete-products'))
                                    @if($item->status !== -1)
                                        <div class="mt-2" data-toggle="tooltip" data-placement="top"
                                             data-original-title="Xóa sản phẩm">
                                            <a href="{{ route('admin_product_delete_page', $item->id) }}"
                                               class="text-danger" onclick="return confirm('Bạn muốn xóa ?')"><i
                                                        class="fa fa-trash"></i>
                                            </a></div>
                                    @endif
                                    @endif

                                    <div class="mt-2" data-toggle="tooltip" data-placement="top"
                                         data-original-title="Xem sản phẩm">
                                        <a href="{{ $item->routeProductDetail() }}" target="_blank"
                                           class="text-info"><i class="fa fa-chain"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">
                                Không tìm thấy dữ liệu!
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                <div class="m-3">
                    <div class="pull-right">Tổng cộng: {{ $data->total() }} bản ghi / {{ $data->lastPage() }}
                        trang
                    </div>

                    {!! $data->appends($search_data->toArray())->links('basecontainer::admin.inc.paginator') !!}
                </div>
            </div>
        </div>
        <!--/.col-->
    </div>
@stop

@section('css')

@stop
@section('js_bot')

@stop

@push('js_bot_all')

    <script>
        $(document).ready(function () {
            $("#cate_id").select2({
                // width: '100%',
                tags: true,
            });

            $('.datepicker').datetimepicker({
                format: 'd/m/Y',
                timepicker: false
            });
        });
    </script>

@endpush
