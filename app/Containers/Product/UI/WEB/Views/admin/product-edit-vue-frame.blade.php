<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Product Edit</title>
  {{-- <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet"> --}}
  {{-- <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500&display=swap" rel="stylesheet"> --}}
  {{-- <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet"> --}}
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
  <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body id="sectionContent">
  <div id="app">
    <product-edit></product-edit>
  </div>
  <script>
    window.product_id = '{{ $product_id }}';
    window.onbeforeunload = function() {};
  </script>
  <script src="{{ asset('admin/GroupJS/product/product-edit.js') }}?v={{time()}}"></script>
</body>
</html>
