<div class="tab-pane" id="option">
    <div class="tabbable">
        <div class="row">
            <div class="col-sm-2">
                <ul class="nav flex-column nav-pills" id="option-content">
                    @if(isset($product_options))
                    @foreach($product_options as $k_prd_opt => $product_option)
                    <li><a class="nav-link {{$loop->first ? 'active' : ''}}" href="#tab-option{{ $k_prd_opt }}" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-option{{ $k_prd_opt }}\']').parent().remove(); $('#tab-option{{ $k_prd_opt }}').remove(); $('#option a:first').tab('show');"></i> {{ $product_option['name'] }}</a></li>
                    @endforeach
                    @endif
                    <li>
                        <input type="text" value="" placeholder="Tùy chọn" id="input-option" class="form-control mt-3"/>
                    </li>
                </ul>
            </div>
            <div class="col-sm-10">
                <div class="tab-content">
                    @if(isset($product_options))
                    @php($option_value_row = @$option_value_row ?? 0)
                    @foreach($product_options as $k_prd_opt => $product_option)
                    <div class="tab-pane {{$loop->first ? 'active' : ''}}" id="tab-option{{ $k_prd_opt }}">
                        <input type="hidden" name="product_option[{{ $k_prd_opt }}][product_option_id]" value="{{ $product_option['product_option_id'] }}"/>
                        <input type="hidden" name="product_option[{{ $k_prd_opt }}][name]" value="{{ $product_option['name'] }}"/>
                        <input type="hidden" name="product_option[{{ $k_prd_opt }}][option_id]" value="{{ $product_option['option_id'] }}"/>
                        <input type="hidden" name="product_option[{{ $k_prd_opt }}][type]" value="{{ $product_option['type'] }}"/>
                        @if(1 == 2 )
                        //Không muốn cho hiển thị ra, tùy dự án, cần thì cho
                        <div class="form-group row align-items-center">
                            <label class="col-sm-2 control-label text-right mb-0" for="input-required{{ $k_prd_opt }}">Required</label>
                            <div class="col-sm-10">
                                <select name="product_option[{{ $k_prd_opt }}][required]" id="input-required{{ $k_prd_opt }}" class="form-control">
                                    <option value="1" {{$product_option['required'] ? 'selected="selected"' : ''}} >Có</option>
                                    <option value="0" {{$product_option['required'] ? '' : 'selected="selected"'}}>Không</option>
                                </select>
                            </div>
                        </div>
                        @endif

                        @include('product::admin.components.type_option',['option_value_row' => &$option_value_row])
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@push('js_bot_all')
    <script>
        $('.lfm').filemanager('image');

        $('#option').on('click','div.table-opts .wrap-img-opt i.fa-close',function(e){
            var ele = $(this);
            var target_input = $('#'+ele.attr('data-input-imgs'));
            let current_imgs = target_input.val();
            current_imgs = current_imgs ? JSON.parse(target_input.val()) : [];
            if(Array.isArray(current_imgs)) {
                let filtered = current_imgs.filter(function(obj){
                    return obj.thumb_nail != ele.attr('data-src')
                });

                target_input.val(Array.isArray(filtered) ? JSON.stringify(filtered) : '').trigger('change');

                ele.parent().remove();
            }
        });

        // var option_value_row = {{ isset($option_value_row) ? $option_value_row : 0 }};

        function addOptionValue(option_row,ele) {
            var option_value_row = parseInt($(ele).attr('data-current-row'));

            html = '<tr id="option-value-row' + option_value_row + '">';
            html += '  <td class="text-left"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][option_value_id]" class="form-control">';
            html += $('#option-values' + option_row).html();
            html += '  </select><input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][product_option_value_id]" value="" /></td>';
            // html += '  <td class="text-right"><input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][quantity]" value="" placeholder="Quantity" class="form-control" /></td>';
            // html += '  <td class="text-left"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][subtract]" class="form-control">';
            // html += '    <option value="1">Yes</option>';
            // html += '    <option value="0">No</option>';
            // html += '  </select></td>';
            html += '  <td class="text-right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][price_prefix]" class="form-control">';
            html += '    <option value="+">+</option>';
            html += '    <option value="-">-</option>';
            html += '  </select>';
            html += '  <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][price]" value="" placeholder="Giá trị" class="form-control" onkeypress="return shop.numberOnly()" onkeyup="shop.mixMoney(this)" onfocus="this.select()" /></td>';
            // html += '  <td class="text-right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][points_prefix]" class="form-control">';
            // html += '    <option value="+">+</option>';
            // html += '    <option value="-">-</option>';
            // html += '  </select>';
            // html += '  <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][points]" value="" placeholder="Điểm" class="form-control" /></td>';
            // html += '  <td class="text-right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][weight_prefix]" class="form-control">';
            // html += '    <option value="+">+</option>';
            // html += '    <option value="-">-</option>';
            // html += '  </select>';
            // html += '  <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][weight]" value="" placeholder="Trọng lượng" class="form-control" /></td>';
            html += '<td>';
            html += '        <input id="input-imgs-'+option_row+'-'+option_value_row+'" type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][imgs]" />';
            html += '        <a id="a-btn-imgs-'+option_row+'-'+option_value_row+'" data-input="input-imgs-'+option_row+'-'+option_value_row+'" data-preview="preview-imgs-'+option_row+'-'+option_value_row+'" class="btn btn-primary lfm" ><i class="fa fa-picture-o"></i> Chọn ảnh</a>';
            html += '        <div id="preview-imgs-'+option_row+'-'+option_value_row+'" class="preview-imgs mt-3"></div>';
            html += '    </td>';
            html += '  <td class="text-right"><button type="button" onclick="$(\'#option-value-row' + option_value_row + '\').remove();" data-toggle="tooltip" rel="tooltip" title="Xóa" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#option-value' + option_row + ' tbody').append(html);
            $('[rel=tooltip]').tooltip();
            $('#a-btn-imgs-'+option_row+'-'+option_value_row).filemanager('image');

            option_value_row++;
            $(ele).attr('data-current-row',option_value_row);
        }

        $('.date').datetimepicker({
            {{--language: '{{ datepicker }}',--}}
            pickTime: false
        });

        $('.time').datetimepicker({
            {{--language: '{{ datepicker }}',--}}
            pickDate: false
        });

        $('.datetime').datetimepicker({
{{--            language: '{{ datepicker }}',--}}
            pickDate: true,
            pickTime: true
        });

        var option_row = {{ count(@$product_options ?? []) }};
        $('#input-option').autocomplete({
            classes: {
                "ui-autocomplete": "dropdown-menu"
            },
            'source': function(request, response) {
                $.ajax({
                    url: '{{route('admin_option_get_autôcmplete')}}',
                    dataType: 'json',
                    data: {name:request.term,_token:ENV.token},
                    success: function(json) {
                        response($.map(json.data, function(item) {
                            return {
                                category: item['category'],
                                label: item['name'],
                                value: item['option_id'],
                                type: item['type'],
                                option_value: item['option_value']
                            }
                        }));
                    }
                });
            },
            'select': function(event, ui) {

                var item = ui.item;
                console.log(option_row);
                html = '<div class="tab-pane" id="tab-option' + option_row + '">';
                html += '	<input type="hidden" name="product_option[' + option_row + '][product_option_id]" value="" />';
                html += '	<input type="hidden" name="product_option[' + option_row + '][name]" value="' + item['label'] + '" />';
                html += '	<input type="hidden" name="product_option[' + option_row + '][option_id]" value="' + item['value'] + '" />';
                html += '	<input type="hidden" name="product_option[' + option_row + '][type]" value="' + item['type'] + '" />';

                // html += '	<div class="form-group row align-items-center">';
                // html += '	  <label class="col-sm-2 control-label text-right mb-0" for="input-required' + option_row + '">Required</label>';
                // html += '	  <div class="col-sm-10"><select name="product_option[' + option_row + '][required]" id="input-required' + option_row + '" class="form-control">';
                // html += '	      <option value="1">Có</option>';
                // html += '	      <option value="0">Không</option>';
                // html += '	  </select></div>';
                // html += '	</div>';

                if (item['type'] == 'text') {
                    html += '	<div class="form-group row align-items-center">';
                    html += '	  <label class="col-sm-2 control-label text-right mb-0" for="input-value' + option_row + '">Nội dung tùy chọn</label>';
                    html += '	  <div class="col-sm-10"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="Nội dung tùy chọn" id="input-value' + option_row + '" class="form-control" /></div>';
                    html += '	</div>';
                }

                if (item['type'] == 'textarea') {
                    html += '	<div class="form-group row align-items-center">';
                    html += '	  <label class="col-sm-2 control-label text-right mb-0" for="input-value' + option_row + '">Nội dung tùy chọn</label>';
                    html += '	  <div class="col-sm-10"><textarea name="product_option[' + option_row + '][value]" rows="5" placeholder="Nội dung tùy chọn" id="input-value' + option_row + '" class="form-control"></textarea></div>';
                    html += '	</div>';
                }

                if (item['type'] == 'file') {
                    html += '	<div class="form-group" style="display: none;">';
                    html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '">Nội dung tùy chọn</label>';
                    html += '	  <div class="col-sm-10"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="Nội dung tùy chọn" id="input-value' + option_row + '" class="form-control" /></div>';
                    html += '	</div>';
                }

                if (item['type'] == 'date') {
                    html += '	<div class="form-group row align-items-center">';
                    html += '	  <label class="col-sm-2 control-label text-right mb-0" for="input-value' + option_row + '">Nội dung tùy chọn</label>';
                    html += '	  <div class="col-sm-3"><div class="input-group date"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="Nội dung tùy chọn" data-date-format="YYYY-MM-DD" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
                    html += '	</div>';
                }

                if (item['type'] == 'time') {
                    html += '	<div class="form-group row align-items-center">';
                    html += '	  <label class="col-sm-2 control-label text-right mb-0" for="input-value' + option_row + '">Nội dung tùy chọn</label>';
                    html += '	  <div class="col-sm-10"><div class="input-group time"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="Nội dung tùy chọn" data-date-format="HH:mm" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
                    html += '	</div>';
                }

                if (item['type'] == 'datetime') {
                    html += '	<div class="form-group row align-items-center">';
                    html += '	  <label class="col-sm-2 control-label text-right mb-0" for="input-value' + option_row + '">Nội dung tùy chọn</label>';
                    html += '	  <div class="col-sm-10"><div class="input-group datetime"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="Nội dung tùy chọn" data-date-format="YYYY-MM-DD HH:mm" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
                    html += '	</div>';
                }

                if (item['type'] == 'select' || item['type'] == 'radio' || item['type'] == 'checkbox' || item['type'] == 'image') {
                    html += '<div class="table-responsive table-opts">';
                    html += '  <table id="option-value' + option_row + '" class="table table-striped table-bordered table-hover  table-options"">';
                    html += '  	 <thead>';
                    html += '      <tr>';
                    html += '        <td class="text-left">Nội dung tùy chọn</td>';
                    // html += '        <td class="text-right">Quantity</td>';
                    // html += '        <td class="text-left">Trừ tồn kho</td>';
                    html += '        <td class="text-right">Giá trị</td>';
                    // html += '        <td class="text-right">Tích điểm</td>';
                    // html += '        <td class="text-right">Trọng lượng</td>';
                    html += '        <td></td>';
                    html += '      </tr>';
                    html += '  	 </thead>';
                    html += '  	 <tbody>';
                    html += '    </tbody>';
                    html += '    <tfoot>';
                    html += '      <tr>';
                    html += '        <td colspan="6"></td>';
                    html += '        <td class="text-left"><button data-current-row="0" type="button" onclick="addOptionValue(' + option_row + ',this);" data-toggle="tooltip" title="Thêm" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>';
                    html += '      </tr>';
                    html += '    </tfoot>';
                    html += '  </table>';
                    html += '</div>';

                    html += '  <select id="option-values' + option_row + '" style="display: none;">';

                    for (i = 0; i < item['option_value'].length; i++) {
                        html += '  <option value="' + item['option_value'][i]['option_value_id'] + '">' + item['option_value'][i]['name'] + '</option>';
                    }

                    html += '  </select>';
                    html += '</div>';
                }

                $('#option .tab-content').append(html);

                $('#option-content > li:last-child').before('<li><a class="nav-link" href="#tab-option' + option_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick=" $(\'#option-content a:first\').tab(\'show\');$(\'a[href=\\\'#tab-option' + option_row + '\\\']\').parent().remove(); $(\'#tab-option' + option_row + '\').remove();"></i> ' + item['label'] + '</li>');

                $('#option-content a[href=\'#tab-option' + option_row + '\']').tab('show');

                $('[data-toggle=\'tooltip\']').tooltip({
                    container: 'body',
                    html: true
                });

                $('.date').datetimepicker({
                    pickTime: false
                });

                $('.time').datetimepicker({
                    pickDate: false
                });

                $('.datetime').datetimepicker({
                    pickDate: true,
                    pickTime: true
                });
                $(this).val('');
                option_row++;

                return false;
            }
        });
    </script>
@endpush
