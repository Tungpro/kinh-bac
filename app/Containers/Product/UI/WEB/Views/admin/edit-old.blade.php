@extends('basecontainer::admin.layouts.default')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if (isset($editMode) && $editMode)
                {!! Form::open(['url' => route('admin_product_edit_page', $data->id), 'files' => true]) !!}
            @else
                {!! Form::open(['url' => route('admin_product_add_page'), 'files' => true]) !!}
            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            <div class="card card-accent-primary">
                <div class="card-header">
                    <i class="fa fa-pencil"></i>THÔNG TIN SẢN PHẨM
                </div>
                <div class="card-body">
                    <div class="tabbable boxed parentTabs">
                        <ul class="nav nav-tabs nav-underline nav-underline-primary mb-3">
                            <li class="nav-item">
                                <a class="nav-link active" href="#general"><i class="fa fa-empire"></i> Chung</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#data"><i class="fa fa-deviantart"></i> Thông tin chi tiết</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#links"><i class="fa fa-link"></i> Liên kết</a>
                            </li>
                            {{-- <li class="nav-item"> --}}
                            {{-- <a class="nav-link"  href="#attribute" ><i class="fa fa-balance-scale"></i> Thông số</a> --}}
                            {{-- </li> --}}
                            <li class="nav-item">
                                <a class="nav-link" href="#option"><i class="fa fa-anchor"></i> Tùy chọn</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#image"><i class="fa fa-image"></i> Hình ảnh</a>
                            </li>
                        </ul>

                        <div class="tab-content p-0">
                            @include('product::admin.edit_tabs.general')

                            @include('product::admin.edit_tabs.data')

                            @include('product::admin.edit_tabs.links')

                            {{-- @components('BackEnd::pages.product.edit_tabs.attribute') --}}

                            @include('product::admin.edit_tabs.option')

                            @include('product::admin.edit_tabs.image')
                        </div>
                    </div>
                </div>
            </div>

            {{-- <div class="card card-accent-primary"> --}}
            {{-- <div class="card-header"> --}}
            {{-- <i class="fa fa-pencil"></i>THÔNG TIN GIÁ CƠ SỞ --}}
            {{-- </div> --}}
            {{-- <div class="card-body"> --}}
            {{-- <div class="row"> --}}
            {{-- <div class="col-sm-3"> --}}
            {{-- <div class="form-group"> --}}
            {{-- <label for="base_price">Giá hiển thị</label> --}}
            {{-- <input type="text" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" id="base_price" name="base_price" value="{{ old_blade('price') ? \FunctionLib::numberFormat(old_blade('price')) : 0 }}" required onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()"> --}}
            {{-- </div> --}}
            {{-- </div> --}}
            {{-- <div class="col-sm-3"> --}}
            {{-- <div class="form-group"> --}}
            {{-- <label for="base_priceStrike">Giá gạch</label> --}}
            {{-- <input type="text" class="form-control{{ $errors->has('priceStrike') ? ' is-invalid' : '' }}" id="base_priceStrike" name="base_priceStrike" value="{{ \old_blade('priceStrike') ? Lib::numberFormat(old_blade('priceStrike')) : 0 }}" onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()"> --}}
            {{-- </div> --}}
            {{-- </div> --}}
            {{-- <div class="col-sm-3"> --}}
            {{-- <div class="form-group"> --}}
            {{-- <label for="unit">Đơn vị</label> --}}
            {{-- <input type="text" class="form-control{{ $errors->has('unit') ? ' is-invalid' : '' }}" id="unit" name="unit" value="{{ old_blade('unit')}}"> --}}
            {{-- </div> --}}
            {{-- </div> --}}
            {{-- </div> --}}
            {{-- </div> --}}
            {{-- </div> --}}

            {{-- <div class="card card-accent-primary"> --}}
            {{-- <div class="card-header"> --}}
            {{-- <i class="fa fa-pencil"></i>THÔNG TIN GIÁ VÀ SỐ LƯỢNG --}}
            {{-- </div> --}}
            {{-- <div class="card-body"> --}}
            {{-- @components('BackEnd::pages.product.components.price_infor') --}}
            {{-- </div> --}}
            {{-- </div> --}}

            {{-- <div class="card card-accent-primary"> --}}
            {{-- <div class="card-header"> --}}
            {{-- <i class="fa fa-pencil"></i>THÔNG TIN HẠNG MỤC SẢN PHẨM --}}
            {{-- </div> --}}
            {{-- <div class="card-body"> --}}
            {{-- @components('BackEnd::pages.product.components.filter_infor') --}}
            {{-- </div> --}}
            {{-- </div> --}}

            {{-- <div class="card card-accent-primary"> --}}
            {{-- <div class="card-header"> --}}
            {{-- <i class="fa fa-image"></i>Khối lượng --}}
            {{-- </div> --}}
            {{-- <div class="card-body"> --}}
            {{-- <div class="row"> --}}
            {{-- <div class="col-sm-6"> --}}
            {{-- <div class="form-group"> --}}
            {{-- <label for="filters"></label> --}}
            {{-- @if (\FunctionLib::can($permission, 'filter')) --}}
            {{-- <input type="text" class="form-control" id="filters" name="filters" value="{{ $filters }}" required> --}}
            {{-- @else --}}
            {{-- <input type="text" class="form-control text-danger" value="Chưa có quyền gán bộ lọc" disabled> --}}
            {{-- @endif --}}
            {{-- </div> --}}
            {{-- </div> --}}
            {{-- </div> --}}
            {{-- </div> --}}
            {{-- </div> --}}

            <div class="mb-3">
                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Cập nhật</button>
                &nbsp;&nbsp;
                <a class="btn btn-sm btn-danger" href="{{ redirect()->back()->getTargetUrl() }}"><i class="fa fa-ban"></i>
                    Quay lại</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('css')
    {!! \FunctionLib::addMedia('admin/js/library/tag/jquery.tag-editor.css') !!}
    {{-- {!! \FunctionLib::addMedia('admin/js/library/chosen/chosen.min.css') !!} --}}
    {!! \FunctionLib::addMedia('admin/js/library/uploadifive/uploadifive.css') !!}
@stop

@section('js_bot')
    {!! \FunctionLib::addMedia('admin/js/library/tinymce/tinymce.min.js') !!}
    {!! \FunctionLib::addMedia('admin/js/vue.js') !!}
    {!! \FunctionLib::addMedia('admin/js/library/uploadifive/jquery.uploadifive.min.js') !!}
    {!! \FunctionLib::addMedia('admin/js/library/uploadifive/multiupload.js') !!}
    {!! \FunctionLib::addMedia('admin/js/library/tinymce/tinymce.min.js') !!}
    {!! \FunctionLib::addMedia('admin/js/library/tag/jquery.caret.min.js') !!}
    {!! \FunctionLib::addMedia('admin/js/library/tag/jquery.tag-editor.min.js') !!}
    {!! \FunctionLib::addMedia('admin/js/library/jquery.form.js') !!}
    {!! \FunctionLib::addMedia('admin/js/library/jquery.sortable.js') !!}\
    {!! \FunctionLib::addMedia('admin/js/lfm-img-option-picking.js') !!}

    <script type="text/javascript">
        $("ul.nav-tabs a").click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });

        admin.system.tinyMCE('.prd_editor', plugins = '', menubar = '', toolbar = '', height = 300);

        //         shop.ready.add(function(){
        // {{-- @if (\FunctionLib::can($permission, 'tag')) --}}
        // {{-- shop.admin.tags.init({{ $tagType }}, '#tags', {{ @$data->id }}); --}}
        // {{-- @endif --}}

        //         }, true);

    </script>
@stop
