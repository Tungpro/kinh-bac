@extends('basecontainer::admin.layouts.default')

@section('content')
  <div class="row" id="productEdit">
    <product-edit></product-edit>
  </div>
@endsection

@push('js_bot_all')
<script>
  window.product_id = '{{ $input['id'] }}';
  $(window).bind('beforeunload', function() {} );
</script>
<script src="{{ asset('admin/GroupJS/product/product-edit.js') }}?v={{time()}}"></script>
@endpush
