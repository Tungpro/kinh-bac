<?php

namespace App\Containers\Product\UI\WEB\Controllers\Admin\EshopSync;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\StringLib;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;
use App\Containers\Product\Actions\Api\GetLPrdsNullEshopIdAction;
class Controller extends AdminController
{
    use ApiResTrait;

    public function __construct()
    {

        parent::__construct();
    }

    public function ajaxGetProductByEshopIdNull()
    {

      try {
        $pagi = false;
        $limit = 10;
        $result = app(GetLPrdsNullEshopIdAction::class)->run($pagi, $limit);
        return FunctionLib::ajaxRespond(true, 'Hoàn thành', $result);
      } catch (\Throwable $th) {
        return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
      }
    }
} // End class
