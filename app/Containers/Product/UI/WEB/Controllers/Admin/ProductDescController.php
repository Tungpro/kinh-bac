<?php

namespace App\Containers\Product\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\WebController;
use App\Containers\Product\UI\WEB\Requests\FilterProductRequest;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;

/**
 * Class ProductDescController
 *
 * @package App\Containers\Product\UI\WEB\Controllers
 */
class ProductDescController extends WebController
{
  use ApiResTrait;

  public function filter(FilterProductRequest $request)
  {
    $transporter = $request->toTransporter();
    $products = Apiato::call('Product@ProductDesc\FilterProductBySearchableAction', [
      $transporter,
      [],
      [
        'id',
        'product_id',
        'name'
      ]
    ]);
    return $this->sendResponse($products);
  }
} // End class
