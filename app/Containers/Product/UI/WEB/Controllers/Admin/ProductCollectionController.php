<?php

namespace App\Containers\Product\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Ship\Parents\Controllers\AdminController;
use App\Containers\Product\UI\WEB\Requests\Admin\GetAllProductCollectionRequest;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;

/**
 * Class ProductCollectionController
 *
 * @package App\Containers\Product\UI\WEB\Controllers
 */
class ProductCollectionController extends AdminController
{
  use ApiResTrait;

  public function __construct()
  {
    if (FunctionLib::isDontUseShareData([])) {
      $this->dontUseShareData = true;
    }

    parent::__construct();
  }

  /**
   * Show all entities
   *
   * @param GetAllProductsRequest $request
   */
  public function index(GetAllProductCollectionRequest $request)
  {
    $transporter = $request->toTransporter();
    $productCollections = Apiato::call('Product@ProductCollection\GetAllProductCollectionAction', [
      $transporter,
      ['desc:id,image,name,product_collection_id']
    ]);
    return $this->sendResponse($productCollections);
  }

  /**
   * Show one entity
   *
   * @param FindProductByIdRequest $request
   */
  public function show(FindProductByIdRequest $request)
  {
    $product = Apiato::call('Product@FindProductByIdAction', [$request]);

    // ..
  }

  /**
   * Create entity (show UI)
   *
   * @param CreateProductRequest $request
   */
  public function create(CreateProductRequest $request)
  {
    // ..
  }

  /**
   * Add a new entity
   *
   * @param StoreProductRequest $request
   */
  public function store(StoreProductRequest $request)
  {
    $product = Apiato::call('Product@CreateProductAction', [$request]);

    // ..
  }

  /**
   * Edit entity (show UI)
   *
   * @param EditProductRequest $request
   */
  public function edit(EditProductRequest $request)
  {
    $product = Apiato::call('Product@GetProductByIdAction', [$request]);

    // ..
  }

  /**
   * Update a given entity
   *
   * @param UpdateProductRequest $request
   */
  public function update(UpdateProductRequest $request)
  {
    $product = Apiato::call('Product@UpdateProductAction', [$request]);

    // ..
  }

  /**
   * Delete a given entity
   *
   * @param DeleteProductRequest $request
   */
  public function delete(DeleteProductRequest $request)
  {
    $result = Apiato::call('Product@DeleteProductAction', [$request]);

    // ..
  }
} // End class
