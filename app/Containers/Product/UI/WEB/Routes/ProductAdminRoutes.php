<?php
Route::group(
[
    'prefix' => 'products',
    'namespace' => '\App\Containers\Product\UI\WEB\Controllers\Admin',
    'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
    'middleware' => [
        'auth:admin',
    ],
],function () use ($router) {
    $router->get('/', [
        'as'   => 'admin_product_home_page',
        'uses' => 'Controller@index',
    ]);

    $router->get('/edit/{id}', [
        'as'   => 'admin_product_edit_page',
        'uses' => 'Controller@edit',
    ]);

    $router->post('/edit/{id}', [
        'as'   => 'admin_product_edit_page',
        'uses' => 'Controller@update',
    ]);

    $router->get('/add', [
        'as'   => 'admin_product_add',
        'uses' => 'Controller@add',
    ]);

    $router->get('/{id}', [
        'as'   => 'admin_product_delete_page',
        'uses' => 'Controller@delete',
    ]);

    $router->get('/filter', [
        'as' => 'admin.products.filter',
        'uses' => 'ProductDescController@filter',
    ]);

    $router->get('/collection', [
        'as' => 'admin.pdc.index',
        'uses' => 'ProductCollectionController@index',
    ]);

    $router->post('/uploadImgGallery', [
        'as'   => 'admin_product_upload_img_gallery',
        'uses' => 'Controller@uploadImgGallery',
    ]);

    $router->post('/change-pos', [
        'as'   => 'admin_product_change_pos_gallery',
        'uses' => 'Controller@ajaxItemChangePos',
    ]);

    $router->post('/remove_img', [
        'as'   => 'admin_product_remove_img_gallery',
        'uses' => 'Controller@ajaxItemImgDel',
    ]);

    $router->get('/edit-prod-frame/{id}', [
        'as' => 'admin.product-edit-frame',
        'uses' => 'Controller@editProductFrame'
    ]);

    $router->post('/status/{field}', [
        'as'   => 'admin_product_change_status',
        'uses' => 'Controller@updateSomeStatus',
    ]);
});
