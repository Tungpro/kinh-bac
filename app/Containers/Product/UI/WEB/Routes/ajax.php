<?php

use Illuminate\Support\Facades\Route;

Route::namespace('\App\Containers\Product\UI\WEB\Controllers\Admin')->prefix('admin/ajax')->group(function () {
    Route::prefix('products')->group(function () {
        Route::get('/{productId}/variants', [
            'as' => 'admin.products.variants.findByProductId',
            'uses' => 'AjaxProductVariantController@ajaxFindVariantByProductId',
            'middleware' => [
                'auth:admin',
            ]
        ]);
        Route::POST('/{productId}/variantsByIDs', [
            'as' => 'admin.products.variants.variantsByIDs',
            'uses' => 'AjaxProductVariantController@ajaxVariantsByIDs',
            'middleware' => [
                'auth:admin',
            ]
        ]);
        Route::get('/{product_id}-{lang_id}/controller/', [
            'as' => 'admin.products.controller.productByID',
            'uses' => 'Controller@ajaxProductByID',
            'middleware' => [
                'auth:admin',
            ]
        ]);

        Route::get('/{product_id}/controller/imgData', [
            'as' => 'admin.products.controller.imgData',
            'uses' => 'Controller@ajaxImgData',
            'middleware' => [
                'auth:admin',
            ]
        ]);

        Route::get('/controller/lang', [
            'as' => 'admin.products.controller.lang',
            'uses' => 'Controller@ajaxGetLang',
            'middleware' => [
                'auth:admin',
            ]
        ]);
        Route::get('/{cate_id}/controller/getPrdByIdCate', [
            'as' => 'admin.product.category.getPrdByIdCate',
            'uses' => 'ProductCategoryController@ajaxGetPrdByIdCate',
            'middleware' => [
                'auth:admin',
            ]
        ]);

        Route::get('/{cate_id}/controller/getPrdByIdCateForOffset', [
            'as' => 'admin.product.category.getPrdByIdCateForOffset',
            'uses' => 'ProductCategoryController@ajaxGetPrdByIdCateForOffset',
            'middleware' => [
                'auth:admin',
            ]
        ]);

        Route::get('/{product_id}/controller/prdData', [
            'as' => 'admin.products.controller.prdData',
            'uses' => 'Controller@ajaxProductDataByID',
            'middleware' => [
                'auth:admin',
            ]
        ]);

        Route::get('/{product_id}-{img_id}/controller/itemImgDel', [
            'as' => 'admin.products.controller.itemImgDel',
            'uses' => 'Controller@ajaxItemImgDel',
            'middleware' => [
                'auth:admin',
            ]
        ]);
        Route::post('/{product_id}/controller/save', [
            'as' => 'admin.products.controller.save',
            'uses' => 'Controller@ajaxSaveProductDetail',
            'middleware' => [
                'auth:admin',
            ]
        ]);

        Route::post('/controller/getIdsPrd', [
            'as' => 'admin.products.controller.getIdsPrd',
            'uses' => 'Controller@ajaxGetIdsPrd',
            'middleware' => [
                'auth:admin',
            ]
        ]);

        Route::post('/controller/imgUpload', [
            'as' => 'admin.products.controller.imgUpload',
            'uses' => 'Controller@uploadImgsGallery',
            'middleware' => [
                'auth:admin',
            ]
        ]);

        Route::post('/controller/imgUploadHome', [
            'as' => 'admin.products.controller.imgUpload',
            'uses' => 'Controller@uploadImgsHomeGallery',
            'middleware' => [
                'auth:admin',
            ]
        ]);

        Route::get('/controller/getStock', [
            'as' => 'api_stock_get_all',
            'uses' => 'Controller@ajaxStockStatus',
            'middleware' => [
                'auth:admin',
            ]
        ]);
        Route::get('/controller/getLPrdsNullEshopIdAction', [
            'as' => 'api_prd_null_id',
            'uses' => 'EshopSync\Controller@ajaxGetProductByEshopIdNull',
            'middleware' => [
                'auth:admin',
            ]
        ]);

        Route::get('/controller/remove_product_not_root', [
            'as' => 'admin_product_remove_not_root',
            'uses' => 'EshopSync\Controller@removeProductNotRoot',
            'middleware' => [
                'auth:admin',
            ]
        ]);

        Route::post('/{product_id}/variants/store', [
            'as' => 'admin.products.variants.store',
            'uses' => 'AjaxProductVariantController@ajaxStoreProductVariant',
            'middleware' => [
                'auth:admin',
            ]
        ]);

        Route::post('/{product_id}/variants/storeUpdate', [
            'as' => 'admin.products.variants.storeUpdate',
            'uses' => 'AjaxProductVariantController@ajaxStoreUpdateProductVariant',
            'middleware' => [
                'auth:admin',
            ]
        ]);

        Route::post('/controller/itemImgUpdate', [
            'as' => 'admin.products.controller.itemImgUpdate',
            'uses' => 'Controller@ajaxItemImgUpdate',
            'middleware' => [
                'auth:admin',
            ]
        ]);

        Route::delete('/{product_id}/variants/{product_variant_id}', [
            'as' => 'admin.products.variants.delete',
            'uses' => 'AjaxProductVariantController@ajaxDeleteProductVariant',
            'middleware' => [
                'auth:admin',
            ]
        ]);
    });
});
