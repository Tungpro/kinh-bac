<?php

namespace App\Containers\Product\UI\WEB\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class AjaxStoreProductVariantRequest.
 */
class AjaxGetProductRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\Product\Data\Transporters\DefaultTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => 'update-products',
        'roles'       => 'admin',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        // 'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        'product_id',
        'lang_id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
//            'product_description.*.name'           => 'required|max:255',
        ];
    }

        /**
     * Custom message for validation
     *
     * @return array
     */

    public function messages()
    {
        return [
//            'product_description.*.name.required' => 'Tên không được bỏ trống',
//            'product_description.*.name.max' => 'Tên tối đa 255 ký tự',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
