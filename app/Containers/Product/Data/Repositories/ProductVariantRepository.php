<?php

namespace App\Containers\Product\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ProductVariantRepository
 */
class ProductVariantRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id' => '=',
        // ...
    ];

}
