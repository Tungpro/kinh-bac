<?php

namespace App\Containers\Product\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ProductRepository.
 */
class ProductRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Product';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'sku',
        'status',
        'created_at',
        'desc.name',
        'desc.language_id'
    ];
}
