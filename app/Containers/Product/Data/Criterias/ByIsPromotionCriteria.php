<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-06 13:36:46
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-03 22:25:11
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class ByIsPromotionCriteria extends Criteria
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where('is_promotion', $this->value);
    }
}
