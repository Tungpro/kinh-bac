<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-06-23 21:08:24
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-08 22:50:18
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class ByIsNewCriteria extends Criteria
{
    private $isNew;

    public function __construct($isNew)
    {
        $this->isNew = $isNew;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where('is_new', $this->isNew);
    }
}
