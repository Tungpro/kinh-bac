<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-11 12:42:18
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class ByCategoriesCriteria extends Criteria
{
    private $cate_ids;

    public function __construct($cate_ids)
    {
        $this->cate_ids = $cate_ids;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->whereHas('categories', function (Builder $q) {
            $q->whereIn('category.category_id', $this->cate_ids);
        });
    }
}
