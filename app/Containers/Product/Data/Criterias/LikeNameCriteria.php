<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-06 13:38:18
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class LikeNameCriteria extends Criteria
{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->whereHas('desc', function (Builder $query) {
            $query->where('name', 'like', '%' . $this->name . '%');
        });
    }
}
