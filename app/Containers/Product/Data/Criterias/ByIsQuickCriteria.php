<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-01 16:00:53
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class ByIsQuickCriteria extends Criteria
{
    private $isQuick;

    public function __construct($isQuick)
    {
        $this->isQuick = $isQuick;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where('is_quick', $this->isQuick);
    }
}
