<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-06 13:38:06
 * @ Description: Happy Coding!
 */


namespace App\Containers\Product\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class ByTypeCriteria extends Criteria
{
    private $type;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where('type', $this->type);
    }
}
