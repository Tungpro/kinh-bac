<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-06-23 21:08:24
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-24 01:26:10
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Data\Criterias;

use App\Ship\Parents\Criterias\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class ByIsTopSearchingCriteria extends Criteria
{
    private $isTopSearching;

    public function __construct($isTopSearching)
    {
        $this->isTopSearching = $isTopSearching;
    }

    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where('is_top_searching', $this->isTopSearching);
    }
}
