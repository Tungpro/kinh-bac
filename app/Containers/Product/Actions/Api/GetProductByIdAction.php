<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-03 13:46:27
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-21 17:54:35
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\Api;

use App\Containers\Localization\Models\Language;
use App\Containers\Product\Models\Product;
use App\Containers\Product\Tasks\GetProductByIdForApiTask;
use App\Ship\Parents\Actions\Action;

class GetProductByIdAction extends Action
{
    protected $getProductByIdForApiTask;

    public function __construct(GetProductByIdForApiTask $getProductByIdForApiTask)
    {
        $this->getProductByIdForApiTask = $getProductByIdForApiTask;
        parent::__construct();
    }

    public function run(int $productId): ?Product
    {
        return $this->remember(function () use ($productId) {
            $data = $this->getProductByIdForApiTask->run($productId);

            return $data;
        });
    }

    public function currentLang(Language $currentLang = null): self
    {
        $this->currentLang = $currentLang;
        return $this;
    }

    public function mustHaveDesc(): self
    {
        $this->getProductByIdForApiTask->currentLang($this->currentLang)->mustHaveDescByLang();
        return $this;
    }
}
