<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-03 13:46:27
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-17 10:10:10
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\Api;

use App\Containers\Localization\Models\Language;
use App\Containers\Product\Actions\Api\SubActions\GetSuggestionSubAction;
use App\Containers\Product\Actions\Api\SubActions\GetSimilarSubAction;
use App\Containers\Product\Actions\ProductVariant\SubActions\FindVariantByProductIdSubAction;
use App\Containers\Product\Models\Product;
use App\Containers\Product\Tasks\GetProductByIdForApiTask;
use App\Containers\PromotionCampaign\Actions\FrontEnd\SubActions\CaclcRunningTimeLineSubAction;
use App\Containers\PromotionCampaign\Actions\FrontEnd\SubActions\GetMostRecentlyCampaignOnProductSubAction;
use App\Containers\PromotionCampaign\Actions\SubActions\CalcProductPriceByCampaignSubAction;
use App\Containers\PromotionCampaign\Tasks\FrontEnd\GetProductByCampaignIdTask;
use App\Ship\Parents\Actions\Action;

class GetProductByIdForApiAction extends Action
{
    protected $getProductByIdForApiTask;
    protected $withExtraData = false;
    //protected $getVariantByidTask;
    //protected $getAllProductsTask;

    public function __construct(
        GetProductByIdForApiTask $getProductByIdForApiTask
        /*GetVariantByidTask $getVariantByidTask,*/
        /*GetAllProductsTask $getAllProductsTask*/
    )
    {
        $this->getProductByIdForApiTask = $getProductByIdForApiTask;
        //$this->getVariantByidTask = $getVariantByidTask;
        //$this->getAllProductsTask = $getAllProductsTask;
        parent::__construct();
    }

    public function run(int $productId, $variantId, Language $currentLang = null): ?Product
    {
        $this->getProductByIdForApiTask->currentLang($currentLang);

        return $this->remember(function () use ($productId, $variantId, $currentLang) {
            $product = $this->getProductByIdForApiTask->run($productId, $this->withExtraData);

            if (!empty($product)) {
                $product->variants = app(FindVariantByProductIdSubAction::class)->run(
                    $productId,
                    ['product_option_values', 'product_option_values.option.desc', 'product_option_values.option_value.desc'],
                    1
                );

                if ($this->withExtraData) {
                    //$this->detectCampaign($product);
                    $categoryIds = $product->categories->pluck('category_id')->toArray();
                    /*if ($variantId > 0 && $variant = $this->getVariantByidTask->run($productId, $variantId)) {
                        $product->variant = $variant;
                        $product->price = $variant->price;
                        $product->global_price = $variant->global_price;
                    }*/
                    $product->similar = app(GetSimilarSubAction::class)->exceptProductIds([$productId])->run($categoryIds, $currentLang, [
                        'with_relationship' => [
                            /*'manufacturer', 'specialTags', 'specialTags.desc'*/
                        ]
                    ]);
                    $product->suggests = app(GetSuggestionSubAction::class)->exceptProductIds([$productId])->run($categoryIds, $currentLang);
                    //$product->topBuy = app(GetTopBuyingSubAction::class)->exceptProductIds([$productId])->run($categoryIds, 5, $currentLang);
                }

                return $product;
            }

            return null;
        }, null, [], 0, $this->skipCache);
    }

    private function detectCampaign(&$product): void
    {
        $currentCampaign = app(GetMostRecentlyCampaignOnProductSubAction::class)->run([$product->id]);

        $runningTimeline = app(CaclcRunningTimeLineSubAction::class)->run($currentCampaign);

        // dd($runningTimeline,$this->isInTimeline($currentCampaign, $runningTimeline, $product->id));

        if (!empty($currentCampaign)) {
            $product->campaign = $currentCampaign;
            $product->campaign->runningTimeLine = false;

            $this->calcPrice($product, $runningTimeline, $currentCampaign);
        }
    }

    private function calcPrice(&$product, $runningTimeline, $currentCampaign): void
    {
        if (!empty($runningTimeline) || ($currentCampaign->relationLoaded('timeline') && !$currentCampaign->timeline->IsEmpty())) {
            if ($this->isInTimeline($currentCampaign, $runningTimeline, $product->id) === true) {
                app(CalcProductPriceByCampaignSubAction::class)->run($product->campaign, $product);
                $product->campaign->runningTimeLine = $runningTimeline;
            } else {
                $product->campaign = app(GetMostRecentlyCampaignOnProductSubAction::class)->onlyNormal()->run([$product->id]);
                app(CalcProductPriceByCampaignSubAction::class)->run($product->campaign, $product);
            }
        } else {
            // if($product->campaign)
            // dd(($currentCampaign->relationLoaded('timeline') && !$currentCampaign->timeline->IsEmpty()));
            app(CalcProductPriceByCampaignSubAction::class)->run($product->campaign, $product);
        }
    }

    private function isInTimeline($currentCampaign, $runningTimeline, $productId)
    {
        if (!empty($runningTimeline)) {
            $inTimeLine = app(GetProductByCampaignIdTask::class)
                ->timelineId($runningTimeline->id)
                ->campaignId($currentCampaign->id)
                ->justCheckOnlyOne($productId);

            return $inTimeLine->run();
        }

        return null;
    }

    public function withExtraData(bool $withExtraData = true): self
    {
        $this->withExtraData = $withExtraData;
        return $this;
    }
}
