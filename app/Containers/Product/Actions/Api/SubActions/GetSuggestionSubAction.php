<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-25 23:31:45
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-17 10:09:38
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\Api\SubActions;

use App\Containers\Localization\Models\Language;
use App\Containers\Product\Enums\ProductStatus;
use App\Containers\Product\Tasks\GetAllProductsTask;
use App\Ship\Parents\Actions\SubAction;

class GetSuggestionSubAction extends SubAction
{
    private $limit = 8;

    protected $criterias = [];
    protected $exceptProductIds = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function run(?array $categoryIds, Language $currentLang): ?iterable
    {
        if (!empty($categoryIds)) {
            $this->criterias[] = ['byCategories' => [$categoryIds]];
            $this->criterias[] = ['inRandomOrder' => []];

            $suggestProduct = app(GetAllProductsTask::class)
                ->byCategories($categoryIds)
                ->activeStatus(ProductStatus::ACTIVE)
                ->inRandomOrder();

            if (!empty($this->exceptProductIds)) {
                $suggestProduct->exceptProductIds($this->exceptProductIds);
            }

            $suggestProduct = $suggestProduct->run([], $currentLang, true, $this->limit);

            return (!empty($suggestProduct) && !$suggestProduct->IsEmpty()) ? $suggestProduct : collect([]);
        }
        return null;
    }

    public function exceptProductIds(array $ids): self
    {
        $this->exceptProductIds = $ids;
        return $this;
    }
}
