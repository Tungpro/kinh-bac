<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-25 23:31:45
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-17 10:09:38
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\Api\SubActions;

use App\Containers\Localization\Models\Language;
use App\Containers\Product\Enums\ProductStatus;
use App\Containers\Product\Tasks\GetAllProductsTask;
use App\Ship\Parents\Actions\SubAction;
use App\Containers\Category\Tasks\GetRootPathsByCateIdTask;

class GetSimilarSubAction extends SubAction
{
    private $limit = 7;

    protected $criterias = [];
    protected $exceptProductIds = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function run(?array $categoryIds, Language $currentLang, array $externalData = []): ?iterable
    {
        if (!empty($categoryIds)) {
            $this->criterias[] = ['byCategories' => [$categoryIds]];
            $this->criterias[] = ['inRandomOrder' => []];
            $root_cate = app(GetRootPathsByCateIdTask::class)->run([$categoryIds]);

            if (isset($root_cate->path_id) && !empty($root_cate->path_id)) {
                $cate_id = $root_cate->path_id;
                $arr_cate_ids = [$cate_id];
            } else {
                $arr_cate_ids = $categoryIds;
            }

            //app(GetAllCateChildsTask::class)->run([$cate_id], $arr_cate_ids);
            $arr_cate_ids = array_unique($arr_cate_ids);
            $suggestProduct = app(GetAllProductsTask::class)
                ->byCategories($arr_cate_ids)
                ->activeStatus(ProductStatus::ACTIVE)
                ->inRandomOrder();

            if (!empty($this->exceptProductIds)) {
                $suggestProduct->exceptProductIds($this->exceptProductIds);
            }
            $suggestProduct = $suggestProduct->run($externalData, $currentLang, true, $this->limit);

            return (!empty($suggestProduct) && !$suggestProduct->IsEmpty()) ? $suggestProduct : collect([]);
        }
        return null;
    }

    public function exceptProductIds(array $ids): self
    {
        $this->exceptProductIds = $ids;
        return $this;
    }
}
