<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-03 13:46:27
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-21 17:54:35
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\Api;

use App\Containers\Localization\Models\Language;
use App\Containers\Product\Enums\ProductStatus;
use App\Containers\Product\Models\Product;
use App\Containers\Product\Tasks\GetProductByManufacturerTask;
use App\Ship\Parents\Actions\Action;

class GetProductByManufacturerAction extends Action
{
    protected $getProductByManufacturerTask;

    public function __construct(GetProductByManufacturerTask $getProductByManufacturerTask)
    {
        $this->getProductByManufacturerTask = $getProductByManufacturerTask;
        parent::__construct();
    }

    public function run(int $manufacturer_id,Language $currentLang = null,$limit=20)
    {
         $data = $this->getProductByManufacturerTask->activeStatus(ProductStatus::ACTIVE)->currentLang($currentLang)->mustHaveDescByLang()->run($manufacturer_id,$limit);

            return $data;
    }
}
