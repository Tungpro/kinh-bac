<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-03 13:46:27
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-21 17:54:35
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\Api;

use App\Containers\Localization\Models\Language;
use App\Containers\Product\Enums\ProductStatus;
use App\Containers\Product\Tasks\GetAllProductsTask;
use App\Ship\Parents\Actions\Action;

class GetSuggestionProductAction extends Action
{
    protected $criterias = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function run(?array $categoryIds,int $id = 0 ,Language $currentLang, int $limit = 5,array $orderBy = ['id' => 'desc']): ?iterable
    {
        $suggestProductApp = app(GetAllProductsTask::class);
        if (!empty($categoryIds)) $suggestProductApp->byCategories($categoryIds);
        $suggestProduct = $suggestProductApp->exceptProductId($id)->orderBy($orderBy)->equalStatus(ProductStatus::ACTIVE)->inRandomOrder()->run([], $currentLang, true, $limit);
        return (!empty($suggestProduct) && !$suggestProduct->IsEmpty()) ? $suggestProduct : collect([]);
    }

    public function setStatus(int $status = ProductStatus::ACTIVE): self
    {
        $this->criterias[] = ['equalStatus' => [$status]];
        return $this;
    }
}
