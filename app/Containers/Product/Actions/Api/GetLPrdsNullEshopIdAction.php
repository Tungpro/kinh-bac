<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-03 13:46:27
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-21 17:54:35
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\Api;

use App\Containers\Localization\Models\Language;
use App\Containers\Product\Models\Product;
use App\Containers\Product\Tasks\Product\GetLPrdsNullEshopIdTasks;
use App\Ship\Parents\Actions\Action;

class GetLPrdsNullEshopIdAction extends Action
{
    protected $GetLPrdsNullEshopIdTasks;

    public function __construct(GetLPrdsNullEshopIdTasks $GetLPrdsNullEshopIdTasks)
    {
        $this->GetLPrdsNullEshopIdTasks = $GetLPrdsNullEshopIdTasks;
        parent::__construct();
    }

    public function run($notPaginate, $limit)
    {
        return $this->remember(function () use($notPaginate, $limit) {
            $data = $this->GetLPrdsNullEshopIdTasks->run($notPaginate, $limit);

            return $data;
        });
    }
}
