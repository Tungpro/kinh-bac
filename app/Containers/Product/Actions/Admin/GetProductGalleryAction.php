<?php

namespace App\Containers\Product\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class GetProductGalleryAction.
 *
 */
class GetProductGalleryAction extends Action
{

    /**
     * @return mixed
     */
    public function run(int $product_id, $type = 'product', $json = false)
    {
        return $this->call('Product@Admin\GetProductGalleryTask', [
            $product_id,
            $type,
            $json
        ]);
    }
}
