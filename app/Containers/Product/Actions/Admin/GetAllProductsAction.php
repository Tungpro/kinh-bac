<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-18 10:22:36
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-11 18:14:42
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use Carbon\Carbon;
use Apiato\Core\Foundation\Facades\FunctionLib;

class GetAllProductsAction extends Action
{
    public function run(array $filters, array $external_data = [], int $limit = 10, bool $skipPagination = false): ?iterable
    {
        $criterias = [];
        if (isset($filters['id']) && $filters['id'] != '') {
            $criterias[] = ['equalId' => [$filters['id']]];
        } elseif (isset($filters['ids']) && $filters['ids'] != '') {
            $criterias[] = ['equalIds' => [$filters['ids']]];
        } else {
            if (isset($filters['status']) && $filters['status'] != '') {
                $criterias[] = ['equalStatus' => [$filters['status']]];
            } else {
                $criterias[] = ['greaterThanStatus' => [0]];
            }
            if (isset($filters['sync_id']) && $filters['sync_id'] != '') {
              $criterias[] = ['equalSyncId' => [$filters['sync_id']]];
            }
            if (isset($filters['time_from']) && !empty($filters['time_from'])) {
                // $timestamp = \Carbon\Carbon::parse($filters['time_from'])->timestamp;
                $criterias[] = ['createdAt' => [Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['time_from'])), '>=']];
            }
            if (isset($filters['time_to']) && !empty($filters['time_to'])) {
                $criterias[] = ['createdAt' => [Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['time_to'],true))]];
            }
            if (isset($filters['is_new']) && $filters['is_new'] == 'no_get') {
                $criterias[] = ['isNew' => [0]];
            } else {
                if (isset($filters['is_new'])) {
                    $criterias[] = ['isNew' => [1]];
                }
            }
            if (isset($filters['hot']) && $filters['hot'] == 'no_get') {
                $criterias[] = ['isNew' => [0]];
            } else {
                if (isset($filters['hot'])) {
                    $criterias[] = ['isHot' => [1]];
                }
            }
            if (isset($filters['type']) && $filters['type'] > -1) {
                $criterias[] = ['equalType' => [$filters['type']]];
            }
        }

        // $conds[] = ['date_available', '<=', Carbon::now()];

        // $data = $data->with(array_merge(['desc' => function ($query) {
        //     $query->select('id', 'product_id', 'name', 'short_description', 'meta_title');
        //     $query->activeLang($this->defaultLang);
        // }], isset($external_data['with_relationship']) ? $external_data['with_relationship'] : []));

        // $data->where($conds);

        if (isset($filters['cate_ids']) && is_array($filters['cate_ids'])) {
            $criterias[] = ['byCategories' => [$filters['cate_ids']]];
        } elseif (isset($filters['cate_ids'])) {
            $criterias[] = ['byCategories' => [[$filters['cate_ids']]]];
        }

        if (isset($filters['name'])) {
            $criterias[] = ['likeName' => [$filters['name']]];
        }

        if (isset($filters['group_filter_ids']) && is_array($filters['group_filter_ids'])) {
            $criterias[] = ['ByFilterGroupsCriteria' => [$filters['group_filter_ids']]];
        }

        $products = Apiato::call('Product@GetAllProductsTask', [$external_data, Apiato::call('Localization@GetDefaultLanguageTask'), $skipPagination, $limit], array_merge([
            'addRequestCriteria','ordereByCreated'
        ], $criterias));

        return $products;
    }
}
