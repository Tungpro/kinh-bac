<?php

namespace App\Containers\Product\Actions\Admin;

use App\Ship\Parents\Actions\Action;

class FindProductByInputAction extends Action
{
    public function run(string $input, int $limit, $orderBy = ['id' => 'DESC'])
    {
        return $this->call('Product@FindProductByInputTask', [$input, $limit], [
            ['orderBy' => [$orderBy]]
        ]);
    }
}
