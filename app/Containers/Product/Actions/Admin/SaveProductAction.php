<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-04-15 16:08:17
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-21 17:44:10
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\Admin;

use App\Containers\Product\Models\Product;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class SaveProductAction.
 *
 */
class SaveProductAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
      if(isset($data['product_data']['primary_category_id']) && $data['product_data']['primary_category_id'] == 0 || $data['product_data']['primary_category_id'] == null){
        if(isset($data['product_category']) && count($data['product_category']) > 0)
          $data['product_data']['primary_category_id']  =  $data['product_category'][0];
      }
      $product = $this->call('Product@SaveProductTask', [$data['product_data'],@$data['product_id']]);
      if ($product) {
            $original_desc = $this->call('Product@ProductDesc\GetAllProductDescTask', [$product->id]);
            $this->call('Product@ProductDesc\SaveProductDescTask', [$data, $original_desc, $product->id]);
            if (isset($data['product_category'])) {
              $this->call('Product@ProductCategory\SaveProductCategoryTask', [$data['product_category'],$product]);
            }
//            if (isset($data['product_filter'])) {
//                $this->call('Product@ProductFilter\SaveProductFilterTask', [$data['product_filter'],$product]);
//              }
            if(!empty($data['fileList']) && $data['product_id'] == 'NaN'){
                $img=[];
                foreach ($data['fileList'] as $key => $value) {
                    # code...
                    $img[] =  $value['id'];
                }
                $this->call('Product@Admin\UpdateProductImageTask', [$img,$product->id]);
            }
            $dataTag['product_tags'] = $data['product_tags'];
            $dataTag['product_labels'] = $data['product_labels'];
            $this->call('Tags@TagDetail\SaveTagLabelDetailTask', [array_merge($dataTag),$product->id]);

          $this->clearCache();

          return $product;
      }
    }
}
