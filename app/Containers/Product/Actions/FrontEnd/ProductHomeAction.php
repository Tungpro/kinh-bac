<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-08 21:54:52
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-12 19:49:44
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\Product\Tasks\FrontEnd\ProductsHomeTask;
use App\Ship\Parents\Actions\Action;

class ProductHomeAction extends Action
{
    protected $getProductHomeTask;

    public function __construct(ProductsHomeTask $getProductCollectionTask)
    {
        $this->getProductHomeTask = $getProductCollectionTask;
        parent::__construct();
    }

    public function run(string $select = "*", Language $currentLang = null,array $orderBy = ['created_at' => 'desc','sort_order' => 'asc'], array $where = [], $limit = false)
    {
        return  $this->getProductHomeTask->run(
            $select,
            $currentLang,
            $orderBy,
            $where,
            $limit
        );
    }

}
