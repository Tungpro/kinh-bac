<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-03 13:46:27
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-21 22:23:57
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\FrontEnd;

use Apiato\Core\Foundation\ImageURL;
use App\Containers\Localization\Models\Language;
use App\Containers\Product\Actions\Api\GetProductByIdAction;
use App\Containers\Product\Actions\ProductVariant\SubActions\CheckStockSubAction;
use App\Containers\Product\Actions\ProductVariant\SubActions\GetVariantByIdSubAction;
use App\Containers\Product\Models\Product;
use App\Containers\Product\Models\ProductDesc;
use App\Containers\Product\Models\ProductVariant;
use App\Containers\PromotionCampaign\Actions\SubActions\CalcProductPriceByCampaignSubAction;
use App\Containers\PromotionCampaign\Models\PromotionCampaign;
use App\Ship\Parents\Actions\Action;
use Illuminate\Http\Response;

class PreCheckingToAddCartItemAction extends Action
{
    protected $getProductByIdSlugTask;
    protected $product = null, $currentLang = null, $breakThrowEverything = false;
    protected $getVariantByIdSubAction,
        $checkStockSubAction,
        $getProductByIdAction,
        $calcProductPriceByCampaignSubAction;

    protected $promotionCampaign;

    public function __construct(
        GetVariantByIdSubAction $getVariantByIdSubAction,
        CheckStockSubAction $checkStockSubAction,
        GetProductByIdAction $getProductByIdAction,
        CalcProductPriceByCampaignSubAction $calcProductPriceByCampaignSubAction
    ) {
        $this->getVariantByIdSubAction = $getVariantByIdSubAction;
        $this->checkStockSubAction = $checkStockSubAction;
        $this->getProductByIdAction = $getProductByIdAction;
        $this->calcProductPriceByCampaignSubAction = $calcProductPriceByCampaignSubAction;
        parent::__construct();
    }

    public function run(?int $product_id, $product_variant_id, $product_quantity, bool $forceSelected = false)
    {
        $item = [
            'id' => $product_id,
            'variantId' => $product_variant_id,
            'quantity' => $product_quantity,
            'message' => '',
            'inStock' => 0,
        ];

        $variant = $this->getVariantByIdSubAction->run($item['id'], $item['variantId'], $product_quantity);

        // if ($product_id == 229) {
        //     dd($product_id, $variant, $item['variantId']);
        // }

        $stock = $this->checkStockSubAction->quanNeedle($item['quantity'])->run($variant);

        $item['inStock'] = !empty($stock) ? $stock : 0;

        if ($stock === false) {
            $err = ['selected' => false, 'error_code' => Response::HTTP_NOT_ACCEPTABLE, 'inStock' => $variant->stock > 0 ? $variant->stock : 0, 'message' => __('site.khongdusoluonghang')];

            if ($this->breakThrowEverything) {
                return $err;
            }

            $item = array_merge($item, $err);
        }

        $this->product = !empty($this->product) ? $this->product : $this->getProductByIdAction->currentLang($this->currentLang)->mustHaveDesc()->run($item['id']);
        // dump($item['id']);
        if (empty($this->product)) {
            $err = ['selected' => false, 'error_code' => Response::HTTP_NOT_FOUND, 'message' => __('site.khongtontaisanpham')];

            if ($this->breakThrowEverything) {
                return $err;
            }

            $item = array_merge($item, $err);

            return $item;
        }
        // dd($this->product,$variant);
        $this->calcCampaign($variant);

        $item['imageUrl'] = ImageURL::getImageUrl($this->product->image, 'product', 'medium');
        $item['productUrl'] = route('web_product_detail_page', ['slug' => $this->product->desc->slug, 'id' => $this->product->id]);
        $item['productPath'] = routeFEOnlyPath('web_product_detail_page', ['slug' => $this->product->desc->slug, 'id' => $this->product->id]); //$product->desc->slug.'-'.$product->id;
        $item['imageUrl'] = ImageURL::getImageUrl($this->product->image, 'product', 'medium');
        $item['name'] = $this->product->desc->name;
        // $item['messages'] = $err['message'];

        $item['price'] = isset($variant) && $variant->price > 0 ? $variant->price : $this->product->price;
        $item['globalPrice'] = isset($variant) ? $variant->global_price : 0;
        $item['variantId'] = isset($variant) ? $variant->product_variant_id : 0;
        $item['options'] = [];

        // dd($variant->product_option_values);

        if (isset($variant->product_option_values)) {
            foreach ($variant->product_option_values as $p_opt_val) {
                // dd($p_opt_val->option_value);
                if (isset($p_opt_val->option_value) && !empty($p_opt_val->option_value)) {
                    $item['options'][] = [
                        'option_name' => $p_opt_val->option->desc->name,
                        'option_value' => $p_opt_val->option_value->desc->name,
                    ];
                }
            }
        }
        // dd($item);
        return $item;
    }

    /**
     * Để tạm refactor sau
     */
    private function breakingProcess($code, $message)
    {
        $err = ['error_code' => $code, 'message' => $message];

        if ($this->breakThrowEverything) {
            return $err;
        }

        return '';
    }

    private function calcCampaign($variant)
    {
        if (!empty($this->promotionCampaign)) {
            /**
             * Nếu sản phẩm truyền vào ko rỗng thì set thêm data
             */
            if (!empty($this->product)) {
                $this->product->variants = $variant;
            }

            $this->calcProductPriceByCampaignSubAction->run($this->promotionCampaign, $this->product);
        }
    }

    public function breakThrowEverything(bool $bool = true): self
    {
        $this->breakThrowEverything = $bool;
        return $this;
    }

    public function currentCampaign(?PromotionCampaign $currentCampaign): self
    {
        $this->promotionCampaign = $currentCampaign;
        return $this;
    }

    public function currentProduct(Product &$product): self
    {
        $this->product = $product;
        return $this;
    }

    public function resetProduc(): self
    {
        $this->product = null;
        return $this;
    }

    public function currentLang(Language $currentLang = null): self
    {
        $this->currentLang = $currentLang;
        return $this;
    }
}
