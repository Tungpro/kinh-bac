<?php

namespace App\Containers\Product\Actions\ProductVariant;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class DeleteProductVariantAction extends Action
{
  public function run($transporter)
  {
    DB::beginTransaction();
    try {
      $whereConditional = Arr::only($transporter->toArray(), ['product_id', 'product_variant_id']);
      $productVariants = Apiato::call('Product@ProductVariant\DeleteProductVariantTask', [$whereConditional]);
      DB::commit();
      return $productVariants;
    }catch(\Exception $e) {
      DB::rollBack();
      dd($e->getMessage(), $e->getFile(), $e->getLine());
    }
  }
}
