<?php

namespace App\Containers\Product\Actions\ProductVariant;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class StoreProductVariantAction extends Action
{
  public function run($transporter, array $load=[])
  {
    DB::beginTransaction();
    try {
      $payload = $transporter->toArray();
      if(isset($payload['product_variant_id']) && !empty($payload['product_variant_id']))
        $productVariantData = Arr::only($payload, ['price', 'sku', 'stock', 'product_id', 'status', 'product_variant_id']);
      else
        $productVariantData = Arr::only($payload, ['price', 'sku', 'stock', 'product_id', 'status']);
      $productVariants = Apiato::call('Product@ProductVariant\StoreProductVariantTask', [
        $productVariantData
      ]);


      $productOptionInsert = Apiato::call('Product@ProductOption\StoreProductOptionByProductIdTask', [
        $productVariants->product_id,
        $payload['choice']['optionIds']
      ]);
      $productVariantsOptionValue = Apiato::call('Product@ProductVariant\StoreProductVariantOptionValueTask', [
        $productVariants,
        $payload['choice'],
        $productOptionInsert,@$payload['imageVarials'],@$payload['old_product_option_values']
      ]);

      DB::commit();
      $productVariants->load($load);
      return $productVariants;
    }catch(\Exception $e) {
      DB::rollBack();
      dd($e->getMessage(), $e->getFile(), $e->getLine());
    }
  }
}
