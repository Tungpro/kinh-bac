<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-14 12:32:05
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-01 16:56:50
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\ProductVariant;

use App\Containers\Product\Actions\ProductVariant\SubActions\CheckStockSubAction;
use App\Containers\Product\Models\ProductVariant;
use App\Ship\Parents\Actions\Action;

class CheckStockAction extends Action
{
    protected $quanNeedle = 0;

    public function run(ProductVariant $variant): bool
    {
        return app(CheckStockSubAction::class)->quanNeedle($this->quanNeedle)->run($variant);
    }

    public function quanNeedle(int $quanNeedle): self
    {
        $this->quanNeedle = $quanNeedle;
        return $this;
    }
}
