<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-14 18:27:45
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-14 18:29:54
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\ProductVariant\SubActions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\SubAction;

class FindVariantByProductIdSubAction extends SubAction
{
    public function run(int $productId = 0, array $with = [], int $isset_root = 0)
    {
        $productvariants = Apiato::call('Product@ProductVariant\FindVariantByProductIdTask', [$productId, $isset_root], [
            ['with' => [$with]]
        ]);

        return $productvariants;
    }
}