<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-14 18:27:45
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-17 11:28:54
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\ProductVariant\SubActions;

use App\Containers\Product\Enums\ProductVariantStatus;
use App\Containers\Product\Models\ProductVariant;
use App\Ship\Parents\Actions\SubAction;

class CheckStockSubAction extends SubAction
{
    protected $quanNeedle = 0;

    public function run(?ProductVariant $variant)
    {
        if (empty($variant) || $variant->stock <= 0 || ($this->quanNeedle > 0 &&  $variant->stock < $this->quanNeedle)) {
            return false;
        }

        if ($variant->status == ProductVariantStatus::NOT_ON_SALE) {
            return false;
        }

        return $variant->stock;
    }

    public function quanNeedle(int $quanNeedle): self
    {
        $this->quanNeedle = $quanNeedle;
        return $this;
    }
}
