<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-14 12:32:05
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-24 13:21:45
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\ProductVariant;

use App\Containers\Product\Tasks\ProductVariant\UpdateStockByVariantIdTask;
use App\Ship\Parents\Actions\Action;
use Illuminate\Support\Facades\DB;

class UpdateStockByVariantIdAction extends Action
{
    protected $variantStock = [];
    protected $increase = true;

    public function run()
    {
        // dd($this->variantStock,$this->increase);
        return DB::transaction(function () {
            $task = app(UpdateStockByVariantIdTask::class)->setVariantStock($this->variantStock);

            $this->increase ? $task->increase() : $task->decrease();

            return $task->run();
        });
    }

    /**
     * @var variantStock [variantId => quantity]
     */
    public function setVariantStock(array $variantStock): self
    {
        $this->variantStock = $variantStock;
        return $this;
    }

    public function increase() :self {
        $this->increase = true;
        return $this;
    }

    public function decrease() :self {
        $this->increase = false;
        return $this;
    }
}
