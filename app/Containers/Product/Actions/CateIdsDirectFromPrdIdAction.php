<?php

namespace App\Containers\Product\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Product\Tasks\CateIdsDirectFromPrdIdTask;
use App\Ship\Parents\Actions\Action;

/**
 * Class CateIdsDirectFromPrdIdAction.
 *
 */
class CateIdsDirectFromPrdIdAction extends Action
{

    /**
     * @return mixed
     */
    public function run($product_id)
    {
        $data = app(CateIdsDirectFromPrdIdTask::class)->run($product_id);
        return $data;
    }
}
