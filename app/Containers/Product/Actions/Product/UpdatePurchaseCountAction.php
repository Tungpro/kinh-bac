<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-14 12:32:05
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-24 16:46:09
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Actions\Product;

use App\Containers\Product\Tasks\Product\UpdatePurchaseCountTask;
use App\Ship\Parents\Actions\Action;
use Illuminate\Support\Facades\DB;

class UpdatePurchaseCountAction extends Action
{
    protected $productPurchased = [];
    protected $increase = true;

    public function run()
    {
        return DB::transaction(function () {
            $task = app(UpdatePurchaseCountTask::class)->setProductPurchased($this->productPurchased);

            $this->increase ? $task->increase() : $task->decrease();

            return $task->run();
        });
    }

    /**
     * @var productPurchased [productId => quantity]
     */
    public function setProductPurchased(array $productPurchased): self
    {
        $this->productPurchased = $productPurchased;
        return $this;
    }

    public function increase() :self {
        $this->increase = true;
        return $this;
    }

    public function decrease() :self {
        $this->increase = false;
        return $this;
    }
}
