<?php

namespace App\Containers\Product\Traits;

use Apiato\Core\Foundation\FunctionLib;

trait DateTrait {
  public function getCreatedAtAttribute($value): string {
    return FunctionLib::dateFormat(strtotime($value), "d/m/Y \l\ú\c H:i",true);
  }

  public function getUpdatedAtAttribute($value): string {
    return FunctionLib::dateFormat(strtotime($value), 'd/m/Y \l\ú\c H:i',true);
  }
}
