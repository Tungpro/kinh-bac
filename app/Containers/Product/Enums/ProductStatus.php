<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-05 16:13:56
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class ProductStatus extends BaseEnum
{
    const IS_NEW = 1;
    const IS_HOT = 1;
    const IS_HOME = 1;
    const IS_QUICK = 1;
    const IS_TOP_SEARCHING = 1;
    const IS_SALE = 1; // Bán chạy
    const IS_PROMOTION = 1; // Sản phẩm khuyến mãi
    const NON_PROMOTION = 0; // Không phải sản phẩm khuyến mãi
    const SHIPPING_REQUIRED = 1;
    const SHIPPING_NOT_REQUIRED = 0;
}
