<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-15 14:15:02
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class ProductVariantStatus extends BaseEnum
{
    const ON_SALE = 1;
    const NOT_ON_SALE = 2;
    const IS_ROOT = 1;
}
