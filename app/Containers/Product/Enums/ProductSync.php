<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-05 16:13:56
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class ProductSync extends BaseEnum
{
    const KEY_ISSET_SYNC = 'eshop_product_id';
    const NAME_SYNC ='ESHOP';
    const SYNC = 1;
    const TEXT = [
      self::SYNC => 'CÓ ĐỒNG BỘ HAY KO'
    ];
}
