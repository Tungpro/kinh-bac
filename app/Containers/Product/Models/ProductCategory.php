<?php
/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-01-31 18:03:27
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-02-01 17:28:28
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Models;

use App\Ship\Parents\Models\Model;
use App\Containers\Category\Models\CategoryDesc;


class ProductCategory extends Model
{
    protected $table = 'product_category';
    // protected $primaryKey = ['product_id','category_id'];

    public function category()
    {
        return $this->hasOne(CategoryDesc::class,'category_id','category_id')->where('language_id',1);
    }

    public function product()
    {
        return $this->hasOne(Product::class,'id','product_id')->where('status',2);
    }

    public function products(){
        return $this->belongsToMany(Product::class,ProductCategory::getTableName(),'category_id','product_id');
    }
}