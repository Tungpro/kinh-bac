<?php
/**
 * Created by PhpStorm.
 * Filename: ProductOptionValue.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 9/20/20
 * Time: 15:19
 */

namespace App\Containers\Product\Models;

use App\Ship\Parents\Models\Model;
use App\Containers\Option\Models\Option;

class ProductOption extends Model
{
    protected $table = 'product_option';
    protected $primaryKey = 'product_option_id';
    protected $fillable = [
      'option_id',
      'product_id',
    ];

    public function product() {
      return $this->belongsTo(Product::class, 'product_id');
    }

    public function option() {
      return $this->belongsTo(Option::class, 'option_id');
    }

    public function productOptionValues() {
      return $this->hasMany(ProductOptionValue::class, 'product_option_id', 'product_option_id');
    }
}
