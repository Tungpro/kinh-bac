<?php

namespace App\Containers\Product\Models;

use App\Ship\Parents\Models\Model;

class ProductsViewed extends Model
{
    //
    protected $table = 'product_viewed';
    public $timestamps = false;

    public function viewed(){
        return $this->hasMany(Product::class, 'id', 'product_id');
    }
}

