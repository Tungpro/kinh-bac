<?php
/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2020-11-09 10:45:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-04-15 18:19:01
 * @ Description: Happy Coding!
 */

namespace App\Containers\Product\Models;

use App\Ship\Parents\Models\Model;
use Apiato\Core\Traits\HasCompositePrimaryKey;
use App\Models\System\Language;

class ProductType extends Model {

    use HasCompositePrimaryKey;

    protected $table = 'product_type';
    protected $primaryKey = ['id', 'language_id'];
    public $incrementing = false;

    public function language()
    {
        return $this->hasOne(Language::class,'language_id','language_id');
    }
}