<?php

namespace App\Containers\BaseContainer\Enums;

class StatusEnum
{
    const VISIBLE = 1;
    const HIDDEN = 2;

    const STATUS_TEXT = [
        self::VISIBLE => 'Hiển thị',
        self::HIDDEN => 'Ẩn',
    ];

}
