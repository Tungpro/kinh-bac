<?php

namespace App\Containers\BaseContainer\Traits;

use App\Containers\File\Actions\UploadImageAction;
use App\Containers\File\Actions\UploadMultipleImageAction;

trait UploadImageTrait
{
    public function uploadImage($transporter, $request, $field, string $prefix = '', string $folder)
    {
        if (isset($request->$field)) {
            $image = app(UploadImageAction::class)->run($request, $field, $prefix, $folder);
            if (!$image['error']) {
                if (is_object($transporter)) {
                    $transporter->$field = $image['fileName'];
                } else {
                    $transporter[$field] = $image['fileName'];
                }
            } else {
                return $image;
            }
        }

        return $transporter;
    }

    public function uploadMultipleImage(&$transporter, $request, $field, string $prefix = '', string $folder, $encode = true)
    {
        if (isset($request->$field)) {
            $image = app(UploadMultipleImageAction::class)->run($request, $field, $prefix, $folder);
            if (!$image['error']) {
                if (is_object($transporter)) {
                    $transporter->$field = $encode ? json_encode($image['fileName']) : $image['fileName'];
                } else {
                    $transporter[$field] = $encode ? json_encode($image['fileName']) : $image['fileName'];
                }
            } else {
                return $image;
            }
        }

        return $transporter;
    }
}
