<?php


namespace App\Containers\BaseContainer\Traits;


use App\Containers\Localization\Models\Language;

trait RequestBaseLanguage
{

    public function getLanguage()
    {
        return Language::get();
    }

    public function messagesLang(array $rules = [])
    {
        $message = [];
        $getLanguage = $this->getLanguage();
        
        foreach ($rules as $rule => $text) {
            foreach ($getLanguage as $language) {
                $message[str_replace('*', $language->language_id, $rule)] = str_replace('language', $getLanguage->count() > 1 ? $language->name : '', $text);
            }
        }
     
        return $message;
    }
}
