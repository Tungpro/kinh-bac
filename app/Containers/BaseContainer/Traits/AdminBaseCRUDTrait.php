<?php

namespace App\Containers\BaseContainer\Traits;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\BaseContainer\Enums\StatusEnum;
use Exception;
use Illuminate\Support\Facades\DB;

trait AdminBaseCRUDTrait
{
    private function destroyRecord($id)
    {
        DB::beginTransaction();
        try {
            app($this->actions['destroy'])->run($id);

            DB::commit();
            return FunctionLib::ajaxRespondV2(true, 'Xóa bản ghi thành công!');
        } catch (Exception $e) {
            DB::rollBack();
            return FunctionLib::ajaxRespondV2(false, 'Có lỗi khi xóa bản ghi!');
        }
    }

    private function updateStatusRecord($request)
    {
        DB::beginTransaction();
        try {
            app($this->actions['status'])->run($request);

            DB::commit();
            return FunctionLib::ajaxRespondV2(true, StatusEnum::STATUS_TEXT[$request->status] . ' bản ghi thành công!');
        } catch (Exception $e) {
            DB::rollBack();
            return FunctionLib::ajaxRespondV2(false, 'Có lỗi khi cập nhật bản ghi!');
        }
    }
}