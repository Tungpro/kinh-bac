<?php

namespace App\Containers\BaseContainer\Traits;

use App\Containers\BaseContainer\Data\Criterias\AdminBaseSearchCriteria;

trait ScopeAdminBaseSearchTrait
{
    public function extraSearchCondition($request)
    {
        // implement here
    }

    public function scopeAdminBaseSearch($request)
    {
        if (isset($request->scopeAdminBaseSearch) && $request->scopeAdminBaseSearch == true) {
            $this->repository->pushCriteria(new AdminBaseSearchCriteria($request));
            $this->extraSearchCondition($request);
        }


        return $this;
    }

    public function returnDataByLimit($limit = 20, $offset = 0, $exceptID = '')
    {

        if(!empty($exceptID)) {
            $this->repository->except($exceptID);
        }
        if ($offset > 0) {
            return $this->repository->limit($limit);
        } else if (is_null($limit)) {
            return $this->repository->get();
        } else if ($limit === 0) {
            return $this->repository->first();
        } else {
            return ($limit < 0) ? $this->repository->offset($offset)->limit(abs($limit))->get() : $this->repository->offset($offset)->paginate($limit);
        }
    }
}