<?php


namespace App\Containers\BaseContainer\Traits;


use Apiato\Core\Foundation\StringLib;
use App\Containers\Localization\Models\Language;

trait BaseLanguage
{

    public function getLanguage()
    {
        return  config('localization-container.supported_languages');
    }

    public function slug($slug = '',$name = '', int $k = 0)
    {
        // chú ý: config lang thứ tự key phải trùng với trong database ;
        if(empty($slug) && $k > 2){
            $getLanguage = $this->getLanguage();
            foreach ($getLanguage as $key => $language) {
                if($k > 2 && $k== $key+1){
                    return   StringLib::slug($name, '-', $language);
                }
            }
        }elseif (empty($slug)){
            return StringLib::slug($name);
        }
        return StringLib::slug($slug);
    }
}
