

<footer class="bg-[#F5F5F5] p-[64px_0px_39px]">
    <div class="container">
        <div class="footer text-gray1 text-14 leading-[25px]">
            <a href="{{route('web_home_page')}}" class="footer__logo block"><img
                    src="{{ImageURL::getImageUrl($settings['website']['logo_footer'],'setting','original')}}"
                    alt="{{@$settings['website']['site_name']}}"/></a>
            <div class="footer__col footer__col--same headquarters">
                <h3 class="text-14 text-red font-[700] mb-4">{{__('site.trusochinh')}}:</h3>
                <ul class="no-disc">
                    @if(!empty($settings['website']['address']))
                    <li><img src="{{asset('template/dist/images/ft-map.png')}}" alt="" /> <span>{{@$settings['website']['address']}}</span></li>
                    @endif

                    @if(!empty($settings['contact']['email']))
                            <li>
                        <img src="{{asset('template/dist/images/ft-mail.png')}}" alt="" />
                        <div>
                            <a href="mailto:{{@$settings['contact']['email']}}">{{@$settings['contact']['email']}}</a>
{{--                            , <br />--}}
{{--                            <a href="mailto:{{@$settings['contact']['email_2']}}">{{@$settings['contact']['email_2']}}</a>--}}
                        </div>
                    </li>
                    @endif
                    @if(!empty($settings['contact']['hotline']))
                    <li><img src="{{asset('template/dist/images/ft-phone.png')}}" alt="" /> <span> <a href="tel:{{str_replace(['(', ')', '.', ' '],'',@$settings['contact']['hotline'])}}">
                                {{@$settings['contact']['hotline']}}</a><span>
                    @endif
                    @if(!empty($settings['contact']['hotline2']))
                    <li><img src="{{asset('template/dist/images/ft-in.png')}}" alt="" /> <span> <a href="tel:{{str_replace(['(', ')', '.', ' '],'',@$settings['contact']['hotline_2'])}}">{{@$settings['contact']['hotline_2']}}
                                {{@$settings['contact']['hotline2']}}</a></span>
                    @endif
                </ul>
            </div>
            <div class="footer__col footer__col--same office">
                <h3 class="text-14 text-red font-[700] mb-4">{{__('site.vanphongdaidien')}}:</h3>
                <ul class="no-disc">
                    @if($settings['website']['office_vietnam'])
                    <li>
                        <div><strong>{{__('site.vietnam')}}:</strong></div>
                        {{@$settings['website']['office_vietnam']}}
                    </li>
                    @endif
                    @if($settings['website']['office_japan'])
                    <li>
                        <div><strong>{{__('site.nhatban')}}:</strong></div>
                        {{@$settings['website']['office_japan']}}
                    </li>
                     @endif
                </ul>
            </div>
            <div class="footer__col footer__col--menu">
                @if(isset($footerMenus[0]))
                <ul class="no-disc xl:mt-[36px]">

                    @foreach ($footerMenus[0] as $key => $footerMenuLvl1)
                        @if($key <= count($footerMenus[0])/2)
                        <li><a href="{{ $footerMenuLvl1->menu_link }}"><strong>{{ @$footerMenuLvl1->desc_lang->name }}</strong></a></li>
                        @else
                            <li><a href="{{ $footerMenuLvl1->menu_link }}">{{ @$footerMenuLvl1->desc_lang->name }}</a></li>
                        @endif
                    @endforeach
                </ul>
                 @endif
            </div>
        </div>
        <div class="footer__right flex items-center text-gray1 text-14">
            <div>{{__('site.banquyenthuocve')}}</div>
            <div class="ml-auto">{{__('site.designby')}}</div>
        </div>
    </div>
</footer>
@push('js_bot_all')
{{--    <script> const url_api_store_subscriber = '{{ route('api_store_subscriber') }}';</script>--}}

{{--    {!! FunctionLib::addMedia('js/pages/base/subscriber.js') !!}--}}
@endpush


