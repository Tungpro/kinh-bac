@if(isset($banner) && $banner->isNotEmpty())

    <section
        class="banner-general bg-section relative" style="background-image: url('{{$banner[0]->getImageUrl('super_large')}}')">
        <h1 class="banner-title w-fit inline-block text-center font-rockness text-[50px] leading-snug text-white px-3">
            {{$banner[0]->desc->name}}
        </h1>

        <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>
    </section>
@endif
