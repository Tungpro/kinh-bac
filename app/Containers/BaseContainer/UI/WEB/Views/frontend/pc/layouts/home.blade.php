@php($def = isset($def) ? $def : FunctionLib::tplShareGlobal('web', $settings ?? []))
@php($seoDefault = FunctionLib::getSeo('web', $def))
@php($versionMedia = $settings['other']['version'] ?? 0)
    <!DOCTYPE html>
<html lang="{{ $def['lang'] }}">

<head>
    @include('basecontainer::frontend.pc.layouts.includes.meta')

    @include('basecontainer::frontend.pc.layouts.includes.head_media')
</head>

<body>
<div id="app">
    <div class="{{@$extraCoverClasses}}">
        <x-header-component :menus="$menus" :class="'in-home'"></x-header-component>
        @yield('content')
        <div class="overlay-body"></div>
        <x-footer-component :menus="$menus"></x-footer-component>
        @yield('content_popup')

    </div>
</div>

@include('basecontainer::frontend.pc.layouts.includes.bottom_media')
</body>

</html>
