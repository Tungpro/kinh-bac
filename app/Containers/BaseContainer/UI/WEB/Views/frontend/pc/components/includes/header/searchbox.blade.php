<div class="search-box" id="search-box-vue">
    <div class="button-search">
        <form action="{{ route('web_product_search_page') }}" method="GET">
            <input  type="text" 
                    name="q" 
                    id="textSearch" 
                    class="text-search" 
                    autocomplete="off" 
                    title=""
                    v-model="searchKeyword" 
                    v-on:focus="showSearchBox" 
                    @keyup="getSuggestProducts" 
                    ref="toggleSearch">
            <button class="btn-search-custom" type="submit">
                <i class="fa fa-search" aria-hidden="true"></i>

            </button>
        </form>
    </div>
    
    <div id="search-result-wrap" v-show="showSearchBoxStatus" class="" style=" display:none;">
        <div id="search-search-empty" v-if="searchKeyword.length == 0">
            <div class="search-result-content">
                <p class="title text-uppercase">
                    tìm kiếm của bạn
                </p>
                <!-- từ khóa đã tìm kiếm cache-search-->
                <a class="search-item search-suggest-item cache-search" href="javascript:;">
                    <span class="icons icon-search-cache"></span>
                    <div class="keyword">Mỹ phẩm</div>
                </a>
                <!-- end từ khóa đã tìm kiếm -->
                <a class="search-item search-suggest-item " href="javascript:;">
                    <span class="icons icon-search"></span>
                    <div class="keyword">Thời trang</div>
                </a>
                <a class="search-item search-suggest-item " href="javascript:;">
                    <span class="icons icon-search"></span>
                    <div class="keyword">Mỹ phẩm 2</div>
                </a>
                <a class="search-item search-suggest-item " href="javascript:;">
                    <span class="icons icon-search"></span>
                    <div class="keyword">Thời trang 2</div>
                </a>
                <a class="search-item search-suggest-item " href="javascript:;">
                    <span class="icons icon-search"></span>
                    <div class="keyword">Mỹ phẩm 3</div>
                </a>
                <a class="search-item search-suggest-item " href="javascript:;">
                    <span class="icons icon-search"></span>
                    <div class="keyword">Thời trang 3</div>
                </a>


                <p class="title text-uppercase">
                    tìm kiếm hàng đầu
                </p>
                <a class="search-item search-suggest-item " href="javascript:;">
                    <span class="icons icon-search"></span>
                    <div class="keyword">Thời trang</div>
                </a>
                <a class="search-item search-suggest-item " href="javascript:;">
                    <span class="icons icon-search"></span>
                    <div class="keyword">Mỹ phẩm 2</div>
                </a>
                <a class="search-item search-suggest-item " href="javascript:;">
                    <span class="icons icon-search"></span>
                    <div class="keyword">Thời trang 2</div>
                </a>
                <a class="search-item search-suggest-item " href="javascript:;">
                    <span class="icons icon-search"></span>
                    <div class="keyword">Mỹ phẩm 3</div>
                </a>
                <a class="search-item search-suggest-item " href="javascript:;">
                    <span class="icons icon-search"></span>
                    <div class="keyword">Thời trang 3</div>
                </a>
            </div>
        </div>

        <div id="search-result"
             v-if="searchKeyword.length > 0 && suggestProducts.products && suggestProducts.products.data.length > 0 ">
            <div class="search-result-content search-has-result">
                <ul class="list-unstyled m-0 p-0 search-list-result">
                    <li v-for="(prd_item,idx_prd_item) in suggestProducts.products.data">
                        <a :href="prd_item.product_url" class="search-prod">
                            <img :src="prd_item.product_image_small" alt="">
                            <div class="search-prod-info">
                                <p class="name" v-text="prd_item.name"></p>
                                <div class="price-box">
                                    <span class="normal-price color-red" v-text="formatVND(prd_item.price)"></span>
                                    <span class="old-price color-gray line-through"
                                        v-text="formatVND(prd_item.old_price)"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
                <p class="mb-0 text-center" id="search-result-totals">
                    <a :href="getUrlSearch" class="text-black">
                        Xem thêm <span v-text="suggestProducts.products.meta.pagination.total"></span> kết quả
                    </a>

                </p>

            </div>
        </div>
    </div>

</div>
@push('js_bot_all')
    <script>
        let url_api_sugg_prd_search = "{{ route('api_personalish_suggest_prd_search_box') }}",
            url_search = "{{ route('web_product_search_page') }}";
        let q = "{{ request()->q }}";
        let app_search_box_vue = new Vue({
            el: '#search-box-vue',
            data: {
                preload: preload_functions,
                url_search: url_search,
                suggestProducts: {},
                searchKeyword: q,
                showSearchBoxStatus: false,
                isLoading: true,
            },
            async beforeMount() {

            },
            async mounted() {

            },
            created() {
                window.addEventListener("click", this.hideSearchBox);
            },
            beforeDestroy() {
                window.removeEventListener("click", this.hideSearchBox);
            },
            computed: {
                getUrlSearch() {
                    return url_search + '?q=' + this.searchKeyword;
                }
            },
            watch: {

            },
            methods: {
                loading: function() {
                    this.isLoading = true;
                },
                loaded: function() {
                    this.isLoading = false;
                },
                showSearchBox: function() {
                    this.showSearchBoxStatus = true;
                },
                hideSearchBox: function(e) {
                    if (!this.$refs.toggleSearch.contains(e.target)) {
                        this.showSearchBoxStatus = false;
                    }
                },
                formatVND: function(value) {
                    return this.preload.formatVND(value);
                },
                getSuggestProducts: async function() {
                    this.preload.freeze(async () => {
                        this.loading();
                        await this.preload.fetchData(
                            url_api_sugg_prd_search,
                            new URLSearchParams({
                                q: this.searchKeyword
                            }), {
                                method: 'GET'
                            }
                        ).then(json => {
                            this.suggestProducts = json;
                            this.loaded();
                        }).catch(error => {
                            this.loaded();
                        });
                    }, 400);
                }
            }
        });
    </script>
@endpush
