@php($def = isset($def) ? $def : \FunctionLib::tplShareGlobal('web', $settings ?? []))
@php($seoDefault = \FunctionLib::getSeo('web', $def))
@php($versionMedia = $settings['other']['version'] ?? 0)
<!DOCTYPE html>
<html lang="{{ $def['lang'] }}">

<head>
    @include('basecontainer::frontend.pc.layouts.includes.meta')

    @include('basecontainer::frontend.pc.layouts.includes.head_media')
</head>

<body>
    <div id="app">
        <x-header-component :menus="$menus" :class="'not-home'"></x-header-component>

        <main class="{{ $mainClass }}">
            <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>
            @yield('content')
        </main>

        <x-footer-component :menus="$menus"></x-footer-component>
        @yield('content_popup')

        <x-modal-auth-component :menus="$menus"></x-modal-auth-component>
    </div>

    @include('basecontainer::frontend.pc.layouts.includes.bottom_media')
</body>

</html>
