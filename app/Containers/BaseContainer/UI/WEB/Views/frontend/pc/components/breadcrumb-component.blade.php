
@if(isset($breadcrumb) && !empty($breadcrumb))
<ul class="breadcrumb flex flex-wrap uppercase font-utm tracking-[0.03em] mb-7 text-16 lg:text-18">
    @foreach ($breadcrumb as $key => $item)
        @if(empty($item['show_home']))
            @if(!$item['isCurrent'])
               <li class="breadcrumb-item">
                   <a href="{{$item['link']}}">@if($key == 0) <img src="{{asset('template/dist/images/icon-home.png')}}" alt=""/> @else{{ $item['title'] }} @endif</a>
               </li>
            @else
                <li class="breadcrumb-item active">{{ $item['title'] }}</li>
            @endif
        @endif
    @endforeach
</ul>
@endif
