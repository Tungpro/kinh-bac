<header class="py-[18px] bg-white">
    <div class="container font-san font-[400]">
        <div class="header">
            <a href="{{route('web_home_page')}}" class="logo"><img
                    src="{{ ImageURL::getImageUrl($settings['website']['logo'],'setting','original') }}"
                    alt="{{@$settings['website']['site_name']}}"/>
            </a>
            <div class="header__menu uppercase">
                <ul class="main-menu relative">
                    @if (isset($headerMenus[0]) &&  $headerMenus[0]->isNotEmpty())
                        @foreach ($headerMenus[0] as $key => $headerMenuLvl1)
                            <li class="@if(isset($headerMenus[$headerMenuLvl1->id]))has-submenu @endif
                                       @if (isset($headerMenus[$headerMenuLvl1->id])) has-sub-menu @endif
                                       @if($key == 0) font-san font-[400] @endif
                                ">
                                <a href="{{ @$headerMenuLvl1->menu_link }}">
                                    {{@$headerMenuLvl1->desc_lang->name}}
                                    @if (isset($headerMenus[$headerMenuLvl1->id]))
                                    <span class="icon"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                    @endif
                                </a>
                                @if (isset($headerMenus[$headerMenuLvl1->id]))
                                    <span class="mb-click xl:hidden"></span>
                                    <ul class="sub-menu">
                                        @foreach ($headerMenus[$headerMenuLvl1->id] as $footerMenuLvl2)
                                            <li>
                                                <a href="{{ $footerMenuLvl2->menu_link }}"
                                                   @if(@$footerMenuLvl2->no_follow === 1) rel="nofollow" @endif
                                                   @if(@$footerMenuLvl2->newtab === 1) target="_blank" @endif
                                                   class="{{$footerMenuLvl2->getActiveMenuClass()}}"
                                                >{{ @$footerMenuLvl2->desc_lang->name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    @endif
                </ul>
                <div class="header__languages relative">
                    <a href="javascript:;" class="languages flex items-center uppercase text-[#4F4F4F] text-14">
                        <svg width="20" height="19" viewBox="0 0 20 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                fill-rule="evenodd"
                                clip-rule="evenodd"
                                d="M10 19C6.80304 19 3.97491 17.4208 2.2531 15H2V14.6256C1.05061 13.1469 0.5 11.3878 0.5 9.5C0.5 4.42101 4.48572 0.272939 9.5 0.0129321V0H10H10.5V0.0129321C15.5143 0.272939 19.5 4.42101 19.5 9.5C19.5 11.3878 18.9494 13.1469 18 14.6256V15H17.7469C16.0251 17.4208 13.197 19 10 19ZM13.0109 17.4513C14.3675 16.9373 15.5583 16.0861 16.481 15H14.485C14.0868 15.9671 13.5864 16.7999 13.0109 17.4513ZM14.845 14H17.2124C17.9871 12.761 18.4514 11.308 18.4964 9.75H15.4981C15.4752 11.285 15.2414 12.7294 14.845 14ZM14.498 9.75C14.4731 11.3193 14.2142 12.768 13.7931 14H10.5V9.75H14.498ZM15.4831 8.75H18.4674C18.3654 7.58311 18.0276 6.4838 17.5019 5.5H14.9902C15.2595 6.50098 15.431 7.59657 15.4831 8.75ZM13.9515 5.5C14.2382 6.47939 14.4256 7.57684 14.482 8.75H10.5V5.5H13.9515ZM14.6775 4.5H16.8746C15.909 3.17461 14.5694 2.13913 13.0109 1.54868C13.6833 2.30986 14.2533 3.31864 14.6775 4.5ZM10.5 1.05748C11.3659 1.25563 12.2639 1.97126 13.0237 3.28352C13.2376 3.6531 13.4339 4.0603 13.6086 4.5H10.5V1.05748ZM6.98908 1.54868C5.43063 2.13914 4.09103 3.17461 3.1254 4.5H5.32252C5.74669 3.31864 6.31668 2.30986 6.98908 1.54868ZM6.39143 4.5C6.56613 4.0603 6.76237 3.6531 6.97634 3.28352C7.73607 1.97126 8.63407 1.25563 9.5 1.05748V4.5H6.39143ZM5.00985 5.5H2.4981C1.97244 6.4838 1.63465 7.58311 1.53263 8.75H4.51689C4.56904 7.59657 4.74046 6.50098 5.00985 5.5ZM5.51796 8.75C5.57442 7.57684 5.76175 6.47939 6.04849 5.5H9.5V8.75H5.51796ZM4.50187 9.75H1.50361C1.54861 11.308 2.01289 12.761 2.78755 14H5.15497C4.75857 12.7294 4.52482 11.285 4.50187 9.75ZM6.20693 14C5.78575 12.768 5.52692 11.3193 5.50198 9.75H9.5V14H6.20693ZM5.51496 15H3.51903C4.44166 16.0861 5.63245 16.9373 6.98908 17.4513C6.41363 16.7999 5.91318 15.9671 5.51496 15ZM6.60575 15H9.5V17.9425C8.63407 17.7444 7.73607 17.0287 6.97634 15.7165C6.84594 15.4912 6.72212 15.252 6.60575 15ZM10.5 17.9425V15H13.3942C13.2779 15.252 13.1541 15.4912 13.0237 15.7165C12.2639 17.0287 11.3659 17.7444 10.5 17.9425Z"
                                fill="#BDBDBD"
                            />
                        </svg>
                        <span class="inline-block mx-[6px]">{{__('site.ngonngu')}}</span>
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    <div class="language-list text-gray2">
                        <a href="{{\Apiato\Core\Foundation\UrlLib::getChangeLangUrl('vi', $currentLang)}}" class="item"><img src="{{asset('template/dist/images/flat-vn.png')}}" alt="" /><span>VietNam</span></a>
                        <a href="{{\Apiato\Core\Foundation\UrlLib::getChangeLangUrl('en', $currentLang)}}" class="item"><img src="{{asset('template/dist/images/flat-en.png')}}" alt="" /><span>England</span></a>
                        <a href="{{\Apiato\Core\Foundation\UrlLib::getChangeLangUrl('ja', $currentLang)}}" class="item"><img src="{{asset('template/dist/images/ja.png')}}" alt="" /><span>Japan</span></a>
                        <a href="{{\Apiato\Core\Foundation\UrlLib::getChangeLangUrl('kn', $currentLang)}}" class="item"><img src="{{asset('template/dist/images/kn.png')}}" alt="" /><span>Korea</span></a>
                        <a href="{{\Apiato\Core\Foundation\UrlLib::getChangeLangUrl('zh', $currentLang)}}" class="item"><img src="{{asset('template/dist/images/zh.png')}}" alt="" /><span>China</span></a>
                    </div>
                </div>
            </div>

            <a href="javascript:;" class="hambuger xl:hidden ml-auto"><img src="{{asset('template/dist/images/menu.png')}}" alt="" /></a>
        </div>
    </div>
    <div class="header__overlay"></div>
</header>

