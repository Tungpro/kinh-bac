<div class="row">
    <div class="{{$fileSingleColClass ?? 'col-lg-12'}}">
        <div class="form-group">
            <label for="fileInput{{$fileSingleKey ?? 'file'}}">{{$fileSingleTittle ?? 'File'}}</label>
            <input type="file" id="fileInput{{$fileSingleKey ?? 'file'}}" name="{{$fileSingleKey ?? 'file'}}"
                   accept=".doc,.docx,.ppt, .pptx,.txt,.pdf,.zip,.rar"
                   class="form-control pt-1 {{ $errors->has($fileSingleKey ?? 'file') ? 'is-invalid' : '' }}">

            @if(!empty($data->$fileSingleKey))
            <div class="mt-2">
                <p width="100" class="file" id="filePreview{{$fileSingleKey ?? 'file'}}"
                >{{$data->$fileSingleKey}}</p>

                <input type="hidden" name="delete_{{$fileSingleKey ?? 'file'}}"
                       id="fileRemoveFlag{{$fileSingleKey ?? 'file'}}" value="">
                <a class="btn-danger btn-sm ml-3 mt-0 text-decoration-none {{!empty($data->{$fileSingleKey ?? 'file'}) ? '' : 'd-none'}}"
                   id="fileRemove{{$fileSingleKey ?? 'file'}}" href="javascript:void(0)"
                   onclick="removeFile{{$fileSingleKey ?? 'file'}}()"
                   title="Xóa file"><i class="fa fa-trash"></i> {{__('Xóa')}}</a>
            </div>
            @endif

            @if(!empty($fileDesc) && !empty($data['all_desc'][$it_lang['language_id']][$fileDesc]))

                <div class="fileDesc mt-2">
                    <div class="inputFileDesc">
                        <input type="hidden" class="inputFile" name="{{$fileSingleKey ?? 'file'}}" value="">
                    </div>
                    <div class="fileDescPreview">
                        <p width="100" class="file"
                        >{{@$data['all_desc'][$it_lang['language_id']][$fileDesc]}}</p>
                        <a href="javascript:void(0)" onclick="deleteFileDesc(this)" class="btn-danger file-desc btn-sm ml-3 mt-0 text-decoration-none fileRemoveFlag {{ @$data['all_desc'][$it_lang['language_id']][$fileDesc] ? '' : 'd-none'}}"
                           id="fileRemove{{$fileDesc ?? 'file'}}" href="javascript:void(0)"
                           title="Xóa file"><i class="fa fa-trash"></i> {{__('Xóa')}}</a>
                    </div>

                </div>
            @endif
        </div>
    </div>
</div>

@push('js_bot_all')
    <script type="text/javascript">

        function removeFile{{$fileSingleKey ?? 'file'}}() {
            document.getElementById('filePreview{{$fileSingleKey ?? 'file'}}').remove();
            document.getElementById('fileRemoveFlag{{$fileSingleKey ?? 'file'}}').value = 1;
        }

       function  deleteFileDesc(thisA) {
             $(thisA).closest('.fileDesc').find('.inputFileDesc').children('.inputFile').val(1)
             console.log('data', $(thisA).closest('.fileDesc').find('.inputFileDesc').find('.inputFile').val()) ;
           $(thisA).closest('.fileDesc').find('.fileDescPreview').remove();
        }

    </script>
@endpush