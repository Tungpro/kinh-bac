<div class="row">
    <div class="{{$imageMultiColClass ?? 'col-lg-12'}}">
        <div class="custom-file-container" data-upload-id="holderUploadMulti{{$imageMultiKey ?? ''}}">
            <label>{{$imageMultiTittle ?? 'Hình ảnh'}} <a href="javascript:void(0)"
                                                          class="custom-file-container__image-clear"></a></label>
            <label class="custom-file-container__custom-file">
                <input type="file" class="custom-file-container__custom-file__custom-file-input"
                       name="{{$imageMultiKey ?? 'images'}}[]" accept="image/*" multiple
                       aria-label="Chọn hình ảnh"
                       id="images_upload_multiple{{$imageMultiKey ?? ''}}">
                <input type="hidden" name="MAX_FILE_SIZE" value="15728640"/>
                <span class="custom-file-container__custom-file__custom-file-control"></span>
            </label>
            <div class="custom-file-container__image-preview" id="sortable-container{{$imageMultiKey ?? ''}}"></div>
        </div>

        <div id="holderOldImageName{{$imageMultiKey ?? ''}}" style="display:none;"></div>
    </div>
    <div></div>
</div>

@push('css_bot_all')
    {!! FunctionLib::addMedia('admin/js/library/file-upload-with-preview/file-upload-with-preview-4.1.0.min.css', '21042022') !!}

    <style type="text/css">
        .custom-file-container__image-multi-preview {
            flex: 0 0 10%;
            width: 10%;
            height: 34%;
        }

        .custom-file-container__image-preview {
            overflow: auto;
            border: 1px dotted #d6d6d6;
            background-color: #f6f6f6;
        }

        .custom-file-container__image-multi-preview {
            cursor: grab;
        }
    </style>
@endpush

@push('js_bot_all')
    {!! FunctionLib::addMedia('admin/js/library/file-upload-with-preview/file-upload-with-preview-4.1.0.min.js', '21042022') !!}

    <script type="text/javascript">
        let upload{{$imageMultiKey ?? ''}} = new FileUploadWithPreview("holderUploadMulti{{$imageMultiKey ?? ''}}", {
            text: {
                chooseFile: "Chọn hình ảnh ...",
                browse: "Chọn hình ảnh",
                selectedCount: " hình ảnh đã được chọn. Kéo thả hình ảnh để sắp xếp.",
            },
            images: {
                baseImage: '',
                backgroundImage: '',
            },
            presetFiles: presetFiles{{$imageMultiKey ?? ''}}, // init at images.blade.php
        });

        window.addEventListener("fileUploadWithPreview:imagesAdded", function (e) {
            updateImagesInput{{$imageMultiKey ?? ''}}();
        });

        window.addEventListener("fileUploadWithPreview:imageDeleted", function (e) {
            updateImagesInput{{$imageMultiKey ?? ''}}();
        });

        function updateImagesInput{{$imageMultiKey ?? ''}}() {
            document.getElementById('holderOldImageName{{$imageMultiKey ?? ''}}').innerHTML = "";
            const dataTransfer = new DataTransfer();
            upload{{$imageMultiKey ?? ''}}.cachedFileArray.forEach(function (item) {
                let input = document.createElement('input');
                input.type = 'hidden';
                input.name = '{{$imageMultiKey ?? 'images'}}_old[]';
                if (item instanceof File) {
                    dataTransfer.items.add(item);
                } else if (item instanceof Blob) {
                    input.value = item.name;
                }
                document.getElementById('holderOldImageName{{$imageMultiKey ?? ''}}').appendChild(input);
            });
            const input = document.getElementById('images_upload_multiple{{$imageMultiKey ?? ''}}');
            input.files = dataTransfer.files;
        }

        $("#sortable-container{{$imageMultiKey ?? ''}}").sortable({
            cursorAt: {top: 50, left: 50},
            helper: "clone",
            cursor: "grabbing",
            update: function (event, ui) {
                // Get the new token order
                let newTokenOrder = $(this).sortable('toArray', {attribute: 'data-upload-token'});
                // Init new array that we'll file with the correct order
                let sortedCachedFileArray = [];

                // Loop through the newTokenOrder array and add each email in place as found
                for (let x = 0; x < newTokenOrder.length; x++) {
                    let foundIndex = upload{{$imageMultiKey ?? ''}}.cachedFileArray.map(image => image.token).indexOf(newTokenOrder[x]);
                    sortedCachedFileArray.push(upload{{$imageMultiKey ?? ''}}.cachedFileArray[foundIndex])
                }

                // Replace the cachedFileArray with your new sortedCachedFileArray
                upload{{$imageMultiKey ?? ''}}.replaceFiles(sortedCachedFileArray);

                updateImagesInput{{$imageMultiKey ?? ''}}();
            }
        });
    </script>
@endpush