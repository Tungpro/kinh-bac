<div class="row">
    <div class="{{$imageSingleColClass ?? 'col-lg-12'}}">
        <div class="form-group">
            <label for="imageInput{{$imageSingleKey ?? 'image'}}">{{$imageSingleTittle ?? 'Hình ảnh'}}</label>
            <input type="file" id="imageInput{{$imageSingleKey ?? 'image'}}" name="{{$imageSingleKey ?? 'image'}}"
                   accept="image/*"
                   class="form-control pt-1 {{ $errors->has($imageSingleKey ?? 'image') ? 'is-invalid' : '' }}">
            @if(!empty($data->$imageSingleKey))
            <div class="mt-2">
                <img width="100" class="img-thumbnail" id="imagePreview{{$imageSingleKey ?? 'image'}}"
                     src="{{ !empty($data->{$imageSingleKey ?? 'image'}) ? $data->getImageUrl('small', $imageSingleKey ?? 'image') : '' }}"
                >

                <input type="hidden" name="delete_{{$imageSingleKey ?? 'image'}}"
                       id="imageRemoveFlag{{$imageSingleKey ?? 'image'}}" value="">
                <a class="btn-danger btn-sm ml-3 mt-0 text-decoration-none {{!empty($data->{$imageSingleKey ?? 'image'}) ? '' : 'd-none'}}"
                   id="imageRemove{{$imageSingleKey ?? 'image'}}" href="javascript:void(0)"
                   onclick="removeImg{{$imageSingleKey ?? 'image'}}()"
                   title="Xóa ảnh"><i class="fa fa-trash"></i> {{__('Xóa')}}</a>
            </div>
            @endif
        </div>
    </div>
</div>

@push('js_bot_all')
    <script type="text/javascript">
        $(document).ready(function () {
            document.getElementById('imageInput{{$imageSingleKey ?? 'image'}}').addEventListener('change', function () {
                readURL{{$imageSingleKey ?? 'image'}}(this);
            });
        });

        function removeImg{{$imageSingleKey ?? 'image'}}() {
            document.getElementById('imagePreview{{$imageSingleKey ?? 'image'}}').setAttribute('src', '');
            document.getElementById('imageRemoveFlag{{$imageSingleKey ?? 'image'}}').value = 1;
            document.getElementById('imageRemove{{$imageSingleKey ?? 'image'}}').classList.add('d-none');
            document.getElementById('imageInput{{$imageSingleKey ?? 'image'}}').value = null;
        }

        function readURL{{$imageSingleKey ?? 'image'}}(input) {
            if (input.files && input.files[0]) {
                let reader = new FileReader();

                reader.onload = function (e) {
                    document.getElementById('imagePreview{{$imageSingleKey ?? 'image'}}').setAttribute('src', e.target.result);
                    document.getElementById('imageRemoveFlag{{$imageSingleKey ?? 'image'}}').value = null;
                    document.getElementById('imageRemove{{$imageSingleKey ?? 'image'}}').classList.remove('d-none');
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endpush