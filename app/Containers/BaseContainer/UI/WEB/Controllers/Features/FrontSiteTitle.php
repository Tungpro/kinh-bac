<?php

namespace App\Containers\BaseContainer\UI\WEB\Controllers\Features;

use Illuminate\Support\Facades\View;

trait FrontSiteTitle
{
    protected function setSiteTitle($title = ''): void
    {
        $this->settings['website']['site_title'] = $title;

        View::share('site_title', $title);
    }

    protected function updateMetaTag($object = null, string $myTitle = null, string $myDescription = null, string $myKeyword = null): void
    {
        $myTitle = $myTitle ?: @$object->desc->meta_title ?: @$object->desc->title ?:  @$object->desc->name ?: '';
        if (!empty($myTitle)) {
            $this->setSiteTitle($myTitle);
        }

        $myDescription = $myDescription ?: @$object->desc->meta_description ?: @$object->desc->short_description ?: '';
        if (!empty($myDescription)) {
            $this->settings['website']['description'] = $myDescription;
        }

        $myKeyword = $myKeyword ?: @$object->desc->meta_keyword ?: '';
        if (!empty($myKeyword)) {
            $this->settings['website']['keywords'] = $myKeyword;
        }

        if (!empty($object->image)) {
            $this->settings['website']['image_seo'] = @$object->getImageUrl('seo');
            $this->settings['website']['img_width'] = 645;
            $this->settings['website']['img_height'] = 645;
        }

        if (!empty($myTitle) || !empty($myDescription) || !empty($myKeyword) || !empty($object->image)) {
            View::share('settings', $this->settings);
        }
    }
}