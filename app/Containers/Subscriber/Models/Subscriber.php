<?php

namespace App\Containers\Subscriber\Models;

use App\Containers\Construction\Models\Construction;
use App\Ship\core\Traits\HelpersTraits\DateTrait;
use App\Ship\Parents\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscriber extends Model
{
    use DateTrait;
    use SoftDeletes;
    
    protected $table = 'subscribers';

    public const KEY_COOKIE_REQUIRE_INFORMATION = '_KEY_COOKIE_REQUIRE_INFORMATION';
    
    protected $guarded = [];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'subscribers';

   
} // End class
