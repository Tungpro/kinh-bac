<?php


namespace App\Containers\Subscriber\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Subscriber\UI\API\Requests\FrontEnd\StoreSubscriberRequest;
use App\Containers\Subscriber\Actions\CreateSubscriberAction;

trait StoreSubscriber
{
    public function store(CreateSubscriberAction $CreateSubscriberAction, StoreSubscriberRequest $request)
    {
        $CreateSubscriberAction->run($request);
        return response()->json(['success' => true, 'message' => __('site.camonbandadangky')]);
    }
}
