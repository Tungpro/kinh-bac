<?php

namespace App\Containers\Subscriber\UI\API\Rules;

use Illuminate\Contracts\Validation\Rule;

class StoreSubscriberRule implements Rule
{
    public $errorMessage;

    public function __construct()
    {
        $this->errorMessage = __('site.email_email');
    }

    public function passes($attribute, $value)
    {
        if (!empty($value)) {
            if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
                return true;
            }
        } else {
            return true;
        }
    }

    public function message()
    {
        return $this->errorMessage;
    }
}