<?php

namespace App\Containers\Subscriber\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Subscriber\UI\WEB\Requests\DeleteSubscriberRequest;
use App\Containers\Subscriber\UI\WEB\Requests\GetAllSubscribersRequest;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Ship\Parents\Controllers\AdminController;
use Exception;

class SubscriberController extends AdminController
{
    use ApiResTrait;

    public function __construct()
    {
        $this->title = __('site.dangky');
        parent::__construct();

    }

    public function index(GetAllSubscribersRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);

        $subscribers = Apiato::call('Subscriber@GetAllSubscribersAction', [
            $request,
        ]);

        return view('subscriber::Admin.index', [
            'subscribers' => $subscribers,
            'request' => $request,
        ]);
    }

    public function delete(DeleteSubscriberRequest $request)
    {
        try {
            Apiato::call('Subscriber@DeleteSubscriberAction', [$request]);
        } catch (Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }
} // End class

