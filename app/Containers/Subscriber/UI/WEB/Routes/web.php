<?php

$router->group([
  'prefix' => 'subscriber',
  'namespace' => '\App\Containers\Subscriber\UI\WEB\Controllers\Desktop',
  'middleware' => [
    'web'
  ],
  'domain' => parse_url(config('app.url'))['host']
], function () use ($router) {
  $router->post('store', [
      'as' => 'home.subscriber.store',
      'uses'  => 'SubscriberController@store',
      'middleware' => ['web'],
  ]);
});
