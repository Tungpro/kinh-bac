@extends('basecontainer::admin.layouts.default')

@section('right-breads')
    {!! $subscribers->appends($request->all())->links('basecontainer::admin.inc.paginator') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-5"><h1>{{ $site_title }}</h1></div>

            @include('subscriber::Admin.filter')

            <div class="card card-accent-primary">
                <table class="table table-hover table-bordered mb-0" id="tableCustomer">
                    <thead>
                    <tr>
                        <th width="60" class="text-center">ID</th>
                        <th>Thông tin đăng ký</th>
                        <th>Ngày đăng ký</th>
                        <th width="60" class="text-center">Xóa</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse ($subscribers as $subscriber)
                        <tr>
                            <td align="center">{{ $subscriber->id }}</td>
                            <td>
                                <span class="d-block">
                                    <i class="fa fa-user w-15"></i>
                                    &nbsp;{{ $subscriber->email }}
                                </span>

                            </td>
                            <td>{{$subscriber->created_at}}</td>
                            <td align="center">
                                <input type="hidden" class="subscriber-info" value='@bladeJson($subscriber)'>
                                <a class="fa fa-trash text-danger cursor-pointer"
                                   data-href="{{ route('admin.subscriber.delete', $subscriber->id) }}"
                                   data-toggle="tooltip" data-original-title="Xóa thông tin này"
                                   onclick="admin.delete_this(this);">
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7">Không tìm thấy dữ liệu</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                @if ($subscribers->isNotEmpty())
                    <div class="d-flex py-2">
                       <span class="ml-auto mr-3">
                            Tổng cộng: {{ $subscribers->count() }}
                            bản ghi / {{ $subscribers->lastPage() }} trang
                       </span>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('js_bot_before')
    <script src="{{ asset("admin/js/library/language/select2/i18n/{$currentLang}.js") }}"></script>
@endpush