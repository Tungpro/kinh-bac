<?php

namespace App\Containers\Subscriber\Tasks;

use App\Containers\Subscriber\Data\Repositories\SubscriberRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindSubscriberByIdTask extends Task
{

    protected $repository;

    public function __construct(SubscriberRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
