<?php

namespace App\Containers\Subscriber\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteSubscriberAction extends Action
{
    public function run(Request $request)
    {
        $currentSubscriber = Apiato::call('Subscriber@FindSubscriberByIdTask', [$request->id]);
        $deleteResult = Apiato::call('Subscriber@DeleteSubscriberTask', [$request->id]);

        if ($deleteResult) {
            Apiato::call('User@CreateUserLogSubAction', [
                $currentSubscriber->id,
                $currentSubscriber->toArray(),
                [],
                __('Xóa bản ghi đăng ký'),
                get_class($currentSubscriber)
            ]);
        }
        
        return $deleteResult;
    }
}
