<?php

namespace App\Containers\Subscriber\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllSubscribersAction extends Action
{
    public function run(Request $request, array $with=[])
    {
        return Apiato::call('Subscriber@GetAllSubscribersTask', [], [
            [ 'filterSubscribers' => [$request] ],
            [ 'with' => [$with] ]
        ]);
    }
}
