<?php

namespace App\Containers\Agency\UI\WEB\Requests;

use App\Containers\BaseContainer\Traits\RequestBaseLanguage;
use App\Ship\Parents\Requests\Request;

/**
 * Class CreateAgencyRequest.
 *
 */
class CreateAgencyRequest extends Request
{
    use RequestBaseLanguage;
    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'roles'       => 'admin',
        'permissions' => 'agency-manager',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
      return [
//          'agency_description.*.name' => 'bail|required|string|max:255',
      ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
      return $this->messagesLang([
//        'agency_description.*.name.required' => "Tên language không được bỏ trống",
//        'agency_description.*.name.max' => "Tên language tối đa 255 ký tự",
      ]);
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
