<?php

namespace App\Containers\Agency\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\StringLib;
use App\Containers\Agency\Enums\AgencyType;
use App\Containers\Agency\Models\Agency;
use App\Containers\Agency\UI\WEB\Requests\Admin\FindAgencyRequest;
use App\Containers\Agency\UI\WEB\Requests\CreateAgencyRequest;
use App\Containers\Agency\UI\WEB\Requests\GetAllAgencyRequest;
use App\Containers\Agency\UI\WEB\Requests\UpdateAgencyRequest;
use App\Containers\Category\Actions\Admin\GetAllCategoriesAction;
use App\Ship\Parents\Controllers\AdminController;
use Exception;

class Controller extends AdminController
{
    public function __construct()
    {
        $this->title = 'Thông tin liên hệ';

        parent::__construct();

        $categories = app(GetAllCategoriesAction::class)->run(['parent_id' => 0],1,true);

        view()->share('agencyTypes', AgencyType::TEXT);

        view()->share('categories', $categories);
    }
    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllAgencyRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);

        $agencys = Apiato::call('Agency@AgencyListingAction', [$request, $this->perPage]);

        $options = array_merge(['' => 'Chọn vị trí'], config('agency-container.positions'));

        return view('agency::admin.index', [
            'search_data' => $request,
            'data' => $agencys,
            'positions' => $options
        ]);
    }

    public function edit(FindAgencyRequest $request)
    {
        $this->showEditForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['edit', $this->title, 'admin_agency_home_page']);

        try{
            $agency = Apiato::call('Agency@Admin\FindAgencyByIdAction', [$request->id]);

        }catch(Exception $e){
            return redirect()->route('admin_agency_home_page', ['id' => $request->id])->with('status', 'Có lỗi');
        }
// dd(config('agency-container.positions'));
        return view('agency::admin.edit', [
            'data' => $agency,
            'positions' => config('agency-container.positions')
        ]);

        return $this->notfound($request->id);
    }

    public function add()
    {
        $this->showAddForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['add', $this->title, 'admin_agency_home_page']);

        return view('agency::admin.edit', [
            'positions' => config('agency-container.positions')
        ]);
    }

    public function update(UpdateAgencyRequest $request)
    {
        try {
            $data =$request->except('image');

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'agency', StringLib::getClassNameFromString(Agency::class)]);
                if (!$image['error']) {
                    $data['image'] = $image['fileName'];
                }
            }

            $agency = Apiato::call('Agency@UpdateAgencyAction', [$data]);

            if ($agency) {
                return redirect()->route('admin_agencys_edit_page', ['id' => $agency->id])->with('status', 'Cập nhật agency thành công');
            }
        } catch (\Exception $e) {
            $this->throwExceptionViaMess($e);
        }
    }

    public function create(CreateAgencyRequest $request)
    {
        try {
            $data =$request->except('image');

            if (isset($request->image)) {
                $image = Apiato::call('File@UploadImageAction', [$request, 'image', 'agency', StringLib::getClassNameFromString(Agency::class)]);
                if (!$image['error']) {
                    $data['image'] = $image['fileName'];
                }
            }

            $agency = Apiato::call('Agency@CreateAgencyAction', [$data]);
            if ($agency) {
                return redirect()->route('admin_agency_home_page')->with('status', 'Agency đã được thêm mới');
            }
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function delete(FindAgencyRequest $request)
    {
        try {
            Apiato::call('Agency@DeleteAgencyAction', [$request]);
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function enable(FindAgencyRequest $request){
        try {
            Apiato::call('Agency@EnableAgencyAction', [$request]);
            return FunctionLib::ajaxRespondV2(true,'Success');
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function disable(FindAgencyRequest $request){
        try {
            Apiato::call('Agency@DisableAgencyAction', [$request]);
            return FunctionLib::ajaxRespondV2(true,'Success');
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }
}
