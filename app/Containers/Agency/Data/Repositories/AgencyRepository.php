<?php

namespace App\Containers\Agency\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class AgencyRepository.
 */
class AgencyRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Agency';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'status',
        'publish_at',
        'position',
        'views',
        'is_mobile',
        'created_at',
    ];
}
