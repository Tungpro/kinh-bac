<?php

namespace App\Containers\Agency\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class AgencyDescRepository.
 */
class AgencyDescRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Agency';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'news_id',
        'language_id',
        'name',
        'tag',
    ];
}
