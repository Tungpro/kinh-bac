<?php

namespace App\Containers\Agency\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Agency\Models\Agency;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class CreateAgencyAction.
 *
 */
class CreateAgencyAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        $agency = Apiato::call('Agency@CreateAgencyTask',[$data]);

        if ($agency) {
            Apiato::call('Agency@SaveAgencyDescTask', [$data, [], $agency->id]);

            Apiato::call('User@CreateUserLogSubAction', [
                $agency->id,
                [],
                $agency->toArray(),
                'Tạo Agency',
                Agency::class
            ]);
        }

        $this->clearCache();

        return $agency;
    }
}
