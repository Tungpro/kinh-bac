<?php

namespace App\Containers\Agency\Actions;

use App\Containers\Agency\Actions\SubActions\GetAvailableAgencyByPositionSubAction;
use App\Ship\Parents\Actions\Action;
use App\Containers\Localization\Models\Language;
use Illuminate\Database\Eloquent\Collection;

class GetAvailableAgencyByPositionAction extends Action
{
    public function run(array $positions = [], bool $isMobile, array $with = [], array $orderBy = [], Language $currentLang, int $limit = 0, $where): Collection
    {
       return app(GetAvailableAgencyByPositionSubAction::class)->run($positions, $isMobile, $with, $orderBy, $currentLang, $limit, $where);
    }
}
