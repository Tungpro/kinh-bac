<?php

namespace App\Containers\Agency\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Agency\Models\Agency;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class UpdateAgencyAction.
 *
 */
class EnableAgencyAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $agency = Apiato::call('Agency@EnableAgencyTask', [$data->id]);
        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            ['status' => 1],
            ['status' => 2],
            'Hiển thị Agency',
            Agency::class
        ]);

        $this->clearCache();

        return $agency;
    }
}
