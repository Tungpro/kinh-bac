<?php

namespace App\Containers\Agency\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Agency\Models\Agency;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Exception;

/**
 * Class UpdateAgencyAction.
 *
 */
class DisableAgencyAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $data)
    {
        $agency = Apiato::call('Agency@DisableAgencyTask', [$data->id]);

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            ['status' => 2],
            ['status' => 1],
            'Ẩn Agency',
            Agency::class
        ]);

        $this->clearCache();

        return $agency;
    }
}
