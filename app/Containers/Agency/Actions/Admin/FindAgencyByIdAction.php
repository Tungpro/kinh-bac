<?php

namespace App\Containers\Agency\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class FindAgencyByIdAction.
 *
 */
class FindAgencyByIdAction extends Action
{

    /**
     * @return mixed
     */
    public function run($agency_id)
    {
        $data = Apiato::call('Agency@FindAgencyByIdTask', [
            $agency_id,
            Apiato::call('Localization@GetDefaultLanguageTask'),
            ['with_relationship' => ['all_desc']]
        ]);

//        if ($data) {
//            $data->setRelation('all_desc', $data->all_desc->keyBy('language_id'));
//        }

        return $data;
    }
}
