<?php

namespace App\Containers\Agency\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Agency\Models\Agency;
use App\Ship\Parents\Actions\Action;

/**
 * Class DeleteAgencyAction.
 *
 */
class DeleteAgencyAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        Apiato::call('Agency@DeleteAgencyTask', [$data->id]);
        Apiato::call('Agency@DeleteAgencyDescTask', [$data->id]);

        $this->clearCache();

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            [],
            ['status' => -1],
            'XóaAgency',
            Agency::class
        ]);
    }
}
