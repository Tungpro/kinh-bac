<?php

namespace App\Containers\Agency\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Agency\Models\Agency;
use App\Ship\Parents\Actions\Action;

class UpdateAgencyAction extends Action
{
    public function run($data)
    {

        $beforeData = Apiato::call('Agency@FindAgencyByIdTask', [$data['id']]);

        $agency = Apiato::call('Agency@SaveAgencyTask', [$data]);

        if ($agency) {
            $original_desc = Apiato::call('Agency@GetAllAgencyDescTask', [$agency->id]);
            Apiato::call('Agency@SaveAgencyDescTask', [
                $data,
                $original_desc,
                $agency->id
            ]);


            Apiato::call('User@CreateUserLogSubAction', [
                $agency->id,
                $beforeData->toArray(),
                $agency->toArray(),
                'Cập nhật Agency',
                Agency::class
            ]);
        }

        $this->clearCache();

        return $agency;
    }
}
