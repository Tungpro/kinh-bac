<?php

namespace App\Containers\Agency\Tasks;

use App\Containers\Agency\Data\Repositories\AgencyDescRepository;
use App\Ship\Parents\Tasks\Task;

/**
 * Class SaveAgencyDescTask.
 */
class SaveAgencyDescTask extends Task
{

  protected $repository;

  public function __construct(AgencyDescRepository $repository)
  {
    $this->repository = $repository;
  }

  /**
   *
   * @return  mixed
   */
  public function run($data, $original_desc, $agency_id, $edit_id = null)
  {
    $agency_description = isset($data['agency_description']) ? $data['agency_description'] : null;

    if (is_array($agency_description) && !empty($agency_description)) {
      $updates = [];
      $inserts = [];

      foreach ($agency_description as $k => $v) {
        $attrItem = [];


        if (isset($original_desc[$k])) {
          $updates[$original_desc[$k]['id']] = [
            'name' => $v['name'],
           // 'short_description' => $v['short_description'],
          ];
          if (isset($data['image']) && !empty($data['image'])) {
            $updates[$original_desc[$k]['id']]['image'] = $data['image'];
          }

        } else {
          $inserts[] = [
            'agency_id' => $agency_id,
            'language_id' => $k,
            'name' => $v['name'],
           // 'short_description' => $v['short_description'],
          ];
        }

      }


      if (!empty($updates)) {
        $this->repository->updateMultiple($updates);
      }

      if (!empty($inserts)) {
        $this->repository->getModel()->insert($inserts);
      }
    }
  }
}
