<?php

namespace App\Containers\Agency\Tasks;

use App\Containers\Agency\Data\Repositories\AgencyDescRepository;
use App\Containers\Agency\Data\Repositories\AgencyRepository;
use App\Containers\News\Data\Repositories\NewsDescRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

/**
 * Class GetAllAgencyDescTask.
 */
class GetAllAgencyDescTask extends Task
{

    protected $repository;

    public function __construct(AgencyDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($agency_id)
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('agency_id', $agency_id));
        $wery = $this->repository->all()->keyBy('language_id')->toArray();

        return $wery;
    }
}
