<?php

namespace App\Containers\Agency\Tasks;

use App\Containers\Agency\Data\Repositories\AgencyRepository;
use App\Ship\Parents\Tasks\Task;

/**
 * Class FindAgencyByIdTask.
 */
class FindAgencyByIdTask extends Task
{

    protected $repository;

    public function __construct(AgencyRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($agency_id, $defaultLanguage = 1, $external_data = ['with_relationship' => ['all_desc']])
    {

        $data = $this->repository->with(array_merge($external_data['with_relationship']))->find($agency_id);

        return $data;
    }
}