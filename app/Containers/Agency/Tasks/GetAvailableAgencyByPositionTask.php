<?php

namespace App\Containers\Agency\Tasks;

use App\Containers\Agency\Data\Criterias\MobileCriteria;
use App\Containers\Agency\Data\Repositories\AgencyRepository;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Tasks\Task;

class GetAvailableAgencyByPositionTask extends Task
{

    protected $repository;

    public function __construct(AgencyRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Language $currentLang,int $limit = 0)
    {
        $this->currentLang = $currentLang->language_id ?? 1;

        $result = $this->repository->with(['desc' => function ($q) {
            $q->activeLang($this->currentLang);
        }]);

        return $limit > 0 ? $result->limit($limit) : $result->get();
    }

}
