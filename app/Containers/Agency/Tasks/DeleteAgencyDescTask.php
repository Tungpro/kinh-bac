<?php

namespace App\Containers\Agency\Tasks;

use App\Containers\Agency\Data\Repositories\AgencyDescRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeleteAgencyDescTask.
 */
class DeleteAgencyDescTask extends Task
{

    protected $repository;

    public function __construct(AgencyDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($agency_id)
    {
        try {
            $this->repository->getModel()->where('agency_id', $agency_id)->delete();
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
