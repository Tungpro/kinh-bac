<?php

namespace App\Containers\Agency\Tasks;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Agency\Data\Repositories\AgencyRepository;
use App\Containers\Agency\Enums\AgencyType;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;
use Illuminate\Support\Arr;

/**
 * Class SaveAgencyTask.
 */
class SaveAgencyTask extends Task
{

    protected $repository;

    public function __construct(AgencyRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $dataUpdate = Arr::except($data, ['agency_description', '_token', '_headers']);
            $agency = $this->repository->update($dataUpdate, $data['id']);
            return $agency;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}
