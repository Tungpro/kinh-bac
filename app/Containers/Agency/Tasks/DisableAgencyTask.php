<?php

namespace App\Containers\Agency\Tasks;

use App\Containers\Agency\Data\Repositories\AgencyRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeleteAgencyTask.
 */
class DisableAgencyTask extends Task
{

    protected $repository;

    public function __construct(AgencyRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($agency_id)
    {
        try {
            $this->repository->getModel()->where('id', $agency_id)->update(
                [
                    'status' => 1
                ]
            );
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
