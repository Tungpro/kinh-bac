<?php

namespace App\Containers\Agency\Tasks;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Agency\Data\Repositories\AgencyRepository;
use App\Containers\Agency\Enums\AgencyType;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class CreateAgencyTask.
 */
class CreateAgencyTask extends Task
{

    protected $repository;

    public function __construct(AgencyRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $data = Arr::except($data, ['agency_description', '_token', '_headers']);
            $data["publish_at"] = !empty($data['publish_at']) ? Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($data['publish_at'])) : Carbon::now();
            $data["end_at"] = !empty($data['end_at']) ? Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($data['end_at'])) : Carbon::now()->addDays(config('agency-container.days_of_end_at_default'));
            if(isset($data['position']))
            $data['position'] = is_array($data['position']) ? implode(',', $data['position']) : $data['position'];
            $data['is_mobile'] = isset($data['is_mobile']) ? AgencyType::MOBILE : AgencyType::DESKTOP;

            $agency = $this->repository->create($data);
            return $agency;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
