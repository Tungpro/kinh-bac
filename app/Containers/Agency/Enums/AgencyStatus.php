<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-10 16:16:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-10 16:17:03
 * @ Description: Happy Coding!
 */

namespace App\Containers\Agency\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class AgencyStatus extends BaseEnum
{
    const ACTIVE  = 2 ;
    const HIDE = 1 ;
    const DELETE = -1;

    const STATUS = [
        self::ACTIVE => 'Hiển thị',
        self::HIDE => 'Ẩn',
        self::DELETE => 'Xóa',
    ];
}
