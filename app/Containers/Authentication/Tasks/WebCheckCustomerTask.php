<?php

namespace App\Containers\Authentication\Tasks;

use App\Ship\Parents\Tasks\Task;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Containers\Authentication\Exceptions\LoginFailedException;

class WebCheckCustomerTask extends Task
{
    public function run(string $username, string $password, $remember = true): Authenticatable
    {
        $fieldName = filter_var( $username, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
        if (!$user = auth()->guard(config('auth.guard_for.frontend'))->attempt([$fieldName => $username, 'password' => $password], $remember)) {
            throw new LoginFailedException();
        }

        return auth()->guard(config('auth.guard_for.frontend'))->user();
    }
}
