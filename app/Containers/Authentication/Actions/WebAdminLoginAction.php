<?php

namespace App\Containers\Authentication\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Authorization\Exceptions\UserNotAdminException;
use App\Containers\Authorization\Exceptions\UserNotActiveException;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

/**
 * Class WebAdminLoginAction.
 *
 * @author Mahmoud Zalt <mahmoud@zalt.me>
 */
class WebAdminLoginAction extends Action
{

    /**
     * @param \App\Ship\Transporters\DataTransporter $data
     *
     * @return  \Illuminate\Contracts\Auth\Authenticatable
     */
    public function run(DataTransporter $data): Authenticatable
    {
        $user = Apiato::call('Authentication@AdminLoginTask',
            [$data->email, $data->password, $data->remember_me ?? false]);

        Apiato::call('Authentication@CheckIfAdminIsConfirmedTask', [], [['setUser' => [$user]]]);


        if (!$user->isActive()) {
            Auth::guard('admin')->logout();
            throw new UserNotActiveException();
        }

        return $user;
    }
}
