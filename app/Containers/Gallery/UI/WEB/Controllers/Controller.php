<?php

namespace App\Containers\Gallery\UI\WEB\Controllers;

use App\Containers\Gallery\UI\WEB\Requests\CreateGalleryRequest;
use App\Containers\Gallery\UI\WEB\Requests\DeleteGalleryRequest;
use App\Containers\Gallery\UI\WEB\Requests\GetAllGalleriesRequest;
use App\Containers\Gallery\UI\WEB\Requests\FindGalleryByIdRequest;
use App\Containers\Gallery\UI\WEB\Requests\UpdateGalleryRequest;
use App\Containers\Gallery\UI\WEB\Requests\StoreGalleryRequest;
use App\Containers\Gallery\UI\WEB\Requests\EditGalleryRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Ship\Parents\Controllers\AdminController;

/**
 * Class Controller
 *
 * @package App\Containers\Gallery\UI\WEB\Controllers
 */
class Controller extends AdminController
{
    use ApiResTrait;
    /**
     * Show all entities
     *
     * @param GetAllGalleriesRequest $request
     */
    public function index(GetAllGalleriesRequest $request)
    {
        $labels = Apiato::call('Gallery@GetAllGalleryLabelAction', [$request]);
        $pid = $request->pid ?? 0;
        $galleries = Apiato::call('Gallery@GetAllGalleriesAction', [$request, $pid]);
        return view('gallery::index', [
          'galleries' => $galleries,
          'labels' => $labels,
          'input' => $request->all()
        ]);
    }

    /**
     * Show one entity
     *
     * @param FindGalleryByIdRequest $request
     */
    public function show(FindGalleryByIdRequest $request)
    {
        $gallery = Apiato::call('Gallery@FindGalleryByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateGalleryRequest $request
     */
    public function create(CreateGalleryRequest $request)
    {
        // ..
    }

    /**
     * Add a new entity
     *
     * @param StoreGalleryRequest $request
     */
    public function store(StoreGalleryRequest $request)
    {
        $gallery = Apiato::call('Gallery@CreateGalleryAction', [$request]);


    }

    /**
     * Edit entity (show UI)
     *
     * @param EditGalleryRequest $request
     */
    public function edit(EditGalleryRequest $request)
    {
        $gallery = Apiato::call('Gallery@GetGalleryByIdAction', [$request]);

        // ..
    }

    /**
     * Update a given entity
     *
     * @param UpdateGalleryRequest $request
     */
    public function update(UpdateGalleryRequest $request)
    {
        $gallery = Apiato::call('Gallery@UpdateGalleryAction', [$request]);

        // ..
    }

    /**
     * Delete a given entity
     *
     * @param DeleteGalleryRequest $request
     */
    public function delete(DeleteGalleryRequest $request)
    {
      $result = Apiato::call('Gallery@DeleteGalleryAction', [$request]);
      return $this->sendResponse($request, __('Xóa tài liệu thành công'));
    }
}
