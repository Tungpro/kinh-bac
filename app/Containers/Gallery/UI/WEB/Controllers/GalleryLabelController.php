<?php

namespace App\Containers\Gallery\UI\WEB\Controllers;

use App\Containers\Gallery\UI\WEB\Requests\CreateGalleryRequest;
use App\Containers\Gallery\UI\WEB\Requests\DeleteGalleryRequest;
use App\Containers\Gallery\UI\WEB\Requests\GetAllGalleriesRequest;
use App\Containers\Gallery\UI\WEB\Requests\FindGalleryByIdRequest;
use App\Containers\Gallery\UI\WEB\Requests\UpdateGalleryRequest;
use App\Containers\Gallery\UI\WEB\Requests\StoreGalleryRequest;
use App\Containers\Gallery\UI\WEB\Requests\EditGalleryRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Gallery\UI\WEB\Requests\StoreGalleryLabelRequest;
use App\Containers\Gallery\UI\WEB\Requests\UpdateGalleryLabelRequest;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;

/**
 * Class GalleryLabelController
 *
 * @package App\Containers\Gallery\UI\WEB\Controllers
 */
class GalleryLabelController extends AdminController
{
  use ApiResTrait;

  public function __construct()
  {
    $this->dontUseShareData = true;
    parent::__construct();
  }
  /**
   * Show all entities
   *
   * @param GetAllGalleriesRequest $request
   */
  public function index(GetAllGalleriesRequest $request)
  {
    $galleries = Apiato::call('Gallery@GetAllGalleriesAction', [$request]);

    // ..
  }

  /**
   * Show one entity
   *
   * @param FindGalleryByIdRequest $request
   */
  public function show(FindGalleryByIdRequest $request)
  {
    $gallery = Apiato::call('Gallery@FindGalleryByIdAction', [$request]);

    // ..
  }

  /**
   * Create entity (show UI)
   *
   * @param CreateGalleryRequest $request
   */
  public function create(CreateGalleryRequest $request)
  {
    return view('gallery::labels.create');
  }

  /**
   * Add a new entity
   *
   * @param StoreGalleryRequest $request
   */
  public function store(StoreGalleryLabelRequest $request)
  {
    $transporter = new DataTransporter($request);
    $label = Apiato::call('Gallery@StoreGalleryLabelAction', [$transporter]);
    return redirect()->back()->with([
      'flash_level' => 'success',
      'flash_message' => sprintf('Nhãn: %s đã được tạo', $label->title),
      'objectData' => $label,
      'viewRender' => 'gallery::labels.inc.item',
      'itemAppend' => '#labelLeftSideBar',
      'itemEach' => '#labelLeftSideBar p',
    ]);
  }

  /**
   * Edit entity (show UI)
   *
   * @param EditGalleryRequest $request
   */
  public function edit(EditGalleryRequest $request)
  {
    $label = Apiato::call('Gallery@GetGalleryLabelByIdAction', [$request]);
    return view('gallery::labels.edit', [
      'label' => $label
    ]);
  }

  /**
   * Update a given entity
   *
   * @param UpdateGalleryRequest $request
   */
  public function update(UpdateGalleryLabelRequest $request)
  {
    $transporter = new DataTransporter($request);
    $transporter->id = $request->id;

    $label = Apiato::call('Gallery@UpdateGalleryLabelAction', [$transporter]);
    return redirect()->back()->with([
      'flash_level' => 'success',
      'flash_message' => sprintf('Nhãn: %s đã được cập nhật', $label->title),
      'objectData' => $label,
      'viewRender' => 'gallery::labels.inc.item',
      'itemAppend' => '#labelLeftSideBar',
      'itemEach' => '#labelLeftSideBar p',
      'dontPrepend' => 1
    ]);
  }

  /**
   * Delete a given entity
   *
   * @param DeleteGalleryRequest $request
   */
  public function delete(DeleteGalleryRequest $request)
  {
    $result = Apiato::call('Gallery@DeleteGalleryLabelAction', [$request]);
    return $this->sendResponse($request, __('Xoá nhãn thành công'));
  }
}
