@extends('basecontainer::admin.layouts.default_for_iframe')
@section('content')
    <div class="row" id="sectionContent">
        <div class="col-12">
            <div class="card mb-0">
                <form action="{{ route('admin.gallery.folder.store') }}" method="POST">
                    @csrf
                    <div class="card-header d-flex">
                        <a href="javascript:void(0)" class="mt-2">Tạo thư mục</a>
                        <div class="ml-auto">
                            <button type="button" class="btn btn-secondary" onclick="return closeFrame()">Đóng lại</button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Lưu thông tin</button>
                        </div>
                    </div>

                    <div class="card-bod">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="title">Tên thư mục</label>
                                        <input type="text"
                                            class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                            id="title" name="title" value="{{ old('email', @$data->title) }}" required />
                                    </div>

                                    <div class="form-group">
                                        <label for="title">Nhãn</label>
                                        <select name="cat_id" id="cat_id" class="form-control">
                                            @if ($labels->isNotEmpty())
                                                @foreach ($labels as $label)
                                                    <option value="{{ $label->id }}"
                                                        {{ \FunctionLib::selectCombobox(@$input['label_id'], $label->id) }}>
                                                        {{ $label->title }}
                                                    </option>
                                                @endforeach
                                            @else
                                            @endif
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="title">Thư mục cha</label>
                                        <select name="pid" id="pid" class="form-control">
                                            {!! $folders !!}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
