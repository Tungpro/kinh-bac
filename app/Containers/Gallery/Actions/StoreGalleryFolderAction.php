<?php

namespace App\Containers\Gallery\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class StoreGalleryFolderAction extends Action
{
    public function run(DataTransporter $dataTransporter)
    {

        $gallery = Apiato::call('Gallery@CreateGalleryTask', [$dataTransporter->toArray()]);

        return $gallery;
    }
}
