<?php

namespace App\Containers\Gallery\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllGalleryLabelAction extends Action
{
    public function run(Request $request)
    {
        $gallerylabels = Apiato::call('Gallery@GetAllGalleryLabelsTask', [], ['addRequestCriteria']);

        return $gallerylabels;
    }
}
