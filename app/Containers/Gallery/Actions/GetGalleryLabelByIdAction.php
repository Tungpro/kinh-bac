<?php

namespace App\Containers\Gallery\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetGalleryLabelByIdAction extends Action
{
    public function run(Request $request)
    {
        $gallerylabel = Apiato::call('Gallery@FindGalleryLabelByIdTask', [$request->id]);

        return $gallerylabel;
    }
}
