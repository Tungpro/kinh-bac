<?php

namespace App\Containers\Gallery\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class UpdateGalleryLabelAction extends Action
{
    public function run(DataTransporter $dataTransporter)
    {
        $gallerylabel = Apiato::call('Gallery@UpdateGalleryLabelTask', [$dataTransporter->id, $dataTransporter->toArray()]);
        return $gallerylabel;
    }
}
