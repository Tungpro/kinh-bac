<?php

namespace App\Containers\Gallery\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class GalleryLabelRepository
 */
class GalleryLabelRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
