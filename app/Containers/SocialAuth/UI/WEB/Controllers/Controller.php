<?php

namespace App\Containers\SocialAuth\UI\WEB\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Authentication\Actions\FrontEnd\ProxyApiFrontEndLoginAction;
use App\Containers\SocialAuth\Actions\SocialLoginAction;
use App\Ship\Parents\Controllers\WebController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\InvalidStateException;

/**
 * Class Controller
 *
 * @author  Mahmoud Zalt  <mahmoud@zalt.me>
 */
class Controller extends WebController
{

    /**
     * @param $provider
     *
     * @return  mixed
     */
    public function redirectAll($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * @param $provider
     *
     * @return  mixed
     */
    public function handleCallbackAll(Request $request,$provider)
    {
        $userSocialData = Socialite::driver($provider)->user();
        $returnData = app(SocialLoginAction::class)->run($provider,$userSocialData);

        if(!$returnData || !$returnData['user']) {
            throw new InvalidStateException;
        }

        auth()->guard(config('auth.guard_for.frontend'))->login($returnData['user'], true);

        $tokenContent = $this->genTokenApiCustomer($returnData['user']->email, $returnData['user']->password);

        // dd($tokenContent);

        return view('socialauth::login-success', [
            'token' => json_encode($tokenContent),
            'url' => routeFrontEndFromOthers('web_home_page')
        ]);
        // return redirect()->route('web_home_page');
    }

    private function genTokenApiCustomer($username, $password)
    {
        $fieldName = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
        $dataLogin = array_merge([
            $fieldName => $username,
            'password' => $password,
        ], [
            'client_id'       => Config::get('authentication-container.clients.web.customer.id'),
            'client_password' => Config::get('authentication-container.clients.web.customer.secret')
        ]);
        // dd($dataTransporter->toArray());
        $content = app(ProxyApiFrontEndLoginAction::class)->run($dataLogin,true);

        if ($content) {
        }

        return $content;
    }

}
