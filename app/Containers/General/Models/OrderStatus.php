<?php
/**
 * Created by PhpStorm.
 * Filename: StockStatus.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 8/6/20
 * Time: 15:39
 */

namespace App\Containers\General\Models;

use Apiato\Core\Traits\HasCompositePrimaryKey;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class OrderStatus extends Model
{
    use HasCompositePrimaryKey;
    protected $primaryKey = ['order_status_id', 'language_id'];
    protected $table = 'sys_order_status';
    public $incrementing = false;

    public function language()
    {
        return $this->hasOne(Language::class,'language_id','language_id');
    }
}