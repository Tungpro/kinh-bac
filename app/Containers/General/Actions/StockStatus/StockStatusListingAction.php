<?php

namespace App\Containers\General\Actions\StockStatus;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class StockStatusListingAction.
 *
 */
class StockStatusListingAction extends Action
{

    /**
     * @return mixed
     */
    public function run($conds = [['status', '>', -1]], $limit = 20)
    {
        $data = Apiato::call(
            'General@StockStatus\StockStatusListingTask',
            [
                $conds,
                $limit
            ]
        );

        return $data;
    }
}
