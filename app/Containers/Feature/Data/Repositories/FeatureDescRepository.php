<?php

namespace App\Containers\Feature\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class FeatureDescRepository.
 */
class FeatureDescRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Feature';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'feature_id',
        'language_id',
        'name',
    ];
}
