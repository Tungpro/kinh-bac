<?php
/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2020-10-04 16:13:21
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-02-15 15:51:42
 * @ Description:
 */

namespace App\Containers\Feature\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class FeatureDesc extends Model {
    protected $table = 'feature_description';

    public function language()
    {
        return $this->hasOne(Language::class,'language_id','language_id');
    }
}