<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-24 13:35:18
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-30 12:13:59
 * @ Description: Happy Coding!
 */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

Route::group(
    [
        'middleware' => [
            // 'auth:api-customer'
        ],
        'prefix' => app(CheckSegmentLanguageAction::class)->run() . '/notification',
    ],
    function () use ($router) {
        $router->get('/notiList', [
            'as' => 'api_list_noti',
            'uses' => 'FrontEnd\Controller@notiList'
        ]);

      
    }
);
