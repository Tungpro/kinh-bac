<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-21 12:34:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-30 12:13:36
 * @ Description: Happy Coding!
 */

namespace App\Containers\Notification\UI\API\Controllers\FrontEnd;

use App\Containers\BaseContainer\UI\WEB\Controllers\BaseApiFrontController;
use App\Containers\Notification\UI\API\Controllers\FrontEnd\Features\NotificationList;


class Controller extends BaseApiFrontController
{
    use NotificationList;

    public function __construct()
    {
        parent::__construct();
    }
}
