<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-21 12:34:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-14 14:54:14
 * @ Description: Happy Coding!
 */

namespace App\Containers\Notification\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Notification\Actions\GetAllNotificationAction;
use App\Containers\Notification\Enums\TypeNotification;
use App\Containers\Notification\UI\API\Requests\FrontEnd\NotificationListRequest;
use App\Containers\Notification\UI\API\Transformers\FrontEnd\NotiTransfomer;

trait NotificationList
{
    public $data = [];
    public function notiList(NotificationListRequest $request)
    {
        $currentCustomerAuthen = auth('customer')->user();
        $notis= app(GetAllNotificationAction::class)->run(
            $currentCustomerAuthen->id,
            get_class($currentCustomerAuthen),
            10,
            $request->type,
            true,
            $this->currentLang
        );
        $noti_list = $this->transform($notis, new NotiTransfomer, [], [], 'noti_list');
        $this->data['noti_list']= isset($noti_list['data']) ? $noti_list['data'] : [];
        $this->data['pagination'] = isset($noti_list['meta']['pagination']) ? $noti_list['meta']['pagination'] : [];
        $this->data['type_options'] = $this->typeOptions($request->type);
        return $this->data;
    }
    private function typeOptions(?string $selected)
    {
        return [
            [
                'title' => 'Tất cả',
                'type' => '0',
                'selected' => empty($selected) ? true : false
            ],
            [
                'title' => TypeNotification::TEXT[TypeNotification::ORDER],
                'type' => TypeNotification::ORDER,
                'selected' => $selected == TypeNotification::ORDER ? true : false
            ],
            [
                'title' => TypeNotification::TEXT[TypeNotification::PROMOTION],
                'type' => TypeNotification::PROMOTION,
                'selected' => $selected == TypeNotification::PROMOTION ? true : false
            ],
            [
                'title' => TypeNotification::TEXT[TypeNotification::EVENT],
                'type' => TypeNotification::EVENT,
                'selected' => $selected == TypeNotification::EVENT ? true : false
            ],

        ];
    }
}
