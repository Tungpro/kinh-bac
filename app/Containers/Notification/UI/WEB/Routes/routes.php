<?php

use App\Containers\Notification\Actions\PushNotificationAction;
use App\Containers\Notification\Values\NotificationStructureValue;
use App\Containers\User\Models\User;

$router->group([
  'prefix' => 'notifications',
  'namespace' => '\App\Containers\Notification\UI\WEB\Controllers\Admin',
  'middleware' => [
    'auth:admin'
  ],
  'domain' => sprintf('admin.%s', parse_url(config('app.url'))['host'])
], function () use ($router) {
  $router->any('/notifications/test', function () {
    $users = User::all();
    app(PushNotificationAction::class)->run($users, new NotificationStructureValue('asdsad', 'sadsad', []));
  });
  
  $router->get('/me', [
      'as' => 'web_notification_me',
      'uses'  => 'NotificationController@getMyNotification',
      'middleware' => [
        'auth:admin',
      ],
  ]);
  
  $router->post('/mark-read-all', [
    'as' => 'web_notification_mark_read_all',
    'uses'  => 'NotificationController@markReadAll',
    'middleware' => [
      'auth:admin',
    ],
  ]);
  
  $router->post('/mark-read', [
    'as' => 'web_notification_mark_read',
    'uses'  => 'NotificationController@markRead',
    'middleware' => [
      'auth:admin',
    ],
  ]);
  
  $router->post('/mark-unread', [
    'as' => 'web_notification_mark_unread',
    'uses'  => 'NotificationController@markUnread',
    'middleware' => [
      'auth:admin',
    ],
  ]);
  
  // $router->delete('/{id}', [
  //   'as' => 'web_notification_delete',
  //   'uses'  => 'NotificationController@delete',
  //   'middleware' => [
  //     'auth:admin',
  //   ],
  // ]);
  
  $router->get('/redirects/{id}', [
    'as' => 'web_notification_redirect_to_link',
    'uses'  => 'NotificationController@redirectToLink',
    'middleware' => [
      'auth:admin',
    ],
  ]);
});


