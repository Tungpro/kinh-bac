<?php
namespace App\Containers\Notification\UI\WEB\Controllers\Desktop;


use Illuminate\Http\RedirectResponse;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Notification\UI\WEB\Requests\Desktop\MarkAsReadRequest;
use App\Ship\Parents\Controllers\WebController;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Containers\Notification\UI\WEB\Requests\GetAllNotificationsRequest;
use App\Containers\Notification\UI\WEB\Requests\Desktop\RedirectToLinkRequest;

/**
 * Class Controller
 *
 * @package App\Containers\Notification\UI\WEB\Controllers
 */
class NotificationController extends WebController
{
    use ApiResTrait;

    /**
     * Show all entities
     *
     * @param GetAllNotificationsRequest $request
     */
    public function me()
    {
        $currentCustomerAuthen = auth('customer')->user();

        $myNotifications = Apiato::call('Notification@GetMyNotificationAction', [
            $currentCustomerAuthen->id,
            get_class($currentCustomerAuthen),
            20
        ]);

        return view("notification::{$this->screen}.me", [
            'myNotifications' => $myNotifications
        ]);
    }

    public function getTotalUnread() {
        $currentCustomerAuthen = auth('customer')->user();
        $totalUnread = Apiato::call('Notification@CountMyUnreadNotificationAction', [
            $currentCustomerAuthen->id,
            get_class($currentCustomerAuthen)
        ]);

        return $this->sendResponse($totalUnread);
    }

    public function redirect(RedirectToLinkRequest $request): RedirectResponse  {
        $notificationAsRead = Apiato::call('Notification@MarkNotificationAsReadAction', [$request]);
        if ($notificationAsRead) {
            return redirect()->to($request->redirect_link);
        }

        return redirect()->back()->with([
            'flash_level' => 'error',
            'flash_message' => 'Mark Notification As Read Failure'
        ]);
    }

    public function markAsRead(MarkAsReadRequest $request) {
        $notificationAsRead = Apiato::call('Notification@MarkNotificationAsReadAction', [$request]);
        return $this->sendResponse($notificationAsRead, __('Đánh dấu đã đọc thông báo thành công'));
    }
} // End class
