<?php

namespace App\Containers\Notification\UI\WEB\Requests\Desktop;

use App\Ship\Parents\Requests\Request;
use App\Containers\Notification\Traits\IsNotificationAuthorTrait;

/**
 * Class CreateNotificationRequest.
 */
class RedirectToLinkRequest extends Request
{
    use IsNotificationAuthorTrait;
    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    // protected $transporter = \App\Ship\Transporters\DataTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        // 'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'redirect_link' => ['required']
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'isAuthor',
        ]);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'read_at' => now()
        ]);
    }
}
