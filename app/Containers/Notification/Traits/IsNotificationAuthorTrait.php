<?php 
namespace App\Containers\Notification\Traits;

use Apiato\Core\Foundation\Facades\Apiato;

trait IsNotificationAuthorTrait {
    
    public function isAuthor() {
        return Apiato::call('Notification@CheckIsNotificationAuthorTask', [
            $this->id,
            $this->user()
        ]);
    }
}