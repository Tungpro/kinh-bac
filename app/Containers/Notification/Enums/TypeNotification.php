<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 17:28:48
 * @ Description: Happy Coding!
 */

namespace App\Containers\Notification\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;
use App\Containers\Notification\Notifications\OrderNotification;

final class TypeNotification extends BaseEnum
{
    /**
     * Đơn hàng
     */
    const ORDER = OrderNotification::class;
    
    /**
     * Khuyến mãi
     */
    const PROMOTION ='promotion' ;

    /**
     * Sự kiện
     */
    const EVENT = 'event';

    

    const TEXT = [
        self::ORDER => 'Đơn hàng',
        self::PROMOTION => 'Khuyến mãi',
        self::EVENT => 'Sự kiện',
    ];
}
