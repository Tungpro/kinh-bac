<?php

namespace App\Containers\Notification\Values;

use App\Ship\Parents\Values\BaseValue;

class NotificationStructureValue extends BaseValue
{

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'notificationstructurevalues';

    public $message; 
    public $link;
    public $otherInfo;
    public $objectTrigger;

    public function __construct(string $message='', string $link='', array $otherInfo=[], $objectTrigger=null)
    {
        $this->message = $message;
        $this->link = $link;
        $this->otherInfo = $otherInfo;
        $this->objectTrigger = $objectTrigger ?? auth()->user();
    }

    public function toArray(): array {
        return [
            'message' => $this->message,
            'link' => $this->link,
            'other_info' => $this->otherInfo,
            'object_trigger' => $this->objectTrigger
        ];
    }
} // End class
