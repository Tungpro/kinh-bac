<?php

namespace App\Containers\Notification\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\User\Models\User;

class CountMyUnreadNotificationAction extends Action
{
    public function run(int $userId, string $type=User::class): int
    {
        $myTotalUnreadNotification = Apiato::call('Notification@CountMyUnreadNotificationTask', [$userId, $type]);
        return $myTotalUnreadNotification;
    }
} // End class
