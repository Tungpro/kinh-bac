<?php

namespace App\Containers\Notification\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\User\Models\User;
use App\Containers\Localization\Models\Language;

class GetAllNotificationAction extends Action
{
    public function run(int $userId, string $notifiable_type=User::class, int $limit=10,$type, $isPanigate=true,Language $currentLang = null)
    {
        $myNotifications = Apiato::call('Notification@GetAllNotificationTask', [
           
            $userId,
            $notifiable_type,
            $limit,
            $type,
            $isPanigate,
            $currentLang
        ]);

        return $myNotifications;
    }
}
