<?php

namespace App\Containers\Notification\Tasks;

use Exception;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Exceptions\NotFoundException;
use Illuminate\Database\Eloquent\Collection;
use App\Ship\Criterias\Eloquent\LimitCriteria;
use App\Ship\Criterias\Eloquent\OrderByCreationDateDescendingCriteria;
use App\Containers\Notification\Data\Repositories\NotificationRepository;

class MarkNotificationReadStatusTask extends Task
{

    protected $repository;

    public function __construct(NotificationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $notificationData=[])
    {
        return $this->repository->update(['read_at' => $notificationData['read_at']], $notificationData['id']);
    }
}
