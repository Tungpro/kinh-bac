<?php

namespace App\Containers\Notification\Tasks;

use App\Containers\Customer\Models\Customer;
use App\Containers\Notification\Data\Repositories\NotificationRepository;
use App\Ship\Parents\Tasks\Task;

class CheckIsNotificationAuthorTask extends Task
{

    protected $repository;

    public function __construct(NotificationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(string $notificationId, Customer $customer): bool
    {
        $notification = $this->repository->find($notificationId);
        return $notification->notifiable_id == $customer->id && $notification->notifiable_type == Customer::class;
    }
}
