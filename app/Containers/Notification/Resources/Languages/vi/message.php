<?php 

return [
    'unread' => 'Chưa đọc',
    'old_notifications' => 'Các thông báo cũ hơn', 
    'recently_notification' => 'Thông báo gần đây',
    'mark_all_as_read' => 'Đánh dấu tất cả là đã đọc',
    'mark_as_read' => 'Đánh dấu là đã đọc',
    'mark_as_unread' => 'Đánh dấu là chưa đọc',
    'no_data' => 'Không có thông báo nào.'
];