<?php

namespace App\Containers\Notification\Models;

use Apiato\Core\Foundation\Facades\ImageURL;
use Apiato\Core\Foundation\FunctionLib;
use App\Containers\User\Models\User;
use App\Containers\Notification\Notifications\OrderNotification;
use App\Ship\Parents\Models\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Containers\Order\Enums\OrderStatus;

class Notification extends Model
{
    // use SoftDeletes;
    
    protected $table = 'notifications';
    public $incrementing = false;

    protected $fillable = ['read_at'];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [
        'data' => 'array'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $appends = ['redirect_link', 'avatar_image'];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'notifications';

    // public function getCreatedAtAttribute($date)
    // {
    //     $unixTimeStamps = strtotime($date);
    //     return date('d/m/Y H:i', $unixTimeStamps);
    // }

    // public function getUpdatedAtAttribute($date)
    // {
    //     $unixTimeStamps = strtotime($date);
    //     return date('d/m/Y H:i', $unixTimeStamps);
    // }

    public function getRedirectLinkAttribute(): string {
        if (auth('admin')->check()) {
            return route('web_notification_redirect_to_link', [
                'id' => $this->id,
                'redirect_link' => $this->data['link']
            ]);
        }
        
        if (auth('customer')->check()) {
            if (strlen($this->data['link']) > 10) {
                return route('notifications.me.redirect', [
                    'id' => $this->id,
                    'redirect_link' => $this->data['link']
                ]);
            }
        }

        return 'javascript:void(0)';
    }

    public function getCarbonDiffForHumans() {
        Carbon::setLocale(config('app.locale'));
        return Carbon::parse($this->created_at)->diffForHumans();
    }

    public function getObjectTriggerAvatar($size = 'mini') {
        if (!empty($this->data['object_trigger']['avatar'])) {
            if( $this->notifiable_type == User::class ||
                isset($this->data['object_trigger']['permissions']) ||
                isset($this->data['object_trigger']['roles']))
            {
                return \ImageURL::getImageUrl($this->data['object_trigger']['avatar'], 'avatar', $size);
            }
            if ( !empty($this->data['object_trigger']['is_contractor']) ) {
                return \ImageURL::getImageUrl($this->data['object_trigger']['avatar'], 'contractor', $size);
            }
            return \ImageURL::getImageUrl($this->data['object_trigger']['avatar'], 'customer', $size);
        }
        return FunctionLib::getDefaultAvatar($size, !empty($this->data['object_trigger']['is_contractor']));
        //return asset('template_mobile/images/avatar-default.png');
    }

    public function isUnread(): bool {
        return empty($this->read_at);
    }

    public function hasRedirectLink(): bool {
        return $this->redirect_link != 'javascript:void(0)';
    }

    public function getAvatarImageAttribute() {
        return $this->getObjectTriggerAvatar('mini');
    }

    public function getImageNotification($size='medium'){
        if (!empty($this->data['other_info']['image'])) {
           
            if( $this->type == OrderNotification::class)
            {
                return \ImageURL::getImageUrl($this->data['other_info']['image'], 'product', $size);
            }
           
        }
        return 'http://placehold.jp/300x300.png';

    }

    public function getStatus(){
        if (!empty($this->data['other_info']['status'])) {
            if( $this->type == OrderNotification::class)
            {
                return OrderStatus::TEXT[$this->data['other_info']['status']];
            }
           
        }
       
    }
}
