<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-10 12:10:36
 * @ Description: Happy Coding!
 */

namespace App\Containers\Personalish\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class ServiceOption extends BaseEnum
{
    /**
     * Giao nhanh
     */
    const FAST_SHIP = 1;

    const ABCXYZ = 2;

    const TEXT = [
        self::FAST_SHIP => 'Giao hàng nhanh',
        self::ABCXYZ => 'Abcxyz'
    ];

    const SERVICE_OPTIONS = [
        [
            "display_value" => self::TEXT[self::FAST_SHIP],
            "query_value" => self::FAST_SHIP,
            "selected" => false
        ],
        [
            "display_value" => self::TEXT[self::ABCXYZ],
            "query_value" => self::ABCXYZ,
            "selected" => false
        ],
    ];
}
