<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-10 11:50:52
 * @ Description: Happy Coding!
 */

namespace App\Containers\Personalish\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class RatingOption extends BaseEnum
{
    /**
     * 5 sao
     */
    const FIVE = 5;

    /**
     * 4 sao
     */
    const FOUR = 4;

    /**
     * 3 sao
     */
    const THREE = 3;

    /**
     * 2 sao
     */
    const TWO = 2;

    /**
     * 1 sao
     */
    const ONE = 1;

    const TEXT = [
        self::FIVE => '5 sao',
        self::FOUR => '4 sao',
        self::THREE => '3 sao',
        self::TWO => '2 sao',
        self::ONE => '1 sao',
    ];

    const RATING_OPTIONS = [
        [
            "display_value" => "từ 5 sao",
            "query_value" => self::FIVE,
            "selected" => false
        ],
        [
            "display_value" => "từ 4 sao",
            "query_value" => self::FOUR,
            "selected" => false
        ],
        [
            "display_value" => "từ 3 sao",
            "query_value" => self::THREE,
            "selected" => false
        ],
    ];
}
