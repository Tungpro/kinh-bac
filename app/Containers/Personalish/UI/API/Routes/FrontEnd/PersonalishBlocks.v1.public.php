<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-04 16:55:35
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-26 15:33:42
 * @ Description: Happy Coding!
 */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

Route::group(
[
    'middleware' => [
        // 'api',
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run().'/personalish/blocks',
],
function () use ($router) {
    $router->any('/listingProduct', [
        'as' => 'api_personalish_listing_product',
        'uses'       => 'FrontEnd\Controller@listingProduct'
    ]);
    $router->any('/suggestProductByCart', [
        'as' => 'api_personalish_suggest_prd_cart',
        'uses'       => 'FrontEnd\Controller@suggestProductByCart'
    ]);
    $router->get('/suggestProductSearchBox', [
        'as' => 'api_personalish_suggest_prd_search_box',
        'uses'       => 'FrontEnd\Controller@suggestProductSearchBox'
    ]);
});