<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-09 01:30:53
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-02 14:51:49
 * @ Description: Happy Coding!
 */

namespace App\Containers\Personalish\UI\API\Transformers\FrontEnd\Filter;

use App\Containers\Filter\Enums\FilterType;
use App\Ship\Parents\Transformers\Transformer;

class FilterTransformer extends Transformer
{
    public $selectedValues;

    public function __construct(array $selectedValues)
    {
        $this->selectedValues = $selectedValues;
        parent::__construct();
    }
    public function transform($filter)
    {
        $returnData = [
            'count' => 0,
            'query_value' => (int)$filter->filter_id,
            'display_value' => $filter->desc->name,
            'selected' => isset($this->selectedValues[FilterType::OPTION]) ?  in_array($filter->filter_id,$this->selectedValues[FilterType::OPTION]) : false
        ];

        if($filter->value_range) {
            $returnData['value_range'] = $filter->value_range;
            $returnData['selected'] = isset($this->selectedValues[FilterType::RANGE_OPTION]) ?  in_array(explode('-',$filter->value_range),[$this->selectedValues[FilterType::RANGE_OPTION]]) : false;
        }

        return $returnData;
    }
}
