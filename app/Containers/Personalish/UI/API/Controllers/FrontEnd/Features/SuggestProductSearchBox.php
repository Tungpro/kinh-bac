<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-26 16:43:34
 * @ Description: Happy Coding!
 */

namespace App\Containers\Personalish\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Personalish\Enums\SortOption;
use App\Containers\Personalish\Services\ListingProductService;
use App\Containers\Personalish\UI\API\Requests\FrontEnd\SuggestProductSearchBoxRequest;
use App\Containers\Product\UI\API\Transformers\FrontEnd\ProductListTransformer;

trait SuggestProductSearchBox
{
    public function suggestProductSearchBox(SuggestProductSearchBoxRequest $request, ListingProductService $listingProductService)
    {
        $q = $request->q;
        $this->data['block'] = [
            'code' => 'suggest_product_search_box',
            'title' => 'Product Listing',
            'icon' => ''
        ];

        if ($q) {
            $this->data['products'] = $listingProductService->skipCache()->getListingProduct([
                'likeName' => $q,
                'sortOption' => SortOption::TOP_SELLER,
                // 'inRandomOrder' => true
            ], $this->currentLang, 10, request()->page ?? 1);

            $this->data['products'] = $this->transform($this->data['products'], new ProductListTransformer, [], [], 'product_list');
        } else {
            $this->data['products'] = [];
        }

        return $this->data;
    }
}
