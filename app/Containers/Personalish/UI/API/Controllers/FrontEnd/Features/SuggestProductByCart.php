<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-19 19:59:13
 * @ Description: Happy Coding!
 */

namespace App\Containers\Personalish\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Personalish\Services\ListingProductService;
use App\Containers\Personalish\UI\API\Requests\FrontEnd\SuggestProductCartRequest;
use App\Containers\Product\Actions\CateIdsDirectFromPrdIdAction;
use App\Containers\Product\UI\API\Transformers\FrontEnd\ProductListTransformer;
use App\Containers\ShoppingCart\Actions\FrontEnd\GetContentCartAction;

trait SuggestProductByCart
{
    public function suggestProductByCart(SuggestProductCartRequest $request,ListingProductService $listingProductService)
    {
        $cart = app(GetContentCartAction::class)->currentLang($this->currentLang)->instance()->currentLang($this->currentLang)->run();

        if($cart['count'] > 0){
            $prdIds = [];
            foreach($cart['items'] as $item) {
                $prdIds[] = $item['id'];
            }

            $cateIds = app(CateIdsDirectFromPrdIdAction::class)->run($prdIds);
        }

        $this->data['block'] = [
            'code' => 'suggest_product_cart',
            'title' => 'Product Listing',
            'icon' => ''
        ];

        $this->data['products'] = $listingProductService->skipCache()->skipPagin()->getListingProduct([
            'cate_ids' => isset($cateIds) ? array_unique($cateIds) : [],
            'inRandomOrder' => true
        ], $this->currentLang, 10, request()->page ?? 1);

        $this->data['products'] = $this->transform($this->data['products'], new ProductListTransformer, [], [], 'product_list');

        return $this->data;
    }
}
