<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-26 15:27:52
 * @ Description: Happy Coding!
 */

namespace App\Containers\Personalish\UI\API\Controllers\FrontEnd;

use App\Containers\BaseContainer\UI\WEB\Controllers\BaseApiFrontController;
use App\Containers\Personalish\UI\API\Controllers\FrontEnd\Features\ListingProduct;
use App\Containers\Personalish\UI\API\Controllers\FrontEnd\Features\SuggestProductByCart;
use App\Containers\Personalish\UI\API\Controllers\FrontEnd\Features\SuggestProductSearchBox;

class Controller extends BaseApiFrontController
{
    use ListingProduct,
        SuggestProductByCart,
        SuggestProductSearchBox;

    public function __construct()
    {
        parent::__construct();
    }
}
