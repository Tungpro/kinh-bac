<?php

use App\Containers\Personalish\Enums\SortOption;

return [
    "item_show_options" => [
        [
            "display_value" => "20",
            "query_value" => "20",
            "selected" => false
        ],
        [
            "display_value" => "40",
            "query_value" => "40",
            "selected" => false
        ],
        [
            "display_value" => "60",
            "query_value" => "60",
            "selected" => false
        ]
    ],
];
