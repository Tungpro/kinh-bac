<?php

namespace App\Containers\Tags\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class TagsRepository
 */
class TagFilterRepository extends Repository
{

    /**
     * @var array
     */
    protected $container = 'Tags';
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
