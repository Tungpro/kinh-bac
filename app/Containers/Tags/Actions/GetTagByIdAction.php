<?php

namespace App\Containers\Tags\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetTagByIdAction extends Action
{
    public function run($tag_id)
    {
      $tags = Apiato::call('Tags@GetTagsByIdTask', [$tag_id]);
        return $tags;
    }
}
