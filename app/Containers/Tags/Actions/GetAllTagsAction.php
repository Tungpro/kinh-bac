<?php

namespace App\Containers\Tags\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllTagsAction extends Action
{
    public function run(Request $request,$data,$perPage=15)
    {
        $tags = Apiato::call('Tags@GetAllTagsTask', [$data,$perPage]);

        return $tags;
    }
}
