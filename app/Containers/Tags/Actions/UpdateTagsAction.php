<?php

namespace App\Containers\Tags\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateTagsAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $tags = Apiato::call('Tags@UpdateTagsTask', [$request->id, $data]);
        $this->clearCache();
        return $tags;
    }
}
