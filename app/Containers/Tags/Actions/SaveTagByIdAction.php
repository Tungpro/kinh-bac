<?php

namespace App\Containers\Tags\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class SaveTagByIdAction extends Action
{
    public function run($id,$data)
    {
      $tags =  Apiato::call('Tags@SaveTagsTask', [$id,$data]);

      Apiato::call('Tags@TagDes\SaveTagsDesTask', [$tags->id,$data['all_desc']]);
      if(isset($data['tag_filter'])){
        $this->call('Tags@TagFilter\SaveTagFilterTask', [$data['tag_filter'],$tags]);
      }
      if(isset($data['tag_category'])){
        $this->call('Tags@TagCategory\SaveTagCategoryTask', [$data['tag_category'],$tags]);
      }
      $this->clearCache();

      return $tags;

    }
}
