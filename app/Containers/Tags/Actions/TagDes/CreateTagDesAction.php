<?php

namespace App\Containers\Tags\Actions\TagDes;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class CreateTagDesAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $tags = Apiato::call('Tags@CreateTagDesTask', [$data]);

        return $tags;
    }
}
