<?php

namespace App\Containers\Tags\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetTagsByIDPrdAction extends Action
{
    public function run($prd_id,$input='',$type='product',$cate='tag')
    {
      $tags = Apiato::call('Tags@FindTagsByIdTask', [$prd_id,$input,$type,$cate]);
        return $tags;
    }
}
