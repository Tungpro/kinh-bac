<?php

namespace App\Containers\Tags\Actions\TagCategory;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class CateIdsDirectFromPrdIdAction.
 *
 */
class CateIdsDirectFromIdAction extends Action
{

    /**
     * @return mixed
     */
    public function run($id)
    {
        $data = Apiato::call('Tags@TagCategory\CateIdsDirectFromIdTask', [
            $id
        ]);
        return $data;
    }
}
