<?php

namespace App\Containers\Tags\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindTagsByIdAction extends Action
{
    public function run(Request $request)
    {
        $tags = Apiato::call('Tags@FindTagsByIdTask', [$request->id]);

        return $tags;
    }
}
