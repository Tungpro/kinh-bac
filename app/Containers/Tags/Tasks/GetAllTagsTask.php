<?php

namespace App\Containers\Tags\Tasks;

use App\Containers\Tags\Data\Repositories\TagsRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;

class GetAllTagsTask extends Task
{

    protected $repository;

    public function __construct(TagsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($filters,$perPage){
      if( isset($filters['id']) && !empty($filters['id'])){
          $this->repository->pushCriteria(new ThisEqualThatCriteria('id', $filters['id']));
        }
      if(isset($filters['title']) && !empty($filters['title']) ){
        $this->repository->pushCriteria(new ThisOperationThatCriteria('title', '%'.$filters['title'].'%' ,'like'));
      }
      $this->repository->orderBy('created', 'desc');
      $result =  $this->repository->paginate($perPage);
        return $result;
    }
}
