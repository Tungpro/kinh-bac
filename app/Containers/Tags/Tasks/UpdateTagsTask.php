<?php

namespace App\Containers\Tags\Tasks;

use App\Containers\Tags\Data\Repositories\TagsRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateTagsTask extends Task
{

    protected $repository;

    public function __construct(TagsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
