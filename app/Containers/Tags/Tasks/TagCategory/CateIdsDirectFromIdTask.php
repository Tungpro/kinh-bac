<?php

namespace App\Containers\Tags\Tasks\TagCategory;

use App\Containers\Tags\Data\Repositories\TagCategoryRepository;
use App\Ship\Criterias\Eloquent\SelectFieldsCriteria;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

/**
 * Class CateIdsDirectFromPrdIdTask.
 */
class CateIdsDirectFromIdTask extends Task
{

    protected $repository;

    public function __construct(TagCategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return  mixed
     */
    public function run($campaign_id)
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('tag_id',$campaign_id));
        $this->repository->pushCriteria(new SelectFieldsCriteria(['category_id']));

        $result = $this->repository->all();

        if($result && !$result->isEmpty()) {
            return $result->pluck('category_id')->toArray();
        }

        return [];
    }
}
