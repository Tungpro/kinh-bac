<?php

namespace App\Containers\Tags\Tasks;

use App\Containers\Tags\Data\Repositories\TagsRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindTagsByInputTask extends Task
{

    protected $repository;

    public function __construct(TagsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($input,$type,$cate)
    {
        try {
            return $this->repository->getByInput($input,$type,$cate);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
