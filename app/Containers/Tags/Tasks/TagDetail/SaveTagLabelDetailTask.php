<?php

namespace App\Containers\Tags\Tasks\TagDetail;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Tags\Data\Repositories\TagDetailsRepository;
/**
 * Class SaveProductCategoryTask.
 */
class SaveTagLabelDetailTask extends Task
{
    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function __construct(TagDetailsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($object_tags,$prd_id)
    {
        return $this->repository->saveAndAddTagLabel($object_tags,$prd_id);
    }
}
