<?php

namespace App\Containers\Tags\Tasks\TagDetail;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Tags\Data\Repositories\TagDetailsRepository;
/**
 * Class SaveProductCategoryTask.
 */
class SaveTagDetailTask extends Task
{
    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function __construct(TagDetailsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($object_tags,$prd_id,$type,$cate)
    {
        return $this->repository->saveAndAddTag($object_tags,$prd_id,$type,$cate);
    }
}
