<?php

namespace App\Containers\Tags\Tasks\TagDes;
use Illuminate\Support\Str;
use App\Containers\Tags\Data\Repositories\TagDescRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Apiato\Core\Foundation\Facades\Apiato;

class SaveTagsDesTask extends Task
{

    protected $repository;

    public function __construct(TagDescRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id='', array $data)
    {
            if(!empty($data)){
              foreach ($data as $key => $item) {
                if(!empty($item) && isset($item['id'])){
                    $this->repository->getModel()->where('tag_id',  $id)->where('language_id',$item['language_id'])->update($item);
                }elseif(!empty($item) && !isset($item['id'])){
                  $item['tag_id'] = $id;
                  $checkIsset =  $this->repository->getModel()->where('tag_id',  $id)->where('language_id',$item['language_id'])->first();
                  if(empty($checkIsset))
                    $this->repository->getModel()->insert($item);
                  else{
                    $this->repository->getModel()->where('tag_id',  $id)->where('language_id',$item['language_id'])->update($item);
                  }
                }
              }
            }

    }
}
