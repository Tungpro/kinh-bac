<?php

namespace App\Containers\Tags\Tasks;

use App\Containers\Tags\Data\Repositories\TagsRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Tags\Models\Tags;
use Exception;

class CreateTagsTask extends Task
{

    protected $repository;

    public function __construct(TagsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
