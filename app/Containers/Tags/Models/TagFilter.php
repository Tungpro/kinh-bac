<?php
/**
 * Created by PhpStorm.
 * Filename: ProductDesc.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 7/23/20
 * Time: 10:38
 */

namespace App\Containers\Tags\Models;

use App\Ship\Parents\Models\Model;
use App\Containers\Localization\Models\Language;

class TagFilter extends Model
{
    protected $table = 'tag_filter';
    protected $primaryKey = ['tag_id','filter_id'];
}
