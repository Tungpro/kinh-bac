<?php
/**
 * Created by PhpStorm.
 * Filename: ProductDesc.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 7/23/20
 * Time: 10:38
 */

namespace App\Containers\Tags\Models;

use App\Ship\Parents\Models\Model;
use App\Containers\Localization\Models\Language;

class TagDesc extends Model
{
    protected $table = 'tag_description';
    protected $fillable = ['language_id'];
    public function tag()
    {
        return $this->hasOne(Tag::class,'id','tag_id');
    }

    public function language()
    {
        return $this->hasOne(Language::class,'language_id','language_id');
    }
}
