<?php

namespace App\Containers\Tags\Models;

use Exception;
use App\Ship\Parents\Models\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Apiato\Core\Foundation\Facades\ImageURL;
use App\Containers\Collection\Models\Collection;
use App\Containers\Tags\Enums\TagType;
use App\Containers\Tags\Models\TagDesc;
use App\Containers\Category\Models\Category;
use App\Containers\Filter\Models\Filter;

class Tags extends Model
{
    protected $table = 'tags';

    protected $fillable = ['title', 'color', 'image', 'save_title', 'created', 'type', 'cate', 'primary_category_id'];

    protected $attributes = [];

    protected $hidden = [];

    protected $casts = [];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
    public $timestamps = false;

    const TYPE = [
        'product' => 'Sản phẩm',
        'news' => 'Tin tức',
        'collection' => 'Bộ sưu tập',
    ];

    const CATE = [
        'tag' => 'Tag',
        'label' => 'Nhãn đặc biệt',
    ];
    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'tags';

    public function getAll()
    {
        return $this->get();
    }

    public function getByInput($input, $type = '', $cate = 'tags', $limit = 30)
    {
        return $this->where('title', 'like', '%' . $input . '%')->where('cate', $cate)->where('type', $type)->limit($limit)->get();
    }

    public function getById($id)
    {
        return $this->with('all_desc')->find($id);
    }

    public function desc()
    {
        return $this->hasOne(TagDesc::class, 'tag_id', 'id');
    }

    public function all_desc()
    {
        return $this->hasMany(TagDesc::class, 'tag_id', 'id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, TagCategory::getTableName(), 'tag_id', 'category_id');
    }

    public function getTypeTag()
    {
        return self::TYPE;
    }

    public function filters()
    {
        return $this->belongsToMany(Filter::class, TagFilter::getTableName(), 'tag_id', 'filter_id');
    }

    public function getCateTag()
    {
        return self::CATE;
    }

    public function collection()
    {
        return $this->hasOne(Collection::class, 'id', 'object_id')->where('type', TagType::COLLECTION);
    }

    public function getImageUrl($size = 'original')
    {
        return ImageURL::getImageUrl($this->image, 'tags', $size);
    }

    public static function addTags($data, $type, $cate)
    {
        $tag = new Tags;
        try {
            $data = [
                'title' => $data['tags']['title'],
                'safe_title' => Str::slug($data['tags']['title']),
                'created' => time(),
                'type' => $type,
                'cate' => $cate,
            ];
            // $dataUpdate = Arr::except($data->toArray(), ['news_description', '_token', '_headers']);

            $tag = $tag->insertGetId($data);
            return $tag;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
