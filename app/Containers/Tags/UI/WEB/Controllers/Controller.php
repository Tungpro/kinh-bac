<?php

namespace App\Containers\Tags\UI\WEB\Controllers;

use App\Containers\Tags\UI\WEB\Requests\GetAllTagsRequest;
use App\Containers\Tags\UI\WEB\Requests\ListTagsRequest;
use App\Containers\Tags\UI\WEB\Requests\EditTagsRequest;
use App\Containers\Tags\UI\WEB\Requests\SaveTagsRequest;
use App\Containers\Tags\UI\WEB\Requests\AjaxGetTagRequest;
use App\Containers\Tags\UI\WEB\Requests\UploadImgTagRequest;
use App\Ship\Parents\Controllers\AdminController;
use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\StringLib;
use App\Containers\Tags\Models\Tags;


/**
 * Class Controller
 *
 * @package App\Containers\Tags\UI\WEB\Controllers
 */
class Controller extends AdminController
{
    /**
     * Show all entities
     *
     * @param GetAllTagsRequest $request
     */
    public function __construct()
    {
        $this->title = 'Quản trị Tag và Nhãn';
        parent::__construct();
    }
    public function index(GetAllTagsRequest $request)
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Tag', $this->form == 'list' ? '' : route('admin_tag_home_page',['']));
        \View::share('breadcrumb', $this->breadcrumb);

        return view('tags::admin.index', [
        ]);
    }

    public function ajaxGetTagByIdPrd(AjaxGetTagRequest $request){
      $transporter = $request->toTransporter();
      $transporter = $transporter->toArray();
      $tags = Apiato::call('Tags@GetTagsByIDPrdAction', [$transporter['object_id'],@$transporter['queryString'],$transporter['type'],$transporter['cate']]);
      if(!empty($tags)){
        return FunctionLib::ajaxRespond(true, 'Hoàn thành', $tags->toArray());
      }
      return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    public function ajaxGetSearchTagByIdPrd(AjaxGetTagRequest $request){
      $transporter = $request->toTransporter();
      $transporter = $transporter->toArray();
      $tags = Apiato::call('Tags@GetTagsByInputAction', [@$transporter['queryString'],$transporter['type'],$transporter['cate']])->toArray();
      if(!empty($tags)){
          return FunctionLib::ajaxRespond(true, 'Hoàn thành', $tags);
      }
      return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    public function ajaxGetCate(){
      try {
        $cate = Apiato::call('Tags@GetCateTask');
        return FunctionLib::ajaxRespond(true, 'ok', $cate);
      } catch (\Exception $th) {
        return $th;
      }
    }

    public function ajaxGetType(){
      try {
        $cate = Apiato::call('Tags@GetTypeTask');
        return FunctionLib::ajaxRespond(true, 'ok', $cate);
      } catch (\Exception $th) {
        return $th;
      }
    }

    public function ajaxImgUpload(UploadImgTagRequest $request){
        if ($request->hasFile('Filedata')) {
            $image = $request->file('Filedata');
        if ($image->isValid()) {
            $title = basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension());
            $fname = Apiato::call('File@UploadImageAction', [$request, 'Filedata', $title, StringLib::getClassNameFromString(Tags::class)]);
            if (!empty($fname) && !$fname['error']) {
                    $link= \ImageURL::getImageUrl(@$fname['fileName'], 'tags', 'small');
                    $fname['link']=$link;
                    return FunctionLib::ajaxRespond(true, 'ok',$fname);
            }
            return FunctionLib::ajaxRespond(false, 'Upload ảnh thất bại!');
        }
        return FunctionLib::ajaxRespond(false, 'File không hợp lệ!');
    }
    return FunctionLib::ajaxRespond(false, 'Không tìm thấy ảnh!');
    }

    public function ajaxTagList(ListTagsRequest $request){
        $tags = Apiato::call('Tags@GetAllTagsAction', [$request,$request->fillters]);
        return FunctionLib::ajaxRespond(true, 'ok', $tags);
    }
    public function ajaxTagByID(EditTagsRequest $request){
      $tag=[];
        if(isset($request->id)){
            $tagData =  Apiato::call('Tags@GetTagByIdAction',[$request->id]);
            if(!empty($tagData->all_desc)){
              foreach ($tagData->all_desc as $item) {
                $tag[$item->language_id] = $item;
              }
              unset($tagData['all_desc']);
              $tagData['all_desc'] = $tag;
            }else{
              $langs = Apiato::call('Localization@GetAllLanguageDBAction');

            }
            if(!empty($tagData)){
                return FunctionLib::ajaxRespond(true, 'ok',$tagData);
            }else{
                return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
            }
        }
        return FunctionLib::ajaxRespond(false, 'Lỗi');
    }

    public function ajaxSaveTag(SaveTagsRequest $request){
       try {
          $tagData =  Apiato::call('Tags@SaveTagByIdAction',[$request->id,$request->data]);
          return FunctionLib::ajaxRespond(true, 'Thành công',$tagData);
       } catch (\Throwable $th) {
          return FunctionLib::ajaxRespond(false, 'Lỗi');
       }
    }
    public function ajaxFilterDataByID(SaveTagsRequest $request){
      try {
          $dataFilter = Apiato::call('Tags@TagFilter\GetFiltersByIdAction',[$request->id]);
          $data=[];
          foreach($dataFilter as $key => $item){
              $data[$key] = $item->filter_id;
          }
          return FunctionLib::ajaxRespond(true, 'Hoàn thành', $data);
      } catch (\Exception $e) {
          return FunctionLib::ajaxRespond(false, 'Xảy ra lỗi');
      }
    }

    public function ajaxDeleteTag(EditTagsRequest $request){
        if(isset($request->id)){
            $tagData =  Apiato::call('Tags@DeleteTagByIdAction',[$request->id]);
            if(!empty($tagData)){
                return FunctionLib::ajaxRespond(true, 'Thành công',$tagData);
            }else{
                return FunctionLib::ajaxRespond(false, 'Không tìm thấy tag');
            }
        }
        return FunctionLib::ajaxRespond(false, 'Lỗi');
    }
}
