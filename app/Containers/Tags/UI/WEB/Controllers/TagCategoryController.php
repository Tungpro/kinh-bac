<?php

namespace App\Containers\Tags\UI\WEB\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\WebController;
use App\Containers\Tags\UI\WEB\Requests\EditTagsRequest;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Ship\Parents\Controllers\AdminController;

/**
 * Class ProductDescController
 *
 * @package App\Containers\Product\UI\WEB\Controllers
 */
class TagCategoryController extends AdminController
{
  use ApiResTrait;

  public function ajaxCateDataByID(EditTagsRequest $request){
    $transporter = $request->toTransporter();
    if ($transporter->id > 0) {
     $cate = Apiato::call('Category@GetCategoryByIdAction', [Apiato::call('Tags@TagCategory\CateIdsDirectFromIdAction', [$transporter->id])]);
     $cateHis = [];
     foreach($cate as $key => $item){
      $cateHis[$key] = $item->category_id;
     }
     if(!empty($cate)){
          return FunctionLib::ajaxRespond(true, 'Hoàn thành', $cateHis);
      }
      return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }
  }

} // End class
