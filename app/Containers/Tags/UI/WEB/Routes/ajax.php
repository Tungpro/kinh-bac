<?php

use Illuminate\Support\Facades\Route;

Route::namespace('\App\Containers\Tags\UI\WEB\Controllers')->prefix('admin/ajax')->group(function () {
  Route::prefix('tags')->group(function () {
    Route::any('/controller/tagList', [
      'as' => 'admin.tags.listTags',
      'uses' => 'Controller@ajaxTagList',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::GET('/controller/tagByID/{id}',[
      'as' => 'admin.tags.controller.tagByID',
      'uses' => 'Controller@ajaxTagByID',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::GET('/controller/{id}/tagDelete',[
      'as' => 'admin.tags.controller.tagDelete',
      'uses' => 'Controller@ajaxDeleteTag',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::GET('/controller/{id}/getDataByID',[
      'as' => 'admin.tags.controller.getDataByID',
      'uses' => 'Controller@ajaxGetDataByID',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::GET('/controller/type',[
      'as' => 'admin.tags.controller.type',
      'uses' => 'Controller@ajaxGetType',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::GET('/controller/cate',[
      'as' => 'admin.tags.controller.cate',
      'uses' => 'Controller@ajaxGetCate',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::POST('/controller/{id}/tagSave',[
      'as' => 'admin.tags.controller.tagSave',
      'uses' => 'Controller@ajaxSaveTag',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::POST('/controller/imgUpload',[
      'as' => 'admin.tags.controller.imgUpload',
      'uses' => 'Controller@ajaxImgUpload',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::get('/{object_id}/controller/getTagByIdPrd/{type}/{cate}',[
      'as' => 'admin.tags.controller.getTagByIdPrd',
      'uses' => 'Controller@ajaxGetTagByIdPrd',
       'middleware' => [
        'auth:admin',
      ]
    ]);

    Route::post('/{object_id}/controller/getSearchTagByIdPrd/{type}/{cate}',[
      'as' => 'admin.tags.controller.getSearchTagByIdPrd',
      'uses' => 'Controller@ajaxGetSearchTagByIdPrd',
       'middleware' => [
        'auth:admin',
      ]
    ]);

    Route::get('/{id}/controller/filterData', [
      'as' => 'admin.tag.controller.filterData',
      'uses' => 'Controller@ajaxFilterDataByID',
      'middleware' => [
          'auth:admin',
      ],
    ]);
    Route::get('/{id}/controller/cateData', [
      'as' => 'admin.tag.controller.cateData',
      'uses' => 'TagCategoryController@ajaxCateDataByID',
      'middleware' => [
          'auth:admin',
      ],
    ]);
  });
});
