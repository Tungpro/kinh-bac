<?php

namespace App\Containers\Tags\UI\API\Controllers;

use App\Containers\Tags\UI\API\Requests\CreateTagsRequest;
use App\Containers\Tags\UI\API\Requests\DeleteTagsRequest;
use App\Containers\Tags\UI\API\Requests\GetAllTagsRequest;
use App\Containers\Tags\UI\API\Requests\FindTagsByIdRequest;
use App\Containers\Tags\UI\API\Requests\UpdateTagsRequest;
use App\Containers\Tags\UI\API\Transformers\TagsTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Tags\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateTagsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTags(CreateTagsRequest $request)
    {
        $tags = Apiato::call('Tags@CreateTagsAction', [$request]);

        return $this->created($this->transform($tags, TagsTransformer::class));
    }

    /**
     * @param FindTagsByIdRequest $request
     * @return array
     */
    public function findTagsById(FindTagsByIdRequest $request)
    {
        $tags = Apiato::call('Tags@FindTagsByIdAction', [$request]);

        return $this->transform($tags, TagsTransformer::class);
    }

    /**
     * @param GetAllTagsRequest $request
     * @return array
     */
    public function getAllTags(GetAllTagsRequest $request)
    {
        $tags = Apiato::call('Tags@GetAllTagsAction', [$request]);

        return $this->transform($tags, TagsTransformer::class);
    }

    /**
     * @param UpdateTagsRequest $request
     * @return array
     */
    public function updateTags(UpdateTagsRequest $request)
    {
        $tags = Apiato::call('Tags@UpdateTagsAction', [$request]);

        return $this->transform($tags, TagsTransformer::class);
    }

    /**
     * @param DeleteTagsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTags(DeleteTagsRequest $request)
    {
        Apiato::call('Tags@DeleteTagsAction', [$request]);

        return $this->noContent();
    }
}
