<?php

/**
 * @apiGroup           Tags
 * @apiName            findTagsById
 *
 * @api                {GET} /v1/tags/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('tags/{id}', [
    'as' => 'api_tags_find_tags_by_id',
    'uses'  => 'Controller@findTagsById',
    'middleware' => [
      'auth:api',
    ],
]);
