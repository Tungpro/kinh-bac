<?php

namespace App\Containers\Tags\UI\API\Transformers;

use App\Containers\Tags\Models\Tags;
use App\Ship\Parents\Transformers\Transformer;

class TagsTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected array $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected array $availableIncludes = [

    ];

    /**
     * @param Tags $entity
     *
     * @return array
     */
    public function transform(Tags $entity)
    {
        $response = [
            'object' => 'Tags',
            'id' => $entity->getHashedKey(),
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
