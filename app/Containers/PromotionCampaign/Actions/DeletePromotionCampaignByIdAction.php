<?php

namespace App\Containers\PromotionCampaign\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeletePromotionCampaignByIdAction extends Action
{
    public function run($id)
    {
      $this->clearCache();

      return Apiato::call('PromotionCampaign@DeletePromotionCampaignByIdTask', [$id]);
    }
}
