<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-14 17:30:31
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-27 16:36:19
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Actions;

use App\Ship\Parents\Actions\Action;
use App\Containers\PromotionCampaign\Models\PromotionCampaign;
use App\Containers\PromotionCampaign\Actions\SubActions\CalcProductPriceByCampaignSubAction;

class CalcProductPriceByCampaignAction extends Action
{
    protected $percent, $priceParity;
    protected $campaign, $forceApply = false;

    public function run(?PromotionCampaign $campaign, &$products): void
    {
        // dd($campaign);
        app(CalcProductPriceByCampaignSubAction::class)->run($campaign, $products);
    }

    public function forceApply(): self
    {
        $this->forceApply = true;
        return $this;
    }
}
