<?php

namespace App\Containers\PromotionCampaign\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class SavePromotionCampaignByIdAction extends Action
{
    public function run($id,$data,$pickProduct,$fileListImg,$campaignCategory,$timeline)
    {
      if(empty($data['primary_category_id']) && isset($campaignCategory) &&  count($campaignCategory) > 0){
          $data['primary_category_id']  =  $campaignCategory[0];
      }
      $promotionCampaign =  Apiato::call('PromotionCampaign@SavePromotionCampaignTask', [$id,$data]);
      if($id!=='undefined'){
        Apiato::call('PromotionCampaign@PromotionCampaignDes\SavePromotionCampaignDesTask', [$id,$data['all_desc']]);
      }else{
        Apiato::call('PromotionCampaign@PromotionCampaignDes\SavePromotionCampaignDesTask', [$promotionCampaign->id,$data['all_desc']]);
      }
      if(!empty($timeline)){
        $this->call('PromotionCampaign@PromotionCampaignTimeline\SavePromotionCampaignTimelineTask', [$timeline, $id!=='undefined' ? $id : $promotionCampaign->id]);
      }
      if(!empty($pickProduct)){
        $this->call('PromotionCampaign@PromotionCampaignProduct\SavePromotionCampaignProductTask', [$pickProduct, $id!=='undefined' ? $id : $promotionCampaign->id]);
      }
      if(!empty($fileListImg)){
        $this->call('PromotionCampaign@PromotionCampaignImage\SavePromotionCampaignBannerTask', [$fileListImg, $id!=='undefined' ? $id : $promotionCampaign->id]);
      }
      if (!empty($campaignCategory)) {
        $campaign_category = [];
        foreach($campaignCategory as $key => $item){
            $campaign_category[$key]['promotion_campaign_id'] =  $id!=='undefined' ? $id : $promotionCampaign->id;
            $campaign_category[$key]['category_id'] = $item;
        }
        $this->call('PromotionCampaign@PromotionCampaignCategory\SavePromotionCampaignCategoryTask', [$campaign_category, $promotionCampaign]);
      }
      $this->clearCache();
      return $promotionCampaign;

    }
}
