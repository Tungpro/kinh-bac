<?php

namespace App\Containers\PromotionCampaign\Actions\TimelineCampaign;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Containers\PromotionCampaign\Tasks\PromotionCampaignTimeline\TimelineCampaignFromIdTask;
/**
 * Class CateIdsDirectFromPrdIdAction.
 *
 */
class GetPromotionCampaignTimelineAction extends Action
{

    /**
     * @return mixed
     */
    public function run($id)
    {
        // $data = Apiato::call('promotionCampaign@PromotionCampaignTimeline\TimelineCampaignFromIdTask', [
        //     $id
        // ]);
        $data = app(TimelineCampaignFromIdTask::class)->run($id);
        return $data;
    }
}
