<?php

namespace App\Containers\PromotionCampaign\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindPromotionCampaignByIdAction extends Action
{
    public function run(int $id)
    {
        $PromotionCampaign = Apiato::call('PromotionCampaign@FindPromotionCampaignByIdTask', [$id]);

        return $PromotionCampaign;
    }
}
