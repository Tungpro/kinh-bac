<?php

namespace App\Containers\PromotionCampaign\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetPromotionCampaignPrdDataAction extends Action
{
    public function run($request)
    {   $data=[];
        $PromotionCampaign = Apiato::call('PromotionCampaign@PromotionCampaignProduct\GetPromotionCampaignPrdTask', [$request]);
        foreach ($PromotionCampaign as $key => $task){
            $categories =  Apiato::call('Category@GetCategoryByIdAction', [Apiato::call('Product@CateIdsDirectFromPrdIdAction', [$task['id']])])->toArray();
            $PromotionCampaign[$key]['categories'][0]['category_id'] = @$categories[0]->category_id;
            $PromotionCampaign[$key]['categories'][0]['name'] = @$categories[0]->name;
            $PromotionCampaign[$key]['timeline'] = Apiato::call('PromotionCampaign@PromotionCampaignItemTimeline\GetPromotionCampaignItemTimelineByIDTask', [$task['id_item']]);
            if(isset($request->cate_id) && !empty($request->cate_id)){
                foreach ($PromotionCampaign[$key]['categories'] as $k => $v){
                    # code..
                    if(@$v['category_id'] == $request->cate_id){
                        $data[$key] = $PromotionCampaign[$key];
                    }
                }
            }
        }
        if(isset($request->cate_id) && !empty($request->cate_id)) return $data;
        return $PromotionCampaign;
    }
}
