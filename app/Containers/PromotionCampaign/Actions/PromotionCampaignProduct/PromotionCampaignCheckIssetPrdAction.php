<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-14 17:30:31
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-27 16:36:19
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Actions\PromotionCampaignProduct;

use App\Ship\Parents\Actions\Action;
use App\Containers\PromotionCampaign\Tasks\PromotionCampaignProduct\PromotionCampaignCheckIssetPrdTask;
class PromotionCampaignCheckIssetPrdAction extends Action
{
    public function run(array $pickProduct, $id)
    {
      return app(PromotionCampaignCheckIssetPrdTask::class)->run($pickProduct, $id);
    }

}
