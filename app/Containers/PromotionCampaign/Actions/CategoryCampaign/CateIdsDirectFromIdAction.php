<?php

namespace App\Containers\PromotionCampaign\Actions\CategoryCampaign;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class CateIdsDirectFromPrdIdAction.
 *
 */
class CateIdsDirectFromIdAction extends Action
{

    /**
     * @return mixed
     */
    public function run($id)
    {
        $data = Apiato::call('promotionCampaign@PromotionCampaignCategory\CateIdsDirectFromIdTask', [
            $id
        ]);
        return $data;
    }
}
