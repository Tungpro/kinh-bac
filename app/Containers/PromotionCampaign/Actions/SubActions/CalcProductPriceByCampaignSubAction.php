<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-14 17:30:31
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-27 16:39:59
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Actions\SubActions;

use Apiato\Core\Abstracts\Actions\SubAction;
use App\Containers\PromotionCampaign\Enums\PromotionCampaignPriceType;
use App\Containers\PromotionCampaign\Models\PromotionCampaign;
use App\Containers\PromotionCampaign\Models\PromotionCampaignProduct;
use App\Containers\PromotionCampaign\Tasks\Calculators\CalcPriceNormalFromCampaignTask;
use App\Containers\PromotionCampaign\Tasks\Calculators\CalcPriceParityFromCampaignTask;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class CalcProductPriceByCampaignSubAction extends SubAction
{
    protected $percent, $priceParity;
    protected $campaign;

    public function run(?PromotionCampaign $campaign, &$products): void
    {
        if (empty($campaign)) {
            return;
        }

        $this->campaign = &$campaign;
        $this->percent = $campaign->promote_percent;
        $this->priceParity = $campaign->parity_price;
        // dd($products);
        if ($products instanceof iterable || $products instanceof LengthAwarePaginator) {
            foreach ($products as &$product) {
                if ($product instanceof PromotionCampaignProduct) {
                    $this->calc($campaign, $product->product);
                } else {
                    // Để tạm đây, khi nào có logic thì bổ sung sau
                    $this->calc($campaign, $product);
                }
            }
        } else {
            // if($campaign->formating == PromotionCampaignType::FLASH) {

            // }

            $this->calc($campaign, $products);
        }
    }

    private function calc($campaign, &$product)
    {
        if ($campaign->formatprice == PromotionCampaignPriceType::NORMAL) {
            app(CalcPriceNormalFromCampaignTask::class)->run($product, $this->percent);
        } else {
            app(CalcPriceParityFromCampaignTask::class)->run($product, $this->priceParity);
        }
    }
}
