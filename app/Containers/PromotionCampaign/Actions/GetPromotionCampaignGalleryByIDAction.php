<?php

namespace App\Containers\PromotionCampaign\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class GetProductGalleryAction.
 *
 */
class GetPromotionCampaignGalleryByIDAction extends Action
{

    /**
     * @return mixed
     */
    public function run(int $campaign_id, $json = false)
    {
        return $this->call('PromotionCampaign@PromotionCampaignImage\GetPromotionCampaignGalleryByIDCollectionTask', [
            $campaign_id,
            $json
        ]);
    }
}
