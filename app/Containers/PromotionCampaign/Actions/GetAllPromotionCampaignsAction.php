<?php

namespace App\Containers\PromotionCampaign\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllPromotionCampaignsAction extends Action
{
    public function run($filter , $perPage = 11,$skipPagination = true, $external_data = [])
    {
        if(empty($external_data))
            $PromotionCampaigns = Apiato::call('PromotionCampaign@GetAllPromotionCampaignsTask', [$filter, $perPage, $skipPagination]);
        else
            $PromotionCampaigns = Apiato::call('PromotionCampaign@GetAllPromotionCampaignsTask', [$filter, $perPage, $skipPagination, $external_data]);

        return $PromotionCampaigns;
    }
}
