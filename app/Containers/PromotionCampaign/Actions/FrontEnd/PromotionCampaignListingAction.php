<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-30 17:09:00
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-05 14:52:58
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\PromotionCampaign\Tasks\FrontEnd\PromotionCampaignListingTask;
use App\Ship\Parents\Actions\Action;

class PromotionCampaignListingAction extends Action
{
    public function run(?array $filters = [],?array $with, ?Language $currentLang, $skipPagination = false, $limit = 10,int $currentPage = 1)
    {
        return $this->remember(function() use($filters,$with,$currentLang,$skipPagination,$limit,$currentPage) {
            $task = app(PromotionCampaignListingTask::class)->with($with)->currentLang($currentLang);
        
            $task = $this->detectTaskCriteriaFunc($filters,$task);

            if($skipPagination) {
                $task->skipPagin();
            }

            return $task->run($limit);
        },null,[],0,$this->skipCache);
    }
}
