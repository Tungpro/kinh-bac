<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-30 17:09:00
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-20 11:47:35
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\PromotionCampaign\Tasks\FrontEnd\GetRunningCampaignTask;
use App\Ship\Parents\Actions\Action;

class GetRunningFlashSaleCampaignAction extends Action
{
    public function run(?array $filters = [], ?array $with, ?Language $currentLang)
    {
        $task = app(GetRunningCampaignTask::class)
            ->isActive()
            ->onlyFlashSale()
            ->startMostRecently()
            ->externalWith($this->externalWith)
            ->with($with)
            ->currentLang($currentLang);

        $task = $this->detectTaskCriteriaFunc($filters, $task);

        return $task->run();
    }

    public function descCategory(?Language $currentLang): self
    {
        $languageId = $currentLang ? $currentLang->language_id : 1;
        $this->externalWith = array_merge($this->externalWith, [
            'categories.desc' => function ($query) use ($languageId) {
                $query->select('category_id', 'name', 'slug', 'meta_title');
                $query->whereHas('language', function ($q) use ($languageId) {
                    $q->where('language_id', $languageId);
                });
            }
        ]);
        return $this;
    }
}
