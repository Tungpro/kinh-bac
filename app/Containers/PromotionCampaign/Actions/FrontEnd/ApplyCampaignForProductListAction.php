<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-30 17:09:00
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-01 16:30:56
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Actions\FrontEnd;

use App\Containers\PromotionCampaign\Actions\FrontEnd\SubActions\GetMostRecentlyCampaignOnProductListSubAction;
use App\Containers\PromotionCampaign\Actions\SubActions\CalcProductPriceByCampaignSubAction;
use App\Ship\Parents\Actions\Action;

class ApplyCampaignForProductListAction extends Action
{
    public $getMostRecentlyCampaignOnProductListSubAction, $calcProductPriceByCampaignSubAction;

    public function __construct(
        GetMostRecentlyCampaignOnProductListSubAction $getMostRecentlyCampaignOnProductListSubAction,
        CalcProductPriceByCampaignSubAction $calcProductPriceByCampaignSubAction
    ) {
        $this->getMostRecentlyCampaignOnProductListSubAction = $getMostRecentlyCampaignOnProductListSubAction;
        $this->calcProductPriceByCampaignSubAction = $calcProductPriceByCampaignSubAction;
        parent::__construct();
    }

    public function run(&$products, $campaigns = null)
    {
        $campaigns = !empty($campaigns) ? $campaigns : $this->getMostRecentlyCampaignOnProductListSubAction->run($products->pluck('id')->toArray());

        foreach ($products as $product) {
            if (isset($campaigns[$product->id])) {
                $this->calcProductPriceByCampaignSubAction->run($campaigns[$product->id], $product);
            }
        }
    }
}
