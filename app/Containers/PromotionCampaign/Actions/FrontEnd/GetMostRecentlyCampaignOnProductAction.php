<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-30 17:09:00
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-13 17:09:56
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Actions\FrontEnd;

use App\Containers\PromotionCampaign\Tasks\FrontEnd\GetMostRencentlyCampaignOnProductTask;
use App\Ship\Parents\Actions\Action;

class GetMostRecentlyCampaignOnProductAction extends Action
{
    public function run(?array $productIds = [])
    {
        $campaign = app(GetMostRencentlyCampaignOnProductTask::class)
            ->productIds($productIds)
            ->run();

        return $campaign;
    }
}
