<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-30 17:09:00
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-28 16:52:56
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Actions\FrontEnd\SubActions;

use App\Containers\PromotionCampaign\Enums\PromotionCampaignStatus;
use App\Containers\PromotionCampaign\Tasks\FrontEnd\GetMostRencentlyCampaignOnProductListTask;
use App\Ship\Parents\Actions\SubAction;
use Illuminate\Support\Facades\DB;

class GetMostRecentlyCampaignOnProductListSubAction extends SubAction
{
    protected $task;

    public function __construct()
    {
        $this->task = app(GetMostRencentlyCampaignOnProductListTask::class);
        parent::__construct();
    }

    public function run(?array $productIds = [])
    {
        // $this->externalWith = array_merge($this->externalWith, [
        //     'promotion_campaign_product' => function ($q) use ($productIds) {
        //         $q->whereIn('product_id', $productIds);
        //         // $q->limit(1);
        //     }
        // ]);

        $campaign = $this->task
            ->productIds($productIds)
            // ->externalWith($this->externalWith)
            // ->startMostRecently()
            // ->isActive()
            // ->with(['timeline', 'promotion_campaign_product'])
            // ->groupBy('product_id')
            ->run();

        return $campaign;
    }

    public function onlyNormal(): self
    {
        $this->task->onlyNormal();
        return $this;
    }
}
