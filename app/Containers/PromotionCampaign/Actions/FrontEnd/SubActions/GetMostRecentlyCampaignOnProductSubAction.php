<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-30 17:09:00
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-27 17:22:17
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Actions\FrontEnd\SubActions;

use App\Containers\PromotionCampaign\Tasks\FrontEnd\GetMostRencentlyCampaignOnProductTask;
use App\Ship\Parents\Actions\SubAction;

class GetMostRecentlyCampaignOnProductSubAction extends SubAction
{
    protected $task;

    public function __construct()
    {
        $this->task = app(GetMostRencentlyCampaignOnProductTask::class);
        parent::__construct();
    }

    public function run(?array $productIds = [])
    {
        $campaign = $this->task
            ->productIds($productIds)
            ->startMostRecently()
            ->isActive()
            ->with(['timeline'])
            ->run();

        return $campaign;
    }

    public function onlyNormal(): self
    {
        $this->task->onlyNormal();
        return $this;
    }
}
