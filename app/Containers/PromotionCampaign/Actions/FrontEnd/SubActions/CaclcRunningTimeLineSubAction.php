<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-30 17:09:00
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-29 15:47:52
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Actions\FrontEnd\SubActions;

use App\Containers\PromotionCampaign\Tasks\Calculators\CalcRunningTimeLineTask;
use App\Ship\Parents\Actions\Action;

class CaclcRunningTimeLineSubAction extends Action
{
    protected $calcRunningTimeLineTask;

    public function __construct(CalcRunningTimeLineTask $calcRunningTimeLineTask)
    {
        $this->calcRunningTimeLineTask = $calcRunningTimeLineTask;
        parent::__construct();
    }
    /**
     * Truyền con trỏ vào để có thể update ngược dữ liệu timeline trở lại campaign
     */
    public function run(&$currentCampaign)
    {
        return $this->calcRunningTimeLineTask->run($currentCampaign);
    }
}
