<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-30 17:09:00
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-20 11:46:41
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Actions\FrontEnd;

use App\Containers\PromotionCampaign\Tasks\FrontEnd\GetProductByCampaignIdTask;
use App\Ship\Parents\Actions\Action;

class GetProductByCampaignIdAction extends Action
{
    protected $checkOnlyOne = false, $productId = null;

    public function run(array $filters, array $with = [], array $column = ['*'], int $limit = 50, int $currentPage = 1)
    {
        $products = app(GetProductByCampaignIdTask::class)
            ->timelineId($filters['timelineId'])
            ->campaignId($filters['campaignId'])
            ->categoryId($filters['categoryId']);

        if($this->checkOnlyOne) {
            $products->justCheckOnlyOne($this->productId);
        }

        return $products->run($limit);
    }

    /**
     * Chỉ check 1 id sản phẩm duy nhất
     * xem có tồn tại với campaign + timeline hay không
     */
    public function justCheckOnlyOne(?int $productId): self
    {
        $this->checkOnlyOne = true;
        $this->productId = $productId;
        return $this;
    }
}
