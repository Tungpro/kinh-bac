<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-30 17:09:00
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-27 17:22:12
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Actions\FrontEnd;

use App\Containers\PromotionCampaign\Actions\FrontEnd\SubActions\GetMostRecentlyCampaignOnProductListSubAction;
use App\Ship\Parents\Actions\Action;

class GetMostRecentlyCampaignOnProductListAction extends Action
{
    public function run(?array $productIds = [])
    {
        $campaign = app(GetMostRecentlyCampaignOnProductListSubAction::class)
            ->run($productIds);

        return $campaign;
    }
}
