<?php

namespace App\Containers\PromotionCampaign\Tasks;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetAllPromotionCampaignsTask extends Task
{

    protected $repository;

    public function __construct(PromotionCampaignRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($filters, $perPage, $skipPagination = false, $external_data = ['with_relationship' => ['all_desc']], $orderBy = 'desc')
    {
        $this->repository->with(array_merge($external_data['with_relationship'],['desc','promotion_campaign_product']));
        if(isset($filters['name']) && $filters['name']!= ''){
            $this->repository->whereHas('desc' , function (Builder $query) use($filters){
                $query->where('name', 'like', '%' . $filters['name'] . '%');
            });
        }
        if(isset($filters['id']) && $filters['id']!= ''){
            $this->repository->where('id', $filters['id']);
        }
        $this->repository->orderBy('id', $orderBy);
        return $skipPagination ?  $this->repository->get()  : $this->repository->paginate($perPage);
    }
}
