<?php

namespace App\Containers\PromotionCampaign\Tasks\PromotionCampaignTimeline;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignTimelineRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class GetProductGalleryTask.
 */
class SavePromotionCampaignTimelineTask extends Task
{

    protected $repository;

    public function __construct(PromotionCampaignTimelineRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($timeline_data,$campaign_id)
    {
        if(!empty($timeline_data)){
            foreach($timeline_data as $key => $items){
                $data=[
                    'value' => $items['value'],
                    'promotion_campaign_id' => $campaign_id,
                ];
                if(isset($items['id'])){
                    $checkisset = $this->repository->where('promotion_campaign_id',$campaign_id)->where('value',$items['value'])->where('id','!=',$items['id'])->count();
                    if($checkisset==0 && !empty($items['value'])){
                        $this->repository->where('id',$items['id'])->update($data);
                        $keyIsset[]= $items['id'];
                    }
                }
                else{
                    $checkisset = $this->repository->where('promotion_campaign_id',$campaign_id)->where('value',$items['value'])->count();
                    if($checkisset==0  && !empty($items['value'])){
                        $timeline = $this->repository->insertGetId($data);
                        $keyIsset[]= $timeline;
                    }
                }
            }
            if(isset($keyIsset)) {
                $this->repository->where('promotion_campaign_id',$campaign_id)->whereNotIn('id',$keyIsset)->delete();
            }
        }
        // return $collection->id;
    }
}
