<?php

namespace App\Containers\PromotionCampaign\Tasks\PromotionCampaignTimeline;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignTimelineRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class GetProductGalleryTask.
 */
class TimelineCampaignFromIdTask extends Task
{

    protected $repository;

    public function __construct(PromotionCampaignTimelineRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run(int $id)
    {
       return $this->repository->where('promotion_campaign_id', $id)->get();
    }
}
