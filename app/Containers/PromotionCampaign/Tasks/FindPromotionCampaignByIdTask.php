<?php

namespace App\Containers\PromotionCampaign\Tasks;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;
use Apiato\Core\Foundation\Facades\Apiato;

class FindPromotionCampaignByIdTask extends Task
{

    protected $repository;

    public function __construct(PromotionCampaignRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $id)
    {
        $defaultLanguage = Apiato::call('Localization@GetDefaultLanguageTask');
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;
        return  $this->repository->where('id',$id)->with(['promotion_campaign_product' => function ($query) use($language_id){
            $query->with(['desc_product' => function ($queryChild) use($language_id){
                $queryChild->where('language_id', $language_id)->first();
            }]);
        },'all_desc'])->first();
    }
}
