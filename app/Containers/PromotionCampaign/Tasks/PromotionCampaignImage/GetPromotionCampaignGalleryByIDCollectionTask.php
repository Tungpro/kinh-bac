<?php

namespace App\Containers\PromotionCampaign\Tasks\PromotionCampaignImage;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignImageRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class GetProductGalleryTask.
 */
class GetPromotionCampaignGalleryByIDCollectionTask extends Task
{

    protected $repository;

    public function __construct(PromotionCampaignImageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($campaign_id, $json = false)
    {
        $images = $this->repository->getModel()->where('id', $campaign_id)->first();
        // $images = $this->repository->getModel()->where('type', $type)->where('object_id', $campaign_id)->orderByRaw('sort desc,created desc')->get();
        $data = [];
        if(!empty($images)) {
                $tmp = $images->toArray();
                $tmp['img'] = $images->image;
                $tmp['image_sm'] = $images->getImageUrl('small');
                $tmp['image_md'] = $images->getImageUrl('medium');
                $tmp['image'] = $images->getImageUrl('social');
                $tmp['image_org'] = $images->getImageUrl();
                array_push($data, $tmp);
            
        }
        return $json ? json_encode($data) : $data;
    }
}
