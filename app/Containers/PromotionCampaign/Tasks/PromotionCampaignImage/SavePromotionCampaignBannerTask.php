<?php

namespace App\Containers\PromotionCampaign\Tasks\PromotionCampaignImage;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignImageRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class GetProductGalleryTask.
 */
class SavePromotionCampaignBannerTask extends Task
{

    protected $repository;

    public function __construct(PromotionCampaignImageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($campaign_data,$campaign_id)
    {
        // if(!empty($campaign_id))
        //     $this->repository->where('object_id',$campaign_id)->delete();
        // dd($campaign_data);
        if(!empty($campaign_data)){
            foreach($campaign_data as $key => $items){
                $keyIsset[]= $items['id'];
                $data=[
                    'promotion_campaign_id' => $campaign_id,
                    'sort' => $items['sort'] ?? 0,
                    'link' => $items['link'] ?? ''
                ];
                $this->repository->where('image',$items['name'])->update($data);
            }
            $this->repository->where('promotion_campaign_id',$campaign_id)->whereNotIn('id',$keyIsset)->delete();
        }
        // return $collection->id;
    }
}
