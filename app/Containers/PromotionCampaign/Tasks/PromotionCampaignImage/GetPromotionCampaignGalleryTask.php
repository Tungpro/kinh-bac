<?php

namespace App\Containers\PromotionCampaign\Tasks\PromotionCampaignImage;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignImageRepository;
use App\Ship\Parents\Tasks\Task;


class GetPromotionCampaignGalleryTask extends Task
{

    protected $repository;

    public function __construct(PromotionCampaignImageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($campaign_id, $json = false)
    {
        $images = $this->repository->getModel()->where('promotion_campaign_id', $campaign_id)->orderByRaw('id asc')->get();
        // $images = $this->repository->getModel()->where('type', $type)->where('object_id', $campaign_id)->orderByRaw('sort desc,created desc')->get();
        $data = [];
        if(!$images->isEmpty()) {
            foreach ($images as $image) {
                $tmp = $image->toArray();
                $tmp['img'] = $image->image;
                $tmp['image_sm'] = $image->getImageUrl('small');
                $tmp['image_md'] = $image->getImageUrl('medium');
                $tmp['image'] = $image->getImageUrl('social');
                $tmp['image_org'] = $image->getImageUrl();
                array_push($data, $tmp);
            }
        }
        return $json ? json_encode($data) : $data;
    }
}
