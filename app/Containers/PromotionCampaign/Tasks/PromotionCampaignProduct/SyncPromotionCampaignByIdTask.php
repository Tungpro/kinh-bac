<?php

namespace App\Containers\PromotionCampaign\Tasks\PromotionCampaignProduct;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignProductRepository;
use App\Ship\Parents\Tasks\Task;

class SyncPromotionCampaignByIdTask extends Task
{

    protected $repository;

    public function __construct(PromotionCampaignProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($syncPrd, $id){
      if(isset($id)){
        return $this->repository->where('promotion_campaign_id','!=', $id)->whereIn('product_id', $syncPrd)->delete();
      }else{
        return $this->repository->whereIn('product_id', $syncPrd)->delete();
      }
    }

}
