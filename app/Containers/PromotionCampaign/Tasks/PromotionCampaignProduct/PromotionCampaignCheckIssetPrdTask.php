<?php

namespace App\Containers\PromotionCampaign\Tasks\PromotionCampaignProduct;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignProductRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ByPrdIDCriteria;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Containers\Localization\Models\Language;

class PromotionCampaignCheckIssetPrdTask extends Task
{

    protected $repository;

    public function __construct(PromotionCampaignProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($pickPrd, $id){
      $checkIssetArray= [];
      foreach ($pickPrd as $key => $item) {
          if(!isset($item['id_item']) || empty($item['id_item'])){
            if(isset($id) && $id !='NaN')
              $checkIsset = $this->repository->where('product_id', $item['id'])->where('promotion_campaign_id','!=',$id)->count();
            else
              $checkIsset = $this->repository->where('product_id', $item['id'])->count();

            if($checkIsset > 0)
              $checkIssetArray[$key] = $item['id'];
          }
      }
      return $checkIssetArray;
    }

}
