<?php

namespace App\Containers\PromotionCampaign\Tasks\PromotionCampaignProduct;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignProductRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ByPrdIDCriteria;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Containers\Localization\Models\Language;

class GetPromotionCampaignPrdTask extends Task
{

    protected $repository;

    public function __construct(PromotionCampaignProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($request){
        if(!empty($request->prd_id)){
          $this->repository->pushCriteria(new ByPrdIDCriteria($request->prd_id));
        }
        if( !empty($request->id)){
          $this->repository->pushCriteria(new ThisEqualThatCriteria('promotion_campaign_id', $request->id));
        }
        $this->repository->pushCriteria(new WithCriteria(['product'=> function($query) use($request)
        {
          $query->select('id', 'price', 'type', 'status', 'image')->with(['all_desc' => function($q) use($request) {
              $q->activeLang(1);
              $q->where('name',  'LIKE', '%'.$request->inputTitle.'%');
        }]);
        },'timeline']));
        if($request->selectTime !='undefined' && !empty($request->selectTime)){
          $this->repository->whereHas('timeline', function ($query) use ($request) {
            $query->where('timeline_id', $request->selectTime);
          });
        }
        $result = $this->repository->orderBy('created_at', 'ASC')->get();
        $data=[];
        foreach ($result as $key => $item){
            if(isset($item->product->all_desc[0]->name)){
              $data[$key]['id'] = $item->product_id;
              $data[$key]['id_item'] = $item->id;
              // $data[$key]['description'] = @$item->product->all_desc[0]->description;
              $data[$key]['short_description'] = @$item->product->all_desc[0]->short_description;
              $data[$key]['image'] = @$item->product->image;
              $data[$key]['imageUrl'] = @$item->product->image_url;
              $data[$key]['name'] = @$item->product->all_desc[0]->name;
              // $data[$key]['price'] = @$item->product->price;
              $data[$key]['sort'] = $item->sort;
              // $data[$key]['timeline'] = $item->timeline;
            }else{
              unset($data[$key]);
            }
        }
        return $data;
    }

}
