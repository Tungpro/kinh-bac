<?php

namespace App\Containers\PromotionCampaign\Tasks\PromotionCampaignProduct;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignProductRepository;
use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignTimelineRepository;
use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignItemTimelineRepository;
use App\Ship\Parents\Tasks\Task;
use App\Containers\PromotionCampaign\Models\PromotionCampaignProduct;

/**
 * Class GetProductGalleryTask.
 */
class SavePromotionCampaignProductTask extends Task
{

    protected $repository;

    public function __construct(PromotionCampaignProductRepository $repository, PromotionCampaignTimelineRepository $repositoryTimeline, PromotionCampaignItemTimelineRepository $repositoryItemTimeline)
    {
        $this->repository = $repository;
        $this->repositoryTimeline = $repositoryTimeline;
        $this->repositoryItemTimeline = $repositoryItemTimeline;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($campaign_data,$campaign_id)
    {
        if(!empty($campaign_data) && !empty($campaign_id)){
            foreach($campaign_data as $key => $items){
                foreach($items['timeline']  as $k => $itemsTimeline){
                    if(!is_numeric($itemsTimeline)){
                        $timelineDataCheckIsset = $this->repositoryTimeline->where('value',$itemsTimeline)->where('promotion_campaign_id',$campaign_id)->first();
                        if(isset($timelineDataCheckIsset->id)){
                            $items['timeline'][$k] =  $timelineDataCheckIsset->id;
                        }else{
                            unset($items['timeline'][$k]);
                        }
                    }else{
                        $timelineDataCheckIsset = $this->repositoryTimeline->where('id',$itemsTimeline)->where('promotion_campaign_id',$campaign_id)->first();
                        if(isset($timelineDataCheckIsset->id)){
                            $items['timeline'][$k] =  $timelineDataCheckIsset->id;
                        }else{
                            unset($items['timeline'][$k]);
                        }
                    }
                }
               try {
                    $data=[
                        'promotion_campaign_id' => $campaign_id,
                        'product_id' => $items['id'],
                        // 'timeline' => json_encode($items['timeline'] ?? []),
                        'sort' => $items['sort'],
                    ];
                    $checkIssetPrd = $this->repository->where('product_id', $items['id'])->where('promotion_campaign_id',$campaign_id)->get('id')->first();

                    if(empty($checkIssetPrd))
                        $proData = $this->repository->create($data);
                    else{
                        $proData = $this->repository->update($data,$checkIssetPrd['id']);
                    }
                    $proData->timeline()->sync($items['timeline']);
                    $arr_pick_prd[] = $proData['id'];
               } catch (\Throwable $th) {
                    continue;
               }
            }
            $this->repository->where('promotion_campaign_id',$campaign_id)->whereNotIn('id',$arr_pick_prd)->delete();
        }
        // return $collection->id;
    }
}
