<?php

namespace App\Containers\PromotionCampaign\Tasks;
use Illuminate\Support\Str;
use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class SavePromotionCampaignTask extends Task
{

    protected $repository;

    public function __construct(PromotionCampaignRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id='', array $data)
    {
        try {
            if($id!='undefined'){
                unset($data['all_desc'],$data['promotion_campaign_product'],$data['created_at'],$data['updated_at'],$data['id'],$data['timeline']);
                $data['start_date'] = $data['start_date']/1000;
                $data['end_date'] = $data['end_date']/1000;
                return $this->repository->update($data, $id);
                // return $this->repository->getModel()->where('id',  $id)->update($data);
            }
            else{
                $promotionCampaign = $this->repository->getModel();
                $promotionCampaign->formating = $data['formating'];
                $promotionCampaign->formatprice = @$data['formatprice'];
                $promotionCampaign->promote_percent = $data['promote_percent'];
                $promotionCampaign->start_date = $data['start_date']/1000;
                $promotionCampaign->end_date = $data['end_date']/1000;
                $promotionCampaign->status = $data['status'];
                $promotionCampaign->image = @$data['image'];
                $promotionCampaign->image_mobile = @$data['image_mobile'];
                $promotionCampaign->properties = @$data['properties'];
                $promotionCampaign->save();
                return $promotionCampaign;
            }


        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
