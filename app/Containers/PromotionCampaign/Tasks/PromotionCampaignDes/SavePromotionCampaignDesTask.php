<?php

namespace App\Containers\PromotionCampaign\Tasks\PromotionCampaignDes;
use Illuminate\Support\Str;
use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignDescriptionRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Apiato\Core\Foundation\Facades\Apiato;

class SavePromotionCampaignDesTask extends Task
{

    protected $repository;

    public function __construct(PromotionCampaignDescriptionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id='', array $data)
    {
            if(!empty($data)){
              foreach ($data as $key => $item) {
                if(!empty($item) && isset($item['id'])){
                    unset($item['id']);
                    $this->repository->getModel()->where('promotion_campaign_id',  $id)->where('language_id',$item['language_id'])->update($item);
                }elseif(!empty($item) && !isset($item['id'])){
                  $item['promotion_campaign_id'] = $id;
                    $this->repository->getModel()->insert($item);
                }
              }
            }

    }
}
