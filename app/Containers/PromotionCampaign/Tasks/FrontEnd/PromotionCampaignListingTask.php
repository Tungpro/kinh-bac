<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-04 14:38:48
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-04 15:01:54
 * @ Description: Happy Coding!
 */


namespace App\Containers\PromotionCampaign\Tasks\FrontEnd;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignRepository;
use App\Containers\PromotionCampaign\Enums\PromotionCampaignStatus;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class PromotionCampaignListingTask extends Task
{
    protected $repository;

    public function __construct(PromotionCampaignRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $limit = 15)
    {
        $language_id = $this->currentLang ? $this->currentLang->language_id : 1;

        $this->repository->with(['desc' => function ($query) use ($language_id) {
            $query->select('id', 'promotion_campaign_id', 'name','short_description', 'meta_title');
            $query->whereHas('language', function ($q) use ($language_id) {
                $q->where('language_id', $language_id);
            });
        }]);

        return $this->skipPagin ? ($limit == 0 ? $this->repository->all() : $this->repository->limit($limit)) : $this->repository->paginate($limit);
    }

    public function isActive(bool $bool = true) :self {
        $this->repository->pushCriteria(new ThisOperationThatCriteria('start_date',time(),'<='));
        $this->repository->pushCriteria(new ThisOperationThatCriteria('end_date',time(),'>'));
        $this->repository->pushCriteria(new ThisEqualThatCriteria('status', $bool ? PromotionCampaignStatus::ACTIVE : PromotionCampaignStatus::DE_ACTIVE));
        return $this;
    }
}
