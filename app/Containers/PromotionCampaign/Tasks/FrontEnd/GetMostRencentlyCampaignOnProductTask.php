<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-04 14:38:48
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-27 17:16:19
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Tasks\FrontEnd;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignRepository;
use App\Containers\PromotionCampaign\Enums\PromotionCampaignStatus;
use App\Containers\PromotionCampaign\Enums\PromotionCampaignType;
use App\Containers\PromotionCampaign\Models\PromotionCampaign;
use App\Ship\Criterias\Eloquent\OrderByFieldsCriteria;
use App\Ship\Criterias\Eloquent\OrderByFieldValuesCriteria;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;

class GetMostRencentlyCampaignOnProductTask extends Task
{
    protected $repository, $productIds = null;

    public function __construct(PromotionCampaignRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(): ?PromotionCampaign
    {
        if (!empty($this->productIds)) {
            $this->repository->with([
                
            ]);

            $this->repository->whereHas('promotion_campaign_product',function($q) {
                $q->select(['product_id','promotion_campaign_id']);
                $q->whereIn('product_id', $this->productIds);
            });

            // DB::enableQueryLog();
            return $this->repository->first();
            // dd(DB::getQueryLog());
        }

        return null;
    }

    public function productIds(?array $productIds): self
    {
        $this->productIds = $productIds;
        return $this;
    }

    public function onlyFlashSale(): self
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('formating', PromotionCampaignType::FLASH));
        return $this;
    }

    public function onlyNormal(): self
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('formating', PromotionCampaignType::NORMAL));
        return $this;
    }

    public function startMostRecently(): self
    {
        // ưu tiên cho chiến dịch flashsale được áp dụng, kể cả các chiến dịch thường mới hơn
        $this->repository->pushCriteria(new OrderByFieldValuesCriteria('formating',[PromotionCampaignType::FLASH],'desc'));
        $this->repository->pushCriteria(new OrderByFieldsCriteria([
            ['start_date', 'desc'],
        ]));
        return $this;
    }

    public function isActive(bool $bool = true): self
    {
        $this->repository->pushCriteria(new ThisOperationThatCriteria('start_date', time(), '<='));
        $this->repository->pushCriteria(new ThisOperationThatCriteria('end_date', time(), '>'));

        // $this->repository->orderByRaw("FIELD(formating, '".PromotionCampaignType::FLASH."') DESC");

        $this->repository->pushCriteria(new ThisEqualThatCriteria('status', $bool ? PromotionCampaignStatus::ACTIVE : PromotionCampaignStatus::DE_ACTIVE));
        return $this;
    }
}
