<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-04 14:38:48
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-28 17:44:10
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Tasks\FrontEnd;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignRepository;
use App\Containers\PromotionCampaign\Enums\PromotionCampaignStatus;
use App\Containers\PromotionCampaign\Models\PromotionCampaign;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Facades\DB;

class GetMostRencentlyCampaignOnProductListTask extends Task
{
    protected $repository, $productIds = null;

    public function __construct(PromotionCampaignRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        if (!empty($this->productIds)) {
            $now = time();
            $selects = [
                'p.id as product_id',
                'pc.id',
                'pc.promote_percent',
                'pc.parity_price ',
                'pc.start_date ',
                'pc.end_date ',
                'pc.status ',
                'pc.primary_category_id',
                'pc.formating ',
                'pc.formatprice ',
                'pc.properties ',
                'pc.created_at ',
                'pc.updated_at ',
                'pc.deleted_at'
            ];

            $sql = sprintf("select
                    " . implode(',', $selects) . "
                from
                    products p
                inner join promotion_campaign_items pci on
                    p.id = pci.product_id
                inner join promotion_campaign pc on
                    pc.id = pci.promotion_campaign_id
                where
                    p.id in ( " . implode(',', $this->productIds) . ")
                    and pc.`end_date` > " . $now . "
                    and pc.`status` = " . PromotionCampaignStatus::ACTIVE . "
                    and pc.start_date = (
                        select
                            max(pc.start_date) as max_start_date
                        from
                            products p
                        inner join promotion_campaign_items pci on
                            p.id = pci.product_id
                        inner join promotion_campaign pc on
                            pc.id = pci.promotion_campaign_id
                        where
                            p.id in ( " . implode(',', $this->productIds) . ")
                            and pc.`start_date` <= " . $now . "
                            and pc.`end_date` > " . $now . "
                            and pc.`status` = " . PromotionCampaignStatus::ACTIVE . "
                    )
                group by
                    p.id
                order by
                    FIELD(formating, 'flash') desc,
                    start_date desc;");

            $result = collect(DB::select(DB::raw($sql)))->keyBy('product_id')->toArray();

            if (!empty($result)) {
                foreach ($result as &$item) {
                    $item = new PromotionCampaign((array)$item);
                }

                return $result;
            }
        }

        return null;
    }

    public function productIds(?array $productIds): self
    {
        $this->productIds = $productIds;
        return $this;
    }

    public function onlyFlashSale(): self
    {
        return $this;
    }

    public function onlyNormal(): self
    {
        return $this;
    }
}
