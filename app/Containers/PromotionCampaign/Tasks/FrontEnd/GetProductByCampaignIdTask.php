<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-04 14:38:48
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-20 11:52:43
 * @ Description: Happy Coding!
 */


namespace App\Containers\PromotionCampaign\Tasks\FrontEnd;

use App\Containers\Product\Enums\ProductStatus;
use App\Containers\Product\Enums\ProductVariantStatus;
use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignProductRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Facades\DB;

class GetProductByCampaignIdTask extends Task
{
    protected $repository;
    protected $timelineId = 0, $categoryId = 0, $checkOnlyOne = false, $productId = null;

    public function __construct(PromotionCampaignProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($limit = 10)
    {
        $language_id = $this->currentLang ? $this->currentLang->language_id : 1;
        $bonusWith = [];

        if ($this->timelineId > 0) {
            $bonusWith[] = 'timeline';
            $this->repository->whereHas('timeline', function ($q) {
                $q->where('id', $this->timelineId);
            });
        }

        if ($this->categoryId > 0) {
            // $bonusWith[] = 'product.categories';
            $this->repository->whereHas('product.categories', function ($q) {
                $q->where('category.category_id', $this->categoryId);
            });
        }

        $this->repository->whereHas('product', function ($q) {
            $q->where('status', ProductStatus::ACTIVE);
            if(!empty($this->productId)) {
                $q->where('id', $this->productId);
            }
        });

        $this->repository->whereHas('product.desc', function ($q) {
            $q->where('slug', '!=', '')->whereNotNull('slug');
        });

        $this->repository->with(array_merge($this->defaultWith($language_id), $bonusWith));

        // DB::enableQueryLog();
        return $this->checkOnlyOne ? $this->repository->exists() : $this->repository->paginate($limit);
        // dd(DB::getQueryLog());
    }

    private function defaultWith($language_id)
    {
        return $this->checkOnlyOne ? [
            'product'
        ] : [
            'product.desc' => function ($query) use ($language_id) {
                $query->select('id', 'product_id', 'name', 'slug', 'short_description', 'meta_title');
                $query->whereHas('language', function ($q) use ($language_id) {
                    $q->where('language_id', $language_id);
                });
            }, 'product.specialOffers.desc' => function ($query) use ($language_id) {
                $query->select('id', 'special_offer_id', 'name');
                $query->whereHas('language', function ($q) use ($language_id) {
                    $q->where('language_id', $language_id);
                });
            },
            'product.sumStockVariant' => function ($query) {
                $query->select(DB::raw("product_id, SUM(stock) as sum_stock"))->where('status', ProductVariantStatus::ON_SALE)->groupBy('product_id');
            }
        ];
    }

    public function campaignId(?int $id): self
    {
        if (!empty($id)) {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('promotion_campaign_id', $id));
        }
        return $this;
    }

    public function timelineId(?int $id): self
    {
        if (!empty($id)) {
            $this->timelineId = $id;
        }
        return $this;
    }

    public function categoryId(?int $id): self
    {
        if (!empty($id)) {
            $this->categoryId = $id;
        }
        return $this;
    }

    public function justCheckOnlyOne(?int $productId): self
    {
        $this->checkOnlyOne = true;
        $this->productId = $productId;
        return $this;
    }
}
