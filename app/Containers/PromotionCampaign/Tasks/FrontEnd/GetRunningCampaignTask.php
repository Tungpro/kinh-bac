<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-04 14:38:48
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-10 16:54:32
 * @ Description: Happy Coding!
 */


namespace App\Containers\PromotionCampaign\Tasks\FrontEnd;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignRepository;
use App\Containers\PromotionCampaign\Enums\PromotionCampaignStatus;
use App\Containers\PromotionCampaign\Enums\PromotionCampaignType;
use App\Ship\Criterias\Eloquent\OrderByFieldsCriteria;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;

class GetRunningCampaignTask extends Task
{
    protected $repository;

    public function __construct(PromotionCampaignRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        $language_id = $this->currentLang ? $this->currentLang->language_id : 1;

        $this->repository->with(['desc' => function ($query) use ($language_id) {
            $query->select('id', 'promotion_campaign_id', 'name', 'short_description', 'meta_title');
            $query->whereHas('language', function ($q) use ($language_id) {
                $q->where('language_id', $language_id);
            });
        }]);

        return $this->repository->first();
    }

    public function onlyFlashSale(): self
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('formating', PromotionCampaignType::FLASH));
        return $this;
    }

    public function startMostRecently(): self
    {
        $this->repository->pushCriteria(new OrderByFieldsCriteria([
            ['start_date', 'desc']
        ]));
        return $this;
    }

    public function isActive(bool $bool = true): self
    {
        $this->repository->pushCriteria(new ThisOperationThatCriteria('start_date', time(), '<='));
        $this->repository->pushCriteria(new ThisOperationThatCriteria('end_date', time(), '>'));
        $this->repository->pushCriteria(new ThisEqualThatCriteria('status', $bool ? PromotionCampaignStatus::ACTIVE : PromotionCampaignStatus::DE_ACTIVE));
        return $this;
    }
}
