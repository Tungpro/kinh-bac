<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-27 15:18:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-27 15:28:53
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Tasks\Calculators;

use App\Ship\Parents\Tasks\Task;

class CalcPriceParityFromCampaignTask extends Task
{
    public function run(&$product, $priceParity): void
    {
        $product->discount = -1;
        $product->price = $priceParity;

        if (isset($product->variants) && !empty($product->variants)) {
            foreach ($product->variants as $variant) {
                $variant->price = $priceParity;
            }
        }
    }
}
