<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-27 15:18:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-29 15:47:58
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Tasks\Calculators;

use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;

class CalcRunningTimeLineTask extends Task
{
    /**
     * Truyền con trỏ vào để có thể update ngược dữ liệu timeline trở lại campaign
     */
    public function run(&$currentCampaign)
    {
        $runningTimeline = null;
        if (!empty($currentCampaign) && $currentCampaign->relationLoaded('timeline') && !$currentCampaign->timeline->IsEmpty()) {
            $currentCampaign->timeline = $currentCampaign->timeline->sortBy('value', SORT_NATURAL)->values();
            $now = Carbon::now();
            // dd($currentCampaign->timeline,$now);
            $nearestTimeNow = null;
            $nextTimeNow = null;
            foreach ($currentCampaign->timeline as $index => $line) {
                $dateFromTimeline = Carbon::parse($line->value);
                $line->is_passed = $dateFromTimeline < $now;
                $line->is_running = false;
                $line->end_at = false;
                if ($dateFromTimeline < $now) {
                    $nearestTimeNow = $line;
                    $nextTimeNow = $currentCampaign->timeline->get(++$index);
                }
            }
            $currentCampaign->timeline = $currentCampaign->timeline->keyBy('id');

            if (!empty($nearestTimeNow)) {
                $runningTimeline = &$currentCampaign->timeline[$nearestTimeNow->id];

                $runningTimeline->is_running = true;

                if (!empty($nextTimeNow)) {
                    $nextTimeNow = Carbon::parse($nextTimeNow->value);
                } else {
                    $nextTimeNow = Carbon::parse('23:59:59');
                }

                $runningTimeline->end_at_sec = $nextTimeNow->diffInSeconds($now);
                $runningTimeline->end_at = $nextTimeNow->timestamp;
            }
            // dd($currentCampaign->timeline,$nextTimeNow,$runningTimeline,$now,Carbon::parse($runningTimeline->value));
        }

        return $runningTimeline;
    }
}
