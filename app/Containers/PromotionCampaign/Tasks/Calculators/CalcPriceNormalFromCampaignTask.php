<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-27 15:18:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-01 20:41:11
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Tasks\Calculators;

use App\Ship\Parents\Tasks\Task;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class CalcPriceNormalFromCampaignTask extends Task
{
    public function run(&$product, $percent): void
    {
        $product->discount = $percent;
        $product->price = $product->global_price > 0 ?
            ($product->global_price - ($product->global_price * $percent / 100)) : ($product->price - ($product->price * $percent / 100));
        // dd($product->variants);
        if (
            isset($product->variants)
            &&  ($product->variants instanceof iterable || $product->variants instanceof Collection || $product->variants instanceof LengthAwarePaginator)
            && !empty($product->variants)
        ) {
            foreach ($product->variants as $variant) {
                $variant->price = $variant->global_price > 0 ?
                    $this->variantPricaGTZero($variant, $percent, $product->global_price) :
                    $this->variantPriceLTZero($variant, $percent, $product->global_price);
            }
        } else if (isset($product->variants) && !empty($product->variants)) {
            // die('xxx');
            $product->variants->price = $product->variants->global_price > 0 ?
                $this->variantPricaGTZero($product->variants, $percent, $product->global_price) :
                $this->variantPriceLTZero($product->variants, $percent, $product->global_price);
        }
    }

    private function variantPricaGTZero($variant, $percent, $referencePrice)
    {
        return ($variant->global_price - ($variant->global_price * $percent / 100));
    }

    private function variantPriceLTZero($variant, $percent, $referencePrice)
    {
        $value = 0;
        if ($referencePrice > 0) {
            $value = ($referencePrice - ($referencePrice * $percent / 100));
        } else {
            $value = ($variant->price - ($variant->price * $percent / 100));
        }

        return $value;
    }
}
