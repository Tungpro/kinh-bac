<?php

namespace App\Containers\PromotionCampaign\Tasks;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignRepository;
use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignProductRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeletePromotionCampaignByIdTask extends Task
{

    protected $repository;

    public function __construct(PromotionCampaignRepository $repository, PromotionCampaignProductRepository $repositoryPrd)
    {
        $this->repository = $repository;
        $this->repositoryPrd = $repositoryPrd;
    }

    public function run($id)
    {
        try {
            $this->repositoryPrd->where('promotion_campaign_id',$id)->delete();
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
