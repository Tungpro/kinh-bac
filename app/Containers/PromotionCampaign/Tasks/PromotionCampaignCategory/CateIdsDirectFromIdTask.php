<?php

namespace App\Containers\PromotionCampaign\Tasks\PromotionCampaignCategory;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignCategoryRepository;
use App\Ship\Criterias\Eloquent\SelectFieldsCriteria;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

/**
 * Class CateIdsDirectFromPrdIdTask.
 */
class CateIdsDirectFromIdTask extends Task
{

    protected $repository;

    public function __construct(PromotionCampaignCategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return  mixed
     */
    public function run($campaign_id)
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('promotion_campaign_id',$campaign_id));
        $this->repository->pushCriteria(new SelectFieldsCriteria(['category_id']));

        $result = $this->repository->all();

        if($result && !$result->isEmpty()) {
            return $result->pluck('category_id')->toArray();
        }

        return [];
    }
}
