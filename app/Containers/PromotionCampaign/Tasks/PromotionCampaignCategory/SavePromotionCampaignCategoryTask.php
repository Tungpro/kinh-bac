<?php

namespace App\Containers\PromotionCampaign\Tasks\PromotionCampaignCategory;

use App\Containers\PromotionCampaign\Models\PromotionCampaign;
use App\Ship\Parents\Tasks\Task;

/**
 * Class SaveProductCategoryTask.
 */
class SavePromotionCampaignCategoryTask extends Task
{
    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($campaign_category, PromotionCampaign $current_campaign_model)
    {
        // $current_campaign_model = new PromotionCampaign;
        if ($campaign_category) {
            if (is_array($campaign_category)) {
                $current_campaign_model->categories()->sync($campaign_category);
            } else {
                $current_campaign_model->categories()->attach($campaign_category);
            }
        }else {
            $current_campaign_model->categories()->sync([]);
        }
    }
}
