<?php

namespace App\Containers\PromotionCampaign\Tasks\PromotionCampaignItemTimeline;

use App\Containers\PromotionCampaign\Data\Repositories\PromotionCampaignItemTimelineRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class GetProductGalleryTask.
 */
class GetPromotionCampaignItemTimelineByIDTask extends Task
{

    protected $repository;

    public function __construct(PromotionCampaignItemTimelineRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run(int $id)
    {
       return $this->repository->where('promotion_item_id', $id)->pluck('timeline_id');
    }
}
