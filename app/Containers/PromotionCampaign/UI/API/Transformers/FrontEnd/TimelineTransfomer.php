<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-04 14:54:56
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-10 22:58:34
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\UI\API\Transformers\FrontEnd;

use App\Containers\PromotionCampaign\Models\PromotionCampaignTimeline;
use App\Ship\Parents\Transformers\Transformer;
use Carbon\Carbon;

class TimelineTransfomer extends Transformer
{

    protected array $defaultIncludes = [];
    protected array $availableIncludes = [];

    public $selectedTimelineId = 0;

    public function __construct(int $selectedTimelineId = 0)
    {
        $this->selectedTimelineId = $selectedTimelineId;
        parent::__construct();
    }

    public function transform(PromotionCampaignTimeline $entity)
    {
        $data =[
            'id' => $entity->id,
            'active' => $this->selectedTimelineId == $entity->id ? true : false,
            'value' => $entity->value,
            'end_at_sec' => $entity->end_at_sec,
            'end_at' => $entity->end_at,
            'is_passed' => $entity->is_passed,
            'is_running' => $entity->is_running,
        ];

        return $data;
    }
}
