<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-02 11:43:56
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-20 17:38:08
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\UI\API\Transformers\FrontEnd;

use App\Containers\BaseContainer\UI\API\Transformers\FrontEnd\BaseCategoryTransformer;

class PromotionCampaignCateTransfomer extends BaseCategoryTransformer
{
    public function transform($category)
    {
        $response = parent::transform($category);

        $response['link'] = route('web_product_promotion_detail_cate',['slug' => $category['desc']['slug'],'id' => $category['category_id']]);

        return $response;
    }
}
