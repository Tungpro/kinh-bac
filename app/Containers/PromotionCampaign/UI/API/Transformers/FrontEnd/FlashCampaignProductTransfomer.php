<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-04 14:54:56
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-12 17:23:18
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\UI\API\Transformers\FrontEnd;

use App\Containers\Product\UI\API\Transformers\FrontEnd\ProductListTransformer;

class FlashCampaignProductTransfomer extends ProductListTransformer
{
    protected array $defaultIncludes = [];
    protected array $availableIncludes = [];

    public function transform($data)
    {
        // dd($data);
        $product = parent::transform($data->product);
        // dd($data->product->sumStockVariant->sum_stock);

        $product['flash_quantity'] = isset($data->product->sumStockVariant) && !empty($data->product->sumStockVariant) ? (int)$data->product->sumStockVariant->sum_stock : 0;
        $product['flash_sold'] = 0;

        return $product;

    }
}
