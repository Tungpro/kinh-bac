<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-04 14:54:56
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-13 10:55:52
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\UI\API\Transformers\FrontEnd;

use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\ImageURL;
use Apiato\Core\Foundation\StringLib;
use App\Containers\PromotionCampaign\Models\PromotionCampaign;
use App\Ship\Parents\Transformers\Transformer;
use Carbon\Carbon;

class MostRecentlyFlashCampaignTransfomer extends Transformer
{
    protected array $defaultIncludes = [
        'timeline',
        'categories'
    ];

    protected array $availableIncludes = [];

    /**
     * [
     *  @var Array 'cate_ids'
     *  @var Int 'timeline_id'
     * ]
     */
    public $selectedValues;

    public function __construct(array $selectedValues =  [])
    {
        $this->selectedValues = $selectedValues;
        parent::__construct();
    }

    public function transform(PromotionCampaign $entity)
    {
        $data = [
            'id' => $entity->id,
            'name' => $entity->desc->name,
            'short_description' => $entity->desc->short_description,
            'image' => ImageURL::getImageUrl($entity->image, 'promotioncampaign', 'medium'),
            'image_mobile' => ImageURL::getImageUrl($entity->image_mobile, 'promotioncampaign', 'medium'),
            'start_date' => FunctionLib::dateFormat($entity->start_date, 'd/m/Y - H:i:s'),
            'end_date' => FunctionLib::dateFormat($entity->end_date, 'd/m/Y - H:i:s'),
            'link' => route('web_product_sale_campaign', ['slug' => StringLib::slug($entity->desc->name), 'id' => $entity->id]),
        ];

        return $data;
    }

    public function includeTimeline($campaign)
    {
        $timeline = $campaign->timeline;

        return (!empty($timeline) && !$timeline->IsEmpty()) ? $this->collection($timeline, new TimelineTransfomer(@$this->selectedValues['timeline_id'] ?? 0), 'campaing_timeline') : $this->null();
    }

    public function includeCategories($campaign)
    {
        $categories = $campaign->categories;

        return (!empty($categories) && !$categories->IsEmpty()) ? $this->collection($categories, new PromotionCampaignCateTransfomer(@$this->selectedValues['cate_ids'] ?? [])) : $this->null();
    }
}
