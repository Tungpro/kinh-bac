<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-04 14:54:56
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-05 14:26:34
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\UI\API\Transformers\FrontEnd;

use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\ImageURL;
use Apiato\Core\Foundation\StringLib;
use App\Containers\PromotionCampaign\Models\PromotionCampaign;
use App\Ship\Parents\Transformers\Transformer;

class CampaignListTransfomer extends Transformer
{

    protected array $defaultIncludes = [];
    protected array $availableIncludes = [];

    public function transform(PromotionCampaign $entity)
    {
        $data =[
            'id' => $entity->id,
            'name' => $entity->desc->name,
            'short_description' => $entity->desc->short_description,
            'image' => ImageURL::getImageUrl($entity->image,'promotioncampaign','medium'),
            'image_mobile' => ImageURL::getImageUrl($entity->image_mobile,'promotioncampaign','medium'),
            'start_date' => FunctionLib::dateFormat($entity->start_date,'d/m/Y - H:i:s'),
            'end_date' => FunctionLib::dateFormat($entity->end_date,'d/m/Y - H:i:s'),
            'link' => route('web_product_sale_campaign',['slug' => StringLib::slug($entity->desc->name),'id' => $entity->id])
        ];

        return $data;
    }
}
