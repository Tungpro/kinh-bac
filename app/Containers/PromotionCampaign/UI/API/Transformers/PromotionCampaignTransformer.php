<?php

namespace App\Containers\PromotionCampaign\UI\API\Transformers;

use App\Containers\PromotionCampaign\Models\PromotionCampaign;
use App\Ship\Parents\Transformers\Transformer;

class PromotionCampaignTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected array $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected array $availableIncludes = [

    ];

    /**
     * @param PromotionCampaign $entity
     *
     * @return array
     */
    public function transform(PromotionCampaign $entity)
    {
        $response = [
            'object' => 'PromotionCampaign',
            'id' => $entity->getHashedKey(),
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
