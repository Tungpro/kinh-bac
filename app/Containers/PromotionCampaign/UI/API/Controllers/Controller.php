<?php

namespace App\Containers\PromotionCampaign\UI\API\Controllers;

use App\Containers\PromotionCampaign\UI\API\Requests\CreatePromotionCampaignRequest;
use App\Containers\PromotionCampaign\UI\API\Requests\DeletePromotionCampaignRequest;
use App\Containers\PromotionCampaign\UI\API\Requests\GetAllPromotionCampaignsRequest;
use App\Containers\PromotionCampaign\UI\API\Requests\FindPromotionCampaignByIdRequest;
use App\Containers\PromotionCampaign\UI\API\Requests\UpdatePromotionCampaignRequest;
use App\Containers\PromotionCampaign\UI\API\Transformers\PromotionCampaignTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\PromotionCampaign\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreatePromotionCampaignRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createPromotionCampaign(CreatePromotionCampaignRequest $request)
    {
        $promotioncampaign = Apiato::call('PromotionCampaign@CreatePromotionCampaignAction', [$request]);

        return $this->created($this->transform($promotioncampaign, PromotionCampaignTransformer::class));
    }

    /**
     * @param FindPromotionCampaignByIdRequest $request
     * @return array
     */
    public function findPromotionCampaignById(FindPromotionCampaignByIdRequest $request)
    {
        $promotioncampaign = Apiato::call('PromotionCampaign@FindPromotionCampaignByIdAction', [$request]);

        return $this->transform($promotioncampaign, PromotionCampaignTransformer::class);
    }

    /**
     * @param GetAllPromotionCampaignsRequest $request
     * @return array
     */
    public function getAllPromotionCampaigns(GetAllPromotionCampaignsRequest $request)
    {
        $promotioncampaigns = Apiato::call('PromotionCampaign@GetAllPromotionCampaignsAction', [$request]);

        return $this->transform($promotioncampaigns, PromotionCampaignTransformer::class);
    }

    /**
     * @param UpdatePromotionCampaignRequest $request
     * @return array
     */
    public function updatePromotionCampaign(UpdatePromotionCampaignRequest $request)
    {
        $promotioncampaign = Apiato::call('PromotionCampaign@UpdatePromotionCampaignAction', [$request]);

        return $this->transform($promotioncampaign, PromotionCampaignTransformer::class);
    }

    /**
     * @param DeletePromotionCampaignRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePromotionCampaign(DeletePromotionCampaignRequest $request)
    {
        Apiato::call('PromotionCampaign@DeletePromotionCampaignAction', [$request]);

        return $this->noContent();
    }
}
