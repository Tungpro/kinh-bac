<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-04 14:24:32
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-10 17:33:44
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\UI\API\Controllers\FrontEnd;

use App\Containers\BaseContainer\UI\WEB\Controllers\BaseApiFrontController;
use App\Containers\PromotionCampaign\UI\API\Controllers\FrontEnd\Features\CampaignList;
use App\Containers\PromotionCampaign\UI\API\Controllers\FrontEnd\Features\GetMostRecently;

class Controller extends BaseApiFrontController
{
    use CampaignList,
        GetMostRecently;

    public function __construct()
    {
        parent::__construct();
    }
}
