<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-05 14:53:08
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\UI\API\Controllers\FrontEnd\Features;

use App\Containers\PromotionCampaign\Actions\FrontEnd\PromotionCampaignListingAction;
use App\Containers\PromotionCampaign\UI\API\Requests\FrontEnd\CampaignListRequest;
use App\Containers\PromotionCampaign\UI\API\Transformers\FrontEnd\CampaignListTransfomer;

trait CampaignList
{
    public function campaignList(CampaignListRequest $request)
    {
        $campaign = app(PromotionCampaignListingAction::class)->run(
            [
                'isActive'
            ],
            [],// With array
            $this->currentLang,
            false,
            15,
            $request->page
        );

        // return $campaign;
        return $this->transform($campaign, CampaignListTransfomer::class,[], [], 'campaign_list');
    }
}
