<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-31 16:12:16
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\UI\API\Controllers\FrontEnd\Features;

use Apiato\Core\Transformers\Serializers\PureArraySerializer;
use App\Containers\PromotionCampaign\Actions\CalcProductPriceByCampaignAction;
use App\Containers\PromotionCampaign\Actions\FrontEnd\CaclcRunningTimeLineAction;
use App\Containers\PromotionCampaign\Actions\FrontEnd\GetProductByCampaignIdAction;
use App\Containers\PromotionCampaign\Actions\FrontEnd\GetRunningFlashSaleCampaignAction;
use App\Containers\PromotionCampaign\UI\API\Requests\FrontEnd\GetMostRecentlyRequest;
use App\Containers\PromotionCampaign\UI\API\Transformers\FrontEnd\FlashCampaignProductTransfomer;
use App\Containers\PromotionCampaign\UI\API\Transformers\FrontEnd\MostRecentlyFlashCampaignTransfomer;
use Carbon\Carbon;

trait GetMostRecently
{
    /**
     * Hiệ tại đang chỉ get flashsale ở đây
     */
    public function getMostRecently(GetMostRecentlyRequest $request)
    {
        $currentCampaign = app(GetRunningFlashSaleCampaignAction::class)
            ->run([], [
                'timeline',
                'categories',
                'categories.desc',
            ], $this->currentLang);

        if(!empty($currentCampaign)){
            $runningTimeline = app(CaclcRunningTimeLineAction::class)->run($currentCampaign);

            $selectedTimeline = $request->timelineId;
            $selectedCate = $request->categoryId;

            $products = app(GetProductByCampaignIdAction::class)->run([
                'timelineId' => !empty($runningTimeline) ? $runningTimeline->id : 0,
                'campaignId' => $currentCampaign->id,
                'categoryId' => (int)$selectedCate
            ]);

            if(!empty($products) && !$products->IsEmpty()) {
                app(CalcProductPriceByCampaignAction::class)->run($currentCampaign, $products);
            }

            // dd($products);
            // return $products;
            // 
            // return $currentCampaign;
            // dd($runningTimeline->id);
            $campaign = $this->transform($currentCampaign, new MostRecentlyFlashCampaignTransfomer(['cate_ids' => [$selectedCate], 'timeline_id' => !empty($runningTimeline) ? $runningTimeline->id : 0]), [], [], 'flash_campaign', new PureArraySerializer, false);
        }

        return $data = [
            'campaign' => $campaign ?? [],
            'products' => $this->transform($products ?? [], new FlashCampaignProductTransfomer, [], [], 'product_list')
        ];
    }
}
