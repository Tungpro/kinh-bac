<?php

/**
 * @apiGroup           PromotionCampaign
 * @apiName            createPromotionCampaign
 *
 * @api                {POST} /v1/promotioncampaigns Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('promotioncampaigns', [
    'as' => 'api_promotioncampaign_create_promotion_campaign',
    'uses'  => 'Controller@createPromotionCampaign',
    'middleware' => [
      'auth:api',
    ],
]);
