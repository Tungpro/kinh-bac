<?php

/**
 * @apiGroup           PromotionCampaign
 * @apiName            updatePromotionCampaign
 *
 * @api                {PATCH} /v1/promotioncampaigns/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->patch('promotioncampaigns/{id}', [
    'as' => 'api_promotioncampaign_update_promotion_campaign',
    'uses'  => 'Controller@updatePromotionCampaign',
    'middleware' => [
      'auth:api',
    ],
]);
