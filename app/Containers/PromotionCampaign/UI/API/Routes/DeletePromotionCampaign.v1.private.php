<?php

/**
 * @apiGroup           PromotionCampaign
 * @apiName            deletePromotionCampaign
 *
 * @api                {DELETE} /v1/promotioncampaigns/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('promotioncampaigns/{id}', [
    'as' => 'api_promotioncampaign_delete_promotion_campaign',
    'uses'  => 'Controller@deletePromotionCampaign',
    'middleware' => [
      'auth:api',
    ],
]);
