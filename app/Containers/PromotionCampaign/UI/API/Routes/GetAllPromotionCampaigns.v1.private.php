<?php

/**
 * @apiGroup           PromotionCampaign
 * @apiName            getAllPromotionCampaigns
 *
 * @api                {GET} /v1/promotioncampaigns Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('promotioncampaigns', [
    'as' => 'api_promotioncampaign_get_all_promotion_campaigns',
    'uses'  => 'Controller@getAllPromotionCampaigns',
    'middleware' => [
      'auth:api',
    ],
]);
