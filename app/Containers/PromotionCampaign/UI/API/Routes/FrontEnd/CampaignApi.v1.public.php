<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-04 16:55:35
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-10 22:44:44
 * @ Description: Happy Coding!
 */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

Route::group(
[
    'middleware' => [
        // 'api',
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run().'/promotioncampaign',
],
function () use ($router) {
    $router->any('/campaignList', [
        'as' => 'api_list_promotion_campaign',
        'uses'       => 'FrontEnd\Controller@campaignList'
    ]);

    $router->any('/getMostRecently', [
        'as' => 'api_get_most_recently_campaign',
        'uses'       => 'FrontEnd\Controller@getMostRecently'
    ]);
});