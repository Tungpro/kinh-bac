<?php

/**
 * @apiGroup           PromotionCampaign
 * @apiName            findPromotionCampaignById
 *
 * @api                {GET} /v1/promotioncampaigns/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('promotioncampaigns/{id}', [
    'as' => 'api_promotioncampaign_find_promotion_campaign_by_id',
    'uses'  => 'Controller@findPromotionCampaignById',
    'middleware' => [
      'auth:api',
    ],
]);
