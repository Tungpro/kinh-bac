<?php

use Illuminate\Support\Facades\Route;

Route::namespace('\App\Containers\PromotionCampaign\UI\WEB\Controllers')->prefix('admin/ajax')->group(function () {
  Route::prefix('promotionCampaign')->group(function () {
    Route::any('/controller/promotionCampaignList', [
      'as' => 'admin.promotionCampaign.promotionCampaignList',
      'uses' => 'Controller@ajaxPromotionCampaignList',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::GET('/controller/promotionCampaignByID/{id}',[
      'as' => 'admin.promotionCampaign.controller.promotionCampaignByID',
      'uses' => 'Controller@ajaxPromotionCampaignByID',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::GET('/controller/{id}/promotionCampaignTimelineByID',[
      'as' => 'admin.promotionCampaign.controller.promotionCampaignTimelineByID',
      'uses' => 'ControllerTimeline@ajaxPromotionCampaignTimelineByID',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::GET('/controller/{id}/promotionCampaignDelete',[
      'as' => 'admin.promotionCampaign.controller.promotionCampaignDelete',
      'uses' => 'Controller@ajaxDeletePromotionCampaign',
       'middleware' => [
        'auth:admin',
      ]
    ]);

    Route::POST('/controller/{id}/savePromotionCampaign',[
      'as' => 'admin.promotionCampaign.controller.savePromotionCampaign',
      'uses' => 'Controller@ajaxSavePromotionCampaign',
       'middleware' => [
        'auth:admin',
      ]
    ]);

    Route::POST('/controller/{id}/syncPromotionCampaign',[
      'as' => 'admin.promotionCampaign.controller.syncPromotionCampaign',
      'uses' => 'Controller@ajaxSyncPromotionCampaign',
       'middleware' => [
        'auth:admin',
      ]
    ]);



    Route::POST('/controller/{id}/promotionCampaignCheckIssetPrd', [
      'as' => 'admin.promotionCampaign.variants.promotionCampaignCheckIssetPrd',
      'uses' => 'Controller@ajaxPromotionCampaignCheckIssetPrd',
       'middleware' => [
        'auth:admin',
      ]
    ]);


    Route::get('/{id}/controller/imgData', [
      'as' => 'admin.promotionCampaign.controller.imgData',
      'uses' => 'PromotionCampaignImgController@ajaxImgData',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::POST('/controller/imgUpload',[
      'as' => 'admin.promotionCampaign.controller.imgUpload',
      'uses' => 'Controller@ajaxImgUpload',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::post('controller/imgUploadSlider', [
      'as' => 'admin.collection.controller.imgUploadSlider',
      'uses' => 'Controller@ajaxImageUploadSlider',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::get('/controller/getFormatList',[
      'as' => 'admin.promotionCampaign.controller.getFormatList',
      'uses' => 'Controller@ajaxGetFormatList',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::get('/controller/getFormatPrice',[
      'as' => 'admin.promotionCampaign.controller.getFormatPrice',
      'uses' => 'Controller@ajaxGetFormatPrice',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::get('/{id}/controller/dataPromotionCampaign', [
      'as' => 'admin.promotionCampaign.controller.dataPromotionCampaign',
      'uses' => 'Controller@ajaxDataCollectionPromotionCampaign',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::POST('/controller/searchProduct', [
      'as' => 'admin.promotionCampaign.searchProduct',
      'uses' => 'Controller@ajaxSearchProduct',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::get('/{id}/controller/cateData', [
      'as' => 'admin.category.controller.cateData',
      'uses' => 'PromotionCampaignCategoryController@ajaxCateDataByID',
      'middleware' => [
          'auth:admin',
      ],
    ]);
    Route::post('/controller/itemImgUpdate',[
      'as' => 'admin.promotionCampaign.controller.itemImgUpdate',
      'uses' => 'Controller@ajaxItemImgUpdate',
       'middleware' => [
        'auth:admin',
      ]
    ]);
  });
});
