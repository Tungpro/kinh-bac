<?php
Route::group(
[
    'prefix' => 'promotionCampaign',
    'namespace' => '\App\Containers\PromotionCampaign\UI\WEB\Controllers',
    'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
    'middleware' => [
        'auth:admin',
    ],
],function () use ($router) {
    $router->get('/{abc}', [
      'as'   => 'admin_promotionCampaign_home_page',
      'uses' => 'Controller@index',
      'middleware' => [
        'auth:admin',
      ],
  ]);
    $router->get('/', [
      'as'   => 'admin_promotionCampaign_home_page',
      'uses' => 'Controller@index',
      'middleware' => [
        'auth:admin',
      ],
  ]);

});
