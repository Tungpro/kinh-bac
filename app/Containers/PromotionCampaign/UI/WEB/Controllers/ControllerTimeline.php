<?php

namespace App\Containers\PromotionCampaign\UI\WEB\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\WebController;
use App\Containers\PromotionCampaign\UI\WEB\Requests\PromotionCampaignImgByIdRequest;
use App\Containers\PromotionCampaign\Actions\TimelineCampaign\GetPromotionCampaignTimelineAction;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use Apiato\Core\Foundation\Facades\FunctionLib;

/**
 * Class ProductDescController
 *
 * @package App\Containers\Product\UI\WEB\Controllers
 */
class ControllerTimeline extends WebController
{
  use ApiResTrait;

  public function ajaxPromotionCampaignTimelineByID(PromotionCampaignImgByIdRequest $request)
  {
    $transporter = $request->toTransporter();
      if ($transporter->id > 0) {
          // $timeline = Apiato::call('PromotionCampaign@TimelineCampaign\GetPromotionCampaignTimelineAction', [$transporter->id]);
          $timeline = app(GetPromotionCampaignTimelineAction::class)->run($transporter->id);
          if(!empty($timeline)){
              return FunctionLib::ajaxRespond(true, 'ok', $timeline);
          }
          return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
      }
  }
} // End class
