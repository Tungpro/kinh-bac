<?php

namespace App\Containers\PromotionCampaign\UI\WEB\Controllers;

use App\Containers\PromotionCampaign\UI\WEB\Requests\GetAllPromotionCampaignsRequest;
use App\Containers\PromotionCampaign\UI\WEB\Requests\UploadImgPromotionCampaignRequest;
use App\Containers\PromotionCampaign\UI\WEB\Requests\PromotionCampaignListRequest;
use App\Containers\PromotionCampaign\UI\WEB\Requests\SavePromotionCampaignRequest;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\AdminController;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\StringLib;
use App\Containers\PromotionCampaign\Models\PromotionCampaign;
use App\Containers\PromotionCampaign\Models\PromotionCampaignImage;
use App\Containers\PromotionCampaign\UI\API\Requests\FindPromotionCampaignByIdRequest;
use App\Containers\PromotionCampaign\Actions\PromotionCampaignProduct\PromotionCampaignCheckIssetPrdAction;
use App\Containers\PromotionCampaign\Actions\PromotionCampaignProduct\SyncPromotionCampaignByIdAction;
class Controller extends AdminController
{

    public function index()
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Promotion Campaign', $this->form == 'list' ? '' : route('admin_promotionCampaign_home_page',['']));
        \View::share('breadcrumb', $this->breadcrumb);

        return view('promotioncampaign::admin.index',[]);
    }

    public function ajaxPromotionCampaignList(PromotionCampaignListRequest $request){
        $skipPagination = false;
        $perPage = 10;
        try {
            $PromotionCampaign = Apiato::call('PromotionCampaign@GetAllPromotionCampaignsAction', [$request->fillters, $perPage, $skipPagination, []]);
            return FunctionLib::ajaxRespond(true, 'Hoàn thành', $PromotionCampaign);
        } catch (\Throwable $th) {
            return FunctionLib::ajaxRespond(true, 'Lỗi');
        }
    }

    public function ajaxPromotionCampaignByID (FindPromotionCampaignByIdRequest $request){
        $dataAlldes = [];
        try {
            $PromotionCampaign = Apiato::call('PromotionCampaign@FindPromotionCampaignByIdAction', [$request->id]);
            foreach ($PromotionCampaign->all_desc as  $value) {
                $dataAlldes[$value['language_id']] = $value;
            }
            $PromotionCampaign->all_desc = $dataAlldes;
            $PromotionCampaign->timeline = json_decode($PromotionCampaign->timeline, true);
            $PromotionCampaign->start_date = $PromotionCampaign->start_date * 1000;
            $PromotionCampaign->end_date = $PromotionCampaign->end_date * 1000;
            return FunctionLib::ajaxRespond(true, 'Hoàn thành', $PromotionCampaign);
        } catch (\Throwable $th) {
            return FunctionLib::ajaxRespond(true, 'Lỗi');

        }

    }

    public function ajaxImgUpload(UploadImgPromotionCampaignRequest $request){
        if ($request->hasFile('Filedata')) {
            $image = $request->file('Filedata');
        if ($image->isValid()) {
            $title = basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension());
            $fname = Apiato::call('File@UploadImageAction', [$request, 'Filedata', $title, StringLib::getClassNameFromString(PromotionCampaign::class)]);
            if (!empty($fname) && !$fname['error']) {
                    $link= \ImageURL::getImageUrl(@$fname['fileName'], 'promotioncampaign', 'slide');
                    $fname['link']=$link;
                    return FunctionLib::ajaxRespond(true, 'ok',$fname);
            }
            return FunctionLib::ajaxRespond(false, 'Upload ảnh thất bại!');
        }
        return FunctionLib::ajaxRespond(false, 'File không hợp lệ!');
    }
    return FunctionLib::ajaxRespond(false, 'Không tìm thấy ảnh!');
    }
    public function ajaxImageUploadSlider(UploadImgPromotionCampaignRequest $request){
        if ($request->hasFile('Filedata')) {
            $image = $request->file('Filedata');
              if ($image->isValid()) {
                  $title = basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension());
                  $fname = Apiato::call('File@UploadImageAction', [$request, 'Filedata', $title, StringLib::getClassNameFromString(PromotionCampaign::class)]);
                  if (!empty($fname) && !$fname['error']) {
                      $imgGallery = new PromotionCampaignImage();
                      $imgGallery->promotion_campaign_id = $request->promotion_campaign_id;
                      $imgGallery->image = $fname['fileName'];
                      $imgGallery->sort = @$request->sort;
                      //                    $imgGallery->changed = time();
                      // $imgGallery->user_id = \Auth::id();
                      //                    $imgGallery->uname = \Auth::user()->user_name;
                      //                    $imgGallery->lang = $request->lang;
                      // $imgGallery->sort = ProductImage::getSortInsert($request->lang);
                      $imgGallery->save();

                      return FunctionLib::ajaxRespond(true, 'ok', ['images' => Apiato::call('PromotionCampaign@GetPromotionCampaignGalleryByIDAction', [$imgGallery->id])]);

                  }
                  return FunctionLib::ajaxRespond(false, 'Upload ảnh thất bại!');
              }
              return FunctionLib::ajaxRespond(false, 'File không hợp lệ!');
          }
          return FunctionLib::ajaxRespond(false, 'Không tìm thấy ảnh!');
    }
    public function ajaxSavePromotionCampaign(SavePromotionCampaignRequest $request){
        try {
            $PromotionCampaignData =  Apiato::call('PromotionCampaign@SavePromotionCampaignByIdAction',[$request->id,$request->data,$request->collectionPickProduct,$request->fileListImg,$request->campaignCategory,$request->timeline]);
            return FunctionLib::ajaxRespond(true, 'Thành công',$PromotionCampaignData);
         } catch (\Throwable $th) {
            return FunctionLib::ajaxRespond(false, 'Lỗi');
         }
    }

    public function ajaxSyncPromotionCampaign(SavePromotionCampaignRequest $request)
    {
      // try {
        $PromotionCampaignData = app(SyncPromotionCampaignByIdAction::class)->run($request->prdSync, $request->id);
        return FunctionLib::ajaxRespond(true, 'Thành công',$PromotionCampaignData);
    //  } catch (\Throwable $th) {
    //     return FunctionLib::ajaxRespond(false, 'Lỗi');
    //  }

    }
    public function ajaxDeletePromotionCampaign(GetAllPromotionCampaignsRequest $request){
        if(isset($request->id)){
            $PromotionCampaignData =  Apiato::call('PromotionCampaign@DeletePromotionCampaignByIdAction',[$request->id]);
            if(!empty($PromotionCampaignData)){
                return FunctionLib::ajaxRespond(true, 'Thành công',$PromotionCampaignData);
            }else{
                return FunctionLib::ajaxRespond(false, 'Không tìm thấy');
            }
        }
        return FunctionLib::ajaxRespond(false, 'Lỗi');
    }

    public function ajaxDataCollectionPromotionCampaign(SavePromotionCampaignRequest $request){
        $transporter = $request->toTransporter();
        if ($transporter->id > 0) {
            $PromotionCampaign = Apiato::call('PromotionCampaign@GetPromotionCampaignPrdDataAction', [$transporter]);
            return FunctionLib::ajaxRespond(true, 'Thành công',$PromotionCampaign);

        }
        return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    public function ajaxGetFormatList(){
        $format = new PromotionCampaign;
        $format = $format->getFormatPromotionCampaign();
        return FunctionLib::ajaxRespond(true, 'Thành công',$format);
    }
    public function ajaxGetFormatPrice(){
        $format = new PromotionCampaign;
        $format = $format->getFormatPricePromotionCampaign();
        return FunctionLib::ajaxRespond(true, 'Thành công',$format);
    }

    public function ajaxItemImgUpdate(GetAllPromotionCampaignsRequest $request){
      $data = $request->editImage;
      if ($data['id'] > 0 ) {
          $cur = PromotionCampaignImage::find($data['id']);
          if ($cur) {
              $cur->sort =(int) $data['sort'];
              $cur->link =$data['link'];
              $cur->save();
              return FunctionLib::ajaxRespond(true, 'Hoàn thành',$cur);
          }
      }
      return FunctionLib::ajaxRespond(false, 'Dữ liệu không chính xác');
    }

    public function ajaxPromotionCampaignCheckIssetPrd(SavePromotionCampaignRequest $request){
      try {
        //code...
        $dataIsset = app(PromotionCampaignCheckIssetPrdAction::class)->run($request->pickPrd, $request->id);
        return FunctionLib::ajaxRespond(true, 'Thành công', $dataIsset);
      } catch (\Throwable $th) {
        //throw $th;
        return FunctionLib::ajaxRespond(false, 'Lỗi');
      }
    }
}
