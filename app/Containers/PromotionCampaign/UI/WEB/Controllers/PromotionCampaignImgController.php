<?php

namespace App\Containers\PromotionCampaign\UI\WEB\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\WebController;
use App\Containers\PromotionCampaign\UI\WEB\Requests\PromotionCampaignImgByIdRequest;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use Apiato\Core\Foundation\Facades\FunctionLib;

/**
 * Class ProductDescController
 *
 * @package App\Containers\Product\UI\WEB\Controllers
 */
class PromotionCampaignImgController extends WebController
{
  use ApiResTrait;

  public function ajaxImgData(PromotionCampaignImgByIdRequest $request)
  {
    $transporter = $request->toTransporter();
      if ($transporter->id > 0) {
          $PromotionCampaignImgs = Apiato::call('PromotionCampaign@GetPromotionCampaignGalleryAction', [$transporter->id]);
          if(!empty($PromotionCampaignImgs)){
              return FunctionLib::ajaxRespond(true, 'ok', $PromotionCampaignImgs);
          }
          return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
      }
  }
} // End class
