<?php

namespace App\Containers\PromotionCampaign\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class PromotionCampaignRepository
 */
class PromotionCampaignProductRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
