<?php

namespace App\Containers\PromotionCampaign\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class PromotionCampaignRepository
 */
class PromotionCampaignItemTimelineRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'promotion_item_id' => '=',
        // ...
    ];

}
