<?php

namespace App\Containers\PromotionCampaign\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class PromotionCampaignRepository
 */
class PromotionCampaignImageRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
