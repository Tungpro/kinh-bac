<?php

namespace App\Containers\PromotionCampaign\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class PromotionCampaignRepository
 */
class PromotionCampaignCategoryRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'promotion_campaign_id' => '=',
        // ...
    ];

}
