<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-28 17:46:37
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class PromotionCampaignPriceType extends BaseEnum
{
    /**
     * Giảm giá thường
     */
    const NORMAL = 'normal';

    /**
     * Đồng giá sản phẩm
     */
    const PARITY = 'parity';

    const TEXT = [
        self::NORMAL => 'Loại hình giá bình thường',
        self::PARITY => 'Loại hình giá đồng giá'
    ];
}
