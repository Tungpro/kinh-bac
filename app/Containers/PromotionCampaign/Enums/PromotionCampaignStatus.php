<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-04 14:45:04
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class PromotionCampaignStatus extends BaseEnum
{

}
