<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-28 17:47:24
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class PromotionCampaignType extends BaseEnum
{
    /**
     * Chiến dịch giảm giá thường
     */
    const NORMAL = 'normal';

    /**
     * Chiến dịch giảm giá Flashsale
     */
    const FLASH = 'flash';

    const TEXT = [
        self::NORMAL => 'Chiến dịch bình thường',
        self::FLASH => 'Chiến dịch flashsale'
    ];
}
