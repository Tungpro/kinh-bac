<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-10 21:36:22
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-10 21:41:12
 * @ Description: Happy Coding!
 */

namespace App\Containers\PromotionCampaign\Models;

use App\Ship\Parents\Models\Model;

class PromotionCampaignItemTimeline extends Model
{
    protected $fillable = [];

    protected $attributes = [];

    protected $hidden = [];

    protected $casts = [];

    protected $guarded = [];

    protected $table = 'promotion_campaign_item_timeline';

    public $timestamps = false;

    public function timeline()
    {
        return $this->belongsToMany(PromotionCampaignTimeline::class, 'promotion_item_id', 'id');
    }
}
