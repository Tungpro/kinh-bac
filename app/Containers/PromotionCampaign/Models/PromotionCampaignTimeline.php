<?php

namespace App\Containers\PromotionCampaign\Models;

use App\Ship\Parents\Models\Model;

class PromotionCampaignTimeline extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];
    public $timestamps = false;

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $table = 'promotion_campaign_timeline';
    // protected $primaryKey = ['promotion_campaign_id','category_id'];
}
