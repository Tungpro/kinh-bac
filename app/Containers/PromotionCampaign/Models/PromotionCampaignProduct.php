<?php

namespace App\Containers\PromotionCampaign\Models;

use App\Ship\Parents\Models\Model;
use App\Containers\Product\Models\ProductDesc;
use App\Containers\Product\Models\Product;

class PromotionCampaignProduct extends Model
{
    protected $fillable = [];

    protected $attributes = [];

    protected $hidden = [];

    protected $casts = [];

    protected $guarded = [];

    protected $table = 'promotion_campaign_items';

    protected $primaryKey = 'id';

    public function desc_product()
    {
        return $this->hasOne(ProductDesc::class, 'product_id', 'product_id');
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function timeline()
    {
        return $this->belongsToMany(PromotionCampaignTimeline::class, PromotionCampaignItemTimeline::getTableName(), 'promotion_item_id', 'timeline_id');
    }
}
