<?php

namespace App\Containers\PromotionCampaign\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class PromotionCampaignDescription extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];
    public $timestamps = false;

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $table = 'promotion_campaign_description';
    protected $primaryKey = 'id';

    public function language()
    {
        return $this->hasOne(Language::class,'language_id','language_id');
    }
}
