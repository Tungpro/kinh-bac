<?php

namespace App\Containers\PromotionCampaign\Models;

use App\Ship\Parents\Models\Model;
use Apiato\Core\Foundation\ImageURL;
class PromotionCampaignImage extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];
    public $timestamps = false;

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $table = 'promotion_campaign_img';
    protected $resourceKey = 'promotioncampaigns';
    public function getImageUrl($size = 'original')
    {
        return ImageURL::getImageUrl($this->image, 'promotioncampaign', $size);
    }

}
