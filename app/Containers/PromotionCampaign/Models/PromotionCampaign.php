<?php

namespace App\Containers\PromotionCampaign\Models;

use App\Ship\Parents\Models\Model;
use App\Containers\Category\Models\Category;
use App\Containers\PromotionCampaign\Enums\PromotionCampaignPriceType;
use App\Containers\PromotionCampaign\Enums\PromotionCampaignType;

class PromotionCampaign extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'id',
        'image',
        'image_mobile',
        'promote_percent',
        'start_date',
        'end_date',
        'status',
        'formating',
        'formatprice',
        'properties',
        'timeline',
        'parity_price',
        'primary_category_id',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $table = 'promotion_campaign';

    // protected $resourceKey = 'promotioncampaigns';
    public function desc()
    {
        return $this->hasOne(PromotionCampaignDescription::class, 'promotion_campaign_id', 'id');
    }

    public function all_desc()
    {
        return $this->hasMany(PromotionCampaignDescription::class, 'promotion_campaign_id', 'id');
    }

    public function promotion_campaign_product()
    {
        return $this->hasMany(PromotionCampaignProduct::class, 'promotion_campaign_id', 'id');
    }

    public function getFormatPromotionCampaign()
    {
        return PromotionCampaignType::TEXT;
    }

    public function getFormatPricePromotionCampaign()
    {
        return PromotionCampaignPriceType::TEXT;
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, PromotionCampaignCategory::getTableName(), 'promotion_campaign_id', 'category_id');
    }

    public function timeline()
    {
        return $this->hasMany(PromotionCampaignTimeline::class, 'promotion_campaign_id', 'id');
    }
}
