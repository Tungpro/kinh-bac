<?php


namespace App\Containers\Authorization\Exceptions;

use App\Ship\Parents\Exceptions\Exception;
use Symfony\Component\HttpFoundation\Response;

class UserNotActiveException extends Exception
{
    CONST RESPONSE_MESSAGE = 'Tài khoản của bạn chưa được kích hoạt. Liên hệ quản trị viên để được hỗ trợ!';

    public $httpStatusCode = Response::HTTP_FORBIDDEN;

    public $message = self::RESPONSE_MESSAGE;
}
