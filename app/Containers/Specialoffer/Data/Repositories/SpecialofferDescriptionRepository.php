<?php

namespace App\Containers\Specialoffer\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class SpecialofferRepository
 */
class SpecialofferDescriptionRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
