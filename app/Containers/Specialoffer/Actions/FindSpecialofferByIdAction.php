<?php

namespace App\Containers\Specialoffer\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindSpecialofferByIdAction extends Action
{
    public function run(int $id)
    {
        $specialoffer = Apiato::call('Specialoffer@FindSpecialofferByIdTask', [$id]);

        return $specialoffer;
    }
}
