<?php

namespace App\Containers\Specialoffer\Tasks\SpecialofferProduct;

use App\Containers\Specialoffer\Data\Repositories\SpecialofferProductRepository;
use App\Ship\Parents\Tasks\Task;

/**
 * Class GetProductGalleryTask.
 */
class SaveSpecialofferProductTask extends Task
{

    protected $repository;

    public function __construct(SpecialofferProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($special_offer_data,$special_offer_id)
    {   
        if(!empty($special_offer_id))
            $this->repository->where('special_offer_id',$special_offer_id)->delete();
        if(!empty($special_offer_data)){
            foreach($special_offer_data as $key => $items){
                $data=[
                    'special_offer_id' => $special_offer_id,
                    'product_id' => $key
                ];
                $this->repository->create($data);
            }
          
        }
        // return $collection->id;
    }
}
