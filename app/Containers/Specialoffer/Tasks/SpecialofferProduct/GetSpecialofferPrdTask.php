<?php

namespace App\Containers\Specialoffer\Tasks\SpecialofferProduct;

use App\Containers\Specialoffer\Data\Repositories\SpecialofferProductRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ByPrdIDCriteria;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Containers\Localization\Models\Language;

class GetSpecialofferPrdTask extends Task
{

    protected $repository;

    public function __construct(SpecialofferProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($request){
        if(!empty($request->prd_id)){
          $this->repository->pushCriteria(new ByPrdIDCriteria($request->prd_id));
        }
        if( !empty($request->id)){
          $this->repository->pushCriteria(new ThisEqualThatCriteria('special_offer_id', $request->id));
        }
        $this->repository->pushCriteria(new WithCriteria(['product'=> function($query) use($request)
        {
          $query->select('id', 'price', 'type', 'status', 'image')->with(['all_desc' => function($q) use($request) {
              $q->activeLang(1);
              $q->where('name',  'LIKE', '%'.$request->inputTitle.'%');
        }]);
        }]));
        $result = $this->repository->get();
        $data=[];
        foreach ($result as $item){
            if(isset($item->product->all_desc[0]->name)){
              $data[$item->product_id]['id'] = $item->product_id;
              $data[$item->product_id]['description'] = $item->product->all_desc[0]->description;
              $data[$item->product_id]['short_description'] = $item->product->all_desc[0]->short_description;
              $data[$item->product_id]['image'] = $item->product->image;
              $data[$item->product_id]['imageUrl'] = $item->product->image_url;
              $data[$item->product_id]['name'] =$item->product->all_desc[0]->name;
              $data[$item->product_id]['price'] = $item->product->price;
            }else{
              unset($data[$item->product_id]);
            }
        }
        return $data;
    }
    
}
