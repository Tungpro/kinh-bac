<?php

namespace App\Containers\Specialoffer\Tasks\SpecialofferProduct;

use App\Containers\Specialoffer\Data\Repositories\SpecialofferProductRepository;
use App\Ship\Parents\Tasks\Task;

class GetsSpecOfferByPrdIdsTask extends Task
{
    protected $repository;

    public function __construct(SpecialofferProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(?array $arrPrdIds)
    {
        
    }
}
