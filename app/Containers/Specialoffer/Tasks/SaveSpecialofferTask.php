<?php

namespace App\Containers\Specialoffer\Tasks;
use Illuminate\Support\Str;
use App\Containers\Specialoffer\Data\Repositories\SpecialofferRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class SaveSpecialofferTask extends Task
{

    protected $repository;

    public function __construct(SpecialofferRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id='', array $data)
    {
        // try {
            if($id!='undefined'){
                unset($data['all_desc'],$data['type'],$data['special_offer_product'],$data['created_at'],$data['updated_at']);
                $data['offer_time_start'] = $data['offer_time_start']/1000;
                $data['offer_time_end'] = $data['offer_time_end']/1000;
                return $this->repository->getModel()->where('id',  $id)->update($data);
            }
            else{
                $specialoffer = $this->repository->getModel();
                $specialoffer->offer_discount_type = $data['offer_discount_type'];
                $specialoffer->offer_discount_value = $data['offer_discount_value'];
                $specialoffer->offer_time_start = $data['offer_time_start']/1000;
                $specialoffer->offer_time_end = $data['offer_time_end']/1000;
                $specialoffer->status = $data['status'];
                $specialoffer->image = $data['image'];
                $specialoffer->save();
                return $specialoffer->id;
            }


        // }
        // catch (Exception $exception) {
        //     throw new UpdateResourceFailedException();
        // }
    }
}
