<?php

namespace App\Containers\Specialoffer\Tasks;

use App\Containers\Specialoffer\Data\Repositories\SpecialofferRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;
use Apiato\Core\Foundation\Facades\Apiato;

class FindSpecialofferByIdTask extends Task
{

    protected $repository;

    public function __construct(SpecialofferRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $id)
    {
        $defaultLanguage = Apiato::call('Localization@GetDefaultLanguageTask');
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;
        return  $this->repository->where('id',$id)->with(['special_offer_product' => function ($query) use($language_id){
            $query->with(['desc_product' => function ($queryChild) use($language_id){
                $queryChild->where('language_id', $language_id)->first();
            }]);
        },'all_desc'])->first();
    }
}
