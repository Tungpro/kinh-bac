<?php

use Illuminate\Support\Facades\Route;

Route::namespace('\App\Containers\Specialoffer\UI\WEB\Controllers')->prefix('admin/ajax')->group(function () {
  Route::prefix('specialoffer')->group(function () {
    Route::any('/controller/specialofferList', [
      'as' => 'admin.specialoffer.specialofferList',
      'uses' => 'Controller@ajaxSpecialofferList',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::GET('/controller/specialofferByID/{id}',[
      'as' => 'admin.specialoffer.controller.specialofferByID',
      'uses' => 'Controller@ajaxSpecialofferByID',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::GET('/controller/{id}/specialofferDelete',[
      'as' => 'admin.specialoffer.controller.specialofferDelete',
      'uses' => 'Controller@ajaxDeleteSpecialoffer',
       'middleware' => [
        'auth:admin',
      ]
    ]);

    Route::POST('/controller/{id}/saveSpecialoffer',[
      'as' => 'admin.specialoffer.controller.saveSpecialoffer',
      'uses' => 'Controller@ajaxSaveSpecialoffer',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::POST('/controller/imgUpload',[
      'as' => 'admin.specialoffer.controller.imgUpload',
      'uses' => 'Controller@ajaxImgUpload',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::get('/controller/getTypeList',[
      'as' => 'admin.specialoffer.controller.getTypeList',
      'uses' => 'Controller@ajaxGetTypeList',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::get('/{id}/controller/dataSpecialoffer', [
      'as' => 'admin.specialoffer.controller.dataSpecialoffer',
      'uses' => 'Controller@ajaxDataSpecialoffer',
       'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::POST('/controller/searchProduct', [
      'as' => 'admin.specialoffer.searchProduct',
      'uses' => 'Controller@ajaxSearchProduct',
       'middleware' => [
        'auth:admin',
      ]
    ]);
  });
});
