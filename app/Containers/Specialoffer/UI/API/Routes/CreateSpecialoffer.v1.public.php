<?php

/**
 * @apiGroup           Specialoffer
 * @apiName            createSpecialoffer
 *
 * @api                {POST} /v1/specialoffers Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('specialoffers', [
    'as' => 'api_specialoffer_create_specialoffer',
    'uses'  => 'Controller@createSpecialoffer',
    'middleware' => [
      'auth:api',
    ],
]);
