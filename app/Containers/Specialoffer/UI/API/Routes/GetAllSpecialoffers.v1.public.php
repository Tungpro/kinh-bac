<?php

/**
 * @apiGroup           Specialoffer
 * @apiName            getAllSpecialoffers
 *
 * @api                {GET} /v1/specialoffers Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('specialoffers', [
    'as' => 'api_specialoffer_get_all_specialoffers',
    'uses'  => 'Controller@getAllSpecialoffers',
    'middleware' => [
      'auth:api',
    ],
]);
