<?php

/**
 * @apiGroup           Specialoffer
 * @apiName            deleteSpecialoffer
 *
 * @api                {DELETE} /v1/specialoffers/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('specialoffers/{id}', [
    'as' => 'api_specialoffer_delete_specialoffer',
    'uses'  => 'Controller@deleteSpecialoffer',
    'middleware' => [
      'auth:api',
    ],
]);
