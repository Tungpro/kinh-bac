<?php

namespace App\Containers\ShoppingCart\Models;

use App\Ship\Parents\Models\Model;

class ShoppingCart extends Model
{
    protected $table = 'shoppingcarts';

    protected $primaryKey = 'identifier';
    protected $keyType = 'string';

    // protected $primaryKey = ['identifier','instance'];

    protected $fillable = [
        'identifier',
        'instance',
        'content'
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'shoppingcarts';
}
