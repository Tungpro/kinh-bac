<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 10:07:07
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-01 20:42:11
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Bussiness;

use Apiato\Core\Traits\HasResourceKeyTrait;
use App\Containers\ShoppingCart\Contracts\Buyable;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Arr;

class CartItem implements Arrayable, Jsonable
{
    use HasResourceKeyTrait;

    /**
     * The rowID of the cart item.
     *
     * @var string
     */
    public $rowId;

    /**
     * The ID of the cart item.
     *
     * @var int|string
     */
    public $id;

    /**
     * The ID of the cart item.
     *
     * @var int|string
     */
    public $variantId;

    /**
     * The quantity for this cart item.
     *
     * @var int|float
     */
    public $quantity;

    public $inStock;

    /**
     * The name of the cart item.
     *
     * @var string
     */
    public $name;

    /**
     * The imageUrl of the cart item.
     *
     * @var string
     */
    public $imageUrl;

    public $productUrl;

    public $productPath;

    /**
     * The price without TAX of the cart item.
     *
     * @var float
     */
    public $price;

    /**
     * The price (Giá thị trường) without TAX of the cart item.
     *
     * @var float
     */
    public $globalPrice;

    /**
     * The options for this cart item.
     *
     * @var array
     */
    public $options;

    /**
     * The selected for this cart item.
     *
     * @var bool|int
     */
    public $selected = false;

    /**
     * Thông báo đi theo từng sản phẩm
     * 
     * @var array
     */
    public $messages = [];

    /**
     * The FQN of the associated model.
     *
     * @var string|null
     */
    private $associatedModel = null;

    /**
     * The tax rate for the cart item.
     *
     * @var int|float
     */
    private $taxRate = 0;

    /**
     * CartItem constructor.
     *
     * @param int|string $id
     * @param string     $name
     * @param float      $price
     * @param array      $options
     */
    public function __construct($data)
    {
        if (empty($data['id'])) {
            throw new \InvalidArgumentException('Please supply a valid identifier.');
        }
        if (empty($data['name'])) {
            throw new \InvalidArgumentException('Please supply a valid name.');
        }
        if (strlen($data['price']) < 0 || !is_numeric($data['price'])) {
            throw new \InvalidArgumentException('Please supply a valid price.');
        }

        $this->id       = $data['id'];
        $this->variantId = $data['variantId'];
        $this->quantity = $data['quantity'];
        $this->imageUrl = $data['imageUrl'];
        $this->productUrl = $data['productUrl'];
        $this->productPath = $data['productPath'];
        $this->name     = $data['name'];
        $this->price    = floatval($data['price']);
        $this->globalPrice = floatval($data['globalPrice']);
        // $this->options  = new CartItemOptions($data['options']);
        $this->options  = $data['options'];
        $this->selected = (bool)@$data['selected'];
        $this->message = $data['message'];
        $this->rowId = $this->generateRowId($data['id'], $data['variantId']);
    }

    /**
     * Returns the formatted price without TAX.
     *
     * @param int    $decimals
     * @param string $decimalPoint
     * @param string $thousandSeperator
     * @return string
     */
    public function price($decimals = null, $decimalPoint = null, $thousandSeperator = null)
    {
        return $this->numberFormat($this->price, $decimals, $decimalPoint, $thousandSeperator);
    }

    /**
     * Returns the formatted price with TAX.
     *
     * @param int    $decimals
     * @param string $decimalPoint
     * @param string $thousandSeperator
     * @return string
     */
    public function priceTax($decimals = null, $decimalPoint = null, $thousandSeperator = null)
    {
        return $this->numberFormat($this->priceTax, $decimals, $decimalPoint, $thousandSeperator);
    }

    /**
     * Returns the formatted subtotal.
     * Subtotal is price for whole CartItem without TAX
     *
     * @param int    $decimals
     * @param string $decimalPoint
     * @param string $thousandSeperator
     * @return string
     */
    public function subtotal($decimals = null, $decimalPoint = null, $thousandSeperator = null)
    {
        return $this->numberFormat($this->subtotal, $decimals, $decimalPoint, $thousandSeperator);
    }

    /**
     * Returns the formatted total.
     * Total is price for whole CartItem with TAX
     *
     * @param int    $decimals
     * @param string $decimalPoint
     * @param string $thousandSeperator
     * @return string
     */
    public function total($decimals = null, $decimalPoint = null, $thousandSeperator = null)
    {
        return $this->numberFormat($this->total, $decimals, $decimalPoint, $thousandSeperator);
    }

    /**
     * Returns the formatted tax.
     *
     * @param int    $decimals
     * @param string $decimalPoint
     * @param string $thousandSeperator
     * @return string
     */
    public function tax($decimals = null, $decimalPoint = null, $thousandSeperator = null)
    {
        return $this->numberFormat($this->tax, $decimals, $decimalPoint, $thousandSeperator);
    }

    /**
     * Returns the formatted tax.
     *
     * @param int    $decimals
     * @param string $decimalPoint
     * @param string $thousandSeperator
     * @return string
     */
    public function taxTotal($decimals = null, $decimalPoint = null, $thousandSeperator = null)
    {
        return $this->numberFormat($this->taxTotal, $decimals, $decimalPoint, $thousandSeperator);
    }

    /**
     * Set the quantity for this cart item.
     *
     * @param int|float $qty
     */
    public function setQuantity($qty)
    {
        if (empty($qty) || !is_numeric($qty))
            throw new \InvalidArgumentException('Please supply a valid quantity.');

        $this->quantity = $qty;
    }

    /**
     * Update the cart item from a Buyable.
     *
     * @param \Gloudemans\Shoppingcart\Contracts\Buyable $item
     * @return void
     */
    public function updateFromBuyable(Buyable $item)
    {
        $this->id       = $item->getBuyableIdentifier($this->options);
        $this->name     = $item->getBuyableDescription($this->options);
        $this->price    = $item->getBuyablePrice($this->options);
        // $this->priceTax = $this->price + $this->tax;
    }

    /**
     * Update the cart item from an array.
     *
     * @param array $attributes
     * @return void
     */
    public function updateFromArray(array $attributes)
    {
        $this->id       = Arr::get($attributes, 'id', $this->id);
        $this->variantId = Arr::get($attributes, 'variantId', $this->variantId);
        
        $this->quantity      = Arr::get($attributes, 'quantity', $this->quantity);
        $this->inStock      = Arr::get($attributes, 'inStock', $this->inStock);

        $this->name     = Arr::get($attributes, 'name', $this->name);
        $this->imageUrl     = Arr::get($attributes, 'imageUrl', $this->imageUrl);
        $this->productUrl     = Arr::get($attributes, 'productUrl', $this->productUrl);
        $this->productPath     = Arr::get($attributes, 'productPath', $this->productPath);
        $this->price    = Arr::get($attributes, 'price', $this->price);
        $this->globalPrice    = Arr::get($attributes, 'globalPrice', $this->globalPrice);
        // $this->priceTax = $this->price + $this->tax;
        // $this->options  = new CartItemOptions(Arr::get($attributes, 'options', $this->options));

        $this->options  = Arr::get($attributes, 'options', $this->options);
        $this->selected    = Arr::get($attributes, 'selected', $this->selected);

        $this->message = Arr::get($attributes, 'message', $this->message);

        // $this->rowId = $this->generateRowId($this->id, $this->options->all());
        $this->rowId = $this->generateRowId($this->id, $this->variantId);
    }

    /**
     * Associate the cart item with the given model.
     *
     * @param mixed $model
     * @return \Gloudemans\Shoppingcart\CartItem
     */
    public function associate($model)
    {
        $this->associatedModel = is_string($model) ? $model : get_class($model);

        return $this;
    }

    /**
     * Set the tax rate.
     *
     * @param int|float $taxRate
     * @return \Gloudemans\Shoppingcart\CartItem
     */
    public function setTaxRate($taxRate)
    {
        $this->taxRate = $taxRate;

        return $this;
    }

    public function setSelected(bool $bool = true)
    {
        $this->selected = $bool;

        return $this;
    }

    public function setMessage(string $message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get an attribute from the cart item or get the associated model.
     *
     * @param string $attribute
     * @return mixed
     */
    public function __get($attribute)
    {
        if (property_exists($this, $attribute)) {
            return $this->{$attribute};
        }

        if ($attribute === 'priceTax') {
            return $this->price + $this->tax;
        }

        if ($attribute === 'subtotal') {
            return $this->quantity * $this->price;
        }

        if ($attribute === 'total') {
            return $this->quantity * ($this->priceTax);
        }

        // if($attribute === 'tax') {
        //     return $this->price * ($this->taxRate / 100);
        // }

        // if($attribute === 'taxTotal') {
        //     return $this->tax * $this->qty;
        // }

        if ($attribute === 'model' && isset($this->associatedModel)) {
            return with(new $this->associatedModel)->find($this->id);
        }

        return null;
    }

    /**
     * Create a new instance from a Buyable.
     *
     * @param \Gloudemans\Shoppingcart\Contracts\Buyable $item
     * @param array                                      $options
     * @return \Gloudemans\Shoppingcart\CartItem
     */
    public static function fromBuyable(Buyable $item, array $options = [])
    {
        return new self($item->getBuyableIdentifier($options), $item->getBuyableDescription($options), $item->getBuyablePrice($options), $options);
    }

    /**
     * Create a new instance from the given array.
     *
     * @param array $attributes
     * @return \Gloudemans\Shoppingcart\CartItem
     */
    public static function fromArray(array $attributes)
    {
        // $options = Arr::get($attributes, 'options', []);

        // dd($options);

        return new self($attributes);
    }

    /**
     * Create a new instance from the given attributes.
     *
     * @param int|string $id
     * @param string     $name
     * @param float      $price
     * @param array      $options
     * @return \Gloudemans\Shoppingcart\CartItem
     */
    public static function fromAttributes($id, $name, $price, array $options = [])
    {
        return new self($id, $name, $price, $options);
    }

    /**
     * Generate a unique id for the cart item.
     *
     * @param string $id
     * @param array  $options
     * @return string
     */
    public static function generateRowId(int $id, int $variantId)
    {
        // ksort($options);

        // return md5($id . serialize($options));
        return md5($id . $variantId);
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'rowId'    => $this->rowId,
            'id'       => $this->id,
            'variantId' => $this->variantId,
            'name'     => $this->name,
            'imageUrl'     => $this->imageUrl,
            'productUrl'     => $this->productUrl,
            'productPath'     => $this->productPath,
            'quantity'      => $this->quantity,
            'inStock'      => $this->inStock,
            'price'    => $this->price,
            'globalPrice' => $this->globalPrice,
            // 'options'  => $this->options->toArray(),
            'options'  => $this->options,
            'selected'  => $this->selected,
            'message' => $this->message,
            // 'tax'      => $this->tax,
            'subtotal' => $this->subtotal
        ];
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * Get the formatted number.
     *
     * @param float  $value
     * @param int    $decimals
     * @param string $decimalPoint
     * @param string $thousandSeperator
     * @return string
     */
    private function numberFormat($value, $decimals, $decimalPoint, $thousandSeperator)
    {
        if (is_null($decimals)) {
            $decimals = is_null(config('shoppingcart-container.format.decimals')) ? 2 : config('shoppingcart-container.format.decimals');
        }

        if (is_null($decimalPoint)) {
            $decimalPoint = is_null(config('shoppingcart-container.format.decimal_point')) ? '.' : config('shoppingcart-container.format.decimal_point');
        }

        if (is_null($thousandSeperator)) {
            $thousandSeperator = is_null(config('shoppingcart-container.format.thousand_seperator')) ? ',' : config('shoppingcart-container.format.thousand_seperator');
        }

        return number_format($value, $decimals, $decimalPoint, $thousandSeperator);
    }
}
