<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 10:07:07
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-19 20:14:26
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Bussiness;

use App\Containers\Localization\Models\Language;
use App\Containers\ShoppingCart\Bussiness\Traits\CalcTotalTrait;
use App\Containers\ShoppingCart\Bussiness\Traits\CouponTrait;
use App\Containers\ShoppingCart\Bussiness\Traits\DeliveryAndPaymentTrait;
use App\Containers\ShoppingCart\Bussiness\Traits\GettersTrait;
use App\Containers\ShoppingCart\Bussiness\Traits\MutationTrait;
use App\Containers\ShoppingCart\Bussiness\Traits\StoreActionsTrait;
use App\Containers\ShoppingCart\Enums\CartInstance;
use App\Containers\ShoppingCart\Contracts\Buyable;
use App\Containers\ShoppingCart\Data\Repositories\ShoppingCartRepository;
use Illuminate\Session\SessionManager;
use Illuminate\Contracts\Events\Dispatcher;

class Cart
{
    use CouponTrait,
        CalcTotalTrait,
        StoreActionsTrait,
        MutationTrait,
        GettersTrait,
        DeliveryAndPaymentTrait;

    const DEFAULT_INSTANCE = CartInstance::DEFAULT_INSTANCE;

    const CART_ALIAS = 'cart';

    /**
     * Instance of the session manager.
     *
     * @var \Illuminate\Session\SessionManager
     */
    private $session;

    private $repository;

    /**
     * Instance of the event dispatcher.
     * 
     * @var \Illuminate\Contracts\Events\Dispatcher
     */
    private $events;

    /**
     * Holds the current cart instance.
     *
     * @var string
     */
    private $instance;

    private $seperator = '.';

    public $currentLang = null;

    /**
     * Cart constructor.
     *
     * @param \Illuminate\Session\SessionManager      $session
     * @param \Illuminate\Contracts\Events\Dispatcher $events
     */
    public function __construct(ShoppingCartRepository $repository, SessionManager $session, Dispatcher $events)
    {
        $this->session = $session;

        $this->repository = $repository;

        $this->events = $events;

        $this->instance(self::DEFAULT_INSTANCE);
    }

    public function currentLang(?Language $currentLang): self
    {
        $this->currentLang = $currentLang;
        return $this;
    }

    /**
     * Set the current cart instance.
     *
     * @param string|null $instance
     * @return \Gloudemans\Shoppingcart\Cart
     */
    public function instance($instance = null)
    {
        $instance = $instance ?: self::DEFAULT_INSTANCE;

        $this->instance = sprintf('%s' . $this->seperator . '%s', self::CART_ALIAS, $instance);

        return $this;
    }

    /**
     * Get the current cart instance.
     *
     * @return string
     */
    public function currentInstance()
    {
        return str_replace(self::CART_ALIAS . $this->seperator, '', $this->instance);
    }

    /**
     * Magic method to make accessing the total, tax and subtotal properties possible.
     *
     * @param string $attribute
     * @return float|null
     */
    public function __get($attribute)
    {
        if ($attribute === 'total') {
            return $this->total();
        }

        if ($attribute === 'tax') {
            return $this->tax();
        }

        if ($attribute === 'subtotal') {
            return $this->subtotal();
        }

        return null;
    }

    /**
     * Check if the item is a multidimensional array or an array of Buyables.
     *
     * @param mixed $item
     * @return bool
     */
    private function isMulti($item)
    {
        if (!is_array($item)) return false;

        return is_array(head($item)) || head($item) instanceof Buyable;
    }

    /**
     * Get the Formated number
     *
     * @param $value
     * @param $decimals
     * @param $decimalPoint
     * @param $thousandSeperator
     * @return string
     */
    private function numberFormat($value, $decimals, $decimalPoint, $thousandSeperator)
    {
        if (is_null($decimals)) {
            $decimals = is_null(config('shoppingcart-container.format.decimals')) ? 2 : config('shoppingcart-container.format.decimals');
        }
        if (is_null($decimalPoint)) {
            $decimalPoint = is_null(config('shoppingcart-container.format.decimal_point')) ? '.' : config('shoppingcart-container.format.decimal_point');
        }
        if (is_null($thousandSeperator)) {
            $thousandSeperator = is_null(config('shoppingcart-container.format.thousand_seperator')) ? ',' : config('shoppingcart-container.format.thousand_seperator');
        }

        return number_format($value, $decimals, $decimalPoint, $thousandSeperator);
    }
}
