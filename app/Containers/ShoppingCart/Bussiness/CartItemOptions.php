<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 10:07:07
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-05 10:38:29
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Bussiness;

use Illuminate\Support\Collection;

class CartItemOptions extends Collection
{
    /**
     * Get the option by the given key.
     *
     * @param string $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->get($key);
    }
}