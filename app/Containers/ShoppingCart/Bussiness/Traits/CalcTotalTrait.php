<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 10:07:07
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-19 15:51:54
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Bussiness\Traits;

use App\Containers\ShoppingCart\Bussiness\CartItem;

trait CalcTotalTrait
{
    /**
     * Get the number of items in the cart.
     *
     * @return int|float
     */
    public function count()
    {
        $content = $this->getContent();

        return $content->sum('quantity');
    }

    /**
     * Get the number of items in the cart.
     *
     * @return int|float
     */
    public function countSelected()
    {
        $content = $this->getContent();

        return $content->sum('selected');
    }

    /**
     * Get the total price of the items in the cart.
     *
     * @param int    $decimals
     * @param string $decimalPoint
     * @param string $thousandSeperator
     * @return string
     */
    public function total($decimals = null, $decimalPoint = null, $thousandSeperator = null)
    {
        $content = $this->getContent();

        $total = $content->reduce(function ($total, CartItem $cartItem) {
            return $total + ($cartItem->selected ? ($cartItem->quantity * $cartItem->priceTax) : 0);
        }, 0);

        // return $this->numberFormat($total, $decimals, $decimalPoint, $thousandSeperator);

        return $total;
    }

    /**
     * Get the total tax of the items in the cart.
     *
     * @param int    $decimals
     * @param string $decimalPoint
     * @param string $thousandSeperator
     * @return float
     */
    public function tax($decimals = null, $decimalPoint = null, $thousandSeperator = null)
    {
        $content = $this->getContent();

        $tax = $content->reduce(function ($tax, CartItem $cartItem) {
            return $tax + ($cartItem->selected ? ($cartItem->quantity * $cartItem->tax) :  0);
        }, 0);

        // return $this->numberFormat($tax, $decimals, $decimalPoint, $thousandSeperator);
        return $tax;
    }

    /**
     * Get the subtotal (total - tax) of the items in the cart.
     *
     * @param int    $decimals
     * @param string $decimalPoint
     * @param string $thousandSeperator
     * @return float
     */
    public function subtotal($decimals = null, $decimalPoint = null, $thousandSeperator = null)
    {
        $content = $this->getContent();

        $subTotal = $content->reduce(function ($subTotal, CartItem $cartItem) {
            return $subTotal + ($cartItem->selected ? ($cartItem->quantity * $cartItem->price) : 0);
        }, 0);

        // return $this->numberFormat($subTotal, $decimals, $decimalPoint, $thousandSeperator);

        return $subTotal;
    }
}