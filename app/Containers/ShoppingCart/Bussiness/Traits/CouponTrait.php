<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 10:07:07
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-27 12:00:27
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Bussiness\Traits;

use App\Containers\Discount\Enums\DiscountCodeExtraRule;
use App\Containers\Discount\Enums\DiscountCodeType;
use App\Containers\Discount\Models\DiscountCode;
use App\Containers\Discount\Tasks\Getters\GetDiscountCodeByTask;
use App\Containers\Discount\Tasks\Getters\ListingDiscountCodeTask;
use Illuminate\Support\Collection;

trait CouponTrait
{
    protected $suffixCoupon = '_coupon';
    protected $maxCouponUsed = 1;

    protected function getCouponContent()
    {
        $content = $this->session->has($this->instance . $this->suffixCoupon)
            ? $this->session->get($this->instance . $this->suffixCoupon)
            : new Collection();

        return $content;
    }

    public function addCoupon(DiscountCode $code)
    {
        $couponContent = $this->getCouponContent();

        if ($couponContent->count() >= $this->maxCouponUsed) {
            $couponContent = new Collection();
        }

        $couponContent->put($code->code, $code->code);

        $this->session->put($this->instance . $this->suffixCoupon, $couponContent);

        return $code;
    }

    public function removeCoupon(DiscountCode $code)
    {
        $couponContent = $this->getCouponContent();
        // dd($couponContent->has($code->code),$code->code);
        if ($couponContent->has($code->code)) {
            $couponContent->pull($code->code);

            $this->session->put($this->instance . $this->suffixCoupon, $couponContent);
        }
    }

    public function calcCouponValue($total = 0, $shippingFee = 0)
    {
        $contentCoupon = $this->getCouponContent();

        $coupons = app(ListingDiscountCodeTask::class)
            ->isActive()
            ->byListCode($contentCoupon->toArray())
            ->skipPagin()
            ->run();
        // dd($coupons);
        $value = 0;
        foreach ($coupons as $coupon) {
            if ($coupon->discount_type == DiscountCodeType::PERCENT) {
                // Check ExtraRules: Giá trị tối thiêu, số lượng sp tối thiếu, .....
                $value += $this->detectExtraRule($coupon, $total, $shippingFee) ?
                    // value > 100 thì mặc định là giảm 100% cmnl
                    ($total * ($coupon->discount_value <= 100 ? $coupon->discount_value : 100)) / 100 : 0;
            } elseif ($coupon->discount_type == DiscountCodeType::AMOUNT) {
                $value += $this->detectExtraRule($coupon, $total, $shippingFee) ? $coupon->discount_value : 0;
            } elseif ($coupon->discount_type == DiscountCodeType::FREESHIP) {
                $value += $this->detectFreeshipRule($coupon,$total,$shippingFee);
            }
        }

        return [
            'value' => $value,
            'coupons' => !empty($coupons) && $value > 0 ? $coupons : collect(),
        ];
    }

    private function detectExtraRule(DiscountCode $coupon, $total, $shippingFee): bool
    {
        // Tổng đơn tối thiểu
        if ($coupon->extra_rule == DiscountCodeExtraRule::ORDER_MINIMUM) {
            return $total >= (float)$coupon->extra_rule_value;
        }

        return true;
    }

    private function detectFreeshipRule(DiscountCode $coupon,$total,$shippingFee) {
        $value = 0;
        if($coupon->hasShipRate) {
            /**
             * giá trị ship tối đa được phép giảm, 
             * nếu > phí ship thì chỉ giảm đúng phí ban đầu.
             */
            $value += $shippingFee - ($shippingFee - ($shippingFee >= $coupon->ship_rate ? $coupon->ship_rate : 0));
        }

        return $value;
    }
}
