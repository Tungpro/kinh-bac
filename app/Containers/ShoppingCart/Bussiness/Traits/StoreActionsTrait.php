<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 10:07:07
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-19 19:21:59
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Bussiness\Traits;

use App\Containers\ShoppingCart\Events\CartRestoredEvent;
use App\Containers\ShoppingCart\Events\CartStoredEvent;
use App\Containers\ShoppingCart\Models\ShoppingCart;

trait StoreActionsTrait
{
    /**
     * Store an the current instance of the cart.
     *
     * @param mixed $identifier
     * @return void
     */
    public function store(string $identifier)
    {
        $content = $this->getContent();
        $couponContent = $this->getCouponContent();
        $deliveryContent = $this->getDeliveryContent();
        // dd($this->storedCartWithIdentifierExists($identifier));

        $cart = $this->storedCartWithIdentifierExists($identifier, $this->currentInstance());
        // dd($cart, $this->instance);
        // if ($existed) {
        //     throw new CartAlreadyStoredException("A cart with identifier {$identifier} was already stored.");
        // }
        if (!empty($cart)) {
            $cart->content = serialize($content);
            $cart->coupon_content = serialize($couponContent);
            $cart->delivery_content = serialize($deliveryContent);
            $cart->save();
        } else {
            $this->repository->create([
                'identifier' => $identifier,
                'instance' =>  $this->currentInstance(),
                'content' =>  serialize($content),
                'coupon_content' =>  serialize($couponContent),
                'delivery_content' => serialize($deliveryContent),
            ]);
        }

        // $this->getConnection()->table($this->getTableName())->insert([
        //     'identifier' => $identifier,
        //     'instance' => $this->currentInstance(),
        //     'content' => serialize($content)
        // ]);

        CartStoredEvent::dispatch();
    }

    /**
     * Restore the cart with the given identifier.
     *
     * @param mixed $identifier
     * @return void
     */
    public function restore($identifier)
    {
        $stored = $this->storedCartWithIdentifierExists($identifier, $this->currentInstance());

        if ($stored) {
            $storedContent = unserialize($stored->content);
            $storedCouponContent = !empty($stored->coupon_content) ? unserialize($stored->coupon_content) : [];
            $storedDeliveryContent = !empty($stored->delivery_content) ? unserialize($stored->delivery_content) : [];

            $currentInstance = $this->currentInstance();

            $this->instance($stored->instance);

            $content = $this->getContent();
            $couponContent = $this->getCouponContent();
            $deliveryContent = $this->getDeliveryContent();

            foreach ($storedContent as $cartItem) {
                $content->put($cartItem->rowId, $cartItem);
            }

            foreach ($storedCouponContent as $couponItem) {
                $couponContent->put($couponItem, $couponItem);
            }

            foreach ($storedDeliveryContent as $deliveryItem) {
                $deliveryContent->put($deliveryItem, $deliveryItem);
            }

            CartRestoredEvent::dispatch();

            $this->session->put($this->instance, $content);
            $this->session->put($this->instance . $this->suffixCoupon, $couponContent);
            $this->session->put($this->instance . $this->suffixDelivery, $deliveryContent);

            $this->instance($currentInstance);
        }
    }

    /**
     * @param $identifier
     * @return bool
     */
    private function storedCartWithIdentifierExists(string $identifier, string $instance): ?ShoppingCart
    {
        $returnData = $this->repository->findWhere([
            ['identifier', '=', $identifier],
            ['instance', '=', $instance]
        ])->first();

        // dd($returnData,$instance,$identifier);

        return $returnData;
    }
}
