<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 10:07:07
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-01 20:34:16
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Bussiness\Traits;

use App\Containers\Product\Actions\FrontEnd\PreCheckingToAddCartItemAction;
use App\Containers\Product\Actions\FrontEnd\ProductListingAction;
use App\Containers\PromotionCampaign\Actions\FrontEnd\SubActions\GetMostRecentlyCampaignOnProductListSubAction;
use App\Containers\PromotionCampaign\Actions\SubActions\CalcProductPriceByCampaignSubAction;
use App\Containers\ShoppingCart\Bussiness\CartItem;
use App\Containers\ShoppingCart\Exceptions\InvalidRowIDException;
use Closure;
use Illuminate\Support\Collection;
use PhpParser\Node\Expr\Cast\Double;

trait GettersTrait
{
    /**
     * Get a cart item from the cart by its rowId.
     *
     * @param string $rowId
     * @return \Gloudemans\Shoppingcart\CartItem
     */
    public function get($rowId)
    {
        $content = $this->getContent();

        if (!$content->has($rowId))
            throw new InvalidRowIDException("The cart does not contain rowId {$rowId}.");

        return $content->get($rowId);
    }

    /**
     * Get the content of the cart.
     *
     * @return \Illuminate\Support\Collection
     */
    public function content(bool $selectedItem = false, bool $isRefresh = false, bool $pureContent = false)
    {
        // if (is_null($this->session->get($this->instance))) {
        //     return new Collection([]);
        // }

        // return $this->session->get($this->instance);
        $content = $this->getContent();
        // $couponContent = $this->getCouponContent();

        if ($isRefresh) {
            /**
             *  variantId => productId
             */
            $productIds = $content->pluck('id', 'variantId')->toArray();

            // dd($productIds);

            $products = app(ProductListingAction::class)->skipCache()->run(
                [
                    'equalIds' => $productIds
                ], // filters
                [], // orderBy
                0, // limit
                true, // Skip pagination
                $this->currentLang, // curent lang
            );

            $campaigns = app(GetMostRecentlyCampaignOnProductListSubAction::class)->run($productIds);

            $variantIds = array_flip($productIds);

            $preCheckingToAddCartItemAction = app(PreCheckingToAddCartItemAction::class);

            foreach ($products as $product) {
                $filterItems = $content->filter(function($item) use($product) {
                    return $item->id == $product->id;
                });

                foreach ($filterItems as $rowId => $itemCart) {
                // $rowId = CartItem::generateRowId($product->id, $variantIds[$product->id]);
                $currentCartItem = $content->get($rowId);

                    $itemUpdate = $preCheckingToAddCartItemAction
                        ->currentCampaign(isset($campaigns[$product->id]) ? $campaigns[$product->id] : null)
                        ->currentProduct($product)
                        ->run($product->id, @$itemCart->variantId ?? null, @$itemCart->quantity ?? null);

                    $currentCartItem->updateFromArray($itemUpdate);
                }
            }

            // dd($products, $campaigns, $content);
        }

        if ($selectedItem) {
            $content = $content->filter(function ($item) {
                return $item->selected == true;
            });
        }

        $arr = [
            'items' => $content->toArray(),
            'count' => $this->count(),
            'count_selected' => $this->countSelected(),
            'tax' => $this->tax(),
            'sub_total' => $this->subtotal(),
            'total' => $this->total(),
            'delivery' => $pureContent ? false : $this->calcDeliveryFee(),
        ];

        $arr['coupon'] = $pureContent ? false : $this->calcCouponValue($arr['sub_total'], (float)$arr['delivery']['value']);

        $arr['total'] = (float)$arr['total'] - (float)@$arr['coupon']['value'];

        return $arr;
    }

    /**
     * Get the carts content, if there is no cart content set yet, return a new empty Collection
     *
     * @return \Illuminate\Support\Collection
     */
    protected function getContent()
    {
        $content = $this->session->has($this->instance)
            ? $this->session->get($this->instance)
            : new Collection();

        return $content;
    }

    /**
     * Search the cart content for a cart item matching the given search closure.
     *
     * @param \Closure $search
     * @return \Illuminate\Support\Collection
     */
    public function search(Closure $search)
    {
        $content = $this->getContent();

        return $content->filter($search);
    }

    public function refreshCart()
    {
    }
}
