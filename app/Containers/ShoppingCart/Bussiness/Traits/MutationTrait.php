<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 10:07:07
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-19 15:44:09
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Bussiness\Traits;

use App\Containers\ShoppingCart\Bussiness\CartItem;
use App\Containers\ShoppingCart\Contracts\Buyable;
use App\Containers\ShoppingCart\Events\CartAddItemEvent;
use App\Containers\ShoppingCart\Events\CartRemovedItemEvent;
use App\Containers\ShoppingCart\Events\CartUpdatedItemEvent;
use App\Containers\ShoppingCart\Exceptions\UnknownModelException;

trait MutationTrait
{
    /**
     * Add an item to the cart.
     *
     * @param mixed     $id
     * @param mixed     $name
     * @param int|float $quantity
     * @param float     $price
     * @param array     $options
     * @return \Gloudemans\Shoppingcart\CartItem
     */
    public function add($id, $name = null, $quantity = null, $price = null, array $options = [])
    {
        if ($this->isMulti($id)) {
            return array_map(function ($item) {
                return $this->add($item);
            }, $id);
        }

        $cartItem = $this->createCartItem($id, $name, $quantity, $price, $options);

        $content = $this->getContent();

        if ($content->has($cartItem->rowId)) {
            // $cartItem->quantity += $content->get($cartItem->rowId)->quantity;
        }

        $content->put($cartItem->rowId, $cartItem);

        $this->session->put($this->instance, $content);

        // dd($content);
        CartAddItemEvent::dispatch($cartItem, $this->instance);

        return $cartItem;
    }

    /**
     * Update the cart item with the given rowId.
     *
     * @param string $rowId
     * @param mixed  $quantity
     * @return \Gloudemans\Shoppingcart\CartItem
     */
    public function update($rowId, $quantity)
    {
        $cartItem = $this->get($rowId);

        if ($quantity instanceof Buyable) {
            $cartItem->updateFromBuyable($quantity);
        } elseif (is_array($quantity)) {
            $cartItem->updateFromArray($quantity);
        } else {
            $cartItem->quantity = $quantity;
        }

        $content = $this->getContent();

        if ($rowId !== $cartItem->rowId) {
            $content->pull($rowId);

            if ($content->has($cartItem->rowId)) {
                $existingCartItem = $this->get($cartItem->rowId);
                $cartItem->setQuantity($existingCartItem->quantity + $cartItem->quantity);
            }
        }

        if ($cartItem->quantity <= 0) {
            $this->remove($cartItem->rowId);
            return;
        } else {
            $content->put($cartItem->rowId, $cartItem);
        }

        $this->session->put($this->instance, $content);

        CartUpdatedItemEvent::dispatch($cartItem, $this->instance);

        return $cartItem;
    }

    /**
     * Remove the cart item with the given rowId from the cart.
     *
     * @param string $rowId
     * @return void
     */
    public function remove($rowId)
    {
        $cartItem = $this->get($rowId);

        $content = $this->getContent();

        $content->pull($cartItem->rowId);

        $this->session->put($this->instance, $content);

        CartRemovedItemEvent::dispatch($cartItem, $this->instance);
    }

    /**
     * Xóa toàn bộ sản phẩm được customer chọn
     */
    public function removeSelected()
    {
        $content = $this->getContent();

        $content = $content->filter(function ($item) {
            return $item->selected == false;
        });

        $this->session->put($this->instance, $content);
    }

    /**
     * Associate the cart item with the given rowId with the given model.
     *
     * @param string $rowId
     * @param mixed  $model
     * @return void
     */
    public function associate($rowId, $model)
    {
        if (is_string($model) && !class_exists($model)) {
            throw new UnknownModelException("The supplied model {$model} does not exist.");
        }

        $cartItem = $this->get($rowId);

        $cartItem->associate($model);

        $content = $this->getContent();

        $content->put($cartItem->rowId, $cartItem);

        $this->session->put($this->instance, $content);
    }

    /**
     * Set the tax rate for the cart item with the given rowId.
     *
     * @param string    $rowId
     * @param int|float $taxRate
     * @return void
     */
    public function setTax($rowId, $taxRate)
    {
        $cartItem = $this->get($rowId);

        $cartItem->setTaxRate($taxRate);

        $content = $this->getContent();

        $content->put($cartItem->rowId, $cartItem);

        $this->session->put($this->instance, $content);
    }

    /**
     * Set the tax rate for the cart item with the given rowId.
     *
     * @param string    $rowId
     * @param int|float $taxRate
     * @return void
     */
    public function setSelected($rowId, bool $bool = true)
    {
        $content = $this->getContent();

        if (is_array($rowId)) {
            foreach ($rowId as $item) {
                $cartItem = $this->get($item['rowId']);
                $cartItem->setSelected($item['selected']);

                $content->put($cartItem->rowId, $cartItem);
            }
        } else {
            $cartItem = $this->get($rowId);

            $cartItem->setSelected($bool);

            $content->put($cartItem->rowId, $cartItem);
        }

        $this->session->put($this->instance, $content);
    }

    /**
     * Create a new CartItem from the supplied attributes.
     *
     * @param mixed     $id
     * @param mixed     $name
     * @param int|float $quantity
     * @param float     $price
     * @param array     $options
     * @return \Gloudemans\Shoppingcart\CartItem
     */
    private function createCartItem($id, $name, $quantity, $price, array $options)
    {
        if ($id instanceof Buyable) {
            $cartItem = CartItem::fromBuyable($id, $quantity ?: []);
            $cartItem->setQuantity($name ?: 1);
            $cartItem->associate($id);
        } elseif (is_array($id)) {
            $cartItem = CartItem::fromArray($id);
            $cartItem->setQuantity($id['quantity']);
        } else {
            $cartItem = CartItem::fromAttributes($id, $name, $price, $options);
            $cartItem->setQuantity($quantity);
        }

        $cartItem->setTaxRate(config('shoppingcart-container.tax'));

        return $cartItem;
    }

    /**
     * Destroy the current cart instance.
     *
     * @return void
     */
    public function destroy()
    {
        $this->session->remove($this->instance);
        $this->session->remove($this->instance . $this->suffixCoupon);
    }
}