<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 10:07:07
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-19 20:14:02
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Bussiness\Traits;

use App\Containers\Settings\Enums\DeliveryTypeStatus;
use App\Containers\Settings\Models\DeliveryType;
use App\Containers\Settings\Models\PaymentType;
use App\Containers\Settings\Tasks\DeliveryType\DeliveryTypeListingTask;
use Illuminate\Support\Collection;

trait DeliveryAndPaymentTrait
{
    protected $suffixDelivery = '_delivery';

    protected function getDeliveryContent()
    {
        $content = $this->session->has($this->instance . $this->suffixDelivery)
            ? $this->session->get($this->instance . $this->suffixDelivery)
            : new Collection();

        return $content;
    }

    public function setDelivery(DeliveryType $deliveryType)
    {
        $deliveryContent = new Collection();

        $deliveryContent->put($deliveryType->id, $deliveryType->id);

        $this->session->put($this->instance . $this->suffixDelivery, $deliveryContent);

        return $deliveryType;
    }

    public function calcDeliveryFee()
    {
        $deliveryContent = $this->getDeliveryContent();
        
        $deliveries = app(DeliveryTypeListingTask::class)
            ->withDescription($this->currentLang->language_id)
            ->filters(['ids' => $deliveryContent->toArray(),'status' => DeliveryTypeStatus::ACTIVE])
            ->run(true);

        $value = 0;
        foreach ($deliveries as $delivery) {
            $value += $delivery->shipping_fee;
        }

        return [
            'value' => $value,
            'delivery_type' => !empty($deliveries) ? $deliveries : collect(),
        ];
    }

    public function setPaymentType(PaymentType $paymentType)
    {
    }
}
