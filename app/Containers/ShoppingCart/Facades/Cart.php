<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-09 16:13:42
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-05 10:38:38
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Facades;

use Illuminate\Support\Facades\Facade;

class Cart extends Facade {
    protected static function getFacadeAccessor()
    {
        return 'Cart';
    }
}
