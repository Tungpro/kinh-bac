<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 11:35:28
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-19 20:18:02
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\ShoppingCart\Enums\CartInstance;
use App\Containers\ShoppingCart\Facades\Cart;
use App\Ship\Parents\Actions\Action;

class BaseCartAction extends Action
{
    protected $identify, $currentLang = null;

    public function run()
    {
        return Cart::content();
    }

    // Hàm này batw buộc phải được gọi trước khi setup instance
    public function currentLang(?Language $currentLang): self
    {
        $this->currentLang = $currentLang;
        Cart::currentLang($this->currentLang);
        return $this;
    }

    // khởi tạo instance Shopping cart
    public function instance(string $instance = CartInstance::DEFAULT_INSTANCE): self
    {
        Cart::instance(app()->getLocale() . '_' . $instance);
        return $this;
    }

    // Định danh cho đối tượng (Customer, ... ) gắn liền với shopping cart
    public function setIdentify(string $identify): self
    {
        $this->identify = $identify;

        return $this;
    }
}
