<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 11:35:28
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-29 15:35:37
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Actions\FrontEnd;

use Apiato\Core\Traits\ResponseTrait;
use Apiato\Core\Transformers\Serializers\PureArraySerializer;
use App\Containers\Discount\UI\API\Transformers\FrontEnd\ListCouponCodeTransfomer;
use App\Containers\Settings\UI\API\Transformers\FrontEnd\DeliveryAndPayment\DeliveryMethodsTransfomer;
use App\Containers\ShoppingCart\Facades\Cart;

class GetContentCartAction extends BaseCartAction
{
    use ResponseTrait;

    protected $onlySelected = false, $isPureContent = false, $isRefresh = false;

    public function run()
    {
        $cart = Cart::content($this->onlySelected,$this->isRefresh, $this->isPureContent);

        if (!$this->isPureContent) {
            if (isset($cart['delivery']['delivery_type']) && !empty($cart['delivery']['delivery_type'])) {
                $cart['delivery']['delivery_type'] = $this->transform($cart['delivery']['delivery_type'], new DeliveryMethodsTransfomer(), [], [], 'delivery-methods', new PureArraySerializer, false);
            }

            if (isset($cart['coupon']['coupons']) && !empty($cart['coupon']['coupons'])) {
                $cart['coupon']['coupons'] = $this->transform($cart['coupon']['coupons'], new ListCouponCodeTransfomer(), [], [], 'coupons', new PureArraySerializer, false);
            }
        }

        return $cart;
    }

    // chỉ trả về danh sách sản phẩm đã được chọn bởi customer
    public function onlySelected(bool $onlySelected = true): self
    {
        $this->onlySelected = $onlySelected;
        return $this;
    }

    public function pureContent(): self
    {
        $this->isPureContent = true;
        return $this;
    }

    public function refreshCart(): self
    {
        $this->isRefresh = true;
        return $this;
    }
}
