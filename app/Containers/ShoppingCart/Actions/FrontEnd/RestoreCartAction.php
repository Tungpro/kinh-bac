<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 11:35:28
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-15 18:47:26
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Actions\FrontEnd;

use App\Containers\ShoppingCart\Facades\Cart;

class RestoreCartAction extends BaseCartAction
{
    public function run()
    {
        if ($this->identify) {
            Cart::restore($this->identify);
        }
    }
}
