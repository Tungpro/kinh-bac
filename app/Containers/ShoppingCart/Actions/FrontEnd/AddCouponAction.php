<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 11:35:28
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-27 11:12:32
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Actions\FrontEnd;

use App\Containers\Discount\Models\DiscountCode;
use App\Containers\ShoppingCart\Facades\Cart;

class AddCouponAction extends BaseCartAction
{
    protected $couponItems;

    public function run()
    {
        $cart = Cart::addCoupon($this->couponItems);

        return $cart;
    }

    public function addCoupon(DiscountCode $discountCode): self
    {
        $this->couponItems = $discountCode;
        return $this;
    }
}
