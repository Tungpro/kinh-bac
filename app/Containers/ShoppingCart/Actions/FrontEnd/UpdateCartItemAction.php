<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 11:35:28
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-15 20:02:00
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Actions\FrontEnd;

use App\Containers\ShoppingCart\Facades\Cart;

class UpdateCartItemAction extends BaseCartAction
{
    protected $quantity = 1, $rowId = '';

    public function run()
    {
        Cart::update($this->rowId, $this->quantity);

        return Cart::content();
    }

    public function quantity(int $quantity): self
    {
        $this->quantity = $quantity > 0 ? $quantity : 1;
        return $this;
    }

    public function rowId(string $rowId): self
    {
        $this->rowId = $rowId;
        return $this;
    }
}
