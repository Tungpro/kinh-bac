<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 11:35:28
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-15 22:47:53
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Actions\FrontEnd;

use App\Containers\ShoppingCart\Facades\Cart;

class SetSelectedCartItemAction extends BaseCartAction
{
    public function selected(array $selected) : self {
        Cart::setSelected($selected);
        return $this;
    }
}
