<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 11:35:28
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-19 17:40:48
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Actions\FrontEnd;

use App\Containers\Discount\Models\DiscountCode;
use App\Containers\ShoppingCart\Facades\Cart;

class RemoveCouponAction extends BaseCartAction
{
    protected $couponItems;

    public function run()
    {
        return Cart::removeCoupon($this->couponItems);
    }

    public function removeCoupon(DiscountCode $discountCode): self
    {
        $this->couponItems = $discountCode;
        return $this;
    }
}
