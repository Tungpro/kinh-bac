<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 11:35:28
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-15 19:05:49
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Actions\FrontEnd;

use App\Containers\ShoppingCart\Facades\Cart;

class AddCartItemAction extends BaseCartAction
{
    protected $cartItem = [];

    public function run()
    {
        return $this->cartItem;
    }

    public function add(array $item) : self {
        $this->cartItem = Cart::add($item);
        return $this;
    }
}
