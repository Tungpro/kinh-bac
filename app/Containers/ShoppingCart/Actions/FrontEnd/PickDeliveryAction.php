<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 11:35:28
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-19 17:06:31
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Actions\FrontEnd;

use App\Containers\Settings\Models\DeliveryType;
use App\Containers\ShoppingCart\Facades\Cart;

class PickDeliveryAction extends BaseCartAction
{
    protected $delivery = [];

    public function run()
    {
        return Cart::setDelivery($this->delivery);
    }

    public function pick(DeliveryType $delivery) : self {
        $this->delivery = $delivery;
        return $this;
    }
}
