<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-12 11:35:28
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-18 14:42:25
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Actions\FrontEnd;

use App\Containers\ShoppingCart\Facades\Cart;

class DestroyCartAction extends BaseCartAction
{
    public function run() : void
    {
        Cart::destroy();
    }
}
