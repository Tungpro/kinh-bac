<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-09 16:13:28
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-07-12 09:54:35
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Exceptions;

use RuntimeException;

class CartAlreadyStoredException extends RuntimeException {}