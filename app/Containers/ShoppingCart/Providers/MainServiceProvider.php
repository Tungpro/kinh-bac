<?php

namespace App\Containers\ShoppingCart\Providers;

use App\Containers\ShoppingCart\Bussiness\Cart;
use App\Ship\Parents\Providers\MainProvider;
use Illuminate\Auth\Events\Logout;
use Illuminate\Session\SessionManager;

/**
 * Class MainServiceProvider.
 *
 * The Main Service Provider of this container, it will be automatically registered in the framework.
 */
class MainServiceProvider extends MainProvider
{

    /**
     * Container Service Providers.
     *
     * @var array
     */
    public $serviceProviders = [
        // InternalServiceProviderExample::class,
        EventServiceProvider::class
    ];

    /**
     * Container Aliases
     *
     * @var  array
     */
    public $aliases = [
        // 'Foo' => Bar::class,
    ];

    public function boot() {
        parent::boot();
    }

    /**
     * Register anything in the container.
     */
    public function register()
    {
        parent::register();

        // $this->app->bind('Cart', Cart::class);

        $this->app->alias(Cart::class, 'Cart');

        $this->app['events']->listen(Logout::class, function () {
            if ($this->app['config']->get('shoppingcart-container.destroy_on_logout')) {
                $this->app->make(SessionManager::class)->forget('cart');
            }
        });
    }
}
