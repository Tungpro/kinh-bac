<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-13 13:33:42
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-07-13 16:33:57
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Providers;

use App\Containers\ShoppingCart\Events\CartAddItemEvent;
use App\Containers\ShoppingCart\Events\CartInitEvent;
use App\Containers\ShoppingCart\Events\CartRemovedItemEvent;
use App\Containers\ShoppingCart\Events\CartRestoredEvent;
use App\Containers\ShoppingCart\Events\CartStoredEvent;
use App\Containers\ShoppingCart\Events\CartUpdatedItemEvent;
use App\Containers\ShoppingCart\Events\Handlers\CartAddItemEventHandler;
use App\Containers\ShoppingCart\Events\Handlers\CartInitEventHandler;
use App\Containers\ShoppingCart\Events\Handlers\CartRemovedItemEventHandler;
use App\Containers\ShoppingCart\Events\Handlers\CartRestoredEventHandler;
use App\Containers\ShoppingCart\Events\Handlers\CartStoredEventHandler;
use App\Containers\ShoppingCart\Events\Handlers\CartUpdatedItemEventHandler;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        CartInitEvent::class => [
            CartInitEventHandler::class
        ],
        CartAddItemEvent::class => [
            CartAddItemEventHandler::class
        ],
        CartUpdatedItemEvent::class => [
            CartUpdatedItemEventHandler::class
        ],
        CartRemovedItemEvent::class => [
            CartRemovedItemEventHandler::class
        ],
        CartStoredEvent::class => [
            CartStoredEventHandler::class
        ],
        CartRestoredEvent::class => [
            CartRestoredEventHandler::class
        ]
    ];

    public function boot() {
        parent::boot();
    }
}
