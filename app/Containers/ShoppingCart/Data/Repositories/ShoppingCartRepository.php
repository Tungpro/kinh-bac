<?php

namespace App\Containers\ShoppingCart\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ShoppingCartRepository
 */
class ShoppingCartRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
