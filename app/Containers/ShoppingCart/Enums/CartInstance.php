<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-14 10:53:33
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class CartInstance extends BaseEnum
{
    const DEFAULT_INSTANCE = 'default';
}
