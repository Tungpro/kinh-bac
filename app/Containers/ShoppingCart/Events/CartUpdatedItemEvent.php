<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-13 13:34:52
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-14 16:45:52
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Events;

use App\Containers\ShoppingCart\Bussiness\CartItem;
use App\Ship\Parents\Events\Event;
use Illuminate\Queue\SerializesModels;


class CartUpdatedItemEvent extends Event
{
    use SerializesModels;

    public $item, $instance;

    public function __construct(CartItem $item, string $instance)
    {
        $this->item = $item;
        $this->instance = $instance;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
