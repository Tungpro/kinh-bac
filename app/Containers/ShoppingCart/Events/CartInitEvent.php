<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-13 13:34:52
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-07-13 16:34:20
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Events;

use App\Ship\Parents\Events\Event;
use Illuminate\Queue\SerializesModels;


class CartInitEvent extends Event
{
    use SerializesModels;

    public function __construct()
    {
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
