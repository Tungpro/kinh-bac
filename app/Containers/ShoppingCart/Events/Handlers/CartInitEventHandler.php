<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-13 13:34:47
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-19 20:10:00
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Events\Handlers;

use App\Containers\Localization\Actions\GetCurrentLangAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\RestoreCartAction;
use App\Containers\ShoppingCart\Events\CartInitEvent;

class CartInitEventHandler
{
    public function __construct()
    {
    }

    public function handle(CartInitEvent $event)
    {
        // dd(app(GetCurrentLangAction::class)->run());
        if (auth()->guard(config('auth.guard_for.frontend'))->check()) {
            app(RestoreCartAction::class)->currentLang(app(GetCurrentLangAction::class)->run())->instance()->setIdentify(auth()->guard(config('auth.guard_for.frontend'))->id())->run();
        }
    }
}
