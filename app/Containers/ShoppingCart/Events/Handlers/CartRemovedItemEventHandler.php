<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-13 13:34:47
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-14 10:19:36
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Events\Handlers;

use App\Containers\ShoppingCart\Events\CartRemovedItemEvent;
use App\Containers\ShoppingCart\Facades\Cart;

class CartRemovedItemEventHandler
{
    public function __construct()
    {
    }

    public function handle(CartRemovedItemEvent $event)
    {
        if (\Auth::guard('customer')->check()) {
            $customer_id = \Auth::guard('customer')->id();

            Cart::store($customer_id);
        }
    }
}
