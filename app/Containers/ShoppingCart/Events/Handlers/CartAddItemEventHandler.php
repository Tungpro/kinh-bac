<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-13 13:34:47
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-14 17:09:08
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Events\Handlers;

use App\Containers\ShoppingCart\Events\CartAddItemEvent;
use App\Containers\ShoppingCart\Facades\Cart;

class CartAddItemEventHandler
{
    public function __construct()
    {
    }

    public function handle(CartAddItemEvent $event)
    {
        if (\Auth::guard('customer')->check()) {
            // $customer_id = \Auth::guard('customer')->id();
            // dd(Cart::instance($event->instance)->content());
            // Cart::instance($event->instance)->store($customer_id);
        }
    }
}
