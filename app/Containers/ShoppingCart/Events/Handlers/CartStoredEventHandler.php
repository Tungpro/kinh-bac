<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-13 13:34:47
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-14 16:56:25
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Events\Handlers;

use App\Containers\ShoppingCart\Events\CartStoredEvent;
use App\Containers\ShoppingCart\Facades\Cart;

class CartStoredEventHandler
{
    public function __construct()
    {
    }

    public function handle(CartStoredEvent $event)
    {
        
    }
}
