<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-13 13:34:47
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-07-13 16:21:44
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\Events\Handlers;

use App\Containers\ShoppingCart\Events\CartRestoredEvent;

class CartRestoredEventHandler
{
    public function __construct()
    {
    }

    public function handle(CartRestoredEvent $event)
    {
    }
}
