<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-13 21:47:12
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 18:18:04
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features;

use Apiato\Core\Transformers\Serializers\PureArraySerializer;
use App\Containers\Settings\Actions\DeliveryType\Gets\GetDeliveryTypeByIdAction;
use App\Containers\Settings\UI\API\Transformers\FrontEnd\DeliveryAndPayment\DeliveryMethodsTransfomer;
use App\Containers\ShoppingCart\Actions\FrontEnd\GetContentCartAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\PickDeliveryAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\StoreCartAction;
use App\Containers\ShoppingCart\UI\API\Requests\FrontEnd\PickDeliveryTypeRequest;

trait PickDeliveryType
{
    public function pickDeliveryType(PickDeliveryTypeRequest $request)
    {
        $delivery = app(GetDeliveryTypeByIdAction::class)->run($request->id,$this->currentLang);

        if(!empty($delivery)) {
            app(PickDeliveryAction::class)->currentLang($this->currentLang)->instance()->pick($delivery)->run();

            app(StoreCartAction::class)->currentLang($this->currentLang)->instance()->setIdentify($this->user->id)->run();
        }

        $cart = app(GetContentCartAction::class)->instance()->currentLang($this->currentLang);
        
        // if(isset($request->selected)) {
            $cart->onlySelected();
        // };

        $cart = $cart->run();

        return $cart;
    }
}
