<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-13 21:47:12
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-27 11:47:27
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Discount\Actions\Getters\GetCodeByCodeAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\AddCouponAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\GetContentCartAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\StoreCartAction;
use App\Containers\ShoppingCart\UI\API\Requests\FrontEnd\ApplyDiscountCodeRequest;
use Illuminate\Http\Response;

trait ApplyDiscountCode
{
    public function applyDiscountCode(ApplyDiscountCodeRequest $request)
    {
        $code = app(GetCodeByCodeAction::class)->run($request->code);

        if(!empty($code)) {
            $coupon = app(AddCouponAction::class)->currentLang($this->currentLang)->instance()->addCoupon($code)->run();
            app(StoreCartAction::class)->currentLang($this->currentLang)->instance()->setIdentify($this->user->id)->run();
        }

        $cart = app(GetContentCartAction::class)->instance()->currentLang($this->currentLang);
        
        // if(isset($request->selected)) {
            $cart->onlySelected();
        // };

        $cart = $cart->run();

        if(!isset($cart['coupon']['coupons']) || empty($cart['coupon']['coupons']) || empty($cart['coupon']['value'])) {
            return $this->sendError('Not Acceptable',Response::HTTP_NOT_ACCEPTABLE,__('site.khongdudienkienapdungmgg'));
        }

        return $cart;
    }
}
