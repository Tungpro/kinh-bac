<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-13 21:47:12
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-19 20:00:16
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Product\Actions\ProductVariant\CheckStockAction;
use App\Containers\Product\Actions\ProductVariant\GetVariantByIdAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\StoreCartAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\UpdateCartItemAction;
use App\Containers\ShoppingCart\UI\API\Requests\FrontEnd\UpdateCartItemRequest;
use Illuminate\Http\Response;

trait UpdateCartItem
{
    public function updateCartItem(UpdateCartItemRequest $request)
    {
        $variant = app(GetVariantByIdAction::class)->run($request->productId, $request->variantId);

        if (!$stock = app(CheckStockAction::class)->quanNeedle($request->quantity)->run($variant)) {
            return $this->sendError('', Response::HTTP_NOT_ACCEPTABLE, 'Không đủ số lượng hàng',);
        }

        $cart = app(UpdateCartItemAction::class)->currentLang($this->currentLang)->instance()->rowId($request->itemId)->quantity($request->quantity)->run();

        if($this->user) {
            app(StoreCartAction::class)->currentLang($this->currentLang)->instance()->setIdentify($this->user->id)->run();
        }

        return $cart;
    }
}
