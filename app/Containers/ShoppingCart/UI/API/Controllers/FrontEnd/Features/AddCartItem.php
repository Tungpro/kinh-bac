<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-13 21:47:12
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-01 17:30:14
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Product\Actions\FrontEnd\PreCheckingToAddCartItemAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\AddCartItemAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\GetContentCartAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\StoreCartAction;
use App\Containers\ShoppingCart\UI\API\Requests\FrontEnd\AddCartItemRequest;
use Illuminate\Http\Response;

trait AddCartItem
{
    public function addCartItem(AddCartItemRequest $request)
    {
        $item = app(PreCheckingToAddCartItemAction::class)->currentLang($this->currentLang)->run(
            $request->product_id,
            $request->product_variant_id,
            $request->product_quantity
        );

        if(isset($item['error_code'])) {
            return $this->sendError('', $item['error_code'], $item['message']);
        }

        $item = app(AddCartItemAction::class)->currentLang($this->currentLang)->instance()->add($item)->run();

        if ($item && $this->user) {
            app(StoreCartAction::class)->currentLang($this->currentLang)->instance()->setIdentify($this->user->id)->run();
        }

        $cart = app(GetContentCartAction::class)->currentLang($this->currentLang)->instance()->run();

        return $this->sendResponse([
            'item' => $item,
            'cart' => [
                'count' => $cart['count'],
                'count_selected' => $cart['count_selected'],
            ],
            'html' => view('basecontainer::frontend.pc.components.includes.header.partials.cart-content-header', ['cart' => $cart])->render()
        ], 'success', Response::HTTP_OK);
    }
}
