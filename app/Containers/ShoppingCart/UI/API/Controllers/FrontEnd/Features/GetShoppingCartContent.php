<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-13 21:47:12
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-01 16:15:31
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features;

use App\Containers\ShoppingCart\Actions\FrontEnd\GetContentCartAction;
use App\Containers\ShoppingCart\UI\API\Requests\FrontEnd\GetShoppingCartContentRequest;

trait GetShoppingCartContent
{
    public function getShoppingCartContent(GetShoppingCartContentRequest $request)
    {
        $cart = app(GetContentCartAction::class)
            ->instance()
            ->currentLang($this->currentLang)
            ->refreshCart();

        // dd(Cart::content());
        if (isset($request->selected)) {
            $cart->onlySelected();
        }

        $cart = $cart->run();

        return $cart;
    }
}
