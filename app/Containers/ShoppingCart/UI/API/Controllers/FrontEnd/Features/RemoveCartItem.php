<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-13 21:47:12
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-19 19:59:55
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features;

use App\Containers\ShoppingCart\Actions\FrontEnd\RemoveItemAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\StoreCartAction;
use App\Containers\ShoppingCart\UI\API\Requests\FrontEnd\RemoveCartItemRequest;

trait RemoveCartItem
{
    public function removeCartItem(RemoveCartItemRequest $request)
    {
        $cart = app(RemoveItemAction::class)->currentLang($this->currentLang)->instance()->remove($request->itemId)->run();

        if($this->user) {
            app(StoreCartAction::class)->currentLang($this->currentLang)->instance()->setIdentify($this->user->id)->run();
        }
        
        return $cart;
    }
}
