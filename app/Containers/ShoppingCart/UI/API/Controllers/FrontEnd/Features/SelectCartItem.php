<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-13 21:47:12
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-19 20:00:02
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features;

use App\Containers\ShoppingCart\Actions\FrontEnd\SetSelectedCartItemAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\StoreCartAction;
use App\Containers\ShoppingCart\UI\API\Requests\FrontEnd\SelectCartItemRequest;

trait SelectCartItem
{
    public function selectCartItem(SelectCartItemRequest $request)
    {
        $selected = [];
        foreach($request->items as $item) {
            $selected[] = [
                'rowId' => $item['itemId'],
                'selected' => (bool)$item['selected']
            ];
        }
        $cart = app(SetSelectedCartItemAction::class)->currentLang($this->currentLang)->instance()->selected($selected)->run();

        if($this->user) {
            app(StoreCartAction::class)->currentLang($this->currentLang)->instance()->setIdentify($this->user->id)->run();
        }
        
        return $cart;
    }
}
