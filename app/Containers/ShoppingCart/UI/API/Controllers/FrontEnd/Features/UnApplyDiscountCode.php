<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-13 21:47:12
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-26 15:43:45
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Discount\Actions\Getters\GetCodeByCodeAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\GetContentCartAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\RemoveCouponAction;
use App\Containers\ShoppingCart\Actions\FrontEnd\StoreCartAction;
use App\Containers\ShoppingCart\UI\API\Requests\FrontEnd\UnApplyDiscountCodeRequest;

trait UnApplyDiscountCode
{
    public function unApplyDiscountCode(UnApplyDiscountCodeRequest $request)
    {
        $code = app(GetCodeByCodeAction::class)->run($request->code);

        if(!empty($code)) {
            $coupon = app(RemoveCouponAction::class)->currentLang($this->currentLang)->instance()->removeCoupon($code)->run();
            app(StoreCartAction::class)->currentLang($this->currentLang)->instance()->setIdentify($this->user->id)->run();
        }

        $cart = app(GetContentCartAction::class)->instance()->currentLang($this->currentLang);
        
        if(isset($request->selected)) {
            $cart->onlySelected();
        };

        $cart = $cart->run();

        return $cart;
    }
}
