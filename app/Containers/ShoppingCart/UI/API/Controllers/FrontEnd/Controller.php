<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-09 16:10:42
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-19 17:39:42
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd;

use App\Containers\BaseContainer\UI\WEB\Controllers\BaseApiFrontController;
use App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features\AddCartItem;
use App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features\ApplyDiscountCode;
use App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features\GetShoppingCartContent;
use App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features\PickDeliveryType;
use App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features\PickPaymentType;
use App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features\RemoveCartItem;
use App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features\SelectCartItem;
use App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features\UnApplyDiscountCode;
use App\Containers\ShoppingCart\UI\API\Controllers\FrontEnd\Features\UpdateCartItem;

class Controller extends BaseApiFrontController
{
    use GetShoppingCartContent,
        AddCartItem,
        UpdateCartItem,
        RemoveCartItem,
        SelectCartItem,
        ApplyDiscountCode,
        UnApplyDiscountCode,
        PickDeliveryType,
        PickPaymentType;

    public function __construct()
    {
        parent::__construct();
    }
}
