<?php

/**
 * Created by PhpStorm.
 * Filename: PointTypesRules.php
 * User: Thang Nguyen Nhan
 * Date: 6/24/20
 * Time: 17:43
 */

namespace App\Containers\ShoppingCart\UI\API\Rules\FrontEnd;

use App\Containers\Product\Enums\ProductStatus;
use App\Containers\Product\Enums\ProductVariantStatus;
use App\Containers\Product\Models\Product;
use App\Containers\Product\Models\ProductVariant;
use Illuminate\Contracts\Validation\Rule;

class SelectCartItemRules implements Rule
{
    protected $msg = '';
    protected $items;

    protected $mustHave = [
        'variantId','itemId','productId','selected'
    ];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($items)
    {
        $this->items = $items;
        sort($this->mustHave);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        foreach($this->items as $item) {
            $sortArr = array_keys($item);
            sort($sortArr);
            if(!in_array($sortArr,[$this->mustHave]) || !($this->existsProduct($item['productId']) && $this->existsVariant($item['variantId'],$item['productId'])) ) {
                return false;
            }
        }

        return true;
    }

    private function existsProduct(int $productId) {
        return Product::where('id',$productId)->where('status', ProductStatus::ACTIVE)->exists();
    }

    private function existsVariant(int $variantId,int $productId) {
        return ProductVariant::where('product_variant_id',$variantId)->where('product_id',$productId)->where('status', ProductVariantStatus::ON_SALE)->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Sản phẩm đã hết hàng';
    }
}
