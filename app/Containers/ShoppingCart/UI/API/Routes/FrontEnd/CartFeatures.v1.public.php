<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-04 16:55:35
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-19 17:39:53
 * @ Description: Happy Coding!
 */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

Route::group(
[
    'middleware' => [
        // 'web'
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run().'/',
],
function () use ($router) {
    $router->group([
        'prefix' => 'cart/mine'
    ], function() use($router) {
        $router->any('/info', [
            'as' => 'api_mine_cart_infor',
            'uses'       => 'FrontEnd\Controller@listingProduct'
        ]);

        $router->put('/coupons/{code?}', [
            'as' => 'api_mine_cart_coupon',
            'uses'       => 'FrontEnd\Controller@applyDiscountCode'
        ])->where([
            'code' => '[a-zA-Z0-9]+'
        ]);

        $router->delete('/coupons/{code?}', [
            'as' => 'api_mine_cart_coupon',
            'uses'       => 'FrontEnd\Controller@unApplyDiscountCode'
        ])->where([
            'code' => '[a-zA-Z0-9]+'
        ]);

        $router->post('/pickPaymentType', [
            'as' => 'api_mine_cart_pick_payment',
            'uses'       => 'FrontEnd\Controller@pickPaymentType'
        ]);

        $router->post('/pickDeliveryType', [
            'as' => 'api_mine_cart_pick_delivery',
            'uses'       => 'FrontEnd\Controller@pickDeliveryType'
        ]);
    });
    
    $router->group([
        'prefix' => 'intended_cart'
    ], function() use($router) {
        $router->any('/items', [
            'as' => 'api_itended_cart_items',
            'uses'       => 'FrontEnd\Controller@getShoppingCartContent'
        ]);
        $router->put('/update/{itemCartId?}', [
            'as' => 'api_itended_cart_update',
            'uses'       => 'FrontEnd\Controller@updateCartItem'
        ])->where([
            // 'itemCartId' => '[0-9]+'
            'itemCartId' => '[a-zA-Z0-9_\-]+'
        ]);
        $router->put('/remove/{itemCartId?}', [
            'as' => 'api_itended_cart_remove',
            'uses'       => 'FrontEnd\Controller@removeCartItem'
        ])->where([
            // 'itemCartId' => '[0-9]+'
            'itemCartId' => '[a-zA-Z0-9_\-]+'
        ]);
        $router->put('/select/{itemCartId?}', [
            'as' => 'api_itended_cart_select_item',
            'uses'       => 'FrontEnd\Controller@selectCartItem'
        ])->where([
            // 'itemCartId' => '[0-9]+'
            'itemCartId' => '[a-zA-Z0-9_\-]+'
        ]);
        $router->put('/add', [
            'as' => 'api_itended_cart_add',
            'uses'       => 'FrontEnd\Controller@addCartItem'
        ]);
    });
});