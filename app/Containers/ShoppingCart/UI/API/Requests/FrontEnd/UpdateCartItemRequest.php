<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-09 16:10:42
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-15 20:10:27
 * @ Description: Happy Coding!
 */


namespace App\Containers\ShoppingCart\UI\API\Requests\FrontEnd;

use App\Containers\Product\Enums\ProductStatus;
use App\Containers\Product\Models\Product;
use App\Containers\Product\Models\ProductVariant;
use App\Ship\Parents\Requests\Request;

class UpdateCartItemRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    // protected $transporter = \App\Ship\Transporters\DataTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        // 'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'productId' => ['required','exists:'.Product::getTableName().',id,status,'.ProductStatus::ACTIVE],
            'variantId' => ['required','exists:'.ProductVariant::getTableName().',product_variant_id,product_id,'.request()->productId]
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }
}
