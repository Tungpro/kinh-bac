<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-09 16:10:42
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-19 21:21:49
 * @ Description: Happy Coding!
 */


namespace App\Containers\ShoppingCart\UI\API\Requests\FrontEnd;

use App\Containers\ShoppingCart\UI\API\Rules\FrontEnd\SelectCartItemRules;

class SelectCartItemRequest extends UpdateCartItemRequest
{
    public function rules()
    {
        return [
            'items' => ['required',new SelectCartItemRules(request()->items)]
        ];
    }
}
