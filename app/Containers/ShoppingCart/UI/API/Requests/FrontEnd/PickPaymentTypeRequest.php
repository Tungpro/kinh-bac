<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-09 16:10:42
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-19 16:30:17
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\UI\API\Requests\FrontEnd;

use App\Containers\Settings\Enums\PaymentStatus;
use App\Containers\Settings\Models\PaymentType;
use App\Ship\Parents\Requests\Request;

class PickPaymentTypeRequest extends Request
{

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    // protected $transporter = \App\Ship\Transporters\DataTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        // 'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'id' => ['required', 'exists:' . PaymentType::getTableName() . ',id,status,' . PaymentStatus::ACTIVE]
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }
}
