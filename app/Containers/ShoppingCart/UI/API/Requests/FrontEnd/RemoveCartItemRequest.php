<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-09 16:10:42
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-15 20:10:14
 * @ Description: Happy Coding!
 */


namespace App\Containers\ShoppingCart\UI\API\Requests\FrontEnd;

class RemoveCartItemRequest extends UpdateCartItemRequest
{

}
