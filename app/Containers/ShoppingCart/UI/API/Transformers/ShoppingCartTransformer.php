<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-09 16:10:10
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-13 23:56:38
 * @ Description: Happy Coding!
 */

namespace App\Containers\ShoppingCart\UI\API\Transformers;

use App\Containers\Shoppingcart\Bussiness\CartItem;
use App\Ship\Parents\Transformers\Transformer;

class ShoppingCartTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected array $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected array $availableIncludes = [

    ];

    public function transform($entity)
    {
        // $response = [
        //     'object' => 'ShoppingCart',
        //     'id' => $entity->getHashedKey(),
        //     'created_at' => $entity->created_at,
        //     'updated_at' => $entity->updated_at,

        // ];

        // $response = $this->ifAdmin([
        //     'real_id'    => $entity->id,
        //     // 'deleted_at' => $entity->deleted_at,
        // ], $response);

        return $entity;
    }
}
