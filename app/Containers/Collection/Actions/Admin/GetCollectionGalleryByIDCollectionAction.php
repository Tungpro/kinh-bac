<?php

namespace App\Containers\Collection\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class GetProductGalleryAction.
 *
 */
class GetCollectionGalleryByIDCollectionAction extends Action
{

    /**
     * @return mixed
     */
    public function run(int $collection_id, $type = 'collection', $json = false)
    {
        return $this->call('Collection@Admin\GetCollectionGalleryByIDCollectionTask', [
            $collection_id,
            $type,
            $json
        ]);
    }
}
