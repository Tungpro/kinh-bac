<?php

namespace App\Containers\Collection\Actions\Admin;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllCollectionAction extends Action
{
    public function run($filters, $perPage = 15, $with = [])
    {
        return Apiato::call('Collection@Admin\GetAllCollectionTask', [$filters, $perPage], [
            ['with' => [$with]],
        ]);
    }
}
