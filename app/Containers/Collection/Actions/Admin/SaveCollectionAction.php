<?php

namespace App\Containers\Collection\Actions\Admin;

use App\Ship\Parents\Actions\Action;

class SaveCollectionAction extends Action
{
    public function run(array $data)
    {
        if (empty($data['object_id'])) {
            $object_id = $this->call('Collection@Admin\AddCollectionTask', [$data['content'], '']);
        } else {
            $object_id = $this->call('Collection@Admin\AddCollectionTask', [$data['content'], $data['object_id']]);
            $object_id = $data['object_id'];
        }
        if (!empty($data['collection'])) {
            $this->call('Collection@Admin\SaveCollectionDetailTask', [$data['collection'], $object_id]);
        }

        $this->call('Collection@Admin\SaveCollectionBannerTask', [$data['fileList'], $object_id]);

        if (!empty($data['content'])) {
            $this->call('Collection@Admin\SaveCollectionDescTask', [$data['content'], $object_id]);
        }
        if (!empty($data['general'])) {
            $this->call('Collection@Admin\SaveCollectionTask', [$data['general'], $object_id, $data]);
        }
        if (!empty($data['collectionTags'])) {
            $this->call('Tags@TagDetail\SaveTagDetailTask', [$data['collectionTags'], $object_id, 'collection', 'tag']);
        }
        $this->clearCache();

        return $object_id;
    }
}
