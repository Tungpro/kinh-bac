<?php
/**
 * Created by PhpStorm.
 * Filename: ProductDesc.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 7/23/20
 * Time: 10:38
 */

namespace App\Containers\Collection\Models;

use App\Ship\Parents\Models\Model;
use App\Containers\Localization\Models\Language;

class CollectionDesc extends Model
{
    protected $table = 'collection_description';
    protected $fillable = ['language_id'];
    public function collection()
    {
        return $this->hasOne(Collection::class,'id','collection_id');
    }

    public function language()
    {
        return $this->hasOne(Language::class,'language_id','language_id');
    }
}
