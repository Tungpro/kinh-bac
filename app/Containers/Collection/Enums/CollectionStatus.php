<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-30 17:06:35
 * @ Description: Happy Coding!
 */

namespace App\Containers\Collection\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class CollectionStatus extends BaseEnum
{
    const ACTIVE = 1;
    const DE_ACTIVE = 0;
}
