<?php

namespace App\Containers\Collection\UI\API\Controllers;

use App\Containers\Collection\UI\API\Requests\CreateCollectionRequest;
use App\Containers\Collection\UI\API\Requests\DeleteCollectionRequest;
use App\Containers\Collection\UI\API\Requests\GetAllCollectionsRequest;
use App\Containers\Collection\UI\API\Requests\FindCollectionByIdRequest;
use App\Containers\Collection\UI\API\Requests\UpdateCollectionRequest;
use App\Containers\Collection\UI\API\Transformers\CollectionTransformer;
use App\Ship\Parents\Controllers\ApiController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\Collection\UI\API\Controllers
 */
class Controller extends ApiController
{
    /**
     * @param CreateCollectionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCollection(CreateCollectionRequest $request)
    {
        $collection = Apiato::call('Collection@CreateCollectionAction', [$request]);

        return $this->created($this->transform($collection, CollectionTransformer::class));
    }

    /**
     * @param FindCollectionByIdRequest $request
     * @return array
     */
    public function findCollectionById(FindCollectionByIdRequest $request)
    {
        $collection = Apiato::call('Collection@FindCollectionByIdAction', [$request]);

        return $this->transform($collection, CollectionTransformer::class);
    }

    /**
     * @param GetAllCollectionsRequest $request
     * @return array
     */
    public function getAllCollections(GetAllCollectionsRequest $request)
    {
        $collections = Apiato::call('Collection@GetAllCollectionsAction', [$request]);

        return $this->transform($collections, CollectionTransformer::class);
    }

    /**
     * @param UpdateCollectionRequest $request
     * @return array
     */
    public function updateCollection(UpdateCollectionRequest $request)
    {
        $collection = Apiato::call('Collection@UpdateCollectionAction', [$request]);

        return $this->transform($collection, CollectionTransformer::class);
    }

    /**
     * @param DeleteCollectionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCollection(DeleteCollectionRequest $request)
    {
        Apiato::call('Collection@DeleteCollectionAction', [$request]);

        return $this->noContent();
    }
}
