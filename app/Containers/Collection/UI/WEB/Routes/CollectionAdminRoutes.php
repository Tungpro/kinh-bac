<?php
Route::group(
[
    'prefix' => 'collection',
    'namespace' => '\App\Containers\Collection\UI\WEB\Controllers\Admin',
    'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
    'middleware' => [
        'auth:admin',
    ],
],function () use ($router) {
    $router->get('/{abc}', [
        'as'   => 'admin_collection_home_page',
        'uses' => 'Controller@index',
    ]);
    $router->get('/', [
      'as'   => 'admin_collection_home_page',
      'uses' => 'Controller@index',
    ]);
    $router->get('/edit/{id}', [
        'as'   => 'admin_collection_edit_page',
        'uses' => 'Controller@edit',
    ]);

    $router->post('/edit/{id}', [
        'as'   => 'admin_collection_edit_page',
        'uses' => 'Controller@update',
    ]);

    $router->get('/add', [
        'as'   => 'admin_collection_add_page',
        'uses' => 'Controller@add',
    ]);

    $router->post('/add', [
        'as'   => 'admin_collection_add_page',
        'uses' => 'Controller@create',
    ]);

    $router->delete('/{id}', [
        'as'   => 'admin_collection_delete_page',
        'uses' => 'Controller@delete',
    ]);

    $router->get('/filter', [
        'as' => 'admin.collection.filter',
        'uses' => 'ProductDescController@filter',
    ]);

    $router->get('/collection', [
        'as' => 'admin.pdc.index',
        'uses' => 'ProductCollectionController@index',
    ]);

    $router->post('/uploadImgGallery', [
        'as'   => 'admin_collection_upload_img_gallery',
        'uses' => 'Controller@uploadImgGallery',
    ]);

    $router->post('/change-pos', [
        'as'   => 'admin_collection_change_pos_gallery',
        'uses' => 'Controller@ajaxItemChangePos',
    ]);

    $router->post('/remove_img', [
        'as'   => 'admin_collection_remove_img_gallery',
        'uses' => 'Controller@ajaxItemImgDel',
    ]);

    $router->get('/edit-prod-frame/{id}', [
        'as' => 'admin.product-edit-frame',
        'uses' => 'Controller@editProductFrame'
    ]);
});
