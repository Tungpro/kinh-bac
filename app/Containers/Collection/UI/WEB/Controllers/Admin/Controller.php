<?php

namespace App\Containers\Collection\UI\WEB\Controllers\Admin;

use App\Containers\Collection\UI\WEB\Requests\ImageUploadRequest;
use App\Containers\Collection\UI\WEB\Requests\BannerRequest;
use App\Containers\Collection\UI\WEB\Requests\ajaxCollectionSaveRequest;
use App\Containers\Collection\UI\WEB\Requests\ListCollectionRequest;
use App\Containers\Collection\UI\WEB\Requests\GetAllCollectionsRequest;
use App\Containers\Product\UI\WEB\Requests\AjaxFindProductByInput;
use App\Ship\Parents\Controllers\AdminController;
use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\StringLib;
use App\Containers\Collection\Models\CollectionImage;
use Illuminate\Support\Facades\View;

/**
 * Class Controller
 *
 * @package App\Containers\Collection\UI\WEB\Controllers
 */
class Controller extends AdminController
{
    /**
     * Show all entities
     */
    public function __construct()
    {
        $this->title = 'Quản trị Bộ sưu tập';
        parent::__construct();
    }

    public function index(GetAllCollectionsRequest $request)
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Bộ sưu tập', $this->form == 'list' ? '' : route('admin_collection_home_page', ['']));
        // $this->breadcrumb[] = FunctionLib::addBreadcrumb('', $this->form == 'list' ? '' : route('admin_collection_home_page',['']));
        View::share('breadcrumb', $this->breadcrumb);

        return view('collection::admin.index', [
            'input' => $request->all()
        ]);
    }

    public function ajaxSearchProduct(AjaxFindProductByInput $request)
    {
        $limit = 40;
        $data = Apiato::call('Product@Admin\FindProductByInputAction', [$request->queryString ?? '', $limit, ['name' => 'ASC']]);
        if (!empty($data))
            return FunctionLib::ajaxRespond(true, 'ok', $data);
        else
            return FunctionLib::ajaxRespond(false, 'Không tìm thấy sản phẩm');

    }

    public function ajaxImageUpload(ImageUploadRequest $request)
    {
        if ($request->hasFile('Filedata')) {
            $image = $request->file('Filedata');
            if ($image->isValid()) {
                $title = basename($image->getClientOriginalName(), '.' . $image->getClientOriginalExtension());
                $fname = Apiato::call('File@UploadImageAction', [$request, 'Filedata', $title, StringLib::getClassNameFromString(Collection::class)]);
                if (!empty($fname) && !$fname['error']) {
                    $imgGallery = new CollectionImage();
                    $imgGallery->object_id = $request->object_id;
                    $imgGallery->image = $fname['fileName'];
                    $imgGallery->created = time();
                    $imgGallery->type = 'collection';
                    //                    $imgGallery->changed = time();
                    // $imgGallery->user_id = \Auth::id();
                    //                    $imgGallery->uname = \Auth::user()->user_name;
                    //                    $imgGallery->lang = $request->lang;
                    // $imgGallery->sort = ProductImage::getSortInsert($request->lang);
                    $imgGallery->save();
                    if (empty($imgGallery->object_id)) {
                        return FunctionLib::ajaxRespond(true, 'ok', ['images' => Apiato::call('Collection@Admin\GetCollectionGalleryByIDCollectionAction', [$imgGallery->id])]);
                    } else {
                        return FunctionLib::ajaxRespond(true, 'ok', ['images' => Apiato::call('Collection@Admin\GetCollectionGalleryAction', [$request->object_id])]);
                    }
                }
                return FunctionLib::ajaxRespond(false, 'Upload ảnh thất bại!');
            }
            return FunctionLib::ajaxRespond(false, 'File không hợp lệ!');
        }
        return FunctionLib::ajaxRespond(false, 'Không tìm thấy ảnh!');
    }

    public function ajaxCollectionSave(ajaxCollectionSaveRequest $request)
    {
        $transporter = $request->toTransporter()->toArray();
        $collection = Apiato::call('Collection@Admin\SaveCollectionAction', [$transporter]);
        if (!empty($collection))
            return FunctionLib::ajaxRespond(true, 'Hoàn thành', $collection);
        else {
            return FunctionLib::ajaxRespond(false, 'Không thành công!');
        }
    }

    public function ajaxCollectionList(ListCollectionRequest $request)
    {
        $collection = Apiato::call('Collection@Admin\GetAllCollectionAction', [$request->fillters, 15, ['desc']]);
        if ($collection) {
            foreach ($collection as &$aCollection) {
                $aCollection->route_detail = $aCollection->routeCollectionDetail();
            }
        }

        //dd($collection->toArray());
        return FunctionLib::ajaxRespond(true, 'ok', $collection);
    }

    public function ajaxDeleteCollection(ListCollectionRequest $request)
    {
        if (isset($request->id)) {
            $collectionData = Apiato::call('Collection@Admin\DeleteCollectionByIdAction', [$request->id]);
            if (!empty($collectionData)) {
                return FunctionLib::ajaxRespond(true, 'Thành công', $collectionData);
            } else {
                return FunctionLib::ajaxRespond(false, 'Không tìm thấy Collection');
            }
        }
        return FunctionLib::ajaxRespond(false, 'Lỗi');
    }

    public function ajaxImgData(BannerRequest $request)
    {
        $transporter = $request->toTransporter();
        if ($transporter->id > 0) {
            $collectionImgs = Apiato::call('collection@Admin\GetCollectionGalleryAction', [$transporter->id]);
            if (!empty($collectionImgs)) {
                return FunctionLib::ajaxRespond(true, 'ok', $collectionImgs);
            }
            return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
        }
    }

    public function ajaxDataCollection(BannerRequest $request)
    {
        $transporter = $request->toTransporter();
        if ($transporter->id > 0) {
            $collection = Apiato::call('collection@Admin\GetCollectionPrdDataAction', [$transporter]);
            return FunctionLib::ajaxRespond(true, 'Thành công', $collection);

        }
        return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    public function ajaxCollectionID(BannerRequest $request)
    {
        $collection = [];
        if (isset($request->id)) {
            $collectionData = Apiato::call('Collection@Admin\GetCollectionByIdAction', [$request->id]);
            if (!empty($collectionData->all_desc)) {
                foreach ($collectionData->all_desc as $item) {
                    if (isset($item->language_id))
                        $collection[$item->language_id] = $item;
                }
                unset($collectionData['all_desc']);
                $collectionData['all_desc'] = $collection;
            } else {
                $langs = Apiato::call('Localization@GetAllLanguageDBAction', []);

            }
            if (!empty($collectionData)) {
                return FunctionLib::ajaxRespond(true, 'ok', $collectionData);
            } else {
                return FunctionLib::ajaxRespond(false, 'Không tìm thấy collection');
            }
        }
        return FunctionLib::ajaxRespond(false, 'Lỗi');
    }

    public function ajaxItemImgUpdate(GetAllCollectionsRequest $request)
    {
        $data = $request->editImage;
        if ($data['id'] > 0) {
            $cur = CollectionImage::find($data['id']);
            if ($cur) {
                $cur->sort = (int)$data['sort'];
                $cur->link = $data['link'];
                $cur->save();
                return FunctionLib::ajaxRespond(true, 'Hoàn thành', $cur);
            }
        }
        return FunctionLib::ajaxRespond(false, 'Dữ liệu không chính xác');
    }
}
