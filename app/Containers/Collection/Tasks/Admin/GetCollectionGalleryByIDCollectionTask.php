<?php

namespace App\Containers\Collection\Tasks\Admin;

use App\Containers\Collection\Data\Repositories\CollectionImageRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class GetProductGalleryTask.
 */
class GetCollectionGalleryByIDCollectionTask extends Task
{

    protected $repository;

    public function __construct(CollectionImageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($collection_id, $type = 'collection', $json = false)
    {
        $images = $this->repository->getModel()->where('type', $type)->where('id', $collection_id)->first();
        // $images = $this->repository->getModel()->where('type', $type)->where('object_id', $collection_id)->orderByRaw('sort desc,created desc')->get();
        $data = [];
        if(!empty($images)) {
                $tmp = $images->toArray();
                $tmp['img'] = $images->image;
                $tmp['image_sm'] = $images->getImageUrl('hotel_small');
                $tmp['image_md'] = $images->getImageUrl('hotel_preview');
                $tmp['image'] = $images->getImageUrl('hotel_large');
                $tmp['image_org'] = $images->getImageUrl();
                array_push($data, $tmp);
            
        }
        return $json ? json_encode($data) : $data;
    }
}
