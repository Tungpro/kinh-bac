<?php

namespace App\Containers\Collection\Tasks\Admin;

use App\Containers\Collection\Data\Repositories\CollectionRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class GetCollectionByIdTask extends Task
{
    protected $repository;

    public function __construct(CollectionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        } catch (Exception $exception) {
            throw new NotFoundException();
        }
    }

    public function withDescLang()
    {
        $this->currentLang = $this->currentLang->language_id ?? 1;

        $this->repository->with(['desc' => function ($query) {
            $query->activeLang($this->currentLang);
        }]);

        return $this;
    }
}
