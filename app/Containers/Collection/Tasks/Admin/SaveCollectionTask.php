<?php

namespace App\Containers\Collection\Tasks\Admin;

use App\Containers\Collection\Data\Repositories\CollectionRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Arr;

class SaveCollectionTask extends Task
{
    protected $repository;

    public function __construct(CollectionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($collection_data, $collection_id, $dataAll = [])
    {
        if (!empty($dataAll['collection'])) {
            $categoryIds = Arr::pluck($dataAll['collection'], 'categories.*.category_id');
            $categoryIds = Arr::collapse($categoryIds);
            $categoryIds = array_unique($categoryIds);
            $categoryIds = implode(',', $categoryIds);
        }
        if (!empty($collection_data)) {
            $this->repository->where('id', $collection_id)->update([
                'image' => $collection_data['image'],
                'is_good_price' => (int)$collection_data['is_good_price'],
                'status' => (int)$collection_data['status'],
                'color' => $collection_data['color'],
                'category_ids' => !empty($categoryIds) ? $categoryIds : null,
                'sort_order' => (int)$collection_data['sort_order'],
            ]);
        }
    }
}
