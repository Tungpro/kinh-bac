<?php

namespace App\Containers\Collection\Tasks\Admin;

use App\Containers\Collection\Data\Repositories\CollectionImageRepository;
use App\Ship\Parents\Tasks\Task;

class SaveCollectionBannerTask extends Task
{
    protected $repository;

    public function __construct(CollectionImageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($collection_data, $collection_id)
    {
        if (!empty($collection_data)) {
            $keyIsset = [];
            foreach ($collection_data as $key => $items) {
                $keyIsset[] = $items['id'];
                $data = [
                    'object_id' => $collection_id,
                    'type' => 'collection',
                    'created' => time(),
                    'sort' => $items['sort'] ?? 0
                ];
                $this->repository->where('image', $items['name'])->update($data);
            }
            $this->repository->where('object_id', $collection_id)->whereNotIn('id', $keyIsset)->delete();
        } else {
            // Nếu mà rỗng ảnh thì => xóa all => delete all
            $this->repository->where('object_id', $collection_id)->where('type', 'collection')->delete();
        }
        // return $collection->id;
    }
}
