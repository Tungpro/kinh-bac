<?php

namespace App\Containers\Collection\Tasks\Admin;

use App\Containers\Collection\Data\Repositories\CollectionRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

class GetAllCollectionTask extends Task
{
    protected $repository;

    public function __construct(CollectionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($filters, $perPage)
    {
        if (isset($filters['id'])) {
            $this->repository->pushCriteria(new ThisOperationThatCriteria('id', '%' . $filters['id'] . '%', 'like'));
        }
        $this->repository->orderBy('created', 'desc');
        $this->repository->with('all_desc');

        return $this->repository->whereHas(
            'all_desc', function (Builder $q) use ($filters) {
            if (isset($filters['title']) && !empty($filters['title'])) {
                $q->where(DB::raw('lower(name)'), 'like', '%' . strtolower($filters['title']) . '%');
            }
        }
        )->paginate($perPage);
    }
}
