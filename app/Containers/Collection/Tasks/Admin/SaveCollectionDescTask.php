<?php

namespace App\Containers\Collection\Tasks\Admin;

use Illuminate\Support\Str;
use App\Containers\Collection\Data\Repositories\CollectionDescRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Apiato\Core\Foundation\Facades\Apiato;

class SaveCollectionDescTask extends Task
{

  protected $repository;

  public function __construct(CollectionDescRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run(array $data, $id = '')
  {
    if (!empty($data)) {
      foreach ($data as $key => $item) {
        if (!empty($item) && isset($item['id'])) {
          $this->repository->getModel()->where('collection_id',  $id)->where('language_id', $item['language_id'])->update($item);
        } elseif (!empty($item) && !isset($item['id'])) {
          $item['collection_id'] = $id;
          $checkIsset =  $this->repository->getModel()->where('collection_id',  $id)->where('language_id', $item['language_id'])->first();
          if (empty($checkIsset))
            $this->repository->getModel()->insert($item);
          else {
            $this->repository->getModel()->where('collection_id',  $id)->where('language_id', $item['language_id'])->update($item);
          }
        }
      }
    }
  }
}
