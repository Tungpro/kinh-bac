<?php

namespace App\Containers\Collection\Tasks\Admin;

use App\Containers\Collection\Data\Repositories\CollectionDetailRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class GetProductGalleryTask.
 */
class SaveCollectionDetailTask extends Task
{

    protected $repository;

    public function __construct(CollectionDetailRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($collection_data,$collection_id)
    {   if(!empty($collection_id))
            $this->repository->where('collection_id',$collection_id)->delete();
        if(!empty($collection_data)){
            foreach($collection_data as $key => $items){
                $data=[
                    'collection_id' => $collection_id,
                    'object_id' => $key
                ];
                $this->repository->create($data);
            }
          
        }
        // return $collection->id;
    }
}
