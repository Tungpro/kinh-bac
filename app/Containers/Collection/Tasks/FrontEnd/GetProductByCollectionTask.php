<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-30 16:49:08
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-30 21:35:02
 * @ Description: Happy Coding!
 */

namespace App\Containers\Collection\Tasks\FrontEnd;

use App\Containers\Collection\Data\Repositories\CollectionDetailRepository;
use App\Containers\Collection\Data\Repositories\CollectionRepository;
use App\Containers\Collection\Enums\CollectionStatus;
use App\Ship\Criterias\Eloquent\SelectFieldsCriteria;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;

class GetProductByCollectionTask extends Task
{
    protected $repository;

    public function __construct(CollectionDetailRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $limit = 10)
    {
        $language_id = $this->currentLang ? $this->currentLang->language_id : 1;

        $this->repository->with([
            'product',
            'product.desc' => function ($query) use ($language_id) {
                $query->select('id','product_id', 'name', 'slug', 'short_description');
                $query->whereHas('language', function ($q) use ($language_id) {
                    $q->where('language_id', $language_id);
                });
            }
        ]);

        return $this->skipPagin ? ($limit == 0 ? $this->repository->all() : $this->repository->limit($limit)) : $this->repository->paginate($limit);
    }

    public function collectionId(int $id): self
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('collection_id', $id));
        return $this;
    }

    public function selectFields(array $column = ['*']): self
    {
        $this->repository->pushCriteria(new SelectFieldsCriteria($column));
        return $this;
    }
}
