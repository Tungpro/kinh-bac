<?php

namespace App\Containers\Collection\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class CollectionImageRepository.
 */
class CollectionImageRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Collection';
}
