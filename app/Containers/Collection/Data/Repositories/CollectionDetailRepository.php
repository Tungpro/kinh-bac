<?php

namespace App\Containers\Collection\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class CollectionRepository
 */
class CollectionDetailRepository extends Repository
{

    /**
     * @var array
     */
    protected $container = 'collection';

    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
