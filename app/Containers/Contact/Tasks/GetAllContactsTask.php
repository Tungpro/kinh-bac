<?php

namespace App\Containers\Contact\Tasks;

use App\Containers\Contact\Data\Criterias\FilterContactCriteria;
use App\Containers\Contact\Data\Repositories\ContactRepository;
use App\Ship\Criterias\Eloquent\OrderByCreationDateDescendingCriteria;
use App\Ship\Parents\Tasks\Task;

class GetAllContactsTask extends Task
{

    protected $repository;

    public function __construct(ContactRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        $this->repository->pushCriteria(new OrderByCreationDateDescendingCriteria());
        return $this->repository->paginate(20);
    }

    public function filterContacts($request)
    {
        $this->repository->pushCriteria(new FilterContactCriteria($request->all()));
    }
}
