<?php

namespace App\Containers\Contact\Tasks;

use App\Containers\Contact\Data\Repositories\ContactRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateContactProductTask extends Task
{
    protected $repository;

    public function __construct(ContactRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($contact,array $data)
    {

        try {
            $product_variant = $data['checked'] ?? [];

            if (!empty($product_variant)) {
                $contact->option_value()->sync($product_variant);
            }else {
                $contact->option_value()->sync([]);
            }
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
