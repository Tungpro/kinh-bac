<?php

namespace App\Containers\Contact\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\StringLib;
use App\Containers\Contact\Models\Contact;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cookie;

class CreateContactAction extends Action
{
    public function run(Request $request, $saveCookie = false)
    {
        $data = $request->all();

        if (!empty($data['product_id'])) {
            $data['type'] = !empty($data['product_id']) ? Contact::TYPE_V : Contact::TYPE_P;
            $data['object_id'] = $data['product_id'];
        }
        else {
            $data['type'] = $data['type'] ? $data['type'] : '';
        }

        if (!empty($request->file)) {
            $file = Apiato::call('File@UploadFileAction', [$request, 'file', 'contact', StringLib::getClassNameFromString(Contact::class)]);
            if (!empty($file) && isset($file['error']) && $file['error']) {
                return redirect()->back()->withInput()->withErrors(['error:' => $file['msg']]);
            }
            $data['file'] = $file['fileName'];
        }

        $dataCreate = Arr::only($data, ['phone', 'name', 'email', 'message', 'type', 'object_id', 'file', 'birthday', 'degree', 'experience']);


        $contact = Apiato::call('Contact@CreateContactTask', [$dataCreate]);

        if($contact && !empty($data['checked'])){
            $contact_product = Apiato::call('Contact@CreateContactProductTask', [$contact,$data]);
        }

        if ($saveCookie) {
            Cookie::queue(Contact::KEY_COOKIE_REQUIRE_INFORMATION, $contact);
        }

        return $contact;
    }
}
