<?php

namespace App\Containers\Contact\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteContactAction extends Action
{
    public function run(Request $request)
    {
        $currentContact = Apiato::call('Contact@FindContactByIdTask', [$request->id]);
        $deleteResult = Apiato::call('Contact@DeleteContactTask', [$request->id]);

        if ($deleteResult) {
            Apiato::call('User@CreateUserLogSubAction', [
                $currentContact->id,
                $currentContact->toArray(),
                [],
                __('Xóa bản ghi liên hệ'),
                get_class($currentContact)
            ]);
        }
        
        return $deleteResult;
    }
}
