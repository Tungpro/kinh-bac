<?php

namespace App\Containers\Contact\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Contact\Models\Contact;
use Illuminate\Support\Facades\Cookie;

class GetCookieContactAction extends Action
{
    public function run()
    {
        $contact = Cookie::get(Contact::KEY_COOKIE_REQUIRE_INFORMATION);
        return $contact;
    }
}
