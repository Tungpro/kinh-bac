<?php

namespace App\Containers\Contact\UI\WEB\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class PhoneNumberRule implements Rule
{
    private $network;

    public function __construct($network = NULL)
    {
        $this->network = $network;
    }

    public function passes($attribute, $value)
    {
        $phoneNumberRegexs = $this->getPhoneNumberRegexs();
        if (!empty($this->network)) {
            $phoneNumberRegexs = Arr::only($phoneNumberRegexs, [$this->network]);
        }

        foreach ($phoneNumberRegexs as $phoneNumberRegex) {
            if (preg_match($phoneNumberRegex, $value)) {
                return true;
            }
        }
        return false;
    }

    public function message()
    {
        return __('site.sodienthoaikhongdungdinhdang');
    }

    private function getPhoneNumberRegexs()
    {
        return [
            'VMS' => '/^0(90|93|89|78|76|77|79|70)\d{7}$/',
            'VNP' => '/^0(91|94|88|85|83|82|81|84)\d{7}$/',
            'VTL' => '/^0(96|97|98|86|32|33|34|35|36|37|38|39)\d{7}$/',
            'VNM' => '/^0(92|56|58)\d{7}$/',
            'Gtel' => '/^0(199|99|59)\d{7}$/',
            'Desk' => '/^0(212|213|214|215|216|232|233|234|235|236|237|238|239|203|204|205|206|207|208|209|220|221|222|225|226|227|228|229|290|291|292|293|294|296|297|299|251|252|254|255|256|257|258|259|260|261|262|263|269|270|271|272|273|274|275|276|277)\d{7}$/',
            'DeskCity' => '/^0(24|28)\d{8}$/'
        ];
    }
}
