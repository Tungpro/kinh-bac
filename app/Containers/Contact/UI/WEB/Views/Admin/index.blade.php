@extends('basecontainer::admin.layouts.default')

@section('right-breads')
    {!! $contacts->appends($request->all())->links('basecontainer::admin.inc.paginator') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-5"><h1>{{ $site_title }}</h1></div>

            @include('contact::Admin.filter')

            <div class="card card-accent-primary">
                <table class="table table-hover table-bordered mb-0" id="tableCustomer">
                    <thead>
                    <tr>
                        <th width="60" class="text-center">ID</th>
                        <th>Thông tin liên hệ</th>
                        <th>Loại liên hệ</th>
                        <th width="50%">Thông tin</th>
                        <th>Ngày tạo</th>
                        <th width="60" class="text-center">Xóa</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse ($contacts as $contact)
                        <tr>
                            <td class="text-center">{{ $contact->id }}</td>
                            <td>
                                @if (!empty($contact->name))
                                    <span class="d-block">
                                            <i class="fa fa-user w-15"></i>&nbsp;&nbsp;&nbsp;{{ $contact->name }}
                                        </span>
                                @endif

                                @if (!empty($contact->email))
                                    <span class="d-block"><i class="fa fa-envelope-o w-15"></i>
                                            &nbsp;<a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a>
                                        </span>
                                @endif

                                @if (!empty($contact->phone))
                                    <span class="d-block"><i class="fa fa-mobile w-15"></i>
                                            &nbsp;&nbsp;&nbsp;<a href="tel:{{ $contact->phone }}"
                                                                 class="text-body">{{ $contact->phone }}</a>
                                        </span>
                                @endif
                            </td>
                            <td>{{ \App\Containers\Contact\Models\Contact::contactTypeText()[$contact->type] ?? '--' }}</td>
                            <td title="{{$contact->message}}">
                                @if($contact->type === \App\Containers\Contact\Models\Contact::TYPE_C)
                                    <span class="d-block"><b>Lời nhắn:</b> {{$contact->message}}</span>
                                @elseif($contact->type === \App\Containers\Contact\Models\Contact::TYPE_R)
                                    @if (!empty($contact->news))
                                        <span class="d-block"><b>{{$contact->news->desc->name}}:</b> <a href="{{$contact->news->linkRecruit()}}" target="_blank" class="text-info" title="Xem chi tiết"><i class="fa fa-chain"></i></a></span>
                                    @endif
                                    @if (!empty($contact->birthday))
                                        <span class="d-block"><b>Năm sinh:</b> {{$contact->birthday}}</span>
                                    @endif
                                    @if (!empty($contact->degree))
                                           <span class="d-block"><b>Bằng cấp:</b> {{$contact->degree}}</span>
                                    @endif
                                    @if (!empty($contact->experience))
                                            <span class="d-block"><b>Kinh nghiệm:</b> {{$contact->experience}}</span>
                                    @endif
                                    @if (!empty($contact->file))
                                            <span class="d-block"><b>CV:</b> <a href="{{asset('/upload/contact/'.$contact->file)}}" target="_blank" class="text-info" title="Xem cv"><i class="fa fa-chain"></i></a></span>
                                    @endif
                                @endif
                                {{--                                @if(isset($contact->product))--}}
                                {{--                                    <span class="d-block"><b>Sản phẩm:</b>--}}
                                {{--                                            <a data-original-title="Xem trong Quản trị" data-toggle="tooltip"--}}
                                {{--                                               href="{{route('admin_product_edit_page', $contact->product->id)}}"--}}
                                {{--                                               data-product_id="{{ @$$contact->product->id ?? '--' }}"--}}
                                {{--                                               target="_blank">{{ @$contact->product->desc->name ?? '--' }}</a>--}}
                                {{--                                            <a class="ml-2" data-toggle="tooltip"--}}
                                {{--                                               data-original-title="Xem sản phẩm"--}}
                                {{--                                               href="{{@$contact->product->routeProductDetail()}}"--}}
                                {{--                                               target="_blank"><i class="fa fa-chain"></i></a>--}}
                                {{--                                        </span>--}}
                                {{--                                @endif--}}
                                {{--                                @if(isset($options) && $options->isNotEmpty() && isset($contact->option_value) && $contact->option_value->isNotEmpty())--}}
                                {{--                                    <span class="d-block"><b>Biến thể:</b></span>--}}
                                {{--                                    <span class="d-block ml-3">--}}
                                {{--                                          @foreach($options as $id => $op)--}}
                                {{--                                                @foreach($contact->option_value as $product_option_values)--}}
                                {{--                                                   @if($id == $product_option_values->option->id)--}}
                                {{--                                                    <b>- {{$op ?? 'Option'}}</b>--}}
                                {{--                                                    : {{$product_option_values->desc->name ? $product_option_values->desc->name : '----'}}<br>--}}
                                {{--                                                   @endif--}}
                                {{--                                                @endforeach--}}
                                {{--                                        @endforeach--}}
                                {{--                                        </span>--}}
                                {{--                                @endif--}}
                            </td>
                            <td>{{$contact->created_at}}</td>
                            <td class="text-center">
                                <input type="hidden" class="contact-info" value='@bladeJson($contact)'>
                                <a class="fa fa-trash text-danger cursor-pointer"
                                   data-href="{{ route('admin.contact.delete', $contact->id) }}"
                                   data-toggle="tooltip"
                                   data-original-title="Xóa liên hệ này"
                                   onclick="admin.delete_this(this);">
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7">Không tìm thấy dữ liệu</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

                @if ($contacts->isNotEmpty())
                    <div class="d-flex py-2">
                       <span class="ml-auto mr-3">
                            Tổng cộng: {{ $contacts->count() }}
                            bản ghi / {{ $contacts->lastPage() }} trang
                       </span>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('js_bot_before')
    <script src="{{ asset("admin/js/library/language/select2/i18n/{$currentLang}.js") }}"></script>

@endpush