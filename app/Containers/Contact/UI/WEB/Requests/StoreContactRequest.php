<?php

namespace App\Containers\Contact\UI\WEB\Requests;

use App\Containers\Contact\Models\Contact;
use App\Containers\Contact\UI\WEB\Rules\PhoneNumberRule;
use App\Ship\core\Traits\HelpersTraits\SecurityTrait;
use App\Ship\Parents\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StoreContactRequest.
 */
class StoreContactRequest extends Request
{
    use SecurityTrait;

    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    // protected $transporter = \App\Ship\Transporters\DataTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => '',
        'roles' => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        // 'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    public function rules()
    {
        if (!empty(request('product_id'))) {
            $rules['phone'] = [
                'required',
                new PhoneNumberRule(),
                Rule::unique('contact')->where(function ($query) {
                    return $query->where([
                        ['deleted_at', '=', null],
                        ['type', '=', Contact::TYPE_V],
                        ['object_id', '=', request('product_id')],
                    ]);
                })
            ];

        } elseif (!empty(request('type')) && request('type') == Contact::TYPE_R){

             $rules['phone'] = ['required', 'max:15'];
             $rules['birthday'] = ['required','min:4', 'max:4'];
             $rules['degree'] = ['required', 'min:3', 'max:100'];
             $rules['experience'] = ['required', 'min:3', 'max:100'];

             if(!empty(request('file')) && request('file') != 'null') {
                 $rules['file'] = ['mimes:doc,docx,pdf', 'max:3072'];
             }
        }
        else {
            $rules['phone'] = ['required', 'max:15'];
        }

            $rules['name'] = ['required', 'min:3', 'max:100'];
            $rules['email'] = ['required', 'email:rfc,filter', 'max:100']; //'unique:contact,email,NULL,id,deleted_at,NULL'
            $rules['message'] = ['max:2000'];

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => __('site.name_required'),
            'name.min' => __('site.name_min', ['min' => 3]),
            'name.max' => __('site.name_max', ['max' => 100]),
            'email.required' => __('site.email_required'),
            'email.email' => __('site.email_email'),
            'email.unique' => __('site.email_unique'),
            'phone.required' => __('site.phone_required'),
            'phone.max' => __('site.phone_max'),
            'phone.regex' => __('site.phone_regex'),
            'phone.unique' => __('site.phone_unique'),
            'message.required' => __('site.message_required'),
            'message.max' => __('site.message_max'),

            // tuyển dụng
            'birthday.required' => __('site.birthday_required'),
            'birthday.min' => __('site.birthday_min', ['min' => 4]),
            'birthday.max' => __('site.birthday_max', ['max' => 4]),
            'birthday.numeric' => __('site.birthday_numeric'),
            'degree.required' => __('site.degree_required'),
            'degree.min' => __('site.degree_min', ['min' => 3]),
            'degree.max' => __('site.degree_max', ['max' => 100]),
            'experience.required' => __('site.experience_required'),
            'experience.min' => __('site.experience_min', ['min' => 3]),
            'experience.max' => __('site.experience_max', ['max' => 100]),
            'file.max' => __('site.file_cv_max', ['max' => '3072kb']),
            'file.mimes' => __('site.file_cv_mimes'),
        ];
    }

    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'name' => $this->cleanXSS($this->name),
            'email' => $this->cleanXSS($this->email),
            'phone' => $this->cleanXSS($this->phone),
            'message' => $this->cleanXSS($this->message),

            'birthday' => $this->cleanXSS($this->birthday),
            'degree' => $this->cleanXSS($this->degree),
            'experience' => $this->cleanXSS($this->experience),

        ]);
    }
}
