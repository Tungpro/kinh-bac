<?php

namespace App\Containers\Contact\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Contact\Models\Contact;
use App\Containers\Contact\UI\WEB\Requests\DeleteContactRequest;
use App\Containers\Contact\UI\WEB\Requests\GetAllContactsRequest;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Ship\Parents\Controllers\AdminController;
use Exception;

class ContactController extends AdminController
{
    use ApiResTrait;

    public function __construct()
    {
        $this->title = __('Liên hệ');
        parent::__construct();
    }

    public function index(GetAllContactsRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);

        $contacts = Apiato::call('Contact@GetAllContactsAction', [
            $request,
            [
//                'productVariant:product_variant_id,product_id,status',
//                'productVariant.product:id',
//                'productVariant.product.desc:id,product_id,name',
//
//                'productVariant.product_option_values:product_option_value_id,product_option_id,product_id,option_id,option_value_id,product_variant_id',
//                'productVariant.product_option_values.option_value:id,option_id',
//                'productVariant.product_option_values.option_value.desc:id,option_value_id,option_id,name',

//            'product.desc',
//            'option_value','option_value.desc','option_value.option'

              'news.desc'
            ],
        ]);

//        $options = Apiato::call('Option@GetAllOptionsAction', [$request, ['desc:id,option_id,name'], 'ASC', ['id']])->pluck('desc.name', 'id');

        return view('contact::Admin.index', [
            'contacts' => $contacts,
            'request' => $request,
            'contactTypes' => Contact::contactTypeText(),
        //    'options' => $options,
        ]);
    }

    public function delete(DeleteContactRequest $request)
    {
        try {
            Apiato::call('Contact@DeleteContactAction', [$request]);
        } catch (Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }
} // End class

