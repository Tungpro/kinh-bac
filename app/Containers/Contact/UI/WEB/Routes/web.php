<?php

/** @var Route $router */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

$router->group([
    'namespace' => '\App\Containers\Contact\UI\WEB\Controllers\Desktop',
    'middleware' => [
        'htmloptimized',
        'Maintenance',
        'WebLocaleRedirect'
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run(),
    'domain' => parse_url(config('app.url'))['host'],
], function () use ($router) {
    $router->post('contacts/store', ['as' => 'home.contact.store', 'uses' => 'ContactController@store']);

});
