<?php

namespace App\Containers\Contact\Models;

use App\Containers\EShopBizfly\Models\Product;
use App\Containers\Product\Models\ProductVariant;
use App\Ship\core\Traits\HelpersTraits\DateTrait;
use App\Ship\Parents\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactProduct extends Model
{
    use DateTrait;

    use SoftDeletes;
    protected $table = 'contact_product_variants';

    protected $guarded = [];

    protected $attributes = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];


} // End class
