<?php

namespace App\Containers\Contact\Models;

use App\Containers\EShopBizfly\Models\Product;
use App\Containers\News\Models\News;
use App\Containers\Option\Models\OptionValue;
use App\Containers\Product\Models\ProductOption;
use App\Containers\Product\Models\ProductOptionValue;
use App\Containers\Product\Models\ProductVariant;
use App\Ship\core\Traits\HelpersTraits\DateTrait;
use App\Ship\Parents\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use DateTrait;
    use SoftDeletes;

    const TYPE_C = 'contact';
    const TYPE_R = 'recruit';
    const TYPE_P_V = 'product_variant';
    const TYPE_P = 'product';
    const TYPE_V = 'variant';

    protected $table = 'contact';

    public const KEY_COOKIE_REQUIRE_INFORMATION = '_KEY_COOKIE_REQUIRE_INFORMATION';

    protected $guarded = [];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'contacts';

    public static function contactTypeText()
    {
        return [
            self::TYPE_C => 'Liên hệ',
            self::TYPE_R => 'Tuyển dụng',
        //    self::TYPE_P => 'Quan tâm sản phẩm',
        //    self::TYPE_V => 'Quan tâm sản phẩm',
        ];
    }

    public function productVariant()
    {
        return $this->belongsTo(ProductVariant::class, 'object_id', 'product_variant_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'object_id', 'id');
    }

    public function news()
    {
        return $this->belongsTo(News::class, 'object_id', 'id');
    }

    public function option_value()
    {
        return $this->belongsToMany(OptionValue::class, ContactProduct::getTableName(), 'contact_id', 'product_variant_id');
    }


} // End class
