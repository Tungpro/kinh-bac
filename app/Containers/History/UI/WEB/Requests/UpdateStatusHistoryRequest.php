<?php

namespace App\Containers\History\UI\WEB\Requests;

use App\Containers\History\Models\History;
use App\Ship\Parents\Requests\Request;

class UpdateStatusHistoryRequest extends Request
{
    protected $access = [
        'permissions' => 'history-edit',
        'roles' => 'admin',
    ];

    protected $decode = [];

    protected $urlParameters = [
        'id',
    ];

    public function rules()
    {
        return [
            'id' => ['required', sprintf('exists:%s,id', History::TABLE_NAME)],

        ];
    }

    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
