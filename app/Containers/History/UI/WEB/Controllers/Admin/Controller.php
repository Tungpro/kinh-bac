<?php

namespace App\Containers\History\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\History\UI\WEB\Requests\CreateHistoryRequest;
use App\Containers\History\UI\WEB\Requests\DeleteHistoryRequest;
use App\Containers\History\UI\WEB\Requests\GetAllHistorysRequest;
use App\Containers\History\UI\WEB\Requests\UpdateHistoryRequest;
use App\Containers\History\UI\WEB\Requests\StoreHistoryRequest;
use App\Containers\History\UI\WEB\Requests\EditHistoryRequest;
use App\Containers\History\UI\WEB\Requests\UpdateStatusHistoryRequest;
use App\Ship\Parents\Controllers\AdminController;
use Apiato\Core\Foundation\Facades\Apiato;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class Controller extends AdminController
{
    protected $containerName = 'history';

    public function __construct()
    {
        $this->title = 'lịch sử phát triển';
        $this->imageField = 'image';
        $this->imageKey = 'history';
        $this->actions = [
            'list' => 'History@GetAllHistorysAction',
            'create' => 'History@CreateHistoryAction',
            'edit' => 'History@FindHistoryByIdAction',
            'update' => 'History@UpdateHistoryAction',
            'delete' => 'History@DeleteHistoryAction',
        ];
        $this->routes = [
            'list' => 'admin.history.index',
            'create' => 'admin.history.create',
            'store' => 'admin.history.store',
            'edit' => 'admin.history.edit',
            'update' => 'admin.history.update',
            'destroy' => 'admin.history.destroy',
            'disable' => "admin.$this->containerName.disable",
            'enable' => "admin.$this->containerName.enable",
        ];
        View::share('routes', $this->routes);
        View::share('containerName', $this->containerName);

        parent::__construct();
    }

    public function index(GetAllHistorysRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);

        $request->merge(['scopeAdminBaseSearch' => true]);
        $data = Apiato::call($this->actions['list'], [$request, $this->perPage, null, ['desc']]);

        return view("$this->containerName::Admin.index", [
            'request' => $request,
            'data' => $data,
        ]);
    }

    public function create(CreateHistoryRequest $request)
    {
        $this->showAddForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['add', $this->title, $this->routes['list']]);

        return view("$this->containerName::Admin.form", []);
    }

    public function store(StoreHistoryRequest $request)
    {
        $this->editMode = false;
        return $this->save($request);
    }

    public function edit($id, EditHistoryRequest $request)
    {
        $this->showEditForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['edit', $this->title, $this->routes['list']]);

        $editedObj = Apiato::call($this->actions['edit'], [$id, ['all_desc']]);

        if ($editedObj) {

            return view("$this->containerName::Admin.form", [
                'data' => $editedObj,
            ]);
        }

        return redirect()->route($this->routes['list']);
    }

    public function update(UpdateHistoryRequest $request)
    {
        $this->editMode = true;
        return $this->save($request);
    }

    public function destroy($id, DeleteHistoryRequest $request)
    {
        try {

            Apiato::call($this->actions['delete'], [$id]);
            return FunctionLib::ajaxRespondV2(true, 'Xóa bản ghi thành công!');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình xóa dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function enable(UpdateStatusHistoryRequest $request)
    {
        try {
            Apiato::call('History@EnableHistoryAction', [$request]);

            return FunctionLib::ajaxRespondV2(true, 'Hiện bản ghi thành công!');
        } catch (Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình sửa dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function disable(UpdateStatusHistoryRequest $request)
    {
        try {
            Apiato::call('History@DisableHistoryAction', [$request]);

            return FunctionLib::ajaxRespondV2(true, 'Ẩn bản ghi thành công!');
        } catch (Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình sửa dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }
}
