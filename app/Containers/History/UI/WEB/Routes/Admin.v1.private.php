<?php

/** @var Route $router */
$router->group([
    'middleware' => ['auth:admin'],
    'namespace' => '\App\Containers\History\UI\WEB\Controllers\Admin',
    'domain' => sprintf('admin.%s', parse_url(config('app.url'))['host'])
], function () use ($router) {
    $router->resource('history', 'Controller')
        ->names([
            'index' => 'admin.history.index',
            'create' => 'admin.history.create',
            'store' => 'admin.history.store',
            'edit' => 'admin.history.edit',
            'update' => 'admin.history.update',
            'destroy' => 'admin.history.destroy',
        ])
        ->except(['show']);

    $router->post('/history/enable/{id}', ['as' => 'admin.history.enable', 'uses' => 'Controller@enable',]);
    $router->post('/history/disable/{id}', ['as' => 'admin.history.disable', 'uses' => 'Controller@disable',]);
});
