@extends('basecontainer::admin.layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-4">
                <h1>{{ $site_title }}</h1>
            </div>
            <div class="tabbable boxed parentTabs">
                <div class="tab-content p-0">
                    <div class="tab-pane active">
                        <div class="tabbable">
                            <form action="{{ route($routes['list']) }}" method="GET" id="searchForm">
                                <div class="card card-accent-primary">
                                    <div class="card-body">
                                        @include('basecontainer::admin.inc.base-search')
                                        <div class="row">

                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-sm btn-primary mr-2">
                                            <i class="fa fa-search"></i> {{__('Tìm kiếm')}}
                                        </button>
                                        <a href="{{route($routes['list'])}}"
                                           class="btn btn-sm btn-primary mr-2"><i
                                                    class="fa fa-refresh"></i> {{__('Reset')}}</a>
                                        @can('history-create')
                                            <a href="{{route($routes['create'])}}"
                                               class="btn btn-sm btn-primary">
                                                <i class="fa fa-plus"></i> {{__('Thêm mới')}}
                                            </a>
                                        @endcan
                                    </div>
                                </div>
                            </form>

                            <div class="card card-accent-info">
                                <div class="card-header">
                                    <i class="fa fa-align-justify"></i> {{__('Danh sách')}}
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        @if ($data->isNotEmpty())
                                            <table class="table table-bordered table-hover mb-0">
                                                <thead>
                                                <tr>
                                                    <th width="40">{{ __('ID') }}</th>
                                                    <th>{{ __('Lịch sử phát triển') }}</th>
                                                    <th class="text-center">{{ __('Ảnh') }}</th>
                                                    <th class="text-center" width="50">{{ __('Sort') }}</th>
                                                    <th class="text-center">{{ __('Ngày tạo') }}</th>
                                                    @canany([$containerName.'-edit', $containerName.'-delete'])
                                                        <th class="text-center" width="50">{{ __('Lệnh') }}</th>
                                                    @endcanany
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($data as $item)
                                                    <tr data-sort_order="{{ @$item->sort_order }}"
                                                        data-status="{{@$item->status}}">
                                                        <td>{{ $item->id }}</td>
                                                        <td>
                                                            <b>Tên: </b>{{ @$item->desc->name }}<br>
                                                            @if(!empty($item->birth_year))
                                                                <b>Năm sinh: </b>{{ $item->birth_year }}<br>
                                                            @endif
                                                            @if(!empty($item->email))
                                                                <b>Email: </b>{{ $item->email }}
                                                            @endif
                                                        </td>
                                                        <td class="text-center">
                                                            <img src="{{ @$item->getImageUrl('small') }}"
                                                                 class="img-thumbnail" width="100"
                                                                 alt="{{ @$item->desc->name }}">
                                                        </td>
                                                        <td class="text-center">{{ @$item->sort_order ?? '-' }}</td>
                                                        <td class="text-center">{{!empty($item->created_at) ? $item->created_at->format("H:i:s d/m/Y") : '---'}}</td>
                                                        @canany([$containerName.'-edit', $containerName.'-delete'])
                                                            <td class="text-center">

                                                                @include("basecontainer::admin.inc.edit_tabs.index-commands")
                                                            </td>
                                                        @endcanany
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    @include('basecontainer::admin.inc.bottom-pager-infor')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection