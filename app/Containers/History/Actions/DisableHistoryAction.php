<?php

namespace App\Containers\History\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\History\Models\History;
use App\Ship\Parents\Actions\Action;

class DisableHistoryAction extends Action
{
    public function run($data)
    {
        $object = Apiato::call('History@DisableHistoryTask', [$data->id]);

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            [],
            [],
            'Ẩn Lịch sử phát triển',
            History::class
        ]);

        $this->clearCache();

        return $object;
    }
}
