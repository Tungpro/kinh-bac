<?php

namespace App\Containers\History\Actions;

use App\Containers\History\Data\Repositories\HistoryDescRepository;
use App\Containers\History\Models\History;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;

class CreateHistoryAction extends Action
{
    public function run($request)
    {
        $data = Arr::only($request, [
           'name', 'description', 'status', 'sort_order', 'image',
        ]);
        $data['created_by'] = auth()->id();

        $objCreated = Apiato::call('History@CreateHistoryTask', [Arr::except($data, ['description'])]);

        if ($objCreated) {
            // Create Desc
           Apiato::call('BaseContainer@Admin\SaveModelDescTask', [app(HistoryDescRepository::class), $data, [], $objCreated->id, 'history_id']);

            // Create Log
            Apiato::call('User@CreateUserLogSubAction', [
                $objCreated->id,
                [],
                $objCreated->load('all_desc')->toArray(),
                'Tạo lịch sử phát triển',
                History::class
            ]);
        }

        $this->clearCache();

        return $objCreated;
    }
}
