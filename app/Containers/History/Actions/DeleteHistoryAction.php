<?php

namespace App\Containers\History\Actions;

use App\Containers\History\Data\Repositories\HistoryDescRepository;
use App\Containers\History\Models\History;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteHistoryAction extends Action
{
    public function run($id)
    {
        $deleted = Apiato::call('History@DeleteHistoryTask', [$id]);

        if ($deleted) {
            Apiato::call('BaseContainer@Admin\DeleteModelDescTask', [app(HistoryDescRepository::class), $id, 'History_id']);

            Apiato::call('User@CreateUserLogSubAction', [
                $id,
                [],
                [],
                'Xóa Lịch sử phát triển',
                History::class
            ]);

            $this->clearCache();

            return $deleted;
        }


        return false;
    }
}
