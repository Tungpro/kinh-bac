<?php

namespace App\Containers\History\Actions;

use App\Containers\History\Data\Repositories\HistoryDescRepository;
use App\Containers\History\Models\History;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;

class UpdateHistoryAction extends Action
{
    public function run($request)
    {
        $data = Arr::only($request, [
            'id', 'description', 'status', 'sort_order', 'birth_year', 'email', 'image',
        ]);
        $data['updated_by'] = auth()->id();

        $objBefore = Apiato::call('History@FindHistoryByIdTask', [$data['id']], [['with' => [['all_desc']]]]);
        $objUpdated = Apiato::call('History@UpdateHistoryTask', [$data['id'], Arr::except($data, ['description'])]);

        if ($objUpdated) {
            $originalDesc = Apiato::call('BaseContainer@Admin\GetAllModelDescTask', [app(HistoryDescRepository::class), $objUpdated->id, 'History_id']);
            Apiato::call('BaseContainer@Admin\SaveModelDescTask', [app(HistoryDescRepository::class), $data, $originalDesc, $objUpdated->id, 'History_id']);

            // Create Log
            Apiato::call('User@CreateUserLogSubAction', [
                $objUpdated->id,
                $objBefore->toArray(),
                $objUpdated->load('all_desc')->toArray(),
                'Sửa lịch sử phát triển',
                History::class
            ]);
        }

        $this->clearCache();

        return $objUpdated;
    }
}
