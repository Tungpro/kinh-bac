<?php

namespace App\Containers\History\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class FindHistoryByIdAction extends Action
{
    public function run($id, array $withData = [], array $conditions = [], $selectFields = ['*'])
    {
        return Apiato::call('History@FindHistoryByIdTask', [$id], [
            ['with' => [$withData]],
            ['where' => [$conditions]],
            ['selectFields' => [$selectFields]]
        ]);
    }
}
