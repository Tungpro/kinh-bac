<?php

namespace App\Containers\History\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class FindHistoryByConditionsAction extends Action
{
    public function run(array $conditions = [], array $withData = [], $selectFields = ['*'])
    {
        return Apiato::call('History@FindHistoryByConditionsTask', [], [
            ['where' => [$conditions]],
            ['with' => [$withData]],
            ['selectFields' => [$selectFields]]
        ]);
    }
}
