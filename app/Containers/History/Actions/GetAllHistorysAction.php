<?php

namespace App\Containers\History\Actions;

use App\Containers\History\Tasks\GetAllHistorysTask;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllHistorysAction extends Action
{
    public function run($request, $limit = 20,
                        ?Language $currentLang = null, array $with = [],array $selectFields = ['*'],
                        array $conditions = [], array $filters = [], int $offset = 0, string $exceptID = '', array $orderBy = ['sort_order' => 'ASC', 'id' => 'DESC']
    )
    {
        return $this->remember(function () use ($request, $limit, $currentLang, $with, $selectFields, $conditions,$filters, $exceptID, $offset, $orderBy) {
            return app(GetAllHistorysTask::class)->currentLang($currentLang)->with($with)->where($conditions)
                ->selectFields($selectFields)->orderBy($orderBy)->scopeAdminBaseSearch($request)->scopeFEAvailableData($request)->run(
                    $currentLang, $filters, $limit ,$offset, $exceptID
                );
        }, null, [], 0, $request->skipCache ?? true);

    }

}
