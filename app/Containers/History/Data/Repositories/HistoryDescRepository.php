<?php

namespace App\Containers\History\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class HistoryDescRepository
 */
class HistoryDescRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
