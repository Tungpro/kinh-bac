<?php

namespace App\Containers\History\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class HistoryRepository
 */
class HistoryRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
