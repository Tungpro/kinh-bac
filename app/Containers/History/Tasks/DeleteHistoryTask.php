<?php

namespace App\Containers\History\Tasks;

use App\Containers\History\Data\Repositories\HistoryRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteHistoryTask extends Task
{
    protected $repository;

    public function __construct(HistoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            $examCategory = $this->repository->find($id);

            return $examCategory->delete();
        } catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
