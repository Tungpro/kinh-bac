<?php

namespace App\Containers\History\Tasks;

use App\Containers\History\Data\Repositories\HistoryRepository;
use App\Ship\core\Foundation\BladeHelper;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DisableHistoryTask extends Task
{
    protected $repository;

    public function __construct(HistoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->getModel()->where('id', $id)->update(['status' => BladeHelper::AN]);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
