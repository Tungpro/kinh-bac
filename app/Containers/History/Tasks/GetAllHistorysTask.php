<?php

namespace App\Containers\History\Tasks;

use App\Containers\BaseContainer\Traits\ScopeAdminBaseSearchTrait;
use App\Containers\BaseContainer\Traits\ScopeFEAvailableDataTrait;
use App\Containers\Localization\Models\Language;
use App\Containers\History\Data\Repositories\HistoryRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllHistorysTask extends Task
{
    use ScopeAdminBaseSearchTrait;
    use ScopeFEAvailableDataTrait;

    protected $repository;

    public function __construct(HistoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Language $currentLang = null,array $filters = [], $limit = 20,int $offset = 0,string $exceptID = '')
    {
        $language_id = $currentLang ? $currentLang->language_id : 1;
        return $this->with([
            'desc' => function ($query) use ($language_id) {
                $query->where('language_id', $language_id);
            },
        ])->returnDataByLimit($limit,$offset,$exceptID);

    }

}
