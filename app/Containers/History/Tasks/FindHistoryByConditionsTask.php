<?php

namespace App\Containers\History\Tasks;

use App\Containers\History\Data\Repositories\HistoryRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindHistoryByConditionsTask extends Task
{
    protected $repository;

    public function __construct(HistoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        try {
            return $this->repository->first();
        } catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
