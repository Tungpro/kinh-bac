<?php

namespace App\Containers\History\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Ship\core\Traits\HelpersTraits\LangTrait;
use App\Ship\Parents\Models\Model;

class History extends Model
{
    use LangTrait;

    const TABLE_NAME = 'history';
    protected $table = self::TABLE_NAME;

    protected $fillable = [
        'status', 'sort_order', 'image', 'created_by', 'updated_by',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $resourceKey = 'history';

    /*relationship*/
    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', HistoryDesc::class, 'history_id', 'id');
    }

    public function desc()
    {
        return $this->hasOne(HistoryDesc::class, 'history_id', 'id');
    }

    /*function */
    public function getImageUrl($size = 'small')
    {
        return ImageURL::getImageUrl($this->image, 'history', $size);
    }

    public function getPositionArr()
    {
        if (!empty($this->desc->description)) {
            return explode('|', $this->desc->description);
        }
        return [];
    }
}
