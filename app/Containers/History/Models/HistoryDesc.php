<?php

namespace App\Containers\History\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class HistoryDesc extends Model
{
    const TABLE_NAME = 'history_desc';
    protected $table = self::TABLE_NAME;

    protected $fillable = [
        'history_id', 'language_id', 'name', 'description','short_description'
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'history_desc';

    public function language()
    {
        return $this->hasOne(Language::class, 'language_id', 'language_id');
    }

}
