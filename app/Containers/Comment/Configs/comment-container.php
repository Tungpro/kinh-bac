<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Comment Container
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'status' => [
        '1' => __('Đánh giá gần đây'),
        '2' => __('Đã duyệt'),
        '3' => __('Đã xóa')
    ],

    'bad_words' => [
        'sort_by' => [
            'bad_word ASC' => 'Từ cấm: A-Z',
            'bad_word DESC' => 'Từ cấm: Z-A',
            'id ASC' => 'Cũ nhất đến mới nhất',
            'id DESC' => 'Mới nhất đến cũ nhất',
            'updated_at DESC' => 'Cập nhật gần đây',
        ]
    ],

    'status_define' => [
        'recently' => 1,
        'approved' => 2,
        'deleted' => 3
    ]
];
