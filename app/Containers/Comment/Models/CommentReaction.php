<?php

namespace App\Containers\Comment\Models;

use App\Ship\Parents\Models\Model;

class CommentReaction extends Model
{
    public $table = 'comment_reaction';

    protected $guarded = [];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'commentreactions';
}
