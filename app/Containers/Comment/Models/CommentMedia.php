<?php

namespace App\Containers\Comment\Models;

use App\Ship\Parents\Models\Model;

class CommentMedia extends Model
{
    protected $table = 'comment_media';

    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'commentimages';

    public function comment() {
      return $this->belongsTo(Comment::class, 'comment_id');
    }

    public function isImageMedia(): bool {
      return $this->type == 'image';
    }

    public function isVideoMedia(): bool {
      return $this->type == 'video';
    }

    public function getThumbnailVideo(): string {
      $source = $this->source;
      parse_str( parse_url( $source, PHP_URL_QUERY), $params );
      
      if ( !empty($params['v']) ) {
        return sprintf('https://img.youtube.com/vi/%s/0.jpg', $params['v']);
      }

      return 'https://img.youtube.com/vi/default-thumbnail/0.jpg';
    }
}
