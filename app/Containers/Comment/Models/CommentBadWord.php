<?php

namespace App\Containers\Comment\Models;

use App\Ship\Parents\Models\Model;

class CommentBadWord extends Model
{
    protected $table = 'comment_badword';

    protected $guarded = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'commentbadwords';
}
