<?php

namespace App\Containers\Comment\Models;

use App\Containers\Comment\Enums\CommentEmotional;
use App\Containers\Comment\Enums\CommentStatus;
use App\Ship\Parents\Models\Model;
use App\Containers\User\Models\User;
use App\Containers\Product\Models\Product;
use App\Containers\Customer\Models\Customer;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Containers\Comment\Traits\CommentTrait;
use App\Ship\core\Traits\HelpersTraits\DateTrait;

class Comment extends Model
{
    use SoftDeletes;
    use CommentTrait;
    use DateTrait;
    const PER_PAGE = 10;
    protected $table = 'comment';

    protected $guarded = [];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'comments';

    protected $starWithinEmo = [
      1 => CommentEmotional::BAD,
      2 => CommentEmotional::NOT_OK,
      3 => CommentEmotional::JUST_OK,
      4 => CommentEmotional::HAPPY,
      5 => CommentEmotional::VERY_HAPPY,
    ];

    public function returnEmotion() {
      return $this->starWithinEmo[$this->rating] ?? $this->starWithinEmo[5];
    }

    public function medias() {
      return $this->hasMany(CommentMedia::class, 'comment_id', 'id');
    }

    public function imageMedias() {
      return $this->hasMany(CommentMedia::class, 'comment_id', 'id')->where('type', 'image');
    }

    public function videoMedias() {
      return $this->hasMany(CommentMedia::class, 'comment_id', 'id')->where('type', 'video');
    }

    public function user() {
      return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function customer() {
      return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function childrens() {
      return $this->hasMany(Comment::class, 'parent_id', 'id');
    }

    public function aprovedReplies(){
      return $this->childrens()->where('status',CommentStatus::ACTIVE)->orderBy('created_at', 'desc');
    }

    public function threeChildrens() {
      return $this->childrens()->nPerGroup('parent_id', 3)->orderBy('created_at', 'desc');
    }

    public function myReplyComment() {

      return $this->hasOne(Comment::class, 'parent_id', 'id')->where('customer_id', @auth('customer')->id())->orderBy('id', 'desc');
    }

    public function images() {
      return $this->hasMany(CommentImage::class, 'comment_id', 'id');
    }

    public function isProductComment() {
      return $this->commentable_type == Product::class;
    }

    public function isParentComment() {
      return empty($this->parent_id);
    }

    public function isCommentOfUser() {
      return !empty($this->uid);
    }

    public function isApproved() {
      return !empty($this->approved);
    }

    public function product() {
      return $this->belongsTo(Product::class, 'object_id', 'id');
    }

    public function getOppositeApprovedText() {
      if ($this->isApproved()) {
        return 'Hủy duyệt';
      }
      return 'Duyệt - Hiển thị';
    }

    public function isMyComment() {
      return !empty($this->uid) && $this->uid == auth()->id();
    }

    public function isIssuesHandled() {
      return !empty($this->is_issues_handled);
    }
} // End class
