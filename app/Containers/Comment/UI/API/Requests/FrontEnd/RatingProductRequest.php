<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-09 00:40:03
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-09 14:36:44
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\UI\API\Requests\FrontEnd;

use App\Containers\Product\Models\Product;
use App\Ship\Parents\Requests\ApiRequest;

class RatingProductRequest extends ApiRequest
{

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => null,
        'roles' => null,
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        'productId'
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        //customer_id,!'.$user->id
        // $user = auth()->guard(config('auth.guard_for.api-customer'))->user();
        return [
            'productId' => ['required','exists:'.Product::getTableName().',id'],
            'rating' => ['required','numeric'],
            'content' => ['required','max:2000','min:50']
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }
}
