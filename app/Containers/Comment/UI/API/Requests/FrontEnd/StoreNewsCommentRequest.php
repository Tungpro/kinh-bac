<?php

namespace App\Containers\Comment\UI\API\Requests\FrontEnd;

use App\Containers\News\Models\News;
use App\Ship\Parents\Requests\Request;
use Illuminate\Validation\Rule;

class StoreNewsCommentRequest extends Request
{
    protected $access = [
        'permissions' => '',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'gender' => ['required', Rule::in(1, 2)],
            'name' => ['required', 'min:3', 'max:150'],
            'email' => ['required', 'min:3', 'max:191'],
            'object_id' => ['required', 'numeric', 'min:0'],
            'content' => ['required', 'min:5']
        ];
    }

    public function messages()
    {
        return [
            
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'type' => News::class,
            'customer_id' => auth('api-customer')->id()
        ]);
    }
}
