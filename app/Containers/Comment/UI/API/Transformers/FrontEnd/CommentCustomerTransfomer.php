<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-07 13:09:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-07 15:55:20
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\UI\API\Transformers\FrontEnd;

use Apiato\Core\Foundation\ImageURL;
use App\Ship\Parents\Transformers\Transformer;

class CommentCustomerTransfomer extends Transformer
{
    public function transform($customer)
    {
        $response = [
            'id' => $customer['id'],
            'fullname' => $customer['fullname'],
            'avatar' => ImageURL::getImageUrl($customer['avatar'],'customer','avatar'),
            'email' => $customer['email']
        ];

        return $response;
    }
}
