<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-07 13:09:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-08 09:49:27
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\UI\API\Transformers\FrontEnd;

use Apiato\Core\Foundation\ImageURL;
use App\Ship\Parents\Transformers\Transformer;

class CommentStatisticTransfomer extends Transformer
{
    protected array $defaultIncludes = [
        'medias'
    ];

    public function transform($combineData)
    {
        // dd($combineData);
        // dd($combineData->totalRatingStar);
        krsort($combineData->totalRatingStar);
        $response = [
            'total_rating' => $combineData->totalRating,
            'star' => $combineData->totalRatingStar
        ];

        return $response;
    }

    public function includeMedias($combineData)
    {
        return !empty($combineData->medias) ? $this->collection($combineData->medias, new CommentMediaTransfomer, 'medias') : $this->null();
    }
}
