<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-07 13:09:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-09 00:57:28
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\UI\API\Transformers\FrontEnd;

use App\Ship\Parents\Transformers\Transformer;

class CommentByProductTransfomer extends Transformer
{
    protected array $defaultIncludes = [
        'customer',
        'replies',
        'medias'
    ];
    public function transform($comment)
    {
        $response = [
            'id' => $comment->id,
            'customer_id' => $comment->customer_id,
            'object_id' => $comment->object_id,
            'parent_id' => $comment->parent_id,
            'content' => $comment->content,
            'status' => $comment->status,
            'rating' => $comment->rating,
            'rating_emo' => $comment->returnEmotion(),
            'total_reaction' => $comment->total_reaction,
            'created_at' => $comment->created_at,
            'reply' => [
                'content' => '',
                'show_form' => false
            ]
        ];
        return $response;
    }

    public function includeCustomer($comment)
    {
        return !empty($comment->customer) ? $this->item($comment->customer, new CommentCustomerTransfomer, 'data') : $this->null();
    }

    public function includeReplies($comment)
    {
        return $comment->aprovedReplies()->exists() && !empty($comment->aprovedReplies) && !$comment->aprovedReplies->IsEmpty() ? $this->collection($comment->aprovedReplies, new CommentByProductTransfomer, 'data') : $this->null();
    }

    public function includeMedias($comment)
    {
        return $comment->medias()->exists() && !empty($comment->medias) && !$comment->medias->IsEmpty() ? $this->collection($comment->medias, new CommentMediaTransfomer, 'data') : $this->null();
    }
}
