<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-07 13:09:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-07 16:06:44
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\UI\API\Transformers\FrontEnd;

use Apiato\Core\Foundation\ImageURL;
use App\Ship\Parents\Transformers\Transformer;

class CommentMediaTransfomer extends Transformer
{
    public function transform($media)
    {
        $response = [
            'id' => $media['id'],
            'image' => ImageURL::getImageUrl($media['source'],'comment','medium'),
            'image_larger' => ImageURL::getImageUrl($media['source'],'comment','large'),
            'image_fhd' => ImageURL::getImageUrl($media['source'],'comment','largex2'),
            'image_original' => ImageURL::getImageUrl($media['source'],'comment','original'),
        ];

        return $response;
    }
}
