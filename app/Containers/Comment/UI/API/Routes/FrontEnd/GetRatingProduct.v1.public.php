<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-04 16:55:35
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-08 22:23:59
 * @ Description: Happy Coding!
 */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

Route::group(
[
    'middleware' => [
        'api',
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run().'/comment/product',
],
function () use ($router) {
    $router->any('/{productId}', [
        'as' => 'api_comment_get_by_product_id',
        'uses'       => 'FrontEnd\Controller@getCommentByProductId'
    ])->where([
        'productId' => '[0-9]+'
    ]);

    $router->any('/statistic/{productId}', [
        'as' => 'api_comment_statistic_by_product_id',
        'uses'       => 'FrontEnd\Controller@getCommentStatistic'
    ])->where([
        'productId' => '[0-9]+'
    ]);
});