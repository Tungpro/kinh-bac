<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-04 16:55:35
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-09 13:03:20
 * @ Description: Happy Coding!
 */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

Route::group(
[
    'middleware' => [
        'api',
        'auth:api-customer'
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run().'/comment',
],
function () use ($router) {
    $router->any('/reply/{productId}', [
        'as' => 'api_comment_reply_product_id',
        'uses'       => 'FrontEnd\Controller@replyComment'
    ])->where([
        'productId' => '[0-9]+'
    ]);

    $router->any('/rating/{productId}', [
        'as' => 'api_comment_rating_product_id',
        'uses'       => 'FrontEnd\Controller@ratingProduct'
    ])->where([
        'productId' => '[0-9]+'
    ]);
});


Route::group(
[
    'middleware' => [
        'api',
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run().'/comment',
],
function () use ($router) {
    $router->post('store/{newsId}', [
        'as' => 'api_comment_news_store',
        'uses' => 'FrontEnd\Controller@storeNewsComment'
    ]);

    $router->get('getComment/{newsId}', [
        'as' => 'api_comment_news_get',
        'uses' => 'FrontEnd\Controller@GetCommentByNewtId'
    ]);
});
