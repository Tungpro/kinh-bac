<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-09 13:02:36
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\UI\API\Controllers\FrontEnd;

use App\Containers\BaseContainer\UI\WEB\Controllers\BaseApiFrontController;
use App\Containers\Comment\UI\API\Controllers\FrontEnd\Features\GetCommentByNewtId;
use App\Containers\Comment\UI\API\Controllers\FrontEnd\Features\ReplyComment;
use App\Containers\Comment\UI\API\Controllers\FrontEnd\Features\RatingProduct;
use App\Containers\Comment\UI\API\Controllers\FrontEnd\Features\StoreNewsComment;
use App\Containers\Comment\UI\API\Controllers\FrontEnd\Features\GetCommentStatistic;
use App\Containers\Comment\UI\API\Controllers\FrontEnd\Features\GetCommentByProductId;

class Controller extends BaseApiFrontController
{
    use GetCommentByProductId,
        GetCommentByNewtId,
        GetCommentStatistic,
        ReplyComment,
        RatingProduct,
        StoreNewsComment;

    public function __construct()
    {
        parent::__construct();
    }
}
