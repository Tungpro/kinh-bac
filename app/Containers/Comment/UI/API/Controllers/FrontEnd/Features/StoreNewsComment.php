<?php

namespace App\Containers\Comment\UI\API\Controllers\FrontEnd\Features;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Comment\UI\API\Requests\FrontEnd\StoreNewsCommentRequest;
use Illuminate\Http\Response;

trait StoreNewsComment
{
    public function storeNewsComment(StoreNewsCommentRequest $request)
    {
        $comment = Apiato::call('Comment@FrontEnd\StoreNewsCommentAction', [$request]);
        return $this->sendResponse($comment, __('Cảm ơn bạn đã để lại bình luận'));
    }
} // End class
