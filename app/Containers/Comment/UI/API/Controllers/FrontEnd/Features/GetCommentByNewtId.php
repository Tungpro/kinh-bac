<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-09 11:18:53
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\UI\API\Controllers\FrontEnd\Features;

use Apiato\Core\Transformers\Serializers\ArraySerializer;
use App\Containers\Comment\Actions\GetCommentsByObjectIdAction;
use App\Containers\Comment\UI\API\Requests\FrontEnd\GetCommentByNewIdRequest;
use App\Containers\Comment\UI\API\Transformers\FrontEnd\CommentByProductTransfomer;
use App\Containers\News\Models\News;

trait GetCommentByNewtId
{
    public function GetCommentByNewtId(GetCommentByNewIdRequest $request)
    {
        $newID = $request->newsId;
        $filters = [
            'objectId' => $newID,
            'type' => News::class
        ];
        $comments = app(GetCommentsByObjectIdAction::class)->run($filters, [
            'customer:id,fullname,avatar,email',
            'aprovedReplies',
            'aprovedReplies.customer:id,fullname,avatar,email',
            'medias'
        ],5,null,$request->page ?? 1);
        return $this->transform($comments, CommentByProductTransfomer::class, [],  [
            'message' => 'Success',
            'status' => true
        ], 'data',new ArraySerializer());
    }
}
