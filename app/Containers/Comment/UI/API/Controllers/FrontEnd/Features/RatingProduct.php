<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-13 17:05:27
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Comment\Actions\CreateCommentAction;
use App\Containers\Comment\UI\API\Requests\FrontEnd\RatingProductRequest;
use Illuminate\Http\Response;

trait RatingProduct
{
    public function ratingProduct(RatingProductRequest $request)
    {
        $result = app(CreateCommentAction::class)->run([
            'object_id' => $request->productId,
            'content' => $request->content,
            'customer_id' => $this->user->id,
            'parent_id' => 0,
            'rating' => (int)$request->rating,
            'status' => 1,
        ]);

        return $this->sendResponse($result,'Success',Response::HTTP_OK);
    }
}
