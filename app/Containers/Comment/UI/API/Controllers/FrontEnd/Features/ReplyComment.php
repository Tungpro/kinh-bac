<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-09 00:21:10
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\UI\API\Controllers\FrontEnd\Features;

use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Containers\Comment\Actions\CreateReplyCommentAction;
use App\Containers\Comment\UI\API\Requests\FrontEnd\ReplyCommentRequest;
use Illuminate\Http\Response;

trait ReplyComment
{
    public function replyComment(ReplyCommentRequest $request)
    {
        $result = app(CreateReplyCommentAction::class)->run([
            'object_id' => $request->productId,
            'content' => $request->content,
            'customer_id' => $this->user->id,
            'parent_id' => $request->commentId,
            'rating' => 0,
            'status' => 1,
        ]);

        return $this->sendResponse($result,'Success',Response::HTTP_OK);
    }
}
