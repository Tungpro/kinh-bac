<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 14:57:39
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-07 18:20:21
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\UI\API\Controllers\FrontEnd\Features;

use App\Containers\Comment\Actions\FrontEnd\GetCommentStatisticByPrdAction;
use App\Containers\Comment\UI\API\Requests\FrontEnd\GetCommentStatisticByProductIdRequest;
use App\Containers\Comment\UI\API\Transformers\FrontEnd\CommentStatisticTransfomer;
use stdClass;

trait GetCommentStatistic
{
    public function getCommentStatistic(GetCommentStatisticByProductIdRequest $request)
    {
        $productId = $request->productId;
        $combineData = app(GetCommentStatisticByPrdAction::class)->run($productId, [],10);
        // dd($commentMedias);
        // return $comments;
        return $this->transform($combineData, CommentStatisticTransfomer::class, [],  [
            'message' => 'Success',
            'status' => true
        ], 'product_options');
    }
}
