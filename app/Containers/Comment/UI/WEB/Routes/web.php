<?php 

$router->group([
    'prefix' => 'comments',
    'namespace' => '\App\Containers\Comment\UI\WEB\Controllers\FrontEnd',
    'middleware' => [
      'web'
    ],
    'domain' => parse_url(config('app.url'))['host']
  ], function () use ($router) {
    $router->any('posts', [
        'as' => 'frontend.comment.post',
        'uses' => 'CommentController@post'
    ]);
});

