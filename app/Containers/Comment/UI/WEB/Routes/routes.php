<?php

$router->group([
  'prefix' => 'comments',
  'namespace' => '\App\Containers\Comment\UI\WEB\Controllers\Admin',
  'middleware' => [
    'auth:admin'
  ],
  'domain' => sprintf('admin.%s', parse_url(config('app.url'))['host'])
], function () use ($router) {
  $router->get('/', [
    'as' => 'admin.comments.index',
    'uses' => 'CommentController@index',
  ]);

  $router->delete('/{id}', [
    'as' => 'admin.comments.delete',
    'uses' => 'CommentController@delete',
  ]);

  $router->put('/delete-display/{id}', [
    'as' => 'admin.comments.delete-display',
    'uses' => 'CommentActionController@deleteDisplay',
  ]);

  $router->get('{id}/edit', [
    'as' => 'admin.comments.edit',
    'uses' => 'CommentController@edit',
  ]);

  $router->put('{id}', [
    'as' => 'admin.comments.update',
    'uses' => 'CommentController@update',
  ]);

  $router->get('{id}', [
    'as' => 'admin.comments.show',
    'uses' => 'CommentController@show',
  ]);

  $router->post('load-more', [
    'as' => 'admin.comments.getCommentExceptId',
    'uses' => 'CommentActionController@loadMore',
  ]);

  $router->post('approve', [
    'as' => 'admin.comments.approve',
    'uses' => 'CommentActionController@approve',
  ]);

  $router->get('get-reply/{id}', [
    'as' => 'admin.comments.get-reply',
    'uses' => 'CommentActionController@getReply',
  ]);

  $router->post('reply', [
    'as' => 'admin.comments.reply',
    'uses' => 'CommentActionController@reply',
  ]);

  $router->post('issues-handle', [
    'as' => 'admin.comments.issues-handle',
    'uses' => 'CommentActionController@issuesHandler',
  ]);
}); // End groups comment


$router->group([
  'prefix' => 'bad-word',
  'namespace' => '\App\Containers\Comment\UI\WEB\Controllers\Admin',
  'middleware' => [
    'auth:admin'
  ],
  'domain' => sprintf('admin.%s', parse_url(config('app.url'))['host'])
], function () use ($router) {
  $router->get('/', [
    'as' => 'admin.comments.bad_word.index',
    'uses' => 'CommentBadWordController@index',
  ]);

  $router->get('create', [
    'as' => 'admin.comments.bad_word.create',
    'uses' => 'CommentBadWordController@create',
  ]);

  $router->post('store', [
    'as' => 'admin.comments.bad_word.store',
    'uses' => 'CommentBadWordController@store',
  ]);

  $router->get('{id}/edit', [
    'as' => 'admin.comments.bad_word.edit',
    'uses' => 'CommentBadWordController@edit',
  ]);

  $router->put('{id}', [
    'as' => 'admin.comments.bad_word.update',
    'uses' => 'CommentBadWordController@update',
  ]);

  $router->delete('{id}', [
    'as' => 'admin.comments.bad_word.delete',
    'uses' => 'CommentBadWordController@delete',
  ]);

  $router->post('/generate', [
    'as' => 'admin.comments.bad_word.generate',
    'uses' => 'CommentBadWordController@generateLanguageFile'
  ]);
});
