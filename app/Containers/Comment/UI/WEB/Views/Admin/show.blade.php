@extends('basecontainer::admin.layouts.default_for_iframe')
@section('content')
<div class="row" id="sectionContent">
  <div class="col-12">
    <div class="card mb-0">
      <div class="card-header">
        <button class="btn btn-link">Xem chi tiết cuộc hội thoại</button>
        <div class="d-flex ml-auto">
          <button type="button" class="btn btn-secondary mr-2" onclick='return closeFrame(@bladeJson($comment))'>Đóng lại</button>
        </div>
      </div>

      <div class="card-body">
        @include('comment::inc.child-item', ['coment' => $comment, 'isNotShowAction' => true])
      </div>
    </div>
  </div>
</div>
@endsection
