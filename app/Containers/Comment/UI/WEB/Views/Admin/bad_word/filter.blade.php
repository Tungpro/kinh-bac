<form action="{{ route('admin.comments.bad_word.index') }}" method="GET" id="searchForm">
  <div class="card">
      <div class="card-body">
          <div class="row">
                <div class="form-group col-sm-3">
                  <div class="input-group">
                      <div class="input-group-prepend">
                          <span class="input-group-text">ID</span>
                      </div>

                      <input  type="number" 
                              class="form-control" 
                              name="id" 
                              value="{{ $request->id }}"
                              placeholder="Nhập ID"
                      />
                  </div>
              </div>
              
              <div class="form-group col-sm-3">
                  <div class="input-group">
                      <div class="input-group-prepend" title="{{ __('faq::message.keyword') }}">
                          <span class="input-group-text"><i class="fa fa-filter"></i></span>
                      </div>
  
                      <input  type="text" 
                              class="form-control" 
                              name="bad_word" 
                              value="{{ $request->bad_word }}"
                              placeholder="{{ __('faq::message.keyword_input') }}"
                      />
                  </div>
              </div>

              <div class="form-group col-sm-3">
                <div class="input-group">
                    <div class="input-group-prepend" title="{{ __('faq::message.keyword') }}">
                        <span class="input-group-text"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i></span>
                    </div>

                    <select name="sort_by" id="sort_by" class="form-control">
                      <option value="">---Sắp xếp theo---</option>
                      @foreach ($badWordsSortBy as $sortKey => $sortText)
                        <option value="{{ $sortKey }}" @if (optional($request)->sort_by == $sortKey) selected @endif>
                          {{  $sortText }}
                        </option>
                      @endforeach
                    </select>
                </div>
            </div>
          </div><!-- /.row -->
      </div><!-- /.card-body -->
  
      <div class="card-footer">
          <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> {{ __('faq::message.search') }}</button>
          <a class="btn btn-sm btn-primary" href="{{ route('admin.comments.bad_word.index') }}"><i class="fa fa-refresh"></i> {{ __('faq::message.reset') }}</a>
          
          @can('add-badword')
            <a href="{{ route('admin.comments.bad_word.create') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Thêm từ</a>
          @endcan

          <button  type="button"
              class="btn btn-sm btn-success pull-right"
              onclick="return comment_bad_word.generateLanguageFile(this)">
              Xuất Data
          </button>
      </div>
  </div><!-- /.card -->
  </form>