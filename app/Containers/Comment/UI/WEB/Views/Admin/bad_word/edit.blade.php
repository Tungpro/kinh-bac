@extends('basecontainer::admin.layouts.default')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    @lang('Sửa từ cấm')
                </div>

                <div class="card-body">
                    <form action="{{ route('admin.comments.bad_word.update', ['id' => $badWord->id]) }}" method="POST" class="form-horizontal">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="">Từ cấm:</label>
                            <input  type="text" 
                                    name="bad_word" 
                                    id="bad_words" 
                                    class="form-control" 
                                    value="{{ old('bad_word', $badWord->bad_word) }}"
                                    maxlength="70"
                                    minlength="1" 
                            />
                        </div>
        

                        <button type="submit" class="btn btn-primary">Cập nhật</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection


