@extends('basecontainer::admin.layouts.default')

@section('right-breads')
{!! $badWords->appends($request->all())->links('basecontainer::admin.inc.paginator') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-5"><h1>{{ $site_title }}</h1></div>

            @include('comment::Admin.bad_word.filter')
            
            <div class="card card-accent-info">
                <div class="card-header"><i class="fa fa-align-justify"></i> Danh sách từ cấm</div>

                <table class="table table-hover table-bordered mb-0" id="tableCustomer">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Từ cấm</th>
                            <th>Ngày tạo</th>
                            <th>Ngày cập nhật</th>
                            <th>Thao tác</th>
                        </tr>
                    </thead>

                    <tbody>
                        @forelse ($badWords as $badWord)
                            <tr>
                                <td>{{ $badWord->id }}</td>
                                <td>{{ $badWord->bad_word }}</td>
                                <td>{{ $badWord->created_at }}</td>
                                <td>{{ $badWord->updated_at }}</td>
                                <td>
                                    <div class="d-inline">
                                        @can('edit-badword')
                                            <a href="{{ route('admin.comments.bad_word.edit', ['id' => $badWord->id]) }}">
                                                <i class="fa fa-pencil"></i> Sửa
                                            </a>
                                        @endcan

                                        @can('delete-badword')
                                            <form action="{{ route('admin.comments.bad_word.delete', ['id' => $badWord->id]) }}" method="POST" title="{{ __('faq::message.delete') }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="button"
                                                        class="btn btn-link p-0 text-danger"
                                                        onclick="return comment_bad_word.delete('{{ $badWord->bad_word }}', this)">
                                                        <i class="fa fa-trash text-danger"></i> Xóa
                                                </button>
                                            </form>
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                        @empty 
                            <tr>
                                <td colspan="5">{{ __('Không có dữ liệu') }}</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>

                @if ($badWords->isNotEmpty())
                    <div class="d-flex py-2">
                       <span class="ml-auto mr-3">
                            {{ __('faq::message.total') }}: {{ $badWords->count() }} 
                            {{ __('faq::message.record') }} / {{ $badWords->lastPage() }} {{ __('faq::message.page')}}
                       </span>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('css_bot_all')
    {!! \FunctionLib::addMedia('admin/css/jquery.datetimepicker.min.css') !!}
    {!! \FunctionLib::addMedia('template/libs/select2/select2.min.css') !!}
@endpush

@push('js_bot_before')
    {!! \FunctionLib::addMedia('admin/js/library/jquery.datetimepicker.min.js') !!}
    {!! \FunctionLib::addMedia('admin/js/library/select2.min.js') !!}
    {!! \FunctionLib::addMedia("admin/js/library/language/select2/i18n/{$currentLang}.js") !!}
@endpush
