@extends('basecontainer::admin.layouts.default')

@section('right-breads')
{!! $comments->appends($request->all())->links('basecontainer::admin.inc.paginator') !!}
@endsection

@section('content')
  <div class="row">
      <div class="col-sm-12 position-relative">
        <div class="mb-5"><h1>{{ $site_title }}</h1></div>

        <div class="card">
          <div class="card-header d-flex py-0">
              <ul class="nav nav-tabs nav-underline nav-underline-primary card-header-tabs" style="border-bottom: unset;border-color:unset;">
                @foreach ($commentStatusConfig as $statusId => $statusText)
                  <li class="nav-item">
                    <a  class="nav-link {{ $request->status == $statusId ? 'active' : '' }}"
                        href="{{ route('admin.comments.index', ['status' => $statusId]) }}">
                        {{ $statusText }}
                        @if ( isset($totalCommentByStatus[$statusId]) && $totalCommentByStatus[$statusId]->total_comment > 0)
                          ({{ $totalCommentByStatus[$statusId]->total_comment }})
                        @endif
                    </a>
                  </li>
                @endforeach
              </ul>
          </div>

          <div class="card-body">
            @include('comment::Admin.inc.filter')
          </div>

          <table class="table table-bordered table-hover mb-0">
            <thead>
                <tr>
                    <th style="width: 5%;">ID</th>
                    <th>Đánh giá</th>
                    <th>Rating</th>
                    <th style="width: 25%;">Sản phẩm</th>
                    {{-- <th style="width: 10%;">Thao tác</th> --}}
                </tr>
            </thead>

            <tbody>
              @forelse ($comments as $comment)
                @include('comment::Admin.inc.table-item', ['coment' => $comment])
              @empty
                <tr>
                  <td colspan="4" class="text-center">Không có dữ liệu bình luận</td>
                </tr>
              @endforelse
            </tbody>
          </table>
        </div>
      </div>
  </div>
@endsection
@push('css_bot_all')
    <style>
      .tales {
        width: 100%;
      }
      .carousel-inner{
        width:100%;
        max-height: 200px !important;
      }
      .wd-cls{
        width: 220px;
      }
    </style>
@endpush
@push('js_bot_all')
  {!! \FunctionLib::addMedia('admin/js/comment/comment-construction.js') !!}
  {!! \FunctionLib::addMedia('admin/js/comment/comment_bad_word.js') !!}
@endpush


