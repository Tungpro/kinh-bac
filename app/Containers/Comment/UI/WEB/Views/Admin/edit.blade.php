@extends('basecontainer::admin.layouts.default_for_iframe')
@section('content')
<div class="row" id="sectionContent">
  <div class="col-12">
    <div class="card mb-0">
      <form action="{{ route('admin.comments.update', ['id' => $comment->id]) }}" method="POST">
        @method('PUT')
        @csrf
        <div class="card-header d-flex">
          <button class="btn btn-link">Sửa nội dung bình luận</button>
          <div class="d-flex ml-auto">
            <button type="button" class="btn btn-secondary mr-2" onclick='return closeFrame(@bladeJson($comment))'>Đóng lại</button>
            <button type="submit" class="btn btn-primary">Lưu thay đổi</button>
          </div>
        </div>

        <div class="card-body">
            <div class="form-group">
              <label for="">Tiêu đề</label>
              <input type="text" name="title" class="form-control" value="{{ old('title', $comment->title) }}">
            </div>

            <div class="form-group">
              <label for="">Nội dung bình luận</label>
              <textarea name="comment"
                        class="form-control"
                        cols="30"
                        rows="10"
                        placeholder="Nhập nội dung bình luận....">{{ old('comment', $comment->comment) }}</textarea>
            </div>

            <div class="form-group">
              <label for="">Sao đánh giá</label>
              @for($i=1; $i<=5; $i++)
                <div class="form-check">
                  <input  class="form-check-input"
                          type="radio"
                          name="rate_point"
                          value="{{ $i }}"
                          id="markStart{{$i}}"
                          @if($comment->rate_point == $i) checked @endif>
                  <label class="form-check-label" for="markStart{{$i}}">
                    @for($j=1; $j<=$i; $j++)
                      <i class="fa fa-star mark-star"></i>
                    @endfor
                  </label>
                </div>
              @endfor
            </div>

            <div class="form-group">
              <label for="" class="font-weight-bold text-success">
                <i class="fa fa-check"></i> Duyệt bình luận (Các bình luận chỉ được hiển thị nếu được duyệt thành công)
              </label>
              <div class="form-check">
                <input  class="form-check-input"
                        type="radio"
                        name="approved"
                        value="1"
                        id="approveComment"
                        @if($comment->approved == 1) checked @endif>
                <label class="form-check-label" for="approveComment">
                  Duyệt bình luận
                </label>
              </div>

              <div class="form-check">
                <input  class="form-check-input"
                        type="radio"
                        name="approved"
                        value="0"
                        id="unApproveComment"
                        @if($comment->approved == 0) checked @endif>
                <label class="form-check-label" for="unApproveComment">
                  Không duyệt bình luận
                </label>
              </div>
            </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
