@extends('basecontainer::admin.layouts.default_for_iframe')
@section('content')
<div class="row" id="sectionContent">
  <div class="col-12">
    <div class="card mb-0">
      <form action="{{ route('admin.comments.post-reply', ['reply_to' => $replyToComment->id]) }}" method="POST">
        @csrf
        <div class="card-header d-flex">
          <button class="btn btn-link">Trả lời bình luận: {{ $replyToComment->title }}</button>
          <div class="d-flex ml-auto">
            <button type="button" class="btn btn-secondary mr-2" onclick='return closeFrame(@bladeJson($replyToComment))'>Đóng lại</button>
            <button type="submit" class="btn btn-primary">Trả lời</button>
          </div>
        </div>

        <div class="card-body">
          <div class="media m-2 comment-item" data-id="{{ $replyToComment->id }}">
            <img class="mr-3" src="https://via.placeholder.com/45" alt="Generic placeholder image">
            <div class="media-body">
              <div class="d-flex">
                <div class="col">
                  <h6 class="mt-0 mb-0">
                    @if ($replyToComment->user)
                      <i class="fa fa-user text-warning"></i>
                      @php($userEmail = optional($replyToComment->user)->email)
                      <a href="mailto::{{ $userEmail }}" class="text-warning">
                        <i>{{ $userEmail }}</i>
                      </a>
                    @elseif ($replyToComment->customer)
                      @php($userCustomer = optional($replyToComment->customer)->email)
                      <i class="fa fa-address-card text-info font-weight-bold" aria-hidden="true"></i>
                      <a href="mailto::{{ $userCustomer }}" class="text-info">
                        <i>{{ $userCustomer }}</i>
                      </a>
                    @else
                    <i class="fa fa-user-secret" aria-hidden="true"></i> Vãng lai
                    @endif
                    > {{ $replyToComment->title ?? 'Không có tiêu đề' }}

                      <span>
                        @if ($replyToComment->isApproved())
                          <i class="fa fa-eye text-success" data-toggle="tooltip" data-placement="top" title="Đang hiển thị"></i>
                        @else
                          <i class="fa fa-eye-slash" data-toggle="tooltip" data-placement="top" title="Không hiển thị"></i>
                        @endif
                      </span>
                  </h6>

                  {{ $replyToComment->comment }}.

                  @if (!empty($replyToComment->childrens_count) && $replyToComment->childrens_count > 3 && $replyToComment->threeChildrens)
                    <p>
                      @php($exceptIds = $replyToComment->threeChildrens->pluck('id')->toArray())
                      <a  class="text-primary"
                          href="javascript:void(0)"
                          onclick="loadMoreComment('{{ $replyToComment->id }}', this)">
                          Tải thêm bình luận
                      </a>
                    </p>
                  @endif
                </div>

                <div class="ml-auto mr-2">
                  <p class="mb-0">
                    @if (isset($input['approved']) || $replyToComment->isParentComment())
                        <i class="fa fa-cube" aria-hidden="true"></i>
                        <a href="#">{{ optional($replyToComment->product->desc)->name }}</a> |
                    @endif

                    @if (!$replyToComment->isCommentOfUser())
                      @for($i=1; $i<=5; $i++)
                        @if ($replyToComment->rate_point >= $i)
                          <i class="fa fa-star mark-star"></i>
                        @else
                        <i class="fa fa-star-o"></i>
                        @endif
                      @endfor
                      &nbsp; |
                    @endif

                    <em>{{ $replyToComment->created_at}}</em>
                  </p>
                </div>
              </div>

              <hr>
              <div id="wrap" class="row container m-2">
                <div class="col-12">
                  <div class="form-group">
                    <label for="">Tiêu đề</label>
                    <input type="text" name="title" class="form-control" value="{{ old('title', '') }}">
                  </div>

                  <div class="form-group">
                    <label for="">Nội dung bình luận</label>
                    <textarea name="comment"
                              class="form-control"
                              cols="30"
                              rows="10"
                              placeholder="Nhập nội dung bình luận....">{{ old('comment', '') }}</textarea>
                  </div>

                  <div class="form-group">
                    <label for="" class="font-weight-bold text-success">
                      <i class="fa fa-check"></i> Hiển thị
                    </label>
                    <div class="form-check">
                      <input  class="form-check-input"
                              type="radio"
                              name="approved"
                              value="1"
                              id="approveComment"
                              @if($replyToComment->approved == 1) checked @endif>
                      <label class="form-check-label" for="approveComment">
                        Có hiển thị
                      </label>
                    </div>

                    <div class="form-check">
                      <input  class="form-check-input"
                              type="radio"
                              name="approved"
                              value="0"
                              id="unApproveComment"
                              @if($replyToComment->approved == 0) checked @endif>
                      <label class="form-check-label" for="unApproveComment">
                        Không hiển thị
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </div>
      </form>
    </div>
  </div>
</div>
@endsection
