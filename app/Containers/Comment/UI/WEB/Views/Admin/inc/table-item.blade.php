@if ($comment->customer)
    <tr>
        <td>
            {{-- <div class="form-check">
                <input type="checkbox" class="mx-auto">
            </div> --}}
            {{$comment->id}}
        </td>
        <td>
            @if ($comment->image_medias_count > 0 || $comment->video_medias_count > 0 )
                <p class="d-flex">
                    @if ($comment->image_medias_count > 0)
                        <span class="mr-4">
                            <i class="fa fa-file-image-o" aria-hidden="true"></i> {{ $comment->image_medias_count }}
                        </span>
                    @endif

                    @if ($comment->video_medias_count > 0)
                        <span>
                            <i class="fa fa-video-camera" aria-hidden="true"></i> {{ $comment->video_medias_count }}
                        </span>
                    @endif
                </p>
                <p>
                  <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample{{$comment->id}}" role="button" aria-expanded="false" aria-controls="collapseExample{{$comment->id}}">
                      Ảnh và Media (Xem thêm)
                  </a>
                </p>
                <div class="collapse" id="collapseExample{{$comment->id}}">
                  @if(!empty($comment->imageMedias)  && count($comment->imageMedias) > 0)
                  <div class="card card-body wd-cls">
                      <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                        @foreach ($comment->imageMedias as $key => $items)
                          <div @if($key==0) class="carousel-item active" @else class="carousel-item" @endif >
                            <img class="d-block" style="width:220px;height:220px" src="{{ \ImageURL::getImageUrl($items->source, 'comment', 'large') }}">
                          </div>
                        @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleSlidesOnly" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleSlidesOnly" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                    </div>
                  </div>
                  @endif
                  @if(!empty($comment->videoMedias) && count($comment->videoMedias) > 0)
                  <div class="card card-body">
                    @foreach ($comment->videoMedias as $key => $items)
                    <iframe width="360" height="215" src="https://www.youtube.com/embed/{{\StringLib::getYoutubeID($items->source)}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    @endforeach
                  </div>
                  @endif
                </div>
            @endif

            <div>
                <div class="card border-primary">
                    <div class="card-body">
                        {!! $comment->content !!}
                        {{-- @if ($comment->getCommentLength() <= $comment->commentLimitLength)

                                {!! $comment->content !!}
                        @else
                            {!! \Str::limit($comment->content, $comment->commentLimitLength) !!}
                            <input type="hidden" class="comment-full-content" value='@bladeJson($comment->content)'>
                            <span   class="text-info cursor-pointer"
                                    onclick="return comment.showFullContent(this)">
                                    Xem thêm <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </span>
                        @endif --}}
                    </div>
                </div>
            </div>
            <p class="d-flex">
                <span>
                    <img src="{{ $comment->customer->getAvatarImage('avatar') }}"
                        style="width: 30px; height: 30px;"
                        class="rounded-circle"
                    />

                    {!! $comment->getCommentAuthorInfo() !!}

                    <span>&#8226; {{$comment->created_at }}</span>
                    {{-- <span>&#8226; {{ $comment->formatDate($comment->created_at) }}</span> --}}
                </span>
            </p>

            @if ($comment->threeChildrens->isNotEmpty())
                <div class="ml-4 pl-2">
                    @foreach ($comment->threeChildrens as $replyComment)
                        @if ($replyComment->customer)
                            {!! $replyComment->content !!}

                            <p class="d-flex">
                                <img src="{{ $replyComment->customer->getAvatarImage('avatar') }}"
                                    style="width: 30px; height: 30px;"
                                    class="rounded-circle"
                                />

                                <span class="ml-2">
                                    {!! $replyComment->getCommentAuthorInfo() !!}

                                    {{-- <span>&#8226; {{ $replyComment->formatDate($replyComment->created_at) }}</span> --}}
                                    <span>&#8226; {{ $replyComment->created_at }}</span>
                                </span>
                            </p>
                        @endif
                    @endforeach
                </div>
            @endif
        </td>
        <td>
            {{$comment->rating}}
        </td>
        <td>
            @if ($comment->product->isActive())
                <a target="_blank" href="{{ $comment->product->getLinkDetail() }}">
                    {{ optional($comment->product)->desc->name }}
                </a>
            @else
                {{ optional($comment->product)->desc->name }}
            @endif
        </td>
        <td>
            @if ($comment->status == 1)
            <div>
                <form action="{{ route('admin.comments.approve') }}" method="POST">
                    @csrf
                    <button class="btn">
                        <span><i class="fa fa-check" aria-hidden="true"></i>Duyệt</span>
                    </button>
                    <input type="hidden" value="{{ $comment->id }}" name="id">
                </form>
            </div>
            @endif
            @if (!$comment->isDeletedComment())
            <div>
                {{-- @can('comment-del-display') --}}
                <form action="{{ route('admin.comments.delete-display', ['id' => $comment->id]) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <button type="button" class="btn btn-link p-0" onclick="return comment.delete(this);">
                        <i class="fa fa-trash"></i> Xóa
                    </button>
                </form>
                {{-- @endcan --}}
            </div>
            @endif
        </td>
    </tr>
@endif

