<form action="{{ route('admin.comments.index') }}" method="GET" class="form-inline">
    <div class="form-group col-3 pl-0">
        <input type="text" class="form-control w-100" placeholder="Tìm kiếm đánh giá" name="comment_content"
            value="{{ optional($request)->comment_content }}" />
    </div>

    <div class="form-group mx-2">
        <input type="text" class="form-control datetimepicker" autocomplete="off" placeholder="Thời gian đánh giá" name="created_at"
            value="{{ optional($request)->created_at }}" />
    </div>

    <input type="hidden" name="status" value="{{ $request->status }}">
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Tìm</button>
    </div>
</form>
@push('js_bot_all')
    <script>
        $('.datetimepicker').datetimepicker({
            format: 'd/m/Y',
            timepicker: false
        });
    </script>
@endpush
