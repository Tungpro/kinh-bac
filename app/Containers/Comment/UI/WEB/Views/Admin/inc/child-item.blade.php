<div class="media m-2 comment-item" data-id="{{ $comment->id }}">
  <img class="mr-3" src="https://via.placeholder.com/45" alt="Generic placeholder image">
  <div class="media-body">
    <div class="d-flex">
      <div class="col">
        <h6 class="mt-0 mb-0">
          @if ($comment->user)
            <i class="fa fa-user text-warning"></i>
            @php($userEmail = optional($comment->user)->email)
            <a href="mailto::{{ $userEmail }}" class="text-warning">
              <i>{{ $userEmail }}</i>
            </a>
          @elseif ($comment->customer)
            @php($userCustomer = optional($comment->customer)->email)
            <i class="fa fa-address-card text-info font-weight-bold" aria-hidden="true"></i>
            <a href="mailto::{{ $userCustomer }}" class="text-info">
              <i>{{ $userCustomer }}</i>
            </a>
          @else
          <i class="fa fa-user-secret" aria-hidden="true"></i> Vãng lai
          @endif
          > {{ $comment->title ?? 'Không có tiêu đề' }}

            <span>
              @if ($comment->isApproved())
                <i class="fa fa-eye text-success" data-toggle="tooltip" data-placement="top" title="Đang hiển thị"></i>
              @else
                <i class="fa fa-eye-slash" data-toggle="tooltip" data-placement="top" title="Không hiển thị"></i>
              @endif

              @if ($comment->isIssuesHandled())
                <i class="fa fa-check-square-o text-success ml-2" data-toggle="tooltip" data-placement="top" title="Đã xử lý than phiền"></i>
              @endif
            </span>
        </h6>

        {{ trim($comment->comment) }}.

        @if (!empty($comment->childrens_count) && $comment->childrens_count > 3 && $comment->threeChildrens)
          <p>
            @php($exceptIds = $comment->threeChildrens->pluck('id')->toArray())
            <a  class="text-primary"
                href="javascript:void(0)"
                onclick="loadMoreComment('{{ $comment->id }}', this)">
                Tải thêm bình luận
            </a>
          </p>
        @endif
      </div>

      <div class="ml-auto mr-2">
        <p class="mb-0">
          @if (isset($input['approved']) || $comment->isParentComment())
              <i class="fa fa-cube" aria-hidden="true"></i>
              <a href="#">Test AA</a> |
          @endif

          @if (!$comment->isCommentOfUser())
            @for($i=1; $i<=5; $i++)
              @if ($comment->rate_point >= $i)
                <i class="fa fa-star mark-star"></i>
              @else
              <i class="fa fa-star-o"></i>
              @endif
            @endfor
            &nbsp; |
          @endif

          <em>{{ $comment->created_at}}</em>
        </p>

        <div class="btn-group pull-right mr-1">
          @dropdown('Chọn')
          <div class="dropdown-menu">
              <input type="hidden" class="comment-item-hidden" value='@bladeJson($comment)'>
              <a  class="dropdown-item text-info"
                  href="javascript:void(0)"
                  onclick="return loadIframe(this, '{{ route('admin.comments.edit', ['id' => $comment->id]) }}')">
                <i class="fa fa-pencil mr-2"></i> Sửa
              </a>

              @if (!$comment->isMyComment())
                <a  class="dropdown-item text-success"
                    href="javascript:void(0)"
                    onclick="return loadIframe(this, '{{ route('admin.comments.get-reply', ['id' => $comment->id]) }}')">
                    <i class="fa fa-reply mr-2"></i> Trả lời
                </a>
              @endif

              @php($confirmApprove = sprintf('%s bình luận này?', $comment->getOppositeApprovedText()))
              <a  class="dropdown-item text-muted"
                  href="javascript:void(0)"
                  onclick="return confirmAction(this, '{{ route('admin.comments.toggle-approve')}}', '{{ $confirmApprove }}')">
                <i class="fa fa-eye{{ $comment->isApproved() ? '-slash text-muted' : ' text-success' }} mr-2"></i> {{ $comment->getOppositeApprovedText() }}
              </a>

              @if ($comment->isParentComment())
                <a  class="dropdown-item text-muted"
                    href="javascript:void(0)"
                    onclick="return confirmAction(this, '{{ route('admin.comments.issues-handle')}}', 'Đánh dấu là xử lý xong nếu có than phiền?')">
                  <i class="fa fa-check-square-o mr-2"></i> Đánh dấu là xử lý xong nếu có than phiền
                </a>

                <a  class="dropdown-item text-muted"
                    onclick="return loadIframe(this, '{{ route('admin.comments.show', ['id' => $comment->id]) }}')">
                  <i class="fa fa-eye mr-2"></i> Xem nội dung cuộc hội thoại
                </a>
              @endif


              <a  class="dropdown-item text-danger"
                  href="#"
                  onclick="return deleteComment(this, '{{ route('admin.comments.delete', ['id' => $comment->id]) }}')">
                <i class="fa fa-trash mr-2"></i> Xóa
              </a>
            </div>
        </div>
      </div>
    </div>

    @if (empty($comment->parent_id))
      @foreach ($comment->threeChildrens as $child)
        @include('comment::inc.child-item', ['comment' => $child])
        @if (!$loop->last)
          <hr class="mt-0 mb-1">
        @endif
      @endforeach
    @endif
  </div>
</div>
