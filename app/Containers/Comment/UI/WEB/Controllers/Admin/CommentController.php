<?php

namespace App\Containers\Comment\UI\WEB\Controllers\Admin;

use Illuminate\Support\Arr;
use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\WebController;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Containers\Comment\UI\WEB\Requests\EditCommentRequest;
use App\Containers\Comment\UI\WEB\Requests\DeleteCommentRequest;
use App\Containers\Comment\UI\WEB\Requests\UpdateCommentRequest;
use App\Containers\Comment\UI\WEB\Requests\GetAllCommentsRequest;
use App\Containers\Comment\UI\WEB\Requests\FindCommentByIdRequest;
use App\Containers\Construction\Models\Construction;
use App\Containers\Customer\Models\Customer;

/**
 * Class Controller
 *
 * @package App\Containers\Comment\UI\WEB\Controllers
 */
class CommentController extends AdminController
{
  use ApiResTrait;
  public function __construct()
  {
    if (FunctionLib::isDontUseShareData(['store', 'update', 'delete', 'edit'])) {
      $this->dontUseShareData = true;
    }

    parent::__construct();
  }

  /**
   * Show all entities
   *
   * @param GetAllCommentsRequest $request
   */
  public function index(GetAllCommentsRequest $request)
  {
    Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);


    $language_id = Apiato::call('Localization@GetDefaultLanguageTask')->language_id ?? 1;

    $comments = Apiato::call('Comment@GetAllCommentsAction', [
      $request,
      [
        'customer:id,fullname,avatar,email,is_contractor',
        'user:id,name',
        'product',
        'product.desc' => function ($query) use ($language_id) {
          $query->select('id', 'product_id', 'name','slug', 'short_description', 'meta_title');
          $query->whereHas('language', function ($q) use ($language_id) {
              $q->where('language_id', $language_id);
          });
      },
        'threeChildrens',
        'threeChildrens.customer'
      ],
    ]);

    $customer = app(Customer::class);
    if ($request->customer_id) {
      $customer = Apiato::call('Customer@FindCustomerByIdAction', [$request->customer_id]);
    }

    $totalCommentByStatus = Apiato::call('Comment@CountCommentStatusAction', []);
    return view('comment::Admin.index', [
      'comments' => $comments,
      'request' => $request,
      'customer' => $customer,
      'totalCommentByStatus' => $totalCommentByStatus,
      'commentStatusConfig' => config('comment-container.status'),
      'site_title' => __('Quản lý bình luận'),
    ]);
  }

  /**
   * Show one entity
   *
   * @param FindCommentByIdRequest $request
   */
  public function show(FindCommentByIdRequest $request)
  {

    $transporter = $request->toTransporter();
    $comment = Apiato::call('Comment@FindCommentByIdAction', [
      $transporter,
      [
        'product:id',
        'product.desc:id,product_id,language_id,name',
        'user:id,email,name',
        'customer:id,fullname,email',
        'threeChildrens.user:id,email,name',
        'threeChildrens.customer:id,fullname,email',
      ]
    ]);
    if ($request->ajax()) {
      return $this->sendResponse($comment);
    }
    return view('comment::Admin.show', ['comment' => $comment]);
  }


  /**
   * Edit entity (show UI)
   *
   * @param EditCommentRequest $request
   */
  public function edit(EditCommentRequest $request)
  {
    $comment = Apiato::call('Comment@GetCommentByIdAction', [$request]);
    return view('comment::Admin.edit', [
      'comment' => $comment
    ]);
  }

  /**
   * Update a given entity
   *
   * @param UpdateCommentRequest $request
   */
  public function update(UpdateCommentRequest $request)
  {
    $transporter = $request->toTransporter();
    $comment = Apiato::call('Comment@UpdateCommentAction', [$transporter]);
    return redirect()->back()->with([
      'flash_level' => 'success',
      'flash_message' => 'Cập nhật nội dung bình luận thành công'
    ]);
  }

  /**
   * Delete a given entity
   *
   * @param DeleteCommentRequest $request
   */
  public function delete(DeleteCommentRequest $request)
  {
    $result = Apiato::call('Comment@DeleteCommentAction', [$request]);
    return $this->sendResponse($result, __('Xóa bình luận thành công'));
  }
} // End class
