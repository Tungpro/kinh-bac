<?php

namespace App\Containers\Comment\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\WebController;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Containers\Comment\Jobs\GenerateLanguageFileJob;
use App\Containers\Comment\UI\WEB\Requests\EditCommentBadWordRequest;
use App\Containers\Comment\UI\WEB\Requests\StoreCommentBadWordRequest;
use App\Containers\Comment\UI\WEB\Requests\DeleteCommentBadWordRequest;
use App\Containers\Comment\UI\WEB\Requests\GenerateLanguageFileRequest;
use App\Containers\Comment\UI\WEB\Requests\GetAllCommentBadWordRequest;
use App\Containers\Comment\UI\WEB\Requests\UpdateCommentBadWordRequest;

/**
 * Class CommentBadWordController
 *
 * @package App\Containers\Comment\UI\WEB\Controllers
 */
class CommentBadWordController extends AdminController
{
    use ApiResTrait;

    public $title;

    public function __construct()
    {
      $this->title = __('Từ cấm');

      if (FunctionLib::isDontUseShareData(['store', 'update', 'delete'])) {
        $this->dontUseShareData = true;
      }

      parent::__construct();
    }

    public function index(GetAllCommentBadWordRequest $request) {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);
        $badWords = Apiato::call('Comment@GetAllCommentBadWordAction', [$request]);
        return view('comment::Admin.bad_word.index', [
            'badWords' => $badWords,
            'request' => $request,
            'badWordsSortBy' => config('comment-container.bad_words.sort_by')
        ]);
    }

    public function create() {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['add', $this->title, 'admin.comments.bad_word.index']);

        return view('comment::Admin.bad_word.create');
    }

    public function store(StoreCommentBadWordRequest $request) {
        Apiato::call('Comment@StoreCommentBadWordAction', [$request]);
        return redirect()->back()->with([
            'flash_level' => 'success',
            'flash_message' => 'Thêm các từ cấm thành công'
        ]);
    }

    public function edit(EditCommentBadWordRequest $request) {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['edit', $this->title, 'admin.comments.bad_word.index']);

        $badWord = Apiato::call('Comment@FindCommentBadWordByIdAction', [$request]);
        
        return view('comment::Admin.bad_word.edit', [
            'badWord' => $badWord
        ]);
    }

    public function update(UpdateCommentBadWordRequest $request) {
        Apiato::call('Comment@UpdateCommentBadWordAction', [$request]);
        return redirect()->back()->with([
            'flash_level' => 'success',
            'flash_message' => 'Cập nhật từ cấm thành công'
        ]);
    }

    public function delete(DeleteCommentBadWordRequest $request) {
        Apiato::call('Comment@DeleteCommentBadWordAction', [$request]);
        return redirect()->back()->with([
            'flash_level' => 'success',
            'flash_message' => __('Xóa từ cấm thành công')
        ]);
    }

    public function generateLanguageFile(GenerateLanguageFileRequest $request) {
        $currentUser = auth('admin')->user();
        $result = dispatch(new GenerateLanguageFileJob($currentUser));
        return $this->sendResponse($result, __('Hệ thống đã nhận được yêu cầu và sẽ gửi bạn lại kết quả của hành động này qua hệ thống thông báo'));
    }
} // End class
