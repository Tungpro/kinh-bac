<?php

namespace App\Containers\Comment\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Comment\Events\Events\ApproveCommentEvent;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Containers\Comment\UI\WEB\Requests\EditCommentRequest;
use App\Containers\Comment\UI\WEB\Requests\ApproveCommentRequest;
use App\Containers\Comment\UI\WEB\Requests\GetReplyCommentRequest;
use App\Containers\Comment\UI\WEB\Requests\LoadMoreCommentRequest;
use App\Containers\Comment\UI\WEB\Requests\ReplyCommentRequest;
use App\Containers\Comment\UI\WEB\Requests\DeleteDisplayCommentRequest;
use App\Containers\Comment\UI\WEB\Requests\IssuesHandlerCommentRequest; 

/**
 * Class CommentActionController
 *
 * @package App\Containers\Comment\UI\WEB\Controllers
 */
class CommentActionController extends AdminController
{
    use ApiResTrait;

    public function __construct()
    {
      if (FunctionLib::isDontUseShareData(['loadMore', 'approve', 'getReply'])) {
        $this->dontUseShareData = true;
      }

      parent::__construct();
    }
    /**
     * Tải thêm comments
     *
     * @param LoadMoreCommentRequest $request
     */
    public function loadMore(LoadMoreCommentRequest $request)
    {
        $transporter = $request->toTransporter();
        $comments = Apiato::call('Comment@GetChildCommentByParentIdAction', [
          $transporter,
          [
            'product:id',
            'product.desc:id,product_id,language_id,name',
            'user:id,email,name',
            'customer:id,fullname,email',
            'threeChildrens.user:id,email,name',
            'threeChildrens.customer:id,fullname,email'
          ]
        ]);

        $html = view('comment::Admin.actions.render', ['comments' => $comments])->render();
        return $this->sendResponse($html);
    }

    /**
     * Approve Comment
     *
     * @param ApproveCommentRequest $request
     */
    public function approve(ApproveCommentRequest $request)
    {
        $transporter = $request->toTransporter();
        $comment = Apiato::call('Comment@ApproveCommentAction', [$transporter, ['status' => $transporter->status]]);
        // event(new ApproveCommentEvent($comment));
        if ($request->ajax()) {
          return $this->sendResponse($comment, __('Approve Comment Successfully'));
        }
        return redirect()->back()->with([
          'flash_level' => 'success',
          'flash_message' => __('Duyệt bình luận thành công')
        ]);
    }

    /**
     * Create entity (show UI)
     *
     * @param GetReplyCommentRequest $request
     */
    public function getReply(GetReplyCommentRequest $request)
    {
      $transporter = $request->toTransporter();
      $replyToComment = Apiato::call('Comment@GetCommentByIdAction', [
        $request,
        [
          'product:id',
          'product.desc:id,product_id,language_id,name',
          'user:id,email,name',
          'customer:id,fullname,email',
          'threeChildrens.user:id,email,name',
          'threeChildrens.customer:id,fullname,email',
        ]
      ]);

      return view('comment::Admin.actions.reply', [
        'replyToComment' => $replyToComment,
        'input' => $request->all()
      ]);
    }

    /**
     * Reply the comment
     *
     * @param ReplyCommentRequest $request
     */
    public function reply(ReplyCommentRequest $request)
    {
        $transporter = $request->toTransporter();
        $transporter->user_id = auth('admin')->id();
        $comment = Apiato::call('Comment@CreateReplyCommentAction', [$transporter]);

        if ($request->ajax()) {
          return $this->sendResponse($comment, __('Trả lời bình luận thành công'));
        }
        
        return redirect()->back()->with([
          'flash_level' => 'success',
          'flash_message' => 'Trả lời bình luận thành công'
        ]);
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditCommentRequest $request
     */
    public function issuesHandler(IssuesHandlerCommentRequest $request)
    {
        $transporter = $request->toTransporter();
        $transporter->is_issues_handled = 1;
        $comment = Apiato::call('Comment@IssuesHandlerCommentAction', [$transporter]);
        return $this->sendResponse($comment, __('Đánh dấu là đã xử lý bình luận than phiền'));
    }

    public function deleteDisplay(DeleteDisplayCommentRequest $request) {
      $transporter = $request->toTransporter();
      Apiato::call('Comment@DeleteDisplayCommentByIdAction', [$transporter, ['status' => $transporter->status]]);
      return redirect()->back()->with([
        'flash_level' => 'success',
        'flash_message' => 'Xóa bình luận thành công'
      ]);
    }
} // End class
