<?php

namespace App\Containers\Comment\UI\WEB\Controllers\FrontEnd;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\WebController;
use App\Containers\Comment\UI\WEB\Requests\PostCommentRequest;

/**
 * Class Controller
 *
 * @package App\Containers\Comment\UI\WEB\Controllers
 */
class CommentController extends WebController
{
    public function post(PostCommentRequest $request) {
        dd("Comment Content: " . $request->content);
    }
} // End class
