<?php

namespace App\Containers\Comment\UI\WEB\Requests;

use App\Ship\core\Traits\HelpersTraits\SecurityTrait;
use App\Ship\Parents\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class GetAllCommentBadWordRequest.
 */
class GetAllCommentBadWordRequest extends Request
{
    use SecurityTrait;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => 'view-badword',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
       
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
       
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        $validationRules = [];

        if(!empty(request('id'))) {
            $validationRules['id'] = ['required', 'numeric'];
        }

        if (!empty(request('sort_by'))) {
            $sortBy = config('comment-container.bad_words.sort_by');
            $sortByKeys = array_keys($sortBy);
            $validationRules['sort_by'] = ['required', Rule::in($sortByKeys)];
        }

        return $validationRules;
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'bad_word' => $this->cleanXSS($this->bad_word)
        ]);
    }
}
