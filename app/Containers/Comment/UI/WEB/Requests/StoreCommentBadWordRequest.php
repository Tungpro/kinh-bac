<?php

namespace App\Containers\Comment\UI\WEB\Requests;

use App\Ship\Parents\Requests\Request;

/**
 * Class StoreCommentBadWordRequest.
 */
class StoreCommentBadWordRequest extends Request
{
    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => 'add-badword',
        'roles'       => '',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
       
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
       
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'bad_word' => ['required', 'unique:comment_badword,bad_word'],
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'bad_word' => $this->bad_word
        ]);
    }
}
