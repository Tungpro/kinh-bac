<?php

namespace App\Containers\Comment\UI\WEB\Requests;

use App\Ship\core\Traits\HelpersTraits\SecurityTrait;
use App\Ship\Parents\Requests\Request;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

/**
 * Class GetAllCommentsRequest.
 */
class GetAllCommentsRequest extends Request
{
    use SecurityTrait;
    /**
     * The assigned Transporter for this Request
     *
     * @var string
     */
    protected $transporter = \App\Containers\Comment\Data\Transporters\GetAllCommentsTransporter::class;

    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'permissions' => 'comment-list',
        'roles'       => 'admin',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [
        // 'id',
    ];

    /**
     * Defining the URL parameters (e.g, `/user/{id}`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        $commentStatusConfig = config('comment-container.status');
        $commentStatusIds = array_keys($commentStatusConfig);

        $validationRules = [
            'status' => ['required', Rule::in($commentStatusIds)]
        ];

        if ( !empty(request('customer_id')) ) {
            $validationRules['customer_id'] = ['numeric', 'min:1'];
        }

        if ( !empty(request('construction_id')) ) {
            $validationRules['construction_id'] = ['numeric', 'min:1'];
        }

        if ( !empty(request('created_at')) ) {
            $validationRules['created_at'] = ['date_format:d/m/Y'];
        }

        if ( !empty(request('comment_content')) ) {
            $validationRules['comment_content'] = ['max:255'];
        }
        
        return $validationRules;
    }

    /**
     * @return  bool
     */
    public function messages()
    {
        return [
            'created_at.date_format' => 'Định dạng ngày tháng không thỏa mãn'
        ];
    }

    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }

    protected function prepareForValidation()
    {
        $commentStatusConfig = config('comment-container.status');
        $commentStatusIds = array_keys($commentStatusConfig);
        $commentStatus = !empty($this->status) ? $this->status : Arr::first($commentStatusIds);

        $this->merge([
            // 'parent_id' => 0,
            'status' => $commentStatus,
            'content' => strip_tags($this->comment_content)
        ]);
    }
}
