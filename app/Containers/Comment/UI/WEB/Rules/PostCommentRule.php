<?php

namespace App\Containers\Comment\UI\WEB\Rules;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Validation\Rule;

class PostCommentRule implements Rule
{
    public $errorMessage;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $splitWordsLine = explode("\n", str_replace("\r", "", $value));
        $contentWords = [];

        foreach ($splitWordsLine as $word) {
            $contentWords = array_merge($contentWords, explode(' ', $word));
        }

        $badWords = array_filter($contentWords, function ($word) {
            return strlen(trim($word)) > 0;
        }); 

        $fileName = public_path('lang/bad_word_lang.json');
        $commentRulesRaw = file_get_contents($fileName);
        $commentBadWordsRules = json_decode($commentRulesRaw, true);
        
        $commentBadWordsLowerRules = array_map(function ($badWordItem) {
            return mb_strtolower($badWordItem);
        }, $commentBadWordsRules);

        
        $hasSuspectBadWord = false;

        $suspecWords = [];
        foreach ($badWords as $word) {
            $lowerWord = mb_strtolower($word);

            if ( in_array($word, $commentBadWordsRules) || in_array($lowerWord, $commentBadWordsLowerRules) )  {
                $hasSuspectBadWord = true;
                $suspecWords[] = $word;
            }
        }

        $this->errorMessage = sprintf('Nội dung: "%s" đang không hợp lệ, vui lòng sử dụng nội dung khác', implode(', ', $suspecWords));
        
        return $hasSuspectBadWord !== true;
    }   

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->errorMessage;
    }
}
