<?php

namespace App\Containers\Comment\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class CommentMediaRepository
 */
class CommentMediaRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
