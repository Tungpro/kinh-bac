<?php

namespace App\Containers\Comment\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class CommentBadWordRepository
 */
class CommentBadWordRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
