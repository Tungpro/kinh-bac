<?php
namespace App\Containers\Comment\Data\Criterias;

use Carbon\Carbon;
use App\Ship\Parents\Criterias\Criteria;
use App\Containers\Product\Models\Product;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class FilterCommentBadWordCriteria extends Criteria
{
  public $request;

  public function __construct($request)
  {
    $this->request = $request;
  }

  public function apply($model, PrettusRepositoryInterface $repository) {

    if (!empty($this->request->bad_word)) {
      $model = $model->where('bad_word', 'LIKE', '%'.$this->request->bad_word.'%');
    }

    if (!empty($this->request->id)) {
        $model = $model->where('id', $this->request->id);
    }

    if ( !empty($this->request->sort_by) ) {
        $model = $model->orderByRaw($this->request->sort_by);
    }else {
        $model = $model->orderBy('id', 'DESC');
    }

    return $model;
  }
} // End class


