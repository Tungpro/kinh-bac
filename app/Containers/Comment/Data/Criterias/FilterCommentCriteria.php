<?php
namespace App\Containers\Comment\Data\Criterias;

use Carbon\Carbon;
use App\Ship\Parents\Criterias\Criteria;
use App\Containers\Product\Models\Product;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class FilterCommentCriteria extends Criteria
{
  public $request;

  public function __construct($request)
  {
    $this->request = $request;
  }

  public function apply($model, PrettusRepositoryInterface $repository) {
    $model = $model->where('status', $this->request->status)->where('parent_id', 0);

    if (isset($this->request->parent_id)) {
      $model = $model->where('parent_id', $this->request->parent_id);
    }

    if (isset($this->request->approved)) {
      $model = $model->where('approved', $this->request->approved);
    }

    if ($this->request->rate_point) {
      $model = $model->where('rate_point', $this->request->rate_point);
    }

    if ($this->request->has_image) {
      switch($this->request->has_image) {
        case 1:
          $model = $model->has('images');
          break;

        case 2:
          $model = $model->doesntHave('images');
          break;

        default:
          // code
          break;
      }
    }

    if ($this->request->customer_id) {
      $model = $model->where('customer_id', $this->request->customer_id);
    }

    if ($this->request->created_at) {
      $createdAt = Carbon::createFromFormat('d/m/Y', $this->request->created_at);
      $model = $model->whereDate('created_at', '=', $createdAt);
    }

    if ($this->request->comment_content) {
      $model = $model->where('content', 'LIKE', '%'.trim($this->request->comment_content) .'%');
    }

    if (!empty($this->request->construction_id)) {
      $model = $model->where('construction_id', $this->request->construction_id);
    }

    if ($this->request->sortBy) {
      $model = $model->orderByRaw($this->request->sortBy);
    }

    return $model;
  }
} // End class


