<?php
namespace App\Containers\Comment\Data\Criterias;

use Carbon\Carbon;
use App\Ship\Parents\Criterias\Criteria;
use App\Containers\Product\Models\Product;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class OrderBySpecialCommentCriteria extends Criteria
{
  public $commentId;

  public function __construct(int $commentId)
  {
    $this->commentId = $commentId;
  }

  public function apply($model, PrettusRepositoryInterface $repository) {
    $model = $model->orderByRaw("FIELD(id, {$this->commentId}) DESC, created_at DESC");
    return $model;
  }
} // End class


