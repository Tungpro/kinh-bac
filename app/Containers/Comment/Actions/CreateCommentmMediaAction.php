<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Comment\Models\Comment;
use Illuminate\Support\Arr;

class CreateCommentmMediaAction extends Action
{
    public function run(Comment $comment, array $commentMediaSources=[], string $type='image')
    {
        $commentMediaData = [];

        if (!empty($commentMediaSources)) {
            foreach ($commentMediaSources as $commentMediaSrc) {
                $commentMediaData[] = [
                    'comment_id' => $comment->id, 
                    'type' => $type,
                    'source' => $commentMediaSrc,
                    'created_at' => now(),
                    'updated_at' => now()
                ];
            }

            return Apiato::call('Comment@CreateCommentMediaTask', [$commentMediaData]);
        }
    }
}
