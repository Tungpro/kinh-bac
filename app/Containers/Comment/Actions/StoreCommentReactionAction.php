<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class StoreCommentReactionAction extends Action
{
    public function run(Request $request, int $reactionerId)
    {
        $data = [
            'comment_id' => $request->comment_id,
            'reactioner_id' => $reactionerId
        ];

        $commentReaction = Apiato::call('Comment@StoreCommentReactionTask', [$data]);
        
        if ($commentReaction->wasRecentlyCreated) {
            Apiato::call('Comment@ReCacucalteTotalCommentTask', [
                $request->comment_id
            ]);
        }
        return $commentReaction;
    }
}
