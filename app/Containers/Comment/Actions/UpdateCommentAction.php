<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;

class UpdateCommentAction extends Action
{
    public function run($transporter, array $updateFields=[])
    {
        if (empty($updateFields)) {
            $payload = Arr::only($transporter->toArray(), ['title', 'comment', 'rate_point', 'approved']);
        }else {
            $payload = $updateFields;
        }
        $comment = Apiato::call('Comment@UpdateCommentTask', [$transporter->id, $payload]);
        return $comment;
    }
}
