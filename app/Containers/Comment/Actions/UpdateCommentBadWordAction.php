<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Facades\DB;

class UpdateCommentBadWordAction extends Action
{
    public function run(Request $request)
    {
        DB::beginTransaction();
        try {
            $fileName = public_path('lang/bad_word_lang.json');
            $data = $request->all();
            
            $oldBadWord = Apiato::call('Comment@FindCommentBadWordByIdTask', [$request->id]);
            
            $currentBadWord = Apiato::call('Comment@UpdateCommentBadWordTask', [$request->id, $data]);
            
            $badWordsListRules = Apiato::call('Comment@EditCommentBadWordFromFileTask', [$currentBadWord, $fileName,  $oldBadWord]);
            Apiato::call('Comment@RegenerateBadWordLanguageFileTask', [$badWordsListRules, $fileName]);

            DB::commit();
            return $currentBadWord;
        }catch(\Exception $e) {
            DB::rollBack();
        }
        
    }
}
