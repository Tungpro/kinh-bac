<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetCommentsByContractorIdAction extends Action
{
    public function run(int $contractorId, array $with=['medias'])
    {
        $constructionIds = Apiato::call('Construction@GetConstructionsByContractorIdTask', [$contractorId], [
            [
                'selectFields' => [
                    ['id']
                ]
            ]
        ]);

        $constructionIdsMerge = $constructionIds->pluck('id')->toArray();

        return Apiato::call('Comment@GetCommentsByContractorIdTask', [$constructionIdsMerge], [
            [
                'with' => [$with]
            ]
        ]);
    }
}
