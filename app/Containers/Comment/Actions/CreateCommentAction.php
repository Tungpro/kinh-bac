<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;

class CreateCommentAction extends Action
{
    public function run(array $data)
    {
        $commentData = Arr::only($data, [
            'customer_id',
            'object_id',
            'parent_id',
            'name',
            'email',
            'content',
            'status',
            'rating'
        ]);


        return $this->transactionalCall('Comment@CreateCommentTask', [$commentData]);
    }
}
