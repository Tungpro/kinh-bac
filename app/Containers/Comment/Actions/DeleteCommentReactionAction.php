<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Facades\DB;

class DeleteCommentReactionAction extends Action
{
    public function run(array $payload)
    {
        $deletedRow = Apiato::call('Comment@DeleteCommentReactionTask', [$payload]);
        if ($deletedRow > 0) {
            $comment = Apiato::call('Comment@ReCacucalteTotalCommentTask', [
                $payload['comment_id'],
                'sub'
            ]);
        }
        return $deletedRow;
    }
}
