<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Arr;

class CreateReplyCommentAction extends Action
{
  public function run(array $data)
  {
    $data = Arr::only($data, [
      'object_id',
      'parent_id',
      'content',
      'status',
      'user_id',
      'customer_id',
      'name',
      'email'
    ]);


    $comment = Apiato::call('Comment@CreateCommentTask', [$data]);

    return $comment;
  }
}
