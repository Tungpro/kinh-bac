<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-06 15:30:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-08 10:53:33
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class GetCommentsByObjectIdAction extends Action
{
    public function run(array $filters, array $with = [], int $limit = 10, $specialCommentId = null, int $currentPage = 1)
    {
        $criterias = [
            ['with' => [$with]],
            ['specialCommentOrder' => [$specialCommentId]]
        ];

        if (!empty($filters['top_comment_ids'])) {
            $criterias[] = ['notIn' => ['id', $filters['top_comment_ids']]];
        }

        return Apiato::call('Comment@GetCommentsByObjectIdTask', [
            $filters,
            '',
            $limit,
            $currentPage
        ], $criterias);
    }
}
