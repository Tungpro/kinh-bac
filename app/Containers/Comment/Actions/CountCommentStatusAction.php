<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class CountCommentStatusAction extends Action
{
    public function run(int $objectId=0)
    {
        $totalCommentStatus = Apiato::call('Comment@CountCommentStatusTask', [], [
            [
                'whereObjectId' => [$objectId]
            ]
        ]);

        if ($totalCommentStatus->isEmpty()) {
            foreach (config('comment-container.status') as $commentStatusId => $commentStatusText) {
                $totalCommentStatus->push(['status' => $commentStatusId, 'total_comment' => 0]);
            }

            $totalCommentStatus = $totalCommentStatus->keyBy('status');
        }

        return $totalCommentStatus;
    }
}
