<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetChildCommentByParentIdAction extends Action
{
    public function run($transporter, array $with=[])
    {
        $comments = Apiato::call('Comment@GetChildCommentByParentIdTask', [$transporter->id], [
          ['with' => [$with]]
        ]);

        return $comments;
    }
}
