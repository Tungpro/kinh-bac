<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Comment\Events\Events\ApproveCommentEvent;

class ApproveCommentAction extends Action
{
    public function run($transporter, array $updateFields=[])
    {
        if (empty($updateFields)) {
            $payload = $transporter->toArray();
        }else {
            $payload = $updateFields;
        } 
        $resultUpdate = $this->call('Comment@UpdateCommentTask', [$transporter->id, $payload]);

        if($resultUpdate) {
            $this->call('Comment@CalcAvgRatingPointTask',[$resultUpdate->object_id,$resultUpdate->type ?? '']);
        }

        return $resultUpdate;
    }
}
