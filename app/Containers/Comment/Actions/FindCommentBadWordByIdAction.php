<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class FindCommentBadWordByIdAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Comment@FindCommentBadWordByIdTask', [$request->id]);
    }
}
