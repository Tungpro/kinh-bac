<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Facades\DB;

class GetMyReactionByCommentIdsAction extends Action
{
    public function run(array $commentIds=[], $customerId)
    {
        $commentreactions = Apiato::call('Comment@GetMyReactionByCommentIdsTask', [
            $commentIds,
            $customerId
        ])->keyBy('comment_id');

        return $commentreactions;
    }
}
