<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Comment\Jobs\GenerateLanguageFileJob;
use App\Containers\Comment\Data\Repositories\CommentBadWordRepository;

class GenerateLanguageFileAction extends Action
{
    
    public function run(): bool
    {
        try {
            $fileName = public_path('/lang/bad_word_lang.json');
        
            if (!file_exists($fileName)) {
                $badWords = Apiato::call('Comment@GetAllCommentBadWordTask', [false]);
                Apiato::call('Comment@GenerateBadWordLanguageFileTask', [$badWords, $fileName]);
            }
            
            return true;
        }catch(\Exception $e) {
            return false;
        }
    }
}
                        