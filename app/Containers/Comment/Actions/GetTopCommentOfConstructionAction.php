<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetTopCommentOfConstructionAction extends Action
{
    public function run(int $constructionId, array $with=[], int $limit)
    {
        return Apiato::call('Comment@GetTopCommentOfConstructionTask', [
            [ 
                ['construction_id', '=', $constructionId],
                ['total_reaction', '>', 0],
                ['parent_id', '=', 0],
                ['status', '=', config('comment-container.status_define.approved')]
            ]
        ], [
            [ 'with' => [$with] ],
            [ 'limit' => [$limit] ]
        ]);
    }
}
