<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class IssuesHandlerCommentAction extends Action
{
    public function run(DataTransporter $transporter)
    {
      $payload = ['is_issues_handled' => $transporter->is_issues_handled];
      $comment = Apiato::call('Comment@UpdateCommentTask', [$transporter->id, $payload]);
      return $comment;
    }
}
