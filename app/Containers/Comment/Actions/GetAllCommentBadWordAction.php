<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllCommentBadWordAction extends Action
{
    public function run($request)
    {
        return Apiato::call('Comment@GetAllCommentBadWordTask', [], [
            [
                'filterBadwords' => [$request]
            ]
        ]);
    }
}
