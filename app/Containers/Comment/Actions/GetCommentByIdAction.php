<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetCommentByIdAction extends Action
{
    public function run(Request $request, array $with=[])
    {
        $comment = Apiato::call('Comment@FindCommentByIdTask', [$request->id], [
          ['with' => [$with]]
        ]);

        return $comment;
    }
}
