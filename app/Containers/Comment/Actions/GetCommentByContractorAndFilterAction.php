<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class GetCommentByContractorAndFilterAction extends Action
{
    public function run(int $customerId, array $condition = [], array $withData = [], $limit = 10)
    {
        return Apiato::call('Comment@GetCommentByContractorAndFilterTask', [
            $customerId, $condition , $limit
            ], [
                [
                    'filterData' => [$condition]
                ],[
                    'withData' => [ $withData ]
                ]
            ]
        );
    }
}
