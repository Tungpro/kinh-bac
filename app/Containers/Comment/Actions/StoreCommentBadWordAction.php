<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class StoreCommentBadWordAction extends Action
{
    public function run($request)
    {
        DB::beginTransaction();
        try {
            $fileName = public_path('lang/bad_word_lang.json');

            $badWord = Arr::only($request->all(), ['bad_word']);

            $badWord = Apiato::call('Comment@StoreCommentBadWordTask', [$badWord]);
            $badWordsListRules = Apiato::call('Comment@AddCommentBadWordFromFileTask', [$badWord, $fileName]);
            Apiato::call('Comment@RegenerateBadWordLanguageFileTask', [$badWordsListRules, $fileName]);

            DB::commit();
            return $badWord;
        }catch(\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        
    }
}
