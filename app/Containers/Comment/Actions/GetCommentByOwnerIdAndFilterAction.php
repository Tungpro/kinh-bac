<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class GetCommentByOwnerIdAndFilterAction extends Action
{
    public function run(int $ownerId, array $condition = [], array $withData = [], $limit = 10)
    {
        return Apiato::call('Comment@GetCommentByOwnerIdAndFilterTask', [
            $ownerId, 
            $condition ,
             $limit
        ], [
                [
                    'filterData' => [$condition]
                ],[
                    'withData' => [ $withData ]
                ]
            ]
        );
    }
}
