<?php

namespace App\Containers\Comment\Actions;

use Illuminate\Support\Arr;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteDisplayCommentByIdAction extends Action
{
    public function run($transporter, array $updateFields=[])
    {
        $currentComment = Apiato::call('Comment@FindCommentByIdTask', [$transporter->id]);
        
        if (empty($updateFields)) {
            $payload = Arr::only($transporter->toArray(), ['title', 'comment', 'rate_point', 'approved']);
        }else {
            $payload = $updateFields;
        }
        $comment = Apiato::call('Comment@UpdateCommentTask', [$transporter->id, $payload]);
        Apiato::call('User@CreateUserLogSubAction', [
            $currentComment->id,
            $currentComment->toArray(),
            $comment->toArray(),
            __('Xóa bình luận'),
            get_class($comment)
        ]);
        return $comment;
    }
}
