<?php

namespace App\Containers\Comment\Actions\FrontEnd;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;

class StoreNewsCommentAction extends Action
{
    public function run(Request $request)
    {
        return Apiato::call('Comment@CreateCommentTask', [$request->all()]);
    }
}
