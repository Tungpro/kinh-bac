<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-06 15:30:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-11 20:54:49
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\Actions\FrontEnd;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use stdClass;

class GetCommentStatisticByPrdAction extends Action
{
    public function run($filters, array $with=[], int $limit=10)
    {
        $criterias = [
            [ 'with' => [$with] ],
        ];

        if (is_array($filters)) {
            $objectId = $filters['object_id'];
        }else {
            $objectId = $filters;
        }
        
        $commentMedias = Apiato::call('Comment@GetCommentMediasByObjectIdTask', [
            $objectId, 
            '',
            $limit
        ], $criterias);


        $totalRating = Apiato::call('Comment@CountCommentStatusTask', [''], [
            [
                'whereObjectId' => [$objectId]
            ]
        ]);

        $totalRatingStar = $this->call('Comment@CountingRatingStarsTask',[$objectId]);

        for ($i = 1; $i <= 5; $i++) {
            $count = $totalRatingStar[$i]['total_rating'] ?? 0;
            $totalRatingStar['star_'.$i]['total_rating'] = $totalRatingStar[$i]['total_rating'] ?? 0;
            $totalRatingStar['star_'.$i]['rating'] = $totalRatingStar[$i]['rating'] ?? $i;
            $totalRatingStar['star_'.$i]['percent'] = $totalRating > 0 ? (int)ceil($count / $totalRating * 100) : 0;
            unset($totalRatingStar[$i]);
        }

        $combineData = new stdClass();
        $combineData->medias = $commentMedias;
        $combineData->totalRating = $totalRating;
        $combineData->totalRatingStar = $totalRatingStar;

        return $combineData;
    }
}
