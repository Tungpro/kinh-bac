<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllCommentsAction extends Action
{
    public function run($request, array $with=[])
    {
        $comments = Apiato::call('Comment@GetAllCommentsTask', [], [
          ['with' => [$with]],
          [ 'filterComments' => [$request] ]
        ]);

        return $comments;
    }
}
