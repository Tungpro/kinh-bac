<?php

namespace App\Containers\Comment\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Facades\DB;

class DeleteCommentBadWordAction extends Action
{
    public function run($request)
    {
        DB::beginTransaction();

        try {
            $fileName = public_path('lang/bad_word_lang.json');

            $currentBadWord =  Apiato::call('Comment@FindCommentBadWordByIdTask', [$request->id]);
            $badWord = Apiato::call('Comment@DeleteCommentBadWordTask', [$request->id]);
            
            $badWordsListRules = Apiato::call('Comment@RemoveCommentBadWordFromFileTask', [$currentBadWord, $fileName]);
            
            Apiato::call('Comment@RegenerateBadWordLanguageFileTask', [$badWordsListRules, $fileName]);
            DB::commit();
            return $badWord;
        }catch(\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
}
