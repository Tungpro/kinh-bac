<?php

namespace App\Containers\Comment\Jobs;

use App\Ship\Parents\Jobs\Job;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Notification\Values\NotificationStructureValue;

/**
 * Class GenerateLanguageFileJob
 */
 class GenerateLanguageFileJob extends Job
 {
    private $currentUser;

    public function __construct($currentUser)
    {
        $this->currentUser = $currentUser;
    }

    public function handle()
    {
        Apiato::call('Comment@GenerateLanguageFileAction', []);
        return Apiato::call('Notification@PushNotificationAction', [
            collect([$this->currentUser]),
            new NotificationStructureValue(
                'Bạn đã xuất file bad_word_lang.json thành công', 
                '#', 
                []
            )
        ]);
    }

 }
