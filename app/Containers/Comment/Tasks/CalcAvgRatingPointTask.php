<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Containers\EShopBizfly\Data\Repositories\ProductRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CalcAvgRatingPointTask extends Task
{
    protected $repository,$productRepository;

    public function __construct(CommentRepository $repository, ProductRepository $productRepository)
    {
        $this->repository = $repository;
        $this->productRepository = $productRepository;
    }

    public function run(int $objectId,string $objectType = '')
    {
        try {
            $result = $this->repository->selectRaw("avg(rating) as avg_rating")
                                            ->approved()
                                            ->where('object_id', $objectId)
                                            ->where('parent_id', 0)
                                            ->first()->avg_rating ?? 0;

            if($objectType) {
                // chỗ này xử lý cho các loại object type khác nhau, có thể là Product, News, vvv
            }else {
                $this->productRepository->update(['avg_rating' => (int)round($result)],$objectId);
            }

            return $result;
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }
}
