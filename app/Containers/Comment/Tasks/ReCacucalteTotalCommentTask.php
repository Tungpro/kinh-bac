<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class ReCacucalteTotalCommentTask extends Task
{

    protected $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $id, string $type='plus')
    {
        try {
            $comment =  $this->repository->lockForUpdate()->find($id);
            if ($type == 'plus') {
                return $comment->increment('total_reaction');
            }

            return $comment->decrement('total_reaction');
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
