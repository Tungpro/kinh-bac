<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentReactionRepository;
use App\Ship\Parents\Tasks\Task;

class GetMyReactionByCommentIdsTask extends Task
{

    protected $repository;

    public function __construct(CommentReactionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $commentIds, $customerId)
    {
        return $this->repository->scopeQuery(function ($query) use($commentIds, $customerId) {
            return $query->whereIn('comment_id', $commentIds)
                         ->where('reactioner_id', $customerId);
        })->get();
    }
}
