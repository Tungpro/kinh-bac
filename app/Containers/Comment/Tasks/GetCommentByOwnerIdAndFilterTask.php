<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class GetCommentByOwnerIdAndFilterTask extends Task
{
    protected $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $ownerId, array $condition = [] ,$limit)
    {
        $this->repository->whereHas('construction', function($query) use($ownerId) {
            $query->where('construction.customer_id', $ownerId);
        })->where('parent_id', 0);

        if(!empty($condition['constructionID'])){
            $this->repository->where('comment.construction_id', $condition['constructionID']);
        }

        if(!empty($condition['content'])){

            $this->repository->where('comment.content', 'LIKE' , '%'. $condition['content'] .'%');
        }

        return $this->repository->orderBy('id', 'DESC')->paginate($limit);
    }

    public function withData(array $withData = []){

        $this->repository->with($withData);
    }
} // End class
