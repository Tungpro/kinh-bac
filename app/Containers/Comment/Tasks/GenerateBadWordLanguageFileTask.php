<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentBadWordRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Collection;

class GenerateBadWordLanguageFileTask extends Task
{

    protected $repository;

    public function __construct(CommentBadWordRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Collection $badWords, string $fileName)
    {
        $badWords = $badWords->pluck('bad_word')->toArray();
        $badWordsDictionary = json_encode($badWords, JSON_UNESCAPED_UNICODE);
        
        if (!file_exists($fileName)) {
            file_put_contents($fileName, $badWordsDictionary);
        }

        return true;
    }
}
