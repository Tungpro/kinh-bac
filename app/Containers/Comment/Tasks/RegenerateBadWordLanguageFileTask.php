<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentBadWordRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class RegenerateBadWordLanguageFileTask extends Task
{

    protected $repository;

    public function __construct(CommentBadWordRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $badWordsListRules=[], string $fileName='')
    {
        $badWordsDictionary = json_encode(array_values($badWordsListRules), JSON_UNESCAPED_UNICODE);
        file_put_contents($fileName, $badWordsDictionary);

        return true;
    }
}
