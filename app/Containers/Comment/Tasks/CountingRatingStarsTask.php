<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Containers\Comment\Enums\CommentStatus;
use App\Ship\Criterias\Eloquent\WhereColumnCriteria;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CountingRatingStarsTask extends Task
{

    protected $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $objectId,string $objectType = ''): ?array
    {
        try {
            return $this->repository->selectRaw("COUNT(id) as total_rating, rating")
                ->where('object_id',$objectId)
                // ->where('type',$objectType)
                ->approved()
                ->where('parent_id',0)
                ->groupBy('rating')
                ->get()
                ->keyBy('rating')->toArray();
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }
}
