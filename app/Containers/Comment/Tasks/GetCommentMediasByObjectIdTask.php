<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-06 15:30:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-30 21:53:33
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Containers\Comment\Data\Repositories\CommentMediaRepository;
use App\Containers\Comment\Enums\CommentStatus;
use App\Ship\Criterias\Eloquent\OrderByCreationDateAscendingCriteria;

class GetCommentMediasByObjectIdTask extends Task
{

    protected $repository;

    public function __construct(CommentMediaRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $objectId = 0, string $type = '', int $limit = 10)
    {
        $this->repository->pushCriteria(new OrderByCreationDateAscendingCriteria());
        return $this->repository->where('object_id', $objectId)
            ->whereHas('comment', function ($q) {
                $q->where('status', CommentStatus::ACTIVE);
            })
            //  ->where('type',$type)
            ->paginate($limit);
    }

    public function with(array $with = []): self
    {
        $this->repository->pushCriteria(new WithCriteria($with));
        return $this;
    }
}
