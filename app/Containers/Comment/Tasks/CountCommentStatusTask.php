<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Authorization\Data\Criterias\WhereInGuardCriteria;
use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Ship\Criterias\Eloquent\WhereColumnCriteria;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CountCommentStatusTask extends Task
{

    protected $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(string $groupBy = 'status')
    {
        try {
            if($groupBy == 'status') {
                return $this->repository->selectRaw("COUNT(id) as total_comment, status")
                                        ->groupBy('status')
                                        ->get()
                                        ->keyBy('status');
            }else {
                return $this->repository->selectRaw("COUNT(id) as total_comment")
                                        ->approved()
                                        ->first()->total_comment ?? 0;
            }
        }
        catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    public function whereObjectId(int $objectId=0) {
        if (!empty($objectId)) {
            $this->repository->pushCriteria(new WhereColumnCriteria(
                [
                    'object_id' => $objectId,
                    'parent_id' => 0
                ]
            ));
        }
    }
}
