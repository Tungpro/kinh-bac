<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Ship\Criterias\Eloquent\OrderByCreationDateDescendingCriteria;
use App\Ship\Parents\Tasks\Task;

class GetCommentsByContractorIdTask extends Task
{

    protected $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $constructionIds=[])
    {
        $this->repository->pushCriteria(new OrderByCreationDateDescendingCriteria());
        return $this->repository->scopeQuery(function ($query) use ($constructionIds) {
            return $query->whereIn('construction_id', $constructionIds);
        })->where('parent_id', 0)->paginate();
    }
}
