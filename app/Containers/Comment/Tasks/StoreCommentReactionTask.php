<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentReactionRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class StoreCommentReactionTask extends Task
{

    protected $repository;

    public function __construct(CommentReactionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->firstOrCreate($data, []);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
