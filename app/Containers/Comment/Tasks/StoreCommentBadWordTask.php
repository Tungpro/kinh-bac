<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentBadWordRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class StoreCommentBadWordTask extends Task
{

    protected $repository;

    public function __construct(CommentBadWordRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $badWords=[])
    {
        try {
            return $this->repository->create($badWords);
        }
        catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }
}
