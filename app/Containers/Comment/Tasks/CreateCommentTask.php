<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Containers\Product\Data\Repositories\ProductRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Facades\DB;

class CreateCommentTask extends Task
{

    protected $repository,$productRepository;

    public function __construct(CommentRepository $repository, ProductRepository $productRepository)
    {
        $this->repository = $repository;
        $this->productRepository = $productRepository;
    }

    public function run(array $data)
    {
        try {
            $result = $this->repository->create($data);

            if($result) {
                if (isset($data['type'])) {
                    // chỗ này xử lý cho các loại object type khác nhau, có thể là Product, News, vvv
                } else {
                    
                }
            }

            return $result;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
