<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentReactionRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteCommentReactionTask extends Task
{

    protected $repository;

    public function __construct(CommentReactionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $payload)
    {
        try {
            return $this->repository->where($payload)->limit(1)->delete();
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
