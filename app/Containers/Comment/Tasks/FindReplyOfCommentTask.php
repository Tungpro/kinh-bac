<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Ship\Parents\Tasks\Task;

class FindReplyOfCommentTask extends Task
{

    protected $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $commentParentId=0)
    {
        return $this->repository->scopeQuery(function ($query) use ($commentParentId) {
            return $query->where('parent_id', $commentParentId);
        })->first();
    }
}
