<?php

namespace App\Containers\Comment\Tasks;

use Exception;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Comment\Models\CommentBadWord;
use App\Containers\Comment\Data\Repositories\CommentBadWordRepository;

class AddCommentBadWordFromFileTask extends Task
{

    protected $repository;

    public function __construct(CommentBadWordRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(CommentBadWord $currentBadWord, string $fileName=''): array
    {
        try {
            $commentRulesRaw = file_get_contents($fileName);
            $commentBadWordsRules = json_decode($commentRulesRaw, true);
            $commentBadWordsRules[] = $currentBadWord->bad_word;
            return $commentBadWordsRules;
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }
}
