<?php

namespace App\Containers\Comment\Tasks;

use Exception;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Comment\Data\Repositories\CommentMediaRepository;

class CreateCommentMediaTask extends Task
{

    protected $repository;

    public function __construct(CommentMediaRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->insert($data);
        }
        catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }
}
