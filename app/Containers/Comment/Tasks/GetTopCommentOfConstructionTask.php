<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Ship\Criterias\Eloquent\OrderByFieldCriteria;
use App\Ship\Parents\Tasks\Task;

class GetTopCommentOfConstructionTask extends Task
{

    protected $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($where)
    {
        $this->repository->pushCriteria(new OrderByFieldCriteria('total_reaction', 'DESC'));
        return $this->repository->findWhere($where);
    }
}
