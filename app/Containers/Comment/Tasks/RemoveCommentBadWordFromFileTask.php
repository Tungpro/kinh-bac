<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentBadWordRepository;
use App\Containers\Comment\Models\CommentBadWord;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class RemoveCommentBadWordFromFileTask extends Task
{

    protected $repository;

    public function __construct(CommentBadWordRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(CommentBadWord $currentBadWord, string $fileName=''): array
    {
        try {
            $commentRulesRaw = file_get_contents($fileName);
            $commentBadWordsRules = json_decode($commentRulesRaw, true);
            
            $badWordPosition = array_search($currentBadWord->bad_word, $commentBadWordsRules);

            if ($badWordPosition >= 0) {
                unset($commentBadWordsRules[$badWordPosition]);
            }
            
            return $commentBadWordsRules;
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
