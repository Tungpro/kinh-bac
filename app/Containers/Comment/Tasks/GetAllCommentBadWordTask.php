<?php

namespace App\Containers\Comment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Comment\Data\Repositories\CommentBadWordRepository;
use App\Containers\Comment\Data\Criterias\FilterCommentBadWordCriteria;

class GetAllCommentBadWordTask extends Task
{

    protected $repository;

    public function __construct(CommentBadWordRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(bool $hasPaginate=true)
    {
        if ($hasPaginate) {
            return $this->repository->paginate();
        }
        
        return $this->repository->select('bad_word')->get(); 
    }

    public function filterBadwords($request) {
        $this->repository->pushCriteria(new FilterCommentBadWordCriteria($request));
    }
}
