<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Criterias\FilterCommentCriteria;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Ship\Criterias\Eloquent\OrderByCreationDateDescendingCriteria;

class GetAllCommentsTask extends Task
{

    protected $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        $this->repository->pushCriteria(new OrderByCreationDateDescendingCriteria());
        return $this->repository->with(['imageMedias', 'videoMedias'])->withCount(['imageMedias', 'videoMedias'])->paginate();
    }

    public function filterComments($request) {
      $this->repository->pushCriteria(new FilterCommentCriteria($request));
    }
} // End class
