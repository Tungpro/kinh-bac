<?php

namespace App\Containers\Comment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Comment\Data\Repositories\CommentRepository;

class GetCommentByContractorAndFilterTask extends Task
{

    protected $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $customerId, array $condition = [] ,$limit)
    {
        $this->repository->whereHas('construction', function($query) use($customerId) {
            $query->where('construction.customer_id', $customerId)->whereIn('construction.status', [0, 1]);
        });
        
        $this->repository->where('parent_id', 0);

        if(!empty($condition['constructionID'])){

            $this->repository->where('comment.construction_id', $condition['constructionID']);
        }

        if(!empty($condition['content'])){

            $this->repository->where('comment.content', 'LIKE' , '%'. $condition['content'] .'%');
        }
        $this->repository->where('status', '!=', config('comment-container.status_define.deleted'));
        return $this->repository->orderBy('id', 'DESC')->paginate($limit);
    }

    public function withData(array $withData = []){

        $this->repository->with($withData);
    }
} // End class
