<?php

namespace App\Containers\Comment\Tasks;

use Exception;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Comment\Models\CommentBadWord;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Containers\Comment\Data\Repositories\CommentBadWordRepository;

class EditCommentBadWordFromFileTask extends Task
{

    protected $repository;

    public function __construct(CommentBadWordRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(CommentBadWord $currentBadWord, string $fileName, CommentBadWord $oldBadWord): array
    {
        try {
            $commentRulesRaw = file_get_contents($fileName);
            $commentBadWordsRules = json_decode($commentRulesRaw, true);
            
            $badWordPosition = array_search($oldBadWord->bad_word, $commentBadWordsRules);
           
            if ($badWordPosition >= 0) {
                $commentBadWordsRules[$badWordPosition] = $currentBadWord->bad_word;
            }
            
            return $commentBadWordsRules;
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
