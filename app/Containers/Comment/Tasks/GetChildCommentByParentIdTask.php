<?php

namespace App\Containers\Comment\Tasks;

use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Ship\Parents\Tasks\Task;

class GetChildCommentByParentIdTask extends Task
{

    protected $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $commentId)
    {
        return $this->repository->scopeQuery(function ($query) {
          return $query->orderBy('id', 'asc');
        })->findWhere([
          ['parent_id', '=', $commentId]
        ]);
    }

    // public function with(array $with=[]) {
    //   $this->repository->pushCriteria(new WithCriteria($with));
    // }
}
