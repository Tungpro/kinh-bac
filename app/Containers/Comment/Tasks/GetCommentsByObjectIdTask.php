<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-06 15:30:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-30 21:53:08
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\Tasks;

use App\Ship\Parents\Tasks\Task;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Containers\Comment\Data\Repositories\CommentRepository;
use App\Containers\Comment\Data\Criterias\OrderBySpecialCommentCriteria;
use App\Ship\Criterias\Eloquent\OrderByCreationDateAscendingCriteria;
use App\Ship\Criterias\Eloquent\OrderByCreationDateDescendingCriteria;

class GetCommentsByObjectIdTask extends Task
{

    protected $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $filters = [], string $type = '', int $limit = 10, int $currentPage = 1)
    {
        $result = $this->repository->pushCriteria(new OrderByCreationDateDescendingCriteria());

        if (isset($filters['star']) && !empty($filters['star'])) {
            $result->where('rating', $filters['star']);
        }

        if (isset($filters['type']) && !empty($filters['type'])) {
            $result->where('type', $filters['type']);
        }

        $result = $result->where('object_id', $filters['objectId'])
            ->where('parent_id', 0)
            ->approved()
            ->oldest('id');
        return $result->paginate($limit)->appends(request()->except(['page', '_token']));
    }

    public function with(array $with = []): self
    {
        $this->repository->pushCriteria(new WithCriteria($with));
        return $this;
    }

    public function specialCommentOrder($commentId)
    {
        if (!empty($commentId)) {
            $this->repository->pushCriteria(new OrderBySpecialCommentCriteria($commentId));
        }
    }
}
