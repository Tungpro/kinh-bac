<?php
namespace App\Containers\Comment\Traits;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Customer\Models\Customer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;
use Illuminate\Database\Query\Builder;

trait CommentReactionTrait {

    public function getMyReactionFromListComment($comments) {
        $commentIds = $this->getCommentIdsFromCollection($comments);
        return Apiato::call('Comment@GetMyReactionByCommentIdsAction', [
            $commentIds, 
            auth('customer')->id()
        ]);
    }

    public function getMyReactionFromCommentId(int $commentId) {
        return Apiato::call('Comment@GetMyReactionByCommentIdsAction', [
            [$commentId], 
            auth('customer')->id()
        ]);
    }

    public function getCommentIdsFromCollection($comments): array {
        $ids = [];

        if ($comments->isNotEmpty()) {
            foreach ($comments as $comment) {
                $ids[] = $comment->id;
                if ($comment->threeChildrens->isNotEmpty()) {
                    foreach($comment->threeChildrens as $child) {
                        $ids[] = $child->id;
                    }
                }
            }
        }

        return $ids;
    }
} // End class