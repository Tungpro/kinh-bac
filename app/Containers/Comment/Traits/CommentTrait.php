<?php
namespace App\Containers\Comment\Traits;

use App\Containers\Customer\Models\Customer;
use Illuminate\Support\Str;
use Illuminate\Database\Query\Builder;

trait CommentTrait {
    public $commentLimitLength = 200;

    public function isRecentlyComment(): bool {
        return $this->status == 1;
    }

    public function isApprovedComment(): bool {
        return $this->status == 2;
    }

    public function isDeletedComment(): bool {
        return $this->status == 3;
    }

    public function scopeRecently($query) {
        return $query->where('status', 1);
    }

    public function scopeApproved($query) {
        return $query->where('status', 2);
    }

    public function scopeDeleted($query) {
        return $query->where('status', 3);
    }

    public function getCommentLength():int {
        return strlen($this->content);
    }

    public function getCommentAuthorInfo() {
        if (!empty($this->name)) {
            return sprintf('%s (<span class="text-primary">%s</span>)', $this->name, $this->email);
        }

        return sprintf('%s (<span class="text-primary">%s</span>)', $this->customer->fullname, $this->customer->email);
    }

    public function getCommentOwnerDetailUrl(): string {
        return route('construction.detail.comments.owner_redirect', [
            'commentOwnerId' => $this->customer->id
        ]);
    }

    public function getCommentShortName(int $limit=15) {
        return Str::limit($this->name, $limit);
    }

    public function myReaction() {
        return $this->belongsToMany(Customer::class, 'comment_reaction', 'comment_id', 'reactioner_id')
                    ->wherePivot('reactioner_id', auth('customer')->id());
    }
 } // End class