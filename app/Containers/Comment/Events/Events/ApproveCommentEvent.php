<?php

namespace App\Containers\Comment\Events\Events;

use App\Containers\Comment\Models\Comment;
use App\Ship\Parents\Events\Event;
use Illuminate\Queue\SerializesModels;

/**
 * Class PostConstructionCommentEvent
 */
class ApproveCommentEvent extends Event
{
    use SerializesModels;

    public $comment;

    /**
     * ApproveCommentEvent Comment.
     *
     * @param $entity
     */
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
