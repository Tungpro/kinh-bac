<?php

namespace App\Containers\Comment\Events\Handlers;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Comment\Events\Events\ApproveCommentEvent;
use App\Containers\Construction\Actions\UpdateCommentCountConstructionAction;

/**
 * Class UpdateConstructionCommentCountEventHandler
 */
class UpdateConstructionCommentCountEventHandler
{
    public function __construct()
    {
        // Define what you need
    }

    /**
     * @param \App\Containers\Construction\Events\Events\PostConstructionCommentEvent $event
     */
    public function handle(ApproveCommentEvent $event)
    {
        try{
            app(UpdateCommentCountConstructionAction::class)->run($event->comment->construction_id);
        }catch(\Exception $e) {
            Apiato::call('Bizfly@Alert\PushTeleAlertAction', [$e]);
            return true;
        }
    }
}
