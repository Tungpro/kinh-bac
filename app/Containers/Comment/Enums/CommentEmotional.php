<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-07 16:27:51
 * @ Description: Happy Coding!
 */

namespace App\Containers\Comment\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class CommentEmotional extends BaseEnum
{
    const VERY_HAPPY = "Cực kỳ hài lòng";
    const HAPPY = "Hài lòng";
    const JUST_OK = "Bình thường";
    const NOT_OK = "Không hài lòng";
    const BAD = "Rất không hài lòng";
}
