<?php

namespace App\Containers\Comment\Providers;

use App\Containers\Comment\Events\Events\ApproveCommentEvent;
use App\Containers\Comment\Events\Handlers\UpdateConstructionCommentCountEventHandler;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class MainServiceProvider.
 *
 * The Main Service Provider of this container, it will be automatically registered in the framework.
 */
class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        ApproveCommentEvent::class => [
            UpdateConstructionCommentCountEventHandler::class
        ]
    ];

    public function boot() {
        parent::boot();
    }
}
