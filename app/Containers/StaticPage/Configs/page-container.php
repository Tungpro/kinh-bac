<?php

return [
    'positions' => [
        'home_about' => 'Trang Chủ - Giới thiệu',
        'home_project' => 'Trang Chủ - Lĩnh vực hoạt động',
        'home_news_project' => 'Trang Chủ - Các dự án nổi bật',
        'home_statistics' => 'Trang Chủ - Số liệu',
        'home_achievement' => 'Trang Chủ - Thành tích giải thưởng',

        'about_content_1' => 'Trang Giới thiệu - Nội dung',
        'about_list_2' => 'Trang Giới thiệu - Danh sách bài viết',
        'about_project_3' => 'Trang Giới thiệu - Thành tích giải thưởng',
        'about_vision' => 'Trang Giới thiệu - Tầm nhìn ,Sứ mệnh và giá trị cốt lõi',

//        'project_about' => 'Trang Các lĩnh vực hoạt động - Tiêu đề - Mô tả',
//        'testimonial_about' => 'Trang Thông tin hồi đáp cổ đông - Tiêu đề - Mô tả',

    ]
];
