<tr>
  <td>
    <div class="input-group p-1">
        @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
      <input type="text" class="form-control"
             name="staticpage_description[{{$it_lang['language_id']}}][title_child][title_child_name][]"
             id="name_{{$it_lang['language_id']}}"
             placeholder="Nhập lịch"
             value="{{@$item['title_child_name']}}"
      >
    </div>
  </td>

    <td>
        <div class="input-group p-1">
            @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
            <input type="number" class="form-control"
                   name="staticpage_description[{{$it_lang['language_id']}}][title_child][title_child_price][]"
                   id="name_{{$it_lang['language_id']}}"
                   placeholder="Nhập giá"
                   value="{{@$item['title_child_price']}}"
            >
        </div>
    </td>

    <td>
        <div class="input-group p-1">
            @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
            <input type="text" class="form-control"
                   name="staticpage_description[{{$it_lang['language_id']}}][title_child][title_child_unit][]"
                   id="name_{{$it_lang['language_id']}}"
                   placeholder="Nhập đơn vị"
                   value="{{@$item['title_child_unit']}}"
            >
        </div>
    </td>

  <td>
    <div class="input-group p-1">
        @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
      <textarea type="text" class="form-control" placeholder="Mô tả" rows="5"
                name="staticpage_description[{{$it_lang['language_id']}}][title_child][title_child_description][]" id="name_{{$it_lang['language_id']}}">{{@$item['title_child_description']}}</textarea>
    </div>
  </td>

  <td class="px-2 text-center" style="width: 80px">
    <a href="javascript:void(0)" class="badge badge-danger" onclick="deleteATabTitle(this)">
      Xóa
    </a>
  </td>
</tr>
