<div class="tab-pane" id="images">
    <div class="tabbable">

        @include("basecontainer::admin.inc.edit_tabs.image-single", ['imageSingleColClass' => 'col-lg-6', 'imageSingleTittle' => 'Ảnh đại diện', 'imageSingleKey' => 'image' ])

        @include("basecontainer::admin.inc.edit_tabs.image-single", ['imageSingleColClass' => 'col-lg-6', 'imageSingleTittle' => 'Ảnh banner', 'imageSingleKey' => 'banner_image' ])

{{--        @include("basecontainer::admin.inc.edit_tabs.image-multi",['imageMultiTittle' => 'Hình ảnh slide' ])--}}
    </div>
</div>