<?php

namespace App\Containers\StaticPage\UI\WEB\Requests;

use App\Containers\BaseContainer\Traits\RequestBaseLanguage;
use App\Ship\Parents\Requests\Request;

/**
 * Class UpdateNewsRequest.
 *
 */
class UpdateStaticPageRequest extends Request
{
    use RequestBaseLanguage;
    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'roles'       => 'admin',
        'permissions' => 'static-page-update',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'id'           => 'required|exists:static_page,id',
            'image' => 'bail|mimes:jpeg,jpg,png,gif',
//            'staticpage_description.*.name' => 'required|max:255',
            'staticpage_description.*.short_description' => 'max:2000',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required'  => 'ID không tồn tại',
            'id.exists'  => 'ID không tồn tại',
            'image.mimes' => 'Ảnh không đúng định dạng (jpeg, jpg, png, gif)',
        ] + $this->messagesLang([
//            'staticpage_description.*.name.required' => 'Tên language không được bỏ trống',
//            'staticpage_description.*.name.max' => 'Tên language tối đa 255 ký tự',
            'staticpage_description.*.short_description.max' => 'Mô tả language tối đa 2000 ký tự',
        ]);
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
