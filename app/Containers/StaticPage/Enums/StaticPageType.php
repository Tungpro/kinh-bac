<?php


namespace App\Containers\StaticPage\Enums;


class StaticPageType
{
    const MEDIA_IMAGE = 1;

    const MEDIA_TYPE = [
        self::MEDIA_IMAGE => 'Hình ảnh danh sách',
    ];
}