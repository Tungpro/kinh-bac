<?php

namespace App\Containers\StaticPage\Tasks;

use Apiato\Core\Foundation\ImageURL;
use Apiato\Core\Foundation\StringLib;
use App\Containers\BaseContainer\Traits\BaseLanguage;
use App\Containers\StaticPage\Data\Repositories\StaticPageDescRepository;
use App\Containers\StaticPage\Models\StaticPage;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Parents\Requests\Request;

/**
 * Class SaveStaticPageDescTask.
 */
class SaveStaticPageDescTask extends Task
{
    use BaseLanguage;
    protected $repository;

    public function __construct(StaticPageDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  bool
     */
    public function run($data, $original_desc, $parent_id,Request $request)
    {

        try{
            $description = isset($data['staticpage_description']) ? (array)$data['staticpage_description'] : null;

            if (!empty($description) && is_array($description)) {
                $updates = [];
                $inserts = [];
                foreach ($description as $k => $v) {

                    $tmp = $v;
                    // Item
                    $attrItem = [];
                    if(!empty($v['item'])) {
                        $count = count(array_filter($v['item']['item_title']));

                        for ($i = 0; $i < $count; $i++) {
                            if (!empty($tmp['item']['item_icon'][$i])) {
                                $request->request->add(['fileIcon' => $v['item']['item_icon'][$i]]);
                                $upload_icon = $this->imageFile($request, 'fileIcon', $v['item']['item_icon'][$i], 'staticpage');
                            } else {
                                $upload_icon['fileName'] = !empty($v['item']['check_item_icon'][$i]) ? $v['item']['check_item_icon'][$i] : null;
                            }


                            if (!empty($tmp['item']['item_image'][$i])) {
                                $request->request->add(['fileImage' => $request->staticpage_description[$k]['item']['item_image'][$i]]);
                                $upload_image = $this->imageFile($request, 'fileImage', 'staticpage', StringLib::getClassNameFromString(StaticPage::class));

                            } else {
                                $upload_image['fileName'] = !empty($v['item']['check_item_image'][$i]) ? $v['item']['check_item_image'][$i] : null;
                            }

                            $attrItem[] = [
                                'item_icon' => $upload_icon['fileName'],
                                'item_image' => $upload_image['fileName'],
                                'item_title' => $v['item']['item_title'][$i],
                                'item_description' => $v['item']['item_description'][$i],
//                                'item_description' => $v['item']['item_description'][$i],
                            ];
                        }
                    }
                    $tmp['item'] = json_encode($attrItem);

                    // Tiều đề phụ
//                    $attrTitleChild = [];
//                    $countTitle = count(array_filter($v['title_child']['title_child_name']));
//                    $tmp['title_child'] = null;
//                    for ($i = 0; $i < $countTitle; $i++) {
//                        $attrTitleChild[] = [
//                        'title_child_name' => $v['title_child']['title_child_name'][$i],
//                        'title_child_link' => $v['title_child']['title_child_link'][$i],
//                      ];
//                        $tmp['title_child'] = json_encode($attrTitleChild);
//                    }


                    if (isset($original_desc[$k])) {
                        $tmp['link'] = !empty($v['link']) ? $v['link'] : null;
                        $tmp['slug'] = $this->slug('',$v['name'],$k);

                        $updates[$original_desc[$k]['id']] = $tmp;
                    } else {
                        $inserts[] = array_merge($tmp,[
                            'static_page_id' => $parent_id,
                            'language_id' => $k,
                            'link' => empty($v['link']) ? $v['link'] : null,
                            'slug' => $this->slug('',$v['name'],$k),
                        ]);
                    }
                }

                if (!empty($updates)) {
                    $this->repository->updateMultiple($updates);
                }

                if (!empty($inserts)) {
                    $this->repository->getModel()->insert($inserts);
                }

                return true;
            }

        } catch (\Exception $exception) {
            throw $exception;
        }
        return false;
    }


    public function imageFile($request, $imageField, $title, $folder_upload)
    {
        $errorMsg = null;
        $fname = null;
        $file = $request->$imageField;

        $allowedFileExtension = ['jpeg', 'jpg', 'png', 'gif'];
        $extension = $file->getClientOriginalExtension();

        if (in_array($extension, $allowedFileExtension) && $file->isValid()) {
            $fname = ImageURL::makeFileName(!empty($prefix) ? $prefix : $file->getClientOriginalName(), $extension);

            if (!$fname || empty($fname)) {
                $errorMsg = 'Upload ảnh lên server thất bại!';
            } else {
                /* $image = ImageURL::upload($folder_upload, $fname, $folder_upload);*/
                $storeResult = ImageURL::upload($request->$imageField, $fname, $folder_upload);
            }

            if (!$storeResult) {
                $errorMsg = 'Upload file lên server thất bại!';
            }
        } else {
            $errorMsg = 'Upload file lên server sai định dạng!';
        }

        return ['error' => !empty($errorMsg), 'msg' => @$errorMsg ?: 'Success', 'fileName' => $fname];
    }

}
