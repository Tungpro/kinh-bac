<?php
/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2020-10-04 13:58:15
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2020-10-21 14:34:21
 * @ Description:
 */

namespace App\Containers\StaticPage\Models;

use Apiato\Core\Foundation\Facades\ImageURL;
use App\Containers\Banner\Enums\BannerStatus;
use App\Containers\Media\Models\Media;
use App\Containers\StaticPage\Enums\StaticPageStatus;
use App\Ship\Parents\Models\Model;
use Carbon\Carbon;
use Illuminate\Support\Str;

class StaticPage extends Model
{
    protected $table = 'static_page';

    protected $fillable = [
        'status',
        'sort_order',
        'image',
        'banner_image',
        'position',
        'link_video'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function desc()
    {
        return $this->hasOne(StaticPageDesc::class, 'static_page_id', 'id');
    }

    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', StaticPageDesc::class, 'static_page_id', 'id');
    }

    public function medias(){
        return $this->hasMany(Media::class,'object_id','id')->where('model',$this->table)->orderBy('sort_order','ASC');
    }

    /*func*/
    public function positions()
    {
        $positions = config('page-container.positions');

        if (!empty($positions)) {
            $all = explode(',', $this->position);
            if (!empty($all)) {
                $tmp = [];
                foreach ($all as $a) {
                    if (isset($positions[$a])) {
                        $tmp[$a] = $positions[$a];
                    }
                }
                return $tmp;
            }
        }
        return false;
    }

    public function getLink()
    {
        if(!empty($this->desc->link)){
            return $this->desc->link ;
        }
        return route('web.page.detail', ['slug' => Str::slug($this->desc->name), 'id' => $this->id]);
    }

    public function getImageUrl($size = 'original',$column = 'image')
    {
        return ImageURL::getImageUrl($this->$column, 'staticpage', $size);
    }

    public function lang()
    {
        $lang = config('app.locales');
        return isset($lang[$this->lang]) ? $lang[$this->lang] : 'vi';
    }

    /*scope*/
    public function scopeAvailable($query, array $positions = [])
    {
        return $query->where('status', StaticPageStatus::ACTIVE)
            ->where(function ($query) use ($positions) {
                foreach ($positions as $position) {
                    $query->orWhereRaw("LOCATE('{$position}', position) > 0");
                }
            });
    }

}
