<?php

namespace App\Containers\StaticPage\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\StaticPage\Tasks\GetAvailablePageByIdTask;
use App\Ship\Parents\Actions\Action;

class GetAvailablePageByIdAction extends Action
{
    public function run($id, Language $currentLang = null, array $with = [], array $orderBy = [], $conditions = [], $selectFields = ['*'])
    {
         return app(GetAvailablePageByIdTask::class)->selectFields($selectFields)->where($conditions)->orderBy($orderBy)->run($id, $currentLang);
    }
}
