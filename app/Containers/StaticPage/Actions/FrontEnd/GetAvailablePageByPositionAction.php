<?php

namespace App\Containers\StaticPage\Actions\FrontEnd;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Localization\Models\Language;
use App\Containers\StaticPage\Tasks\GetAvailablePageByPositionTask;
use App\Ship\Parents\Actions\Action;


class GetAvailablePageByPositionAction extends Action
{

    public function run(array $positions = [], bool $isMobile, array $with = [], array $orderBy = [], $selectFields = ['*'],Language $currentLang = null,$limit = 0)
    {

        return $this->remember(function () use ($positions, $isMobile, $with, $orderBy, $selectFields, $currentLang, $limit) {
            return app(GetAvailablePageByPositionTask::class)->orderBy($orderBy)->selectFields($selectFields)->run($positions,$currentLang,$limit,$with);
        });
    }
}
