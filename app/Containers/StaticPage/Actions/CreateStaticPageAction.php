<?php

namespace App\Containers\StaticPage\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Media\Task\CreateMediaArrayTask;
use App\Containers\ReProject\Actions\SyncReProjectMediaSubAction;
use App\Containers\ReProject\Enums\ReProjectStatus;
use App\Containers\ReProject\Tasks\HandleReProjectMediaDataByTypeTask;
use App\Containers\StaticPage\Enums\StaticPageType;
use App\Containers\StaticPage\Models\StaticPage;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

/**
 * Class CreateBannerAction.
 *
 */
class CreateStaticPageAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data,Request $request)
    {
        $object = Apiato::call(
            'StaticPage@CreateStaticPageTask',
            [
                $data
            ]
        );
        if ($object) {
            // desc
            $desc = Apiato::call('StaticPage@SaveStaticPageDescTask', [
                $data,
                [],
                $object->id,
                $request
            ]);

            // update medias
             app(CreateMediaArrayTask::class)->run($object, $data['images_old'] ?? [], $data['images'] ?? [], StaticPageType::MEDIA_IMAGE,StaticPage::getTableName());
            if($desc){
                Apiato::call('User@CreateUserLogSubAction', [
                    $object->id,
                    [],
                    $object->toArray(),
                    'Tạo trang tĩnh',
                    StaticPage::class
                ]);
            }
        }

        $this->clearCache();

        return $object;
    }
}
