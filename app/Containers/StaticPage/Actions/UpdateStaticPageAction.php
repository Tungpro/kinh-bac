<?php

namespace App\Containers\StaticPage\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Media\Task\CreateMediaArrayTask;
use App\Containers\StaticPage\Enums\StaticPageType;
use App\Containers\StaticPage\Models\StaticPage;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

/**
 * Class UpdateBannerAction.
 *
 */
class UpdateStaticPageAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data, Request $request)
    {
        $beforeData = Apiato::call('StaticPage@Admin\FindStaticPageByIDSubAction', [$data['id']]);

        $object = Apiato::call('StaticPage@SaveStaticPageTask', [$data]);

        if($object) {
            // update medias
            app(CreateMediaArrayTask::class)->run($object, $data['images_old'] ?? [], $data['images'] ?? [], StaticPageType::MEDIA_IMAGE,StaticPage::getTableName());

            $original_desc = Apiato::call('StaticPage@GetAllStaticPageDescTask', [$object->id]);
            $ok = Apiato::call('StaticPage@SaveStaticPageDescTask', [$data, $original_desc, $object->id,$request]);
            if($ok){
                $object = Apiato::call('StaticPage@Admin\FindStaticPageByIDSubAction', [$object->id]);
                Apiato::call('User@CreateUserLogSubAction', [
                    $object->id,
                    $beforeData->toArray(),
                    $object->toArray(),
                    'Cập nhật trang tĩnh',
                    StaticPage::class
                ]);
            }
        }

        $this->clearCache();
        return $object;
    }
}
