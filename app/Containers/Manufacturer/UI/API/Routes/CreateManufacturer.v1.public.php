<?php

/**
 * @apiGroup           Manufacturer
 * @apiName            createManufacturer
 *
 * @api                {POST} /v1/manufacturers Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->post('manufacturers', [
    'as' => 'api_manufacturer_create_manufacturer',
    'uses'  => 'Controller@createManufacturer',
    'middleware' => [
      'auth:api',
    ],
]);
