<?php

/**
 * @apiGroup           Manufacturer
 * @apiName            deleteManufacturer
 *
 * @api                {DELETE} /v1/manufacturers/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->delete('manufacturers/{id}', [
    'as' => 'api_manufacturer_delete_manufacturer',
    'uses'  => 'Controller@deleteManufacturer',
    'middleware' => [
      'auth:api',
    ],
]);
