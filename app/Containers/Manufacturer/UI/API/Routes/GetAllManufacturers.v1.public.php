<?php

/**
 * @apiGroup           Manufacturer
 * @apiName            getAllManufacturers
 *
 * @api                {GET} /v1/manufacturers Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

/** @var Route $router */
$router->get('manufacturers', [
    'as' => 'api_manufacturer_get_all_manufacturers',
    'uses'  => 'Controller@getAllManufacturers',
    'middleware' => [
      'auth:api',
    ],
]);
