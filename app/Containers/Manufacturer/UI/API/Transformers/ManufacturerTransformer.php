<?php

namespace App\Containers\Manufacturer\UI\API\Transformers;

use App\Containers\Manufacturer\Models\Manufacturer;
use App\Ship\Parents\Transformers\Transformer;

class ManufacturerTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected array $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected array $availableIncludes = [

    ];

    /**
     * @param Manufacturer $entity
     *
     * @return array
     */
    public function transform(Manufacturer $entity)
    {
        $response = [
            'object' => 'Manufacturer',
            'id' => $entity->getHashedKey(),
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
