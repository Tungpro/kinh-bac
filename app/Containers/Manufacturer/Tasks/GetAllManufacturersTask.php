<?php

namespace App\Containers\Manufacturer\Tasks;

use App\Containers\Manufacturer\Data\Repositories\ManufacturerRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
class GetAllManufacturersTask extends Task
{

    protected $repository;

    public function __construct(ManufacturerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $filters,int $perPage, int $notPaginate)
    {
        if( isset($filters['id']) && !empty($filters['id'])){
            $this->repository->pushCriteria(new ThisEqualThatCriteria('manufacturer_id', $filters['id']));
          }
        if(isset($filters['name']) && !empty($filters['name']) ){
          $this->repository->pushCriteria(new ThisOperationThatCriteria('name', '%'.$filters['name'].'%' ,'like'));
        }
        $this->repository->orderBy('manufacturer_id', 'desc');
        return !$notPaginate ? $this->repository->paginate($perPage) : $this->repository->limit($perPage);
    }
}
