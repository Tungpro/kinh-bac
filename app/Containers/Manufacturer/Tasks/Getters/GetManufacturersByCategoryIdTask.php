<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-11-12 17:20:42
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-12 18:53:08
 * @ Description: Happy Coding!
 */

namespace App\Containers\Manufacturer\Tasks\Getters;

use App\Containers\Category\Enums\CategoryStatus;
use App\Containers\Manufacturer\Data\Repositories\ManufacturerRepository;
use App\Containers\Manufacturer\Models\ManufacturerCategory;
use App\Ship\Parents\Tasks\Task;

class GetManufacturersByCategoryIdTask extends Task
{
    protected $repository;
    protected $categoryIds;

    public function __construct(ManufacturerRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    public function run(): iterable
    {
        if (!empty($this->categoryIds)) {
            $this->repository->whereHas('categories', function ($q) {
                $q->where('status', CategoryStatus::ACTIVE);
                $q->whereIn(ManufacturerCategory::getTableName().'.category_id', $this->categoryIds);
            });

            return $this->repository->get();
        }

        return [];
    }

    /**
     * @var mixed $categoryIds
     */
    public function categoryIds($categoryIds): self
    {
        $this->categoryIds = $categoryIds;
        return $this;
    }
}
