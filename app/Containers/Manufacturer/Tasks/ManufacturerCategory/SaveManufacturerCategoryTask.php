<?php

namespace App\Containers\Manufacturer\Tasks\ManufacturerCategory;

use App\Containers\Manufacturer\Models\Manufacturer;
use App\Ship\Parents\Tasks\Task;

/**
 * Class SaveManufacturerCategoryTask.
 */
class SaveManufacturerCategoryTask extends Task
{
    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($manufacturer_category,Manufacturer $current_manufacturer_model)
    {
        if (!empty($manufacturer_category)) {
            if (is_array($manufacturer_category)) {
                $current_manufacturer_model->categories()->sync($manufacturer_category);
            } else {
                $current_manufacturer_model->categories()->attach($manufacturer_category);
            }
        }else {
            $current_manufacturer_model->categories()->sync([]);
        }
    }
}
