<?php

namespace App\Containers\Manufacturer\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class SaveManufacturerByIdAction extends Action
{
    public function run($id,$data,$manufacturerCategory)
    {
      $manufacturer =  Apiato::call('Manufacturer@SaveManufacturerTask', [$id,$data]);
      if(isset($manufacturerCategory)){
        $this->call('Manufacturer@ManufacturerCategory\SaveManufacturerCategoryTask', [$manufacturerCategory,$manufacturer]);
      }
      $this->clearCache();
      return $manufacturer;

    }
}
