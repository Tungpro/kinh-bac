<?php

namespace App\Containers\Manufacturer\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllManufacturersAction extends Action
{
    public function run($data,$perPage=15,$notPaginate=0)
    {
        $manufacturers = Apiato::call('Manufacturer@GetAllManufacturersTask', [$data, $perPage,$notPaginate]);

        return $manufacturers;
    }
}
