<?php

namespace App\Containers\Manufacturer\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Manufacturer\Tasks\CateIdsDirectFromManufacturerIdTask;
use App\Ship\Parents\Actions\Action;

/**
 * Class CateIdsDirectFromPrdIdAction.
 *
 */
class CateIdsDirectFromManufacturerIdAction extends Action
{

    /**
     * @return mixed
     */
    public function run($manufacturer_id)
    {
        $data = app(CateIdsDirectFromManufacturerIdTask::class)->run($manufacturer_id);
        return $data;
    }
}
