@extends('basecontainer::frontend.pc.layouts.home')

@section('content')


<section class="bg-darkBlue h-[45px] w-full mb-9"></section>
<div class="container">

    <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>

    @if(!empty($page))
    <section class="core-values text-gray1 mb-[60px] xl:mb-[205px]"><h2
            class="title-general xl:!text-40 !mb-4 !p-0 !text-gray1"> {{@$page->desc->name}}</h2>
        <div class="container">
              {!! @$page->desc->description !!}
        </div>
    </section>
     @endif
</div>

@endsection