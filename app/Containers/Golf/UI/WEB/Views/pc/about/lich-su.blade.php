@extends('basecontainer::frontend.pc.layouts.home')

@section('content')


<section class="bg-darkBlue h-[45px] w-full mb-9"></section>
<div class="container">

    <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>

    @if(isset($history) && $history->isNotEmpty())
    <div class="container my-[50px] xl:m-[99px_auto_203px]">
        <section class="histories">
            <div class="text-center hidden md:block mb-10">
                <div class="image-flat relative"><img class="mx-auto relative z-[9]" src="{{asset('template/dist/images/history-flat.png')}}"
                                                      alt=""/></div>
            </div>
            <div class="history relative">
                @foreach($history as $item)
                <div class="history__item relative text-gray1">
                    <div class="image"><img src="{{@$item->getImageUrl('medium')}}" alt="{{@$item->desc->name}}"/></div>
                    <div class="content"><h2 class="font-utm uppercase text-32 mb-2">{{@$item->desc->name}}</h2>
                        <p class="text-16">{!! nl2br(@$item->desc->short_description) !!}</p></div>
                </div>
                 @endforeach
            </div>
        </section>
    </div>
    @endif
</div>

@endsection