@extends('basecontainer::frontend.pc.layouts.home')

@section('content')


<section class="bg-darkBlue h-[45px] w-full mb-9"></section>
<div class="container">

    <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>

    @if(!empty($speaker))
        <h2 class="title-general xl:!text-40 !mb-4 !p-0 !text-gray1">{{@$speaker->desc->name_two}}</h2>
        <div class="detail-dist">
            {!! @$speaker->desc->description !!}
        </div>
         @if(isset($speaker->news) && $speaker->news->isNotEmpty())
        <div class="directory__list introduce__history--list relative mb-[50px] xl:mb-[100px]">
            <div class="introduce__history swiper">
                <div class="swiper-wrapper h-auto">

                    @foreach($speaker->news as $item)
                        <div class="swiper-slide">
                            <div class="directory__item">
                                <a href="{{$item->link()}}" class="image block">
                                    <img class="" src="{{$item->getImageUrl('medium2')}}" alt="{{@$item->desc->name}}"/></a>
                                <div class="content p-[10px_5px]">
                                    <h3>
                                        <a href="{{$item->link()}}" class="name font-utm uppercase text-black2 hover:text-red hover-text-red tracking-wider text-18">{{@$item->desc->name}}</a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
            <div class="swiper-pagination mt-6 bottom-0 relative xl:hidden"></div>
            <div class="swiper-button-prev"><img src="{{asset('template/dist/images/icon-prev.png')}}" alt=""/></div>
            <div class="swiper-button-next"><img src="{{asset('template/dist/images/icon-next.png')}}" alt=""/></div>
        </div>
         @endif
     @endif
</div>

@endsection