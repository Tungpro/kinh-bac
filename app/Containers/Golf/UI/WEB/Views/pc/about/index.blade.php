@extends('basecontainer::frontend.pc.layouts.home')

@section('content')

    @php($menuAbout = $menus->filter(function ($item) { return $item->type == config('menu-container.type_key.about_website'); })->groupBy('pid'))
    <main>
        <div class="introduce-section overflow-hidden">
            @if(!empty($banner))
                <div class="mb-[30px] lg:mb-[-40px]">
                    <section class="banner-introduce text-white p-[120px_0px_33px] md:p-[210px_0px_63px]"
                             style="background-image:url('{{@$banner->getImageUrl('super_large')}}')">
                        <div class="container">
                            <h1 class="uppercase text-32 font-iciel text-white m-0">{{@$banner->desc->name}}</h1></div>
                    </section>
                </div>
            @endif
            <div class="introduce">
                <div class="container">
                    <section class="introduce__content mb-[50px] xl:mb-[100px]">
                        <div class="tabs bg-[#EBE7E4] p-[12px_19px] lg:flex items-center">
                            <div class="tab-logo mb-5 mb-lg-0 lg:mb-0">
                                <a href="{{route('web_home_page')}}">
                                    <img src="{{ ImageURL::getImageUrl($settings['website']['logo_about'],'setting','original') }}" alt=""/>
                                </a>
                            </div>
                            @if (isset($menuAbout[0]) &&  $menuAbout[0]->isNotEmpty())
                                <ul class="tab-menu text-14 text-block lg:ml-auto lg:justify-end xl:max-w-[780px]">
                                    @foreach ($menuAbout[0] as $key => $menuAbout1)
                                        <li>
                                            <a class="text-black hover-text-red" href="{{ @$menuAbout1->menu_link }}"> {{@$menuAbout1->desc_lang->name}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>

                        <x-page-file-name :fileName="'about-content-1'" :position="'about_content_1'"
                                          :limit="0"></x-page-file-name>

                    </section>
                    <x-page-file-name :fileName="'about-list-2'" :position="'about_list_2'"
                                      :limit="true"></x-page-file-name>
                    <section class="award pb-[50px] xl:p-[0px_0px_120px]">

                        <x-page-file-name :fileName="'about-project-3'" :position="'about_project_3'"
                                          :limit="0"></x-page-file-name>

                        @if(isset($achievements) && $achievements->isNotEmpty())
                            <div class="award__list ml-auto xl:pr-0 outside-container">
                                <div class="award__list-swiper swiper" >
                                    <div class="swiper-wrapper h-auto">
                                        @foreach($achievements as $item)
                                            <div class="swiper-slide">
                                                <div class="award__item bg-[#F5F5F5] p-[20px_20px_28px]">
                                                    <div class="mb-4"><img src="{{@$item->getImageUrl('medium')}}"
                                                                           alt="{{@$item->desc->name}}"/></div>
                                                    <div class="desc text-16 text-gray1 mb-3">{{@$item->desc->name}}
                                                    </div>
                                                    <div
                                                        class="name uppercase font-iciel text-black2 text-28">{!! @$item->desc->short_description !!}
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                    @if($achievements->count() > 5)
                                        <div class="swiper-nav">
                                            <div class="swiper-button-prev"><img
                                                    src="{{asset('template/dist/images/swiper-button-prev.png')}}"
                                                    alt=""/>
                                            </div>
                                            <div class="swiper-button-next"><img
                                                    src="{{asset('template/dist/images/swiper-button-next.png')}}"
                                                    alt=""/>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        @endif

                    </section>
                </div>

                @if(!empty($speaker))
                    <section class="directory mb-[42px]">
                        <div class="directory-bg hidden md:block h-[252px]"><img class="h-full w-full object-cover"
                                                                                 src="{{asset('template/dist/images/directory-bg.png')}}"
                                                                                 alt=""/></div>
                        <div class="container">
                            <div class="directory__content md:mt-[-120px]">
                                <div class="image-person mb-[20px]"><img
                                        src="{{@$speaker->getImageUrl('medium','about_image')}}"
                                        alt="{{@$speaker->desc->name}}"/></div>
                                <div class="data ml-auto md:mt-[45px]"><h3
                                        class="uppercase text-gray1 md:text-white text-40 font-utm mb-[30px] md:mb-[64px]">{{@$speaker->desc->name_two}}</h3>
                                    <div
                                        class="desc text-gray1 mb-[40px]">{!! nl2br(@$speaker->desc->short_description) !!}</div>

                                    @if(isset($speaker->news) && $speaker->news->isNotEmpty())
                                        <div class="directory__list relative">
                                            <div class="directory__swiper swiper">
                                                <div class="swiper-wrapper h-auto">

                                                    @foreach($speaker->news as $item)
                                                        <div class="swiper-slide">
                                                            <div class="directory__item"><a href="{{$item->link()}}"
                                                                                            class="image block">
                                                                    <img class=""
                                                                         src="{{$item->getImageUrl('medium2')}}"
                                                                         alt="{{@$item->desc->name}}"/></a>
                                                                <div class="content p-[10px_5px]"><h3>
                                                                        <a href="{{$item->link()}}"
                                                                           class="name font-utm uppercase text-black2 hover:text-red hover-text-red tracking-wider text-18">{{@$item->desc->name}}</a>
                                                                    </h3></div>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                            <div class="swiper-button-prev"><img
                                                    src="{{asset('template/dist/images/icon-prev.png')}}" alt=""/></div>
                                            <div class="swiper-button-next"><img
                                                    src="{{asset('template/dist/images/icon-next.png')}}" alt=""/></div>
                                        </div>
                                    @endif

                                    <div class="mt-5 xl:mt-10"><a href="{{$speaker->link()}}" class="btn-outline">
                                            <svg width="37" height="8" viewBox="0 0 37 8" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path d="M0 4H35.5M35.5 4L32.5 0.5M35.5 4L32.5 7.5" stroke="#EC2029"/>
                                            </svg>
                                            {{__('site.timhieuthem')}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                @endif

                <section class="leadership bg-[#F5F5F5] py-[50px] xl:p-[70px_0px_87px]">
                    <div class="container">
                        <div class="text-center">
                            <h3 class="title-general horizontal-line horizontal-line-center !xl:mb-[50px]">{{__('site.banlanhdao')}}</h3>
                            <div class="leadership__list text-center">
                                @foreach($speakers as $key => $item)
                                    @if($key < 2)
                                        <div class="row">
                                            <div class="leadership__item text-center">
                                                <div class="image mb-[12px]"><img class="mx-auto"
                                                                                  src="{{$item->getImageUrl('avatar')}}"
                                                                                  alt="{{@$item->desc->name}}"/></div>
                                                <div class="content">
                                                    <a href="{{$item->link()}}"
                                                       class="name text-18 font-[700] hover:text-red hover-text-red text-black2 mb-[2px] uppercase">{{@$item->desc->name}}</a>
                                                    <div
                                                        class="position text-16 text-black2">{{@$item->desc->position}}</div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach

                                <div class="row row-between">
                                    @foreach($speakers as $key => $item)
                                        @if($key >=2 && $key <= 6)
                                            <div class="leadership__item leadership__item--row text-center">
                                                <div class="image mb-[12px]">
                                                    <img class="mx-auto" src="{{$item->getImageUrl('avatar')}}" alt="{{@$item->desc->name}}"/></div>
                                                <div class="content">
                                                    <a href="{{$item->link()}}"
                                                       class="name text-18 font-[700] hover:text-red hover-text-red text-black2 mb-[2px] uppercase">{{@$item->desc->name}}</a>
                                                    <div
                                                        class="position text-16 text-black2">{{@$item->desc->position}}</div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                @foreach($speakers as $key => $item)
                                    @if($key > 6)
                                        <div class="row">
                                            <div class="leadership__item text-center">
                                                <div class="image mb-[12px]"><img class="mx-auto"
                                                                                  src="{{$item->getImageUrl('avatar')}}"
                                                                                  alt="{{@$item->desc->name}}"/></div>
                                                <div class="content">
                                                    <a href="{{$item->link()}}"
                                                       class="name text-18 font-[700] hover:text-red hover-text-red text-black2 mb-[2px] uppercase">{{@$item->desc->name}}</a>
                                                    <div
                                                        class="position text-16 text-black2">{{@$item->desc->position}}</div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach

                            </div>
                        </div>
                    </div>
                </section>
                @if(isset($news) && $news->isNotEmpty())
                    <section class="company py-[50px] xl:p-[95px_0px_85px]">
                        <div class="container">
                            <div class="text-center"><h3
                                    class="title-general horizontal-line horizontal-line-center xl:!mb-[55px]">{{__('site.congtythanhvien')}}</h3>
                            </div>
                            <div class="company__list relative">
                                <div class="company__list--swiper swiper">
                                    <div class="swiper-wrapper h-auto">
                                        @foreach($news as $item)
                                            <div class="swiper-slide">
                                                <div class="company__item">
                                                    <a href="{{@$item->link()}}" class="image block"><img
                                                            src="{{@$item->getImageUrl('medium1')}}"
                                                            alt="{{@$item->desc->name}}"/></a>
                                                    <div class="content">
                                                        <h3 class="my-[6px]"><a href="{{@$item->link()}}"
                                                                                class="name text-[#333] font-[700] hover:text-red hover-text-red">{{@$item->desc->name}}</a>
                                                        </h3>
                                                        <div class="desc text-gray2 text-14">
                                                            {!! nl2br(@$item->desc->short_description) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="swiper-button-prev swiper-button-outcontainer"><img
                                        src="{{asset('template/dist/images/icon-prev.png')}}"
                                        alt=""/></div>
                                <div class="swiper-button-next swiper-button-outcontainer"><img
                                        src="{{asset('template/dist/images/icon-next.png')}}"
                                        alt=""/></div>
                            </div>
                        </div>
                    </section>
                @endif

                @if(isset($partners) && $partners->isNotEmpty())
                    <section class="associated-company mb-[30px]">
                        <div class="container associated">
                            <div class="text-center"><h3
                                    class="title-general horizontal-line horizontal-line-center xl:!mb-[40px]">{{__('site.congtylienket')}}</h3>
                                <div class="associated__list">
                                    @foreach($partners as $item)
                                        <div class="associated__item">
                                            <div class="image"><img src="{{$item->getImageUrl('small1')}}"
                                                                    alt="{{@$item->desc->name}}"/></div>
                                            <div
                                                class="name font-[700] text-14 text-[#333] my-[10px]">{{@$item->desc->name}}
                                            </div>
                                            <div
                                                class="desc text-gray2 text-14">{!! nl2br(@$item->desc->short_description) !!}</div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </section>
                @endif
                <x-partner-type partnerTypeId="0" returnView="partner-list-page"></x-partner-type>
            </div>
        </div>
    </main>
    {{--        @push('css_bot_all')--}}
    {{--            {!! FunctionLib::addMedia('template/css/bootstrap.min.css') !!}--}}
    {{--        @endpush--}}
    {{--       @push('js_bot_all')--}}
    {{--           {!! FunctionLib::addMedia('template/js/bootstrap.min.js') !!}--}}
    {{--       @endpush--}}
@endsection
