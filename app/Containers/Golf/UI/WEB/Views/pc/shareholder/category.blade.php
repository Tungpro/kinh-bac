@extends('basecontainer::frontend.pc.layouts.home')

@section('content')

<main>
    <section class="block w-full md:h-[45px] h-[20px] bg-darkBlue lg:mb-9 mb-4"></section>
    <section class="block">
        <div class="container">
            <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>
        </div>
    </section>

    <section class="block xl:mb-[120px] md:mb-[80px] mb-[40px]">
        <div class="container">
            <div class="flex flex-row flex-wrap lg:mx-[-15px]">
                @if(isset($categories) && $categories->isNotEmpty())
                <div class="lg:w-1/3 mb-6 lg:mb-0 w-full lg:px-[15px] sidebar-category">
                    <div class="block">
                        @foreach($categories as $item)
                        <a
                            href="{{$item->link()}}"
                            class="{{@$category->category_id == $item->category_id ? 'active' : ''}} flex flex-row items-center font-san font-normal text-16 text-black2 bg-[#f5f5f5] border border-solid border-[#e0e0e0] p-[13px] hover:bg-red hover:text-white hover:font-bold hover:border-red"
                        >
                            {{@$item->desc->name}}
                            <img src="{{asset('template/dist/images/arrow-black-right.svg')}}" class="ml-auto" alt="" />
                        </a>
                        @endforeach

                    </div>
                </div>
                @endif

                <div class="lg:w-2/3 w-full lg:px-[15px]">
                    <!-- filter -->
                    <div class="block text-right lg:mb-[30px] mb-[15px]">
                        <label for="optionYear" class="inline-flex items-center mb-0 cursor-pointer relative max-w-[160px]">

                            <input type="text" name="created_at" id="created_at" class="w-full form-control appearance-none cursor-pointer border border-solid border-[#e0e0e0] rounded-[99px] ml-auto py-[10px] px-[20px] pr-[40px] focus:border-[#e0e0e0]" placeholder="{{__('site.chonnam')}}" autocomplete="off" value="{{@$data_search->created_at}}">

                            <div class="pointer-events-none pl-[10px] absolute top-[50%] translate-y-[-50%] right-[20px]">
                                <svg class="" width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M5 5L4.64645 5.35355L5 5.70711L5.35355 5.35355L5 5ZM8.64645 0.646447L4.64645 4.64645L5.35355 5.35355L9.35355 1.35355L8.64645 0.646447ZM5.35355 4.64645L1.35355 0.646447L0.646447 1.35355L4.64645 5.35355L5.35355 4.64645Z"
                                        fill="#828282"
                                    />
                                </svg>
                            </div>
                        </label>
                    </div>

                   @if(isset($news) && $news->isNotEmpty())
                    <div class="block mb-5">
                        @foreach($news as $item)
                        <div class="block w-full">
                            <span class="block font-san font-normal text-12 text-gray2 mb-2">{{@$item->created_at->format('d/m/Y')}}</span>
                            <div class="flex flex-wrap items-baseline pb-[10px] mb-[15px] border-b border-solid border-bd">
                                <img src="{{asset('template/dist/images/ic-pdf.svg')}}" alt="" />
                                <h4 class="block flex-1 line-clamp-2 font-san font-bold text-16 text-[#000] max-w-[580px] mx-[10px]">
                                    {{@$item->desc->name}}
                                </h4>

                                <a
                                    href="{{!empty($item->linkHolder()) ? $item->linkHolder() : 'javascript:;'}}" @if(!empty($item->linkHolder())) target="_blank" @endif
                                    class="font-san font-normal text-14 text-red ml-auto hover:underline flex-[100%] md:flex-initial text-right mt-2 md:mt-0"
                                >
                                    {{__('site.timhieuthem')}}
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!-- paginate -->
                    {{$news->links('golf::pc.inc.pagination')}}
                     @endif
                </div>
            </div>
        </div>
    </section>
    <x-partner-type partnerTypeId="0" returnView="partner-list-page"></x-partner-type>
</main>

@endsection


@push('js_bot_all')
    <script>
            $('#created_at').datetimepicker({format: 'd/m/Y', timepicker: false});
            $('#created_at').change(function () {
                window.location.href = '?created_at=' + $(this).val()
            })
    </script>
@endpush
