@extends('basecontainer::frontend.pc.layouts.home')

@section('content')

    <main>
        <div class="d-block google-map">
            {!! @$settings['contact']['map_iframe'] !!}
        </div>
        <div class="block py-9">
            <div class="container">
                <div class="relative flex flex-wrap 2xl:-mx-[50px] md:-mx-[15px] xl:mb-16">
                    <div class="order-2 order-lg-1 w-full lg:w-1/2 2xl:px-[50px] md:px-[15px]">
                        <h3 class="block font-utm font-normal text-32 text-black2 uppercase mb-7">{{__('site.lienhevoichungtoi')}}</h3>
                        <div id="page-contact">
                            <input type="hidden" name="type" id="typeContact" value="{{ App\Containers\Contact\Models\Contact::TYPE_C }}">
                            <input
                                v-model="info.name"
                                name=""
                                id=""
                                placeholder="{{__('site.tencuaban')}} *"
                                class="block w-full p-3 border border-solid border-[#e0e0e0] rounded-[5px] font-san font-normal text-14 text-gray2 mb-3"
                            />
                            <input
                                v-model="info.phone"
                                type="text"
                                onkeypress="return shop.numberRic()"
                                name=""
                                id=""
                                placeholder="{{__('site.sodienthoaicuaban')}} *"
                                class="block w-full p-3 border border-solid border-[#e0e0e0] rounded-[5px] font-san font-normal text-14 text-gray2 mb-3"
                            />
                            <input
                                v-model="info.email"
                                type="email"
                                name=""
                                id=""
                                placeholder="{{__('site.emailcuaban')}} *"
                                class="block w-full p-3 border border-solid border-[#e0e0e0] rounded-[5px] font-san font-normal text-14 text-gray2 mb-3"
                            />
                            <textarea
                                v-model="info.message"
                                name=""
                                id=""
                                placeholder="{{__('site.nhapnoidung')}}"
                                class="block w-full p-3 border border-solid border-[#e0e0e0] rounded-[5px] font-san font-normal text-14 text-gray2 mb-9 min-h-[115px] resize-none"
                            ></textarea>
                            <button  @click="saveContact"
                                class="inline-block font-san font-normal text-16 text-white p-[10px_15px] bg-red rounded-[5px] hover:bg-darkBlue transition-all hover:transition-all"
                            >
                               {{__('site.lienhengay')}} <img class="inline-block xl:ml-10 ml-5 mr-auto" src="{{asset('template/dist/images/ic-arrow-light-right-white.svg')}}" alt="" />
                            </button>
                        </div>
                    </div>
                    <div class="order-1 order-lg-2 w-full lg:w-1/2 2xl:px-[50px] md:px-[15px]">
                        <ul class="relative flex flex-wrap -mx-3 lg:mt-16 mt-7">
                            <li class="w-1/2 px-3 md:mb-9 mb-4">
                                <img src="{{asset('template/dist/images/ic-contact-1.svg')}}" class="mb-4 w-10 h-10 object-scale-down" alt="" />
                                <span class="block font-san font-bold text-16 md:text-18 text-black2 mb-1">{{__('site.diachi')}}</span>
                                <span class="block font-san font-normal text-14 md:text-16 text-gray1">{{@$settings['website']['address']}}</span>
                            </li>
                            <li class="w-1/2 px-3 md:mb-9 mb-4">
                                <img src="{{asset('template/dist/images/ic-contact-2.svg')}}" class="mb-4 w-10 h-10 object-scale-down" alt="" />
                                <span class="block font-san font-bold text-16 md:text-18 text-black2 mb-1">{{__('site.email')}}</span> <span class="block">{{@$settings['contact']['email']}}</span>
                            </li>
                            <li class="w-1/2 px-3 md:mb-9 mb-4">
                                <img src="{{asset('template/dist/images/ic-contact-3.svg')}}" class="mb-4 w-10 h-10 object-scale-down" alt="" />
                                <span class="block font-san font-bold text-16 md:text-18 text-black2 mb-1">{{__('site.dienthoai')}}</span>
                                <span class="block font-san font-normal text-14 md:text-16 text-gray1">{{@$settings['contact']['hotline']}}</span>
                            </li>
                            <li class="w-1/2 px-3 md:mb-9 mb-4">
                                <img src="{{asset('template/dist/images/ic-contact-4.svg')}}" class="mb-4 w-10 h-10 object-scale-down" alt="" />
                                <span class="block font-san font-bold text-16 md:text-18 text-black2 mb-1">{{__('site.fax')}}</span>
                                <span class="block font-san font-normal text-14 md:text-16 text-gray1">{{@$settings['contact']['hotline2']}}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </main>
    @push('js_bot_all')
                <script> const url_api_save_contact = '{{ route('home.contact.store') }}';</script>
              {!! FunctionLib::addMedia('js/pages/base/contact.js') !!}
    @endpush
@endsection

