@extends('basecontainer::frontend.pc.layouts.home')

@section('content')

    <main>
        <main>
            <x-banner-component :banner="$banner"></x-banner-component>
            <div class="news-section bg-xanh text-white py-[50px] xl:p-[123px_0px_100px]">
                <div class="container">
                    @if(!empty($request->name) && $news->isNotEmpty())
                        <div class="text-white text-[20px] xl:text-[30px] mb-[35px] xl:mb-[106px]">
                            {{__('site.co')}} <strong
                                class="text-chung">{{$news->toTal()}}</strong> {{__('site.ketquatimkiemvoitukhoa')}}
                            <strong>“{{$request->name}}”</strong>
                        </div>
                    @elseif($news->isEmpty())
                        <div class="text-white text-[20px] xl:text-[30px] mb-[35px] xl:mb-[106px]">
                            {{__('site.khongtimthayketquanaovoitukhoa')}} <strong>“{{$request->name}}”</strong>
                        </div>
                    @endif
                    @if(isset($news) && $news->isNotEmpty())
                    <section class="news-list">
                        <div class="row">
                            @foreach($news as $item)
                                <div class="col-sm-6 col-lg-4 mb-[30px] md:mb-[60px] xl:mb-[100px]">
                                    <div class="news-item relative">
                                        <a href="{{@$item->linkType()}}" class="image block"><img
                                                src="{{$item->getImageUrl('medium7')}}"
                                                alt="{{@$item->desc->name}}"/></a>
                                        <div class="content mt-[8px] xl:mt-[1rem]">
                                            @if(isset($item->categories) && $item->categories->isNotEmpty())
                                                @foreach($item->categories as $cate)
                                                    <div
                                                        class="tags !text-[#CF9C51]">{{@$cate->desc->name}}</div>
                                                @endforeach
                                            @endif
                                            <h3 class="mb-[10px] xl:mb-[16px]">
                                                <a href="{{@$item->linkType()}}"
                                                   class="name hover:text-chung uppercase text-[16px] xl:text-[18px]">
                                                    {{@$item->desc->name}}
                                                </a>
                                            </h3>
{{--                                            <div class="desc">--}}
{{--                                                {{nl2br(@$item->desc->name)}}--}}
{{--                                            </div>--}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        {{$news->links('golf::pc.inc.pagination')}}
                    </section>
                    @endif
                </div>
            </div>
        </main>
        @push('js_bot_all')
            <script>
                $('.header-search').val('{{@$request->name}}')
            </script>
    @endpush
@endsection


