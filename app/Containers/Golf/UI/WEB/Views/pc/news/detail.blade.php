@extends('basecontainer::frontend.pc.layouts.home')

@section('content')

    <main>
        <section class="block w-full md:h-[45px] h-[20px] bg-darkBlue lg:mb-9 mb-4"></section>
        <section class="block">
            <div class="container">
                <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>
            </div>
        </section>
        <section class="block xl:mb-[120px] md:mb-[80px] mb-[40px] detail-dist">
            <div class="container">
                <h2 class="block font-utm text-black2 font-normal lg:text40 text-32 uppercase text-gray1 lg:mb-8 mb-5">
                    {{@$data->desc->name}}
                </h2>

                <img src="{{$data->getImageUrl('large1')}}" alt="{{@$data->desc->name}}" class="w-auto h-auto object-contain mx-auto xl:mb-12 mb-8" />

                <p class="font-san font-normal text-16 text-black2 mb-8">
                    {!! @$data->desc->description !!}
                </p>
            </div>
        </section>
        @php($type = \App\Containers\Category\Enums\CategoryType::NEWS)
        <x-news-type :data="$data" :link="'link'" :cateType="$type" :limit="6"></x-news-type>
    </main>

@endsection