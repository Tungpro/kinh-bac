@extends('basecontainer::frontend.pc.layouts.home')

@section('content')

    <main>
        <section class="block w-full md:h-[45px] h-[20px] bg-darkBlue"></section>
        @if(isset($newsLatest) && $newsLatest->isNotEmpty())
        <section class="block py-[25px] bg-[#f5f5f5]">
            <div class="container">
                <h2 class="block text-center font-iciel font-normal xl:text-40 md:text-32 text-24 uppercase text-gray1 mb-[25px]">{{__('site.tintucmoinhat')}}</h2>
                <div class="relative flex flex-wrap mx-[-10px]">
                    <div class="lg:w-1/2 w-full px-[10px]">
                        <article class="block">
                            <div class="block mb-[20px]">
                                <a href="{{@$newsLatest[0]->link()}}" class="block relative w-full lg:pb-[58%] pb-[52.25%]"
                                ><img
                                        class="absolute w-full h-full object-cover object-center top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%]"
                                        src="{{@$newsLatest[0]->getImageUrl('medium6')}}"
                                        alt="{{@$newsLatest[0]->desc->name}}"
                                    /></a>
                            </div>
                            <div class="block">
                                <a
                                    href="{{@$newsLatest[0]->link()}}"
                                    class="line-clamp-2 font-utm font-normal lg:text-28 md:text-24 text-20 uppercase text-gray3 mb-3 hover:text-red hover-text-red"
                                >{{@$newsLatest[0]->desc->name}}</a>
                                <p class="line-clamp-3 font-san font-normal md:text-16 text-14 text-gray1">
                                    {!! nl2br(@$newsLatest[0]->desc->short_description) !!}
                                </p>
                            </div>
                        </article>
                    </div>
                    <div class="lg:w-1/2 w-full px-[10px]">
                        <div class="relative flex flex-wrap mx-[-10px]">
                            @foreach($newsLatest as $key => $item)
                                @if($key>0)
                                    <div class="md:w-1/2 w-full px-[10px] mb-5">
                                        <article class="block">
                                            <div class="block mb-[20px]">
                                                <a href="{{$item->link()}}"
                                                   class="block relative w-full lg:pb-[61%] pb-[52.25%]"
                                                ><img
                                                        class="absolute w-full h-full object-cover object-center top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%]"
                                                        src="{{$item->getImageUrl('medium7')}}"
                                                        alt="{{@$item->desc->name}}"
                                                    /></a>
                                            </div>
                                            <div class="block">
                                                <a href="{{$item->link()}}"
                                                   class="line-clamp-2 font-utm font-normal md:text-20 text-16 uppercase text-gray3 mb-3 hover:text-red hover-text-red"
                                                >{{@$item->desc->name}}</a>
                                            </div>
                                        </article>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endif
        @if(isset($categories) && $categories->isNotEmpty())
        <section class="block py-9"  id="category">
            <div class="container">
                <div
                    class="tabs-onclick flex md:flex-wrap flex-nowrap lg:whitespace-normal whitespace-nowrap md:overflow-visible overflow-auto items-center md:justify-center border-b-2 border-solid border-[#e0e0e0] mb-5 gap-2"
                >
                    @foreach($categories as $key => $item)
                    <a
                        href="{{$item->link()}}#category"
                        class="{{@$category->category_id == $item->category_id ? 'active' : ($key == 0 && !$category ? 'active' : '')}}  font-utm font-normal xl:text-24 lg:text-20 text-18 uppercase text-[#bdbdbd] xl:px-[25px] md:px-[10px] px-[5px]"
                    >{{@$item->desc->name}} </a>
                    @endforeach
                </div>
                @if(isset($news) && $news->isNotEmpty())
                <div class="block blog--tabs-list">
                    <div class="blog--tabs-item">
                        <div class="relative flex flex-wrap xl:mx-[-25px] mx-[-15px]">

                            @foreach($news as $item)
                            <div class="lg:w-1/3 md:w-1/2 w-full xl:px-[25px] px-[15px] lg:mb-10 mb-5">
                                <article class="block">
                                    <div class="block mb-2">
                                        <a href="{{$item->link()}}"
                                           class="block relative w-full lg:pb-[61%] pb-[52.25%]"
                                        ><img
                                                class="absolute w-full h-full object-cover object-center top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%]"
                                                src="{{$item->getImageUrl('large')}}"
                                                alt="{{@$item->desc->name}}"
                                            /></a>
                                    </div>
                                    <div class="block">
                                        <a href="{{$item->link()}}"
                                           class="line-clamp-2 font-utm font-normal md:text-18 text-16 uppercase text-gray3 mb-2 hover:text-red hover-text-red"
                                        >{{@$item->desc->name}}</a>
                                        <p class="line-clamp-3 font-san font-normal md:text-16 text-14 text-gray2 m-0">
                                            {!! nl2br( @$item->desc->short_description) !!}
                                        </p>
                                    </div>
                                </article>
                            </div>
                            @endforeach

                        </div>
                        <div class="pagination flex flex-wrap justify-center items-center gap-2 xl:my-8 my-5">
                            {{$news->links('golf::pc.inc.pagination')}}
                        </div>

                    </div>
                </div>
                @endif
            </div>
        </section>
        @endif

    </main>
    @push('js_bot_all')

    @endpush
@endsection