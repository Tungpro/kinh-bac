@extends('basecontainer::frontend.pc.layouts.home')

@section('content')

<main>
    <div class="">
        <section
            class="banner-general bg-section relative"
            style="background-image: url('{{$banner->getImageUrl('super_large')}}')">
            <h1 class="banner-title w-fit inline-block text-center font-rockness text-[50px] leading-snug text-white px-3">
                <span>{{@$banner->desc->name}}</span>
            </h1>

            <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>
        </section>

        @if(isset($news) && $news->isNotEmpty())
            <div class="news-section bg-xanh text-white py-[50px] xl:p-[123px_0px_100px]">
                <div class="container news">
                    <section class="news-category">
                        @if(isset($newsHot) && $newsHot->isNotEmpty())
                        <div class="news-category-swiper mb-[50px] xl:mb-[100px] swiper">
                            <div class="swiper-wrapper">
                                @foreach($newsHot as $item)
                                <div class="swiper-slide" v-for="n in 3">
                                    <div class="image"><img src="{{$item->getImageUrl('social3')}}" alt="{{@$item->desc->name}}" /></div>
                                    <div class="content mt-[10px] xl:mt-[19px]">
                                        @if(isset($item->categories) && $item->categories->isNotEmpty())
                                            @foreach($item->categories as $cate)
                                                <div class="tags">{{@$cate->desc->name}}</div>
                                            @endforeach
                                        @endif
                                        <h3>
                                            <a href="{{$item->link()}}" class="name uppercase hover:text-chung text-[18px] xl:text-[25px] mb-[10px]">
                                                {{@$item->desc->name}}
                                            </a>
                                        </h3>
                                        <a href="{{$item->link()}}" class="inline-flex font-gotham button-section">{{__('site.xemchitiet')}}</a>
                                    </div>
                                </div>
                                 @endforeach
                            </div>
                            <div class="swiper-button swiper-button-prev"></div>
                            <div class="swiper-button swiper-button-next"></div>
                        </div>
                        @endif

                        <section class="news-list">
                            <div class="row">
                                @foreach($news as $item)
                                <div class="col-sm-6 col-lg-4 mb-[30px] md:mb-[60px] xl:mb-[100px]">
                                    <div class="news-item relative">
                                        <a href="{{$item->link()}}" class="image block"><img src="{{$item->getImageUrl('medium7')}}" alt="{{@$item->desc->name}}" /></a>
                                        <div class="content mt-[8px] xl:mt-[1rem]">
                                            @if(isset($item->categories) && $item->categories->isNotEmpty())
                                                @foreach($item->categories as $cate)
                                                    <div class="tags">{{@$cate->desc->name}}</div>
                                                @endforeach
                                            @endif
                                            <h3 class="mb-[10px] xl:mb-[16px]">
                                                <a href="{{$item->link()}}" class="name hover:text-chung uppercase text-[16px] xl:text-[18px]">
                                                    {{@$item->desc->name}}
                                                </a>
                                            </h3>
                                            <div class="desc">
                                                {!! nl2br(@$item->desc->short_description) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            {{$news->links('golf::pc.inc.pagination')}}
                        </section>
                    </section>
                </div>
            </div>
        @else
          <div class="text-center"><h1 class="mt-5 mb-5 uppercase text-center text-[25px] xl:text-[40px] font-[400] mb-[20px] xl:mb-[28px]">{{__('site.khongcobaivietnao')}}</h1></div>
        @endif
    </div>
</main>

@endsection