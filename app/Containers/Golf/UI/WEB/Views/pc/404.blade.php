@extends('basecontainer::frontend.pc.layouts.home')

@section('content')
    @if(isset($banner) && $banner->isNotEmpty())
    <section class="banner-general error bg-section relative overflow-hidden" style="background-image: url('{{$banner[0]->getImageUrl('super_large')}}')">
        <div class="error-title w-screen flex items-center justify-center text-white leading-none">
            <img class="shrink-0 mr-[20px]" src="{{asset('template/images/icon-erroe.png')}}" alt="" />
            <div class="text xl:mt-[-30px]">
                <div class="mb-[5px]">
                    <span class="font-gotham font-weight-bold text-[50px] xl:text-[80px]">{{__('site.404')}}</span>
                    <span class="text-[30px] xl:text-[60px] text-uppercase">{{__('site.error')}}</span>
                </div>
                <div class="text-20px xl:text-[25px]">{{@$banner[0]->desc->name}}</div>
            </div>
        </div>
    </section>
    @endif

@endsection
