@extends('basecontainer::frontend.pc.layouts.home')

@section('content')

    <main>

            <section class="block w-full md:h-[45px] h-[20px] bg-darkBlue lg:mb-[35px] mb-[20px]"></section>
            <section class="block">
                <div class="container">
                    <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>
                </div>
            </section>

           @if($banner)
            <section class="block">
                <div class="container">
                    <div class="block max-w-full w-full md:mb-[35px] mb-[15px]">
                        <img class="max-w-full w-full h-auto bg-gray2" src="{{@$banner->getImageUrl('medium1')}}" alt="{{@$banner->desc->name}}" />
                    </div>
                    <div class="block md:mb-[35px] mb-[15px]">
                        <h2 class="block font-iciel font-normal lg:text-40 md:text-32 text-24 text-center uppercase text-black2">{{@$banner->desc->name}}</h2>
                        <p class="block font-san font-normal text-16 text-center text-gray1">
                            {!! nl2br(@$banner->desc->description) !!}
                        </p>
                    </div>
                </div>
            </section>
           @endif

           @if(!empty($data))
            <section
                class="block relative xl:pt-[90px] pt-[30px] before:content-[''] before:absolute before:bg-[#f5f5f5] before:w-full before:h-[440px] before:top-0 before:left-0"
            >
                <div class="container">
                    <div class="relative flex flex-wrap xl:mx-[-22px] mx-[-10px]">
                        @foreach($data as $item)
                        <div class="lg:w-1/3 md:w-1/2 w-full xl:px-[22px] px-[10px] xl:mb-[65px] mb-[30px]">
                            <div class="block bg-white border border-solid border-[#e0e0e0] rounded-[5px] h-full xl:p-[30px] p-[15px]">
                                <img
                                    class="block w-[150px] h-[150px] mr-auto rounded-[50%] object-cover object-center overflow-hidden mb-[20px]"
                                    src="{{$item->getImageUrl('avatar')}}"
                                    alt="{{@$item->desc->name}}"
                                />
                                <div class="block mb-[20px]">
                                    <h3 class="block font-san font-bold text-18 text-black2">{{@$item->desc->name}}</h3>
                                    <span class="block font-san font-normal text-16 text-red">{{@$item->desc->short_description}}</span>
                                </div>
                                <div class="block font-san font-normal text-18 color-gray1">
                                    <p class="m-0">
                                        {!! nl2br(@$item->desc->description) !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>
           @endif

        <x-partner-type partnerTypeId="0" returnView="partner-list-page"></x-partner-type>
    </main>
    @push('js_bot_all')

    @endpush
@endsection