<section class="award py-[50px] xl:p-[120px_0px_120px]">
    @if(isset($page))
    <div class="container">
        <div class="award__slogan max-w-[676px] mb-[40px]" data-slider-card-up>
            <h3 class="title-general horizontal-line">{{@$page->desc->name}}</h3>
            <div class="desc">
                {!! nl2br(@$page->desc->short_description) !!}
            </div>
        </div>
    </div>
    @endif
    @if(isset($achievements) && $achievements->isNotEmpty())
    <div class="award__list ml-auto px-[15px] xl:pr-0 outside-container" data-awards-slider>
        <div class="award__list-swiper swiper">
            <div class="swiper-wrapper h-auto">
                @foreach($achievements as $item)
                <div class="swiper-slide">
                    <div class="award__item bg-[#F5F5F5] p-[20px_20px_28px]" data-slider-card-up>
                        <div class="mb-4"><img src="{{$item->getImageUrl('medium')}}" alt="{{@$item->desc->name}}" /></div>
                        <div class="desc text-16 text-gray1 mb-3">{{@$item->desc->name}}</div>
                        <div class="name uppercase font-iciel text-black2 text-28"> {!! nl2br(@$item->desc->short_description) !!}</div>
                    </div>
                </div>
                @endforeach
            </div>
            @if($achievements->count() > 5)
            <div class="swiper-nav">
                <div class="swiper-button-prev"><img src="{{asset('template/dist/images/swiper-button-prev.png')}}" alt="" /></div>
                <div class="swiper-button-next"><img src="{{asset('template/dist/images/swiper-button-next.png')}}" alt="" /></div>
            </div>
            @endif
        </div>
    </div>
    @endif
</section>