@if(isset($news))
    <div class="news-hot" data-fade-in-up><h3 class="title-general relative horizontal-line">{{__('site.tintucmoinhat')}}</h3>
        <div class="news-item"><a href="{{$news->link()}}" class="image block"><img src="{{@$news->getImageUrl('medium6')}}" alt="{{@$news->desc->name}}"/></a>
            <div class="content p-[19px_10px]"><a href="{{$news->link()}}" class="name font-utm font-[400] text-black2 text-24 hover-text-red tracking-wider">{{@$news->desc->name}}</a></div>
        </div>
    </div>
@endif