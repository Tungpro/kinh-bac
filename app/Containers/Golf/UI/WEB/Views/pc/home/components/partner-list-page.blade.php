@if(isset($partnersByType) && $partnersByType->isNotEmpty())
<section class="partnership mb-[30px]">
    <div class="text-center"><h3 class="title-general horizontal-line horizontal-line-center xl:!mb-[40px]">
            {{__('site.doitacchienluoc')}}</h3></div>
    <div class="partnership__list--swiper swiper">
        <div class="swiper-wrapper h-auto">
            @foreach($partnersByType as $item)
            <div class="swiper-slide"><a  href="{{ !empty($item->desc->link)  ? $item->desc->link : 'javascript:;' }}"><img src="{{@$item->getImageUrl('avatar')}}" alt="{{@$item->desc->name}}"/></a></div>
            @endforeach
        </div>
    </div>
</section>
@endif