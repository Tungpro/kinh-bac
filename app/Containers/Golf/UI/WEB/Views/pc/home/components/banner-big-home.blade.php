
@if(isset($bigHomeBanners) && $bigHomeBanners->isNotEmpty())
    <section class="banner relative vertical-line">
        <div class="banner__swiper swiper relative">
            <div class="swiper-wrapper h-auto">
                @foreach($bigHomeBanners as $item)
                <div class="swiper-slide">
                    <img src="{{@$item->getImageUrl('super_large')}}" alt="{{@$item->desc->name}}" />
                </div>
                @endforeach
            </div>
            <div class="swiper-button-prev">
                <img src="{{asset('template/dist/images/banner-navigation-prev.png')}}" alt="" />
            </div>
            <div class="swiper-button-next">
                <img src="{{asset('template/dist/images/banner-navigation-next.png')}}" alt="" />
            </div>
        </div>
    </section>
@endif
