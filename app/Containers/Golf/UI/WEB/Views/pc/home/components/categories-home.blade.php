

<section class="activity pb-[50px] xl:pb-[181px] vertical-line relative">
    <div class="container">
        @if(isset($page))
        <div class="text-center max-w-[736px] mb-[39px] xl:mb-[73px] mx-auto" data-slider-card-up>
            <h3 class="title-general horizontal-line relative horizontal-line-center">{{@$page->desc->name}}</h3>
            <p>
                {!! @$page->desc->short_description !!}
            </p>
        </div>
        @endif
        @if(isset($categories) && $categories->isNotEmpty())
        <div class="activity__list max-w-[857px] mx-auto">
            @foreach($categories as $item)
            <div class="activity__item text-center" data-slider-card-up>
                <div class="image block" data-opacity-content>
                    <img class="mx-auto" src="{{$item->getImageUrl('avatar')}}" alt="{{@$item->desc->name}}" />
                </div>
                <div class="content mt-11" data-opacity-content>
                     <div class="name text-24 mb-[11px] text-black2 font-utm tracking-wider uppercase">{{@$item->desc->name}}</div>
                    <div class="desc text-gray1 text-16 line-clamp-4 mb-6">
                        {{@$item->desc->description}}
                    </div>
                    <a href="{{@$item->link()}}" class="text-[#1E8ECE] hover:text-red hover-text-red text-16">{{__('site.timhieuthem')}}</a>
                </div>
            </div>
            @endforeach
        </div>
        @endif
    </div>
</section>

