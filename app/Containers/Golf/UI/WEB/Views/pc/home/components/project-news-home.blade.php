
    <section class="project relative bg-[#F5F5F5] p-[50px_0px_50px] xl:p-[88px_0px_158px]">
        @if(isset($page))
        <div class="container">
            <div class="project__slogan max-w-[676px] mb-[40px] xl:mb-[55px]" data-slider-card-up>
                <h3 class="title-general horizontal-line">{{@$page->desc->name}}</h3>
                <div class="desc">
                    {!! nl2br(@$page->desc->short_description) !!}
                </div>
            </div>
        </div>
        @endif
        @if(isset($news) && $news->isNotEmpty())
        <div class="project__list ml-auto px-[15px] xl:pr-0 outside-container">
            <div class="project__list-swiper swiper">
                <div class="swiper-wrapper">
                    @foreach($news as $item)
                    <div class="swiper-slide">
                        <div class="project__item"  data-slider-card-up>
                            <a href="{{@$item->linkProject()}}" class="image block"><img src="{{$item->getImageUrl('medium5')}}" alt="{{@$item->desc->name}}" /></a>
                            <div class="content p-[15px_10px_0px_10px]">
                                <a href="{{@$item->linkProject()}}" class="name mb-4 xl:mb-6 font-utm text-black2 hover:text-red hover-text-red tracking-wider text-24">{{@$item->desc->name}}</a>
                                <div class="desc text-16 mb-7 text-gray1">
                                    {!! @$item->desc->short_description !!}
                                </div>
                                <a href="{{@$item->linkProject()}}" class="viewmore btn-outline">
                                    <svg width="37" height="8" viewBox="0 0 37 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M0 4H35.5M35.5 4L32.5 0.5M35.5 4L32.5 7.5" stroke="#EC2029" />
                                    </svg>
                                    {{__('site.timhieuthem')}}
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
                @if($news->count() > 2)
                <div class="swiper-nav">
                    <div class="swiper-button-prev"><img src="{{asset('template/dist/images/swiper-button-prev.png')}}" alt="" /></div>
                    <div class="swiper-button-next"><img src="{{asset('template/dist/images/swiper-button-next.png')}}" alt="" /></div>
                </div>
                @endif
            </div>
        </div>
        @endif
    </section>
