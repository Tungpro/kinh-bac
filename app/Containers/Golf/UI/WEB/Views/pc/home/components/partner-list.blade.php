
@if(isset($partnersByType) && $partnersByType->isNotEmpty())
<div class="partners" data-fade-in-up><h3 class="title-general horizontal-line">{{__('site.doitacchienluoc')}}</h3>
    <div class="partner__list">
        <div class="partner__list-swiper swiper">
            <div class="swiper-wrapper h-[282px] pb-[35px]">
                @foreach($partnersByType as $item)
                <div class="swiper-slide"><a href="{{ !empty($item->desc->link)  ? $item->desc->link : 'javascript:;' }}"><img src="{{@$item->getImageUrl('avatar')}}" alt="{{@$item->desc->name}}"/></a></div>
                @endforeach
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </div>
</div>
@endif
