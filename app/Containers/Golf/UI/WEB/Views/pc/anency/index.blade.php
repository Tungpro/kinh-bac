@extends('basecontainer::frontend.pc.layouts.home')

@section('content')

    <main>
        <main class="csbh">
            <x-banner-component :banner="$banner" :class="'banner-csbh'"></x-banner-component>
           @if(isset($partners) && $partners->isNotEmpty())
            <section class="box-csbh">
                <div class="container">
                    <div class="row">
                        @foreach($partners as $item)
                        <div class="col-lg-4">
                            <div class="item-csbh">
                                <div class="img">
                                    <img src="{{@$item->getImageUrl('medium')}}" alt="{{@$item->desc->name}}">
                                </div>
                                <h3>{{@$item->desc->name}}</h3>
                                <span>{{@$item->desc->short_description}}</span>
                                <a href="{{!empty($item->desc->link ? @$item->desc->link : 'javacript:;')}}">{{@$item->desc->short_description_two}}</a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>
            @endif
        </main>
        @endsection

