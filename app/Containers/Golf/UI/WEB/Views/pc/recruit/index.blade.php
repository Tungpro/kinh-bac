@extends('basecontainer::frontend.pc.layouts.home')

@section('content')

    <main>

        @if(!empty($banner))
        <section class="block"><img src="{{@$banner->getImageUrl('super_large')}}" class="block w-full h-auto object-contain" alt="{{@$banner->desc->name}}" /></section>
        @endif
        <section class="block lg:py-12 py-9">
            <div class="container">
                @if(!empty($banner))
                <div class="block text-center">
                    <h2 class="block text-center font-iciel font-normal xl:text-40 md:text-32 text-24 uppercase text-gray1 md:mb-6 mb-2">{{@$banner->desc->name}}</h2>
                    <span class="block max-w-[975px] w-full mx-auto lg:mb-[65px] mb-[30px] font-san font-normal md:text-16 text-14 text-gray1">{!! nl2br(@$banner->desc->description) !!}</span>
                </div>
                @endif
                @if(isset($data) && $data->isNotEmpty())
                <div class="block">
                    <div class="relative flex flex-wrap -mx-2">
                        @foreach($data as $item)
                        <div class="w-full xl:w-1/4 lg:w-1/3 sm:w-1/2 px-2">
                            <div class="block xl:mb-11 mb-7">
                                <div class="block mb-2">
                                    <a href="{{$item->linkRecruit()}}"  class="inline-block relative w-full pb-[62%]"
                                    ><img
                                            src="{{$item->getImageUrl('medium8')}}"
                                            class="absolute w-full h-full top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%] object-cover object-center"
                                            alt="{{@$item->desc->name}}"
                                        /></a>
                                </div>
                                <div class="block">
                                    <a href="{{$item->linkRecruit()}}" class="block font-san font-bold text-16 text-black2 mb-2 hover:text-red hover-text-red">{{@$item->desc->name}}</a
                                    >
                                    <span class="block text-gray2 mr-2"></span><span class="block text-gray1">{!!nl2br(@$item->desc->short_description) !!}</span>

                                </div>
                            </div>
                        </div>
                         @endforeach
                            <div class="w-full text-center lg:my-10 my-5">
                                <div class="pagination flex flex-wrap justify-center items-center gap-2">
                                {{$data->links('golf::pc.inc.pagination')}}
                                </div>
                            </div>
                    </div>
                </div>
                @endif
            </div>
        </section>

    </main>
    @push('js_bot_all')

    @endpush
@endsection