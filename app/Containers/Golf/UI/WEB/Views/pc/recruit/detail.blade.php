@extends('basecontainer::frontend.pc.layouts.home')

@section('content')

<main>
    <section class="block w-full md:h-[45px] h-[20px] bg-darkBlue lg:mb-[35px] mb-[20px]"></section>
    <section class="block">
        <div class="container">
            <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>
        </div>
    </section>

    <section class="block xl:my-16 md:my-10 my-7">
        <div class="container">
            <div class="relative flex flex-wrap xl:-mx-6 lg:-mx-3">
                <div class="w-full lg:w-2/3 xl:px-6 lg:px-3">
                    <div class="block xl:mb-9 md:mb-5">
                        <h2 class="block font-iciel font-normal text-40 uppercase text-black2">{{@$data->desc->name}}</h2>
                    </div>
                    <div class="block detail-dist">
                        {!! @$data->desc->description !!}
                    </div>
                </div>
                <div class="w-full lg:w-1/3 xl:px-6 lg:px-3 xl:mt-[-13px]">
                    <div class="sticky top-5">
                        <div class="block lg:mb-9 mb-6 text-center">
                            <a href="javascript:;" title="" data-bs-toggle="modal" data-bs-target="#recruitment" class="lg:block inline-block bg-red rounded-[33px] xl:p-[18px_40px] p-[13px_30px] font-san font-bold text-16 uppercase text-white text-center transition-all hover:transition-all hover:bg-darkBlue">{{__('site.ungtuyenngay')}}</a>
                        </div>
                        <?php
                        $tabItem = json_decode($data->desc->item); ?>
                        @if(!empty($tabItem) && is_array($tabItem))
                        <ul class="bg-[#f5f5f5] p-3 lg:px-6 lg:mb-9 mb-6">
                            @foreach($tabItem as $item)
                            <li class="font-san text-16 text-gray1 lg:mb-6 mb-4 last:mb-0">
                                <span class="block font-bold"> {{@$item->item_title}} </span>
                                <span class="block font-normal"> {!! nl2br(@$item->item_description) !!} </span>
                            </li>
                            @endforeach
                        </ul>
                        @endif
                        @php($cateType = \App\Containers\Category\Enums\CategoryType::RECRUIT)
                        <x-news-type :data="$data" :link="'link'" :file="'news.recrui-involve'" :cateType="$cateType" limit="5"></x-news-type>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade fixed top-0 left-0 hidden w-full h-full outline-none overflow-x-hidden overflow-y-auto" id="recruitment" tabindex="-1" aria-labelledby="exampleModalCenteredScrollable" aria-modal="true" role="dialog">
        <div id="page-contact" class="modal-dialog modal-dialog-centered modal-dialog-scrollable relative max-w-[803px] w-auto px-3 pointer-events-none">
            <div class="modal-content border-none shadow-lg relative flex flex-col w-full pointer-events-auto bg-white bg-clip-padding rounded-md outline-none text-current">
                <button
                    type="button"
                    class="btn-close absolute top-[7px] right-[7px] z-[2] box-content w-4 h-4 p-1 text-black border-none rounded-none opacity-50 focus:shadow-none focus:outline-none focus:opacity-100 hover:text-black hover:opacity-75 hover:no-underline"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                ></button>
                <div class="modal-body relative lg:p-11 md:p-7 p-4">
                    <h2 class="block font-san font-bold md:text-[25px] md:leading-[34px] text-[20px] leading-[28px] uppercase text-center text-black2 md:mb-7 mb-4">
                        {{__('site.ungtuyenchovitrinay')}}
                    </h2>
                    <div>
                        <div class="relative flex flex-wrap items-stretch md:-mx-4">
                            <input type="hidden" name="type" id="typeContact" value="{{ App\Containers\Contact\Models\Contact::TYPE_R }}">
                            <div class="w-full md:w-1/2 md:px-4 mb-[20px]">
                                <span class="block font-san font-normal text-14 text-black2 mb-2">{{__('site.hovaten')}} <span class="inline-block text-red">*</span> </span><input class="block w-full bg-white border border-[#e0e0e0] border-solid rounded-[3px] p-2 text-black2 text-14" v-model="info.name" placeholder="" />
                            </div>
                            <div class="w-full md:w-1/2 md:px-4 mb-[20px]">
                                <span class="block font-san font-normal text-14 text-black2 mb-2">{{__('site.email')}} <span class="inline-block text-red">*</span> </span><input type="email" class="block w-full bg-white border border-[#e0e0e0] border-solid rounded-[3px] p-2 text-black2 text-14" v-model="info.email" placeholder="" />
                            </div>
                            <div class="w-full md:w-1/2 md:px-4 mb-[20px]">
                                <span class="block font-san font-normal text-14 text-black2 mb-2">{{__('site.sodienthoai')}} <span class="inline-block text-red">*</span> </span><input type="text" class="block w-full bg-white border border-[#e0e0e0] border-solid rounded-[3px] p-2 text-black2 text-14" v-model="info.phone" placeholder="" onkeypress="return shop.numberOnly()"></input>
                            </div>
                            <div class="w-full md:w-1/2 md:px-4 mb-[20px]">
                                <span class="block font-san font-normal text-14 text-black2 mb-2">{{__('site.namsinh')}} <span class="inline-block text-red">*</span> </span><input type="text" class="block w-full bg-white border border-[#e0e0e0] border-solid rounded-[3px] p-2 text-black2 text-14" v-model="info.birthday" placeholder=""  onkeypress="return shop.numberRic()"/>
                            </div>
                            <div class="w-full md:px-4 mb-[20px]">
                                <span class="block font-san font-normal text-14 text-black2 mb-2">{{__('site.bangcap')}} <span class="inline-block text-red">*</span> </span><input class="block w-full bg-white border border-[#e0e0e0] border-solid rounded-[3px] p-2 text-black2 text-14" v-model="info.degree" placeholder="" />
                            </div>
                            <div class="w-full md:px-4 mb-[20px]">
                                <span class="block font-san font-normal text-14 text-black2 mb-2">{{__('site.kinhnghiem')}} <span class="inline-block text-red">*</span> </span><input class="block w-full bg-white border border-[#e0e0e0] border-solid rounded-[3px] p-2 text-black2 text-14" v-model="info.experience" placeholder="" />
                            </div>
                            <div class="w-full md:px-4 relative">
                                <input @change="uploadFile" accept=".doc,.docx,.pdf" type="file"  class="block w-full bg-white border border-[#e0e0e0] border-solid rounded-[3px] p-2 text-black2 text-14 cursor-pointer" placeholder="" ref="file" />
                                 <a
                                 href="javascript:"
                                 title=""
                                 class="absolute bottom-0 md:left-4 md:right-4 left-0 right-0 w-auto p-[12px] block bg-[#f5f5f5] text-center font-san font-normal text-14 text-black2 cursor-pointer pointer-events-none hover:bg-red"
                                 >{{__('site.taicvcuaban')}}</a>
                            </div>
                            <div class="w-full md:px-4 mb-[20px]">
                                 <span class="block font-san font-normal text-14 text-black2 mb-2" id="showFile"></span>
                            </div>

                            <div class="block w-full text-center xl:mt-[50px] md:mt-[30px] mt-[15px]">
                                <button @click="saveContact" class="inline-block bg-red font-san font-bold text-14 text-white p-3 max-w-[210px] w-full transition-all hover:transition-all hover:bg-darkBlue">
                                    {{__('site.nophoso')}}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php($type = \App\Containers\Category\Enums\CategoryType::RECRUIT)
    <x-news-type :data="$data" :link="'linkProject'" :type="$type" :limit="5"></x-news-type>

</main>

@endsection


@push('js_bot_all')
<script>
    const url_api_save_contact = "{{route('home.contact.store')}}"
</script>
{!! FunctionLib::addMedia('js/pages/base/contact.js') !!}

@endpush