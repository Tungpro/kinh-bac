@if(!empty($page))
<div class="container px-0">
    <div class="award__slogan max-w-[676px] mb-[40px]"><h3 class="title-general horizontal-line">{{@$page->desc->name}}</h3>
        <div class="desc">
            {!! nl2br($page->desc->short_description) !!}
        </div>
    </div>
</div>
@endif