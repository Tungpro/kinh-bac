@if(isset($dataInvolve) && $dataInvolve->isNotEmpty())

<div class="block">
    <h4 class="block font-san font-bold text-18 text-gray1 mb-3 uppercase">{{__('site.tintuyendungkhac')}}</h4>
    <ul>
        @foreach($dataInvolve as $item)
        <li class="before:content-['\2022'] before:inline-block before:w-4 flex items-baseline">
            <a href="{{$item->linkRecruit()}}"  class="font-san font-normal text-16 text-gray1">{{$item->desc->name}}</a>
        </li>
        @endforeach
    </ul>
</div>
@endif