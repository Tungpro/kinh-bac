@if(!empty($page))
    <section
        class="total-number-section bg-section py-[50px] xl:p-[116px_0px_147px] relative vertical-line relative"
        style="background-image: url('{{@$page->getImageUrl('largex2','banner_image')}}')"
    >
        <div class="container">
            <?php $itemTabs = json_decode(@$page->desc->item, true); ?>
            @if(!empty($itemTabs) && is_array($itemTabs))
                @foreach($itemTabs as $item)
                    <div class="item text-center">
                        <div
                            class="timer count-title count-number  number text-center font-san uppercase text-[40px] md:text-[72px] leading-[56px]
                             md:leading-[86px] text-[#55C1FF] font-[700] flex items-center justify-center">
                            {{@$item['item_title']}}
                        </div>
                        <div
                            class="text-white text-[19px] leading-[29px] uppercase tracking-[-0.03em] font-[600] max-w-[190px] mx-auto">
                            {{@$item['item_description']}}
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </section>
@endif