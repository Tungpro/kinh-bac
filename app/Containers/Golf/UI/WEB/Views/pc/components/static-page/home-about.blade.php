@if(!empty($page))
<section class="consultation relative my-[30px] xl:m-[100px_0px_138px]" id="page-contact" data-opacity-content>
    <div class="container">
        {{--        <div class="consultation__form relative z-[2] bg-white p-[20px] md:p-[29px_25px]">--}}
        {{--            <h3 class="horizontal-line font-utm uppercase tracking-[0.05em] text-black2 text-20 pb-[10px] mb-[25px]">{{__('site.dangkynhantuvan')}}</h3>--}}
        {{--            <input type="hidden" name="type" id="typeContact" value="{{ App\Containers\Contact\Models\Contact::TYPE_C }}">--}}
        {{--            <div class="mb-[23px]">--}}
        {{--                <input type="text"  v-model="info.name" placeholder="{{__('site.hovatencuaban')}}" />--}}
        {{--            </div>--}}
        {{--            <div class="mb-[23px]">--}}
        {{--                <input type="text"  v-model="info.phone" onkeypress="return shop.numberOnly()" placeholder="{{__('site.sodienthoai')}}" />--}}
        {{--            </div>--}}
        {{--            <div class="mb-[39px]">--}}
        {{--                <input type="text" v-model="info.email" placeholder="{{__('site.email')}}" />--}}
        {{--            </div>--}}
        {{--            <button @click="saveContact" class="consultation__btn text-white bg-red font-14">--}}
        {{--                {{__('site.dangkyngay')}}--}}
        {{--                <span class="inline-block ml-[6px]">--}}
        {{--                  <svg width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
        {{--                    <path--}}
        {{--                        d="M11.4246 0.21748C11.2293 0.0222179 10.9127 0.022218 10.7175 0.21748C10.5222 0.412742 10.5222 0.729325 10.7175 0.924587L11.4246 0.21748ZM15 4.50001L15.3536 4.85356L15.7071 4.50001L15.3536 4.14646L15 4.50001ZM10.7175 8.07544C10.5222 8.2707 10.5222 8.58728 10.7175 8.78254C10.9127 8.9778 11.2293 8.9778 11.4246 8.78254L10.7175 8.07544ZM0.642079 4.00001C0.365936 4.00001 0.142079 4.22387 0.142078 4.50001C0.142078 4.77615 0.365936 5.00001 0.642078 5.00001L0.642079 4.00001ZM10.7175 0.924587L14.6465 4.85356L15.3536 4.14646L11.4246 0.21748L10.7175 0.924587ZM14.6465 4.14646L10.7175 8.07544L11.4246 8.78254L15.3536 4.85356L14.6465 4.14646ZM15 4.00001H0.642079L0.642078 5.00001H15L15 4.00001Z"--}}
        {{--                        fill="white"--}}
        {{--                    />--}}
        {{--                  </svg>--}}
        {{--                </span>--}}
        {{--            </button>--}}
        {{--        </div>--}}
        <div class="consultation__form relative z-[2]">
            <img src="{{$page->getImageUrl('large1')}}" alt="{{@$page->desc->name}}"/>
        </div>

            <div class="consultation__content p-[20px] btn-red btn-section bg-[#F5F5F5]">
                <div class="xl:max-w-[577px] xl:ml-9">
                    <h3 class="uppercase relative font-iciel text-black2 text-24 xl:text-32 horizontal-line pb-[10px] mb-[36px]">
                        {{@$page->desc->name}}
                    </h3>
                    <div class="mb-[37px]">
                        <p class="text-gray1 text-16">
                            {!! nl2br(@$page->desc->short_description) !!}
                        </p>
                    </div>

                    <a href="{{!empty($page->desc->link) ? $page->desc->link : 'javascript:;' }}"
                       class="text-black2 hover:text-red hover-text-red flex items-center">
                        <svg width="37" height="8" viewBox="0 0 37 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0 4H35.5M35.5 4L32.5 0.5M35.5 4L32.5 7.5" stroke="#EC2029"/>
                        </svg>
                        <span class="text-14 ml-[10px]">{{__('site.timhieuthem')}}</span>
                    </a>
                </div>
            </div>

    </div>
</section>
@endif
{{--@push('js_bot_all')--}}
{{--    @push('js_bot_all')--}}
{{--        <script> const url_api_save_contact = '{{ route('home.contact.store') }}';</script>--}}
{{--        {!! FunctionLib::addMedia('js/pages/base/contact.js') !!}--}}
{{--    @endpush--}}
{{--@endpush--}}

