
@if(isset($page) && $page->isNotEmpty())
<section class="missions-history mb-[50px] xl:mb-[131px]">
    @foreach($page as $item)
    <div class="item mb-6 md:mb-0">
        <div class="item-content mb-6 md:mb-0">
            <h3 class="title-general lg:max-w-[330px] !mb-[10px] !xl:mb-[34px] !pb-0">{{@$item->desc->name}}</h3>
            <div class="desc text-gray1 mb-[34px]">
                 {!! nl2br(@$item->desc->short_description) !!}
            </div>
            <a href="{{!empty($item->desc->link) ? $item->desc->link : 'javascript:;'}}" class="btn-outline">
                <svg width="37" height="8" viewBox="0 0 37 8" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 4H35.5M35.5 4L32.5 0.5M35.5 4L32.5 7.5" stroke="#EC2029"/>
                </svg>
                {{__('site.timhieuthem')}}</a></div>
        <div class="item-image"><img src="{{$item->getImageUrl('large')}}" alt="{{@$item->desc->name}}"/></div>
    </div>
    @endforeach
</section>

@endif

