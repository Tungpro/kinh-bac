@if(!empty($page))
<div class="content-detail mt-[50px]">
    <h3 class="title-general relative horizontal-line max-w-[336px]">{{@$page->desc->name}}</h3>
    <div>
         {!! @$page->desc->description !!}
    </div>
</div>
@endif