@if(isset($dataInvolve) && $dataInvolve->isNotEmpty())
    <section class="block xl:mb-20 md:mb-14 mb-8">
        <div class="container">
            <h2 class="block font-utm font-normal uppercase xl:text-36 md:text-28 text-20 text-gray1 lg:mb-9 mb-6">{{!empty($title) ? $title : ''}}</h2>

            <div id="relatedLinhvuc" class="relative">
                <!-- Swiper -->
                <div class="swiper news-difference">
                    <div class="swiper-wrapper h-auto">
                        @foreach($dataInvolve as $item)
                            <div class="swiper-slide">
                                <div class="mb-3">
                                    <a href="{{$link == 'linkProject' ? $item->linkProject() : $item->link() }}" class="block relative w-full lg:pb-[61%] pb-[52.25%]">
                                        <img
                                            class="absolute w-full h-full object-cover object-center top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%]"
                                            src="{{$item->getImageUrl('medium1')}}"
                                            alt="{{@$item->desc->name}}"
                                        />
                                    </a>
                                </div>
                                <a href="{{$link == 'linkProject' ? $item->linkProject() : $item->link() }}" class="line-clamp-2 font-utm font-normal md:text-18 text-16 uppercase text-black2 mb-2 hover:text-red hover-text-red">
                                    {{@$item->desc->name}}
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="swiper-button-next">
                    <img src="{{asset('template/dist/images/swiper-arrow-next.svg')}}" alt=""/>
                </div>
                <div class="swiper-button-prev">
                    <img src="{{asset('template/dist/images/swiper-arrow-prev.svg')}}" alt=""/>
                </div>
            </div>
        </div>
    </section>
@endif