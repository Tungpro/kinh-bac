<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/{{\App\Ship\core\Foundation\LangLib::getLocaleFb()}}/sdk.js#xfbml=1&version=v12.0&appId={{@$settings['social']['fb_app_id']}}&autoLogAppEvents=1"
        nonce="Z0d0hcEm"></script>