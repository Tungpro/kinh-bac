<div class="fb-comments" data-href="{{url()->current()}}" data-width="100%" data-colorscheme="light"
     data-numposts="10" data-lazy="true"></div>

@push('css_bot_all')
    <style>
        .fb-comments, .fb-comments span, .fb-comments.fb_iframe_widget span iframe {
            width: 100% !important;
        }
    </style>
@endpush

@push('js_bot_all')
    @include("frontendvv::pc.fb-plugin.initialization")
@endpush