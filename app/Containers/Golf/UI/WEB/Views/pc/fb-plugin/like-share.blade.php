<div class="fb-like" data-href="{{url()->current()}}" data-width="{{isset($width) ? $width : 770}}"
     data-layout="button_count" data-action="like" data-size="small" data-share="{{isset($share) ? $share : true}}"
     data-lazy="true"></div>

@push('js_bot_all')
    @include("frontendvv::pc.fb-plugin.initialization")
@endpush