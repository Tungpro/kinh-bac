<div class="share my-[30px] xxl:my-[100px] hidden md:block">
    <div class="text-[20px] uppercase mb-[12px]">{{__('site.chiase')}}</div>
    <div class="flex items-center share-social-icon">
        <a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}{{!empty($picture)?"&picture=$picture":""}}{{!empty($title)?"&title=$title":""}}"
           target="_blank"><img src="{{asset('template/images/share-fb.png')}}" alt=""/></a>
        <a href="http://www.facebook.com/dialog/send?app_id={{@$settings['social']['facebook_app_id']}}&link={{url()->current()}}&redirect_uri={{url()->current()}}"
           target="_blank"><img src="{{asset('template/images/share-mess.png')}}" alt=""/></a>
        <a href="https://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}&title={{$title??''}}&summary={{$summary??''}}&source=LinkedIn"
           target="_blank"><img src="{{asset('template/images/share-linkin.png')}}" alt=""/></a>
    </div>
</div>
