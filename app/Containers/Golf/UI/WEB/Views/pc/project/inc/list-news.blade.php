@foreach($data as $item)
    <div class="lg:w-1/3 md:w-1/2 w-full px-3 lg:mb-10 mb-5">
        <article class="block">
            <div class="block mb-2">
                <a href="{{$item->linkProject()}}" class="block relative w-full lg:pb-[61%] pb-[52.25%]"
                ><img
                        class="absolute w-full h-full object-cover object-center top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%]"
                        src="{{$item->getImageUrl('medium4')}}"
                        alt="{{@$item->desc->name}}"
                    /></a>
            </div>
            <div class="block">
                <a href="{{$item->linkProject()}}"  class="line-clamp-2 font-utm font-normal md:text-24 text-18 uppercase text-black2 mb-6 hover:text-red hover-text-red"
                >{{@$item->desc->name}}</a
                >
                <p class="line-clamp-3 font-san font-normal md:text-16 text-14 text-gray1 mb-6">
                    {!! @$item->desc->short_description !!}
                </p>
                <a href="{{$item->linkProject()}}" class="block text-14 text-black2 hover:text-red hover-text-red"
                ><img src="{{asset('template/dist/images/svg-arrow-red.svg')}}" alt="" class="inline-block mr-5" /> {{__('site.xemchitiet')}}</a
                >
            </div>
        </article>
    </div>
@endforeach