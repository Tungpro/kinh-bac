@extends('basecontainer::frontend.pc.layouts.home')

@section('content')

    <main>
        <section class="block w-full md:h-[45px] h-[20px] bg-darkBlue lg:mb-9 mb-4"></section>
        <section class="block">
            <div class="container">
                <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>
            </div>
        </section>
        @if($data)
        <section class="block detail-dist">
            <div class="container">


                <div class="block mb-7">
                    <h2 class="block font-iciel font-normal lg:text-40 md:text-32 text-24 uppercase text-gray1">{{@$data->desc->name}}</h2>
                </div>

                @if($data->layout == \App\Containers\News\Enums\NewsType::LAYOUT_PROJECT_IMAGE)
                <div class="block lg:mb-14 mb-4">
                    <div class="relative flex flex-wrap lg:-mx-4">
                        <div class="lg:flex-[0_0_62%] lg:max-w-[62%] w-full lg:px-4 mb-5 lg:mb-0">
                            <div class="swiper-sync">
                                @if(isset($data->medias) && $data->medias->isNotEmpty())
                                <div class="swiper-main relative mb-4">
                                    <!-- Swiper -->

                                    <div class="swiper mySwiper">
                                        <div class="swiper-wrapper h-auto cursor-pointer">

                                            @foreach($data->medias as $key => $item)

                                                @if($key+1 == count($data->medias) && !empty($data->youtube))
                                                <div class="swiper-slide swiper-slide-main">
                                                    <div class="relative w-full pb-[56.5%]">
                                                       {!! @$data->youtube !!}
                                                    </div>
                                                </div>
                                                 @else
                                                    <div class="swiper-slide">
                                                        <div class="relative w-full pb-[56.5%]">
                                                            <img
                                                                class="absolute w-full h-full top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%] object-cover object-center"
                                                                src="{{$item->getImageUrl('large2')}}"
                                                                alt="{{@$data->desc->name}}"
                                                            />
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach

                                        </div>

                                        <!-- pagi -->
                                        <div class="swiper-pagination"></div>

                                    </div>

                                    <!-- nav -->
                                    <div class="swiper-button-next">
                                        <img src="{{asset('template/dist/images/ic-arrow-next-white.svg')}}" alt="" />
                                    </div>
                                    <div class="swiper-button-prev">
                                        <img src="{{asset('template/dist/images/ic-arrow-prev-white.svg')}}" alt="" />
                                    </div>
                                </div>

                                <div class="swiper-thumbs">
                                    <!-- Swiper -->
                                    <div class="swiper mySwiper">
                                        <div class="swiper-wrapper h-auto cursor-pointer">
                                            @foreach($data->medias as $key => $item)
                                            <div class="swiper-slide {{$key+1 == count($data->medias)  && !empty($data->youtube) ? 'swiper-video' : ''}}">
                                                <div class="relative w-full pb-[56.5%]">
                                                    <img
                                                        class="absolute w-full h-full top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%] object-cover object-center"
                                                        src="{{$item->getImageUrl('medium')}}"
                                                        alt="{{@$data->desc->name}}"
                                                    />
                                                </div>
                                            </div>
                                             @endforeach
                                        </div>
                                    </div>
                                </div>

                                @endif
                            </div>
                        </div>
                        <div class="lg:flex-[0_0_38%] lg:max-w-[38%] w-full lg:px-4">
                            <div class="block lg:mt-6">
                                <h2
                                    class="relative block font-utm font-normal text-24 text-red uppercase pb-2 mb-4 before:content-[''] before:absolute before:bg-current before:w-[75px] before:h-[2px] before:bottom-0 before:left-0"
                                >
                                    {{__('site.gioithieuduan')}}
                                </h2>
                                <?php $tabItem = json_decode($data->desc->item); ?>
                                @if(!empty($tabItem) && is_array($tabItem))
                                <ul>
                                    @foreach($tabItem as $item)
                                    <li class="relative flex flex-wrap items-baseline mb-4">
                                        <span class="block font-san font-bold text-16 text-black2 md:min-w-[115px] min-w-[auto] mr-6"> {{@$item->item_title}} </span>
                                        <span class="block font-san font-normal text-16 text-gray1 flex-1"> {!! nl2br(@$item->item_description) !!} </span>
                                    </li>
                                    @endforeach
                                </ul>
                                 @endif

                            </div>
                        </div>
                    </div>
                </div>
                @endif

                <div class="block xl:mb-20 md:mb-16 mb-10">
                    {!! @$data->desc->description !!}
                </div>
            </div>
        </section>
        @endif

        <x-news-type :data="$data" :link="'linkProject'"></x-news-type>

    </main>

@endsection