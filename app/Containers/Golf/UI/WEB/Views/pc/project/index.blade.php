@extends('basecontainer::frontend.pc.layouts.home')

@section('content')

    <main>
        @if($banner)
            <section class="banner relative">
                <img src="{{@$banner->getImageUrl('super_large')}}" alt="{{@$banner->desc->name}}"/>
            </section>
        @endif
        <section class="block lg:py-[60px] py-[30px]">
            @if(!empty($banner))
                <div class="block xl:mb-[75px] md:mb-[40px] mb-[20px]">
                    <div class="container">
                        <h2 class="text-gray1 uppercase lg:text-40 sm:text-32 text-24 text-center font-iciel font-normal md:mb-[20px] mb-[10px]">{{$banner->desc->name}}</h2>
                        <span
                            class="block max-w-[970px] w-[100%] mx-auto font-san font-normal sm:text-16 text-14 text-center text-gray1"
                        >{!! nl2br(@$banner->desc->description) !!}</span
                        >
                    </div>
                </div>
            @endif

            @if(isset($categories) && $categories->isNotEmpty())
                <div class="block xl:mb-[60px]">
                    <div class="container">
                        <div class="qhcd-list">
                            @foreach($categories as $item)
                                <div
                                    class="qhcd-item relative lg:flex flex-wrap items-stretch xl:mb-[55px] mb-[30px] last:mb-0">
                                    <div
                                        class="image lg:flex-auto lg:max-w-[58%] relative w-full lg:pb-[35%] pb-[52.25%] overflow-hidden">
                                        <a href="{{@$item->link()}}">
                                            <img
                                                class="absolute w-full h-full object-contain object-center top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%]"
                                                src="{{$item->getImageUrl('social')}}"
                                                alt="{{@$item->desc->name}}"/>
                                        </a>
                                    </div>

                                    <div
                                        class="content lg:flex-1 lg:max-w-[300px] lg:flex lg:flex-col lg:justify-center lg:py-[30px] lg:mt-0 mt-4">
                                        <a href="{{@$item->link()}}" title=""
                                           class="block text-red xl:text-32 smtext-16:text-28 text-22 uppercase font-normal font-utm xl:mb-[40px] mb-[10px] line-clamp-1">
                                            {{@$item->desc->name}}
                                        </a>
                                        <p class="block font-san font-normal sm:text-16 text-14 text-gray1 mb-4 line-clamp-6">
                                            {!! @$item->desc->description !!}
                                        </p>
                                        <div class="block w-full mt-auto">
                                            <a href="{{@$item->link()}}" class="viewmore btn-outline">
                                                <svg width="37" height="8" viewBox="0 0 37 8" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M0 4H35.5M35.5 4L32.5 0.5M35.5 4L32.5 7.5"
                                                          stroke="#EC2029"/>
                                                </svg>
                                                {{__('site.timhieuthem')}}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            @endif

        </section>
        <x-partner-type partnerTypeId="0" returnView="partner-list-page"></x-partner-type>
    </main>
    @push('js_bot_all')

    @endpush
@endsection