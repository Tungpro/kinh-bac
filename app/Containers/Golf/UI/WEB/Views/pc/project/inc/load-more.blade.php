
@if($data->currentPage() < $data->lastPage())
<div class="w-full xl:mb-20 xl:mt-10 md:my-12 my-7">
    <div class="block w-full text-center mt-auto">
        <a href="javascript:;" onclick="loadMoreData({{ $data->currentPage() + 1}})" class="viewmore btn-outline">
            <svg width="37" height="8" viewBox="0 0 37 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 4H35.5M35.5 4L32.5 0.5M35.5 4L32.5 7.5" stroke="#EC2029"/>
            </svg>

            {{__('site.timhieuthem')}}
        </a>
    </div>
</div>
@endif