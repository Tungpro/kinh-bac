@extends('basecontainer::frontend.pc.layouts.home')

@section('content')

<main>
    <section class="block w-full md:h-[45px] h-[20px] bg-darkBlue lg:mb-9 mb-4"></section>
    <section class="block">
        <div class="container">
            <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>
        </div>
    </section>

    @if(!empty($category))
    <section class="block">
        <div class="container">
            <div class="block mb-7">
                <h2 class="block font-iciel font-normal xl:text-40 lg:text-32 text-24 uppercase text-gray1 mb-4">{{@$category->desc->name}}</h2>
                <span class="block font-san font-normal text-16 text-gray1">{!! @$category->desc->description !!}</span>
            </div>
        </div>
    </section>
    @endif
    @if(isset($news) && $news->isNotEmpty())
    <section class="block">
        <div class="container">
            <div class="relative flex flex-wrap -mx-3"  id="render_data">
                @include('golf::pc.project.inc.list-news', ['data' => $news])
                <input type="hidden" value="{{ @$category->desc->name }}" id="title">
                <input type="hidden" value="{{ @$category->category_id }}" id="cate_ids">
            </div>
            <div id="loadMoreButton">
              @include('golf::pc.project.inc.load-more', ['data' => $news])
            </div>
        </div>
    </section>
    @endif
    <x-partner-type partnerTypeId="0" returnView="partner-list-page"></x-partner-type>
</main>

@endsection


<script>
    function loadMoreData(currentPage, lang){

        $.ajax({
            url: "{{ route('web.project.category-detail-ajax') }}",
            dataType: 'json',
            data: { name: $('#title').val(), category_id: $('#cate_ids').val(), page: currentPage, lang: lang},
            success: function (json) {
                if(json.error == 0)
                {
                    $('#render_data').append(json.data.html);
                    $('#loadMoreButton').html(json.data.loadmore);
                }
            }
        })
    }
</script>
