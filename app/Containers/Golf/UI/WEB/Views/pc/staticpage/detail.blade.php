@extends('basecontainer::frontend.pc.layouts.home')

@section('content')

    <section class="block w-full md:h-[45px] h-[20px] bg-darkBlue lg:mb-9 mb-4"></section>
    <section class="block">
        <div class="container">
            <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>
        </div>
    </section>
    <section class="block xl:mb-[120px] md:mb-[80px] mb-[40px] detail-dist">
        <div class="container">
            <h2 class="block font-iciel font-normal lg:text-40 text-24 uppercase text-gray1 lg:mb-8 mb-5">
                {{@$page->desc->name}}
            </h2>
            <p class="font-san font-normal text-16 text-black2 mb-8">
                {!! @$page->desc->description !!}
            </p>
        </div>
    </section>

@endsection
