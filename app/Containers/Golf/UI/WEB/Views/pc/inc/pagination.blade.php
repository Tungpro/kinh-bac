@if ($paginator->hasPages())

    <div class="pagination flex flex-wrap items-center gap-2">
            @if ($paginator->onFirstPage())
                    <a class="disabled w-[32px] h-[32px] leading-[32px] text-center rounded-[5px] bg-[#f5f5f5]" aria-label="true">
                        <img src="{{asset('template/dist/images/ic-left.svg')}}" alt="" class="block mx-auto w-full h-full object-scale-down" />
                    </a>
            @else
                    <a class="w-[32px] h-[32px] leading-[32px] text-center rounded-[5px] bg-[#f5f5f5]" href="{{ $paginator->previousPageUrl() }}" rel="prev"
                       aria-label="@lang('pagination.previous')">
                        <img src="{{asset('template/dist/images/ic-left.svg')}}" alt="" class="block mx-auto w-full h-full object-scale-down" />
                    </a>
            @endif

            @foreach ($elements as $element)
                @if (is_string($element))<span class="disabled inline-block font-san font-normal text-16 text-black2 w-[32px] h-[32px] leading-[32px] text-center rounded-[5px]">{{ $element }}</span>
                @endif

                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                           <a class="active inline-block font-san font-normal text-16 text-black2 w-[32px] h-[32px] leading-[32px] text-center rounded-[5px]">{{ $page }}</a>
                        @else
                           <a class="inline-block font-san font-normal text-16 text-black2 w-[32px] h-[32px] leading-[32px] text-center rounded-[5px]" href="{{ $url }}">{{ $page }}</a>
                        @endif
                    @endforeach
                @endif
            @endforeach

            @if ($paginator->hasMorePages())
                    <a class="w-[32px] h-[32px] leading-[32px] text-center rounded-[5px] bg-[#f5f5f5]" href="{{ $paginator->nextPageUrl() }}" rel="next"
                       aria-label="@lang('pagination.next')">
                        <img src="{{asset('template/dist/images/arrow-grey-right.svg')}}" alt="" class="block mx-auto w-full h-full object-scale-down" />
                    </a>
            @else
                    <a class="disabled w-[32px] h-[32px] leading-[32px] text-center rounded-[5px] bg-[#f5f5f5]" aria-hidden="true">
                               <img src="{{asset('template/dist/images/arrow-grey-right.svg')}}" alt="" class="block mx-auto w-full h-full object-scale-down" />
                    </a>
            @endif
    </div>
@endif

