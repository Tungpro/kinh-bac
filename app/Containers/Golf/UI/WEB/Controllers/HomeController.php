<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-07 10:28:53
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-05 00:50:43
 * @ Description: Happy Coding!
 */

namespace App\Containers\Golf\UI\WEB\Controllers;

use App\Containers\BaseContainer\UI\WEB\Controllers\BaseFrontEndController;
use App\Containers\Golf\UI\WEB\Controllers\Features\Home\Index;

class HomeController extends BaseFrontEndController
{
    use Index;
}
