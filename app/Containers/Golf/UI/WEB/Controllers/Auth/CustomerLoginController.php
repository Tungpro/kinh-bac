<?php

namespace App\Containers\Golf\UI\WEB\Controllers\Auth;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Authentication\Actions\WebCheckCustomerAction;
use App\Containers\Authentication\Data\Transporters\ProxyApiLoginTransporter;
use App\Containers\Authentication\Exceptions\LoginFailedException;
use App\Containers\Customer\Actions\StoreNewCustomerAction;
use App\Containers\Golf\UI\WEB\Requests\Auth\CustomerRegisterCompleteRequest;
use App\Containers\Golf\UI\WEB\Requests\Auth\CustomerRegisterRequest;
use App\Containers\Golf\UI\WEB\Requests\Auth\LoginRequest;
use App\Containers\ShoppingCart\Actions\Golf\DestroyCartAction;
use App\Ship\Transporters\DataTransporter;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;

class CustomerLoginController extends BaseLogin
{
    public function showLoginPage()
    {
        return $this->returnView('golf', 'auth.customer_login');
    }

    public function showRegisterPage()
    {
        return $this->returnView('golf', 'auth.customer_register');
    }

//     public function logout(Request $request)
//     {
//         $auth = auth()->guard(config('auth.guard_for.golf'));
//         // $token = $auth->user()->clients();
//         // $token->revoke();
// // dd($token);
//         // $auth->logout();
//         app(DestroyCartAction::class)->instance()->run();
//         return redirect(route('web_home_page'));
//     }

    private function genTokenApiCustomer($username, $password)
    {
        $fieldName = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
        $dataTransporter = array_merge([
            $fieldName => $username,
            'password' => $password,
        ], [
            'client_id'       => Config::get('authentication-container.clients.web.customer.id'),
            'client_password' => Config::get('authentication-container.clients.web.customer.secret')
        ]);
        // dd($dataTransporter->toArray());
        $content = Apiato::call('Authentication@Golf\ProxyApiGolfLoginAction', [$dataTransporter]);

        if ($content) {
        }

        return $content;
    }

    public function login(LoginRequest $request)
    {
        try {
            $username = $request->username;
            $password = $request->password;
            $user = app(WebCheckCustomerAction::class)->run([
                'username' => $username,
                'password' => $password,
                'rememberLogin' => true
            ]);
            if (!is_array($user)) {
                $redirectURL = !empty($request->previous_url) ? $request->previous_url : route('web_home_page');

                $tokenContent = $this->genTokenApiCustomer($username, $password);

                return $this->sendResponse(['user' => [
                    // 'is_active' => $user->is_active,
                    'email' => $user->email,
                    'phone' => $user->phone,
                    'token' => $tokenContent
                ], 'url' => $redirectURL]);
            }
        } catch (Exception $e) {
            return FunctionLib::ajaxRespondV2(false, $e instanceof LoginFailedException ? 'Thông tin đăng nhập không chính xác' : $e->getMessage(), ['url' => '#'], Response::HTTP_UNAUTHORIZED);
        }
    }

    public function register(CustomerRegisterRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = new DataTransporter([
                'email' => $request->email,
                'phone' => $request->phone,
                'fullname' => $request->fullname,
                'password' => $request->password,
                'status' => 2
            ]);
            $user = app(StoreNewCustomerAction::class)->run($data);

            $user = app(WebCheckCustomerAction::class)->run([
                'username' => $request->email,
                'password' => $request->password,
                'rememberLogin' => true
            ]);

            DB::commit();

            if (!empty($user)) {

                $redirectURL = !empty($request->previous_url) ? $request->previous_url : route('web_home_page');

                $tokenContent = $this->genTokenApiCustomer($request->email, $request->password);

                return $this->sendResponse(['user' => [
                    // 'is_active' => $user->is_active,
                    'email' => $user->email,
                    'phone' => $user->phone,
                    'token' => $tokenContent
                ], 'url' => $redirectURL]);
            }

            return $this->sendResponse([
                'success' => true,
                'url' => '',
            ], 'Đăng ký thành công');
        } catch (\Exception $e) {
            // throw $e;
            DB::rollBack();
            return $this->sendResponse([
                'sucess' => false,
            ], 'Đăng ký không thành công');
        }
    }

    public function showCompletePage(CustomerRegisterCompleteRequest $request)
    {
        return $this->returnView('authentication::' . $this->screen . '.owner_register_complete');
    }
}
