<?php

namespace App\Containers\Golf\UI\WEB\Controllers\Auth;

use Apiato\Core\Abstracts\Controllers\WebController as ControllersWebController;
use Illuminate\Support\Str;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\WebController;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Containers\Authentication\UI\WEB\Requests\Admin\LogoutRequest;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseGolfController;
use App\Containers\Golf\UI\WEB\Requests\Auth\GenerateOTPCodeRequest;
use App\Containers\Localization\Values\Region;
use App\Ship\core\Traits\HelpersTraits\OTPTrait;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class Controller
 *
 * @author  Ha Ly Manh  <lymanhha@gmail.com>
 */
class BaseLogin extends ControllersWebController
{
    use ApiResTrait, OTPTrait;
    protected $is_contractor;
    /**
     *
     */
    public function logout(LogoutRequest $request)
    {
        Apiato::call('Authentication@WebLogoutAction');
        return redirect(route('web_home_page'));
    }

    /**
     *
     */
    public function genarateCode(GenerateOTPCodeRequest $request)
    {
        request()->session()->put('otpData', [
            'method' => encrypt($request->get('method', 'email')),
            'value' => encrypt($request->get('value', ''))
        ]);
        DB::beginTransaction();
        try{
            $this->otpCode();
            DB::commit();
            return $this->sendResponse([
                'success' => true,
            ], $request->send_again == 1 ? 'Mã OTP đã được gửi lại' : 'Nhập mã xác minh đã được chúng tôi gửi đến email của bạn  để xác nhận yêu cầu thay đổi phương thức nhận mã xác minh');

        }catch(\Exception $e){
            DB::rollBack();
            return $this->sendResponse([
                'success' => false
            ], 'Gửi OTP code không thành công', 422);
        }
    }

    /**
     *
     */
    public function checkCode(LogoutRequest $request){
        /**
         * Get Data From Session
         */
        $sesssionUserData = request()->session()->get('user');
        $sesssionOtpData = request()->session()->get('otpData');
        /**
         * Mark User enabled 2 Layer Protection
         */
        DB::beginTransaction();
        try{
            // Apiato::call('Authentication@CheckOTPCodeAction', [ new DataTransporter(array_merge($request->all(), [ 'user_id' => auth('customer')->id() ?? decrypt($sesssionUserData['user_id']) ])) ]);

            $responseCheckCode = $this->checkOtpCode($request->otp_code, decrypt($sesssionUserData['user_id']));
            if(!$responseCheckCode){
                return $this->sendResponse([
                    'success' => false ], 'Mã OTP Không đúng hoặc Hết hạn');
            }
            /**
             * get user data from Session
             */
            $userId = decrypt($sesssionUserData['user_id']);
            $email = decrypt($sesssionUserData['email']);
            $phone = decrypt($sesssionUserData['phone']);
            /**
             * Get method that User was chosen from session
             * Update User Data
             */
            $method = decrypt($sesssionOtpData['method']);
            if(!$email && $method == 'email'){
                $userData = new DataTransporter([
                    'id' => $userId,
                    'email' => decrypt($sesssionOtpData['value'])
                ]);
                Apiato::call('Customer@UpdateCustomerAction', [ $userData ]);
            }elseif(!$phone && $method == 'phone'){
                $userData = new DataTransporter([
                    'id' => $userId,
                    'phone' => decrypt($sesssionOtpData['value'])
                ]);
                Apiato::call('Customer@UpdateCustomerAction', [ $userData ]);
            }
            /**
             * Process Login after check Code
             */
            //  $sessionInputData = request()->session()->get('inputUser');
            //  $loginInformation = new DataTransporter([
            //     'username' => decrypt($sessionInputData['username']),
            //     'password' => decrypt($sessionInputData['password']),
            //     'is_contractor' => $this->is_contractor
            // ]);
            // Apiato::call('Authentication@WebLoginAction', [ $loginInformation ]);
            DB::commit();
            return $this->sendResponse([
                'success' => true,], 'Kích hoạt mã OTP thành công');
        }catch(\Exception $e){
            DB::rollBack();
            return $this->sendResponse([
                'success' => false ], $e->getMessage());
        }
    }

    public function remeberLogin(){
        $sessionInputData = request()->session()->get('inputUser');
        $loginInformation = new DataTransporter([
            'username' => decrypt($sessionInputData['username']),
            'password' => decrypt($sessionInputData['password']),
            'is_contractor' => $this->is_contractor,
            'remember' => request('remember', false)
        ]);
        Apiato::call('Authentication@WebLoginAction', [ $loginInformation ]);
        $this->forgetSessionLogin();
        return redirect()->to(route('home'));
    }

    protected function otpCode(){
        $sesssionUserData = request()->session()->get('user');
        $code = Str::random(config('authentication-container.otp.length'));
        $response = $this->sendOtp(request('method', 'email'), $code, decrypt($sesssionUserData['email']) ?? request('email'));
        if($response){
            Apiato::call('Authentication@GenerateOTPCodeAction', [
                new DataTransporter([
                    'user_id' => auth('customer')->id() ?? decrypt($sesssionUserData['user_id']),
                    'otp_code' => $code
                ])
            ]);
        }
        return $response;
    }

    private function forgetSessionLogin(){
        request()->session()->forget('user');
        request()->session()->forget('inputUser');
    }
}
