<?php

namespace App\Containers\Golf\UI\WEB\Controllers\Desktop\Testimonial;

use App\Containers\Banner\Actions\GetAvailableBannerByPositionAction;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseFrontEndController;
use App\Containers\Testimonial\Actions\GetAllTestimonialsAction;
use Illuminate\Http\Request;

class Controller extends BaseFrontEndController
{
    public function index()
    {

        $banner = app(GetAvailableBannerByPositionAction::class)->run(
            ['banner_testimonial_top'], $this->isMobile, [], ['sort_order' => 'ASC', 'id' => 'DESC'], $this->currentLang, 0
        );


//        $page = app(GetAvailablePageByPositionAction::class)->run(
//            ['holder_about'], false, ['desc'], ['sort_order' => 'ASC', 'id' => 'DESC'], ['*'], $this->currentLang, 0
//        );

        $payloadRequest = new Request();
        $payloadRequest->merge(['scopeFEAvailableData' => true]);
        $payloadRequest->merge(['skipCache' => false]);

        $data = app(GetAllTestimonialsAction::class)->run(
            $payloadRequest,
            null,
            $this->currentLang,
            ['desc'],
            [['type', '=', config('testimonial-container.type_id.t_default')]],
            ['id', 'image','status']
        );

        $this->frontBreadcrumb(__('site.quanhecodong'), route('web.holder.index'), false);
        $this->frontBreadcrumb(__('site.thongtinhoidapcodong'), '#', true);

        $this->generateMetaTag(null, __('site.thongtinhoidapcodong'));

        return $this->returnView('golf', 'testimonial.index', [
            'data' => $data,
//            'page' => $page,
            'banner' => $banner,
        ]);
    }

}
