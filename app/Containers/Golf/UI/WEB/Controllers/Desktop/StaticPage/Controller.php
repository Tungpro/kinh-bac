<?php

namespace App\Containers\Golf\UI\WEB\Controllers\Desktop\StaticPage;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Banner\Actions\GetAvailableBannerByPositionAction;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseFrontEndController;
use App\Containers\StaticPage\Actions\FrontEnd\GetAvailablePageByIdAction;

class Controller extends BaseFrontEndController
{
    public function detailPage($slug, $id)
    {

        if (!empty($slug) && !empty($id)) {

            $page =  app(GetAvailablePageByIdAction::class)->run(
                $id, $this->currentLang, [], ['id' => 'DESC'], [['status', '=', 2]]
            );

            if ($page && empty($page->position)) {
                $this->generateMetaTag($page);
                $this->frontBreadcrumb($page->desc->name, '#', true);
                if (!empty($page->image)) {
                    $this->settings['website']['image_seo'] = $page->getImageUrl('medium5');
                    $this->settings['website']['img_width'] = 407;
                    $this->settings['website']['img_height'] = 385;
                    view()->share('settings', $this->settings);
                }

                return $this->returnView('golf', 'staticpage.detail', [
                    'page' => $page,
                ]);
            }
        }

        return redirect()->route('web_home_page');
    }

    public function notFound404(){
        $banner =  app(GetAvailableBannerByPositionAction::class)->run(
            ['banner_notfound_top'], $this->isMobile, [], ['sort_order' => 'ASC', 'id' => 'DESC'], $this->currentLang, 0
        );

        return $this->returnView('golf', '404', [
            'banner' => $banner
        ]);
    }
}
