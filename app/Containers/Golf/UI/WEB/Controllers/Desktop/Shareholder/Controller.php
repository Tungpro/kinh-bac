<?php

namespace App\Containers\Golf\UI\WEB\Controllers\Desktop\Shareholder;

use App\Containers\Banner\Actions\GetAvailableBannerByPositionAction;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseFrontEndController;
use App\Containers\Category\Actions\FrontEnd\GetAaliableCategoryByPositionAction;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\News\Actions\FrontEnd\GetAaliableNewsCategoryAction;
use App\Containers\News\Actions\FrontEnd\GetNewsBySlugIdAction;
use App\Containers\News\Actions\FrontEnd\GetNewsListAction;

use App\Containers\StaticPage\Actions\FrontEnd\GetAvailablePageByPositionAction;
use App\Ship\core\Foundation\BladeHelper;
use Illuminate\Http\Request;

class Controller extends BaseFrontEndController
{
    public function index(Request $request)
    {
        $banner = app(GetAvailableBannerByPositionAction::class)->run(
            ['banner_holder_top'], $this->isMobile, [], ['sort_order' => 'ASC', 'id' => 'DESC'], $this->currentLang, 0
        );

        $categories = app(GetAaliableCategoryByPositionAction::class)->run(
            null, null, $this->currentLang, ['desc'],['category_id','type','status','image'],
            ['type' => CategoryType::HOLDER, 'status' => BladeHelper::HIEN_THI]
        );

        $page = app(GetAvailablePageByPositionAction::class)->run(
            ['holder_about'], false, ['desc'], ['sort_order' => 'ASC', 'id' => 'DESC'], ['*'], $this->currentLang, 0
        );

        $this->generateMetaTag(null, __('site.quanhecodong'));

        return $this->returnView('golf', 'shareholder.index', [
            'banner' => $banner,
            'categories' => $categories,
            'page' => $page,
        ]);

        return redirect()->route('web_home_page');

    }

    public function categoryDetail(Request $request,String $slug, int $category_id)
    {
        $category = app(GetAaliableCategoryByPositionAction::class)->run(
            null, 0, $this->currentLang, ['desc'],['category_id','type','hot','is_home'],
            ['type' => CategoryType::HOLDER, 'status' => BladeHelper::HIEN_THI, 'category_id' => $category_id]
        );



        if (!empty($category) && $category->type == CategoryType::HOLDER) {

            $categories = app(GetAaliableCategoryByPositionAction::class)->run(
                null, null, $this->currentLang, ['desc'],['category_id','type','status'],
                ['type' => CategoryType::HOLDER, 'status' => BladeHelper::HIEN_THI]
            );

            $news = app(GetNewsListAction::class)->run(
                null, 14, $this->currentLang, ['desc'],['id','status','sort_order','created_at','file_name'],
                ['status' => BladeHelper::HIEN_THI], ['category_id' => $category_id, 'status' => BladeHelper::HIEN_THI, 'created_at' => $request->created_at],0
            );

            $this->frontBreadcrumb(__('site.quanhecodong'), route('web.project.index'), false);
            $this->frontBreadcrumb(@$category->desc->name, '#', true);

            $this->updateMetaTag($category);

            return $this->returnView('golf', 'shareholder.category', [
                'news' => $news,
                'category' => $category,
                'categories' => $categories,
                'data_search' => $request
            ]);
        }


        return redirect()->route('web_home_page');
    }

    public function detail(string $slug, int $id)
    {
        $project = app(GetNewsBySlugIdAction::class)->run($id, $slug, $this->currentLang,['type' => CategoryType::HOLDER]);

        if(!empty($project->desc->file)){
            return redirect('/upload/news/'.$project->desc->file) ;
        }

        if (!empty($project->file_name)) {
            return redirect('/upload/news/'.$project->file_name) ;
        }

        return redirect()->route('web_home_page');
    }
}
