<?php

namespace App\Containers\Golf\UI\WEB\Controllers\Desktop\Search;

use App\Containers\Banner\Actions\GetAvailableBannerByPositionAction;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseFrontEndController;
use App\Containers\News\Actions\FrontEnd\GetNewsListAction;
use App\Containers\News\Enums\NewsStatus;
use Illuminate\Http\Request;


class Controller extends BaseFrontEndController
{
    public function index(Request $request)
    {
        $banner =  app(GetAvailableBannerByPositionAction::class)->run(
            ['banner_timkiem_top'], $this->isMobile, [], ['sort_order' => 'ASC', 'id' => 'DESC'], $this->currentLang, 0
        );

        $news = app(GetNewsListAction::class)->run(
            ['*'], ['status' => NewsStatus::ACTIVE,'name' => $request->name],
            ['sort_order' => 'asc', 'id' => 'desc'],9 , false, $this->currentLang, ['categories' => 'categories']
        );

        $this->frontBreadcrumb(__('site.timkiem'), '#', true);

        return $this->returnView('golf', "search.index",[
            'banner' => $banner,
            'news' => $news,
            'request' => $request,
        ]);
    }
}
