<?php

namespace App\Containers\Golf\UI\WEB\Controllers\Desktop\Recruit;

use App\Containers\Banner\Actions\GetAvailableBannerByPositionAction;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseFrontEndController;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\Category\Models\Category;
use App\Containers\News\Actions\FrontEnd\GetNewsBySlugIdAction;
use App\Containers\News\Actions\FrontEnd\GetNewsListAction;
use App\Containers\Testimonial\Actions\GetAllTestimonialsAction;
use App\Ship\core\Foundation\BladeHelper;

class Controller extends BaseFrontEndController
{
    public function index()
    {
        $banner = app(GetAvailableBannerByPositionAction::class)->run(
            ['banner_recruit_top'], $this->isMobile, [], ['sort_order' => 'ASC', 'id' => 'DESC'], $this->currentLang, 0
        );

        $news = app(GetNewsListAction::class)->run(
            null, 16, $this->currentLang, ['desc'],['id','status','sort_order','image'],
            ['status' => BladeHelper::HIEN_THI], ['cate_type' => CategoryType::RECRUIT, 'status' => BladeHelper::HIEN_THI],0
        );

        $this->generateMetaTag(null, __('site.tuyendung'));

        return $this->returnView('golf', 'recruit.index', [
            'data' => $news,
            'banner' => $banner,
        ]);
    }

    public function detail(string $slug, int $id)
    {

        $data = app(GetNewsBySlugIdAction::class)->run($id, $slug, $this->currentLang,['type' => CategoryType::RECRUIT]);

        if ($data) {

            $this->frontBreadcrumb(__('site.tuyendung'), '#', true);
            $this->updateMetaTag($data);

            return $this->returnView('golf', 'recruit.detail', [
                'data' => $data,
            ]);
        }

        return redirect()->route('web_home_page');
    }

}
