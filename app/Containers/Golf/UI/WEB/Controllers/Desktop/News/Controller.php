<?php

namespace App\Containers\Golf\UI\WEB\Controllers\Desktop\News;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Banner\Actions\GetAvailableBannerByPositionAction;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseFrontEndController;
use App\Containers\Category\Actions\FrontEnd\GetAaliableCategoryByPositionAction;
use App\Containers\Category\Enums\CategoryStatus;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\News\Actions\FrontEnd\GetAaliableNewsCategoryAction;
use App\Containers\News\Actions\FrontEnd\GetNewsBySlugIdAction;
use App\Containers\News\Actions\FrontEnd\GetNewsListAction;
use App\Containers\News\Enums\NewsStatus;
use App\Containers\Product\Actions\Api\GetProductByIdForApiAction;
use App\Ship\core\Foundation\BladeHelper;
use Illuminate\Http\Request;

class Controller extends BaseFrontEndController
{
    public function index(Request $request,string $slug = '',int $category_id = 0)
    {
        $categories = app(GetAaliableCategoryByPositionAction::class)->run(
            null, null, $this->currentLang, ['desc'],['category_id','type','status'],
            ['type' => CategoryType::NEWS, 'status' => BladeHelper::HIEN_THI]
        );


        if ($categories) {

            $category = app(GetAaliableCategoryByPositionAction::class)->run(
                null, 0, $this->currentLang, ['desc'],['category_id','type','status'],
                ['type' => CategoryType::NEWS, 'status' => BladeHelper::HIEN_THI,'category_id' => $category_id]
            );

            $newsLatest = app(GetNewsListAction::class)->run(
                null, 5, $this->currentLang, ['desc'],['id','status','sort_order','created_at','image'],
                ['status' => BladeHelper::HIEN_THI], ['cate_type' => CategoryType::NEWS, 'status' => BladeHelper::HIEN_THI]
                ,1,'',['created_at' => 'desc']
            );

            $category_id  = $category  ? $category->category_id : $categories[0]->category_id ;

            $news = app(GetNewsListAction::class)->run(
                null, 12, $this->currentLang, ['desc'],['id','status','sort_order','created_at','file_name','image'],
                ['status' => BladeHelper::HIEN_THI], ['cate_type' => CategoryType::NEWS, 'status' => BladeHelper::HIEN_THI,'category_id' => $category_id]
                ,0,'',['created_at' => 'desc']
            );


            $this->generateMetaTag(null, __('site.tintuc'));

            return $this->returnView('golf', 'news.index', [
                'categories' => $categories,
                'newsLatest' => $newsLatest,
                'news' => $news,
                'category' => $category,
                'request'  => $request
            ]);
        }

        return redirect()->route('web_home_page');

    }

    public function detail(string $slug, int $id)
    {
        $project = app(GetNewsBySlugIdAction::class)->run($id, $slug, $this->currentLang,['type' => CategoryType::NEWS]);

        if ($project) {

            $this->frontBreadcrumb(__('site.tintuc'), route('web.news.index'), false);
            $this->frontBreadcrumb($project->categories[0]->desc->name,
                route('web.news.category-detail',['slug' => $project->categories[0]->desc->slug,'id' => $project->categories[0]->category_id]), false);
//            $this->frontBreadcrumb($project->desc->name, '#', true);

            $this->updateMetaTag($project);

            return $this->returnView('golf', 'news.detail', [
                'data' => $project,
            ]);
        }

        return redirect()->route('web_home_page');
    }
}
