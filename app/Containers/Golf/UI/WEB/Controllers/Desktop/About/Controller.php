<?php


namespace App\Containers\Golf\UI\WEB\Controllers\Desktop\About;


use App\Containers\Achievement\Actions\GetAllAchievementsAction;
use App\Containers\Banner\Actions\GetAvailableBannerByPositionAction;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseFrontEndController;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\History\Actions\GetAllHistorysAction;
use App\Containers\News\Actions\FrontEnd\GetNewsListAction;
use App\Containers\Partner\Actions\GetAllPartnersAction;
use App\Containers\Speaker\Actions\GetAllSpeakersAction;
use App\Containers\StaticPage\Actions\FrontEnd\GetAvailablePageByPositionAction;
use App\Ship\core\Foundation\BladeHelper;
use Illuminate\Http\Request;

class Controller extends BaseFrontEndController
{
    public function index()
    {

        $payloadRequest = new Request();
        $payloadRequest->merge(['scopeFEAvailableData' => true]);
        $payloadRequest->merge(['skipCache' => false]);

        $banner = app(GetAvailableBannerByPositionAction::class)->run(
            ['banner_about_top'], $this->isMobile, [], ['sort_order' => 'ASC', 'id' => 'DESC'], $this->currentLang, 0
        );

        $achievements = app(GetAllAchievementsAction::class)->run(
            $payloadRequest, null, $this->currentLang, ['desc'], ['id', 'image','status']
        );

        $speaker = app(GetAllSpeakersAction::class)->run(
            $payloadRequest, 0, $this->currentLang, ['desc', 'news'], ['is_hot' => BladeHelper::YES,'status' => BladeHelper::HIEN_THI], 8, ['id', 'image', 'is_hot', 'status','about_image']
        );

        $speakers = app(GetAllSpeakersAction::class)->run(
            $payloadRequest, null, $this->currentLang, ['desc'], [], 8, ['id', 'image', 'is_hot', 'status', 'about_image']
        );

        $partners = app(GetAllPartnersAction::class)->run(
            $payloadRequest,
            null,
            $this->currentLang,
            ['desc'],
            [['type', '=', config('partner-container.type_id.partner-reason')]],
            ['id', 'image']
        );

        $news = app(GetNewsListAction::class)->run(
            null, 8, $this->currentLang, ['desc'], ['id', 'status', 'is_about', 'sort_order', 'image'],
            ['status' => BladeHelper::HIEN_THI, 'is_about' => BladeHelper::YES], ['cate_type' => CategoryType::NEWS, 'status' => BladeHelper::HIEN_THI], 1
        );

        $this->generateMetaTag(null, __('site.gioithieu'));

        return $this->returnView('golf', 'about.index', [
            'banner' => $banner,
            'achievements' => $achievements,
            'speaker' => $speaker,
            'partners' => $partners,
            'news' => $news,
            'speakers' => $speakers,
        ]);

    }

    public function tamNhin()
    {
        $page = app(GetAvailablePageByPositionAction::class)->run(
            ['about_vision'], false, ['desc'], ['sort_order' => 'ASC', 'id' => 'DESC'], ['*'], $this->currentLang, 0
        );

        $this->frontBreadcrumb(__('site.gioithieu'), route('web.about.index'));
        $this->frontBreadcrumb(__('site.tamnhingiatricotloi'), '#', true);
        $this->updateMetaTag($page);

        return $this->returnView('golf', 'about.tam-nhin', [
            'page' => $page
        ]);
    }

    public function history()
    {
        $payloadRequest = new Request();
        $payloadRequest->merge(['scopeFEAvailableData' => true]);
        $payloadRequest->merge(['skipCache' => false]);

        $history = app(GetAllHistorysAction::class)->run(
            $payloadRequest, null, $this->currentLang, ['desc'], ['id', 'image','status']
        );

        $this->frontBreadcrumb(__('site.gioithieu'), route('web.about.index'));
        $this->frontBreadcrumb(__('site.lichsuhinhthanhvaphattrien'), '#', true);
        $this->generateMetaTag(null, __('site.lichsuhinhthanhvaphattrien'));

        return $this->returnView('golf', 'about.lich-su', [
            'history' => $history
        ]);
    }

    public function detailSpeaker(string $slug = '', int $id = 0)
    {
        $speaker = app(GetAllSpeakersAction::class)->run(
            null, 0, $this->currentLang, ['desc','news'], ['id' => $id, 'status' => BladeHelper::HIEN_THI], 8, ['id', 'image', 'status']
        );

        if($speaker) {
            $this->frontBreadcrumb(__('site.gioithieu'), route('web.about.index'));
            $this->frontBreadcrumb(!empty($speaker->desc->name_two) ? $speaker->desc->name_two : $speaker->desc->name, '#', true);
            $this->updateMetaTag($speaker);

            return $this->returnView('golf', 'about.dien-gia', [
                'speaker' => $speaker,
            ]);
        }
        return redirect()->route('web_home_page');
    }
}