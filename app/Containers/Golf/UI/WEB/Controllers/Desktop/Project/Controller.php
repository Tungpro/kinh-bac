<?php

namespace App\Containers\Golf\UI\WEB\Controllers\Desktop\Project;

use App\Containers\Banner\Actions\GetAvailableBannerByPositionAction;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseFrontEndController;
use App\Containers\Category\Actions\FrontEnd\GetAaliableCategoryByPositionAction;
use App\Containers\Category\Enums\CategoryStatus;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\News\Actions\FrontEnd\GetAaliableNewsCategoryAction;
use App\Containers\News\Actions\FrontEnd\GetNewsBySlugIdAction;
use App\Containers\News\Actions\FrontEnd\GetNewsListAction;
use App\Containers\News\Enums\NewsStatus;

use App\Containers\StaticPage\Actions\FrontEnd\GetAvailablePageByPositionAction;
use App\Ship\core\Foundation\BladeHelper;
use Illuminate\Http\Request;

class Controller extends BaseFrontEndController
{
    public function index(Request $request)
    {
        $banner = app(GetAvailableBannerByPositionAction::class)->run(
            ['banner_project_top'], $this->isMobile, [], ['sort_order' => 'ASC', 'id' => 'DESC'], $this->currentLang, 0
        );

        $categories = app(GetAaliableCategoryByPositionAction::class)->run(
            null, null, $this->currentLang, ['desc'],['category_id','type','hot','is_home','image'],
            ['type' => CategoryType::PROJECT, 'status' => BladeHelper::HIEN_THI]
        );

//        $page = app(GetAvailablePageByPositionAction::class)->run(
//            ['project_about'], false, ['desc'], ['sort_order' => 'ASC', 'id' => 'DESC'], ['*'], $this->currentLang, 0
//        );

        $this->generateMetaTag(null, __('site.linhvuchoatdong'));

        return $this->returnView('golf', 'project.index', [
            'banner' => $banner,
            'categories' => $categories,
//            'page' => $page,
        ]);

        return redirect()->route('web_home_page');

    }

    public function categoryDetail(Request $request,String $slug, int $category_id)
    {
        $category = app(GetAaliableCategoryByPositionAction::class)->run(
            null, 0, $this->currentLang, ['desc'],['category_id','type','hot','is_home'],
            ['type' => CategoryType::PROJECT, 'status' => BladeHelper::HIEN_THI, 'category_id' => $category_id]
        );


        if (!empty($category) && $category->type == CategoryType::PROJECT) {
            $news = app(GetNewsListAction::class)->run(
                null, 9, $this->currentLang, ['desc'],['id','status','sort_order','image'],
                ['status' => BladeHelper::HIEN_THI], ['category_id' => $category_id, 'status' => BladeHelper::HIEN_THI],0
            );

            $this->frontBreadcrumb(__('site.linhvuchoatdong'), route('web.project.index'), false);
            $this->frontBreadcrumb(@$category->desc->name, '#', true);

            $this->updateMetaTag($category);

            return $this->returnView('golf', 'project.category', [
                'news' => $news,
                'category' => $category,
            ]);
        }

        return redirect()->route('web_home_page');
    }

    public function categoryDetailAjax(Request $request)
    {
        $request = $request->only('category_id','name') ;
        $news = app(GetNewsListAction::class)->run(
            null, 9, $this->currentLang, ['desc'],['id','status','sort_order','image'],
            ['status' => BladeHelper::HIEN_THI], ['category_id' => $request['category_id'], 'status' => BladeHelper::HIEN_THI],0
        );


        $html_data = \View::make('golf::pc.project.inc.list-news', ['data' => $news])->render();
        $html_loadMoreButton = \View::make('golf::pc.project.inc.load-more', ['data' => $news])->render();
        return \FunctionLib::ajaxRespondV2(true, 'ok', ['html' => $html_data, 'loadmore' => $html_loadMoreButton]);

    }

    public function detail(string $slug, int $id)
    {

        $project = app(GetNewsBySlugIdAction::class)->run($id, $slug, $this->currentLang,['type' => CategoryType::PROJECT, 'medias' => true]);

        if ($project) {

            $this->frontBreadcrumb(__('site.linhvuchoatdong'), route('web.project.index'), false);
            $this->frontBreadcrumb($project->categories[0]->desc->name,
                route('web.project.category-detail',['slug' => $project->categories[0]->desc->slug,'id' => $project->categories[0]->category_id]), false);
            $this->frontBreadcrumb($project->desc->name, '#', true);

            $this->updateMetaTag($project);

            return $this->returnView('golf', 'project.detail', [
                'data' => $project,
            ]);
        }

        return redirect()->route('web_home_page');
    }
}
