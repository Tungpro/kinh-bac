<?php

namespace App\Containers\Golf\UI\WEB\Controllers\Desktop\Contact;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Agency\Actions\SubActions\GetAvailableAgencyByPositionSubAction;
use App\Containers\Agency\Enums\AgencyStatus;
use App\Containers\Banner\Actions\GetAvailableBannerByPositionAction;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseFrontEndController;


class Controller extends BaseFrontEndController
{
    public function index()
    {
        return $this->returnView('golf', "contact.index",[]);
    }
}
