<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-07 10:39:59
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-05 00:50:37
 * @ Description: Happy Coding!
 */

namespace App\Containers\Golf\UI\WEB\Controllers\Features\Home;

use Exception;
use Illuminate\Http\Request;

trait Index
{
    public function index()
    {
        return $this->returnView('golf', 'home.home-page');
    }
}
