<?php

namespace App\Containers\Golf\UI\WEB\Controllers\Features\Post;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseGolfController;

class Controller extends BaseGolfController
{
    private $limitNews = 12;

    public function index()
    {
        $bannerTop = Apiato::call('Banner@GetAvailableBannerByPositionAction', [
            ['tin_tuc_top'], $this->isMobile, [], ['sort_order' => 'ASC', 'id' => 'DESC'], $this->currentLang, 0
        ]);

        $categories = Apiato::call('Category@Golf\GetAaliableCategoryByPositionAction', [
            ['position' => 'page_tin_tuc'],
            false,
            [],
            ['sort_order' => 'asc', 'created_at' => 'desc'],
            ['*'],
            5,
            $this->currentLang,
            ['news']
        ]);

        if ($categories) {
            $this->frontBreadcrumb(__('site.tintuc'), '#', true);
            $this->generateMetaTag(null, __('site.tintuc'));
            return $this->returnView('golf', 'post.index', [
                'bannerTop' => $bannerTop,
                'categories' => $categories,
            ]);
        }

        return redirect()->route('web_home_page');
    }

    public function category(string $slug, int $id)
    {
        $bannerTop = Apiato::call('Banner@GetAvailableBannerByPositionAction', [
            ['tin_tuc_top'], $this->isMobile, [], ['sort_order' => 'ASC', 'id' => 'DESC'], $this->currentLang, 0
        ]);

        $category = Apiato::call('Category@Golf\GetAaliableCategoryByPositionAction', [
            ['position' => 'page_tin_tuc'],
            false,
            [],
            ['sort_order' => 'asc', 'created_at' => 'desc'],
            ['*'],
            5,
            $this->currentLang,
            [],
            ['category_id' => $id],
        ]);
        if ($category) {
            $category = $category[0];

            $news = Apiato::call('News@Golf\GetAaliableNewsCategoryAction', [
                [],
                ['sort_order' => 'asc', 'created_at' => 'desc'],
                ['*'],
                $this->limitNews,
                $this->currentLang,
                [],
                ['category_id' => $id]
            ]);

            $this->frontBreadcrumb(__('site.tintuc'), route('web.post.index'));
            $this->frontBreadcrumb($category->desc->name, $category->linkDCB(), true);
            $this->generateMetaTag($news);

            return $this->returnView('golf', 'post.category', [
                'bannerTop' => $bannerTop,
                'news' => $news,
                'category' => $category,
            ]);
        }

        return redirect()->route('web_home_page');
    }

}
