<?php

/** @var Route $router */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

$router->group([
    'middleware' => [
        'htmloptimized',
        'Maintenance',
        'WebLocaleRedirect'
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run(),
    'domain' => parse_url(config('app.url'))['host'],
    'namespace' => 'Desktop\Shareholder',
], function () use ($router) {
    $router->group(['prefix' => 'quan-he-nha-dau-tu', 'as' => 'web.holder.'], function ($router) {

        $router->get('/', ['as' => 'index', 'uses' => 'Controller@index']);

        $router->get('danh-muc/{slug}-{id}', ['as' => 'category-detail', 'uses' => 'Controller@categoryDetail'])
            ->where([
                'slug' => '[a-zA-Z0-9_\-]+',
                'id' => '[0-9]+'
            ]);

        $router->get('{slug}-{id}', ['as' => 'detail', 'uses' => 'Controller@detail'])
            ->where([
                'slug' => '[a-zA-Z0-9_\-]+',
                'id' => '[0-9]+'
            ]);
    });
});
