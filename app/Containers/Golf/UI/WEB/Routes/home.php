<?php

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

Route::group(
    [
        'middleware' => [
            'htmloptimized',
            'Maintenance',
            'WebLocaleRedirect'
        ],
    ],
    function ($router) {
        $router->get('/', [
            'as'   => 'web_home_page_no_lang',
            'uses' => 'HomeController@index',
        ]);
        $router->group(
            [
                'prefix' => app(CheckSegmentLanguageAction::class)->run(),
            ],
            function ($router) {
                $router->get('/', [
                    'as'   => 'web_home_page',
                    'uses' => 'HomeController@index',
                ]);
            }
        );
    }
);