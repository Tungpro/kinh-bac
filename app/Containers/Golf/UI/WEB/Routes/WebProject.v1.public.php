<?php

/** @var Route $router */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

$router->group([
    'middleware' => [
        'htmloptimized',
        'Maintenance',
        'WebLocaleRedirect'
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run(),
    'domain' => parse_url(config('app.url'))['host'],
    'namespace' => 'Desktop\Project',
], function () use ($router) {
    $router->group(['prefix' => 'linh-vuc-hoat-dong', 'as' => 'web.project.'], function ($router) {

        $router->get('/', ['as' => 'index', 'uses' => 'Controller@index']);

        $router->get('danh-muc/{slug}-{id}', ['as' => 'category-detail', 'uses' => 'Controller@categoryDetail'])
            ->where([
                'slug' => '[a-zA-Z0-9_\-]+',
                'id' => '[0-9]+'
            ]);

        $router->get('danh-muc/ajax', ['as' => 'category-detail-ajax', 'uses' => 'Controller@categoryDetailAjax'])
            ->where([
                'slug' => '[a-zA-Z0-9_\-]+',
                'id' => '[0-9]+'
            ]);

        $router->get('{slug}-{id}', ['as' => 'detail', 'uses' => 'Controller@detail'])
            ->where([
                'slug' => '[a-zA-Z0-9_\-]+',
                'id' => '[0-9]+'
            ]);
    });
});
