<?php

/** @var Route $router */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

$router->group([
    'middleware' => [
        'htmloptimized',
        'Maintenance',
        'WebLocaleRedirect'
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run(),
    'domain' => parse_url(config('app.url'))['host'],
    'namespace' => 'Desktop\Recruit',
], function () use ($router) {
    $router->group(['prefix' => 'tuyen-dung', 'as' => 'web.recruit.'], function ($router) {
        $router->get('/', ['as' => 'web.recruit.index', 'uses' => 'Controller@index']);

        $router->get('{slug}-{id}', ['as' => 'detail', 'uses' => 'Controller@detail'])
            ->where([
                'slug' => '[a-zA-Z0-9_\-]+',
                'id' => '[0-9]+'
            ]);
    });
});

