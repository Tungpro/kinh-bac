<?php

/** @var Route $router */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

$router->group([
    'middleware' => [
        'htmloptimized',
        'Maintenance',
        'WebLocaleRedirect'
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run(),
    'domain' => parse_url(config('app.url'))['host'],
    'namespace' => 'Desktop\Search',
], function () use ($router) {
    $router->get('tim-kiem', ['as' => 'web.search.index', 'uses' => 'Controller@index']);
});
