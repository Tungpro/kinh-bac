<?php

/** @var Route $router */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

$router->group([
    'middleware' => [
        'htmloptimized',
        'Maintenance',
        'WebLocaleRedirect'
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run(),
    'domain' => parse_url(config('app.url'))['host'],
    'namespace' => 'Desktop\News',
], function () use ($router) {

    $router->get('tin-tuc', ['as' => 'web.news.index', 'uses' => 'Controller@index']);

    $router->get('tin-tuc/danh-muc/{slug}-{id}', ['as' => 'web.news.category-detail', 'uses' => 'Controller@index'])
        ->where([
            'slug' => '[a-zA-Z0-9_\-]+',
            'id' => '[0-9]+'
        ]);

    $router->get('tin-tuc/{slug}-{id}', ['as' => 'web.news.detail', 'uses' => 'Controller@detail'])
        ->where([
            'slug' => '[a-zA-Z0-9_\-]+',
            'id' => '[0-9]+'
        ]);
});
