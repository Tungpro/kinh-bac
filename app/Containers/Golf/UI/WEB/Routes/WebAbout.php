<?php

/** @var Route $router */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

$router->group([
    'middleware' => [
        'htmloptimized',
        'Maintenance',
        'WebLocaleRedirect'
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run(),
    'domain' => parse_url(config('app.url'))['host'],
    'namespace' => 'Desktop\About',
], function () use ($router) {
    $router->group(['prefix'=>'gioi-thieu','as'=>'web.about.'], function($router){

       $router->get('/', ['as' => 'index', 'uses' => 'Controller@index']);

       $router->get('tam-nhin-su-menh-va-gia-tri-cot-loi', ['as' => 'web.about.tamnhin', 'uses' => 'Controller@tamNhin']);

       $router->get('lich-su-phat-trien', ['as' => 'web.about.history', 'uses' => 'Controller@history']);

       $router->get('{slug}-{id}', ['as' => 'speaker', 'uses' => 'Controller@detailSpeaker'])
           ->where([
           'slug' => '[a-zA-Z0-9_\-]+',
           'id' => '[0-9]+'
       ]);
   });

});
