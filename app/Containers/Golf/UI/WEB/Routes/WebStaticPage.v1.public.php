<?php

/** @var Route $router */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

$router->group([
    'middleware' => [
        'htmloptimized',
        'Maintenance',
        'WebLocaleRedirect'
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run(),
    'domain' => parse_url(config('app.url'))['host'],
    'namespace' => 'Desktop\StaticPage',
], function () use ($router) {
    $router->get('trang/{slug}-{id}', ['as' => 'web.page.detail', 'uses' => 'Controller@detailPage'])
        ->where([
            'slug' => '[a-zA-Z0-9_\-]+',
            'id' => '[0-9]+'
        ]);
//    $router->fallback('Controller@notFound404')->name('web.not-found');

    //$router->get('not-found', ['as' => 'web.not-found', 'uses'  => 'Controller@notFound404']);
});

