<?php

/** @var Route $router */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

$router->group([
    'middleware' => [
        'htmloptimized',
        'Maintenance',
        'WebLocaleRedirect'
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run(),
    'domain' => parse_url(config('app.url'))['host'],
    'namespace' => 'Desktop\Testimonial',
], function () use ($router) {
    $router->get('thong-tin-hoi-dap-co-dong', ['as' => 'web.testimonial.index', 'uses' => 'Controller@index']);
});

