<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-31 16:10:44
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-22 14:37:21
 * @ Description: Happy Coding!
 */

namespace App\Containers\Golf\UI\WEB\Components\Home;

use App\Containers\Achievement\Actions\GetAllAchievementsAction;
use App\Containers\BaseContainer\UI\WEB\Components\FrontEnd\BaseComponent;
use App\Containers\StaticPage\Actions\FrontEnd\GetAvailablePageByPositionAction;
use Illuminate\Http\Request;

class AchievementComponent extends BaseComponent
{
    public $achievements;
    public $page;

    public function __construct(string $type = '')
    {
        parent::__construct();
    }

    public function render()
    {
        $payloadRequest = new Request();
        $payloadRequest->merge(['scopeFEAvailableData' => true]);

        $this->achievements =  app(GetAllAchievementsAction::class)->run(
            $payloadRequest, null, $this->currentLang, ['desc'],['id', 'image','status']
        );

       $this->page = app(GetAvailablePageByPositionAction::class)->run(
              ['home_achievement'], false,[], ['sort_order' => 'ASC', 'id' => 'DESC'],['*'],$this->currentLang);


      return $this->returnView('golf', 'home.components.achievement-home');
    }
}
