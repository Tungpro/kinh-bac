<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-31 16:10:44
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-22 14:37:21
 * @ Description: Happy Coding!
 */

namespace App\Containers\Golf\UI\WEB\Components\Home;

use App\Containers\BaseContainer\UI\WEB\Components\FrontEnd\BaseComponent;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\News\Actions\FrontEnd\GetNewsListAction;
use App\Ship\core\Foundation\BladeHelper;

class NewsComponent extends BaseComponent
{
    public $news;

    public function __construct()
    {
        parent::__construct();
    }

    public function render()
    {
        $this->news = app(GetNewsListAction::class)->run(
            null, 0, $this->currentLang, ['desc'],['id','status','is_hot','sort_order','image'],
            ['status' => BladeHelper::HIEN_THI], ['cate_type' => CategoryType::NEWS, 'status' => BladeHelper::HIEN_THI],
            0,'',['created_at' => 'DESC']
        );

      return $this->returnView('golf', 'home.components.news-list');
    }
}
