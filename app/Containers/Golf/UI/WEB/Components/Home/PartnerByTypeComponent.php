<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-31 16:10:44
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-22 14:37:21
 * @ Description: Happy Coding!
 */

namespace App\Containers\Golf\UI\WEB\Components\Home;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\BaseContainer\UI\WEB\Components\FrontEnd\BaseComponent;
use App\Containers\Partner\Actions\GetAllPartnersAction;
use Illuminate\Http\Request;

class PartnerByTypeComponent extends BaseComponent
{
    public $partnersByType;
    public $partnerTypeId;
    public $limit;
    public $returnView;
    public $getAllPartnersAction;
    public $banner;
    public $data_banner;

    public function __construct(GetAllPartnersAction $getAllPartnersAction, $partnerTypeId = 0, $limit = false, $returnView = 'partner-default', $banner = false)
    {
        parent::__construct();
        $this->getAllPartnersAction = $getAllPartnersAction;
        $this->partnerTypeId = $partnerTypeId;
        $this->limit = $limit;
        $this->returnView = $returnView;
        $this->banner  = $banner;
    }

    public function render()
    {
        $payloadRequest = new Request();
        $payloadRequest->merge(['scopeFEAvailableData' => true]);
        $payloadRequest->merge(['skipCache' => false]);
        $this->partnersByType = $this->getAllPartnersAction->run(
            $payloadRequest,
            $this->limit,
            $this->currentLang,
            ['desc'],
            [['type', '=', $this->partnerTypeId]],
            ['id', 'image']
        );

        if($this->banner === "true"){
            $this->data_banner =   Apiato::call('Banner@GetAvailableBannerByPositionAction', [
                ['home_partner'],
                $is_mobile = $this->isMobile,
                [],
                ['sort_order' => 'ASC'],
                $this->currentLang
            ]);
        }

        return $this->returnView('golf', "home.components.{$this->returnView}");
    }
}
