<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-31 16:10:44
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-22 14:37:21
 * @ Description: Happy Coding!
 */

namespace App\Containers\Golf\UI\WEB\Components\Home;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Banner\Actions\GetAvailableBannerByPositionAction;
use App\Containers\BaseContainer\UI\WEB\Components\FrontEnd\BaseComponent;
use App\Containers\News\Actions\FrontEnd\GetNewsListAction;
use App\Containers\News\Enums\NewsStatus;

class BannerBigHomeComponent extends BaseComponent
{
    public $bigHomeBanners;
    public $getAvailableBannerByPositionAction;
    public $news;

    public function __construct(GetAvailableBannerByPositionAction $getAvailableBannerByPositionAction)
    {
        parent::__construct();
        $this->getAvailableBannerByPositionAction = $getAvailableBannerByPositionAction;
    }

    public function render()
    {
        $this->bigHomeBanners = $this->getAvailableBannerByPositionAction->run(
            ['big_home'],
            $is_mobile = $this->isMobile,
            ['desc'],
            ['sort_order' => 'ASC'],
            $this->currentLang,
            null
        );


        return $this->returnView('golf','home.components.banner-big-home');
    }
}
