<?php

namespace App\Containers\Golf\UI\WEB\Components\Home;


use App\Containers\BaseContainer\UI\WEB\Components\FrontEnd\BaseComponent;
use App\Containers\Category\Actions\FrontEnd\GetAaliableCategoryByPositionAction;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\StaticPage\Actions\FrontEnd\GetAvailablePageByPositionAction;
use App\Ship\core\Foundation\BladeHelper;


class ProjectCategoryHomeComponent extends BaseComponent
{
    public $categories;
    public $getAaliableCategoryByPositionAction;
    public $getAvailablePageByPositionAction;
    public $news;
    public $page;

    public function __construct(GetAaliableCategoryByPositionAction $getAaliableCategoryByPositionAction,GetAvailablePageByPositionAction $getAvailablePageByPositionAction)
    {
        parent::__construct();
        $this->getAaliableCategoryByPositionAction = $getAaliableCategoryByPositionAction;
        $this->getAvailablePageByPositionAction = $getAvailablePageByPositionAction;
    }

    public function render()
    {
        $this->categories = $this->getAaliableCategoryByPositionAction->run(
            null, 3, $this->currentLang, ['desc'],['category_id','type','hot','is_home','image'],
            ['is_home' => BladeHelper::YES, 'type' => CategoryType::PROJECT, 'status' => BladeHelper::HIEN_THI],
            ['cate_type' => CategoryType::PROJECT, 'status' => BladeHelper::HIEN_THI]
        );

        $this->page = $this->getAvailablePageByPositionAction->run(
            ['home_project'], false,[], ['sort_order' => 'ASC', 'id' => 'DESC'],['*'],$this->currentLang);

        return $this->returnView('golf','home.components.categories-home');
    }
}
