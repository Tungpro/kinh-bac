<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-31 16:10:44
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-22 14:37:21
 * @ Description: Happy Coding!
 */

namespace App\Containers\Golf\UI\WEB\Components\Home;

use App\Containers\BaseContainer\UI\WEB\Components\FrontEnd\BaseComponent;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\News\Actions\FrontEnd\GetNewsListAction;
use App\Containers\StaticPage\Actions\FrontEnd\GetAvailablePageByPositionAction;
use App\Ship\core\Foundation\BladeHelper;

class ProjectNewsComponent extends BaseComponent
{
    public $news;
    public $page;

    public function __construct(string $type = '')
    {
        parent::__construct();
    }

    public function render()
    {
        $this->news = app(GetNewsListAction::class)->run(
               null, 9, $this->currentLang, ['desc'],['id','status','is_hot','sort_order','image'],
            ['status' => BladeHelper::HIEN_THI,'is_hot' => BladeHelper::YES], ['cate_type' => CategoryType::PROJECT, 'status' => BladeHelper::HIEN_THI]
        );


        $this->page = app(GetAvailablePageByPositionAction::class)->run(
              ['home_news_project'], false,[], ['sort_order' => 'ASC', 'id' => 'DESC'],['*'],$this->currentLang);


      return $this->returnView('golf', 'home.components.project-news-home');
    }
}
