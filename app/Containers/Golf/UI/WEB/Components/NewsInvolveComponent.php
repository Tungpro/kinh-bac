<?php


namespace App\Containers\Golf\UI\WEB\Components;


use App\Containers\BaseContainer\UI\WEB\Components\FrontEnd\BaseComponent;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\News\Actions\FrontEnd\GetNewsListAction;
use App\Containers\News\Enums\NewsType;
use App\Ship\core\Foundation\BladeHelper;

class NewsInvolveComponent extends BaseComponent
{
    public $data;
    public $limit;
    public $title;
    public $link;
    public $dataInvolve;
    public $file;
    public $cateType;

    public function __construct(object $data, int $limit = 8, string $title = '', string $link = '', string $file = 'news.data-involve',string $cateType = CategoryType::PROJECT)
    {
        parent::__construct();
        $this->limit = $limit;
        $this->data = $data;
        $this->title = $title;
        $this->link = $link;
        $this->file = $file;
        $this->cateType = $cateType;

    }

    public function render()
    {
        if (!empty($this->data)) {
            $ids_cate = [];
            if ($this->data->categories) {
                foreach ($this->data->categories as $item) {
                    $ids_cate[] = $item->category_id;
                }
            }

            $this->dataInvolve = app(GetNewsListAction::class)->run(
                null, $this->limit, $this->currentLang, ['desc'], ['id', 'status', 'sort_order','image'],
                [['status', '=', BladeHelper::HIEN_THI], ['id', '!=', $this->data->id]], ['category_id' => $ids_cate, 'status' => BladeHelper::HIEN_THI,'cate_type' => $this->cateType], 0
            );

            if($this->link == 'linkProject'){

                if($this->data->layout == NewsType::LAYOUT_PROJECT_IMAGE){
                    $this->title = __('site.cacduankhac');
                }else{
                   $this->title = __('site.cacdichvukhac');
                }
            }
            if($this->link == 'link') {
                if ($this->data->layout == NewsType::LAYOUT_DEFAULT) {
                    $this->title = __('site.tintuckhac');
                }
            }
        }


        return $this->returnView('golf', "components.". $this->file);

    }

}