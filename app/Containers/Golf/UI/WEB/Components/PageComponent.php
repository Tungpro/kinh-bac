<?php


namespace App\Containers\Golf\UI\WEB\Components;


use App\Containers\BaseContainer\UI\WEB\Components\FrontEnd\BaseComponent;
use App\Containers\StaticPage\Actions\FrontEnd\GetAvailablePageByPositionAction;

class PageComponent extends BaseComponent
{
    public $fileName;
    public $position;
    public $getAvailablePageByPositionAction;
    public $page;
    public  $with = [];
    public $limit;

    public function __construct(GetAvailablePageByPositionAction $getAvailablePageByPositionAction,String $fileName = '', String $position = '',$with = 'desc',$limit = 0)
    {
        parent::__construct();
        $this->getAvailablePageByPositionAction = $getAvailablePageByPositionAction;
        $this->fileName = $fileName;
        $this->position = $position;
        $this->with[] = $with;
        $this->limit = $limit;
    }

    public function render(){
        $this->page = $this->getAvailablePageByPositionAction->run(
            [$this->position], false,$this->with, ['sort_order' => 'ASC', 'id' => 'DESC'],['*'],$this->currentLang,$this->limit
        );

        return $this->returnView('golf',"components.static-page.$this->fileName");
    }

}