<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-31 22:43:25
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-07 19:52:44
 * @ Description: Happy Coding!
 */

namespace App\Containers\Golf\Providers\Components;

use App\Containers\Golf\UI\WEB\Components\Home\AchievementComponent;
use App\Containers\Golf\UI\WEB\Components\Home\BannerBigHomeComponent;
use App\Containers\Golf\UI\WEB\Components\Home\NewsComponent;
use App\Containers\Golf\UI\WEB\Components\Home\PartnerByTypeComponent;
use App\Containers\Golf\UI\WEB\Components\Home\ProjectCategoryHomeComponent;
use App\Containers\Golf\UI\WEB\Components\Home\ProjectNewsComponent;
use App\Containers\Golf\UI\WEB\Components\NewsInvolveComponent;
use App\Containers\Golf\UI\WEB\Components\PageComponent;
use Illuminate\Support\Facades\Blade;
use App\Ship\Parents\Providers\MainProvider;

class HomeComponentsProvider extends MainProvider
{
    public function boot() {

        // Desktop
        Blade::component('banner-big-home', BannerBigHomeComponent::class);
        Blade::component('page-file-name', PageComponent::class);
        Blade::component('categories-home',ProjectCategoryHomeComponent::class);
        Blade::component('project-new-home', ProjectNewsComponent::class);
        Blade::component('achievement-home', AchievementComponent::class);
        Blade::component('news-home', NewsComponent::class);
        Blade::component('partner-type', PartnerByTypeComponent::class);

        // news involve
        Blade::component('news-type', NewsInvolveComponent::class);
    }
}
