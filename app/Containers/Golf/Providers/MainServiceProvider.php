<?php

namespace App\Containers\Golf\Providers;

use App\Containers\Golf\Providers\Components\HomeComponentsProvider;
use App\Ship\Parents\Providers\MainProvider;

class MainServiceProvider extends MainProvider
{

    /**
     * Container Service Providers.
     *
     * @var array
     */
    public $serviceProviders = [
        HomeComponentsProvider::class,
    ];

    /**
     * Container Aliases
     *
     * @var  array
     */
    public $aliases = [

    ];

    /**
     * Register anything in the container.
     */
    public function register()
    {
        parent::register();
    }

    public function boot() {
        parent::boot();
    }
}
