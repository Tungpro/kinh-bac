<?php

namespace App\Containers\Achievement\Tasks;

use App\Containers\Achievement\Data\Repositories\AchievementRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteAchievementTask extends Task
{
    protected $repository;

    public function __construct(AchievementRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            $examCategory = $this->repository->find($id);

            return $examCategory->delete();
        } catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
