<?php

namespace App\Containers\Achievement\Tasks;

use App\Containers\Achievement\Data\Repositories\AchievementRepository;
use App\Ship\core\Foundation\BladeHelper;
use App\Ship\Parents\Tasks\Task;
use Exception;

class EnableAchievementTask extends Task
{
    protected $repository;

    public function __construct(AchievementRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->getModel()->where('id', $id)->update(['status' => BladeHelper::HIEN_THI]);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
