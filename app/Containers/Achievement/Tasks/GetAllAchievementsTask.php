<?php

namespace App\Containers\Achievement\Tasks;

use App\Containers\BaseContainer\Traits\ScopeAdminBaseSearchTrait;
use App\Containers\BaseContainer\Traits\ScopeFEAvailableDataTrait;
use App\Containers\Localization\Models\Language;
use App\Containers\Achievement\Data\Repositories\AchievementRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllAchievementsTask extends Task
{
    use ScopeAdminBaseSearchTrait;
    use ScopeFEAvailableDataTrait;

    protected $repository;

    public function __construct(AchievementRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Language $currentLang = null,array $filters = [], $limit = 20,int $offset = 0,string $exceptID = '')
    {
        $language_id = $currentLang ? $currentLang->language_id : 1;
        return $this->with([
            'desc' => function ($query) use ($language_id) {
                $query->where('language_id', $language_id);
            },
        ])->returnDataByLimit($limit,$offset,$exceptID);

    }

}
