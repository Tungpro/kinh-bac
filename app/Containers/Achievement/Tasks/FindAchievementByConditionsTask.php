<?php

namespace App\Containers\Achievement\Tasks;

use App\Containers\Achievement\Data\Repositories\AchievementRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindAchievementByConditionsTask extends Task
{
    protected $repository;

    public function __construct(AchievementRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        try {
            return $this->repository->first();
        } catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
