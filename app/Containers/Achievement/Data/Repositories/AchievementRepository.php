<?php

namespace App\Containers\Achievement\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class AchievementRepository
 */
class AchievementRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
