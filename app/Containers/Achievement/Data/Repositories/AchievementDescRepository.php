<?php

namespace App\Containers\Achievement\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class AchievementDescRepository
 */
class AchievementDescRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
