<?php

namespace App\Containers\Achievement\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Achievement\UI\WEB\Requests\CreateAchievementRequest;
use App\Containers\Achievement\UI\WEB\Requests\DeleteAchievementRequest;
use App\Containers\Achievement\UI\WEB\Requests\GetAllAchievementsRequest;
use App\Containers\Achievement\UI\WEB\Requests\UpdateAchievementRequest;
use App\Containers\Achievement\UI\WEB\Requests\StoreAchievementRequest;
use App\Containers\Achievement\UI\WEB\Requests\EditAchievementRequest;
use App\Containers\Achievement\UI\WEB\Requests\UpdateStatusAchievementRequest;
use App\Ship\Parents\Controllers\AdminController;
use Apiato\Core\Foundation\Facades\Apiato;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class Controller extends AdminController
{
    protected $containerName = 'achievement';

    public function __construct()
    {
        $this->title = 'thành tích';
        $this->imageField = 'image';
        $this->imageKey = 'achievement';
        $this->actions = [
            'list' => 'Achievement@Admin\GetAllAchievementsAction',
            'create' => 'Achievement@CreateAchievementAction',
            'edit' => 'Achievement@FindAchievementByIdAction',
            'update' => 'Achievement@UpdateAchievementAction',
            'delete' => 'Achievement@DeleteAchievementAction',
        ];
        $this->routes = [
            'list' => 'admin.achievement.index',
            'create' => 'admin.achievement.create',
            'store' => 'admin.achievement.store',
            'edit' => 'admin.achievement.edit',
            'update' => 'admin.achievement.update',
            'destroy' => 'admin.achievement.destroy',
            'disable' => "admin.$this->containerName.disable",
            'enable' => "admin.$this->containerName.enable",
        ];
        View::share('routes', $this->routes);
        View::share('containerName', $this->containerName);

        parent::__construct();
    }

    public function index(GetAllAchievementsRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);

        $request->merge(['scopeAdminBaseSearch' => true]);
        $data = Apiato::call($this->actions['list'], [$request, $this->perPage, null, ['desc']]);

        return view("$this->containerName::Admin.index", [
            'request' => $request,
            'data' => $data,
        ]);
    }

    public function create(CreateAchievementRequest $request)
    {
        $this->showAddForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['add', $this->title, $this->routes['list']]);

        return view("$this->containerName::Admin.form", []);
    }

    public function store(StoreAchievementRequest $request)
    {
        $this->editMode = false;
        return $this->save($request);
    }

    public function edit($id, EditAchievementRequest $request)
    {
        $this->showEditForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['edit', $this->title, $this->routes['list']]);

        $editedObj = Apiato::call($this->actions['edit'], [$id, ['all_desc']]);

        if ($editedObj) {

            return view("$this->containerName::Admin.form", [
                'data' => $editedObj,
            ]);
        }

        return redirect()->route($this->routes['list']);
    }

    public function update(UpdateAchievementRequest $request)
    {
        $this->editMode = true;
        return $this->save($request);
    }

    public function destroy($id, DeleteAchievementRequest $request)
    {
        try {

            Apiato::call($this->actions['delete'], [$id]);
            return FunctionLib::ajaxRespondV2(true, 'Xóa bản ghi thành công!');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình xóa dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function enable(UpdateStatusAchievementRequest $request)
    {
        try {
            Apiato::call('Achievement@EnableAchievementAction', [$request]);

            return FunctionLib::ajaxRespondV2(true, 'Hiện bản ghi thành công!');
        } catch (Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình sửa dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function disable(UpdateStatusAchievementRequest $request)
    {
        try {
            Apiato::call('Achievement@DisableAchievementAction', [$request]);

            return FunctionLib::ajaxRespondV2(true, 'Ẩn bản ghi thành công!');
        } catch (Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình sửa dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }
}
