<?php

/** @var Route $router */
$router->group([
    'middleware' => ['auth:admin'],
    'namespace' => '\App\Containers\Achievement\UI\WEB\Controllers\Admin',
    'domain' => sprintf('admin.%s', parse_url(config('app.url'))['host'])
], function () use ($router) {
    $router->resource('achievement', 'Controller')
        ->names([
            'index' => 'admin.achievement.index',
            'create' => 'admin.achievement.create',
            'store' => 'admin.achievement.store',
            'edit' => 'admin.achievement.edit',
            'update' => 'admin.achievement.update',
            'destroy' => 'admin.achievement.destroy',
        ])
        ->except(['show']);

    $router->post('/achievement/enable/{id}', ['as' => 'admin.achievement.enable', 'uses' => 'Controller@enable',]);
    $router->post('/achievement/disable/{id}', ['as' => 'admin.achievement.disable', 'uses' => 'Controller@disable',]);
});
