<?php

namespace App\Containers\Achievement\UI\WEB\Requests;

use App\Containers\Achievement\Models\Achievement;
use App\Ship\Parents\Requests\Request;

class UpdateStatusAchievementRequest extends Request
{
    protected $access = [
        'permissions' => 'achievement-edit',
        'roles' => 'admin',
    ];

    protected $decode = [];

    protected $urlParameters = [
        'id',
    ];

    public function rules()
    {
        return [
            'id' => ['required', sprintf('exists:%s,id', Achievement::TABLE_NAME)],

        ];
    }

    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
