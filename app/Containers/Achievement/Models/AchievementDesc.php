<?php

namespace App\Containers\Achievement\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class AchievementDesc extends Model
{
    const TABLE_NAME = 'achievement_desc';
    protected $table = self::TABLE_NAME;

    protected $fillable = [
        'achievement_id', 'language_id', 'name', 'description','short_description'
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'achievement_desc';

    public function language()
    {
        return $this->hasOne(Language::class, 'language_id', 'language_id');
    }

}
