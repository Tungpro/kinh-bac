<?php

namespace App\Containers\Achievement\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Ship\core\Traits\HelpersTraits\LangTrait;
use App\Ship\Parents\Models\Model;

class Achievement extends Model
{
    use LangTrait;

    const TABLE_NAME = 'achievement';
    protected $table = self::TABLE_NAME;

    protected $fillable = [
        'status', 'sort_order', 'birth_year', 'email', 'image', 'created_by', 'updated_by',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $resourceKey = 'achievement';

    /*relationship*/
    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', AchievementDesc::class, 'achievement_id', 'id');
    }

    public function desc()
    {
        return $this->hasOne(AchievementDesc::class, 'achievement_id', 'id');
    }

    /*function */
    public function getImageUrl($size = 'small')
    {
        return ImageURL::getImageUrl($this->image, 'achievement', $size);
    }

    public function getPositionArr()
    {
        if (!empty($this->desc->description)) {
            return explode('|', $this->desc->description);
        }
        return [];
    }
}
