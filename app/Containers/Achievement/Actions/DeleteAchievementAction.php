<?php

namespace App\Containers\Achievement\Actions;

use App\Containers\Achievement\Data\Repositories\AchievementDescRepository;
use App\Containers\Achievement\Models\Achievement;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteAchievementAction extends Action
{
    public function run($id)
    {
        $deleted = Apiato::call('Achievement@DeleteAchievementTask', [$id]);

        if ($deleted) {
            Apiato::call('BaseContainer@Admin\DeleteModelDescTask', [app(AchievementDescRepository::class), $id, 'Achievement_id']);

            Apiato::call('User@CreateUserLogSubAction', [
                $id,
                [],
                [],
                'Xóa Thành tích giải thưởng',
                Achievement::class
            ]);

            $this->clearCache();

            return $deleted;
        }

        return false;
    }
}
