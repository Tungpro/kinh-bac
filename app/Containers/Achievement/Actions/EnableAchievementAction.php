<?php

namespace App\Containers\Achievement\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Achievement\Models\Achievement;
use App\Ship\Parents\Actions\Action;

class EnableAchievementAction extends Action
{
    public function run($data)
    {
        $object = Apiato::call('Achievement@EnableAchievementTask', [$data->id]);

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            [],
            [],
            'Hiển thị Thành tích giải thưởng',
            Achievement::class
        ]);

        $this->clearCache();

        return $object;
    }
}
