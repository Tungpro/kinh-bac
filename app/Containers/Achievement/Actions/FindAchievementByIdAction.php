<?php

namespace App\Containers\Achievement\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class FindAchievementByIdAction extends Action
{
    public function run($id, array $withData = [], array $conditions = [], $selectFields = ['*'])
    {
        return Apiato::call('Achievement@FindAchievementByIdTask', [$id], [
            ['with' => [$withData]],
            ['where' => [$conditions]],
            ['selectFields' => [$selectFields]]
        ]);
    }
}
