<?php

namespace App\Containers\Achievement\Actions;

use App\Containers\Achievement\Data\Repositories\AchievementDescRepository;
use App\Containers\Achievement\Models\Achievement;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;

class UpdateAchievementAction extends Action
{
    public function run($request)
    {
        $data = Arr::only($request, [
            'id', 'description', 'status', 'sort_order', 'birth_year', 'email', 'image',
        ]);
        $data['updated_by'] = auth()->id();

        $objBefore = Apiato::call('Achievement@FindAchievementByIdTask', [$data['id']], [['with' => [['all_desc']]]]);
        $objUpdated = Apiato::call('Achievement@UpdateAchievementTask', [$data['id'], Arr::except($data, ['description'])]);

        if ($objUpdated) {
            $originalDesc = Apiato::call('BaseContainer@Admin\GetAllModelDescTask', [app(AchievementDescRepository::class), $objUpdated->id, 'Achievement_id']);
            Apiato::call('BaseContainer@Admin\SaveModelDescTask', [app(AchievementDescRepository::class), $data, $originalDesc, $objUpdated->id, 'Achievement_id']);

            // Create Log
            Apiato::call('User@CreateUserLogSubAction', [
                $objUpdated->id,
                $objBefore->toArray(),
                $objUpdated->load('all_desc')->toArray(),
                'Sửa thành tích',
                Achievement::class
            ]);
        }

        $this->clearCache();

        return $objUpdated;
    }
}
