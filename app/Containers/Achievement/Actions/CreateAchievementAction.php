<?php

namespace App\Containers\Achievement\Actions;

use App\Containers\Achievement\Data\Repositories\AchievementDescRepository;
use App\Containers\Achievement\Models\Achievement;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;

class CreateAchievementAction extends Action
{
    public function run($request)
    {
        $data = Arr::only($request, [
            'description', 'status', 'sort_order', 'birth_year', 'email', 'image',
        ]);
        $data['created_by'] = auth()->id();

        $objCreated = Apiato::call('Achievement@CreateAchievementTask', [Arr::except($data, ['description'])]);

        if ($objCreated) {
            // Create Desc
            Apiato::call('BaseContainer@Admin\SaveModelDescTask', [app(AchievementDescRepository::class), $data, [], $objCreated->id, 'Achievement_id']);

            // Create Log
            Apiato::call('User@CreateUserLogSubAction', [
                $objCreated->id,
                [],
                $objCreated->load('all_desc')->toArray(),
                'Tạo thành tích',
                Achievement::class
            ]);
        }

        $this->clearCache();

        return $objCreated;
    }
}
