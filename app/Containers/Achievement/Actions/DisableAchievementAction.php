<?php

namespace App\Containers\Achievement\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Achievement\Models\Achievement;
use App\Ship\Parents\Actions\Action;

class DisableAchievementAction extends Action
{
    public function run($data)
    {
        $object = Apiato::call('Achievement@DisableAchievementTask', [$data->id]);

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            [],
            [],
            'Ẩn Thành tích giải thưởng',
            Achievement::class
        ]);

        $this->clearCache();

        return $object;
    }
}
