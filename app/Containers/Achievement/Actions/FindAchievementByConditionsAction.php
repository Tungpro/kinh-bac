<?php

namespace App\Containers\Achievement\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class FindAchievementByConditionsAction extends Action
{
    public function run(array $conditions = [], array $withData = [], $selectFields = ['*'])
    {
        return Apiato::call('Achievement@FindAchievementByConditionsTask', [], [
            ['where' => [$conditions]],
            ['with' => [$withData]],
            ['selectFields' => [$selectFields]]
        ]);
    }
}
