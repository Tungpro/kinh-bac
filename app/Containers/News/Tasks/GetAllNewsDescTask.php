<?php

namespace App\Containers\News\Tasks;

use App\Containers\News\Data\Repositories\NewsDescRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

/**
 * Class GetAllNewsDescTask.
 */
class GetAllNewsDescTask extends Task
{

    protected $repository;

    public function __construct(NewsDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($news_id)
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('news_id', $news_id));
        $wery = $this->repository->all()->keyBy('language_id')->toArray();

        return $wery;
    }
}
