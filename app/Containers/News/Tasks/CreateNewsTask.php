<?php

namespace App\Containers\News\Tasks;

use App\Containers\News\Data\Repositories\NewsRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class CreateNewsTask.
 */
class CreateNewsTask extends Task
{

    protected $repository;

    public function __construct(NewsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {
        try {
            $dataUpdate = Arr::except($data, ['news_description', '_token', '_headers']);
            $dataUpdate['author'] = auth()->guard('admin')->user()->name;

            $news = $this->repository->create($dataUpdate);

            return $news;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
