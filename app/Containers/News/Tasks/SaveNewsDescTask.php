<?php

namespace App\Containers\News\Tasks;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\StringLib;
use Apiato\Core\Foundation\ImageURL;
use App\Containers\BaseContainer\Traits\BaseLanguage;
use App\Containers\BaseContainer\Traits\UploadImageTrait;
use App\Containers\File\Actions\UploadFileAction;
use App\Containers\News\Data\Repositories\NewsDescRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Parents\Requests\Request;

/**
 * Class SaveNewsDescTask.
 */
class SaveNewsDescTask extends Task
{
    use BaseLanguage;
    protected $repository;

    public function __construct(NewsDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data, $original_desc, $news_id, Request $request)
    {

        $news_description = isset($data['news_description']) ? (array)$data['news_description'] : null;

        if (is_array($news_description) && !empty($news_description)) {
            $updates = [];
            $inserts = [];
            foreach ($news_description as $k => $v) {
                $attrItem = [];
                if (!empty($v['item'])) {
                    $count = count(array_filter($v['item']['item_title']));

                    for ($i = 0; $i < $count; $i++) {
                        $attrItem[] = [
                            'item_title' => $v['item']['item_title'][$i],
                            'item_description' => $v['item']['item_description'][$i],
                        ];
                    }
                }
                $tmp['item'] = json_encode($attrItem);

                if (!empty($v['file'])) {
                    $request->request->add(['file_' . $k => $v['file']]);
                    $file = $this->uploadFileByField($request, $v, 'file_' . $k, $k);
                }


                if (isset($original_desc[$k])) {
                    $updates[$original_desc[$k]['id']] = [
                        'name' => $v['name'],
                        'slug' => $this->slug($v['slug'], $v['name'], $k),
                        'short_description' => $v['short_description'],
                        'description' => $v['description'],
                        'meta_title' => $v['meta_title'],
                        'meta_description' => $v['meta_description'],
                        'meta_keyword' => $v['meta_keyword'],
                        'item' => $tmp['item'],
                        'file' => empty($v['file']) ? $original_desc[$k]['file'] : $file ,
                    ];
                } else {
                    $inserts[] = [
                        'news_id' => $news_id,
                        'language_id' => $k,
                        'name' => $v['name'],
                        'slug' => $this->slug($v['slug'], $v['name'], $k),
                        'short_description' => $v['short_description'],
                        'description' => $v['description'],
                        'meta_title' => $v['meta_title'],
                        'meta_description' => $v['meta_description'],
                        'meta_keyword' => $v['meta_keyword'],
                        'item' => $tmp['item'],
                        'file' => !empty($v['file']) ? $file : null ,
                    ];
                }
            }


            if (!empty($updates)) {
                $this->repository->updateMultiple($updates);
            }

            if (!empty($inserts)) {
                $this->repository->getModel()->insert($inserts);
            }
        }
    }

    public function to_slug($string, $separator = '-')
    {
        $re = "/(\\s|\\" . $separator . ")+/mu";
        $str = @trim($string);
        $subst = $separator;
        $result = preg_replace($re, $subst, $str);

        return $result;
    }

    public function uploadFileByField($request, &$transporter, $fileField, $k)
    {
        if($transporter['file'] === '1'){
            return null;
        }

        $file = $this->uploadFile($request, $fileField, $k, 'news');

        if (!empty($file) && isset($file['error']) && $file['error']) {
            return redirect()->back()->withInput()->withErrors(['error:' => $file['msg']]);
        }
        return $file['fileName'];
    }

    public function uploadFile($request, $fileField, $title, $folder_upload)
    {
        $errorMsg = null;
        $fname = null;

        $file = $request->$fileField;

        $allowedFileExtension = ['doc', 'docx', 'pdf','zip','rar'];
        $extension = $file->getClientOriginalExtension();

        if (in_array($extension, $allowedFileExtension) && $file->isValid()) {
            $fname = ImageURL::makeFileName(!empty($file->getClientOriginalName()) ? $file->getClientOriginalName().$title : $title, $extension);

            $storeResult = $file->storeAs($folder_upload, $fname, 'upload');

            if (!$storeResult) {
                $errorMsg = 'Upload file lên server thất bại!';
            }
        } else {
            $errorMsg = 'Upload file lên server sai định dạng!';
        }


        return ['error' => !empty($errorMsg), 'msg' => @$errorMsg ?: 'Success', 'fileName' => $fname];
    }
}
