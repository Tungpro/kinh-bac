<?php

namespace App\Containers\News\Tasks\FrontEnd;

use App\Containers\News\Data\Repositories\NewsRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class SaveNewsTask.
 */
class SaveNewsTask extends Task
{

    protected $repository;

    public function __construct(NewsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data)
    {

        try {
            $dataUpdate['views'] = $data->views + 1;

            $news = $this->repository->update($dataUpdate, $data->id);

            return $news;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
