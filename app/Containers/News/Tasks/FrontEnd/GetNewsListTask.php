<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 01:33:06
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-05 15:52:43
 * @ Description: Happy Coding!
 */

namespace App\Containers\News\Tasks\FrontEnd;

use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Containers\BaseContainer\Traits\ScopeAdminBaseSearchTrait;
use App\Containers\BaseContainer\Traits\ScopeFEAvailableDataTrait;
use App\Containers\Localization\Models\Language;
use App\Containers\News\Data\Repositories\NewsRepository;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class GetNewsListTask extends Task
{
    use ScopeAdminBaseSearchTrait;
    use ScopeFEAvailableDataTrait;

    protected $repository;

    public function __construct(NewsRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;

    }

    /**
     *
     * @return  mixed
     */

    public function run(Language $currentLang = null, array $filters = [],$limit = 0, int $offset = 0,string $exceptID = '')
    {
        $language_id = $currentLang->language_id ? $currentLang->language_id : 1;

        if (!empty($filters['cate_type']) || !empty($filters['category_id'])) {
            $this->repository->whereHas('categories', function (Builder $q) use ($filters) {
                if(!empty($filters['cate_type'])){
                    $q->where('category.type', $filters['cate_type']);
                }
                if(!empty($filters['category_id'])){
                    if(is_array($filters['category_id'])){
                        $q->whereIn('category.category_id',$filters['category_id']);
                    }else{
                        $q->where('category.category_id',$filters['category_id']);
                    }
                }
                if(!empty($filters['status'])){
                    $q->where('status',$filters['status']);
                }
            });
        }


        if (isset($filters['name']) && !empty($filters['name'])) {
            $this->repository->whereHas('desc', function (Builder $query) use ($filters) {
                $query->where('name', 'like', '%' . $filters['name'] . '%');
            });
        }

        if (!empty($filters['created_at'])) {
                $createdAtFormat = (Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['created_at']))->format('Y-m-d'));
                $this->repository->whereDate('created_at', '=', $createdAtFormat);
        }

        return  $this->with([
            'desc' => function ($query) use ($language_id,$filters) {
                $query->where('language_id', $language_id);
                if(!empty($filters['slug'])){
                    $query->where('slug', $filters['slug']);
                }
            },
        ])->returnDataByLimit($limit,$offset,$exceptID);
    }
}
