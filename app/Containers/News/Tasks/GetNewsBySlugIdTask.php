<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-05 12:37:37
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-14 16:45:25
 * @ Description: Happy Coding!
 */

namespace App\Containers\News\Tasks;

use App\Containers\Localization\Models\Language;
use App\Containers\News\Data\Repositories\NewsRepository;
use App\Containers\News\Models\News;
use App\Ship\core\Foundation\BladeHelper;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

class GetNewsBySlugIdTask extends Task
{

    protected $repository;

    public function __construct(NewsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run(int $newsId, string $slug, Language $currentLang = null,array $filters = []): ?News
    {
        $language_id = $currentLang ? $currentLang->language_id : 1;

         $this->repository->with([
            'desc' => function ($query) use ($language_id) {
                $query->activeLang($language_id);
            },
        ]);

        if(!empty($filters['type'])) {$this->repository->with([
                'categories' => function ($query) use ($filters) {
                    $query->where('type', $filters['type']);
                },
                'categories.desc' => function ($query) use ($language_id) {
                    $query->activeLang($language_id);
                }
            ]);
        }

        if(!empty($filters['medias'])) {
           $this->repository->with(['medias']);
        }

        $this->repository->whereHas('desc',function (Builder $query) use ($slug,$language_id) {
                   $query->where('slug', $slug);
            }
        );

        if(!empty($filters['type'])) {
            $this->repository->whereHas('categories', function (Builder $query) use ($filters) {
                $query->where('type', $filters['type']);
            });
        }

        // $this->repository->pushCriteria(new ThisEqualThatCriteria('path_id',$category_id));

        return $this->repository->where('id', $newsId)->where('status', BladeHelper::HIEN_THI)->first();
    }
}
