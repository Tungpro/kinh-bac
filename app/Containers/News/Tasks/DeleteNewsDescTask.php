<?php

namespace App\Containers\News\Tasks;

use App\Containers\News\Data\Repositories\NewsDescRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class DeleteNewsDescTask.
 */
class DeleteNewsDescTask extends Task
{

    protected $repository;

    public function __construct(NewsDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($news_id)
    {
        try {
            $this->repository->getModel()->where('news_id', $news_id)->delete();
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
