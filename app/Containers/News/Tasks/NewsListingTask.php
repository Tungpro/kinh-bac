<?php

namespace App\Containers\News\Tasks;

use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Containers\News\Data\Repositories\NewsRepository;
use App\Containers\News\Enums\NewsStatus;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class NewsListingTask.
 */
class NewsListingTask extends Task
{

    protected $repository;

    public function __construct(NewsRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($filters = [], $orderBy = ['created_at' => 'desc', 'id' => 'desc'], $limit = 20, $defaultLanguage = null, $external_data = [], $current_page = 1)
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;

        if (isset($filters['id']) && $filters['id'] != '') {
            $this->repository->pushCriteria(new ThisEqualThatCriteria('id', $filters['id']));
        } else {
            if (isset($filters['status']) && $filters['status'] != '') {
                $this->repository->pushCriteria(new ThisEqualThatCriteria('status', $filters['status']));
            } else {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('status', NewsStatus::NOT_DELETED, '>'));
            }

            if (isset($filters['time_from']) && !empty($filters['time_from']) && $filters['time_from'] != '') {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('created_at', Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['time_from'])), '>='));
            }
            if (isset($filters['time_to']) && !empty($filters['time_to']) && $filters['time_to'] != '') {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('created_at', Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['time_to'],true)), '<='));
            }

            if (isset($filters['publish_from']) && !empty($filters['publish_from']) && $filters['publish_from'] != '') {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('published', Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['publish_from'])), '>='));
            }
            if (isset($filters['publish_to']) && !empty($filters['publish_to']) && $filters['publish_to'] != '') {
                $this->repository->pushCriteria(new ThisOperationThatCriteria('published', Carbon::createFromTimestamp(FunctionLib::getTimestampFromVNDate($filters['publish_to'],true)), '<='));
            }
            if (isset($filters['type']) && $filters['type'] != '') {
                $this->repository->pushCriteria(new ThisEqualThatCriteria('type', $filters['type']));
            }
        }

        $this->repository->with(['desc' => function ($query) use ($language_id) {
            $query->select('id', 'news_id', 'language_id', 'name', 'short_description','slug');
            $query->activeLang($language_id);
        }, 'categories', 'categories.desc' => function ($query) use ($language_id) {
            $query->select('category_id', 'name');
            $query->activeLang($language_id);
        }]);
        if (!empty($filters['cat_id']) || !empty($filters['cate_type'])) {
            $this->repository->whereHas('categories', function (Builder $q) use ($filters) {
                if (!empty($filters['cat_id'])) {
                    $q->where('category.category_id', $filters['cat_id']);
                }
                if (!empty($filters['cate_type'])) {
                    $q->where('category.type', $filters['cate_type']);
                }
            });
        }


        if (isset($filters['name']) && !empty($filters['name'])) {
            $this->repository->whereHas('desc', function (Builder $query) use ($filters) {
                $query->where('name', 'like', '%' . $filters['name'] . '%');
            });
        }

        foreach ($orderBy as $column => $direction) {
            $this->repository->orderBy($column, $direction);
        }

        // \DB::enableQueryLog();

        return $this->repository->paginate($limit);
        // dd(\DB::getQueryLog());
    }
}
