<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 01:35:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-14 16:44:26
 * @ Description: Happy Coding!
 */

namespace App\Containers\News\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\News\Models\News;
use App\Containers\News\Tasks\GetNewsBySlugIdTask;
use App\Ship\Parents\Actions\Action;

class GetNewsBySlugIdAction extends Action
{
    protected $getNewsBySlugIdTask;
    public function __construct(GetNewsBySlugIdTask $getNewsBySlugIdTask)
    {
        parent::__construct();
        $this->getNewsBySlugIdTask = $getNewsBySlugIdTask;
    }
    public function run(int $newsId, string $slug, Language $currentLang = null,array $filters = []): ?News
    {
        return $this->remember(function () use ($newsId, $slug, $currentLang, $filters) {
            return $this->getNewsBySlugIdTask->run($newsId, $slug, $currentLang, $filters);
        });
    }
}
