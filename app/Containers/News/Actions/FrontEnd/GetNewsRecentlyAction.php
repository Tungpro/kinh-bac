<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 01:35:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-05 15:51:55
 * @ Description: Happy Coding!
 */

namespace App\Containers\News\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetNewsRecentlyAction extends Action
{
    public function run(
        $type,
        int $limit = 20,
        bool $isPanination = false,
        Language $currentLang = null,
        array $orderBy = ['created_at' => 'desc', 'id' => 'desc']

    ): iterable {

        return $this->remember(function () use ($type, $limit, $isPanination, $currentLang,$orderBy) {
            return $this->call('News@FrontEnd\GetNewsRecentlyTask', [
                $type,
                $limit,
                $isPanination,
                $currentLang,
                $orderBy

            ]);
        }, null, [5]);
    }
}
