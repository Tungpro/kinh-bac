<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-01 01:35:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-05 15:51:55
 * @ Description: Happy Coding!
 */

namespace App\Containers\News\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Containers\News\Tasks\FrontEnd\GetNewsListTask;
use App\Ship\Parents\Actions\Action;

class GetNewsListAction extends Action
{
    public function run($request, $limit = 20,
                        ?Language $currentLang = null, array $with = [],array $selectFields = ['*'],
                        array $conditions = [], array $filters = [], int $offset = 0, string $exceptID = '', array $orderBy = ['sort_order' => 'ASC', 'id' => 'DESC']
    )
    {
        return $this->remember(function () use ($request, $limit, $currentLang, $with, $selectFields, $conditions,$filters, $exceptID, $offset, $orderBy) {
            return app(GetNewsListTask::class)->currentLang($currentLang)->with($with)->where($conditions)
                ->selectFields($selectFields)->orderBy($orderBy)->scopeAdminBaseSearch($request)->scopeFEAvailableData($request)->run(
                    $currentLang, $filters, $limit ,$offset, $exceptID
                );
        }, null, [], 0, $request->skipCache ?? true);

    }
}
