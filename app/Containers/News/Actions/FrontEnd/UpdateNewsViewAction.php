<?php

namespace App\Containers\News\Actions\FrontEnd;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class UpdateNewsAction.
 *
 */
class UpdateNewsViewAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        $news = Apiato::call(
            'News@FrontEnd\SaveNewsTask',
            [
                $data
            ]
        );
        $this->clearCache();
        return $news;
    }
}
