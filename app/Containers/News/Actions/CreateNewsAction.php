<?php

namespace App\Containers\News\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Media\Task\CreateMediaArrayTask;
use App\Containers\News\Enums\NewsType;
use App\Containers\News\Models\News;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

/**
 * Class CreateNewsAction.
 *
 */
class CreateNewsAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data, Request $request)
    {
        $news = Apiato::call(
            'News@CreateNewsTask',
            [
                $data
            ]
        );

        if ($news) {
            Apiato::call('News@SaveNewsDescTask', [
                $data,
                [],
                $news->id,
                $request
            ]);
            Apiato::call('News@SaveNewsCategoriesTask', [$data, $news]);
            // update medias
            app(CreateMediaArrayTask::class)->run($news, $data['images_old'] ?? [], $data['images'] ?? [], NewsType::MEDIA_IMAGE, News::getTableName());

            Apiato::call('User@CreateUserLogSubAction', [
                $news->id,
                [],
                $news->toArray(),
                'Tạo bài viết',
                StaticPage::class
            ]);
        }

        $this->clearCache();

        return $news;
    }
}
