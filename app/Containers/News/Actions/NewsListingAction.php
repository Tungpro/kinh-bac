<?php

namespace App\Containers\News\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class NewsListingAction.
 *
 */
class NewsListingAction extends Action
{

    /**
     * @return mixed
     */
    public function run(array $filters,$orderBy = ['created_at' => 'desc','id' => 'desc'], $limit = 10,$language_id, $external_data = [],$current_page)
    {
        return $this->remember(function () use($filters, $orderBy, $limit, $language_id, $external_data,$current_page)  {
            if (isset($filters['cat_id']) && !empty($filters['cat_id'])) {
                $arr_cate_ids = [];
                Apiato::call('Category@GetAllCateChildsTask', [[$filters['cat_id']], &$arr_cate_ids]);
                $filters['cate_ids'] = $arr_cate_ids;
            }
            $searchConds = [];
            if (isset($filters['id']) && $filters['id'] != '') {
                $searchConds['id'] = $filters['id'];
            } else {
                if (isset($filters['status']) && $filters['status'] != '') {
                    $searchConds['status'] = $filters['status'];
                }
                if (isset($filters['time_from']) && !empty($filters['time_from']) && $filters['time_from'] != '') {
                    $searchConds['time_from'] = $filters['time_from'];
                }
                if (isset($filters['time_to']) && !empty($filters['time_to']) && $filters['time_to'] != '') {
                    $searchConds['time_to'] = $filters['time_to'];
                }

                if (isset($filters['publish_from']) && !empty($filters['publish_from']) && $filters['publish_from'] != '') {
                    $searchConds['publish_from'] = $filters['publish_from'];
                }
                if (isset($filters['publish_to']) && !empty($filters['publish_to']) && $filters['publish_to'] != '') {
                    $searchConds['publish_to'] = $filters['publish_to'];
                }
                if (isset($filters['type']) && !empty($filters['type'])) {
                    $searchConds['type'] = $filters['type'];
                }

            }
            if (isset($filters['cat_id']) && !empty($filters['cat_id'])) {
                $searchConds['cat_id'] = $filters['cat_id'];
            }
            if (isset($filters['cate_type']) && !empty($filters['cate_type'])) {
                $searchConds['cate_type'] = $filters['cate_type'];
            }
            if (isset($filters['name']) && !empty($filters['name'])) {
                $searchConds['name'] = $filters['name'];
            }

            $data = Apiato::call(
                'News@NewsListingTask',
                [
                    $searchConds,
                    $orderBy,
                    $limit,
                    Apiato::call('Localization@GetDefaultLanguageTask'),
                    $external_data,
                    $filters['page'] ?? 1
                ]
            );

            return $data;

        },null,[4]);
    }
}
