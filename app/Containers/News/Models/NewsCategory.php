<?php
/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2020-10-01 16:05:40
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-02-11 17:28:55
 * @ Description:
 */

namespace App\Containers\News\Models;

use App\Ship\Parents\Models\Model;

class NewsCategory extends Model {
    protected $table = 'news_category';
    protected $primaryKey = ['news_id', 'category_id'];
}