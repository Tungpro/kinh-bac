<?php

namespace App\Containers\News\Models;

use Apiato\Core\Foundation\Facades\ImageURL;
use App\Containers\Category\Enums\CategoryStatus;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\Category\Models\Category;
use App\Containers\Media\Models\Media;
use App\Ship\Parents\Models\Model;
use Illuminate\Support\Str;

class News extends Model
{
    use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;
    protected $table = 'news';

    protected $appends = ['format_image', 'format_link'];

    protected $fillable = [
        'status',
        'image',
        'sort_order',
        'author',
        'author_updated',
        'type',
        'created_at',
        'is_about',
        'is_home',
        'is_hot',
        'is_utilities',
        'file_name',
        'layout',
        'youtube',
    ];

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'cat_id');
    }

    public function getImageUrl($size = 'original')
    {
        return ImageURL::getImageUrl($this->image, 'news', $size);
    }

    public function medias(){
        return $this->hasMany(Media::class,'object_id','id')->where('model',$this->table)->orderBy('sort_order','ASC');
    }

    public function desc()
    {
        return $this->hasOne(NewsDesc::class, 'news_id', 'id');
    }

    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', NewsDesc::class, 'news_id', 'id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, NewsCategory::getTableName(), 'news_id', 'category_id')->where('category.status', '!=', CategoryStatus::DELETE);
    }

    public function activeCategories()
    {
        return $this->belongsToMany(Category::class, NewsCategory::getTableName(), 'news_id', 'category_id')->where('category.status', '!=', CategoryStatus::DELETE);
    }

    public function getFormatDateAttribute()
    {
        return ($this->created_at) ? $this->created_at->format('d/m/Y') : null;
    }

    public function getFormatImageAttribute()
    {
        return ImageURL::getImageUrl($this->image, 'news', 'medium3');
    }


    public function getFormatLinkAttribute()
    {
        $slug = !empty($this->desc->slug) ? $this->desc->slug : Str::slug($this->desc->name);
        if (!empty($slug)) {
            return route('web.news.detail', ['slug' => $slug, 'id' => $this->id]);
        } else {
            return false;
        }
    }

    public function linkType()
    {
        if (isset($this->categories) && $this->categories->isNotEmpty()) {
            if($this->categories[0]->type == CategoryType::PROJECT){
                return $this->linkProject();
            }elseif ($this->categories[0]->type == CategoryType::NEWS){
                return $this->link();
            }elseif ($this->categories[0]->type == CategoryType::RECRUIT){
                return $this->linkRecruit();
            }elseif ($this->categories[0]->type == CategoryType::HOLDER){
               return $this->linkHolder();
            }
        }
        return $this->link();
    }

    public function link()
    {
        if (!empty($this->desc->name)) {
            return route('web.news.detail', ['slug' => !empty($this->desc->slug) ? $this->desc->slug : Str::slug($this->desc->name), 'id' => $this->id]);
        }
        return false;
    }

    public function linkProject()
    {
        if (!empty($this->desc->name)) {
            return route('web.project.detail', ['slug' => !empty($this->desc->slug) ? $this->desc->slug : Str::slug($this->desc->name), 'id' => $this->id]);
        }
        return false;
    }

    public function linkHolder()
    {
        if (!empty($this->desc->name)) {
            return route('web.holder.detail', ['slug' => !empty($this->desc->slug) ? $this->desc->slug : Str::slug($this->desc->name), 'id' => $this->id]);
        }
        return false;
    }

    public function linkRecruit()
    {
        if (!empty($this->desc->name)) {
            return route('web.recruit.detail', ['slug' => !empty($this->desc->slug) ? $this->desc->slug : Str::slug($this->desc->name), 'id' => $this->id]);
        }
        return false;
    }


    public function getPdf(){

        if(!empty($this->file_name)){
            return redirect('/upload/news/'.$this->file_name) ;
        }
       return false ;
    }

}

