<?php

namespace App\Containers\News\UI\WEB\Requests;

use App\Containers\BaseContainer\Traits\RequestBaseLanguage;
use App\Ship\Parents\Requests\Request;

/**
 * Class UpdateNewsRequest.
 *
 */
class UpdateNewsRequest extends Request
{
    use RequestBaseLanguage;
    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'roles'       => 'admin',
        'permissions' => 'edit-news',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'id'           => 'required|exists:news,id',
//            'news_description.*.name' => 'required|max:255',
            'image' => 'bail|nullable|mimes:jpeg,jpg,png,gif',
            'news_category' => 'required',
            'file_name' => 'bail|mimes:doc,docx,ppt,pptx,txt,pdf,zip,rar',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return  $this->messagesLang([
//            'news_description.*.name.required' => 'Tên language không được bỏ trống',
//            'news_description.*.name.max' => 'Tên language tối đa 255 ký tự',
        ])  + [
            'id.required'  => 'ID không tồn tại',
            'id.exists'  => 'ID không tồn tại',
            'news_category.required' => 'Danh mục không được bỏ trống',
            'image.mimes' => 'Ảnh đại diện không đúng định dạng (jpeg, jpg, png, gif)',
            'file_name.mimes' => 'File không đúng định dạng (doc,docx,ppt,pptx,txt,pdf,zip,rar)',
        ];
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
