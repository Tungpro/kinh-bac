<tr>

  <td>
    <div class="input-group p-1">
      @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
      <input type="text" class="form-control" placeholder="Tiêu đề" name="news_description[{{$it_lang['language_id']}}][item][item_title][]" id="name_{{$it_lang['language_id']}}" value="{{@$item['item_title']}}">
    </div>
  </td>

  <td>
    <div class="input-group p-1">
      @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
        <input type="text" class="form-control" placeholder="Mô tả" name="news_description[{{$it_lang['language_id']}}][item][item_description][]" id="name_{{$it_lang['language_id']}}" value="{{@$item['item_description']}}">
    </div>
  </td>

  <td class="px-2 text-center" style="width: 80px">
    <a href="javascript:void(0)" class="badge badge-danger" onclick="deleteATab(this)">
      Xóa
    </a>
  </td>
</tr>