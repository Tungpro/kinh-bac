<div class="tab-pane" id="links">
    <div class="tabbable">

        @foreach(\App\Containers\Category\Enums\CategoryType::TYPE as $key => $value)
        <div class="row form-group align-items-center mt-2">
            <label class="col-sm-2 control-label text-right mb-0" for="input-category">
                <span data-toggle="tooltip" title="Bộ lọc">{{$value}}</span></label>
            <div class="col-sm-6">
                <input type="text" placeholder="Nhập để tìm kiếm danh mục" id="input-category-{{$key}}" class="form-control"/>
            </div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-10">
                <div id="news_category_{{$key}}" class="row mx-0 list-group flex-row">
                    @if(isset($data))
                        @foreach($data->activeCategories as $item)
                            @if(@$item->category_id && @$item->type == $key)
                                <a href="#" id="category-{{$item->category_id}}"
                                   class="col-3 list-group-item list-group-item-action">
                                    <i class="fa fa-close"></i> {!! @$item->desc->name  !!}
                                    <input type="hidden" name="news_category[]" value="{{$item->category_id}}"/>
                                </a>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        @endforeach


    </div>
</div>

@push('js_bot_all')
    <script>

        @foreach(\App\Containers\Category\Enums\CategoryType::TYPE as $key => $value)
            $('input#input-category-{{$key}}').autocomplete({
                classes: {
                    "ui-autocomplete": "dropdown-menu"
                },
                'source': function (request, response) {
                    $.ajax({
                        global: false,
                        url: "{{route('api_category_get_all')}}",
                        dataType: 'json',
                        headers: ENV.headerParams,
                        data: {type: 'select2', cate_type: '{{$key}}', name: request.term, _token: ENV.token},
                        success: function (json) {
                            response($.map(json.data, function (item) {
                                return {
                                    label: item.text,
                                    value: item.text,
                                    id: item.id
                                }
                            }));
                        }
                    });
                },
                'select': function (event, ui) {
                    var item = ui.item;

                    $('#category-' + item.id).remove();

                    var html = '<a href="#" id="category-' + item.id + '" class="col-3 list-group-item list-group-item-action">\n' +
                        '                        <i class="fa fa-close"></i> ' + item.label + '\n' +
                        '                        <input type="hidden" name="news_category[]" value="' + item.id + '" />\n' +
                        '                    </a>';

                    $('#news_category_{{$key}}').append(html);

                    $('input#input-category-{{$key}}').val('');
                    return false;
                }
            });

            $('#news_category_{{$key}}').delegate('.fa-close', 'click', function () {
                $(this).parent().remove();
            });
        @endforeach
    </script>
@endpush
