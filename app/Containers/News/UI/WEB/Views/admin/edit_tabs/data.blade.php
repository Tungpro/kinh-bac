<div class="tab-pane" id="data">
    <div class="tabbable">
        {{-- <div class="row form-group align-items-center">
            <label class="col-sm-3 control-label text-right mb-0" for="published">Ngày hiệu lực <span class="d-block small text-danger">(Ngày mà bài viết chính thức được đăng)</span></label>
            <div class="col-sm-6">
                <input type="text" name="published" readonly value="{{ old('published',@$data['published'] ? Carbon\Carbon::parse($data['published'])->format('d/m/Y H:i') : '') }}" placeholder="dd/mm/YY H:i"
                       id="published" class="form-control">
            </div>
        </div> --}}

{{--        <div class="row form-group align-items-center">--}}
{{--            <label class="col-sm-3 control-label text-right mb-0" for="status">Thông báo <span--}}
{{--                    class="d-block small text-danger">(Có hay không)</span></label>--}}
{{--            <div class="col-sm-3">--}}
{{--                <select class="form-control{{ $errors->has('is_hot') ? ' is-invalid' : '' }}" name="is_hot" id="is_hot">--}}
{{--                    <option value="0" {{@$data['is_hot'] == 0 ? 'selected' : ''}}>Không</option>--}}
{{--                    <option value="1" {{@$data['is_hot'] == 1 ? 'selected' : ''}}>Có</option>--}}
{{--                </select>--}}
{{--            </div>--}}
{{--        </div>--}}

        <div class="row form-group align-items-center">
            <label class="col-sm-3 control-label text-right mb-0" for="is_hot">Kiểu hiển thị<span
                    class="d-block small text-danger">(Có hay không)</span></label>
            <div class="col-sm-3">
                <select class="form-control{{ $errors->has('layout') ? ' is-invalid' : '' }}" name="layout" id="layout">
                        @foreach(\App\Containers\News\Enums\NewsType::LAYOUT as $statusKey => $statusText)
                            <option value="{{$statusKey}}" {{ old('layout', @$data->layout) == $statusKey ? 'selected' : '' }}>{{$statusText}}</option>
                        @endforeach
                </select>
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-3 control-label text-right mb-0" for="is_hot">Nổi bật(Dự án)<span
                    class="d-block small text-danger">(Có hay không)</span></label>
            <div class="col-sm-3">
                <select class="form-control{{ $errors->has('is_hot') ? ' is-invalid' : '' }}" name="is_hot" id="is_hot">
                    @if(!empty(\App\Ship\core\Foundation\BladeHelper::STATUS_YES_NO))
                        @foreach(\App\Ship\core\Foundation\BladeHelper::STATUS_YES_NO as $statusKey => $statusText)
                            <option value="{{$statusKey}}" {{ old('is_hot', @$data->is_hot) == $statusKey ? 'selected' : '' }}>{{$statusText}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-3 control-label text-right mb-0" for="is_about">Công ty thành viên(Tin tức)<span
                    class="d-block small text-danger">(Có hay không)</span></label>
            <div class="col-sm-3">
                <select class="form-control{{ $errors->has('is_about') ? ' is-invalid' : '' }}" name="is_about" id="is_about">
                    @if(!empty(\App\Ship\core\Foundation\BladeHelper::STATUS_YES_NO))
                        @foreach(\App\Ship\core\Foundation\BladeHelper::STATUS_YES_NO as $statusKey => $statusText)
                            <option value="{{$statusKey}}" {{ old('is_about', @$data->is_about) == $statusKey ? 'selected' : '' }}>{{$statusText}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-3 control-label text-right mb-0" for="status">Trạng thái <span
                    class="d-block small text-danger">(Hiển thị hay không)</span></label>
            <div class="col-sm-3">
                <select class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status" id="status">
                    @if(!empty(\App\Ship\core\Foundation\BladeHelper::STATUS_TEXT))
                        @foreach(\App\Ship\core\Foundation\BladeHelper::STATUS_TEXT as $statusKey => $statusText)
                            <option value="{{$statusKey}}" {{ old('status', @$data->status) == $statusKey ? 'selected' : '' }}>{{$statusText}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        {{--<div class="row form-group align-items-center">
            <label class="col-sm-3 control-label text-right mb-0" for="type">Loại tin <span
                    class="d-block small text-danger"></span></label>
            <div class="col-sm-6">
                <select class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type" id="type">
                    @foreach($typeOptions as $key=>$type)
                        <option value="{{ $key }}" @if($key == (@$data['type'] ?? null)) selected @endif>
                            {{ $type }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>--}}

        <div class="row form-group align-items-center">
            <label class="col-sm-3 control-label text-right mb-0" for="sort_order">Sắp xếp <span
                    class="d-block small text-danger">(Vị trí, càng nhỏ thì càng lên đầu)</span></label>
            <div class="col-sm-3">
                <input type="text" name="sort_order"
                       value="{{ old('sort_order', \Apiato\Core\Foundation\FunctionLib::numberFormat(@$data['sort_order']))}}"
                       placeholder="Vị trí sắp xếp"
                       id="sort_order" class="form-control"
                       onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()">
            </div>
        </div>

        <div class="row form-group align-items-center">
            <label class="col-sm-3 control-label text-right mb-0" for="youtube">Link youtube(Lĩnh vực hoạt động)</label>
            <div class="col-sm-3">
                <textarea rows="5" class="form-control" name="youtube" id="youtube" aria-hidden="true">
                    {{ old('youtube', @$data['youtube'])}}
                    </textarea>

            </div>
        </div>
    </div>
</div>

@push('js_bot_all')
    <script>
        $('#published').datetimepicker({format: 'd/m/Y H:i',});
    </script>
@endpush
