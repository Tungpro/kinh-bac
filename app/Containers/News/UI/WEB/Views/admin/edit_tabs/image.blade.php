<div class="tab-pane" id="image">
    <div class="tabbable">

        @include("basecontainer::admin.inc.edit_tabs.image-single", ['imageSingleColClass' => 'col-lg-6', 'imageSingleTittle' => 'Ảnh đại diện', 'imageSingleKey' => 'image' ])

        @include("basecontainer::admin.inc.edit_tabs.image-multi",['imageMultiTittle' => 'Hình ảnh slide' ])

        @include("basecontainer::admin.inc.edit_tabs.file-single", ['fileSingleColClass' => 'col-lg-6', 'fileSingleTittle' => 'File', 'fileSingleKey' => 'file_name', 'desc' => false])

    </div>
</div>