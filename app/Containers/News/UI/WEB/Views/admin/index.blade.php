@extends('basecontainer::admin.layouts.default')

@section('right-breads')
    {!! $data->appends($search_data->toArray())->links('basecontainer::admin.inc.paginator') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-5">
                <h1>{{ $site_title }}</h1>
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif

            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            {!! Form::open(['url' => route('admin_news_home_page'), 'method' => 'get', 'id' => 'searchForm']) !!}
            <div class="card card-accent-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-bookmark-o"></i></span></div>
                                <input type="text" name="name" class="form-control" placeholder="Tiêu đề"
                                       value="{{ $search_data->name }}">
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-list"></i></span></div>
                                <select id="cate_type" name="cate_type" class="form-control">
                                    <option value="">-- Kiểu danh mục --</option>
                                    @foreach(\App\Containers\Category\Enums\CategoryType::TYPE as $key => $item)
                                        <option value="{{$key}}"
                                            {{ $search_data->cate_type == $key ? ' selected="selected"' : '' }}>
                                            {{$item}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-navicon"></i></span></div>
                                <select id="cat_id" name="cat_id" class="form-control">
                                    <option value="">-- Danh mục --</option>
                                    @foreach(\App\Containers\Category\Enums\CategoryType::TYPE as $key => $item)
                                        <optgroup label="{{$item}}">
                                            @foreach ($categories as $category)
                                                @if($category->type == $key)
                                                    <option
                                                        {{ $search_data->cat_id == $category->category_id ? 'selected' : '' }}
                                                        value="{{ $category->category_id }}">{!! $category->name !!}</option>
                                                @endif
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-calendar"></i></span></div>
                                <input type="text" name="time_from" class="datepicker form-control"
                                       placeholder="Ngày tạo từ" autocomplete="off"
                                       value="{{ $search_data->time_from }}">
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-calendar"></i></span></div>
                                <input type="text" name="time_to" class="datepicker form-control"
                                       placeholder="Ngày tạo đến"
                                       autocomplete="off" value="{{ $search_data->time_to }}">
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-list"></i></span></div>
                                <select id="status" name="status" class="form-control">
                                    <option value="">-- Chọn trạng thái --</option>
                                    <option value="1" {{ $search_data->status == 1 ? ' selected="selected"' : '' }}>Đang
                                        hiển thị
                                    </option>
                                    <option value="2" {{ $search_data->status == 2 ? ' selected="selected"' : '' }}>Đang
                                        ẩn
                                    </option>
                                    <option value="-1" {{ $search_data->status == -1 ? ' selected="selected"' : '' }}>Đã
                                        xóa
                                    </option>
                                </select>
                            </div>
                        </div>

                        {{--                    <div class="form-group col-sm-3">--}}
                        {{--                        <div class="input-group">--}}
                        {{--                            <div class="input-group-prepend"><span class=" input-group-text"><i--}}
                        {{--                                        class="fa fa-navicon"></i></span></div>--}}
                        {{--                            <select id="type" name="type" class="form-control">--}}
                        {{--                                <option value="">--Loại tin --</option>--}}
                        {{--                                @foreach($typeOptions as $key=>$type)--}}
                        {{--                                <option value="{{ $key }}" @if($key==($search_data->type ?? null)) selected @endif>--}}
                        {{--                                    {{ $type }}--}}
                        {{--                                </option>--}}
                        {{--                                @endforeach--}}
                        {{--                            </select>--}}
                        {{--                        </div>--}}
                        {{--                    </div>--}}
                        {{-- <div class="form-group col-sm-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" name="publish_from" class="datepicker form-control" placeholder="Ngày xuất bản từ" autocomplete="off" value="{{ $search_data->publish_from }}">
                    </div>
                </div>
                <div class="form-group col-sm-3">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" name="publish_to" class="datepicker form-control" placeholder="Ngày xuất bản đến"
                            autocomplete="off" value="{{ $search_data->publish_to }}">
                    </div>
                </div> --}}
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                    <a href="{{ route('admin_news_home_page') }}" class="btn btn-sm btn-primary"><i
                            class="fa fa-refresh"></i>
                        Reset</a>
                    @if($user->can('create-news'))
                        <a href="{{ route('admin_news_add_page') }}" class="btn btn-sm btn-primary"><i
                                class="fa fa-plus"></i> Thêm
                            mới</a>
                    @endif
                </div>
            </div>
            {!! Form::close() !!}

            <div class="card card-accent-primary">
                <div class="card-body p-0">
                    <table class="table table-bordered table-striped ">
                        <thead>
                        <tr>
                            <th width="55">ID</th>
                            <th>Tiêu đề</th>
                            <th width="85">Sort</th>
                            <th width="125">Ảnh</th>
                            {{-- <th width="100">Ngày xuất</th> --}}
                            <th width="100">Ngày tạo</th>
                            @if($search_data->status != \App\Containers\News\Enums\NewsStatus::DELETE)
                                @if($user->can('edit-news'))
                                    {{--                                <th width="100" class="text-center">Hiện trang chủ</th>--}}
                                    <th width="55" class="text-center">Nổi bật</th>
                                @endif
                                <th width="55">Lệnh</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $item)

                            <tr>
                                <td align="center">{{ $item->id }}</td>
                                <td>
                                    <b>{{ @$item->desc->name }}</b>
                                    @if(isset($item->categories) && $item->categories->isNotEmpty())
                                        @foreach(\App\Containers\Category\Enums\CategoryType::TYPE as $type => $value)
                                            @php($cate_type = false)
                                            @foreach($item->categories as $cate)
                                                @if($cate->type == $type)
                                                    @php($cate_type = true)
                                                @endif
                                            @endforeach
                                            @if($cate_type)
                                                <div class="mt-2 font-sm">
                                                    {{$value}} :
                                                    <div>
                                                        @foreach($item->categories as $cate)
                                                            @if($cate->type == $type)
                                                                <b>{{$cate->desc->name}}</b><br>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach

                                    @endif
                                </td>
                                <td>{{ @$item->sort_order }}</td>
                                <td align="center">
                                    @if ($item->image != '')
                                        <img src="{{ $item->getImageUrl('small') }}" width="100"/>
                                    @endif
                                </td>
                                {{-- <td align="center">
                                                        {!! $item->published ? Carbon\Carbon::parse($item->published)->format("d/m/Y H:i:s") : '---' !!}
                                                    </td> --}}
                                <td align="center">{!! $item->created_at ? Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s') : '---'
        !!} </td>
                                @if($item->status != \App\Containers\News\Enums\NewsStatus::DELETE)
                                    @if($user->can('edit-news'))
                                        {{--                                    <td align="center">--}}
                                        {{--                                        <div class="mb-2">--}}
                                        {{--                                            <a href="javascript:void(0)"--}}
                                        {{--                                               class="{{ $item->is_home ? 'text-danger' : 'text-secondary' }}"--}}
                                        {{--                                               data-route="{{ route('admin_news_change_status', ['field' => 'is_home']) }}"--}}
                                        {{--                                               onclick="admin.updateStatus(this,{{ $item->id }}, {{ $item->is_home ? 0 : 1 }})"--}}
                                        {{--                                               data-toggle="tooltip"--}}
                                        {{--                                               data-placement="top"--}}
                                        {{--                                               data-original-title="{{ $item->is_home ? 'Đang đánh dấu hiện trang chủ, bấm để tắt' : 'Đang bình thường, Click để đánh dấu Hiện trang chủ' }}">--}}
                                        {{--                                                <i class="fa fa-home"></i>--}}
                                        {{--                                            </a>--}}
                                        {{--                                        </div>--}}
                                        {{--                                    </td>--}}
                                        <td align="center">
                                            <div class="mb-2">
                                                <a href="javascript:void(0)"
                                                   class="{{ $item->is_hot ? 'text-danger' : 'text-secondary' }}"
                                                   data-route="{{ route('admin_news_change_status', ['field' => 'is_hot']) }}"
                                                   onclick="admin.updateStatus(this,{{ $item->id }}, {{ $item->is_hot ? 0 : 1 }})"
                                                   data-toggle="tooltip"
                                                   data-placement="top"
                                                   data-original-title="{{ $item->is_hot ? 'Đang đánh dấu Nổi bật, bấm để tắt' : 'Đang bình thường, Click để đánh dấu Nổi bật' }}">
                                                    <i class="fa fa-fire"></i>
                                                </a>
                                            </div>
                                        </td>
                                    @endif

                                    <td align="center">
                                        @if($user->can('edit-news'))
                                            @if ($item->status == 1)
                                                <a href="javascript:void(0)"
                                                   data-route="{{ route('admin_news_change_status', ['field' => 'status']) }}"
                                                   class="text-primary"
                                                   onclick="admin.updateStatus(this,{{ $item->id }},2)"
                                                   title="Đang hiển thị, Click để ẩn"><i class="fa fa-eye"></i></a>
                                            @else
                                                <a href="javascript:void(0)"
                                                   data-route="{{ route('admin_news_change_status', ['field' => 'status']) }}"
                                                   class="text-danger"
                                                   onclick="admin.updateStatus(this,{{ $item->id }}, 1)"
                                                   title="Đang ẩn, Click để hiển thị"><i class="fa fa-eye"></i></a>
                                            @endif
                                            <a href="{{ route('admin_news_edit_page', $item->id) }}"
                                               class="btn text-primary"><i
                                                    class="fa fa-pencil"></i></a>
                                        @endif

                                        @if($user->can('delete-news'))
                                            @if ($item->status != -1)
                                                <a data-href="{{ route('admin_news_delete', $item->id) }}"
                                                   class="btn text-danger"
                                                   onclick="admin.delete_this(this);"><i class="fa fa-trash"></i></a>
                                            @endif
                                        @endif
                                        @if(!empty($item->linkType()))
                                        <a href="{{ $item->linkType() }}" target="_blank" class="text-info"
                                           data-toggle="tooltip" data-placement="top"
                                           data-original-title="Xem bài này"><i class="fa fa-chain"></i></a>
                                        @endif
                                    </td>

                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    @include('basecontainer::admin.inc.bottom-pager-infor')
                </div>
            </div>
        </div>
    </div>
@stop

@push('js_bot_all')
    <script>
        $('.datepicker').datetimepicker({
            format: 'd/m/Y',
            timepicker: false
        });
    </script>
@endpush