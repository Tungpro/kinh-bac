<?php
Route::group(
    [
        'prefix' => 'news',
        'namespace' => '\App\Containers\News\UI\WEB\Controllers\Admin',
        'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
        'middleware' => [
            'auth:admin',
        ],
    ],
    function () use ($router) {
        $router->get('/', [
            'as'   => 'admin_news_home_page',
            'uses' => 'Controller@index',
        ]);

        $router->get('/edit/{id}', [
            'as'   => 'admin_news_edit_page',
            'uses' => 'Controller@edit',
        ]);

        $router->post('/edit/{id}', [
            'as'   => 'admin_news_edit_page',
            'uses' => 'Controller@update',
        ]);

        $router->get('/add', [
            'as'   => 'admin_news_add_page',
            'uses' => 'Controller@add',
        ]);

        $router->post('/add', [
            'as'   => 'admin_news_add_page',
            'uses' => 'Controller@create',
        ]);

        $router->delete('/{id}', [
            'as' => 'admin_news_delete',
            'uses'       => 'Controller@delete',
        ]);

        $router->post('/status/{field}', [
            'as'   => 'admin_news_change_status',
            'uses' => 'Controller@updateSomeStatus',
        ]);
//        $router->post('/edit/{id}', [
//            'as'   => 'web.new.detail',
//            'uses' => 'Controller@edit',
//        ]);
    }
);
