<?php
Route::group(
    [
        'prefix' => 'news',
        'namespace' => '\App\Containers\News\UI\WEB\Controllers\Admin',
        'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
        'middleware' => [
            'auth:admin',
        ],
    ],
    function () use ($router) {
        $router->get('/api/v1/news/get-news', [
            'as'   => 'admin_api_get_news',
            'uses' => 'Controller@getNewsAll',
        ]);
    }
);
