<?php

namespace App\Containers\News\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\StringLib;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\Category\UI\API\Transformers\CategoriesSelect2Transformer;
use App\Containers\Media\Models\Media;
use App\Containers\News\Enums\NewsStatus;
use App\Containers\News\Models\News;
use App\Containers\News\UI\WEB\Controllers\Admin\Features\UpdateSomeStatus;
use App\Containers\News\UI\WEB\Requests\Admin\FindNewsRequest;
use App\Containers\News\UI\WEB\Requests\CreateNewsRequest;
use App\Containers\News\UI\WEB\Requests\EditNewsRequest;
use App\Containers\News\UI\WEB\Requests\StoreNewsRequest;
use App\Containers\News\UI\WEB\Requests\UpdateNewsRequest;
use App\Containers\News\UI\WEB\Requests\GetAllNewsRequest;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Http\Request;
use App\Containers\News\Enums\NewsType;

class Controller extends AdminController
{
    use UpdateSomeStatus;

    public function __construct()
    {
        $this->title = 'Bài viết';
        $this->imageKey = 'news';
        parent::__construct();
    }

    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllNewsRequest $request)
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Bài viết', $this->form == 'list' ? '' : route('admin_news_home_page'));
        \View::share('breadcrumb', $this->breadcrumb);

        $news = Apiato::call('News@NewsListingAction', [$request->all(), ['created_at' => 'desc'], $this->perPage, Apiato::call('Localization@GetDefaultLanguageAction'), [], $request->page ?? 1]);
        return view('news::admin.index', [
            'search_data' => $request,
            'data' => $news,
            'typeOptions' => NewsType::TEXT,
            'categories' => Apiato::call('Category@Admin\GetAllCategoriesAction', [['cate_type' => array_keys(CategoryType::TYPE)], 0, true]),
        ]);
    }

    public function edit(Request $request)
    {
        $this->showEditForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Bài viết', $this->form == 'list' ? '' : route('admin_news_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Chỉnh sửa thông tin');
        \View::share('breadcrumb', $this->breadcrumb);

        $news = Apiato::call('News@Admin\FindNewsByIdAction', [$request->id, ['activeCategories', 'activeCategories.desc', 'medias']]);

        if ($news) {
            $news->mediaImages = $news->medias->where('type', NewsType::MEDIA_IMAGE);
            return view('news::admin.edit', [
                'data' => $news,
                'typeOptions' => NewsType::TEXT
            ]);
        }
        return redirect()->route('admin_news_home_page');
    }

    public function add(Request $request)
    {
        $this->showAddForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Bài viết', $this->form == 'list' ? '' : route('admin_news_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Thêm mới');
        \View::share('breadcrumb', $this->breadcrumb);
        return view('news::admin.edit');
    }

    public function update(UpdateNewsRequest $request)
    {

        try {

            $tranporter = $request->all();
            $this->beforeSave($request, $tranporter);

            $news = Apiato::call('News@UpdateNewsAction', [$tranporter, $request]);

            if ($news) {
                return redirect()->route('admin_news_edit_page', ['id' => $news->id])->with('status', 'Bài viết đã được cập nhật');
            }
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function create(StoreNewsRequest $request)
    {
        try {
            $tranporter = $request->all();
            $this->beforeSave($request, $tranporter);

            $this->beforeSave($request, $tranporter);

            $news = Apiato::call('News@CreateNewsAction', [$tranporter, $request]);

            if ($news) {
                return redirect()->route('admin_news_home_page')->with('status', 'Bài viết đã được thêm mới');
            }
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function delete(FindNewsRequest $request)
    {
        try {
            Apiato::call('News@DeleteNewsAction', [$request]);
        } catch (\Exception $e) {
            // throw $e;
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! ']);
        }
    }

    public function getNewsAll(GetAllNewsRequest $request)
    {
//        $data = [
//            'name' => $request->name,
//            'type' => $request->type,
//        ];
        $news = Apiato::call('News@NewsListingAction', [['name' =>  $request->name,'cate_type' => CategoryType::NEWS], ['created_at' => 'desc'], $this->perPage, Apiato::call('Localization@GetDefaultLanguageAction'), [], 15]);

        return $this->sendResponse($news);
    }


    public function beforeSave($request, &$transporter)
    {

        parent::beforeSave($request, $transporter);

        $this->uploadMultipleImage($transporter, $request, 'images', 'images', StringLib::getClassNameFromString(Media::class), false);
    }

}
