<?php

/**
 * @apiGroup           Category
 * @apiName            getCategories
 * @api                {get} /v1/category/getCategories Get All Catagories
 *
 * @apiVersion         1.0.0
 * @apiPermission      Authenticated User
 *
 * @apiUse             GeneralSuccessMultipleResponse
 */

$router->post('news/updateNews', [
    'as' => 'api_news_edit',
    'uses'       => 'Controller@updateNews',
    'middleware' => [
        'auth:api',
    ],
]);
