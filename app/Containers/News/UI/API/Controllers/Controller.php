<?php

namespace App\Containers\News\UI\API\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\FunctionLib;
use App\Containers\News\UI\API\Requests\UpdateNewsRequest;
use App\Ship\Parents\Controllers\ApiController;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Http\Response;

/**
 * Class Controller.
 */
class Controller extends ApiController
{
    public function updateNews(UpdateNewsRequest $request) {
        $tranporter = new DataTransporter($request);
        $news = Apiato::call('News@UpdateNewsAction', [$tranporter]);

        if ($news) {
            return FunctionLib::ajaxRespondV2(true,'Success');
        }
        return FunctionLib::ajaxRespondV2(false, 'Fail',[],Response::HTTP_NOT_ACCEPTABLE);
    }
}
