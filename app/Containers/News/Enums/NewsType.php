<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-04 22:57:21
 * @ Description: Happy Coding!
 */

namespace App\Containers\News\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class NewsType extends BaseEnum
{
    /**
     * Tin thường
     */
    const NORMAL = 'normal';

    /**
     * Tin khuyến mại
     */
    const PROMOTION ='promotion' ;

    /**
     * Tin sự kiện
     */
    const EVENT = 'event';



    const TEXT = [
        self::NORMAL => 'Tin thường',
        self::PROMOTION => 'Khuyến mãi',
        self::EVENT => 'Sự kiện',
    ];

    // Kedia
    const MEDIA_IMAGE = 1;

    const MEDIA_TYPE = [
        self::MEDIA_IMAGE => 'Hình ảnh danh sách',
    ];

    // Kiểu hiện thị
    const LAYOUT_DEFAULT = 0;
    const LAYOUT_PROJECT_IMAGE = 1;
    const LAYOUT_PROJECT = 2;
    const LAYOUT_RECRUIT = 3;

    const LAYOUT = [
        self::LAYOUT_DEFAULT => 'Hiển thị mặc định',
        self::LAYOUT_PROJECT_IMAGE => 'Hiển thị dạng dự án - slide ảnh',
//        self::LAYOUT_PROJECT => 'Hiển thị dạng dự án - không slide ảnh',
//        self::LAYOUT_RECRUIT => 'Hiển thị dạng tuyển dụng',
    ];
}
