<?php

namespace App\Containers\Partner\Actions;

use App\Containers\Partner\Data\Repositories\PartnerDescRepository;
use App\Containers\Partner\Models\Partner;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class DeletePartnerAction extends Action
{
    public function run($id)
    {
        $deleted = Apiato::call('Partner@DeletePartnerTask', [$id]);

        if ($deleted) {
            Apiato::call('BaseContainer@Admin\DeleteModelDescTask', [app(PartnerDescRepository::class), $id, 'partner_id']);

            Apiato::call('User@CreateUserLogSubAction', [
                $id,
                [],
                [],
                'Xóa Đối tác',
                Partner::class
            ]);

            $this->clearCache();

            return $deleted;
        }

        return false;
    }
}
