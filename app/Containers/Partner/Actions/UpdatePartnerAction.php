<?php

namespace App\Containers\Partner\Actions;

use App\Containers\Partner\Data\Repositories\PartnerDescRepository;
use App\Containers\Partner\Models\Partner;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;

class UpdatePartnerAction extends Action
{
    public function run($request)
    {
        $data = Arr::only($request, [
            'id', 'description', 'status', 'sort_order', 'image', 'type',
        ]);
        $data['updated_by'] = auth()->id();

        $objBefore = Apiato::call('Partner@FindPartnerByIdTask', [$data['id']], [['with' => [['all_desc']]]]);
        $objUpdated = Apiato::call('Partner@UpdatePartnerTask', [$data['id'], Arr::except($data, ['description'])]);

        if ($objUpdated) {
            $originalDesc = Apiato::call('BaseContainer@Admin\GetAllModelDescTask', [app(PartnerDescRepository::class), $objUpdated->id, 'partner_id']);
            Apiato::call('BaseContainer@Admin\SaveModelDescTask', [app(PartnerDescRepository::class), $data, $originalDesc, $objUpdated->id, 'partner_id']);

            // Create Log
            Apiato::call('User@CreateUserLogSubAction', [
                $objUpdated->id,
                $objBefore->toArray(),
                $objUpdated->load('all_desc')->toArray(),
                'Sửa Đối tác',
                Partner::class
            ]);

        }

        $this->clearCache();

        return $objUpdated;
    }
}
