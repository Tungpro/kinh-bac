<?php

namespace App\Containers\Partner\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Partner\Models\Partner;
use App\Ship\Parents\Actions\Action;

class EnablePartnerAction extends Action
{
    public function run($data)
    {
        $object = Apiato::call('Partner@EnablePartnerTask', [$data->id]);

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            [],
            [],
            'Hiển thị Đối tác',
            Partner::class
        ]);

        $this->clearCache();

        return $object;
    }
}
