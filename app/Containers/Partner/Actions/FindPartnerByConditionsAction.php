<?php

namespace App\Containers\Partner\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class FindPartnerByConditionsAction extends Action
{
    public function run(array $conditions = [], array $withData = [], $selectFields = ['*'])
    {
        return Apiato::call('Partner@FindPartnerByConditionsTask', [], [
            ['where' => [$conditions]],
            ['with' => [$withData]],
            ['selectFields' => [$selectFields]]
        ]);
    }
}
