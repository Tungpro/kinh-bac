<?php

namespace App\Containers\Partner\Actions;

use App\Containers\Partner\Data\Repositories\PartnerDescRepository;
use App\Containers\Partner\Models\Partner;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;

class CreatePartnerAction extends Action
{
    public function run($request)
    {
        $data = Arr::only($request, [
            'description', 'status', 'sort_order', 'image', 'type',
        ]);
        $data['created_by'] = auth()->id();

        $objCreated = Apiato::call('Partner@CreatePartnerTask', [Arr::except($data, ['description'])]);

        if ($objCreated) {
            // Create Desc
            Apiato::call('BaseContainer@Admin\SaveModelDescTask', [app(PartnerDescRepository::class), $data, [], $objCreated->id, 'partner_id']);

            // Create Log
            Apiato::call('User@CreateUserLogSubAction', [
                $objCreated->id,
                [],
                $objCreated->load('all_desc')->toArray(),
                'Tạo Đối tác',
                Partner::class
            ]);
        }

        $this->clearCache();

        return $objCreated;
    }
}
