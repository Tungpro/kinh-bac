<?php

namespace App\Containers\Partner\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Partner\Models\Partner;
use App\Ship\Parents\Actions\Action;

class DisablePartnerAction extends Action
{
    public function run($data)
    {
        $object = Apiato::call('Partner@DisablePartnerTask', [$data->id]);

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            [],
            [],
            'Ẩn Đối tác',
            Partner::class
        ]);

        $this->clearCache();

        return $object;
    }
}
