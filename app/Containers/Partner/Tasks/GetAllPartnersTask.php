<?php

namespace App\Containers\Partner\Tasks;

use App\Containers\BaseContainer\Traits\ScopeAdminBaseSearchTrait;
use App\Containers\BaseContainer\Traits\ScopeFEAvailableDataTrait;
use App\Containers\Partner\Data\Repositories\PartnerRepository;
use App\Ship\Criterias\Eloquent\WhereColumnCriteria;
use App\Ship\Parents\Tasks\Task;

class GetAllPartnersTask extends Task
{
    use ScopeAdminBaseSearchTrait;
    use ScopeFEAvailableDataTrait;

    protected $repository;

    public function __construct(PartnerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($request, $limit = 20, $offset = 0, $exceptID = '')
    {

        return $this->returnDataByLimit($limit, $offset, $exceptID);
    }

    public function extraSearchCondition($request)
    {
        if (isset($request->type)) {
            $this->repository->pushCriteria(new WhereColumnCriteria(['type' => $request->type]));
        }

    }
}
