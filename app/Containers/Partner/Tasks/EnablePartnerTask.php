<?php

namespace App\Containers\Partner\Tasks;

use App\Containers\Partner\Data\Repositories\PartnerRepository;
use App\Ship\core\Foundation\BladeHelper;
use App\Ship\Parents\Tasks\Task;
use Exception;

class EnablePartnerTask extends Task
{
    protected $repository;

    public function __construct(PartnerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->getModel()->where('id', $id)->update(['status' => BladeHelper::HIEN_THI]);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
