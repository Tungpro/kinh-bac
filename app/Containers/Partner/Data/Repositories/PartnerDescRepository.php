<?php

namespace App\Containers\Partner\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class PartnerDescRepository extends Repository
{
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
