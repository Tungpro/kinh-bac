<?php

namespace App\Containers\Partner\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class PartnerRepository extends Repository
{
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
