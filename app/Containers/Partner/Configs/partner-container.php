<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Partner Container
    |--------------------------------------------------------------------------
    |
    |
    |
    */

    'type' => [
        0 => 'Đối tác chiến lược - Chỉ hình ảnh (Mặc định)',
        1 => 'Công ty liên kết - Tiêu đề, Mô tả, Hình ảnh',
//        2 => 'Chính sách bảo hành - Tiêu đề, Mô tả, Hình ảnh',
    ],

    'type_id' => [
        'partner-default' => 0,
        'partner-reason' => 1,
//        'partner-policy' => 2,
    ],
];
