<?php

/** @var Route $router */
$router->group([
    'middleware' => ['auth:admin'],
    'namespace' => '\App\Containers\Partner\UI\WEB\Controllers\Admin',
    'domain' => sprintf('admin.%s', parse_url(config('app.url'))['host'])
], function () use ($router) {
    $router->resource('doi-tac', 'Controller')
        ->names([
            'index' => 'admin.partner.index',
            'create' => 'admin.partner.create',
            'store' => 'admin.partner.store',
            'edit' => 'admin.partner.edit',
            'update' => 'admin.partner.update',
            'destroy' => 'admin.partner.destroy',
        ])
        ->except(['show']);

    $router->post('/doi-tac/enable/{id}', ['as' => 'admin.partner.enable', 'uses' => 'Controller@enable',]);
    $router->post('/doi-tac/disable/{id}', ['as' => 'admin.partner.disable', 'uses' => 'Controller@disable',]);
});
