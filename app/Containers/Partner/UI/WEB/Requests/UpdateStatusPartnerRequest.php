<?php

namespace App\Containers\Partner\UI\WEB\Requests;

use App\Containers\Partner\Models\Partner;
use App\Ship\Parents\Requests\Request;

class UpdateStatusPartnerRequest extends Request
{
    protected $access = [
        'permissions' => 'partner-edit',
        'roles' => 'admin',
    ];

    protected $decode = [];

    protected $urlParameters = [
        'id',
    ];

    public function rules()
    {
        return [
            'id' => ['required', sprintf('exists:%s,id', Partner::TABLE_NAME)],

        ];
    }

    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
