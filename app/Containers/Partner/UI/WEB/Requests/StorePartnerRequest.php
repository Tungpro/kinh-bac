<?php

namespace App\Containers\Partner\UI\WEB\Requests;

use App\Containers\BaseContainer\Traits\RequestBaseLanguage;
use App\Ship\Parents\Requests\Request;

class StorePartnerRequest extends Request
{
    use RequestBaseLanguage;

    protected $access = [
        'permissions' => 'partner-create',
        'roles' => 'admin',
    ];

    protected $decode = [
        // 'id',
    ];

    protected $urlParameters = [
        // 'id',
    ];

    public function rules()
    {
        return [
//            'description.*.name' => 'bail|required|string|max:255',
            'description.*.short_description' => 'bail|nullable|string|max:255',
            'description.*.link' => ['bail', 'nullable', 'string', 'regex:/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/', 'max:255'],

            'status' => 'bail|nullable|numeric|in:1,2',
            'sort_order' => 'bail|nullable|numeric|min:0',

            'type' => ['bail', 'required', 'in:' . implode(',', array_keys(config('partner-container.type')))],

            'image' => 'bail|nullable|mimes:jpeg,jpg,png|max:15360',
        ];
    }

    public function attributes()
    {

         return [
            'status' => 'Trạng thái',
            'sort_order' => 'Sắp xếp',

            'type' => 'Loại hiển thị',

            'image' => 'Hình ảnh',
        ] + $this->messagesLang([
//            'description.*.name' => 'Tiêu đề language',
            'description.*.short_description' => 'Mô tả ngắn language',
        ]);
    }



    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
