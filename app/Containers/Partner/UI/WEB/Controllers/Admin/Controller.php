<?php

namespace App\Containers\Partner\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Partner\UI\WEB\Requests\CreatePartnerRequest;
use App\Containers\Partner\UI\WEB\Requests\DeletePartnerRequest;
use App\Containers\Partner\UI\WEB\Requests\GetAllPartnersRequest;
use App\Containers\Partner\UI\WEB\Requests\UpdatePartnerRequest;
use App\Containers\Partner\UI\WEB\Requests\StorePartnerRequest;
use App\Containers\Partner\UI\WEB\Requests\EditPartnerRequest;
use App\Containers\Partner\UI\WEB\Requests\UpdateStatusPartnerRequest;
use App\Ship\Parents\Controllers\AdminController;
use Apiato\Core\Foundation\Facades\Apiato;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class Controller extends AdminController
{
    protected $containerName = 'partner';

    public function __construct()
    {
        $this->title = 'Đối tác';
        $this->imageField = 'image';
        $this->imageKey = 'partner';
        $this->actions = [
            'list' => 'Partner@GetAllPartnersAction',
            'create' => 'Partner@CreatePartnerAction',
            'edit' => 'Partner@FindPartnerByIdAction',
            'update' => 'Partner@UpdatePartnerAction',
            'delete' => 'Partner@DeletePartnerAction',
        ];
        $this->routes = [
            'list' => 'admin.partner.index',
            'create' => 'admin.partner.create',
            'store' => 'admin.partner.store',
            'edit' => 'admin.partner.edit',
            'update' => 'admin.partner.update',
            'destroy' => 'admin.partner.destroy',
            'disable' => "admin.$this->containerName.disable",
            'enable' => "admin.$this->containerName.enable",
        ];
        View::share('routes', $this->routes);
        View::share('containerName', $this->containerName);

        parent::__construct();
    }

    public function index(GetAllPartnersRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);

        $request->merge(['scopeAdminBaseSearch' => true]);
        $data = Apiato::call($this->actions['list'], [$request, $this->perPage, null, ['desc'], [], ['*'], ['type' => 'ASC', 'created_at' => 'DESC']]);

        $types = array_merge(['' => '-- Loại hiển thị --'], config('partner-container.type'));

        return view("$this->containerName::Admin.index", [
            'request' => $request,
            'data' => $data,
            'types' => $types,
        ]);
    }

    public function create(CreatePartnerRequest $request)
    {
        $this->showAddForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['add', $this->title, $this->routes['list']]);

        return view("$this->containerName::Admin.form", [
            'types' => config('partner-container.type'),
        ]);
    }

    public function store(StorePartnerRequest $request)
    {
        $this->editMode = false;
        return $this->save($request);
    }

    public function edit($id, EditPartnerRequest $request)
    {
        $this->showEditForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['edit', $this->title, $this->routes['list']]);

        $editedObj = Apiato::call($this->actions['edit'], [$id, ['all_desc']]);

        if ($editedObj) {

            return view("$this->containerName::Admin.form", [
                'data' => $editedObj,
                'types' => config('partner-container.type'),
            ]);
        }

        return redirect()->route($this->routes['list']);
    }

    public function update(UpdatePartnerRequest $request)
    {
        $this->editMode = true;
        return $this->save($request);
    }

    public function destroy($id, DeletePartnerRequest $request)
    {
        DB::beginTransaction();
        try {
            Apiato::call($this->actions['delete'], [$id]);

            DB::commit();
            return FunctionLib::ajaxRespondV2(true, 'Xóa bản ghi thành công!');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình xóa dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function enable(UpdateStatusPartnerRequest $request)
    {
        try {
            Apiato::call('Partner@EnablePartnerAction', [$request]);

            return FunctionLib::ajaxRespondV2(true, 'Hiện bản ghi thành công!');
        } catch (Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình sửa dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function disable(UpdateStatusPartnerRequest $request)
    {
        try {
            Apiato::call('Partner@DisablePartnerAction', [$request]);

            return FunctionLib::ajaxRespondV2(true, 'Ẩn bản ghi thành công!');
        } catch (Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình sửa dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }
}
