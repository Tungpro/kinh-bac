@extends('basecontainer::admin.layouts.default')

@section('content')
    @if (isset($editMode) && $editMode)
        {!! Form::open(['url' => route($routes['update'], $data->id), 'files' => true]) !!}
        <input type="hidden" name="id" value="{{$data->id}}"/>
        @method('PUT')
    @else
        {!! Form::open(['url' => route($routes['store']), 'files' => true]) !!}
    @endif

    <div class="row">
        <div class="col-12">
            @include('basecontainer::admin.inc.alert')
        </div>
    </div>

    <div class="row">
        <div class="col-7">
            <div class="card card-accent-success">
                <div class="card-header">
                    <i class="fa fa-pencil-square-o"></i> {{__('Thông tin chung')}}
                </div>
                <div class="card-body">
                    <div class="tabbable boxed parentTabs">
                        <div class="tab-content">
                            @include("$containerName::Admin.edit_tabs.general")
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-5">
            <div class="row">
                <div class="col-12">
                    <div class="card card-accent-info">
                        <div class="card-header">
                            <i class="fa fa-deviantart"></i> {{__('Chi tiết')}}
                        </div>
                        <div class="card-body">
                            <div class="tabbable boxed parentTabs">
                                <div class="tab-content">
                                    @include("$containerName::Admin.edit_tabs.detail")
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 mt-2">
                    <div class="card card-accent-warning">
                        <div class="card-header">
                            <i class="fa fa-image"></i> {{__('Hình ảnh')}}
                        </div>
                        <div class="card-body">
                            <div class="tabbable boxed parentTabs">
                                <div class="tab-content">
                                    @include("basecontainer::admin.inc.edit_tabs.image")
                                </div>
                            </div>
                        </div>
                    </div>
                   {{-- <div class="card card-accent-warning">
                        <div class="card-header">
                            <i class="fa fa-image"></i> {{__('File')}} (dành cho trang chính sách bảo hành)
                        </div>
                        <div class="card-body">
                            <div class="tabbable boxed parentTabs">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="file">
                                        <div class="tabbable">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="image">File</label>
                                                        <input type="file" id="image" name="image" accept=".doc, .docx,.ppt, .pptx,.txt,.pdf"
                                                               class="form-control pt-1 {{ $errors->has('file') ? 'is-invalid' : '' }}">
                                                        @if(!empty($data->file))
                                                            <div class="holderFile mt-2">
                                                                <span class="oldFile">{{$data->file}}</span>
--}}{{--                                                                <img width="100" class="oldImg img-thumbnail" src="{{ $data->getImageUrl('small') }}">--}}{{--

                                                                <input type="hidden" name="delete_file" class="delete_file">
                                                                <a class="btn-danger btn-sm ml-3 mt-0 text-decoration-none"
                                                                   href="javascript:void(0)"
                                                                   onclick="$(this).closest('.holderFile').find('.delete_file').val('1');$(this).closest('.holderFile').find('.oldFile').remove();$(this).remove();"
                                                                   title="Xóa file"><i class="fa fa-trash"></i> {{__('Xóa')}}</a>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>

    </div>

    @include('basecontainer::admin.inc.form-btn-submit')
    {!! Form::close() !!}
@stop

@section('js_bot')
    <script type="text/javascript">
        $("ul.nav-tabs a").click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

    </script>
@stop
