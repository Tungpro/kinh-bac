<?php

namespace App\Containers\Partner\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Ship\core\Traits\HelpersTraits\LangTrait;
use App\Ship\Parents\Models\Model;

class Partner extends Model
{
    use LangTrait;

    const TABLE_NAME = 'partner';
    protected $table = self::TABLE_NAME;

    protected $fillable = [
        'status', 'sort_order', 'image', 'type', 'created_by', 'updated_by',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $resourceKey = 'partner';

    /*relationship*/
    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', PartnerDesc::class, 'partner_id', 'id');
    }

    public function desc()
    {
        return $this->hasOne(PartnerDesc::class, 'partner_id', 'id');
    }

    /*function */
    public function getImageUrl($size = 'small')
    {
        return ImageURL::getImageUrl($this->image, 'partner', $size);
    }

}
