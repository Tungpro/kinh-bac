<?php

namespace App\Containers\Partner\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class PartnerDesc extends Model
{
    const TABLE_NAME = 'partner_desc';
    protected $table = self::TABLE_NAME;

    protected $fillable = [
        'partner_id', 'language_id', 'name', 'short_description', 'link',
    ];

    protected $resourceKey = 'partner_desc';

    public function language()
    {
        return $this->hasOne(Language::class, 'language_id', 'language_id');
    }

}
