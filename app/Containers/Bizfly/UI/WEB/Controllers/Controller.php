<?php

namespace App\Containers\Bizfly\UI\WEB\Controllers;

use App\Containers\User\Models\User;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\WebController;
use App\Containers\Notification\Values\NotificationStructureValue;

/**
 * Class Controller
 *
 * @package App\Containers\Bizfly\UI\WEB\Controllers
 */
class Controller extends WebController
{
//    public function test()
//    {
//        return Apiato::call('Notification@PushNotificationAction', [
//            User::all(),
//            new NotificationStructureValue('Remind: You need to confirm with your owner to close constract', 'http://kenh14.vn', ['favorite' => 'life style'])
//        ]);
//    }
} // End class
