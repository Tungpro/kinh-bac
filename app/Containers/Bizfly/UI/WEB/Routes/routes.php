<?php

/** @var Route $router */
$router->get('bizflies/test', [
    'as' => 'web_bizfly_update',
    'uses'  => 'Controller@test',
    'middleware' => [
      'auth:admin',
    ],
]);

/** @var Route $router */
$router->get('bizflies/run_cron_sendmail', [
  'as' => 'web_bizfly_run_cron_sendmail',
  'uses'  => function(){
    \Artisan::call('bizfly_send_mailcron');
    echo 'Done Cron';
  },
  'middleware' => [
    'auth:admin',
  ],
]);