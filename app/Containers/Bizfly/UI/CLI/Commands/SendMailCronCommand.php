<?php

namespace App\Containers\Bizfly\UI\CLI\Commands;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Illuminate\Console\Command;
use Monolog\Handler\TelegramBotHandler;

class SendMailCronCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bizfly_send_mailcron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mail cron Bizfly';

    public $cronEnv;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->cronEnv = config('app.env');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Apiato::call('Bizfly@Alert\PushTeleAlertAction', [
            sprintf('ENV: <code>%s</code> | Enter Crontab', $this->cronEnv),
            'cron'
        ]);

        try {
            $mailCrons = Apiato::call('Bizfly@Mail\GetMailCronAction', [
                ['is_sent' => 0]
            ]);
    
            if ($mailCrons->isNotEmpty()) {
                $successCronIds = [];
    
                foreach ($mailCrons as $mailCron) {
                    $result = Apiato::call('Bizfly@Mail\SendMailAction', [
                        [
                            'subject' => $mailCron->subject,
                            'to' => $mailCron->email,
                            'html' => $mailCron->html
                        ] 
                    ]);
    
                    if ($result) {
                        $successCronIds[] = $mailCron->id;
                    }
                }
    
                if ( !empty($successCronIds) ) {
                    Apiato::call('Bizfly@Mail\UpdateMailCronAction', [
                        $successCronIds,
                        ['is_sent' => 1]
                    ]);
                }
            }
        }catch(\Exception $e) {
            throw $e;
        }
    }
}
