<?php 
namespace App\Containers\Bizfly\Contracts;

interface MailContract {
    /**
     * Send mail function
     *
     * @param array $mailMetaData The basic info of mail message record
     *      $mailMetaData['to']               
     *      $mailMetaData['cc']               
     *      $mailMetaData['bcc']       
     *      $mailMetaData['subject']               
     *      $mailMetaData['content']
     * @return void
     */
    public function send(array $mailMetaData=[]);
}