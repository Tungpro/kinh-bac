<?php

namespace App\Containers\Bizfly\Values;

use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Ship\Parents\Values\BaseValue;
use App\Ship\Parents\Values\Value;

class MailResponseValue extends BaseValue
{
    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'mailresponsevalues';

    public function isOk(): bool {
        if (isset($this->response) && !empty($this->response['id'])) {
            return true;
        }

        return false;
    }

    public function getErrorResponse(): string {
        $currentEnv = config('app.env');
        return sprintf('ENV: <code>%s</code> | Mail Gateway Err Response: <code>%s</code>', $currentEnv, FunctionLib::prettyJson($this->response));
    }
}
