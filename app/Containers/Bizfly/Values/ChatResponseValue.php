<?php

namespace App\Containers\Bizfly\Values;

use App\Ship\Parents\Values\Value;
use App\Ship\Parents\Values\BaseValue;
use Apiato\Core\Foundation\Facades\FunctionLib;

class ChatResponseValue extends BaseValue
{
    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'mailresponsevalues';

    public function isSuccess(): bool {
        return isset($this->response['status']) && $this->response['status'] == true;
    }

    public function getBotId(): int {
        return $this->response['data']['bot_id'];
    }

    public function getErrorResponse(): string {
        $currentEnv = config('app.env');
        return sprintf('ENV: <code>%s</code> | Chat Err Response: <code>%s</code> 
                                             | Request: <code>%s</code>
                                             | Auth: <code>%s</code>', 
                $currentEnv, 
                FunctionLib::prettyJson($this->response),
                FunctionLib::prettyJson($this->request),
                FunctionLib::prettyJson(auth('customer')->user())
        );
    }
}
