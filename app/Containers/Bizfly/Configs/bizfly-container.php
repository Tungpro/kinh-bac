<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Bizfly Container
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'mail' => [
        'end_point' => env('BIZFLY_MAIL_ENDPOINT', 'https://in-x.bizfly.vn/api/'),
        'api_key' => env('BIZFLY_MAIL_APIKEY', ''),
        'domain' => env('APP_DOMAIN', ''),
        'timeout' => 60,
        'namespace' => env('BIZFLY_MAIL_NAMESPACE', 'zmail'),

        'from' => '',

        'channel' => [
            'mailgun' => 'Bizfly Mailgun',
            'amazon' => 'Bizfly Mail Amazon',
            'smtp' => 'Your custom SMTP Server'
        ]
    ],

    'chat' => [
        'domain' => env('BIZFLY_CHAT_DOMAIN', 'https://chat.bizfly.vn'),
    ],

    'alert' => [
        'bot_token' => '1162466766:AAE3zQk50p7px17AUHgr1bC24agwEa9YQT0',
        'alert_group' => '-501784579',
        'alert_cron_group' => '-501784579',
        'api_intergrate_group' => '-501784579',

        'api_endpoint' => 'https://api.telegram.org/bot',
        'ignore_env' => ['local']
    ]
];
