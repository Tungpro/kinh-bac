<?php

namespace App\Containers\Bizfly\Tasks\Chat;

use App\Containers\Bizfly\Data\Repositories\ChatbotRepository;
use App\Containers\Bizfly\Models\Chatbot;

class StoreChatBotIntoDBTask extends BizflyChatTask 
{
    protected $repository;

    public function __construct(ChatbotRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $chatBotData): Chatbot
    {
        return $this->repository->create($chatBotData);
    }
} // End class
