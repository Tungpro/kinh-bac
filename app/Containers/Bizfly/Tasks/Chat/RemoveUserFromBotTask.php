<?php

namespace App\Containers\Bizfly\Tasks\Chat;

use App\Containers\Bizfly\Models\Chatbot;
use App\Containers\Bizfly\Values\ChatResponseValue;

class RemoveUserFromBotTask extends BizflyChatTask 
{
    protected $repository;

    protected $statusRemoveUserFromBot = 0;

    public function run(Chatbot $chatBot, array $settings, $emails): ChatResponseValue
    {
        $url = sprintf('%s/api/bot/change-permission', config('bizfly-container.chat.domain'));
        
        $chatResponseValue = $this->makeRequest([
            'project_token' => $settings['intergrated']['bizfly_project_token'],
            'email' => $emails,
            'email_root' => $settings['intergrated']['bizfly_project_owner'],
            'bot_id' => $chatBot->bizfly_chatbot_id,
            'permission' => $this->statusRemoveUserFromBot
        ], $url);
        
        return $chatResponseValue;
    }
} // End class
