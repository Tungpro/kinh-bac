<?php

namespace App\Containers\Bizfly\Tasks\Chat;

use Exception;
use App\Containers\Bizfly\Contracts\MailContract;
use App\Containers\Bizfly\Data\Repositories\BizflyRepository;
use App\Containers\Bizfly\Values\ChatResponseValue;
use App\Containers\Customer\Models\Customer;

class CreateChatBotTask extends BizflyChatTask 
{
    protected $repository;

    public function run(Customer $customer, array $settings): ChatResponseValue
    {
        $url = sprintf('%s/api/bot/create-bot?debug=1', config('bizfly-container.chat.domain'));
        $botType = $customer->isContractor() ? 'Đối tác' : 'Chủ nhà';
        
        $chatResponseValue = $this->makeRequest([
            'project_token' => $settings['intergrated']['bizfly_project_token'],
            'bot_name' => sprintf('Bot %s: %s', $botType, $customer->email)
        ], $url);

        return $chatResponseValue;
    }
} // End class
