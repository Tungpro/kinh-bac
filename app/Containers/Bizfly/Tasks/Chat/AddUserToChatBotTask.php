<?php

namespace App\Containers\Bizfly\Tasks\Chat;

use Exception;
use App\Containers\Bizfly\Contracts\MailContract;
use App\Containers\Bizfly\Data\Repositories\BizflyRepository;
use App\Containers\Bizfly\Models\Chatbot;
use App\Containers\Bizfly\Values\ChatResponseValue;
use App\Containers\Customer\Models\Customer;

class AddUserToChatBotTask extends BizflyChatTask 
{
    protected $repository;

    public function run(Chatbot $chatBot, array $settings, $emails): ChatResponseValue
    {
        $url = sprintf('%s/api/bot/add-user', config('bizfly-container.chat.domain'));
        
        $chatResponseValue = $this->makeRequest([
            'project_token' => $settings['intergrated']['bizfly_project_token'],
            'email' => $emails,
            'email_root' => $settings['intergrated']['bizfly_project_owner'],
            'bot_id' => $chatBot->bizfly_chatbot_id
        ], $url);
        
        return $chatResponseValue;
    }
} // End class
