<?php

namespace App\Containers\Bizfly\Tasks\Chat;

use App\Containers\Bizfly\Data\Repositories\ChatbotUserRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CheckUserInsideChatBotTask extends Task
{

    protected $repository;

    public function __construct(ChatbotUserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $where=[])
    {
        return $this->repository->where($where)->limit(1)->first();
    }
}
