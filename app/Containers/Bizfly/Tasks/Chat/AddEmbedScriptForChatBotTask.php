<?php

namespace App\Containers\Bizfly\Tasks\Chat;

use Exception;
use App\Containers\Bizfly\Contracts\MailContract;
use App\Containers\Bizfly\Data\Repositories\BizflyRepository;
use App\Containers\Bizfly\Models\Chatbot;
use App\Containers\Bizfly\Values\ChatResponseValue;
use App\Containers\Customer\Models\Customer;

class AddEmbedScriptForChatBotTask extends BizflyChatTask 
{
    protected $repository;

    public function run(Chatbot $chatBot, array $settings, string $domain): ChatResponseValue
    {
        $url = sprintf('%s/api/bot/create-embed', config('bizfly-container.chat.domain'));
        $chatResponseValue = $this->makeRequest([
            'project_token' => $settings['intergrated']['bizfly_project_token'],
            'bot_id' => $chatBot->bizfly_chatbot_id,
            'embed_name' => sprintf('Mã nhúng bot: %s-%s', $chatBot->bizfly_bizfly_chatbot_id, $chatBot->id),
            'embed_color' => '#096DD9',
            'embed_domain_show' => [
                $domain
            ]
        ], $url);
        
        
        return $chatResponseValue;
    }
} // End class
