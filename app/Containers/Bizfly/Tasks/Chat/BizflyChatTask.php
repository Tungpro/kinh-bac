<?php

namespace App\Containers\Bizfly\Tasks\Chat;

use Exception;
use GuzzleHttp\Client;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Bizfly\Values\ChatResponseValue;
use App\Containers\Bizfly\Tasks\Alert\PushTeleAlertTask;
use App\Containers\Bizfly\Data\Repositories\BizflyRepository;

class BizflyChatTask extends Task
{
    protected $repository;

    public function __construct(BizflyRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    public function makeRequest(array $payload, string $url, string $method="POST") {
        try {
            $client = new Client();
            $response = $client->request($method, $url, [
                'form_params' => $payload
            ]);

            $result = json_decode($response->getBody(), true);
            $chatResponseValue =  new ChatResponseValue($url, auth()->user(), $payload, $result, 'chat');
        }catch(\Exception $e) {
            $chatResponseValue = new ChatResponseValue($url, auth()->user(), $payload, $e->getTrace(), 'chat');
        }

        if (!$chatResponseValue->isSuccess()) {
            // Hotfix use app instance, do not use magic call
            app(PushTeleAlertTask::class)->run($chatResponseValue->getErrorResponse());
        }
        
        return $chatResponseValue;
    }
} // End class
