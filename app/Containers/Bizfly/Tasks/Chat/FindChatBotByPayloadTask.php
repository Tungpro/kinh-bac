<?php

namespace App\Containers\Bizfly\Tasks\Chat;

use App\Containers\Bizfly\Data\Repositories\ChatbotRepository;
use App\Containers\Bizfly\Models\Chatbot;

class FindChatBotByPayloadTask extends BizflyChatTask 
{
    protected $repository;

    public function __construct(ChatbotRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $where)
    {
        return $this->repository->findWhere($where)->first();
    }
} // End class
