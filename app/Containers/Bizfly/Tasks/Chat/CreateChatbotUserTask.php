<?php

namespace App\Containers\Bizfly\Tasks\Chat;

use App\Containers\Bizfly\Data\Repositories\ChatbotUserRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateChatbotUserTask extends Task
{

    protected $repository;

    public function __construct(ChatbotUserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        return $this->repository->create($data);
    }
}
