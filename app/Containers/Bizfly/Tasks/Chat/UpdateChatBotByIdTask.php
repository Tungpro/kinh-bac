<?php

namespace App\Containers\Bizfly\Tasks\Chat;

use Exception;
use App\Containers\Bizfly\Models\Chatbot;
use App\Containers\Bizfly\Values\ChatResponseValue;
use App\Containers\Bizfly\Data\Repositories\ChatbotRepository;

class UpdateChatBotByIdTask extends BizflyChatTask 
{
    protected $repository;

    public function __construct(ChatbotRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function run(array $params, int $botId)
    {
        return $this->repository->update($params, $botId);
    }
} // End class
