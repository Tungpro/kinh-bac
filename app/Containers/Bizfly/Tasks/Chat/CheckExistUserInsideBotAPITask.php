<?php

namespace App\Containers\Bizfly\Tasks\Chat;

use App\Containers\Bizfly\Models\Chatbot;
use App\Containers\Bizfly\Values\ChatResponseValue;

class CheckExistUserInsideBotAPITask extends BizflyChatTask 
{
    protected $repository;

    public function run(array $payload): ChatResponseValue
    {
        $url = sprintf('%s/api/bot/check-exist-user', config('bizfly-container.chat.domain'));
        $chatResponseValue = $this->makeRequest($payload, $url);
        
        return $chatResponseValue;
    }
} // End class
