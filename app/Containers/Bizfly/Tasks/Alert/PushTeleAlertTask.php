<?php

namespace App\Containers\Bizfly\Tasks\Alert;

use Exception;
use GuzzleHttp\Client;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Bizfly\Data\Repositories\BizflyRepository;

class PushTeleAlertTask extends Task
{
    protected $repository;
    protected $url;
    protected $token;


    public function __construct(BizflyRepository $repository)
    {
        $this->repository = $repository;
        $this->url = config('bizfly-container.alert.api_endpoint');
        $this->token = config('bizfly-container.alert.bot_token');
    }

    public function run($exception, $groupType='error')
    {
        switch ($groupType) {
            case 'error': 
                $chatId = config('bizfly-container.alert.alert_group');
                break;

            case 'cron': 
                $chatId = config('bizfly-container.alert.alert_cron_group');
                break; 

            default: 
                $chatId = config('bizfly-container.alert.alert_group');
                break;
        }
        
        if (is_string($exception)) {
            $text = $exception;
        }else {
            $detail = "\n Line: " . $exception->getLine()
                . "\n File: " . sprintf('<code>%s</code>', $exception->getFile())
                . "\n Code: " . $exception->getCode();

            $text  = sprintf("ENV: <code>%s</code> |Lỗi: <code>%s</code>", config('app.env'), $exception->getMessage());
            $text .= "\n URL: ".request()->url() ?? 'Không xác định'."
                    \n Method: ".request()->route() ?? request()->route()->getActionName() ?? 'Không xác định';

            $text .= $detail;
        }

        return $this->pushNoty(['chat_id' => $chatId, 'text' => $text]);
    }
 
    public function pushNoty(array $params=[]) {
        try {
            $currentEnv = config('app.env');
            $ignoreEnv = config('bizfly-container.alert.ignore_env');
            if (in_array($currentEnv, $ignoreEnv) || empty($currentEnv)) {
                return true;
            }

            $uri = $this->url . $this->token . '/sendMessage?parse_mode=html';
            $option['verify'] = false;
            $option['form_params'] = $params;
            $option['http_errors'] = false;
            $client = new Client();
            return $client->request("POST", $uri, $option);
        }catch(\Exception $e) {
            return true;
        }
    }
}
