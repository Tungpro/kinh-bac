<?php

namespace App\Containers\Bizfly\Tasks\Mail;

use Exception;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Bizfly\Contracts\MailContract;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Containers\Bizfly\Data\Repositories\BizflyRepository;

class SendMailByAmazonTask extends BizflyMailTask implements MailContract
{
    protected $repository;

    public function __construct(BizflyRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    public function run(array $mailMetaData=[])
    {
        return $this->send($mailMetaData);
    }

    public function send(array $mailMetaData=[])
    {   
        $mailMetaData['from'] = $mailMetaData['from'] ?? $this->from;
        
        $sendMailEndpoint = sprintf('%s%s/messages', $this->endPoint, $this->domain);
        return $this->executeSendMail($sendMailEndpoint, $mailMetaData);
    }
} // End class
