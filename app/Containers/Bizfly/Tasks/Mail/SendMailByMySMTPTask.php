<?php

namespace App\Containers\Bizfly\Tasks\Mail;

use Exception;
use App\Ship\Parents\Tasks\Task;
use App\Containers\Bizfly\Contracts\MailContract;
use App\Containers\Bizfly\Data\Repositories\BizflyRepository;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

class SendMailByMySMTPTask extends Task implements MailContract
{
    protected $isOk = true;
    protected $repository;

    public function __construct(BizflyRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($data): self
    {
        return $this->send($data);
    }

    public function send(array $data = []): self
    {
        Mail::send([], [], function (Message $message) use ($data) {
            $message->to($data['to'])
                ->subject($data['subject'])
                ->setBody($data['html'], 'text/html');
        });

        return $this;
    }

    public function isOk()
    {
        return $this->isOk;
    }
} // End class
