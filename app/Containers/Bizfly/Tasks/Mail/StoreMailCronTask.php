<?php

namespace App\Containers\Bizfly\Tasks\Mail;

use App\Containers\Bizfly\Data\Repositories\MailCronRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class StoreMailCronTask extends Task
{

    protected $repository;

    public function __construct(MailCronRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        return $this->repository->insertOrIgnore($data);
    }
}
