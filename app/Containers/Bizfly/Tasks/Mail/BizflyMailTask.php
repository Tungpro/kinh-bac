<?php

namespace App\Containers\Bizfly\Tasks\Mail;

use App\Ship\Parents\Tasks\Task;
use App\Containers\Bizfly\Values\MailResponseValue;

class BizflyMailTask extends Task 
{
    protected $endPoint;
    protected $domain;
    protected $apiKey;
    protected $timeout;
    
    protected $from;

    public function __construct()
    {
        $bizflyMailConfigurationParams = config('bizfly-container.mail');
        $this->endPoint = $bizflyMailConfigurationParams['end_point'];
        $this->domain   = $bizflyMailConfigurationParams['domain'];
        $this->apiKey   = $bizflyMailConfigurationParams['api_key'];
        $this->timeout  = $bizflyMailConfigurationParams['timeout'];

        $this->from     = $bizflyMailConfigurationParams['from'];
    }

    public function executeSendMail(string $sendMailEndpoint, $mailMetaData) {
        $mailMetaData['o_is_high_priority'] = "yes"; 
        $mailMetaData['o_tracking'] = "no";
        $mailMetaData['html'] = preg_replace('~[\r\n]+~', '', $mailMetaData['html']);
        
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $sendMailEndpoint);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            
            $postVar = "";
            foreach ($mailMetaData as $metaKey => $metaData) {
                $postVar .= $metaKey . "=" . curl_escape($ch, $metaData) . "&";
            }

            $postVar = rtrim($postVar, '&');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postVar);
            curl_setopt($ch, CURLOPT_USERPWD, "api:" . $this->apiKey);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $sendMailResult = curl_exec($ch);
            curl_close($ch);

            if ($sendMailResult) {
                $bizflyMailRecord = json_decode($sendMailResult, true);
                return new MailResponseValue($sendMailEndpoint, auth()->user(), $mailMetaData, $bizflyMailRecord);
            }

            return new MailResponseValue($sendMailEndpoint, auth()->user(), $mailMetaData, json_decode($sendMailResult, true));
        }catch(\Exception $e) {
            return new MailResponseValue($sendMailEndpoint, auth()->user(), $mailMetaData, $e->getTrace());
        }
    }
} // End class
