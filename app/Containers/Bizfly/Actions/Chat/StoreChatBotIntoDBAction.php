<?php

namespace App\Containers\Bizfly\Actions\Chat;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Customer\Models\Customer;

class StoreChatBotIntoDBAction extends Action 
{
    public function run(int $botId, int $customerId)
    {
        $chatBotData = [
            'bot_id' => $botId,
            'customer_id' => $customerId
        ];

        return Apiato::call('Bizfly@Chat\StoreChatBotIntoDBTask', $chatBotData);
    }
} // End class
