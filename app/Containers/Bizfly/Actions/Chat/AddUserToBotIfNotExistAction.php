<?php

namespace App\Containers\Bizfly\Actions\Chat;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Bizfly\Models\Chatbot;

class AddUserToBotIfNotExistAction extends Action
{
    public function run(Chatbot $chatBot, array $settings, string $email, int $customerId)
    {
        return Apiato::call('Bizfly@Chat\AddUserToBotIfNotExistSubAction',  [ 
            $chatBot, 
            $settings, 
            $email, 
            $customerId
        ]);
    }
}
