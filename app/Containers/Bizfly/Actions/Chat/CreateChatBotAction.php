<?php

namespace App\Containers\Bizfly\Actions\Chat;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Customer\Models\Customer;

class CreateChatBotAction extends Action 
{
    public function run(Customer $customer, array $settings=[], $customerId=null)
    {
        return Apiato::call('Bizfly@Chat\CreateChatBotSubAction', [
            $customer,
            $settings,
            $customerId
        ]);
    }
} // End class
