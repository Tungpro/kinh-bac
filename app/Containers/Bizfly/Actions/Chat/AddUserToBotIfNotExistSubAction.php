<?php

namespace App\Containers\Bizfly\Actions\Chat;

use App\Ship\Parents\Actions\SubAction;
use App\Containers\Bizfly\Models\Chatbot;
use Apiato\Core\Foundation\Facades\Apiato;

class AddUserToBotIfNotExistSubAction extends SubAction
{
    public function run(Chatbot $chatBot, array $settings, string $email, int $customerId)
    {
        $addUserToChatBotApiResult = Apiato::call('Bizfly@Chat\AddUserToChatBotTask', [$chatBot, $settings, $email]);
        Apiato::call('Log@StoreApiLogTask', [$addUserToChatBotApiResult->toArray()]);

        return $addUserToChatBotApiResult;
    }
}
