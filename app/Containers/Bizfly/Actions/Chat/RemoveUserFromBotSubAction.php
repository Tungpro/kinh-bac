<?php

namespace App\Containers\Bizfly\Actions\Chat;

use App\Ship\Parents\Actions\SubAction;
use App\Containers\Bizfly\Models\Chatbot;
use Apiato\Core\Foundation\Facades\Apiato;

class RemoveUserFromBotSubAction extends SubAction
{
    public function run(Chatbot $chatBot, array $settings, string $email, int $customerId)
    {
        // Check current user (email) inside bot or not
        $userInsideBot = Apiato::call('Bizfly@Chat\CheckUserInsideChatBotTask', [
            [
                'bot_id' => $chatBot->id, 
                'email' => $email,
                'customer_id' => $customerId
            ]
        ]);

        if ($userInsideBot) {
            $removeUserFromBot = Apiato::call('Bizfly@Chat\RemoveUserFromBotTask', [$chatBot, $settings, $email]);
            Apiato::call('Log@StoreApiLogTask', [$removeUserFromBot->toArray()]);
            
            return $removeUserFromBot;
        }

        return true;
    }
}
