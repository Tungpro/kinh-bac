<?php

namespace App\Containers\Bizfly\Actions\Chat;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class CheckExistUserInsideBotAPIAction extends Action 
{
    public function run(string $project_token, int $bizflyChatBotId, string $email)
    {
        $checkAllowUserToBotApiResult = Apiato::call('Bizfly@Chat\CheckExistUserInsideBotAPITask', [
            [
                'project_token' => $project_token,
                'bot_id' => $bizflyChatBotId,
                'email' => $email
            ]
        ]);
        
        Apiato::call('Log@StoreApiLogTask', [$checkAllowUserToBotApiResult->toArray()]);
        return $checkAllowUserToBotApiResult;
    }
} // End class
