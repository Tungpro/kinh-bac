<?php

namespace App\Containers\Bizfly\Actions\Chat;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Bizfly\Models\Chatbot;
use App\Containers\Customer\Models\Customer;
use Facade\FlareClient\Api;

class AddEmbedScriptForChatBotAction extends Action 
{
    public function run($chatBot, array $settings, string $domain='')
    {
        if ($chatBot) {
            if ( empty($domain) ) {
                $domain = config('app.url');
            }
    
            $chatResponseValue = Apiato::call('Bizfly@Chat\AddEmbedScriptForChatBotTask', [
                $chatBot,
                $settings,
                $domain
            ]);
    
            Apiato::call('Log@StoreApiLogTask', [$chatResponseValue->toArray()]);
    
            if ($chatResponseValue->isSuccess()) {
                $chatBot = Apiato::call('Bizfly@Chat\UpdateChatBotByIdTask', [
                    [
                        'embed_script' => $chatResponseValue->response['data']['embed'],
                        'embed_id' => $chatResponseValue->response['data']['embed_id']
                    ],
    
                    $chatBot->id 
                ]);
    
                return $chatBot;
            }
        }
    }
} // End class
