<?php

namespace App\Containers\Bizfly\Actions\Chat;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class FindChatBotByCustomerIdAction extends Action 
{
    public function run(int $customerId)
    {
        return Apiato::call('Bizfly@Chat\FindChatBotByPayloadTask', [
            ['customer_id' => $customerId]
        ], [
            ['limit' => 1]
        ]);
    }
} // End class
