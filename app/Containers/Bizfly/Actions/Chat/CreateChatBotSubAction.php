<?php

namespace App\Containers\Bizfly\Actions\Chat;

use App\Ship\Parents\Actions\SubAction;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Customer\Models\Customer;
use Illuminate\Support\Facades\DB;

class CreateChatBotSubAction extends SubAction
{
    public function run(Customer $customer, array $settings=[], $customerId=null)
    {
        $createChatBotResponse = Apiato::call('Bizfly@Chat\CreateChatBotTask', [
            $customer, 
            $settings
        ]);

        Apiato::call('Log@StoreApiLogTask', [$createChatBotResponse->toArray()]);

        if ($createChatBotResponse->isSuccess()) {
            $chatBotData = [
                'bizfly_chatbot_id' => $createChatBotResponse->getBotId(),
                'customer_id' => $customer->id
            ];

            $chatBot = Apiato::call('Bizfly@Chat\StoreChatBotIntoDBTask', [$chatBotData]);

            // Add customer and email admin to bot
            $emailsToPut = explode(';', $settings['intergrated']['emails_admin'] ?? '');

            $emailsSettings = [];

            if (!empty($emailsToPut)) {
                foreach ($emailsToPut as $email) {
                    $emailsSettings[] = [
                        'email' => $email, 
                        'is_setting_email' => 'yes'
                    ];
                }
            }

            $emailsSettings[] = [
                'email' => $customer->email,
                'is_setting_email' => 'no'
            ];

            if ( !empty($emailsSettings) ) {
                foreach ($emailsSettings as $emailItem) {

                    if (!empty($emailItem['email'])) {
                        $addUsertToChatBot = Apiato::call('Bizfly@Chat\AddUserToChatBotTask', [$chatBot, $settings, $email]);
                    
                        if ($addUsertToChatBot->isSuccess()) {
                            Apiato::call('Bizfly@Chat\CreateChatbotUserTask', [
                                [
                                    'bot_id' => $chatBot->id,
                                    'email' => $emailItem['email'],
                                    'customer_id' => $customerId,
                                    'is_setting_email' => $emailItem['is_setting_email']
                                ]
                            ]);
                        }

                        Apiato::call('Log@StoreApiLogTask', [$addUsertToChatBot->toArray()]);
                    }
                }
            }
            
            return $chatBot;
        }
    }
}
