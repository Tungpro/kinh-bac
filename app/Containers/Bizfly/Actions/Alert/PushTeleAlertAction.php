<?php

namespace App\Containers\Bizfly\Actions\Alert;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class PushTeleAlertAction extends Action
{
    public function run($exception, $groupType='error')
    {
        $bizfly = Apiato::call('Bizfly@Alert\PushTeleAlertTask', [$exception, $groupType]);

        return $bizfly;
    }
}