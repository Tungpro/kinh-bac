<?php

namespace App\Containers\Bizfly\Actions\Mail;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Bizfly\Contracts\MailContract;
use App\Containers\Bizfly\Tasks\Mail\SendMailByMySMTPTask;

class SendMailAction extends Action
{
    public function run(array $mailMetaData = [])
    {
        // dd($mailMetaData);
        $intergratedSetting = Apiato::call('Settings@FindSettingByKeyTask', ['intergrated']);
        $mailChannelSetting = json_decode($intergratedSetting, true);
        switch ($mailChannelSetting['mail_channel'] ?? '') {
            case 'mailgun':
                $sendMailResult = Apiato::call('Bizfly@Mail\SendMailByMailgunTask', [$mailMetaData]);
                break;

            case 'amazon':
                $sendMailResult = Apiato::call('Bizfly@Mail\SendMailByAmazonTask', [$mailMetaData]);
                break;

            case 'smtp':
                $sendMailResult = Apiato::call('Bizfly@Mail\SendMailByMySMTPTask', [$mailMetaData]);
                break;

            default:
                $sendMailResult = app(SendMailByMySMTPTask::class)->run($mailMetaData);
                break;
        }

        return true;

        // if (!$sendMailResult || !$sendMailResult->isOk()) {
        //     Apiato::call('Bizfly@Alert\PushTeleAlertTask', [
        //         $sendMailResult->getErrorResponse()
        //     ]);
        // }
        // return Apiato::call('Log@StoreApiLogTask', [$sendMailResult->toArray()]);
    }
} // End class
