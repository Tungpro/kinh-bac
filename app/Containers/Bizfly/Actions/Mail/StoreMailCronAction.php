<?php

namespace App\Containers\Bizfly\Actions\Mail;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class StoreMailCronAction extends Action
{
    public function run(array $data=[])
    {   
        $mailcron = Apiato::call('Bizfly@Mail\StoreMailCronTask', [$data]);
        return $mailcron;
    }
}
