<?php

namespace App\Containers\Bizfly\Models;

use App\Ship\Parents\Models\Model;

class MailCron extends Model
{
    protected $table = 'mail_cron';
    
    protected $guarded = [];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'mailcrons';
}
