<?php

namespace App\Containers\Bizfly\Models;

use App\Ship\Parents\Models\Model;

class ChatbotUser extends Model
{
    protected $table = 'bizfly_chatbot_user';

    protected $guarded = [];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'chatbotusers';
}
