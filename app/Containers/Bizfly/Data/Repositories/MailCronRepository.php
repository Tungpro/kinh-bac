<?php

namespace App\Containers\Bizfly\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class MailCronRepository
 */
class MailCronRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
