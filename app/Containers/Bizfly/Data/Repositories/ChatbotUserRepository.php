<?php

namespace App\Containers\Bizfly\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ChatbotUserRepository
 */
class ChatbotUserRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
