<?php

namespace App\Containers\Bizfly\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class BizflyRepository
 */
class BizflyRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
