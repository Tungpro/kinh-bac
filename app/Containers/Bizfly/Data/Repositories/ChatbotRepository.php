<?php

namespace App\Containers\Bizfly\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ChatbotRepository
 */
class ChatbotRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
