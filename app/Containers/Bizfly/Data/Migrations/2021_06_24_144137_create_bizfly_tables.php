<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBizflyTables extends Migration
{

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('bizflies', function (Blueprint $table) {

            $table->increments('id');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('bizflies');
    }
}
