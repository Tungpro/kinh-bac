<?php

namespace App\Containers\Option\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class OptionValueDescRepository.
 */
class OptionValueDescRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Option';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'option_value_id',
        'option_id',
        'language_id',
        'name',
    ];
}
