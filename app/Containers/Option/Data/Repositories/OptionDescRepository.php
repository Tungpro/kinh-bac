<?php

namespace App\Containers\Option\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class OptionDescRepository.
 */
class OptionDescRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Option';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'option_id',
        'status',
        'language_id',
        'name',
    ];
}
