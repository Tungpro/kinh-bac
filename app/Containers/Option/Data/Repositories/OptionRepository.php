<?php

namespace App\Containers\Option\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class OptionRepository.
 */
class OptionRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Option';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'type',
        'status',
        'sort_order',
        'created_at',
        'desc.name',
        'desc.language_id'
    ];
}
