<?php

namespace App\Containers\Option\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\StringLib;
use App\Containers\Option\UI\WEB\Requests\CreateOptionRequest;
use App\Containers\Option\UI\WEB\Requests\DeleteOptionRequest;
use App\Containers\Option\UI\WEB\Requests\FindOptionByKeywordRequest;
use App\Containers\Option\UI\WEB\Requests\FindOptionRequest;
use App\Containers\Option\UI\WEB\Requests\GetAllOptionsRequest;
use App\Containers\Option\UI\WEB\Requests\OptionAutoCompleteRequest;
use App\Containers\Option\UI\WEB\Requests\UpdateOptionRequest;
use App\Ship\core\Traits\HelpersTraits\ApiResTrait;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Http\Request;

class Controller extends AdminController
{
  use ApiResTrait;

  public function __construct()
  {
    $this->title = 'Tùy chọn cho biến thể';
    parent::__construct();
  }

  /**
   * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function index(GetAllOptionsRequest $request)
  {
    $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Tùy chọn', $this->form == 'list' ? '' : route('admin_option_home_page'));
    \View::share('breadcrumb', $this->breadcrumb);

    $options = Apiato::call('Option@OptionListingAction', [new DataTransporter($request), [], $this->perPage]);

    return view('option::admin.index', [
      'search_data' => $request,
      'data' => $options,
    ]);
  }

  public function edit(FindOptionRequest $request)
  {
    $this->showEditForm();
    $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Tùy chọn', $this->form == 'list' ? '' : route('admin_option_home_page'));
    $this->breadcrumb[] = FunctionLib::addBreadcrumb('Chỉnh sửa thông tin');
    \View::share('breadcrumb', $this->breadcrumb);

    $option = Apiato::call('Option@FindByOptionIdAction', [$request->id]);

    if ($option) {

      $values = Apiato::call('Option@OptionValueListingAction', [$option->id]);

      return view('option::admin.edit', [
        'data' => $option,
        'values' => $values
      ]);
    }

    return $this->notfound($request->id);
  }

  public function add(Request $request)
  {
    $this->showAddForm();
    $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Tùy chọn', $this->form == 'list' ? '' : route('admin_option_home_page'));
    $this->breadcrumb[] = FunctionLib::addBreadcrumb('Thêm mới');
    \View::share('breadcrumb', $this->breadcrumb);

    return view('option::admin.edit', []);
  }

  public function update(UpdateOptionRequest $request)
  {
    try {
      Apiato::call('Option@UpdateOptionAction', [$request]);

      return redirect()->route('admin_option_edit_page', ['id' => $request->id])->with('status', 'Option đã được cập nhật');
    } catch (\Exception $e) {
      throw $e;
      $this->throwExceptionViaMess($e);
    }
  }

  public function create(CreateOptionRequest $request)
  {
    try {
      Apiato::call('Option@CreateOptionAction', [$request]);

      return redirect()->route('admin_option_home_page')->with('status', 'Option đã được thêm mới');
    } catch (\Exception $e) {
      // throw $e;
      $this->throwExceptionViaMess($e);
    }
  }

  public function deleteOption(DeleteOptionRequest $request)
  {
    try {
      Apiato::call('Option@DeleteOptionAction', [$request->id]);
    } catch (\Exception $e) {
      // throw $e;
      $this->throwExceptionViaMess($e);
    }
  }

  public function findAutocomplete(FindOptionByKeywordRequest $request) {
    $transporter = $request->toTransporter();
    $options = Apiato::call('Option@FindOptionByKeywordAction', [
      $transporter,
      [
        'desc',
        'option_value.desc'
      ]
    ]);
    return $this->sendResponse($options);
  }

  public function getOptionsAutocomplete(OptionAutoCompleteRequest $request) {
    return FunctionLib::ajaxRespond(true, 'Done', Apiato::call('Option@GetOptionsByNameAction', [$request->name]));
  }
} // End class
