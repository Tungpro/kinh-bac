<?php
Route::group(
    [
        'prefix' => 'option',
        'namespace' => '\App\Containers\Option\UI\WEB\Controllers\Admin',
        'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
        'middleware' => [
            'auth:admin',
        ],
    ],
    function () use ($router) {
        $router->get('/', [
            'as'   => 'admin_option_home_page',
            'uses' => 'Controller@index',
        ]);

        $router->get('/edit/{id}', [
            'as'   => 'admin_option_edit_page',
            'uses' => 'Controller@edit',
        ]);

        $router->post('/edit/{id}', [
            'as'   => 'admin_option_edit_page',
            'uses' => 'Controller@update',
        ]);

        $router->get('/add', [
            'as'   => 'admin_option_add_page',
            'uses' => 'Controller@add',
        ]);

        $router->post('/add', [
            'as'   => 'admin_option_add_page',
            'uses' => 'Controller@create',
        ]);

        $router->delete('/{id}', [
            'as' => 'admin_option_delete',
            'uses'       => 'Controller@deleteOption',
        ]);

        $router->get('/getOptionsAutocomplete', [
            'as'   => 'admin_option_get_autôcmplete',
            'uses' => 'Controller@getOptionsAutocomplete',
        ]);
    }
);
