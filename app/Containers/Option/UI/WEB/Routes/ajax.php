<?php
use Illuminate\Support\Facades\Route;

Route::namespace('\App\Containers\Option\UI\WEB\Controllers\Admin')->prefix('admin/ajax')->group(function () {
  Route::prefix('option')->group(function () {
    Route::get('/getOptionsAutocomplete', [
      'as' => 'admin.option.findAutocomplete',
      'uses' => 'Controller@findAutocomplete'
    ]);

    Route::get('/all', [
      'as' => 'admin.ajax.option.all',
      'uses' => 'AjaxOptionController@all'
    ]);
  });

  Route::prefix('option-value')->group(function () {
    Route::get('/all', [
      'as' => 'admin.ajax.option-value.all',
      'uses' => 'AjaxOptionValueController@all'
    ]);
  });
});

