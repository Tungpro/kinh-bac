@extends('basecontainer::admin.layouts.default')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(isset($editMode) && $editMode)
                {!! Form::open(['url' => route('admin_option_edit_page', $data->id) , 'files' => true]) !!}
            @else
                {!! Form::open(['url' => route('admin_option_add_page') , 'files' => true]) !!}
            @endif
            @if( count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif
            <div class="card card-accent-primary">
                <div class="card-header">
                    <i class="fa fa-pencil"></i> THÔNG TIN CƠ BẢN
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs nav-underline nav-underline-primary mb-3" role="tablist">
                                @foreach($langs as $it_lang)
                                    <li class="nav-item">
                                        <a class="nav-link {{$loop->first ? 'active' : ''}}" data-toggle="tab"
                                           href="#lang_{{$it_lang['language_id']}}" role="tab" aria-controls="website"
                                           aria-expanded="true"><i class="icon-globe"></i> {{$it_lang['name']}}</a>
                                    </li>
                                @endforeach
                            </ul>

                            <div class="tab-content p-0">
                                @foreach($langs as $it_lang)
                                    <div class="tab-pane {{$loop->first ? 'active' : ''}}"
                                         id="lang_{{$it_lang['language_id']}}" role="tabpanel" aria-expanded="true">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="title">Tên <span class="small text-danger">( {{$it_lang['name']}} )</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class=" input-group-text">
                                                                <img src="/admin/img/lang/{{$it_lang['image']}}"
                                                                     title="{{$it_lang['name']}}">
                                                            </span>
                                                        </div>
                                                        <input type="text" class="form-control"
                                                               name="name[{{$it_lang['language_id']}}]"
                                                               id="name_{{$it_lang['language_id']}}"
                                                               value="{{ old('name.'.$it_lang['language_id'],@$data->all_desc[$it_lang['language_id']]->name) }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="row mt-2" style="display: none">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="input-type">Phân loại</label>
                                <select name="type" id="input-type" class="form-control">
                                    <optgroup label="Choose">
                                        <option {{@$data->type == 'select' ? 'selected="selected"' : ''}} value="select">
                                            Select
                                        </option>
                                        {{-- <option {{@$data->type == 'radio' ? 'selected="selected"' : ''}} value="radio">Radio</option>
                                        <option {{@$data->type == 'checkbox' ? 'selected="selected"' : ''}} value="checkbox">Checkbox</option> --}}
                                    </optgroup>
                                    {{-- <optgroup label="Input">
                                        <option {{@$data->type == 'text' ? 'selected="selected"' : ''}} value="text">Text</option>
                                        <option {{@$data->type == 'textarea' ? 'selected="selected"' : ''}} value="textarea">Textarea</option>
                                    </optgroup>
                                    <optgroup label="File">
                                        <option {{@$data->type == 'file' ? 'selected="selected"' : ''}} value="file">File</option>
                                    </optgroup>
                                    <optgroup label="Date">
                                        <option {{@$data->type == 'date' ? 'selected="selected"' : ''}} value="date">Date</option>
                                        <option {{@$data->type == 'time' ? 'selected="selected"' : ''}} value="time">Time</option>
                                        <option {{@$data->type == 'datetime' ? 'selected="selected"' : ''}} value="datetime">Date &amp; Time</option>
                                    </optgroup> --}}
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="title">Sắp xếp</label>
                                <input type="text" class="form-control" name="sort_order" id="sort_order"
                                       value="{{ old('sort_order',@$data->sort_order) }}">
                            </div>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="show_image">Màu thuộc tính hoặc ảnh</label>
                                <select name="show_image" id="show_image" class="form-control">
                                    <option {{@$data->show_image == -1 ? 'selected' : ''}} value="-1">Tắt</option>
                                    <option {{@$data->show_image == 1 ? 'selected' : ''}} value="1">Bật</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-accent-primary">
                <div class="card-header">
                    <i class="fa fa-pencil"></i> THÔNG TIN THUỘC TÍNH
                </div>
                <div class="card-body">
                    <table id="option-value" class="table table-striped table-bordered table-hover table-responsive-sm">
                        <thead>
                        <tr>
                            <th class="text-left required">Tên</th>
                            <td class="text-center">Mã màu</td>
                            <td class="text-center">Hình ảnh</td>
                            <th class="text-right">Sắp xếp</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($values))
                            @foreach($values as $it_value)
                                @php($abc = $loop->index)
                                <tr id="option-value-row{{$loop->index+1}}">
                                    <td class="text-center">
                                        <input type="hidden" name="option_value[option_value_id][]"
                                               value="{{$it_value->id}}">
                                        @foreach($langs as $it_lang)
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <span class=" input-group-text">
                                                        <img src="/admin/img/lang/{{$it_lang['image']}}"
                                                             title="{{$it_lang['name']}}">
                                                    </span>
                                                </div>
                                                <input type="text"
                                                       name="option_value[option_value_description][{{ $it_lang['language_id'] }}][{{@$it_value->all_desc[$it_lang['language_id']]->id ?? '_'.$abc}}]"
                                                       value="{{ @$it_value->all_desc[$it_lang['language_id']]->name ?? '' }}"
                                                       placeholder="" class="form-control">
                                            </div>
                                        @endforeach
                                    </td>
                                    {{--<td class="text-center">
                                        <a id="thumb-image{{$loop->index+1}}"
                                           data-toggle="image" class="img-thumbnail">
                                            <img src="" alt="" title="" data-placeholder=""/>
                                        </a>
                                        <input type="hidden" name="option_value[image][]"
                                               value="{{$it_value->image}}" id="input-image{{$it_value->id}}">
                                    </td>--}}
                                    <td class="text-right">
                                        <input type="color"
                                               name="option_value[image][]"
                                               value="{{$it_value->image}}"
                                               class="form-control">
                                    </td>
                                    <td class="text-right">
                                        <input type="file"
                                               name="option_value[image_two][]"
                                               value="{{$it_value->image_two}}"
                                               class="form-control">
                                        @if(!empty($it_value->image_two))
                                            <input type="hidden"
                                                   name="option_value[check_image_two][]"
                                                   value="{{$it_value->image_two}}"
                                                   class="form-control check_image_two">
                                            <div class="mt-2">
                                                <div class="pull-right">
                                                    <img class="image" width="30%" src="{{ \ImageURL::getImageUrl(@$it_value->image_two, 'optionvalue', 'mediumx2') }}">
                                                    <button type="button" class="btn btn-danger delete-image-two"><i class="fa fa-trash"></i></button>
                                                </div>
                                            </div>
                                        @endif

                                    </td>
                                    <td class="text-right">
                                        <input type="text"
                                               name="option_value[sort_order][]"
                                               value="{{$it_value->sort_order}}"
                                               class="form-control">
                                    </td>
                                    <td class="text-right">
                                        <button type="button"
                                                onclick="$('#option-value-row{{ $loop->index+1 }}').remove();"
                                                data-toggle="tooltip" title="Xóa"
                                                class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>

                        <tfoot>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-right">
                                <button type="button" onclick="addOptionValue();" data-toggle="tooltip" title="Thêm"
                                        class="btn btn-primary">
                                    <i class="fa fa-plus-circle"></i>
                                </button>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="mb-3">
                <button type="submit" id="submit-filter" class="btn btn-sm btn-primary"><i
                            class="fa fa-dot-circle-o"></i> Cập nhật
                </button>
                &nbsp;&nbsp;
                <a class="btn btn-sm btn-danger" href="{{ redirect()->back()->getTargetUrl() }}"><i
                            class="fa fa-ban"></i> Hủy bỏ</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@push('js_bot_all')
    <script>
        $('.delete-image-two').click(function(){
            $(this).closest('.pull-right').find('.image').remove()
            $(this).closest('.text-right').find('.check_image_two').val('');
            $(this).closest('.pull-right').append('<input type="hidden" name="delete_image_two">')
            $(this).remove();
        })

        /*
        $("#attribute_group_id").select2({
            tags: true,
        });

        $('select[name=\'type\']').on('change', function () {
            if (this.value == 'select' || this.value == 'radio' || this.value == 'checkbox' || this.value == 'image') {
                $('#option-value').parent().show();
            } else {
                $('#option-value').parent().hide();
            }
        });

        $('select[name=\'type\']').trigger('change');
        */

        var option_value_row = {{ @$values ? $values->count() : 0 }};

        function addOptionValue() {
            option_value_row++;
            html = '<tr id="option-value-row' + option_value_row + '">';
            html += '  <td class="text-left"><input type="hidden" name="option_value[option_value_id][]" value="_' + option_value_row + '" />';
            @foreach($langs as $it_lang)
            html += '    <div class="input-group mb-2">';
            html += '      <div class="input-group-prepend"><span class=" input-group-text"><img src="/admin/img/lang/{{ $it_lang['image'] }}" title="{{ $it_lang['name'] }}" /></span></div><input type="text" name="option_value[option_value_description][{{ $it_lang['language_id'] }}][_' + option_value_row + ']" value="" placeholder="" class="form-control" />';
            html += '    </div>';
            @endforeach
            html += '  </td>';
            //html += '  <td class="text-center"><a id="thumb-image' + option_value_row + '" data-toggle="image" class="img-thumbnail"><img src="" alt="" title="" data-placeholder="" /></a><input type="hidden" name="option_value[image][]" value="" id="input-image' + option_value_row + '" /></td>';
            html += '<td class="text-right"> <input type="color" name="option_value[image][]" value="" class="form-control"> </td>';
            html += '  <td class="text-right"><input type="text" name="option_value[sort_order][]" value="" placeholder="" class="form-control" /></td>';
            html += '  <td class="text-right"><button type="button" onclick="$(\'#option-value-row' + option_value_row + '\').remove();" data-toggle="tooltip" title="Xóa" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#option-value tbody').append(html);
        }
    </script>
@endpush
