@extends('basecontainer::admin.layouts.default')

@section('right-breads')
    {!! $data->appends($search_data->toArray())->links('basecontainer::admin.inc.paginator') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-5"><h1>{{ $site_title }}</h1></div>

            @if( count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif

            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            {!! Form::open(['url' => route('admin_option_home_page'), 'method' => 'get', 'id' => 'searchForm']) !!}
            <div class="card card-accent-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i
                                            class="fa fa-bookmark-o"></i></span></div>
                                <input type="text" name="name" class="form-control" placeholder="Tên"
                                       value="{{ $search_data->name }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                    <a href="{{ route('admin_option_home_page') }}" class="btn btn-sm btn-primary"><i
                            class="fa fa-refresh"></i> Reset</a>
                    @if($user->can('create-options'))
                        <a href="{{ route('admin_option_add_page') }}" class="btn btn-sm btn-primary"><i
                                class="fa fa-plus"></i> Thêm mới</a>
                    @endif
                </div>
            </div>
            {!! Form::close() !!}

            <div class="card card-accent-primary">
                <div class="card-body p-0">
                    <table class="table table-bordered table-striped table-responsive-sm">
                        <thead>
                        <tr>
                            <th width="55">ID</th>
                            <th>Tên</th>
                            <th>Sắp xếp</th>
                            <th width="55" class="text-center">Lệnh</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $k => $item)
                            <tr>
                                <td>{{++$k}}</td>
                                <td>{{ $item->desc->name }}</td>
                                <td>{{$item->sort_order}}</td>
                                <td align="center">
                                    @if($user->can('update-options'))
                                        <a href="{{ route('admin_option_edit_page', $item->id) }}"
                                           class="btn text-primary"><i class="fa fa-pencil"></i></a>
                                    @endif
                                    @if($user->can('delete-options'))
                                        <a data-href="{{ route('admin_option_delete', $item->id) }}"
                                           class="btn text-danger" onclick="admin.delete_this(this)"><i
                                                class="fa fa-trash"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    @include('basecontainer::admin.inc.bottom-pager-infor')
                </div>
            </div>
        </div>
    </div>
@stop
