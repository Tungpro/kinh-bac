<?php

namespace App\Containers\Option\Tasks;

use App\Containers\Option\Data\Repositories\OptionValueDescRepository;
use App\Containers\Option\Data\Repositories\OptionValueRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class GetOptionValuesByIdsTask.
 */
class GetOptionValuesByIdsAjaxTask extends Task
{

    protected $repository;

    public function __construct(OptionValueRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Bắt buộc Conds phải đủ 3 tham số: field, operation, value cho điều kiện
     * @return  mixed
     */
    public function run(array $optionValueIds=[], array $with=[]): Collection
    {
        return $this->repository->findWhereIn('id', $optionValueIds);
    }

} // End class
