<?php

namespace App\Containers\Option\Tasks;

use App\Containers\Option\Data\Repositories\OptionValueDescRepository;
use App\Containers\Option\Data\Repositories\OptionValueRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Facades\DB;

/**
 * Class GetOptionValuesByIdsTask.
 */
class GetOptionValuesByIdsTask extends Task
{

    protected $repository, $optionValueDescRepository;

    public function __construct(OptionValueRepository $repository,OptionValueDescRepository $optionValueDescRepository)
    {
        $this->repository = $repository;
        $this->optionValueDescRepository = $optionValueDescRepository;
    }

    /**
     * Bắt buộc Conds phải đủ 3 tham số: field, operation, value cho điều kiện
     * @return  mixed
     */
    public function run($option_ids,$defaultLanguage = null)
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;

        $return_data = [];

        $result = DB::table($this->repository->getModel()->getTable() . ' as ov')
            ->leftJoin($this->optionValueDescRepository->getModel()->getTable() . ' as ovd', 'ov.id', '=', 'ovd.option_value_id')
            ->whereIn('ov.option_id', $option_ids)->where('ovd.language_id', $language_id)
            ->orderByRaw('ov.sort_order,ovd.name')->get();

        if (!$result->isEmpty()) {
            foreach ($result as $option_value) {
                $return_data[] = [
                    'option_value_id' => $option_value->option_value_id,
                    'option_id' => $option_value->option_id,
                    'name'            => $option_value->name,
                    'image'           => $option_value->image,
                    'sort_order'      => $option_value->sort_order
                ];
            }
        }

        return $return_data;
    }
}
