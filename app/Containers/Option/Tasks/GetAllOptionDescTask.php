<?php

namespace App\Containers\Option\Tasks;

use App\Containers\Option\Data\Repositories\OptionDescRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class GetAllOptionDescTask.
 */
class GetAllOptionDescTask extends Task
{

    protected $repository;

    public function __construct(OptionDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($option_id)
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('option_id', $option_id));
        $wery = $this->repository->all()->keyBy('language_id')->toArray();

        return $wery;
    }
}
