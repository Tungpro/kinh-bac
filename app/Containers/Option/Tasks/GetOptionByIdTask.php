<?php

namespace App\Containers\Option\Tasks;

use App\Containers\Option\Data\Repositories\OptionRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class GetOptionByIdTask.
 */
class GetOptionByIdTask extends Task
{

    protected $repository;

    public function __construct(OptionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($id)
    {
        $data = $this->repository->with('all_desc')->find($id);
        
        if($data) {
            $data->setRelation('all_desc', $data->all_desc->keyBy('language_id'));
        }

        return $data;
    }
}
