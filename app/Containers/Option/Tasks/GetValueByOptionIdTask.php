<?php

namespace App\Containers\Option\Tasks;

use App\Containers\Option\Data\Repositories\OptionValueRepository;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class GetValueByOptionIdTask.
 */
class GetValueByOptionIdTask extends Task
{

    protected $repository;

    public function __construct(OptionValueRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($option_id)
    {
        $this->repository->pushCriteria(new ThisOperationThatCriteria('status',-1,'>'));
        $this->repository->pushCriteria(new ThisOperationThatCriteria('option_id',$option_id,'='));

        $this->repository->with(['all_desc']);

        //        $data = $this->option_value::with(['desc' => function($query) {
        //            $query->whereHas('language',function (Builder $q)  {
        //                $q->where('is_default', 1);
        //            });
        //        }])->where($conds);

        $this->repository->orderBy('sort_order');
        
        return $this->repository->all();
    }
}
