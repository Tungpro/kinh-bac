<?php

namespace App\Containers\Option\Tasks;

use App\Containers\Option\Data\Repositories\OptionRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class SaveOptionTask.
 */
class SaveOptionTask extends Task
{

    protected $repository;

    public function __construct(OptionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data, $creating = false)
    {
        try {
            
            $dataUpdate = Arr::except($data->toArray(), ['name', 'option_value', '_token', '_headers']);

            if(!$creating) {
                $option = $this->repository->update($dataUpdate, $data->id);
            }else {
                $option = $this->repository->create($dataUpdate);
            }

            return $option;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
