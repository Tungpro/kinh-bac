<?php

namespace App\Containers\Option\Tasks;

use Apiato\Core\Foundation\ImageURL;
use Apiato\Core\Foundation\StringLib;
use App\Containers\Option\Data\Repositories\OptionValueDescRepository;
use App\Containers\Option\Data\Repositories\OptionValueRepository;
use App\Containers\Option\Models\OptionValue;
use App\Ship\Parents\Requests\Request;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;

/**
 * Class SaveOptionValueDescTask.
 */
class SaveOptionValueDescTask extends Task
{

    protected $repository, $optionValueDescRepository;

    public function __construct(OptionValueRepository $repository, OptionValueDescRepository $optionValueDescRepository)
    {
        $this->repository = $repository;
        $this->optionValueDescRepository = $optionValueDescRepository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data,$option_id)
    {
        $option_values = $data['option_value'];

        // dd($option_values);

        if (isset($option_values['option_value_id']) && !empty($option_values)) {

            $updates = [];
            $inserts = [];

            $update_desc = [];
            $insert_desc = [];

            foreach ($option_values['option_value_id'] as $k_id => $it_id) {

                if(!empty($option_values['image_two'][$k_id])) {
                    $data->request->add(['fileImage' => $option_values['image_two'][$k_id]]);
                    $image = $this->image($data, 'fileImage', 'optionvalue',StringLib::getClassNameFromString(OptionValue::class));
                    if (!$image['error']) {
                        $upload_image = $image['fileName'];
                    } else {
                        $join_image = null;
                    }

                } else {
                    $upload_image = !empty($option_values['check_image_two'][$k_id]) ? $option_values['check_image_two'][$k_id] : null;
                }
                if ($it_id  && !StringLib::startsWith($it_id, '_')) {
                    //                    echo $it_id. ' | ';
                    $arr_not_delete[] = $it_id;
                    $updates[$it_id] = [
                        'image' => @$option_values['image'][$k_id],
                        'image_two' => @$upload_image,
                        'sort_order' => @$option_values['sort_order'][$k_id]
                    ];

                    if (!empty($option_values['option_value_description'])) {
                        $reverse = true;
                        foreach ($option_values['option_value_description'] as $k_v_lang => &$it_v_desc) {
                            $fkc_key = array_key_first($it_v_desc);
                            $it_v_desc = array_reverse($it_v_desc, true);
                            $fuck = array_pop($it_v_desc);
                            $it_v_desc = array_reverse($it_v_desc, true);
                            //                            $reverse = !$reverse;
                            //                            dd($fkc_key,$fuck,$it_v_desc);

                            if (!StringLib::startsWith($fkc_key, '_')) {
                                $update_desc[$fkc_key] = ['name' => $fuck];
                            } else {
                                $insert_desc[] = ['option_value_id' => $it_id, 'option_id' => $option_id, 'language_id' => $k_v_lang, 'name' => $fuck];
                            }
                        }
                    }
                } else {

                    $inserts = [
                        'option_id' => $option_id,
                        'image' => @$upload_image,
                        'sort_order' => @$option_values['sort_order'][$k_id],
                        'image_two' => $upload_image]
                    ;
                    $id_return = $this->repository->getModel()->insertGetId($inserts);

                    if ($id_return) {
                        $arr_not_delete[] = $id_return;
                        $insert_new_desc = [];
                        foreach ($option_values['option_value_description'] as $k_v_lang => &$it_v_desc) {
                            $fkc_key = array_key_first($it_v_desc);
                            $it_v_desc = array_reverse($it_v_desc, true);
                            $fuck = array_pop($it_v_desc);
                            $it_v_desc = array_reverse($it_v_desc, true);

                            $insert_new_desc[] = ['option_value_id' => $id_return, 'option_id' => $option_id, 'language_id' => $k_v_lang, 'name' => $fuck];
                        }

                        $this->optionValueDescRepository->getModel()->insert($insert_new_desc);
                    }
                }
            }


            if (!empty($updates)) {
                $this->repository->updateMultiple($updates);
            }

            if (!empty($update_desc)) {
                $this->optionValueDescRepository->updateMultiple($update_desc);
            }

            if (!empty($insert_desc)) {
                $this->optionValueDescRepository->getModel()->insert($insert_desc);
            }

            if (isset($arr_not_delete) && !empty($arr_not_delete)) {
                $this->repository->getModel()->where('option_id', $option_id)->whereNotIn('id', $arr_not_delete)->delete();
                $this->optionValueDescRepository->getModel()->where('option_id', $option_id)->whereNotIn('option_value_id', $arr_not_delete)->delete();
            }
        }else {
            $this->repository->getModel()->where('option_id', $option_id)->delete();
            $this->optionValueDescRepository->getModel()->where('option_id', $option_id)->delete();
        }
    }

    public function image($request, $imageField, $title, $folder_upload)
    {
        $errorMsg = null;
        $fname = null;

        $file = $request->$imageField;

        $allowedFileExtension = ['jpeg', 'jpg', 'png', 'gif'];
        $extension = $file->getClientOriginalExtension();

        if (in_array($extension, $allowedFileExtension) && $file->isValid()) {
            $fname = ImageURL::makeFileName(!empty($prefix) ? $prefix : $file->getClientOriginalName(), $extension);

            if (!$fname || empty($fname)) {
                $errorMsg = 'Upload ảnh lên server thất bại!';
            } else {
                /* $image = ImageURL::upload($folder_upload, $fname, $folder_upload);*/
                $storeResult = ImageURL::upload($request->$imageField, $fname, $folder_upload);
            }

            if (!$storeResult) {
                $errorMsg = 'Upload file lên server thất bại!';
            }
        } else {
            $errorMsg = 'Upload file lên server sai định dạng!';
        }

        return ['error' => !empty($errorMsg), 'msg' => @$errorMsg ?: 'Success', 'fileName' => $fname];
    }

}
