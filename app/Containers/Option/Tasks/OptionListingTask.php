<?php

namespace App\Containers\Option\Tasks;

use App\Containers\Option\Data\Repositories\OptionRepository;
use App\Ship\Criterias\Eloquent\ThisOperationThatCriteria;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class OptionListingTask.
 */
class OptionListingTask extends Task
{

    protected $repository;

    public function __construct(OptionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Bắt buộc Conds phải đủ 3 tham số: field, operation, value cho điều kiện
     * @return  mixed
     */
    public function run($filters = [],$conds = [], $limit = 20,$defaultLanguage = null, $noPaginate = false, $external_data = [])
    {
        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;
        $this->repository->pushCriteria(new ThisOperationThatCriteria('status',-1,'>'));

        $this->repository->with(['desc' => function ($query) use($language_id) {
            $query->activeLang($language_id);
        }]);

        if (!empty($conds)) {
            foreach ($conds as $item) {
                $this->repository->pushCriteria(new ThisOperationThatCriteria($item[0], $item[2], $item[1]));
            }
        }

        if (isset($filters['name']) && $filters['name']) {
            $this->repository->whereHas('desc', function (Builder $query) use ($filters) {
                $query->where('name', 'like', '%' . $filters['name'] . '%');
            });
        }

        $this->repository->orderBy('sort_order');

        return $noPaginate ? $this->repository->all() : $this->repository->paginate($limit);
    }
}
