<?php

namespace App\Containers\Option\Tasks;

use App\Containers\Option\Data\Repositories\OptionDescRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class DeleteOptionDescTask.
 */
class DeleteOptionDescTask extends Task
{

    protected $repository;

    public function __construct(OptionDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @return  mixed
     */
    public function run($option_id)
    {
        try {
            $this->repository->getModel()->where('option_id', $option_id)->delete();
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
