<?php

namespace App\Containers\Option\Tasks;

use App\Containers\Option\Data\Repositories\OptionRepository;
use App\Ship\Criterias\Eloquent\OrderByFieldCriteria;
use App\Ship\Parents\Tasks\Task;

class GetAllOptionsTask extends Task
{
    protected $repository;

    public function __construct(OptionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->all();
    }

    public function orderBy($orderBy)
    {
        $this->repository->pushCriteria(new OrderByFieldCriteria('sort_order', $orderBy));
        return $this;
    }

    // public function with(array $with = []): self
    // {
    //     $this->repository->pushCriteria(new WithCriteria($with));
    //     return $this;
    // }
}
