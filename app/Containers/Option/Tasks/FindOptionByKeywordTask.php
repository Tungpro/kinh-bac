<?php

namespace App\Containers\Option\Tasks;

use App\Containers\Option\Data\Repositories\OptionRepository;
use App\Ship\Criterias\Eloquent\WithCriteria;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Database\Eloquent\Collection;

class FindOptionByKeywordTask extends Task
{

  protected $repository;

  public function __construct(OptionRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run(string $name = ''): Collection
  {
    try {
      $option = $this->repository->where('draft_name', 'LIKE', '%' . $name . '%')->get();
      return $option;
    } catch (Exception $exception) {
      throw $exception;
    }
  }

  public function with(array $with=[])
  {
    $this->repository->pushCriteria(new WithCriteria($with));
  }
}
