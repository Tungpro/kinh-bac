<?php

namespace App\Containers\Option\Tasks;

use App\Containers\Option\Data\Repositories\OptionDescRepository;
use App\Containers\Option\Data\Repositories\OptionRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class SaveOptionDescTask.
 */
class SaveOptionDescTask extends Task
{

    protected $repository;

    public function __construct(OptionDescRepository $repository, OptionRepository $repositoryOption)
    {
        $this->repository = $repository;
        $this->repositoryOption = $repositoryOption;
    }

    /**
     *
     * @return  mixed
     */
    public function run($data, $original_desc, $option_id, $language_id_default = 1)
    {
        $name = $data['name'];
        if (!empty($name)) {
            $updates = [];
            $inserts = [];
            foreach ($name as $k => $v) {
                if($k == $language_id_default){
                    $this->repositoryOption->update(['draft_name' =>  $v], $option_id);
                }
                if (isset($original_desc[$k])) {
                    $updates[$original_desc[$k]['id']] = ['name' => $v];
                } else {
                    $inserts[] = ['option_id' => $option_id, 'language_id' => $k, 'name' => $v];
                }
            }

            if (!empty($updates)) {
                $this->repository->updateMultiple($updates);
            }

            if (!empty($inserts)) {
                $this->repository->getModel()->insert($inserts);
            }
        }
    }
}
