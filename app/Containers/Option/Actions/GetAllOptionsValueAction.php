<?php

namespace App\Containers\Option\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllOptionsValueAction extends Action
{
    public function run(Request $request, array $with=[])
    {
        $optionvalues = Apiato::call('Option@GetAllOptionValuesTask', [], [
          ['with' => [$with]]
        ]);

        return $optionvalues;
    }
}
