<?php

namespace App\Containers\Option\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Transporters\DataTransporter;

class FindOptionByKeywordAction extends Action
{
  public function run(DataTransporter $transporter, array $with = [])
  {
    $options = Apiato::call('Option@FindOptionByKeywordTask', [$transporter->name], [
      [
        'with' => [$with]
      ]
    ]);

    foreach ($options as $option) {
      $option->category = '';
      $option->name = $option->desc->name;
      $option->option_id = $option->id;
      $option->type = 'select';
    }
    return $options;
  }
}
