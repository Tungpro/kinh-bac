<?php

namespace App\Containers\Option\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class FindByOptionIdAction.
 *
 */
class FindByOptionIdAction extends Action
{

    /**
     * @return mixed
     */
    public function run($id)
    {
        $data = Apiato::call(
            'Option@GetOptionByIdTask',
            [
                $id
            ]
        );

        return $data;
    }
}
