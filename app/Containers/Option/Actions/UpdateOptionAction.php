<?php

namespace App\Containers\Option\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class UpdateOptionAction.
 *
 */
class UpdateOptionAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        $option = Apiato::call(
            'Option@SaveOptionTask',
            [
                $data
            ]
        );

        if($option) {
            $original_desc = Apiato::call('Option@GetAllOptionDescTask', [$data->id]);

            Apiato::call('Option@SaveOptionDescTask',
                [
                    $data,
                    $original_desc,
                    $data->id
                ]
            );

            Apiato::call('Option@SaveOptionValueDescTask',[
                $data,
                $data->id
            ]);
        }
        return $option;
    }
}
