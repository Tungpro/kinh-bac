<?php

namespace App\Containers\Option\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllOptionsAction extends Action
{
    public function run(Request $request, array $with = [], $orderBy = 'ASC', $selectFields = ['*'])
    {
        return Apiato::call('Option@GetAllOptionsTask', [], [
            'addRequestCriteria',
            ['with' => [$with]],
            ['orderBy' => [$orderBy]],
            ['selectFields' => [$selectFields]],
        ]);
    }
}
