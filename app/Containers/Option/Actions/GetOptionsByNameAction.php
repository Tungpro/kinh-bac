<?php

namespace App\Containers\Option\Actions;

use App\Ship\Parents\Actions\Action;

/**
 * Class GetOptionsByNameAction.
 *
 */
class GetOptionsByNameAction extends Action
{

    /**
     * @return mixed
     */
    public function run(string $name = '')
    {
        $filter = ['name' => $name];

        $options = $this->call('Option@OptionListingTask',[$filter])->keyBy('id')->toArray();
        // dd($options);
        if (!empty($options)) {
            $option_values = $this->call('Option@GetOptionValuesByIdsTask',[array_keys($options)]);
            // dd($option_values);

            foreach ($option_values as $option_value) {
                if (is_file($option_value['image'])) {
                    $image = '';
                } else {
                    $image = '';
                }
                $options[$option_value['option_id']]['option_value_data'][] = [
                    'option_value_id' => $option_value['option_value_id'],
                    'name'            => strip_tags(html_entity_decode($option_value['name'], ENT_QUOTES, 'UTF-8')),
                    'image'           => $image
                ];
            }
            // dd($options);
            foreach ($options as $k => $option) {
                $type = '';

                if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox') {
                    $type = 'Chooose';
                }

                if ($option['type'] == 'text' || $option['type'] == 'textarea') {
                    $type = 'Input';
                }

                if ($option['type'] == 'file') {
                    $type = 'File';
                }

                if ($option['type'] == 'date' || $option['type'] == 'datetime' || $option['type'] == 'time') {
                    $type = 'Date';
                }
                $return_data[] = array(
                    'option_id'    => $option['id'],
                    'name'         => strip_tags(html_entity_decode($option['desc']['name'], ENT_QUOTES, 'UTF-8')),
                    'category'     => $type,
                    'type'         => $option['type'],
                    'option_value' => @$option['option_value_data'] ?? []
                );
            }
        }

        return isset($return_data) ? $return_data : [];
    }
}
