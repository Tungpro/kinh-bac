<?php

namespace App\Containers\Option\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class DeleteOptionAction.
 *
 */
class DeleteOptionAction extends Action
{

    /**
     * @return mixed
     */
    public function run($option_id)
    {
        try {
            DB::beginTransaction();

            Apiato::call('Option@DeleteOptionTask', [$option_id]);
            Apiato::call('Option@DeleteOptionDescTask', [$option_id]);
            Apiato::call('Option@DeleteOptionValueTask', [$option_id]);
            Apiato::call('Option@DeleteOptionValueDescTask', [$option_id]);
            
            DB::commit();
        }catch(Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
