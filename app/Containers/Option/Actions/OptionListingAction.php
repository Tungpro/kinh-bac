<?php

namespace App\Containers\Option\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class OptionListingAction.
 *
 */
class OptionListingAction extends Action
{

    /**
     * @return mixed
     */
    public function run(DataTransporter $filters,$conds = [], $limit = 20, $noPaginate = false, $external_data = [])
    {
        $data = Apiato::call(
            'Option@OptionListingTask',
            [
                $filters,
                $conds,
                $limit,
                $noPaginate,
                $external_data
            ]
        );

        return $data;
    }
}
