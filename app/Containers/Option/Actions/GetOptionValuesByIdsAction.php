<?php

namespace App\Containers\Option\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class GetOptionValuesByIdsAction.
 *
 */
class GetOptionValuesByIdsAction extends Action
{

    /**
     * @return mixed
     */
    public function run($ids)
    {
        $data = Apiato::call(
            'Option@GetOptionValuesByIdsTask',
            [
                $ids
            ]
        );

        return $data;
    }
}
