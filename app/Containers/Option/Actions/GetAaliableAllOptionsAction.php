<?php

namespace App\Containers\Option\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAaliableAllOptionsAction extends Action
{
    public function run(array $with = [], $orderBy = 'ASC', $selectFields = ['*'])
    {
        return Apiato::call('Option@GetAllOptionsTask', [], [
            'addRequestCriteria',
            ['with' => [$with]],
            ['orderBy' => [$orderBy]],
            ['selectFields' => [$selectFields]],
        ]);
    }
}
