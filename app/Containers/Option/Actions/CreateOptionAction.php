<?php

namespace App\Containers\Option\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class CreateOptionAction.
 *
 */
class CreateOptionAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data)
    {
        $option = Apiato::call(
            'Option@SaveOptionTask',
            [
                $data,
                true // Tạo mới
            ]
        );

        if ($option) {
            Apiato::call(
                'Option@SaveOptionDescTask',
                [
                    $data,
                    [],
                    $option->id
                ]
            );

            Apiato::call('Option@SaveOptionValueDescTask', [
                $data,
                $option->id
            ]);
        }
        return $option;
    }
}
