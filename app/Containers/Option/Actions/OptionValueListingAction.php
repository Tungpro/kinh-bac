<?php

namespace App\Containers\Option\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class OptionValueListingAction.
 *
 */
class OptionValueListingAction extends Action
{

    /**
     * @return mixed
     */
    public function run($option_id)
    {
        $data = Apiato::call(
            'Option@GetValueByOptionIdTask',
            [
                $option_id
            ]
        );

        return $data;
    }
}
