<?php
/**
 * Created by PhpStorm.
 * Filename: OptionValueDesc.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 7/22/20
 * Time: 11:32
 */

namespace App\Containers\Option\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class OptionValueDesc extends Model
{
    protected $table = 'option_value_description';

    protected $fillable = [
        'option_value_id',
        'image',
        'language_id',
        'option_id',
        'name'
    ];

    public function option()
        {
        return $this->hasOne(Option::class,'id','option_id');
    }

    public function option_value() {
            return $this->hasOne(OptionValue::class,'id','option_value_id');
        }

    public function language() {
            return $this->hasOne(Language::class,'language_id','language_id');
    }
}