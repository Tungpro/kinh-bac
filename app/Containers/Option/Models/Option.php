<?php
/**
 * Created by PhpStorm.
 * Filename: Option.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 7/22/20
 * Time: 11:30
 */

namespace App\Containers\Option\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class Option extends Model
{
    protected $table = 'option';

    protected $fillable = [
        'type',
        'language_id',
        'sort_order',
        'name',
        'draft_name',
        'eshop_attr_id',
        'show_image',
    ];

    public function desc()
    {
        return $this->hasOne(OptionDesc::class, 'option_id', 'id');
    }

    public function all_desc()
    {
        return $this->hasMany(OptionDesc::class, 'option_id', 'id');
    }

    public function languages()
    {
        return $this->belongsToMany(Language::class, OptionDesc::getTableName(), 'option_id', 'language_id');
    }

    public function option_value()
    {
        return $this->hasMany(OptionValue::class, 'option_id', 'id');
    }
}
