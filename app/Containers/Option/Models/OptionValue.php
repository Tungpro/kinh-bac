<?php
/**
 * Created by PhpStorm.
 * Filename: OptionValue.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 7/22/20
 * Time: 11:32
 */

namespace App\Containers\Option\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class OptionValue extends Model
{
    protected $table = 'option_value';

    protected $appends = ['name', 'option_value_id'];

    protected $fillable = [
        'option_id',
        'image',
        'image_two',
        'sort_order',
        'status',
        'created_at',
        'eshop_attr_value_id',
        'draft_name'
    ];

    const KEY = 'optionvalue';

    public function desc()
    {
        return $this->hasOne(OptionValueDesc::class, 'option_value_id', 'id');
    }

    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', OptionValueDesc::class, 'option_value_id', 'id');
    }

    public function option()
    {
        return $this->hasOne(Option::class, 'id', 'option_id');
    }

    public function languages()
    {
        return $this->belongsToMany(Language::class, OptionValueDesc::getTableName(), 'option_value_id', 'language_id');
    }

    public function getNameAttribute()
    {
        return $this->draft_name;
    }

    public function getOptionValueIdAttribute()
    {
        return $this->id;
    }

    public function getImageUrl($size = 'original')
    {
        return ImageURL::getImageUrl($this->image_two, self::KEY, $size);
    }
} // End classs
