<?php
/**
 * Created by PhpStorm.
 * Filename: OptionDesc.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 7/22/20
 * Time: 11:31
 */

namespace App\Containers\Option\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class OptionDesc extends Model
{
    protected $table = 'option_description';

    protected $fillable = [
        'option_id',
        'sort_order',
        'status',
        'created_at'
    ];

    public function attribute()
    {
        return $this->hasOne(Option::class,'id','option_id');
    }

    public function language() {
        return $this->hasOne(Language::class,'language_id','language_id');
    }
}