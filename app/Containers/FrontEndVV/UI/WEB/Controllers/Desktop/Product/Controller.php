<?php

namespace App\Containers\FrontEndVV\UI\WEB\Controllers\Desktop\Product;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseFrontEndController;
use App\Containers\FrontEnd\UI\WEB\Controllers\Features\Product\CategoryWithinProducts;
use App\Containers\FrontEnd\UI\WEB\Controllers\Features\Product\DetailProduct;
use App\Containers\FrontEnd\UI\WEB\Controllers\Features\Product\SearchProduct;

class Controller extends BaseFrontEndController
{
    private $limitCategory = 3;
    private $limitProduct = 12;

    use CategoryWithinProducts;
    use DetailProduct;
    use SearchProduct;

    public function productPage()
    {
        $bannerTop = Apiato::call('Banner@GetAvailableBannerByPositionAction', [
            ['san_pham_top'], $this->isMobile, [], ['sort_order' => 'ASC', 'id' => 'DESC'], $this->currentLang, 0
        ]);

        $productCategoryWithProduct = Apiato::call('Category@FrontEnd\GetCategoriesWithProductsAction', [$this->currentLang, $this->limitCategory, $this->limitProduct]);

        $this->frontBreadcrumb(__('site.sanpham'), '#', true);
        $this->generateMetaTag(null, __('site.sanpham'));

        return $this->returnView('frontendvv', 'product.san-pham-theo-danh-muc', [
            'bannerTop' => $bannerTop->first(),
            'productCategoryWithProduct' => $productCategoryWithProduct,
            'strStyle' => 'background:#BDBDBD',
        ]);
    }

}
