<?php

namespace App\Containers\FrontEndVV\UI\WEB\Controllers\Desktop\Contact;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseFrontEndController;

class Controller extends BaseFrontEndController
{
    public function contactPage()
    {
        $bannerContacts = Apiato::call('Banner@GetAvailableBannerByPositionAction', [
            ['contact_slide'], $this->isMobile, [], ['sort_order' => 'ASC', 'id' => 'DESC'], $this->currentLang, 0
        ]);

        view()->share('extraCoverClasses', 'contact-site');

        $this->generateMetaTag(null, __('site.lienhe'));


        return $this->returnView('frontendvv', 'contact.index', [
            'bannerContacts' => $bannerContacts,
        ]);
    }

}
