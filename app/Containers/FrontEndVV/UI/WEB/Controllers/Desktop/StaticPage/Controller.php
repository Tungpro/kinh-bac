<?php

namespace App\Containers\FrontEndVV\UI\WEB\Controllers\Desktop\StaticPage;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseFrontEndController;
use App\Containers\StaticPage\Models\StaticPage;

class Controller extends BaseFrontEndController
{
    public function detailPage($slug, $id)
    {
        if (!empty($slug) && !empty($id)) {

            $page = Apiato::call('StaticPage@FrontEnd\GetAvailablePageByIdAction', [
                $id, $this->currentLang, [], ['id' => 'DESC'], [['status', '=', 2]]
            ]);
//            dd($page->toArray());

            if ($page) {
                view()->share('extraCoverClasses', 'news-detail-site no-banner');
                $this->generateMetaTag($page);
                $this->frontBreadcrumb($page->desc->name, '#', true);


                return $this->returnView('frontendvv', 'staticpage.detail', [
                    'page' => $page,
                ]);
            }
        }

        abort(404);
    }
}
