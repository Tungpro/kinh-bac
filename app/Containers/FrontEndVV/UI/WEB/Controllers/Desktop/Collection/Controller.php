<?php

namespace App\Containers\FrontEndVV\UI\WEB\Controllers\Desktop\Collection;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\BaseContainer\UI\WEB\Controllers\BaseFrontEndController;
use App\Containers\Collection\Actions\FrontEnd\CollectionListingAction;
use App\Containers\Collection\UI\WEB\Requests\GetAllCollectionsRequest;

class Controller extends BaseFrontEndController
{
    public function index(GetAllCollectionsRequest $request)
    {
        $banners = Apiato::call('Banner@GetAvailableBannerByPositionAction', [
            ['bo_suu_tap_top', 'bo_suu_tap_bot'], $this->isMobile, [], ['sort_order' => 'ASC', 'id' => 'DESC'], $this->currentLang, 0
        ]);
        $bannerTop = $banners->filter(function ($item) {
            return str_contains($item->position, 'bo_suu_tap_top');
        })->first();
        $bannerBottom = $banners->filter(function ($item) {
            return str_contains($item->position, 'bo_suu_tap_bot');
        })->first();

        $collections = app(CollectionListingAction::class)->skipCache(true)->run([], [], $this->currentLang, true, 3, [['status', '=', 1]], ['sort_order' => 'ASC', 'id' => 'DESC']);

        $this->frontBreadcrumb(__('site.bosuutap'), '#', true);
        $this->generateMetaTag(null, __('site.bosuutap'));

        return $this->returnView('frontendvv', 'collection.bo-suu-tap', [
            'bannerTop' => $bannerTop,
            'bannerBottom' => $bannerBottom,
            'collections' => $collections,
        ]);
    }

    public function detail($bst_slug, $bst_id)
    {
        $collection = Apiato::call('Collection@FrontEnd\FindCollectionByIdAction', [
            $bst_id,
            [
                'images:id,object_id,image',
                /*'products:id,image,price,global_price',
                'products.desc' => function ($query) {
                    $query->where('language_id', $this->currentLang->language_id);
                }*/
            ],
            $this->currentLang,
            [['status', '=', 1]]
        ]);

        if ($collection) {
            $this->frontBreadcrumb(__('site.bosuutap'), route('web.bo-suu-tap.index'));
            $this->frontBreadcrumb($collection->desc->name, '#', true);
            $this->generateMetaTag($collection);
            view()->share('extraCoverClasses', 'collection-breakout');

            $otherCollections = app(CollectionListingAction::class)->skipCache(true)->run(
                [], [], $this->currentLang, true, 2, [['id', '!=', $collection->id], ['status', '=', 1]], 'inRandomOrder'
            );

            return $this->returnView('frontendvv', 'collection.bo-suu-tap-detail', [
                'collection' => $collection,
                'otherCollections' => $otherCollections,
                'strCategoryIds' => $collection->category_ids,
                'skeletonNumber' => 9,
            ]);
        }

        return redirect()->route('web_home_page');
    }
}
