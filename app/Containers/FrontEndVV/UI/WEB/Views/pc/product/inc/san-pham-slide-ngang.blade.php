@if($productInBlock && $productInBlock->isNotEmpty())
    <div class="product-{{$blockClass ?? 'viewed'}}">
        <h3 class="title-primary title-icon-black has-border">
            <img src="{{asset('template/images/title-icon-black.png')}}">{{$blockTitle}}
        </h3>
        <div class="product-chung-wrapper">
            <div class="product-{{$blockClass ?? 'viewed'}}-list position-relative">
                <div class="product-{{$blockClass ?? 'viewed'}}-swiper swiper">
                    <div class="swiper-wrapper">
                        @foreach($productInBlock as $productB)
                            <div class="swiper-slide product__list--main">
                                <div class="product-component">
                                    <a href="{{$productB->routeProductDetail()}}" class="image">
                                        <img src="{{$productB->getImageUrl('mediumx3')}}"
                                             alt="{{$productB->desc->name}}"></a>
                                    @if(!empty($productB->discount))
                                        <div class="discount">{{$productB->discount}}</div>
                                    @endif
                                    <div class="desc">
                                        <div class="desc-data">
                                            <div class="name">
                                                <a href="{{$productB->routeProductDetail()}}">{{$productB->desc->name}}</a>
                                            </div>
                                            <div class="price">{{$productB->price_format(false)}}</div>
                                            <div class="price-old">{{$productB->price_format(true)}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-button-prev"><img src="{{asset('template/images/cc1.svg')}}"></div>
                <div class="swiper-button-next"><img src="{{asset('template/images/cc2.svg')}}"></div>
            </div>
            <div class="text-center">
                <a href="{{$viewAllHref ?? '#'}}"
                   class="button-main color-black border-black text-uppercase">{{__('site.xemtatca')}}</a>
            </div>
        </div>
    </div>
@endif