@extends('basecontainer::frontend.pc.layouts.home')

@section('content')
    <section class="search-resutl mb-5">
        <section class="banner banner__inrtroduce banner-section"
                 style="background-image: url('{{$bannerTop->getImageUrl('large')}}');">
            <div class="banner__slogan">
                <h2>{{$bannerTop->desc->name}}</h2>
            </div>
        </section>
        <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>

        <div class="container">
            <div class="search-result-wrapper">
                <h1>{{!empty($searchEmptyMsg) ? $searchEmptyMsg : __('site.searchempty')}}</h1>
            </div>
        </div>
    </section>
@endsection