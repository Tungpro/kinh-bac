@if($productInBlock && $productInBlock->isNotEmpty())
    <div class="product-related">
        <h3>{{$blockTitle}}</h3>
        <div class="list">
            @foreach($productInBlock as $productB)
                <div class="list-item">
                    <a href="{{$productB->routeProductDetail()}}" class="image">
                        <img src="{{$productB->getImageUrl('mediumx3')}}" alt="{{$productB->desc->name}}"></a>
                    <div class="desc">
                        <div class="name"><a href="{{$productB->routeProductDetail()}}">{{$productB->desc->name}}</a>
                        </div>
                        <div class="price-wrap">
                            @if(!empty($productB->discount))
                                <div class="discount">{{$productB->discount}}</div>
                            @endif
                            <div class="price">{{$productB->price_format(false)}}
                                <span>{{$productB->price_format(true)}}</span></div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="text-center">
            <a href="{{$viewAllHref ?? '#'}}"
               class="button-main color-black border-black text-uppercase">{{__('site.xemtatca')}}</a>
        </div>
    </div>
@endif