@extends('basecontainer::frontend.pc.layouts.home')

@section('content')
    <main>
        <div class="product-site">
            @if($bannerTop)
                <section class="banner banner__collection banner-section"
                         style="background-image: url('{{$bannerTop->getImageUrl('large')}}');">
                    <div class="banner__slogan">
                        <h2>{{$bannerTop->desc->name}}</h2>
                        <div class="desciption">{!! $bannerTop->desc->short_description !!}</div>
                    </div>
                </section>
            @endif

            <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>

            @if(isset($productCategoryWithProduct) && $productCategoryWithProduct->isNotEmpty())
                <div class="product-wrapper">
                    @foreach($productCategoryWithProduct as $catWithProduct)
                        <div class="product-wrapper--general glasses">
                            <div class="image-big">
                                <img src="{{$catWithProduct->getImageUrl('banner')}}"
                                     alt="{{$catWithProduct->desc->name}}">
                                <div class="content">
                                    <h2><img src="{{asset('template/images/title-icon-white.svg')}}" alt=""> <a
                                            href="{{$catWithProduct->routeCategoryDetail()}}">{{$catWithProduct->desc->name}}</a>
                                    </h2>
                                    <a href="{{$catWithProduct->routeCategoryDetail()}}"
                                       class="button-main color-cam border-cam">{{__('site.xemtatca')}}</a>
                                </div>
                            </div>
                            @if(isset($catWithProduct->products) && $catWithProduct->products->isNotEmpty())
                                <div class="glasses-list product-type product__list--main">
                                    <div class="glasses-swiper swiper">
                                        <div class="swiper-wrapper">
                                            @foreach($catWithProduct->products as $product)
                                                <div class="swiper-slide">
                                                    <div class="product-component">
                                                        <a href="{{$product->routeProductDetail()}}" class="image"><img
                                                                src="{{$product->getImageUrl('mediumx3')}}"
                                                                alt="{{$product->desc->name}}"></a>
                                                        @if(!empty($product->discount))
                                                            <div class="discount">{{$product->discount}}</div>
                                                        @endif
                                                        <div class="desc">
                                                            <div class="desc-data">
                                                                <div class="name"><a
                                                                        href="{{$product->routeProductDetail()}}">{{$product->desc->name}}</a>
                                                                </div>
                                                                <div
                                                                    class="price">{{$product->price_format(false)}}</div>
                                                                <div
                                                                    class="price-old">{{$product->price_format(true)}}</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="swiper-naigation d-flex align-items-center">
                                            <div class="swiper-button-prev"></div>
                                            <div class="swiper-button-next"></div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            @endif

            <x-partner-home :strStyle="$strStyle"></x-partner-home>
        </div>
    </main>
@endsection