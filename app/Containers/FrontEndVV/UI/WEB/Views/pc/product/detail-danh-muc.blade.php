@extends('basecontainer::frontend.pc.layouts.home')

@section('content')
    <main>
        <section class="banner banner__collection banner-section"
                 style="background-image: url('{{\Apiato\Core\Foundation\ImageURL::getImageUrl($currentCategory['image'], 'category', 'large')}}');">
            <div class="banner__slogan">
                <h2>{{$currentCategory['desc']['name']}}</h2>
                <div class="desciption">{!! $currentCategory['desc']['description'] !!}</div>
            </div>
        </section>

        <div id="box-product-listing">
            <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>

            @include('frontendvv::pc.collection.inc.product-with-filter', ['skeletonNumber' => $skeletonNumber])
        </div>

        @if(isset($otherCategories) && $otherCategories && $otherCategories->isNotEmpty())
            <section class="collection-difference">
                <div class="title-line title-primary text-center">{{__('site.khamphasanphamkhac')}}</div>
                <div class="list">
                    @foreach($otherCategories as $otherCategory)
                        <div class="list-item"
                             style="background-image: url('{{$otherCategory->getImageUrl('banner')}}');">
                            <h3>{{$otherCategory->desc->name}}</h3>
                            <a href="{{$otherCategory->routeCategoryDetail()}}"
                               class="button-main color-black text-uppercase">{{__('site.khamphangay')}}</a>
                        </div>
                    @endforeach
                </div>
            </section>
        @endif
    </main>
@endsection

@push('js_bot_all')
    <script>
        let url_api_listing = "{{ route('api_personalish_listing_product') }}";
        let category_id = "";
        let category_ids = "{{ $currentCategory['category_id'] }}";
        let collection_id = "";
        let p_limit = {{$skeletonNumber}};
    </script>

    {!! FunctionLib::addMedia('template/js/general-setup.js', $settings['other']['version']) !!}
    {!! FunctionLib::addMedia('template/js/pages/base/preload-functions.js', $settings['other']['version']) !!}
    {!! FunctionLib::addMedia('template/js/pages/base/pagination.js', $settings['other']['version']) !!}
    {!! FunctionLib::addMedia('template/js/pages/product/category/category.js', $settings['other']['version']) !!}
@endpush