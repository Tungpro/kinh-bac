@extends('basecontainer::frontend.pc.layouts.home')

@section('content')
    <main>
        <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>

        <section class="product-detail--wrapper">
            <div class="sync-slide">
                <div id="sync-product-detail-1" class="owl-carousel owl-theme">
                    @if(!empty($product->video_link))
                        <div class="item-video">
                            {!! $product->video_link !!}
                        </div>
                    @endif
                    @if(!empty($product->image))
                        <div class="item item-image">
                            <img src="{{ $product->getImageUrl('social') }}" alt="{{ $product->desc->name }}">
                        </div>
                    @endif
                    @if(isset($product->images) && $product->images->isNotEmpty())
                        @foreach ($product->images as $image)
                            <div class="item item-image">
                                <img src="{{ $image->getImageUrl('social') }}" alt="{{ $product->desc->name }}">
                            </div>
                        @endforeach
                    @endif
                </div>
                <div id="sync-product-detail-2" class="owl-carousel owl-theme">
                    @if(!empty($product->video_link))
                        <div class="item item-video">
                            <img src="{{ $product->getImageUrl('social') }}" alt="{{ $product->desc->name }}">
                        </div>
                    @endif
                    @if(!empty($product->image))
                        <div class="item item-image">
                            <img src="{{ $product->getImageUrl('social') }}" alt="{{ $product->desc->name }}">
                        </div>
                    @endif
                    @if(isset($product->images) && $product->images->isNotEmpty())
                        @foreach ($product->images as $image)
                            <div class="item item-image">
                                <img src="{{ $image->getImageUrl('social') }}" alt="{{ $product->desc->name }}">
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="product-detail--infomation" id="product-detail-top-info">
                <div class="wrapper">
                    <h3 class="name">{{ $product->desc->name }}</h3>
                    <div class="code">{{__('site.masanpham')}}: <span
                                v-text="(selectedVariant && selectedVariant.sku) ? selectedVariant.sku : '{{ $product->sku ?? '--' }}'"></span>
                    </div>
                    <div class="price">{{$product->price_format(false)}} <span>{{$product->price_format(true)}}</span>
                    </div>

                    <div class="product-variant-box" v-if="options_unique">
                        <div class="color" v-for="(option, option_index) in options_unique" :key="option_index">
                            <h4 v-html="option.name"></h4>
                            <ul class="color-list">
                                <li v-if="option.product_option_values"
                                    v-for="(opt_val, opt_val_index) in option.product_option_values.data"
                                    :key="opt_val_index">
                                    <label :class="opt_val.disabled ? 'disable' : ''"
                                           :for="'option-value-' + opt_val.option_value_id">
                                        <input type="radio"
                                               :id="'option-value-' + opt_val.option_value_id"
                                               :name="'option-' + option.option_id"
                                               :checked="opt_val.checked"
                                               :disabled="opt_val.disabled"
                                               @click="selectedItem(option.option_id,opt_val)">
                                        <span class="checkmark"
                                              :style="{background: opt_val.color, color: option.show_color}"></span>
                                        <span class="name" v-html="opt_val.name"></span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>{{--product-variant-box --}}

                    <p v-show="message.add_cart_failure !== ''" v-html="message.add_cart_failure"
                       class="static-text text-danger color" style="display:none"></p>

                    <div class="form">
                        <input type="text" class="form-control" placeholder="{{__('site.nhapsodienthoaicuaban')}}"
                               v-model="data_to_cart.phone">
                        <button type="button" class="btn-form-send" :disabled="isSavingPContact"
                                @click="submitProductVariant">{{__('site.gui')}} <i class="ri-send-plane-fill"></i>
                        </button>
                    </div>

                    <div class="description">
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <button class="card-header-link" type="button" data-toggle="collapse"
                                            data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        {{__('site.motasanpham')}}<span> <i class="ri-add-line"></i></span>
                                    </button>
                                </div>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                     data-parent="#accordionExample">
                                    <div class="card-body">{!! $product->desc->product_description !!}</div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <button class="card-header-link collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseTwo" aria-expanded="false"
                                            aria-controls="collapseTwo">
                                        {{__('site.chinhsachbaohanh')}}<span> <i class="ri-add-line"></i></span>
                                    </button>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                     data-parent="#accordionExample">
                                    <div class="card-body">{!! $product->desc->policy !!}</div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="card-header-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseThree" aria-expanded="false"
                                                aria-controls="collapseThree">
                                            {{__('site.dacquyenkhimuasanpham')}}<span><i class="ri-add-line"></i></span>
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                     data-parent="#accordionExample">
                                    <div class="card-body">{!! $product->desc->privilege !!}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>{{--product-detail-top-info --}}
        </section>

        <section class="product-detail--content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-xl-8">
                        <h3 class="title-primary color-black text-uppercase">
                            <img src="{{asset('template/images/title-icon-black.png')}}">{{__('site.chitietsanpham')}}
                        </h3>
                        <div class="product-description">{!! $product->desc->description !!}</div>
                    </div>
                    <div class="col-lg-5 col-xl-4">
                        @include('frontendvv::pc.product.inc.san-pham-list-doc', ['productInBlock' => $product->similar, 'blockTitle' => __('site.sanphamlienquan'), 'viewAllHref' => route('web.product.cat-with-product')])
                    </div>
                </div>
            </div>
        </section>

        <section class="product-chung">
            <div class="container">
                @include('frontendvv::pc.product.inc.san-pham-slide-ngang', ['productInBlock' => $viewedProducts, 'blockClass' => 'viewed', 'blockTitle' => __('site.sanphamdaxem'), 'viewAllHref' => route('web.product.cat-with-product')])

                @include('frontendvv::pc.product.inc.san-pham-slide-ngang', ['productInBlock' => $product->suggests, 'blockClass' => 'ghim', 'blockTitle' => __('site.sanphammuakem'), 'viewAllHref' => route('web.product.cat-with-product')])
            </div>
        </section>
    </main>
@endsection

@push('js_bot_all')
    <script>
        let url_api_option = "{{ route('api_product_detail_get_all_options') }}",
            url_api_variants = "{{ route('api_product_detail_get_all_variants') }}",
            url_api_product_infor = "{{ route('api_product_detail', ['id' => $product->id]) }}",
            url_api_add_to_cart = "{{route('api_itended_cart_add')}}",
            url_api_add_to_contact = "{{route('home.contact.store')}}",
            product_id = "{{ $product->id }}";
    </script>

    {!! FunctionLib::addMedia('template/js/general-setup.js', $settings['other']['version']) !!}
    {!! FunctionLib::addMedia('template/js/pages/base/preload-functions.js', $settings['other']['version']) !!}
    {!! FunctionLib::addMedia('template/js/pages/product/detail/modules/data-types.js', $settings['other']['version']) !!}
    {!! FunctionLib::addMedia('template/js/pages/product/detail/top-detail.js', $settings['other']['version']) !!}
@endpush