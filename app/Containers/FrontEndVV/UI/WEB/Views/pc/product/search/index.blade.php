@extends('basecontainer::frontend.pc.layouts.home')

@section('content')
    <section class="search-resutl">
        <section class="banner banner__inrtroduce banner-section"
                 style="background-image: url('{{$bannerTop->getImageUrl('large')}}');">
            <div class="banner__slogan">
                <h2>{{$bannerTop->desc->name}}</h2>
            </div>
        </section>
        <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>
    </section>

    <div id="box-product-listing">
        @include('frontendvv::pc.collection.inc.product-with-filter', ['skeletonNumber' => $skeletonNumber])
    </div>
@endsection

@push('js_bot_all')
    <script>
        let url_api_listing = "{{ route('api_personalish_listing_product') }}";
        let category_id = "";
        let category_ids = "{{ $strCategoryIds ?? '' }}";
        let collection_id = "";
        let p_limit = {{$skeletonNumber}};
    </script>

    {!! FunctionLib::addMedia('template/js/general-setup.js', $settings['other']['version']) !!}
    {!! FunctionLib::addMedia('template/js/pages/base/preload-functions.js', $settings['other']['version']) !!}
    {!! FunctionLib::addMedia('template/js/pages/base/pagination.js', $settings['other']['version']) !!}
    {!! FunctionLib::addMedia('template/js/pages/product/category/category.js', $settings['other']['version']) !!}
@endpush