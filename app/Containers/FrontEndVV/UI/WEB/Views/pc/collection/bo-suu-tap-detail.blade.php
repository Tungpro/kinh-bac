@extends('basecontainer::frontend.pc.layouts.home')

@section('content')
    <main>
        <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>

        <section class="collection-description">
            <div class="container">
                <h3 class="text-uppercase">
                    <span>{{__('site.bosuutap')}} </span>{{$collection->desc->name}}</h3>
                <div class="desc">{{$collection->desc->short_description}}</div>
                <div class="desc">{!! $collection->desc->description !!}</div>
            </div>
        </section>

        @if(!empty($collection->image) || ($collection && $collection->images->isNotEmpty()))
            <section class="gallery">
                @if(!empty($collection->image))
                    <a class="gallery-img" href="{{$collection->getImageUrl('largex2')}}" data-fancybox="images">
                        <img src="{{$collection->getImageUrl('largex2')}}"/>
                    </a>
                @endif
                @if($collection && $collection->images->isNotEmpty())
                    @foreach($collection->images as $collectionImg)
                        <a class="gallery-img" href="{{$collectionImg->getImageUrl('social')}}" data-fancybox="images">
                            <img src="{{$collectionImg->getImageUrl('social')}}"/>
                        </a>
                    @endforeach
                @endif
            </section>
        @endif

        <div id="box-product-listing">
            @include('frontendvv::pc.collection.inc.product-with-filter', ['skeletonNumber' => $skeletonNumber, 'blockTitle' => __('site.sptrongbst')])
        </div>

        @if($otherCollections->isNotEmpty())
            <section class="collection-difference">
                <div class="title-line title-primary text-center">{{__('site.khamphabstkhac')}}</div>
                <div class="list">
                    @foreach($otherCollections as $otherCollection)
                        <div class="list-item"
                             style="background-image: url('{{$otherCollection->getImageUrl('social')}}');">
                            <h3>
                                <span>{{__('site.bosuutap')}}</span> {{$otherCollection->desc->name}}
                            </h3>
                            <a href="{{$otherCollection->routeCollectionDetail()}}"
                               class="button-main color-black text-uppercase">{{__('site.khamphangay')}}</a>
                        </div>
                    @endforeach
                </div>
            </section>
        @endif
    </main>
@endsection

@push('js_bot_all')
    <script>
        let url_api_listing = "{{ route('api_personalish_listing_product') }}";
        let category_id = "";
        let category_ids = "{{ $strCategoryIds ?? '' }}";
        let collection_id = "{{ $collection->id ?? 0 }}";
        let p_limit = {{$skeletonNumber}};
    </script>

    {!! FunctionLib::addMedia('template/js/general-setup.js', $settings['other']['version']) !!}
    {!! FunctionLib::addMedia('template/js/pages/base/preload-functions.js', $settings['other']['version']) !!}
    {!! FunctionLib::addMedia('template/js/pages/base/pagination.js', $settings['other']['version']) !!}
    {!! FunctionLib::addMedia('template/js/pages/product/category/category.js', $settings['other']['version']) !!}
@endpush