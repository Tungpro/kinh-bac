<section class="product-collection" id="category-products">
    @if (isset($searchKey) && !empty($searchKey))
        <div class="search-resutl container">
            <div class="search-result-wrapper mb-0">
                <h1>{!! __('site.searchresult', ['keyword' => $searchKey]) !!}</h1>
            </div>
        </div>
    @endif

    <div class="container">
        @if (!empty($blockTitle))
            <h3 class="title-primary text-center text-uppercase title-line">{{ $blockTitle }}</h3>
        @endif
        <div class="product-container danhmuc-sp-wrap">
            <div class="row row-reverse">
                <div class="col-md-4 col-lg-3">
                    <div class="product-sidebar">
                        <div class="sidebar-sort margin-sidebar"
                             v-show="block_category.sort_options && block_category.sort_options.length > 0">
                            <h3>{{ __('menu::menu.sort') }}</h3>
                            <ul class="list-choose">
                                <li v-for="(sort_item, idx_sort) in block_category.sort_options" :key="idx_sort">
                                    <label class="custom-control custom-radio" :for="'sort_option_' + idx_sort">
                                        <input type="radio" class="custom-control-input" name="sort_option"
                                               :value="sort_item.query_value"
                                               @click="listingProducts('sort_option',sort_item.query_value,false)"
                                               :class="'sort-item' + (sort_item.selected ? ' active' : '')"
                                               :id="'sort_option_' + idx_sort" :checked="!!sort_item.selected">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description"
                                              v-html="sort_item.display_value"></span>
                                    </label>
                                </li>
                            </ul>
                        </div>

                        <div v-for="(filter_group, idx_fg) in block_category.filters" class="for"
                             :key="idx_fg" v-if="filter_group.values.length > 0">
                            <div v-if="filter_group.min || filter_group.max" class="margin36-sidebar sidebar-price">
                                <h3 v-html="filter_group.display_name"></h3>
                                <div class="price-ranger position-relative">
                                    <div class="line">
                                        <div class="line-width" :style="{width: priceFilterWith + '%'}"></div>
                                        <div class="label-group">
                                            <label v-for="(filter_value, idx_val) in filter_group.values"
                                                   :class="filter_value.selected ? 'active' : ''"
                                                   :data-value="filter_value.value_range"
                                                   :key="idx_val"
                                                   :data-ranger="idx_val"
                                                   :for="'price-radio-' + idx_val">
                                                <input type="radio" name="input-check-price"
                                                       :data-price="filter_value.value_range"
                                                       :id="'price-radio-' + idx_val"
                                                       @click="onChangePriceFilter(filter_group, filter_value, idx_val)">
                                                <span class="checkmark"></span>
                                                <span class="text" v-html="filter_value.display_value"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div v-else-if="filter_group.query_name === 'brand'"
                                 class="margin36-sidebar sidebar-brands margin-sidebar">
                                <h3 v-html="filter_group.display_name"></h3>
                                <ul class="list-choose">
                                    <li v-for="(filter_value, idx_val) in filter_group.values" :key="idx_val">
                                        <label class="custom-control custom-checkbox"
                                               :for="filter_group.query_name + '_' + filter_value.query_value">
                                            <input type="checkbox" class="custom-control-input"
                                                   @click="onChangeBrandFilter(filter_group, filter_value)"
                                                   :name="filter_group.query_name"
                                                   :id="filter_group.query_name + '_' + filter_value.query_value"
                                                   :value="filter_value.query_value"
                                                   :class="filter_value.selected ? 'active' : ''"
                                                   :checked="!!filter_value.selected">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description"
                                                  v-html="filter_value.display_value"></span>
                                        </label>
                                    </li>
                                </ul>
                            </div>

                            <div v-else class="margin36-sidebar sidebar-select">
                                <h3 v-html="filter_group.display_name"></h3>
                                <select class="custom-select" :name="filter_group.query_name"
                                        :id="filter_group.query_name"
                                        @change="onChangeSelectFilter(filter_group, $event)">
                                    <option value="" v-text="'{{ __('site.chon') }} ' + filter_group.display_name">
                                    </option>
                                    <option v-for="(filter_value, idx_val) in filter_group.values"
                                            :class="filter_value.selected ? 'active' : ''"
                                            v-html="filter_value.display_value" :value="filter_value.query_value"
                                            :key="idx_val" :data_key="idx_val" :selected="!!filter_value.selected">
                                    </option>
                                </select>
                            </div>
                        </div> {{-- for filter --}}

                        <div class="filter-item">
                            <a data-href="{{ url()->current() }}"
                               @if (isset($searchKey) && !empty($searchKey))
                               @click="listingProducts('q','{{$searchKey}}',true,true)"
                               @else
                               @click="listingProducts('','',false,true)"
                               @endif
                               class="button-main color-black border-black w-100">{{ __('site.xoatatca') }}</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-8 col-lg-9">
                    <div class="product__list--main">
                        <div
                                v-if="isLoading || (!isLoading && (!block_category.products || block_category.products.length == 0) )">
                            <div style="display:none" class="row"
                                 v-show="(!isLoading && (!block_category.products || block_category.products.length == 0) )">
                                <div class="col-md-12 col-lg-12">
                                    <div class="p-4 product-component">
                                        {{ __('site.khongtimthaysanpham') }}
                                    </div>
                                </div>
                            </div>
                            <div v-show="!(!isLoading && (!block_category.products || block_category.products.length == 0) )"
                                 class="row">
                                @include('frontendvv::pc.collection.inc.product-list-skeleton', ['number' =>
                                ($skeletonNumber ?? 9)])
                            </div>
                        </div>

                        <div v-else>
                            <div v-if="(block_category.products && typeof block_category.products != 'undefined') ? true : (isLoading = true)"
                                 class="row">
                                <div v-for="(product,idx_product) in block_category.products" class="col-md-6 col-lg-4"
                                     :id="'product-'+product.product_id" :key="idx_product">
                                    <div class="product-component">
                                        <a :href="product.product_url" class="image">
                                            <img :src="product.product_image" :alt="product.name"/>
                                        </a>
                                        <div class="discount" v-if="product.discount > 0">
                                            <span v-text="'-' + product.discount+'%'">{{-- -15% --}}</span>
                                        </div>
                                        <div class="desc">
                                            <div class="desc-data">
                                                <div class="name">
                                                    <a :href="product.product_url" v-html="product.name"></a>
                                                </div>
                                                <div class="price" v-text="product.price_formated+ ' VNĐ'">
                                                    {{-- 450.000 VNĐ --}}
                                                </div>
                                                <div class="price-old" v-if="product.old_price > 0"
                                                     v-text="product.old_price_formated + ' VNĐ'">
                                                    {{-- 800.000 VNĐ --}}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <nav aria-label="Page navigation"
                             v-show="block_category.pagination && block_category.pagination.total_pages > 1">
                            <ul class="pagination" id="pagination">
                                {{-- html here --}}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
