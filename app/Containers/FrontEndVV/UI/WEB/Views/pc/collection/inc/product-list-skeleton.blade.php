@for ($i = 0; $i < $number; $i++)
    <div class="col-md-6 col-lg-4">
        <div class="product-component">
            <a class="image"></a>
            <div class="desc">
                <div class="desc-data">
                    <div class="name"><a></a></div>
                    <div class="price"></div>
                    <div class="price-old"></div>
                </div>
            </div>
        </div>
    </div>
@endfor