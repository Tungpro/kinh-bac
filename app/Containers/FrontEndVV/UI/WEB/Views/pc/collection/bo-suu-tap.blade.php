@extends('basecontainer::frontend.pc.layouts.home')

@section('content')
    <main>
        <div class="collection-site">
            @if($bannerTop)
                <section class="banner banner__collection banner-section"
                         style="background-image: url('{{$bannerTop->getImageUrl('large')}}');">
                    <div class="banner__slogan">
                        <h2>{{$bannerTop->desc->name}}</h2>
                        <div class="desciption">{!! $bannerTop->desc->short_description !!}</div>
                    </div>
                </section>
            @endif

            <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>

            @if($collections)
                <section class="description-section description-reverse">
                    <div class="container">
                        <div class="description-list">
                            @foreach($collections as $collection)
                                <div class="description-item">
                                    <div class="description-image">
                                        <img src="{{$collection->getImageUrl('social')}}"
                                             alt="{{$collection->desc->name}}">
                                    </div>
                                    <div class="description-content">
                                        <div class="data">
                                            <h3>
                                                <span>{{__('site.bosuutap')}}</span> {{$collection->desc->name}}
                                            </h3>
                                            <p>{{$collection->desc->short_description}}</p>
                                            <a href="{{$collection->routeCollectionDetail()}}"
                                               class="button-main color-black border-black">{{__('site.khamphangay')}}</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </section>
            @endif

            @if($bannerBottom)
                <section class="discovery test"
                         style="background-image: url('{{$bannerBottom->getImageUrl('large')}}');">
                    <div class="discovery-data">
                        <h3>{!! $bannerBottom->desc->short_description !!}</h3>
                        <a href="{{$bannerBottom->getBannerLink()}}"
                           class="button-main color-white border-white text-uppercase">{{$bannerBottom->desc->name}}</a>
                    </div>
                </section>
            @endif
        </div>
    </main>
@endsection
