@extends('basecontainer::frontend.pc.layouts.home')

@section('content')
    @if(!empty($settings['contact']['map_iframe']))
        <section class="banner-map">
            {!! $settings['contact']['map_iframe'] !!}
        </section>
    @endif

    <section class="contact-form" id="page-contact"
             style="background-image: url('{{asset('template/images/contact-bg.png')}}');">
        <h1 class="title-primary text-uppercase text-center color-white">{{@$settings['website']['site_name']}}</h1>
        <div class="container contact-container">
            <div class="form-info">
                <h3>{{__('site.thongtinlienhe')}}</h3>
                <ul>
                    <li>
                        <img src="{{asset('template/images/contact-ic-1.png')}}">
                        <span>{{@$settings['website']['address']}}</span>
                    </li>
                    <li>
                        <img src="{{asset('template/images/contact-ic-2.png')}}">
                        <span><a href="tel:{{str_replace(['(', ')', '.', ' '],'',@$settings['contact']['hotline'])}}">{{@$settings['contact']['hotline']}}</a></span>
                    </li>
                    <li>
                        <img src="{{asset('template/images/contact-ic-3.png')}}">
                        <span><a href="mailto:{{@$settings['contact']['email']}}">{{@$settings['contact']['email']}}</a></span>
                    </li>
                    <li>
                        <img src="{{asset('template/images/contact-ic-4.png')}}">
                        <span><a href="//{{@$settings['contact']['other_website']}}"
                                 target="_blank">{{@$settings['contact']['other_website']}}</a></span>
                    </li>
                </ul>
            </div>
            <div class="form-box">
                <h3>{{__('site.dangkyngay')}}</h3>
                <form id="formLienhe">
                    <input type="text" class="form-control" placeholder="{{__('site.hovaten')}}"
                           v-model="info.name">
                    <input type="text" class="form-control" placeholder="{{__('site.email')}}"
                           v-model="info.email">
                    <input type="text" class="form-control" placeholder="{{__('site.sodienthoai')}}"
                           v-model="info.phone">
                    <textarea class="form-control" cols="30" rows="3" placeholder="{{__('site.ghichu')}}"
                              v-model="info.message"></textarea>
                    <button type="button" class="button-register button-main text-uppercase" :disabled="isSaving"
                            @click="saveContact()">{{__('site.dangky')}}</button>
                </form>
            </div>
        </div>
    </section>

    @if(isset($bannerContacts) && $bannerContacts && $bannerContacts->isNotEmpty())
        <section class="contact-slide">
            <div class="contact-swiper swiper">
                <div class="swiper-wrapper">
                    @foreach($bannerContacts as $banner)
                        <div class="swiper-slide">
                            <img src="{{$banner->getImageUrl('large')}}" alt="{{@$banner->desc->name}}">
                        </div>
                    @endforeach
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </section>
    @endif

@endsection

@push('js_bot_all')
    <script> let url_api_save_contact = "{{route('home.contact.store')}}";</script>

    {!! FunctionLib::addMedia('template/js/pages/base/contact.js') !!}
@endpush