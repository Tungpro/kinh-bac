<div class="social">
    <p><strong><i>{{__('site.chiasebaiviet')}}:</i></strong></p>
    <div class="d-flex align-items-center">
        <a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}{{!empty($picture)?"&picture=$picture":""}}{{!empty($title)?"&title=$title":""}}"
           target="_blank"><img src="{{asset('template/images/share-1.png')}}"></a>

        <a href="http://www.facebook.com/dialog/send?app_id={{@$settings['social']['facebook_app_id']}}&link={{url()->current()}}&redirect_uri={{url()->current()}}"
           target="_blank"><img src="{{asset('template/images/share-3.png')}}"></a>

        <a href="https://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}&title={{$title??''}}&summary={{$summary??''}}&source=LinkedIn"
           target="_blank"><img src="{{asset('template/images/share-4.png')}}"></a>
    </div>
</div>