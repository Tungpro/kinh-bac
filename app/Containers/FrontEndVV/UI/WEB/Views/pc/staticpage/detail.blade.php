@extends('basecontainer::frontend.pc.layouts.home')

@section('content')

    <div class="container">
        <x-breadcrumb-component :breadcrumb="$breadcrumb"></x-breadcrumb-component>

        <h2>{{$page->desc->name}}</h2>

        <div class="content">{{$page->desc->short_description}}</div>
        <div class="content">{!! $page->desc->description !!}</div>
        <div class="clearfix mb-5"></div>

    </div>

@endsection
