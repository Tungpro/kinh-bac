<?php

return [

    /*
    |--------------------------------------------------------------------------
    | EShopBizfly Container
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'account' => [
      'email' => env('ESHOP_ACCOUNT_EMAIL', ''),
      'password' => env('ESHOP_ACCOUNT_PASSWORD', ''),
    ],

    'project_token' => env('ESHOP_PROJECT_TOKEN', ''),
    'domain' => env('ESHOP_DOMAIN', ''),
];
