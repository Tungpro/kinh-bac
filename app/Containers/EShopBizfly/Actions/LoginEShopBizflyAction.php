<?php

namespace App\Containers\EShopBizfly\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class LoginEShopBizflyAction extends Action
{
    public function run(Request $request)
    {

        $eshopbizfly = Apiato::call('EShopBizfly@LoginEShopBizflyTask', []);

        return $eshopbizfly;
    }
}
