<?php

namespace App\Containers\EShopBizfly\Actions\Categories;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Localization\Tasks\GetAllLanguageDBTask;
use App\Containers\EShopBizfly\Tasks\Categories\SyncCategoryEShopTask;
use App\Containers\EShopBizfly\Tasks\Categories\GetAllCategoryEShopTask;
use App\Containers\EShopBizfly\Tasks\Categories\SyncCategoryDescEShopTask;
use App\Containers\EShopBizfly\Tasks\Categories\GetCategoryByEShopCatIdTask;
use Exception;
use Illuminate\Database\QueryException;
use Mockery\CountValidator\Exact;

class SyncCategoryEShopAction extends Action
{
    public function run()
    {
        $categoriesEShop = Apiato::call(GetAllCategoryEShopTask::class, []);
        DB::beginTransaction();
        try {
          $categoriesInsertData = Apiato::call(SyncCategoryEShopTask::class, [$categoriesEShop]);
          $langs = Apiato::call(GetAllLanguageDBTask::class);

          $eshopCatIds = Arr::pluck($categoriesInsertData, 'eshop_cate_id');
          $categoriesAfterInsert = Apiato::call(GetCategoryByEShopCatIdTask::class, [$eshopCatIds]);
          $syncResult = Apiato::call(SyncCategoryDescEShopTask::class, [$categoriesAfterInsert, $langs]);
          DB::commit();
        } catch (\Exception $e) {
          DB::rollBack();

          if ($e instanceof QueryException) {
            $errorCode = $e->errorInfo[1];
            if ($errorCode == 1062) {
              throw new Exception("Các bản ghi này đã được đồng bộ", $errorCode);
            }
          }

          throw $e;
        }

        return $syncResult;
    }
}
