<?php

namespace App\Containers\EShopBizfly\Actions\Categories;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class UpdateEShopBizflyAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $eshopbizfly = Apiato::call('EShopBizfly@UpdateEShopBizflyTask', [$request->id, $data]);

        return $eshopbizfly;
    }
}
