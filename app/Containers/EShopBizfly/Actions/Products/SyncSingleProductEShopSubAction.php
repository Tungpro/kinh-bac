<?php

namespace App\Containers\EShopBizfly\Actions\Products;

use App\Ship\Parents\Actions\SubAction;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Database\Eloquent\Collection;

class SyncSingleProductEShopSubAction extends SubAction
{
    public function run(array $productItem=[], Collection $langs)
    {
      try {
        $product = Apiato::call('EShopBizfly@Products\SingleSyncProductTask', [$productItem]);
        $productDesc = Apiato::call('EShopBizfly@Products\SingleSyncProductDescTask', [$product, $langs]);

        // Sync option and option desc
        $optionDBIds = Apiato::call('EShopBizfly@Products\SingleSyncOptionTask', [$productItem, $langs]);

        // Sync Product Option
        $syncProductOption = Apiato::call('EShopBizfly@Products\SingleSyncProductOptionTask', [$product, $optionDBIds]);

        $syncProductOptionValue = Apiato::call('EShopBizfly@Products\SingleSyncProductOptionValueTask', [
          $product,
          $productItem
        ]);
      }catch(\Exception $e) {
        dump("Line: %s | File: %s", $e->getLine(), $e->getFile());
      }
    }
}
