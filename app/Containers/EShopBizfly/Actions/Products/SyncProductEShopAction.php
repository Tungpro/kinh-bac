<?php

namespace App\Containers\EShopBizfly\Actions\Products;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\EShopBizfly\Tasks\Products\GetAllProductEShopAfterInsertDBTask;
use App\Containers\EShopBizfly\Tasks\Products\GetPaginateProductEShopTask;
use App\Containers\EShopBizfly\Tasks\Products\SyncProductEShopTask;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class SyncProductEShopAction extends Action
{
  private static $limit = 100;

  /**
   * Các bước thực hiện đồng bộ
   * 1. GetPaginateProductEShopTask::class -> truy vấn API để get toàn bộ danh sách về
   * 2. SyncProductEShopTask::class -> Insert multipe vào bảng sản phẩm
   * 3. GetAllProductEShopAfterInsertDBTask::class -> Lấy các sản phẩm vừa insert đó ra
   *
   *
   * @return void
   */
  public function run()
  {
    try {
      $currentPage = 1;
      $productsFromEshopAPI = [];
      do {
        $result = Apiato::call('EShopBizfly@Products\GetPaginateProductEShopTask', [$currentPage, self::$limit]);
        if (array_key_exists('data', $productsFromEshopAPI)) {
          $productsFromEshopAPI['data'] = array_merge($productsFromEshopAPI['data'], $result['data']);
        }else {
          $productsFromEshopAPI = $result;
        }

        ++$currentPage;
      }while($currentPage <= $result['meta']['last_page']);

      return $productsFromEshopAPI;
    } catch (\Exception $e) {
      dd($e->getTrace());
    }
  }
}
