<?php

namespace App\Containers\EShopBizfly\Exceptions;

use App\Ship\Parents\Exceptions\Exception;
use Symfony\Component\HttpFoundation\Response;

class ApiEShopException extends Exception
{
    public $httpStatusCode = Response::HTTP_INTERNAL_SERVER_ERROR;

    public $message = 'Call API E-Shop Server Error';

    public $code = 0;
}
