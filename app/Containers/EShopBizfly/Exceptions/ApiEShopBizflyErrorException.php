<?php

namespace App\Containers\EShopBizfly\Exceptions;

use App\Ship\Parents\Exceptions\Exception;
use Symfony\Component\HttpFoundation\Response;

class ApiEShopBizflyErrorException extends Exception
{
    public $httpStatusCode = Response::HTTP_BAD_REQUEST;

    public $message = 'API E-Shop Error';

    public $code = 0;

    public function addCustomData() {
      return [
          'title' => 'nice',
          'description' => 'one fancy description here',
          'foo' => true,
          'meta' => [
              'bar' => 1234,
          ]
      ];
  }
}
