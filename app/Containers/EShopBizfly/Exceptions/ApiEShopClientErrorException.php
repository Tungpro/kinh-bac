<?php

namespace App\Containers\EShopBizfly\Exceptions;

use Throwable;
use App\Ship\Parents\Exceptions\Exception;
use Symfony\Component\HttpFoundation\Response;

class ApiEShopClientErrorException extends Exception
{
    public $httpStatusCode = Response::HTTP_BAD_REQUEST;

    public $message = 'Call API E-Shop Client Error';

    public $code = 0;

    public function report(Throwable $exception) {
      dd(1212);
    }
}
