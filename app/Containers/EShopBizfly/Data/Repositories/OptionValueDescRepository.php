<?php

namespace App\Containers\EShopBizfly\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class OptionValueDescRepository
 */
class OptionValueDescRepository extends EShopBizflyRepository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
