<?php

namespace App\Containers\EShopBizfly\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class CategoryRepository
 */
class CategoryRepository extends EShopBizflyRepository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

    public function insert(array $categoriesInsert=[]): bool {
      $ids = app($this->model())->insert($categoriesInsert);
      return $ids;
    }
} // End class
