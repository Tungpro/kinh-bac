<?php

namespace App\Containers\EShopBizfly\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ProductOptionValueRepository
 */
class ProductOptionValueRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
