<?php

namespace App\Containers\EShopBizfly\Data\Repositories;

use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use App\Ship\Parents\Repositories\Repository;
use App\Containers\EShopBizfly\Exceptions\ApiEShopException;
use App\Containers\EShopBizfly\Exceptions\ApiEShopClientErrorException;
/**
 * Class EShopBizflyRepository
 * Lớp này dùng để làm BaseRepo
 */
class EShopBizflyRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

    public $cacheKey = 'CACHE_ACCESS_TOKEN';
    public $cacheXpired = 24*60*60;
    public $_DOMAIN_URL;
    public $_PROJECT_ID;
    public $_ACCESS_TOKEN = "";

    public function __construct()
    {
      parent::__construct(app());
      $this->_DOMAIN_URL = config('eshopbizfly-container.domain');
      $this->_PROJECT_ID = config('eshopbizfly-container.project_token');
      $this->_ACCESS_TOKEN = Cache::has($this->cacheKey) ? Cache::get($this->cacheKey) : null;
    }

    public $content_types = [
        'urlencoded' => 'application/x-www-form-urlencoded',
        'json' => 'application/json',
        'application/json' => 'application/json',
        'multi-form-data' => 'multipart/form-data',
    ];

    public $options = [];
    public $params = [];
    public $endPoint = '';

    public function _login(): string
    {
        $this->_ACCESS_TOKEN = Cache::get($this->cacheKey);

        if (empty($this->_ACCESS_TOKEN)) {
            $accountArray = config('eshopbizfly-container.account');
            $response = $this->_makeRequest('login', $accountArray);

            $token = $response && isset($response['data']['token']) ? $response['data']['token'] : '';

            Cache::set($this->cacheKey, $token,$this->cacheXpired);

            return $token;
        }

        return $this->_ACCESS_TOKEN;
    }

    /**
     * Gọi API
     *
     * @param string $url: Đươngd dẫn tương đối
     * @param array $params
     * @param string $method
     * @param array $headers
     * @param string $content_type
     * @param integer $check
     * @return array
     */
    public function _makeRequest(
      string $url='',
      array $params=[],
      string $method='POST',
      array $headers = [],
      string $content_type = 'application/json',
      int $check = 0): array
    {
        if ($method == 'GET') {
            $url .= '?' . http_build_query($params);
        }

        $this->endPoint = $this->_DOMAIN_URL . $url;

        $this->params = array_merge_recursive($this->params, $params);

        $headers = array_merge([
            'Authorization' => 'Bearer ' . $this->_ACCESS_TOKEN,
            'eshop-token' => config('eshopbizfly-container.project_token')
        ], $headers);

        try{
            $request = Http::withHeaders($headers)->timeout(30);
            $response = $this->switchMethod($request, $method, $content_type);

            if ($response->ok()) {
              return $response->json();
            }
        }catch(\Exception $e) {
          dd($e->getMessage());
          throw_if($response->clientError(), ApiEShopClientErrorException::class);
          throw_if($response->serverError(), ApiEShopException::class);
          return [];
        }
    }

    public function switchMethod($request, $method = 'POST', $contentType = '')
    {
      if ($contentType == 'json') {
        $request->withBody(json_encode($this->params), $this->content_types[$contentType]);
      }

      switch ($method) {
        case 'POST':
            return $request->post($this->endPoint, $this->params);
            break;
        case 'GET':
            return $request->get($this->endPoint, $this->params);
            break;
        case 'PUT':
            return $request->put($this->endPoint, $this->params);
            break;
        case 'DELETE':
            return $request->delete($this->endPoint, $this->params);
            break;
        case 'PATCH':
            return $request->patch($this->endPoint, $this->params);
            break;
        default:
            return $request->post($this->endPoint, $this->params);
            break;
      }
    }
} // End class
