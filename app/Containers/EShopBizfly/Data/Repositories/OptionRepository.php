<?php

namespace App\Containers\EShopBizfly\Data\Repositories;

use App\Containers\EShopBizfly\Models\EShopBizfly;
use App\Ship\Parents\Repositories\Repository;

/**
 * Class OptionRepository
 */
class OptionRepository extends EShopBizflyRepository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
