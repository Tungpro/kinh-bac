<?php

namespace App\Containers\EShopBizfly\Data\Repositories;

use App\Containers\EShopBizfly\Models\EShopBizfly;

/**
 * Class OptionValueRepository
 */
class OptionValueRepository extends EShopBizflyRepository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
