<?php

namespace App\Containers\EShopBizfly\Data\Repositories;


/**
 * Class ProductVariantRepository
 */
class ProductVariantRepository extends EShopBizflyRepository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
