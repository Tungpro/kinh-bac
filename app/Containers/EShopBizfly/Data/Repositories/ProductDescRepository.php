<?php

namespace App\Containers\EShopBizfly\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ProductDescRepository
 */
class ProductDescRepository extends EShopBizflyRepository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
