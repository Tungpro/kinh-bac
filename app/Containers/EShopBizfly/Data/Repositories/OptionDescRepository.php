<?php

namespace App\Containers\EShopBizfly\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class OptionDescRepository
 */
class OptionDescRepository extends EShopBizflyRepository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
