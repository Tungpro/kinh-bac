<?php

namespace App\Containers\EShopBizfly\Data\Repositories;

use App\Containers\EShopBizfly\Models\EShopBizfly;
use Illuminate\Database\Eloquent\Collection;
use App\Ship\Parents\Repositories\Repository;

/**
 * Class CategoryDescRepository
 */
class CategoryDescRepository extends EShopBizflyRepository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
    ];


} // End class
