<?php

namespace App\Containers\EShopBizfly\Jobs;

use Throwable;
use Illuminate\Bus\Queueable;
use App\Ship\Parents\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class SyncProductEShopApiJob
 */
class SyncSingleEShopProductJob extends Job implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  public $tries = 3;

  protected $productApiItem;
  protected $langs;


  public function __construct($productApiItem, $langs)
  {
    $this->productApiItem = $productApiItem;
    $this->langs = $langs;
  }

  public function handle()
  {
    try {
      dump($this->langs);
      dd($this->productApiItem);
      dd("abcc");

      Apiato::call('EShopBizfly@Products\SyncSingleProductEShopSubAction', [
        $this->productApiItem,
        $this->langs
      ]);
    }catch(\Exception $e) {
      dd($e->getMessage());
    }
  }


  public function failed(Throwable $exception)
  {
    dd($exception->getMessage());
  }
} // End class
