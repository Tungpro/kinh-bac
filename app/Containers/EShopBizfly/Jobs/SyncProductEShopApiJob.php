<?php

namespace App\Containers\EShopBizfly\Jobs;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Throwable;

/**
 * Class SyncProductEShopApiJob
 */
class SyncProductEShopApiJob extends Job implements ShouldQueue
{
  public $tries = 3;


  public function __construct()
  {
  }

  public function handle()
  {
    $products = Apiato::call('EShopBizfly@Products\SyncProductEShopAction', []);
    return $products;
  }

  public function failed(Throwable $exception)
  {
    dd($exception->getMessage());
  }
} // End class
