<?php

namespace App\Containers\EShopBizfly\Jobs;

use Throwable;
use App\Ship\Parents\Jobs\Job;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class SyncProductEShopJob
 */
class SyncProductEShopJob extends Job implements ShouldQueue
{
  public $tries = 3;

  protected $productApiItem;
  protected $ngonngu;


  public function __construct($productApiItem, $ngonngu)
  {
    $this->productApiItem = $productApiItem;
    $this->ngonngu = $ngonngu;
  }

  public function handle()
  {
    return Apiato::call('EShopBizfly@Products\SyncSingleProductEShopSubAction', [$this->productApiItem, $this->ngonngu]);
  }


  public function failed(Throwable $exception)
  {
    dd($exception->getFile(), $exception->getLine(), $exception->getMessage());
  }
}
