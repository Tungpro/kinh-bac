<?php

/** @var Route $router */
$router->get('eshop/login', [
    'as' => 'web_eshopbizfly_create',
    'uses'  => 'Controller@authen',
    'middleware' => [
      'auth:admin',
    ],
]);

$router->group(['prefix' => 'eshop/categories'], function () use ($router) {
  $router->get('sync', [
    'as' => 'web_eshopbizfly_create',
    'uses'  => 'CategoryController@sync',
    'middleware' => [
      'auth:admin',
    ],
  ]);
});


$router->group(['prefix' => 'eshop/products'], function () use ($router) {
  $router->get('sync', [
    'as' => 'eshop.product.sync',
    'uses'  => 'ProductController@sync',
    'middleware' => [
      'auth:admin',
    ],
  ]);

  $router->get('flush', [
    'as' => 'eshop.product.flush',
    'uses'  => 'ProductController@flush',
    'middleware' => [
      'auth:admin',
    ],
  ]);
});

