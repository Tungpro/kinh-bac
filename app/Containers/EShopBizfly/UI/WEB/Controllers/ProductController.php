<?php

namespace App\Containers\EShopBizfly\UI\WEB\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\EShopBizfly\Models\Option;
use App\Containers\EShopBizfly\Models\Product;
use App\Ship\Parents\Controllers\WebController;
use App\Containers\EShopBizfly\Models\OptionDesc;
use App\Containers\EShopBizfly\Models\OptionValue;
use App\Containers\EShopBizfly\Models\ProductDesc;
use App\Containers\EShopBizfly\Models\ProductOption;
use App\Containers\EShopBizfly\Models\ProductVariant;
use App\Containers\EShopBizfly\Models\OptionValueDesc;
use App\Containers\EShopBizfly\Jobs\SyncProductEShopJob;
use App\Containers\EShopBizfly\Models\ProductOptionValue;
use App\Containers\EShopBizfly\Jobs\SyncProductEShopApiJob;
use App\Containers\EShopBizfly\Jobs\SyncSingleEShopProductJob;

/**
 * Class ProductController
 *
 * @package App\Containers\EShopBizfly\UI\WEB\Controllers
 */
class ProductController extends WebController
{
  public function sync() {
      $productFromApi = Apiato::call('EShopBizfly@Products\SyncProductEShopAction', []);
      $langs = Apiato::call('Localization@GetAllLanguageDBAction', []);

      foreach ($productFromApi['data'] as $productItem) {
        SyncProductEShopJob::dispatch($productItem, $langs)->onQueue('high');
      }

      return "Ok Sync Done";
  }

  public function flush() {
    ProductVariant::query()->delete();
    ProductOptionValue::query()->delete();
    ProductOption::query()->delete();
    OptionValueDesc::query()->delete();
    OptionValue::query()->delete();
    OptionDesc::query()->delete();
    Option::query()->delete();
    ProductDesc::query()->delete();
    Product::query()->delete();

    return 'flush data success';
  }
} // End class
