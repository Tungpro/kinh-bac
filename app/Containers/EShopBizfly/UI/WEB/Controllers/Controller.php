<?php

namespace App\Containers\EShopBizfly\UI\WEB\Controllers;

use App\Containers\EShopBizfly\UI\WEB\Requests\CreateEShopBizflyRequest;
use App\Containers\EShopBizfly\UI\WEB\Requests\DeleteEShopBizflyRequest;
use App\Containers\EShopBizfly\UI\WEB\Requests\GetAllEShopBizfliesRequest;
use App\Containers\EShopBizfly\UI\WEB\Requests\FindEShopBizflyByIdRequest;
use App\Containers\EShopBizfly\UI\WEB\Requests\UpdateEShopBizflyRequest;
use App\Containers\EShopBizfly\UI\WEB\Requests\StoreEShopBizflyRequest;
use App\Containers\EShopBizfly\UI\WEB\Requests\EditEShopBizflyRequest;
use App\Ship\Parents\Controllers\WebController;
use Apiato\Core\Foundation\Facades\Apiato;

/**
 * Class Controller
 *
 * @package App\Containers\EShopBizfly\UI\WEB\Controllers
 */
class Controller extends WebController
{
    /**
     * Show all entities
     *
     * @param GetAllEShopBizfliesRequest $request
     */
    public function authen(GetAllEShopBizfliesRequest $request): string
    {
        $accessToken = Apiato::call('EShopBizfly@LoginEShopBizflyAction', [$request]);
        return $accessToken;
    }
} // End class
