<?php

namespace App\Containers\EShopBizfly\UI\WEB\Controllers;


use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\WebController;
use App\Containers\EShopBizfly\Actions\Categories\SyncCategoryEShopAction;

/**
 * Class CategoryController
 *
 * @package App\Containers\EShopBizfly\UI\WEB\Controllers
 */
class CategoryController extends WebController
{
  public function sync()
  {
      $eshopbizfly = Apiato::call(SyncCategoryEShopAction::class, []);
      return $eshopbizfly;
  }
} // End class
