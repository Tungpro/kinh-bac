<?php

namespace App\Containers\EShopBizfly\Tasks\Products;

use App\Containers\EShopBizfly\Data\Repositories\ProductRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\LazyCollection;

class GetAllProductEShopAfterInsertDBTask extends Task
{

    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $eshopProductIds=[]): LazyCollection
    {
      $products = $this->repository->whereIn('eshop_product_id', $eshopProductIds)->cursor();
      return $products;
    }
}
