<?php

namespace App\Containers\EShopBizfly\Tasks\Products;

use App\Containers\EShopBizfly\Data\Repositories\ProductDescRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\LazyCollection;

class SyncProductDescEShopTask extends Task
{

  protected $repository;

  public function __construct(ProductDescRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run(LazyCollection $products, Collection $langs, array $productFromEShopApiQuery=[])
  {
    try {
      $productEshopCollector = collect($productFromEShopApiQuery['data'])->keyBy('id')->toArray();

      $productDescArr = [];
      foreach ($langs as $lang) {
        foreach ($products as $product) {
          if($product->all_desc->isEmpty()) {
            $productItem = [
              'product_id' => $product->id,
              'language_id' => $lang->language_id,
              'name' => $product['draft_name'],
              'short_description' => $productEshopCollector[$product->eshop_product_id]['descriptions'],
              'description' =>  $productEshopCollector[$product->eshop_product_id]['details'],
            ];

            $productDescArr[] = $productItem;
          }
        }
      }

      return $this->repository->insertOrIgnore($productDescArr);
    } catch (Exception $exception) {
      throw new CreateResourceFailedException();
    }
  }
}
