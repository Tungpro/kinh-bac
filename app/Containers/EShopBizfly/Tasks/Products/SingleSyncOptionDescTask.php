<?php

namespace App\Containers\EShopBizfly\Tasks\Products;

use App\Containers\EShopBizfly\Data\Repositories\OptionDescRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class SingleSyncOptionDescTask extends Task
{

    protected $repository;

    public function __construct(OptionDescRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
