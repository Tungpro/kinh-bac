<?php

namespace App\Containers\EShopBizfly\Tasks\Products;

use App\Containers\EShopBizfly\Data\Repositories\OptionRepository;
use App\Containers\EShopBizfly\Data\Repositories\OptionValueRepository;
use App\Containers\EShopBizfly\Data\Repositories\ProductOptionValueRepository;
use App\Containers\EShopBizfly\Data\Repositories\ProductVariantRepository;
use App\Containers\EShopBizfly\Models\Product;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Database\Eloquent\Collection;

class SingleSyncProductOptionValueTask extends Task
{

    protected $repository;
    protected $optionValueRepository;
    protected $productOptionValueRepository;
    protected $productVariantRepository;

    public function __construct(
      OptionRepository $repository,
      OptionValueRepository $optionValueRepository,
      ProductOptionValueRepository $productOptionValueRepository,
      ProductVariantRepository $productVariantRepository
    )
    {
        $this->repository = $repository;
        $this->optionValueRepository = $optionValueRepository;
        $this->productOptionValueRepository = $productOptionValueRepository;
        $this->productVariantRepository = $productVariantRepository;
    }

    public function run(Product $product, array $productFromApi=[])
    {
      $product->load('options.option_value');
      $input_data = [];

      if (!empty($productFromApi['items'])) {
        $this->productOptionValueRepository->where('product_id', $product->id)->delete();
        $this->productVariantRepository->where('product_id', $product->id)->delete();
        foreach ($productFromApi['items'] as $item) {
          if (!empty($item['attributes'])) {
             // Tạo variant
            $productVariant = $this->productVariantRepository->create([
              'product_id' => $product->id,
              'sku' => $item['sku_code'],
              'price' => $item['price'],
              'global_price' => $item['compare_price']
            ]);

            foreach ($item['attributes'] as $attr) {
              $input_data = $this->buildProductOptionValueData($product, $attr);
              $input_data['product_variant_id'] = $productVariant->product_variant_id;
              $productOptionValue[] = $this->productOptionValueRepository->create($input_data);
            } // End foreach attribute inside item
          } // End if check attribute
        } // End foreach item
      } // Endif
    }

    public function buildProductOptionValueData(Product $product, array $attr=[]): array {
      $input_data = [];

      if ($product->options->isNotEmpty()) {
        foreach ($product->options as $opt) {
          if ($opt->eshop_attr_id == $attr['attribute_id']) {
            foreach ($opt->option_value as $optValue) {
              if ($optValue->eshop_attr_value_id == $attr['value_id']) {
                $input_data = [
                  'product_id' => $product->id,
                  'option_id' => $opt->id,
                  'option_value_id' => $optValue->id,
                  'product_option_id' => $opt->pivot->product_option_id
                ];
              }
            }
          }
        }
      }

      return $input_data;
    }
} // End class
