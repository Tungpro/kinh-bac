<?php

namespace App\Containers\EShopBizfly\Tasks\Products;

use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\LazyCollection;
use Illuminate\Database\Eloquent\Collection;
use App\Containers\EShopBizfly\Data\Repositories\EShopBizflyRepository;
use App\Containers\EShopBizfly\Data\Repositories\ProductOptionValueRepository;
use App\Containers\EShopBizfly\Data\Repositories\ProductVariantRepository;

class HandleProductOptionFromApiTask extends Task
{

  protected $repository;
  protected $productOptionValueRepository;

  public function __construct(ProductVariantRepository $repository, ProductOptionValueRepository $productOptionValueRepository)
  {
    $this->repository = $repository;
    $this->productOptionValueRepository = $productOptionValueRepository;
  }

  /**
   * Từ các "items" của product from API
   * Mapping với product của hệ thống, mỗi bản ghi trong "items" sẽ là 1 bản ghi variant giá
   *
   * @param array $productEShopFromApi: Mảng dữ liệu sản phẩm bên eshop
   * @param LazyCollection<App\Containers\EShopBizfly\Models\Product> $productEShopFromDB: Danh sách sản phẩm đã đồng bộ vào DB
   * @param Collection<App\Containers\EShopBizfly\Models\Option> $optionsFromDB: Danh sách thuộc tính đã đồng bộ vào DB
   * @param Collection<App\Containers\EShopBizfly\Models\OptionValue> $optionValueFromDB
   * @param Collection<App\Containers\EShopBizfly\Models\ProductOption> $listProductOptionDB
   * @return void
   */
  public function run(
    array $productEShopFromApi = [],
    LazyCollection $productEShopFromDB,
    Collection $optionsFromDB,
    Collection $optionValueFromDB,
    Collection $listProductOptionDB
  )
  {
    $optionValueFromDBArray = $optionValueFromDB->keyBy('eshop_attr_value_id')->toArray();
    $optionsFromDBArray = $optionsFromDB->keyBy('eshop_attr_id')->toArray();
    $productEShopFromDBArray = $productEShopFromDB->keyBy('eshop_product_id')->toArray();
    $productApiData = $productEShopFromApi['data'];

    $productVariantDBInput = [];
    $productOptionValueWithPriceInput = [];

    foreach ($productApiData as $product) {
      foreach ($product['items'] as $productOptionPrice) {
        $productVariant = $this->repository->create([
          'product_id' => $productEShopFromDBArray[$product['id']]['id'],
          'sku' => $productOptionPrice['sku_code'],
          'price' => $productOptionPrice['price'],
          'global_price' => $productOptionPrice['compare_price'],
        ]);

        // Xử lý giá cho option sản phẩm nào
        foreach ($productOptionPrice['attributes'] as $attr) {
          $productDbId = $productEShopFromDBArray[$product['id']]['id'];
          $optionDbId =  $optionsFromDBArray[$attr['attribute_id']]['id'];
          foreach ($listProductOptionDB as $prodOptDB) {
            // if ($prodOptDB->productOptionValues->isEmpty()) {
              if ($prodOptDB['product_id'] == $productDbId && $prodOptDB['option_id'] == $optionDbId) {
                if ( isset($optionValueFromDBArray[$attr['value_id']]['id']) ) {
                  $productOptionValueWithPriceInput[] = [
                    'product_option_id' => $prodOptDB['product_option_id'],
                    'product_id' => $productDbId,
                    'option_id' => $optionDbId,
                    'option_value_id' => $optionValueFromDBArray[$attr['value_id']]['id'],
                    'product_variant_id' => $productVariant->product_variant_id,
                    // 'quantity' => $productOptionPrice['quantity_wh'],
                    // 'subtract' => '',
                    // 'price' => '',
                    // 'price_prefix' => '',
                    // 'points' => '',
                    // 'points_prefix' => '',
                    // 'weight' => '',
                    // 'weight_prefix' => '',
                    'images' => ''
                  ];
                }
              }
            // }
          }
        }
      } // End foreach option giá
      break;
    } // End foreach product from API

    return $this->productOptionValueRepository->insertOrIgnore($productOptionValueWithPriceInput);
  }
} // End class
