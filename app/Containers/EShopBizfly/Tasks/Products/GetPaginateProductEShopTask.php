<?php

namespace App\Containers\EShopBizfly\Tasks\Products;

use App\Containers\Category\Models\Category;
use App\Containers\EShopBizfly\Data\Repositories\EShopBizflyRepository;
use App\Containers\EShopBizfly\Data\Repositories\ProductRepository;
use App\Containers\EShopBizfly\Exceptions\ApiEShopBizflyErrorException;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class GetPaginateProductEShopTask extends Task
{

    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $currentPage=1, int $limit=20)
    {
      $productsEShop = $this->repository->_makeRequest('product', [
        'limit' => $limit,
        'sort_type' => 'DESC',
        'page' => $currentPage
      ], 'GET');

      return $productsEShop;
    }
}
