<?php

namespace App\Containers\EShopBizfly\Tasks\Products;

use Exception;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Collection;
use App\Containers\EShopBizfly\Models\Product;
use App\Containers\EShopBizfly\Data\Repositories\ProductDescRepository;

class SingleSyncProductDescTask extends Task
{

    protected $repository;

    public function __construct(ProductDescRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Product $product, Collection $langs): void
    {

      if ($langs->isNotEmpty()) {
        $productDescInput = [];
        foreach ($langs as $lang) {
          $productDescInput[$lang->language_id] = [
            'name' => $product->draft_name
          ];
        }

        if (!empty($productDescInput)) {
          $product->languages()->sync($productDescInput);
        }
      }
    }
} // End class
