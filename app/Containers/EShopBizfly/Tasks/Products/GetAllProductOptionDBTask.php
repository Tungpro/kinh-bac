<?php

namespace App\Containers\EShopBizfly\Tasks\Products;

use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\LazyCollection;
use Illuminate\Database\Eloquent\Collection;
use App\Containers\EShopBizfly\Data\Repositories\ProductOptionRepository;

class GetAllProductOptionDBTask extends Task
{

  protected $repository;

  public function __construct(ProductOptionRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run(array $productOptionArrayInsert = [], array $with=[]): Collection
  {
    $result = $this->repository->with($with)->where(function ($query) use ($productOptionArrayInsert) {
      foreach ($productOptionArrayInsert as $opt) {
        $query->orWhere(function ($sub) use ($opt) {
          $sub->where('product_id', $opt['product_id'])
              ->where('option_id', $opt['option_id']);
        });
      }
    })->get();
    return $result;
  }
}
