<?php

namespace App\Containers\EShopBizfly\Tasks\Products;

use App\Containers\EShopBizfly\Data\Repositories\ProductOptionRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\LazyCollection;

class SyncProductOptionDBTask extends Task
{

  protected $repository;

  public function __construct(ProductOptionRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run(LazyCollection $productsFromDB, Collection $optionsFromDB, array $productsFromAPI)
  {
    $productOptionArray = [];
    $collector = collect($productsFromAPI['data'])->keyBy('id')->toArray();
    foreach ($productsFromDB as $product) {
      $optionOfProductDB = $product->load('options');

      if ($optionOfProductDB->options->isEmpty()) {
        $currentProductAttrFromAPI = $collector[$product->eshop_product_id]['attributes_values'];
        $attrIdsFromAPI = Arr::pluck($currentProductAttrFromAPI, 'attribute_id');
        $optionOfProductFromDB = $optionsFromDB->filter(function ($opt) use ($attrIdsFromAPI) {
          return in_array($opt->eshop_attr_id, $attrIdsFromAPI);
        });

        if ($optionOfProductFromDB->isNotEmpty()) {
          foreach ($optionOfProductFromDB as $opt) {
            $productOptionArray[] = [
              'product_id' => $product->id,
              'option_id' => $opt->id
            ];
          }
        }
      }
    }

    $insertResult = $this->repository->insertOrIgnore($productOptionArray);
    return $productOptionArray;
  }
} // End class
