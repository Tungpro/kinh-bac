<?php

namespace App\Containers\EShopBizfly\Tasks\Products;

use App\Containers\EShopBizfly\Data\Repositories\OptionRepository;
use App\Containers\EShopBizfly\Data\Repositories\OptionValueRepository;
use App\Containers\EShopBizfly\Models\Product;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Database\Eloquent\Collection;

class SingleSyncProductOptionTask extends Task
{

    protected $repository;
    protected $optionValueRepository;

    public function __construct(OptionRepository $repository, OptionValueRepository $optionValueRepository)
    {
        $this->repository = $repository;
        $this->optionValueRepository = $optionValueRepository;
    }

    public function run(Product $product, array $optionIds=[]): void
    {
      if ( !empty($optionIds) ) {
        $product->options()->sync($optionIds);
      }
    }
} // End class
