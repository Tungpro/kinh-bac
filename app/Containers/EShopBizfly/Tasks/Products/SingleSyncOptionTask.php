<?php

namespace App\Containers\EShopBizfly\Tasks\Products;

use App\Containers\EShopBizfly\Data\Repositories\OptionRepository;
use App\Containers\EShopBizfly\Data\Repositories\OptionValueRepository;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Database\Eloquent\Collection;

class SingleSyncOptionTask extends Task
{

    protected $repository;
    protected $optionValueRepository;

    public function __construct(OptionRepository $repository, OptionValueRepository $optionValueRepository)
    {
        $this->repository = $repository;
        $this->optionValueRepository = $optionValueRepository;
    }

    public function run(array $productFromApi, Collection $langs): array
    {
      $optionIds = [];
      if (!empty($productFromApi['attributes_values'])) {
        $optionInput = [];

        foreach ($productFromApi['attributes_values'] as $attrValue) {
          $option = $this->repository->updateOrCreate([
            'eshop_attr_id' => $attrValue['attribute_id'],
          ], ['draft_name' => $attrValue['title']]);

          $optionDescInput = [];
          foreach ($langs as $lang) {
            $optionDescInput[$lang->language_id] = [
              'name' => $option->draft_name
            ];
          }

          $option->languages()->sync($optionDescInput);

          // Sync OptionValue, OptionValueDesc cùng option luôn
          if (!empty($attrValue['value_items'])) {
            foreach ($attrValue['value_items'] as $optVal) {
              $optionValue = $this->optionValueRepository->updateOrCreate([
                'option_id' => $option->id,
                'eshop_attr_value_id' => $optVal['value_id']
              ], ['draft_name' => $optVal['name']]);

              $optionValueDescInput = [];

              foreach ($langs as $lang) {
                $optionValueDescInput[$lang->language_id] = [
                  'name' => $optionValue->draft_name,
                  'option_id' => $option->id
                ];
              }

              $optionValue->languages()->sync($optionValueDescInput);
            }
          }
          $optionIds[] = $option->id;
        }
      } // End if

      return $optionIds;
    }
}
