<?php

namespace App\Containers\EShopBizfly\Tasks\Products;

use Exception;
use Illuminate\Support\Arr;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\LazyCollection;
use Illuminate\Database\Eloquent\Collection;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Containers\EShopBizfly\Data\Repositories\ProductOptionValueRepository;

class SyncProductOptionValueDBTask extends Task
{

  protected $repository;

  public function __construct(ProductOptionValueRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run(Collection $optionsValueCollection, Collection $listProductOption, array $productsFromAPI)
  {
    $collector = collect($productsFromAPI['data'])->keyBy('id')->toArray();
    $productOptionValueArray = [];
    $optionValueDBArray = $optionsValueCollection->keyBy('eshop_attr_value_id')->toArray();

    foreach ($listProductOption as $productOpt) {
      $product = $productOpt->product;
      if ($product) {
        $attributesFromAPI = $collector[$product->eshop_product_id]['attributes_values'];

        if ( !empty($attributesFromAPI) ) {
          $option = $productOpt->option;

          foreach ($attributesFromAPI as $attribiuteItems) {
            if ($attribiuteItems['attribute_id'] == $option->eshop_attr_id) {
              foreach ($attribiuteItems['value_items'] as $attrValue) {
                $eshopApiAttrValueId = $attrValue['value_id'];

                $productOptionValueArray[] = [
                  'product_option_id' => $productOpt->product_option_id,
                  'product_id' => $productOpt->product_id,
                  'option_id' => $productOpt->option_id,
                  'option_value_id' => $optionValueDBArray[$eshopApiAttrValueId]['id']
                ];
              }
            }
          } // End sub-foreach

        } // End if
      }else {
        //dump($productOpt->toArray());
      }
    } // End foreach

    return $this->repository->insertOrIgnore($productOptionValueArray);
  }
} // End class
