<?php

namespace App\Containers\EShopBizfly\Tasks\Products;

use App\Containers\EShopBizfly\Data\Repositories\ProductRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class SyncProductEShopTask extends Task
{

    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $productsFromEshop=[])
    {
        try {
            $productsArr = [];
            foreach ($productsFromEshop['data'] as $product) {
              $productItem = [
                'eshop_product_id' => $product['id'],
                'draft_name' => $product['name'],
                'sku' => '',
                'status' => 2, // $product['status']
                'image' => $product['avatar'],
                'hot' => '',
                'is_new' => '',
                'is_sale' => '',
                'out_of_stock' => '',
                'location' => '',
                'quantity' => $product['quantity'],
                'stock_status_id' => '',
                'manufacturer_id' => '',
                'shipping_required' => '',
                'price' => $product['price'],
                'global_price' => '',
                'points' => '',
                'date_available' => '',
                'weight' => $product['weight'],
                'length' => $product['length'],
                'width' => $product['width'],
                'height' => $product['height'],
                'subtract_stock' => '',
                'minimum' => '',
                'view_count' => '',
                'purchased_count' => '',
                'sort_order' => '',
                'type' => ''
              ];

              $productsArr[] = $productItem;
            }

            $insertResult = $this->repository->insertOrIgnore($productsArr);
            return $productsArr;
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
