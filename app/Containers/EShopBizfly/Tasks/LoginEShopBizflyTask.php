<?php

namespace App\Containers\EShopBizfly\Tasks;

use App\Containers\EShopBizfly\Data\Repositories\EShopBizflyRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class LoginEShopBizflyTask extends Task
{

    protected $repository;

    public function __construct(EShopBizflyRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data=[]): string
    {
        try {
          $accessToken =  $this->repository->_login();
          return $accessToken;
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
