<?php

namespace App\Containers\EShopBizfly\Tasks\Options;

use App\Containers\EShopBizfly\Data\Repositories\OptionValueRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Database\Eloquent\Collection;

class SyncOptionValueTask extends Task
{

  protected $repository;

  public function __construct(OptionValueRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run(Collection $optionsFromDB)
  {
    $optionValueArray = [];
    foreach ($optionsFromDB as $option) {
      if (!empty($option->eshop_attr_value_ids)) {
        foreach ($option->eshop_attr_value_ids as $key => $valueItem) {
          $optionValueArray[] = [
            'option_id' => $option->id,
            'eshop_attr_value_id' => $valueItem['value_id'],
            'status' => 1,
            'sort_order' => $key,
            'draft_name' => $valueItem['name'],
          ];
        }
      }
    }

    $insertResult = $this->repository->insertOrIgnore($optionValueArray);
    return $optionValueArray;
  }
} // End class
