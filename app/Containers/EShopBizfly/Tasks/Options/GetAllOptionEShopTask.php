<?php

namespace App\Containers\EShopBizfly\Tasks\Options;

use App\Containers\EShopBizfly\Data\Repositories\OptionRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Collection;

class GetAllOptionEShopTask extends Task
{

    protected $repository;

    public function __construct(OptionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Undocumented function
     *
     * @param array $eshopAttrIds: Id attribute do bên Eshop trả ra
     * @return void
     */
    public function run(array $eshopAttrIds=[]): Collection
    {
      return $this->repository->findWhereIn('eshop_attr_id', $eshopAttrIds);
    }
}
