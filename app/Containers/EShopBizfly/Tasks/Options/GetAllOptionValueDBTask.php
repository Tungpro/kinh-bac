<?php

namespace App\Containers\EShopBizfly\Tasks\Options;

use App\Containers\EShopBizfly\Data\Repositories\OptionValueRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

class GetAllOptionValueDBTask extends Task
{

    protected $repository;

    public function __construct(OptionValueRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $optionValueInsertArr=[]): Collection
    {
      $eshopAttrValueIds = Arr::pluck($optionValueInsertArr, 'eshop_attr_value_id');
      $result = $this->repository->findWhereIn('eshop_attr_value_id', $eshopAttrValueIds);
      return $result;
    }
}
