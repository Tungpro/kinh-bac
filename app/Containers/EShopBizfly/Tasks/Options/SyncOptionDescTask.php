<?php

namespace App\Containers\EShopBizfly\Tasks\Options;

use App\Containers\EShopBizfly\Data\Repositories\OptionDescRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Database\Eloquent\Collection;

class SyncOptionDescTask extends Task
{

  protected $repository;

  public function __construct(OptionDescRepository $repository)
  {
    $this->repository = $repository;
  }

  public function run(Collection $options, Collection $langs)
  {
    if ($langs->isNotEmpty() && $options->isNotEmpty()) {
      $allOptionDescCollection = $this->repository->all()->keyBy('option_id');
      $allOptionDescArray = $allOptionDescCollection->toArray();
      $optionDescArray = [];

      foreach ($langs as $lang) {
        foreach ($options as $optionItem) {
          if ( !isset($allOptionDescArray[$optionItem->id]) ) {
            $optionDescArray[] = [
              'option_id' => $optionItem->id,
              'language_id' => $lang->language_id,
              'name' => $optionItem->draft_name,
            ];
          }
        }
      } // End foreach

      $insertResult = $this->repository->insertOrIgnore($optionDescArray);
      return $insertResult;
    }
  }
} // End class
