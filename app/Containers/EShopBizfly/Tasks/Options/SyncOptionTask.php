<?php

namespace App\Containers\EShopBizfly\Tasks\Options;

use App\Containers\EShopBizfly\Data\Repositories\OptionRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Support\Arr;

class SyncOptionTask extends Task
{

    protected $repository;

    public function __construct(OptionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $productsFromEShopApi=[])
    {
        try {
          $attributeArray = [];

          foreach ($productsFromEShopApi['data'] as $product) {
            if ( !empty($product['attributes_values']) ) {
              foreach ($product['attributes_values'] as $key => $attr) {
                $attrEshopId = $attr['attribute_id'];
                if ( !array_key_exists($attrEshopId , $attributeArray) ) {
                  $attributeArray[$attrEshopId ] = [
                    'eshop_attr_id' => $attrEshopId,
                    'sort_order' => $key,
                    'status' => 1,
                    'draft_name' => $attr['title'],
                    'eshop_attr_value_ids' => json_encode($attr['value_items'], JSON_UNESCAPED_UNICODE)
                  ];
                }
              } // End foreach
            } // End if
          }

          $insertResult = $this->repository->insertOrIgnore($attributeArray);
          return $attributeArray;
        }
        catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }
}
