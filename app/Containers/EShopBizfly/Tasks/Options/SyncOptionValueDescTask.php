<?php

namespace App\Containers\EShopBizfly\Tasks\Options;

use App\Containers\EShopBizfly\Data\Repositories\OptionValueDescRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;
use Illuminate\Database\Eloquent\Collection;

class SyncOptionValueDescTask extends Task
{

    protected $repository;

    public function __construct(OptionValueDescRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Collection $optionsValue, Collection $langs)
    {
      $optionsValue->load('all_desc');
      $optionValueDesc = [];
      foreach ($langs as $lang) {
        foreach ($optionsValue as $optValItem) {
          if ($optValItem->all_desc->isEmpty()) {
            $optionValueDesc[] = [
              'option_value_id' => $optValItem->id,
              'language_id' => $lang->language_id,
              'option_id' => $optValItem->option_id,
              'name' => $optValItem->draft_name,
            ];
          }
        }
      }
      return $this->repository->insertOrIgnore($optionValueDesc);
    }
}
