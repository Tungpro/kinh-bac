<?php

namespace App\Containers\EShopBizfly\Tasks\Categories;

use App\Containers\EShopBizfly\Data\Repositories\EShopBizflyRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class GetAllCategoryEShopTask extends Task
{

    protected $repository;

    public function __construct(EShopBizflyRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        try {
          $eshopCategories = $this->repository->_makeRequest('categories', [], 'GET');
          return $eshopCategories;
        }
        catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }
}
