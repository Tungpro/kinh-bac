<?php

namespace App\Containers\EShopBizfly\Tasks\Categories;

use App\Containers\Category\Data\Repositories\CategoryRepository;
use App\Containers\EShopBizfly\Data\Repositories\EShopBizflyRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class GetCategoryByEShopCatIdTask extends Task
{

    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $eshopIds=[])
    {
      $categoryCollection = $this->repository->findWhereIn('eshop_cate_id', $eshopIds);
      return $categoryCollection;
    }
}
