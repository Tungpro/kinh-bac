<?php

namespace App\Containers\EShopBizfly\Tasks\Categories;

use App\Containers\EShopBizfly\Data\Repositories\CategoryRepository;
use App\Containers\EShopBizfly\Data\Repositories\EShopBizflyRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class SyncCategoryEShopTask extends Task
{

    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($eshopCategories=[])
    {
      $categories = [];

      foreach ($eshopCategories['data'] as $key => $category) {
        $categoryItem = [
          'image' => end($category['images']),
          'eshop_cate_id' => $category['id'],
          'parent_id' => $category['parent_id'],
          'draft_name' => $category['name'],
          'sort_order' => $key
        ];

        if (!empty($category['children_categories'])) {
          $categoryItem['children_categories'] = json_encode($category['children_categories'], JSON_UNESCAPED_UNICODE);
        }else {
          $categoryItem['children_categories'] = '';
        }

        $categories[] = $categoryItem;
      }
      $insertResult = $this->repository->insertOrIgnore($categories);
      return $categories;
    }
} // End class
