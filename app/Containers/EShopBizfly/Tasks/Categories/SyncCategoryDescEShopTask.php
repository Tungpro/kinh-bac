<?php

namespace App\Containers\EShopBizfly\Tasks\Categories;

use App\Containers\EShopBizfly\Data\Repositories\CategoryDescRepository;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Database\Eloquent\Collection;

class SyncCategoryDescEShopTask extends Task
{

    protected $repository;

    public function __construct(CategoryDescRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Collection $categories, Collection $langs)
    {
      $categoryDesc = [];

      if ($langs->isNotEmpty() && $categories->isNotEmpty()) {
        foreach ($langs as $lang) {
          foreach ($categories as $category) {
            $categoryDesc[] = [
              'category_id' => $category->category_id,
              'language_id' => $lang->language_id,
              'name' => $category->draft_name
            ];
          }
        }
      }

      return $this->repository->getModel()->insert($categoryDesc);
    }
} // End class
