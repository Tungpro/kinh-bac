<?php

namespace App\Containers\EShopBizfly\Models;

use App\Containers\Option\Models\Option as ModelsOption;
use App\Ship\Parents\Models\Model;

class Option extends ModelsOption
{
  protected $casts = [
    'eshop_attr_value_ids' => 'array'
  ];
}
