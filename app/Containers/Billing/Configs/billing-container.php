<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Payment Container
    |--------------------------------------------------------------------------
    */

    'environment' => [
        'production' => [
            '_DOMAIN_URL' => 'https://pay.bizfly.vn/',
            '_API_URL' => 'https://pay.bizfly.vn/payment/pay', // developing live mode
            '_PROJECT_TOKEN' => '' // developing live mode
        ],

        'dev' => [
            '_DOMAIN_URL' => 'https://pay.todo.vn/',
            '_API_URL' => 'https://pay.todo.vn/payment/pay', // developing live mode
            '_PROJECT_TOKEN' => 'Ii6Td07PJpMJUEAx0YbAjDF7XnliW4wo' // developing live mode
        ],
    ],
    'status'=>[
        '_RSP_CODE_COMPLETED'=>0,
        '_RSP_CODE_NOT_COMPLETED'=>99,
        '_RSP_CODE_DUPLICATE_ORDER'=>8,
        '_RSP_CODE_NOT_EXISTED_ORDER'=>7,
        '_RSP_CODE_NULL_PAYGATE'=>6,
        '_RSP_CODE_INVALID_TOKEN'=>4,
        '_RSP_CODE_NULL_TOKEN'=>9,
        '_RSP_CODE_NULL_TRANSACTION_ID'=>10
    ]
];
