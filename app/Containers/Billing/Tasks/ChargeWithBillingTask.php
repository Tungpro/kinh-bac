<?php

namespace App\Containers\Billing\Tasks;

use App\Containers\Billing\Exceptions\BillingApiErrorException;
use App\Containers\Billing\Libs\BizPay;
use App\Containers\Order\Models\Order;
use App\Containers\Payment\Contracts\PaymentChargerInterface;
use App\Containers\Payment\Models\PaymentTransaction;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Facades\Redirect;

/**
 * Class ChargeWithStripeTask.
 *
 * @author Mahmoud Zalt <mahmoud@zalt.me>
 */
class ChargeWithBillingTask extends Task implements PaymentChargerInterface
{

    private $bizpay;

    /**
     * ChargeWithStripeTask constructor.
     *
     * @param BizPay $bizpay
     */
    public function __construct(BizPay $bizpay)
    {
        $this->bizpay = $bizpay;//->make(Config::get('services.stripe.secret'), Config::get('services.stripe.version'));
    }

    /**
     * @param Order $order
     * @return PaymentTransaction
     */
    public function charge(Order $order,$method='')
    {
        $params = array('order_code' => $order->code);
        $params['hash'] = self::getHash('sha256', $order->code, $order->id);
        $return_url = route('web_checkout_payment_complete', $params);

        $response = $this->bizpay->buildUrlCheckout([
            'order_id' => $order->code,
            'order_value' => ($order->total_price + $order->fee_shipping),
            'redirect_url'   => $return_url,
            'email'   => $order->email,
            'fullname'   => $order->fullname,
            'payment_method'   => $method,
            'tel'   => $order->phone,
//            'description'   => $amount,
        ]);
        if (empty($response)) {
          throw new BillingApiErrorException();
        }

        $response = json_decode($response, 1);
        //     if (isset($response['success']) && $response['success'] && $response['data']['redirect']) {
        //         // return ['order' => $order,'url_redirect' => $response['data']['redirect']];
        //     } else {
        //         return ['order' => $order,'order_code' => $order->code, 'err' => $response['message']];
        //     }

         // this data will be stored on the pivot table (user credits)


         $transaction = new PaymentTransaction([
          'transaction_id' => $order->id,
          'status' => $response['success'],
          'is_successful' => 0,
          'data' => $response['data']['redirect'],
        ]);

        return $transaction;
    }


    public static function getHash($algo, $data, $key)
    {
        return hash_hmac($algo, $data, $key);
    }
}
