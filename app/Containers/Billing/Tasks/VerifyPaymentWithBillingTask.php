<?php

namespace App\Containers\Billing\Tasks;

use App\Containers\Billing\Exceptions\BillingApiErrorException;
use App\Containers\Billing\Libs\BizPay;
use App\Containers\Order\Models\Order;
use App\Containers\Payment\Models\PaymentTransaction;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Http\Client\Request;

/**
 * Class ChargeWithStripeTask.
 *
 * @author Mahmoud Zalt <mahmoud@zalt.me>
 */
class VerifyPaymentWithBillingTask extends Task
{

  private $bizpay;

  /**
   * ChargeWithStripeTask constructor.
   *
   * @param BizPay $bizpay
   */
  public function __construct(BizPay $bizpay)
  {
    $this->bizpay = $bizpay; //->make(Config::get('services.stripe.secret'), Config::get('services.stripe.version'));
  }

  public function verify(&$message='')
  {
    $response = $this->bizpay->verifyUrlCallback($message);

    return $response;
  }


  public static function getHash($algo, $data, $key)
  {
    return hash_hmac($algo, $data, $key);
  }
}
