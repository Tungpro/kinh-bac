<?php

/**
 * Bizfly Payment SDK
 * Version 1.9.0
 * Update payment secure
 * Telegram/Tel: 0962.305.259 - Mr. Phương
 */

namespace App\Containers\Billing\Libs;
class BizPay
{
//    public $_DOMAIN_URL = 'https://pay.bizfly.vn'; // production
//    public $_API_URL = 'https://pay.bizfly.vn/payment/v2/pay'; // production

    public $_DOMAIN_URL = 'https://pay.todo.vn'; // dev
    public $_API_URL = 'https://pay.todo.vn/payment/v2/pay'; // dev

    public $_PROJECT_TOKEN = 'mN40ozWTbsGF3c98P7sRrPhc90bHefF0'; // contact with us
    public $_CLIENT_ID = '106'; // contact with us

    /**
     * Get list bank card (atm/visa) saved
     * @param $email
     * @method GET
     * @return bool|string
     * @author by phuong viet
     */
    public function getBankCardWithPayToken($email) {
        $params = http_build_query(
            array(
                'email' => $email,
                'client_id' => $this->_CLIENT_ID,
                'pay_token_hash' => hash('sha256', $this->_CLIENT_ID . $this->_PROJECT_TOKEN . $email)
            )
        );

        return $this->_makeRequest($this->_DOMAIN_URL.'/api/epay/list-bank-card?'.$params, 'get');
    }

    /**
     * Function buildUrlIframe() -> get url bizfly payment iframe
     * @param $orderInfo
     * Example: $orderInfo = [Z
     * (required|String)  "order_id" => "25151-24125415",
     * (required|Integer)  "order_value" => "50000",
     * (required|String)  "project_token" => "ypBrkdtM407veJuj1BVGKVmo7x8WsEL5",
     * (required|String)  "redirect_url" => "https://merchant.com/confirm",
     * (required|String)  "email" => "yoyo@gmail.com",
     * (optional|String)  "fullname" => "phương việt",
     * (optional|String)  "tel" => 0962305259
     * ]
     * @return object
     * Example: $object = [
     * 'success' => true,
     * 'message' => 'Get iframe url success!',
     * 'url' => ' https://pay.todo.vn/payment?params=9574%3A1571280962%3A4cf2ff8303e4cad0167213b75ab7a17c7d5e47c5d3099f63071b460a4398dd42%3Ahttp%25253A%25252F%25252Fagency.local%25252Fdemo%25252Forder%25252Fcallback%3A0%3A0%3Anguyenvietphuong2108%40gmail.com%3A60%3A0a33d8d5c390a901ae3493a888e0dabf&amp;client_pay_gate=2048&amp;voucher_code=&amp;tel=0&amp;iframe=1'
     * ]
     * @author by phương việt
     */
    public function buildUrlIframe($orderInfo)
    {
        $orderInfo = $this->_initOrder($orderInfo);

        if (!$orderInfo['success']) {
            return json_encode([
                'success' => false,
                'message' => 'Payment failed',
                'url' => ''
            ]);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_API_URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($orderInfo['data']));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        ));
        $result = curl_exec($ch);

        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_error($ch)) {

            return json_encode([
                'success' => false,
                'message' => 'created payment link failed',
                'url' => ''
            ]);
        }
        if ($status != 200) {
            curl_close($ch);

            return json_encode([
                'success' => false,
                'message' => 'created payment link failed',
                'url' => ''
            ]);
        }
        curl_close($ch);

        return $result;
    }

    /**
     * Init order info present
     * @param $orderInfo
     * @return array
     */
    protected function _initOrder($orderInfo)
    {
        if (empty($this->_PROJECT_TOKEN) || !isset($orderInfo['order_id']) || !isset($orderInfo['order_value']) || !isset($orderInfo['redirect_url'])) {
            return [
                'success' => false,
                'message' => 'Param invalid',
                'data' => []
            ];
        }

        $orderInfo['merchant_id'] = $this->_CLIENT_ID;

        $orderInfo['redirect_url'] = urlencode($orderInfo['redirect_url']);

        $orderInfo['secret_key'] = $this->__makeSecret($orderInfo);

        return [
            'success' => true,
            'message' => 'success',
            'data' => $orderInfo
        ];
    }

    /**
     * make secret key function
     * @param $orderInfo array
     * @return string
     */
    private function __makeSecret($orderInfo)
    {
        return hash_hmac('SHA256', $this->httpBuildQuery($orderInfo), $this->_PROJECT_TOKEN);
    }

    /**
     * Function buildUrlCheckout() --> create order information and redirect to bizfly billing index
     * @param $orderInfo
     * Example: $orderInfo = [
     * ------------------------------------------
     * A. Required for each order
     * (required|String)  "order_id" => "25151-24125415",
     * (required|Integer)  "order_value" => "50000",
     * (required|String)  "project_token" => "ypBrkdtM407veJuj1BVGKVmo7x8WsEL5",
     * (required|String)  "redirect_url" => "https://merchant.com/confirm",
     * (required|String)  "email" => "nguyenvietphuong@gmail.com",
     * ------------------------------------------
     * B. Yes or no with every order
     * (optional|String)  "fullname" => "phương việt",
     * (optional|String)  "tel" => 0962305259,
     * ------------------------------------------
     * C. Get url payment gateway corresponding to the value in the field payment_method
     * (required|String)  "ignore" => 'index' (Add this field if you want to get the link at the payment gateway corresponding to the value in the field payment_method)
     * (required|String)  "payment_method" => 'wepay', (wepay/wepay_visa/truemoney/vimomo/vnpay/viettelpay)
     * ------------------------------------------
     * D. Payment With Token - Save Bank Card
     * (required|String) "pay_option" => 'PAY_AND_CREATE_TOKEN',
     * (optional|String) "pay_token" => 'owyubzw6t2ztd2t0kujgy83yytswsdf6', (use when pay_option = 'PAY_WITH_TOKEN' & pay_option = 'PURCHASE_OTP')
     * pay_option description:
     *      'PAY_AND_CREATE_TOKEN' : Payment transaction and request create token (VISA save bank card)
     *      'PAY_WITH_TOKEN' : Use when request payment with token (VISA)
     *      'PAY_WITH_RETURNED_TOKEN' : Payment transaction and request create token (ATM save bank card)
     *      'PURCHASE_OTP' : Use when request payment with token (ATM)
     * ]
     * ------------------------------------------
     * E. Set payment gate view in bizfly billing page
     * (required|Integer) "client_pay_gate" => 4127421 (client_page_gate value is payment gate bitwise)
     *
     * @return object
     * Example: $object = [
     * "success": true
     * "message": "Get payment url success!"
     * "data": {
     * "redirect": "http://wallet.local/payment/pay?order_id=1542433-1571366039&order_value=55000&redirect_url=http%253A%252F%252Fagency.local%252Fdemo%252Forder%252Fcallback&recha..."
     * }
     * ]
     * @author by phương việt
     */
    public function buildUrlCheckout($orderInfo)
    {
        $orderInfo = $this->_initOrder($orderInfo);
        if (!$orderInfo['success']) {
            return json_encode([
                'success' => false,
                'message' => 'Payment failed',
                'data' => [
                    'redirect' => ''
                ]
            ]);
        }

        $queryString = http_build_query($orderInfo['data']);

        if (!empty($queryString)) {
            return json_encode([
                'success' => true,
                'message' => 'Get payment url success!',
                'data' => [
                    'redirect' => $this->_API_URL . "?" . $queryString
                ]
            ]);
        } else {
            return json_encode([
                'success' => false,
                'message' => 'Error building order.',
                'data' => [
                    'redirect' => ''
                ]
            ]);
        }
    }

    /**
     * Verify data callback
     * @param $message
     * @return bool
     */
    public function verifyUrlCallback(&$message = '')
    {
        if (isset($_REQUEST['error']) && isset($_REQUEST['message'])) {
            $message = $_REQUEST['message'];
            return false;
        }
        if (!isset($_REQUEST['order_id']) || !isset($_REQUEST['created_order_date']) || !isset($_REQUEST['secure_hash'])) {
            $message = 'Thông tin đơn hàng trả về không hợp lệ';
            return false;
        }

        if ($this->_generalKeyOrder(
                $_REQUEST['order_id'],
                $_REQUEST['created_order_date'],
                $_REQUEST['total_payment'],
                $_REQUEST['order_id_client'],
                $_REQUEST['vid'],
                $_REQUEST['recharge']
            ) === $_REQUEST['secure_hash']) {
            if (isset($_REQUEST['recharge']) && $_REQUEST['recharge']) {
                $message = 'Tài khoản VietID: ' . $_REQUEST['vid'] . ' đã được cộng tiền.';
            } else {
                $message = 'Thông tin thanh toán được chấp nhận.';
            }

            $orderInfo = $this->getInfoOrder($_REQUEST['order_id_client']);

            if (!$this->safetyInfomation($_REQUEST, (array)$orderInfo, $message)) {
                return false;
            }
            if (!$this->verifyExtraData($_REQUEST, (array)$orderInfo, $message)) {
                return false;
            }
            if (!$this->verifySpecialData($message)) {
                return false;
            }
            return true;
        }
        $message = 'Thông tin đơn hàng lỗi';
        return false;
    }

    /**
     * Verify Special Data
     * @param $REQUEST
     * @param $orderInfo
     * @param string $message
     * @return bool
     */
    public function verifySpecialData(&$message = '')
    {
        if (!isset($_REQUEST['specialData'])) {
            return true;
        }

        $secureHashSpecial = isset(json_decode($_REQUEST['specialData'])->secure_hash_special) ? json_decode($_REQUEST['specialData'])->secure_hash_special : false;

        if (!$secureHashSpecial) {
            $message = 'Đơn hàng lỗi, special data không chính xác';
            return false;
        }

        $inputData = array();

        foreach (json_decode($_REQUEST['specialData']) as $key => $value) {
            $inputData[$key] = $value;
        }

        unset($inputData['secure_hash_special']);

        $secureHash = hash_hmac('sha256', md5($this->httpBuildQuery($inputData)), $this->_PROJECT_TOKEN);

        if ($secureHashSpecial === $secureHash) {
            return true;
        } else {
            $message = 'Thông tin special data không đáng tin cậy';

            return false;
        }
    }

    /**
     * Verify Extra Data
     * @param $REQUEST
     * @param $orderInfo
     * @param string $message
     * @return bool
     */
    public function verifyExtraData($REQUEST, $orderInfo, &$message = '')
    {
        if (!isset($_REQUEST['extraData']) || empty($_REQUEST['extraData'])) {
            $message = 'Thiếu thông tin extra data để thực hiện verify';
            return false;
        }
        $REQUEST['extraData'] = json_decode($REQUEST['extraData']);
        if ((string)hash_hmac('SHA256', md5(
                json_encode(($REQUEST['extraData'])->coupons_discount) .
                json_encode(($REQUEST['extraData'])->mybizfly_discount) .
                $REQUEST['extraData']->payment_gate_discount .
                $REQUEST['extraData']->payment_gate_fees
            ), md5($REQUEST['total_payment'] . '@#!$o9iEC29LjDvB1WI')) !== $REQUEST['extraData']->secure_hash_extra) {
            $message = 'Dữ liệu extra data không đáng tin cậy';
            return false;
        }
        $validExtraData = array_diff_assoc([
            'coupons_discount' => json_encode($orderInfo['order_info']->extraData->coupons_discount),
            'mybizfly_discount' => json_encode($orderInfo['order_info']->extraData->mybizfly_discount),
            'payment_gate_discount' => $orderInfo['order_info']->extraData->payment_gate_discount,
            'payment_gate_fees' => $orderInfo['order_info']->extraData->payment_gate_fees
        ], [
            'coupons_discount' => json_encode($REQUEST['extraData']->coupons_discount),
            'mybizfly_discount' => json_encode($REQUEST['extraData']->mybizfly_discount),
            'payment_gate_discount' => $REQUEST['extraData']->payment_gate_discount,
            'payment_gate_fees' => $REQUEST['extraData']->payment_gate_fees
        ]);

        if (count($validExtraData) > 0) {
            $message = 'Dữ liệu extra data không đáng tin cậy';
            return false;
        } else {
            return true;
        }
    }

    /**
     * verify url callback ignore index
     * @param $message
     * @return bool
     */
    public function verifyUrlPaymentGate(&$message = '')
    {
        if (!isset($_GET['RspCode']) || !isset($_GET['hashKey']) || !isset($_GET['link'])) {
            $message = 'Thông tin đơn hàng trả về không hợp lệ !';
            return false;
        }
        if (md5($_GET['link'] . $this->_PROJECT_TOKEN) == $_GET['hashKey']) {
            $message = 'Lấy link cổng thanh toán thành công!';
            return true;
        } else {
            $message = 'Lấy link cổng thanh toán thất bại';
            return true;
        }
    }

    /**
     * check safety information
     * @param $message
     * @return bool
     */
    public function safetyInfomation($REQUEST, $orderInfo, &$message = '')
    {
        if ($orderInfo['RspCode'] === 0) {
            $check = array_diff_assoc([
                'paygate' => $orderInfo['order_info']->paygate,
                'total_payment' => $orderInfo['order_info']->total_payment,
                'status' => $orderInfo['order_info']->status,
                'order_id' => $orderInfo['order_info']->order_id,
                'order_id_client' => $orderInfo['order_info']->order_id_client,
                'vid' => $orderInfo['order_info']->vid,
                'recharge' => $orderInfo['order_info']->recharge,
                'created_order_date' => $orderInfo['order_info']->created_order_date
            ], [
                'paygate' => $REQUEST['gate'],
                'total_payment' => $REQUEST['total_payment'],
                'status' => $REQUEST['status'],
                'order_id' => $REQUEST['order_id'],
                'order_id_client' => $REQUEST['order_id_client'],
                'vid' => $REQUEST['vid'],
                'recharge' => $REQUEST['recharge'],
                'created_order_date' => $REQUEST['created_order_date']
            ]);

            if (count($check) > 0) {
                $message = 'Cảnh báo! Thông tin có sự thay đổi bất thường';
                return false;
            } else {
                return true;
            }
        } else {
            $message = 'Đơn hàng thanh toán thất bại';
            return false;
        }
    }

    /**
     * @param $orderId
     * @param $created_order_date
     * @param $totalPayment
     * @param $order_id_client
     * @param $vid
     * @param $total_payment_discount
     * @param $recharge
     * @return string
     */
    protected function _generalKeyOrder($orderId, $created_order_date, $totalPayment, $order_id_client, $vid, $recharge)
    {
        // new algorithm
        return hash_hmac('SHA256', md5(
            $orderId .
            $created_order_date .
            $totalPayment .
            $order_id_client .
            $vid .
            $recharge
        ), md5($totalPayment . '@paybizfly'));
    }

    /**
     * Get transaction information
     * @param $order_client_id
     * @return object
     */
    public function getInfoOrder($order_client_id)
    {
        $time = time();
        $auth = md5($order_client_id . $time . $this->_PROJECT_TOKEN);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_DOMAIN_URL . '/api/order/info');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array('order_client_id' => $order_client_id)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            "Auth: $auth",
            "Time: $time"
        ));

        $result = curl_exec($ch);

        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (curl_error($ch)) {
            return false;
        }
        if ($status != 200) {
            curl_close($ch);
            return false;
        }
        // close curl
        curl_close($ch);
        return json_decode($result);
    }

    /**
     * verify url callback
     * @param $message
     * @return bool
     */
    public function verifyPostUrlCallback(&$message = '')
    {
        if (isset($_POST['error']) && isset($_POST['message'])) {
            $message = $_POST['message'];
            return false;
        }

        if (!isset($_POST['order_id']) || !isset($_POST['created_order_date']) || !isset($_POST['secure_hash'])) {
            $message = 'Thông tin đơn hàng trả về không hợp lệ';
            return false;
        }

        if ($this->_generalKeyOrder(
                $_POST['order_id'],
                $_POST['created_order_date'],
                $_POST['total_payment'],
                $_POST['order_id_client'],
                $_POST['vid'],
                $_POST['recharge']
            ) === $_POST['secure_hash']) {
            if (isset($_POST['recharge']) && $_POST['recharge']) {
                $message = 'Tài khoản VietID: ' . $_POST['vid'] . ' đã được cộng tiền.';
            } else {
                $message = 'Thông tin thanh toán được chấp nhận.';
            }

            return true;
        }

        $message = 'Thông tin đơn hàng lỗi';
        return false;
    }

    /**
     * Recheck order
     * @param $orderInfo
     * @return array
     */
    public function checkOrder($order_client_id)
    {
        $time = time();
        $auth = md5($order_client_id . $time . $this->_PROJECT_TOKEN);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_DOMAIN_URL . '/api/order/check');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array('order_id_client' => $order_client_id)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            "Auth: $auth",
            "Time: $time"
        ));
        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (curl_error($ch)) {
            return false;
        }
        if ($status != 200) {
            curl_close($ch);
            return false;
        }
        // close curl
        curl_close($ch);
        return $result;
    }

    /**
     * Function getListOrder() -> get list order with client
     * @param $start_date , $end_date, $current_pointer, $limit, $sort
     * Example:
     * (optional)  $start_date = '2020-10-07 10:00:00' (YYYY-MM-DD),
     * (optional)  $end_date = '2020-10-13 10:00:00' (YYYY-MM-DD),
     * (optional)  $current_pointer = 0,
     * (optional)  $limit = 100,
     * (optional)  $sort = 'asc' (asc/des),
     * @return object
     * Example: $object = [
     * "status" => true
     * "msg" => "Lấy dữ liệu thành công !"
     * "list_order" => array:1 [▼
     * 0 => array:10 [▼
     * "order_id" => 13132
     * "order_id_client" => "Event-1578367492-2703"
     * "order_payment_id" => "AT-478816-1578367506"
     * "paygate_id" => 1
     * "total_payment" => 80000
     * "total_payment_discount" => 0
     * "fees" => 0
     * "order_status" => 13
     * "created_order_date" => "2020-01-07 10:25:00"
     * "updated_order_date" => "2020-01-07 10:25:27"
     * ]
     * ]
     * "next" => "https://pay.todo.vn/api/order/get-list-order?client_id=106&start_date=2020-10-07 10:00:00&end_date=2020-10-13 10:00:00&limit=100&sort=asc&current_pointer=13132&access_key=a35319c00607bdf898013ba2853b2966&access_time=1602036798&type=next&create_time=1602036799"
     * "previous" => "https://pay.todo.vn/api/order/get-list-order?client_id=106&start_date=2020-10-07 10:00:00&end_date=2020-10-13 10:00:00&limit=100&sort=asc&current_pointer=13132&access_key=aff58b43b48c554178fd76ed3ac9591c&access_time=1602036798&type=prev&create_time=1602036799"
     * ]
     * @author by phương việt
     */
    public function getListOrder($start_date = '', $end_date = '', $current_pointer = 0, $limit = 100, $sort = 'asc', $type = '')
    {
        $time = time();
        $auth = md5($this->_CLIENT_ID . $current_pointer . $end_date . $limit . $sort . $start_date . $time . $this->_PROJECT_TOKEN . $type);
        $params = [];
        $params['client_id'] = $this->_CLIENT_ID;
        $params['current_pointer'] = $current_pointer;
        $params['start_date'] = $start_date;
        $params['end_date'] = $end_date;
        $params['limit'] = $limit;
        $params['sort'] = $sort;
        $params['type'] = $type;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_DOMAIN_URL . '/api/order/get-list-order');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            "Auth: $auth",
            "Time: $time"
        ));
        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (curl_error($ch)) {
            return false;
        }
        if ($status != 200) {
            curl_close($ch);
            return false;
        }
        // close curl
        curl_close($ch);
        return json_decode($result, 1);
    }

    protected function _makeRequest($url, $type = 'post', $data = []) {
        $ch = curl_init();

        if ($type === 'post') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        ));

        $result = curl_exec($ch);

        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_error($ch)) {
            return false;
        }

        if ($status != 200) {
            curl_close($ch);

            return false;
        }

        curl_close($ch);

        return json_decode($result);
    }

    public function httpBuildQuery($data) {
        ksort($data);

        $i = 0;

        $query = "";

        foreach ($data as $key => $value) {
            if ($i == 1) {
                $query = $query . '&' . $key . "=" . $value;
            } else {
                $query = $query . $key . "=" . $value;

                $i = 1;
            }
        }

        return $query;
    }
}
