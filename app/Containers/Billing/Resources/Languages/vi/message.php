<?php 

return [
    'title' => 'FAQ',

    'id' => 'ID',
    'keyword' => 'Từ khóa',
    'faq_type' => 'Loại FAQ',
    'date_create' => 'Ngày tạo',
    'display_status' => 'Trạng thái hiển thị',
    'search' => 'Tìm kiếm',
    'reset' => 'Hủy tìm',
    
    'create' => 'Tạo FAQ',
    'update' => 'Cập nhật FAQ',

    'sort' => 'Sắp xếp',
    'question' => 'Câu hỏi',
    'answer' => 'Trả lời',
    'action' => 'Thao tác',
    'edit' => 'Sửa FAQ',
    'delete' => 'Xóa FAQ',
    'list' => 'Danh sách FAQ',
    'faq_manager' => 'Quản trị FAQ',
    'show' => 'Hiển thị',
    'hidden' => 'Ẩn',
    'owner' => 'Chủ nhà',
    'contractor' => 'Nhà thầu',
    'no_data' => 'Không có dữ liệu',
    'choose_faq_type' => '---Chọn loại FAQ---',
    'choose_display_status' => '---Chọn trạng thái hiển thị---',
    'keyword_input' => 'Nhập từ khóa...',
    'date_create_picker' => '---Chọn ngày tạo---',
    
    'total' => 'Tổng cộng',
    'record' => 'bản ghi',
    'page' => 'trang',

    'create_success' => 'Tạo FAQ thành công',
    'update_success' => 'Cập nhật FAQ thành công',
    'delete_success' => 'Xóa FAQ thành công',
];