<?php

namespace App\Containers\Billing\Exceptions;

use App\Ship\Parents\Exceptions\Exception;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

/**
 * Class StripeApiErrorException.
 *
 * @author  Mahmoud Zalt <mahmoud@zalt.me>
 */
class BillingApiErrorException extends Exception
{
    public $message = 'Billing API error.';
}
