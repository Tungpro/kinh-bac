<div class="tab-pane" id="contact" role="tabpanel" aria-expanded="false">
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                <label for="title">Email liên hệ</label>
                <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email"
                       name="contact[email]" value="{{ old('contact.email', @$data['contact']['email']) }}" required>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                <label for="fb_page_id">FB page ID for Messenger</label>
                <input type="text" class="form-control{{ $errors->has('fb_page_id') ? ' is-invalid' : '' }}"
                       id="fb_page_id" name="contact[fb_page_id]"
                       value="{{ old('contact.fb_page_id', @$data['contact']['fb_page_id']) }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                <label for="other_website">Website</label>
                <input type="text" class="form-control{{ $errors->has('other_website') ? ' is-invalid' : '' }}"
                       id="other_website" name="contact[other_website]"
                       value="{{ old('contact.other_website', @$data['contact']['other_website']) }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                <label for="title">Hotline</label>
                <input type="text" class="form-control{{ $errors->has('hotline') ? ' is-invalid' : '' }}" id="hotline"
                       name="contact[hotline]" value="{{ old('contact.hotline', @$data['contact']['hotline']) }}">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                <label for="title">Fax</label>
                <input type="text" class="form-control{{ $errors->has('hotline2') ? ' is-invalid' : '' }}" id="hotline"
                       name="contact[hotline2]" value="{{ old('contact.hotline2', @$data['contact']['hotline2']) }}">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-9">
            <div class="form-group">
                <label for="title">Ifram Map</label>
                <textarea class="form-control{{ $errors->has('map_iframe') ? ' is-invalid' : '' }}" id="map_iframe"
                          rows="5"
                          name="contact[map_iframe]">{{ old('contact.map_iframe', @$data['contact']['map_iframe'])}}</textarea>
            </div>
        </div>
    </div>
</div>