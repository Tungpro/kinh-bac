<div class="tab-pane" id="social" role="tabpanel" aria-expanded="false">
{{--    <div class="row">--}}
{{--        <div class="col-sm-6">--}}
{{--            <div class="form-group">--}}
{{--                <label for="facebook_name">Facebook APP ID</label>--}}
{{--                <input type="text" class="form-control{{ $errors->has('facebook_app_id') ? ' is-invalid' : '' }}"--}}
{{--                       id="facebook_app_id" name="social[facebook_app_id]"--}}
{{--                       value="{{ old('social.facebook_app_id', !empty($data['social']['facebook_app_id']) ? $data['social']['facebook_app_id'] : '') }}">--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="facebook_name">Facebook account</label>
                <input type="text" class="form-control{{ $errors->has('facebook_name') ? ' is-invalid' : '' }}"
                       id="facebook_name" name="social[facebook_name]"
                       value="{{ old('social.facebook_name', !empty($data['social']['facebook_name']) ? $data['social']['facebook_name'] : '') }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="fb_page_url">Facebook Page URL (Plugin Page)</label>
                <input type="text" class="form-control{{ $errors->has('fb_page_url') ? ' is-invalid' : '' }}"
                       id="fb_page_url" name="social[fb_page_url]"
                       value="{{ old('social.fb_page_url', @$data['social']['fb_page_url']) }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="facebook">Messenger</label>
                <input type="text" class="form-control{{ $errors->has('messenger') ? ' is-invalid' : '' }}" id="messenger"
                       name="social[messenger]"
                       value="{{ old('social.messenger', !empty($data['social']['messenger']) ? $data['social']['messenger'] : '') }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="zalo">Zalo</label>
                <input type="text" class="form-control{{ $errors->has('zalo') ? ' is-invalid' : '' }}" id="zalo"
                       name="social[zalo]"
                       value="{{ old('social.zalo', !empty($data['social']['zalo']) ? $data['social']['zalo'] : '') }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="instagram">Instagram</label>
                <input type="text" class="form-control{{ $errors->has('instagram') ? ' is-invalid' : '' }}"
                       id="instagram" name="social[instagram]"
                       value="{{ old('social.instagram', !empty($data['social']['instagram']) ? $data['social']['instagram'] : '') }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="youtube">Youtube</label>
                <input type="text" class="form-control{{ $errors->has('youtube') ? ' is-invalid' : '' }}" id="youtube"
                       name="social[youtube]"
                       value="{{ old('social.youtube', !empty($data['social']['youtube']) ? $data['social']['youtube'] : '') }}">
            </div>
        </div>
    </div>
</div>