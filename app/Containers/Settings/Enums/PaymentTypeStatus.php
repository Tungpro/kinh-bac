<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-20 01:20:05
 * @ Description: Happy Coding!
 */

namespace App\Containers\Settings\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class PaymentTypeStatus extends BaseEnum
{
}
