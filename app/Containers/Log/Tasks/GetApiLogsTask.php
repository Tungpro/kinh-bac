<?php

namespace App\Containers\Log\Tasks;

use App\Containers\Log\Data\Repositories\ApiLogRepository;
use App\Ship\Criterias\Eloquent\OrderByCreationDateDescendingCriteria;
use App\Ship\Parents\Tasks\Task;

class GetApiLogsTask extends Task
{

    protected $repository;

    public function __construct(ApiLogRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        $this->repository->pushCriteria(new OrderByCreationDateDescendingCriteria());
        return $this->repository->paginate();
    }
}
