<?php

namespace App\Containers\Log\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class StoreApiLogAction extends Action
{
    public function run(Request $request)
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        $apilog = Apiato::call('Log@StoreApiLogTask', [$data]);

        return $apilog;
    }
}
