<?php

namespace App\Containers\Log\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class ApiLogRepository
 */
class ApiLogRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
