<?php
Route::group(
[
    'prefix' => 'logs',
    'namespace' => '\App\Containers\Log\UI\WEB\Controllers',
    'domain' => 'admin.' . parse_url(\Config::get('app.url'))['host'],
    'middleware' => [
        'auth:admin',
    ],
],function () use ($router) {
    $router->get('/', [
        'as' => 'admin_apilogs_index',
        'uses' => 'ApiLogController@index'
    ]);
    $router->get('/errors', [
        'as'   => 'admin_errors_page',
        'uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index',
    ]);
});