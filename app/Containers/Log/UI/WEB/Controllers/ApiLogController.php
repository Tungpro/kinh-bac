<?php

namespace App\Containers\Log\UI\WEB\Controllers;

use Apiato\Core\Foundation\FunctionLib;
use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Controllers\WebController;
use App\Ship\Parents\Controllers\AdminController;
use App\Containers\Log\UI\WEB\Requests\EditLogRequest;
use App\Containers\Log\UI\WEB\Requests\StoreLogRequest;
use App\Containers\Log\UI\WEB\Requests\CreateLogRequest;
use App\Containers\Log\UI\WEB\Requests\DeleteLogRequest;
use App\Containers\Log\UI\WEB\Requests\UpdateLogRequest;
use App\Containers\Log\UI\WEB\Requests\GetAllLogsRequest;
use App\Containers\Log\UI\WEB\Requests\GetApiLogsRequest;
use App\Containers\Log\UI\WEB\Requests\FindLogByIdRequest;

/**
 * Class ApiLogController
 *
 * @package App\Containers\Log\UI\WEB\Controllers
 */
class ApiLogController extends AdminController
{
    public function __construct()
    {
        $this->title = 'API Logs';
        parent::__construct();
    }

    /**
     * Show all entities
     *
     * @param GetAllLogsRequest $request
     */
    public function index(GetApiLogsRequest $request)
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('API Log');
        \View::share('breadcrumb', $this->breadcrumb);
        
        $apiLogs = Apiato::call('Log@GetApiLogsAction', [$request]);
        return view('log::api_log.index', [
            'apiLogs' => $apiLogs,
            'request' => $request
        ]);
    }

    /**
     * Show one entity
     *
     * @param FindLogByIdRequest $request
     */
    public function show(FindLogByIdRequest $request)
    {
        $log = Apiato::call('Log@FindLogByIdAction', [$request]);

        // ..
    }

    /**
     * Create entity (show UI)
     *
     * @param CreateLogRequest $request
     */
    public function create(CreateLogRequest $request)
    {
        // ..
    }

    /**
     * Add a new entity
     *
     * @param StoreLogRequest $request
     */
    public function store(StoreLogRequest $request)
    {
        $log = Apiato::call('Log@CreateLogAction', [$request]);

        // ..
    }

    /**
     * Edit entity (show UI)
     *
     * @param EditLogRequest $request
     */
    public function edit(EditLogRequest $request)
    {
        $log = Apiato::call('Log@GetLogByIdAction', [$request]);

        // ..
    }

    /**
     * Update a given entity
     *
     * @param UpdateLogRequest $request
     */
    public function update(UpdateLogRequest $request)
    {
        $log = Apiato::call('Log@UpdateLogAction', [$request]);

        // ..
    }

    /**
     * Delete a given entity
     *
     * @param DeleteLogRequest $request
     */
    public function delete(DeleteLogRequest $request)
    {
         $result = Apiato::call('Log@DeleteLogAction', [$request]);

         // ..
    }
}
