@extends('basecontainer::admin.layouts.default')

@section('right-breads')
{!! $apiLogs->appends($request->all())->links('basecontainer::admin.inc.paginator') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-accent-primary">
                <div class="card-header d-flex">
                    <h5>List API Log</h5>
                    @include('log::api_log.filter')
                </div>
                <table class="table table-hover table-bordered mb-0" id="tableCustomer">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Source</th>
                            <th>End Point</th>
                            <th>Object Authen</th>
                            <th>Response</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @forelse ($apiLogs as $apiLog)
                        <tr>
                            <td>{{ $apiLog->id }}</td>
                            <td>{{ $apiLog->source }}</td>
                            <td>{{ $apiLog->end_point }} </td>
                            <td>
                                {{ $apiLog->current_object_authen['name'] }}
                                <code class="d-block">{{ $apiLog->current_object_authen['email'] }}</code>
                            </td>
                            <td><code>@bladeJson($apiLog->response)</code></td> 
                            <td>{{ $apiLog->created_at }}</td>
                            <td>
                                <input type="hidden" class="api-log-item" value='@bladeJson($apiLog)'>
                                <a href="javascript:void(0)" onclick="return log.showLogDetail(this)">Details</a>
                            </td>
                        </tr>
                        @empty 
                            <tr>
                                <td colspan="7">{{ __('Khong co du lieu') }}</td>
                            </tr>
                        @endforelse 
                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- /.row -->

    <div class="modal fade" id="modalApiLogDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Api Log Detail</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <pre id="modal-body-content"></pre>  
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
    </div>
@endsection

