<?php

namespace App\Containers\Banner\UI\WEB\Requests;

use App\Containers\BaseContainer\Traits\RequestBaseLanguage;
use App\Ship\Parents\Requests\Request;

/**
 * Class UpdateNewsRequest.
 *
 */
class UpdateBannerRequest extends Request
{
    use RequestBaseLanguage;
    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'roles' => 'admin',
        'permissions' => 'banner-manager',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
            'id' => 'bail|required|exists:banner,id',
            'image' => 'bail|nullable|mimes:jpeg,jpg,png,gif',
            'position' => 'bail|required',
//            'banner_description.*.name' => 'bail|required|max:255',
            'banner_description.*.description' => 'max:2000',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id.required' => 'ID không tồn tại',
            'id.exists' => 'ID không tồn tại',
            'image.mimes' => 'Ảnh banner không đúng định dạng (jpeg, jpg, png, gif)',
            'position.required' => 'Vui lòng chọn vị trí hiển thị',
        ] + $this->messagesLang([
//            'banner_description.*.name.required' => 'Tiêu đề language được bỏ trống',
//            'banner_description.*.name.max' => 'Tiêu đề language đa 255 ký tự',
            'banner_description.*.description.max' => 'Mô tả language tối đa 2000 ký tự',
        ]);
    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
