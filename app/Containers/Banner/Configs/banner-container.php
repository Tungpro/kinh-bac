<?php

return [
    'status' => [
        'visible' => 1,
        'hidden' => 2,
        'old_delete' => -1
    ],

    'days_of_end_at_default' => 30,

    'positions' => [
        'big_home' => 'Banner Trang Chủ - 1920x754',
        'banner_about_top' => 'Banner Trang giới thiệu - Top - 1920x313',
        'banner_project_top' => 'Banner Trang Lĩnh vực hoạt động - Top - 1920x650',
        'banner_holder_top' => 'Banner Trang Quan hệ cổ đông - Top - 1920x650',
        'banner_testimonial_top' => 'Banner Trang Thông tin hồi đáp cổ đông - Top - 1170x480',
        'banner_recruit_top' => 'Banner Trang Tuyển dụng - Top - 1920x313',


     //   'banner_notfound_top' => 'Banner Trang 404 - Top - 1920x754',
    ]
];
