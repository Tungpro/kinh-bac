<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:16:33
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\Tasks;

use App\Containers\Filter\Data\Repositories\FilterGroupRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

class FindByGroupIdTask extends Task
{

    protected $repository;

    public function __construct(FilterGroupRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, $external_data = [])
    {
        $this->repository->with(array_merge([], isset($external_data['with_relationship']) ? $external_data['with_relationship'] : []));

        $this->repository->pushCriteria(new ThisEqualThatCriteria('filter_group_id', $id));

        return $this->repository->first();
    }
}
