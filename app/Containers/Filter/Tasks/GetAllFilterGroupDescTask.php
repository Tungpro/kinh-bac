<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:16:55
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\Tasks;

use App\Containers\Filter\Data\Repositories\FilterGroupDescRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

class GetAllFilterGroupDescTask extends Task
{

    protected $repository;

    public function __construct(FilterGroupDescRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($filter_group_id)
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('filter_group_id', $filter_group_id));
        $wery = $this->repository->all()->keyBy('language_id')->toArray();

        return $wery;
    }
}
