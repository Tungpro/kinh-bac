<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-26 22:04:39
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\Tasks;

use App\Containers\Filter\Data\Repositories\FilterGroupRepository;
use App\Containers\Filter\Models\FilterDesc;
use App\Containers\Filter\Models\FilterGroupDesc;
use App\Containers\Product\Models\ProductFilter;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Facades\DB;

class GetFilterForProductTask extends Task
{

    protected $repository;

    public function __construct(FilterGroupRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($defaultLanguage = null, $language_id = '', $prd_id)
    {
        $language_id = $language_id != '' ? $language_id : ($defaultLanguage ? $defaultLanguage->language_id : 1);

        $conds[] = ['fd.language_id', '=', $language_id];
        $conds[] = ['pf.product_id', '=', $prd_id];

        $wery = DB::table($this->repository->getModel()->getTableName(), 'f');
        $wery->select('*');
        $wery->addSelect([
            'group' => DB::table(FilterGroupDesc::getTableName(), 'fgd')
                ->select('fgd.name')
                ->whereRaw('f.filter_group_id = fgd.filter_group_id')
                ->where('fgd.language_id', $language_id)
        ]);
        $wery->leftJoin(FilterDesc::getTableName() . ' as fd', 'f.filter_id', '=', 'fd.filter_id');
        $wery->join(ProductFilter::getTableName() . ' as pf', 'f.filter_id', '=', 'pf.filter_id');

        $wery->where($conds);

        return $wery->get();
    }
}
