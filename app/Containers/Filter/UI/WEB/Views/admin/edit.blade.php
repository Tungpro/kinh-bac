@extends('basecontainer::admin.layouts.default')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(isset($editMode) && $editMode)
                {!! Form::open(['url' => route('admin_filters_edit_page', $data->filter_group_id) , 'files' => true]) !!}
            @else
                {!! Form::open(['url' => route('admin_filter_add_page') , 'files' => true]) !!}
            @endif

            @if( count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif
            <div class="card card-accent-primary">
                <div class="card-header">
                    <i class="fa fa-pencil"></i> THÔNG TIN CƠ BẢN
                </div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs nav-underline nav-underline-primary mb-3" >
                                @foreach($langs as $it_lang)
                                    <li class="nav-item">
                                        <a class="nav-link {{$loop->first ? 'active' : ''}}" data-toggle="tab" href="#lang_{{$it_lang['language_id']}}" role="tab" aria-controls="website" aria-expanded="true"><i class="icon-globe"></i> {{$it_lang['name']}}</a>
                                    </li>
                                @endforeach
                            </ul>

                            <div class="tab-content p-0">
                                @foreach($langs as $it_lang)
                                    <div class="tab-pane {{$loop->first ? 'active' : ''}}" id="lang_{{$it_lang['language_id']}}" role="tabpanel" aria-expanded="true">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="title">Tên<span class="small text-danger">( {{$it_lang['name']}} )</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class=" input-group-text"><img src="/admin/img/lang/{{$it_lang['image']}}" title="{{$it_lang['name']}}"></span></div>
                                                        <input type="text" class="form-control" name="description[{{$it_lang['language_id']}}][name]" id="name_{{$it_lang['language_id']}}" value="{{ old('all_desc.'.$it_lang['language_id'].'.name', @$data->all_desc[$it_lang['language_id']]->name) }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="description_{{$it_lang['language_id']}}">Mô tả ngắn <span class="small text-danger">( {{$it_lang['name']}} )</span></label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class=" input-group-text"><img src="/admin/img/lang/{{$it_lang['image']}}" title="{{$it_lang['name']}}"></span></div>
                                                        <textarea rows="5" class="form-control" name="description[{{$it_lang['language_id']}}][short_description]" id="short_description_{{$it_lang['language_id']}}" >{!! old('all_desc.'.$it_lang['language_id'].'.short_description',@$data['all_desc'][$it_lang['language_id']]['short_description'])  !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="min">Giá trị nhỏ nhất <span class="small text-danger">(Giá trị nhỏ nhất (Bỏ qua khi ko sử dụng))</span></label>
                                <input type="text" class="form-control" name="min" id="min" value="{{ old('min',\FunctionLib::numberFormat(@$data->min)) }}" onkeypress="return shop.numberOnly()" onkeyup="shop.mixMoney(this)">
                            </div>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="max">Giá trị lớn nhất <span class="small text-danger">(Giá trị nhỏ nhất (Bỏ qua khi ko sử dụng))</span></label>
                                <input type="text" class="form-control" name="max" id="max" value="{{ old('max',\FunctionLib::numberFormat(@$data->max)) }}" onkeypress="return shop.numberOnly()" onkeyup="shop.mixMoney(this)">
                            </div>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="title">Sắp xếp</label>
                                <input type="text" class="form-control" name="sort_order" id="sort_order" value="{{ old('sort_order',@$data->sort_order) }}" required onkeypress="return shop.numberOnly()" onkeyup="shop.mixMoney(this)">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-accent-primary">
                <div class="card-header">
                    <i class="fa fa-pencil"></i> BỘ LỌC
                </div>
                <div class="card-body">
                    <table id="option-value" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-left required">Tên</td>
                            {{-- <td class="text-center">Ảnh</td> --}}
                            <td>Khoảng giá trị <span class="small text-danger">(ngăn cách dấu trừ - )</span></td>
                            <td class="text-right">Sắp xếp</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($filters))
                            @foreach($filters as $it_value)
                                @php($abc = $loop->index)
                                <tr id="filter-value-row{{$loop->index+1}}">
                                    <td class="text-center">
                                        <input type="hidden" name="filter_value[filter_id][]"
                                               value="{{$it_value->filter_id}}"/>
                                        @foreach($langs as $it_lang)
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend"><span class=" input-group-text"><img src="/admin/img/lang/{{$it_lang['image']}}" title="{{$it_lang['name']}}"></span></div>
                                                <input type="text"
                                                       name="filter_value[filter_description][{{ $it_lang['language_id'] }}][{{@$it_value->all_desc[$it_lang['language_id']]->filter_id ?? '_'.$abc}}]"
                                                       value="{{ @$it_value->all_desc[$it_lang['language_id']]->name ?? '' }}"
                                                       placeholder="" class="form-control"/>
                                            </div>
                                        @endforeach
                                    </td>
                                    {{-- <td class="text-left">
                                        <a href="" id="thumb-image{{$loop->index+1}}"
                                           data-toggle="image" class="img-thumbnail"><img
                                                    src="" alt="" title=""
                                                    data-placeholder=""/></a>
                                        <input type="hidden" name="filter_value[image][]"
                                               value="{{$it_value->image}}" id="input-image{{$it_value->filter_id}}"/>
                                    </td> --}}
                                    <td class="text-right">
                                        <input type="text"
                                               name="filter_value[value_range][]"
                                               value="{{$it_value->value_range}}"
                                               class="form-control"/>
                                    </td>
                                    <td class="text-right">
                                        <input type="text"
                                               name="filter_value[sort_order][]"
                                               value="{{$it_value->sort_order}}"
                                               class="form-control"/>
                                    </td>
                                    <td class="text-right">
                                        <button type="button"
                                                onclick="removeFilter('#filter-value-row{{ $loop->index+1 }}', {{request()->id}}, {{$it_value->filter_id}} );"
                                                data-toggle="tooltip" title="Xóa"
                                                class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>

                        <tfoot>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-right">
                                <button type="button" onclick="addOptionValue();" data-toggle="tooltip" title="Thêm" class="btn btn-primary">
                                    <i class="fa fa-plus-circle"></i>
                                </button>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="mb-3">
                <button type="submit" id="submit-filter" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Cập nhật</button>
                &nbsp;&nbsp;
                <a class="btn btn-sm btn-danger" href="{{ redirect()->back()->getTargetUrl() }}"><i class="fa fa-ban"></i> Hủy bỏ</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@push('js_bot_all')
    <script>
        $('select[name=\'type\']').on('change', function () {
            if (this.value == 'select' || this.value == 'radio' || this.value == 'checkbox' || this.value == 'image') {
                $('#option-value').parent().show();
            } else {
                $('#option-value').parent().hide();
            }
        });

        $('select[name=\'type\']').trigger('change');

        var filter_value_row = {{ @$values ? $values->count() : 0 }};
        function removeFilter(dom_remove, group, filter ) {
            $(dom_remove).remove();
            shop.ajax_popup('filter_group/remove-filter', 'POST', {group:group, filter:filter}, function(json) {

            });
        }
        function addOptionValue() {
            filter_value_row++;
            html = '<tr id="filter-value-row' + filter_value_row + '">';
            html += '  <td class="text-left"><input type="hidden" name="filter_value[filter_id][]" value="_'+filter_value_row+'" />';
            @foreach($langs as $it_lang)
                html += '    <div class="input-group">';
            html += '      <div class="input-group-prepend"><span class=" input-group-text"><img src="/admin/img/lang/{{ $it_lang['image'] }}" title="{{ $it_lang['name'] }}" /></span></div><input type="text" name="filter_value[filter_description][{{ $it_lang['language_id'] }}][_'+filter_value_row+']" value="" placeholder="" class="form-control" />';
            html += '    </div>';
            @endforeach
                html += '  </td>';
            // html += '  <td class="text-center"><a href="#" id="thumb-image' + filter_value_row + '" data-toggle="image" class="img-thumbnail"><img src="" alt="" title="" data-placeholder="" /></a><input type="hidden" name="filter_value[image][]" value="" id="input-image' + filter_value_row + '" /></td>';
            html += '  <td class="text-right"><input type="text" name="filter_value[value_range][]" value="" placeholder="" class="form-control" /></td>';
            html += '  <td class="text-right"><input type="text" name="filter_value[sort_order][]" value="" placeholder="" class="form-control" /></td>';
            html += '  <td class="text-right"><button type="button" onclick="$(\'#filter-value-row' + filter_value_row + '\').remove();" data-toggle="tooltip" title="Xóa" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#option-value tbody').append(html);
        }
    </script>
@endpush
