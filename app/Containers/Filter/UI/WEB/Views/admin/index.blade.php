@extends('basecontainer::admin.layouts.default')

@section('right-breads')
    {!! $data->appends($search_data->toArray())->links('basecontainer::admin.inc.paginator') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mb-5"><h1>Quản trị {{ $site_title }}</h1></div>

            @if( count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{!! $error !!}</div>
                    @endforeach
                </div>
            @endif

            @if (session('status'))
                <div class="alert alert-success">
                    {!! session('status') !!}
                </div>
            @endif

            {!! Form::open(['url' => route('admin_filter_home_page'), 'method' => 'get', 'id' => 'searchForm']) !!}

            <div class="card card-accent-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class=" input-group-text"><i class="fa fa-bookmark-o"></i></span></div>
                                <input type="text" name="name" class="form-control" placeholder="Tên nhóm thuộc tính" value="{{ $search_data->name }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                    <a href="{{ route('admin_filter_home_page') }}" class="btn btn-sm btn-primary"><i
                            class="fa fa-refresh"></i> Reset</a>
                    <a href="{{ route('admin_filter_add_page') }}" class="btn btn-sm btn-primary"><i
                            class="fa fa-plus"></i> Thêm mới</a>
                </div>
            </div>
            {!! Form::close() !!}

            <div class="card card-accent-primary">
                <div class="card-body p-0">
                    <table class="table table-bordered table-striped table-responsive-md">
                        <thead>
                        <tr>
                            <th width="55">ID</th>
                            <th>Tên nhóm</th>
                            <th>Sắp xếp</th>
                            <th width="55" class="text-center">Lệnh</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $k => $item)
                            <tr>
{{--                                <td>{{++$k}}</td>--}}
                                <td>{{$item->filter_group_id}}</td>
                                <td>
                                    <b>{{@$item->desc->name ?? '!<Chưa nhập nội dung>'}}</b>
                                    <div class="mt-2 font-sm"><i>{{ @$item->desc->short_description }}</i></div>
                                </td>
                                <td>{{$item->sort_order}}</td>
                                <td align="center">
                                    <a href="{{ route('admin_filters_edit_page', $item->filter_group_id) }}" class="btn text-primary"><i class="fa fa-pencil"></i></a>
                                    <a data-href="{{ route('admin_filter_delete', $item->filter_group_id) }}"  class="btn text-danger" onclick="admin.delete_this(this)"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @include('basecontainer::admin.inc.bottom-pager-infor')
                </div>
            </div>
        </div>
    </div>
@stop
