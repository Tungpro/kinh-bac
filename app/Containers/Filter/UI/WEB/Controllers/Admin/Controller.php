<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-08 09:58:41
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:20:20
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use App\Containers\Filter\UI\WEB\Requests\CreateFilterGroupRequest;
use App\Containers\Filter\UI\WEB\Requests\FindFilterGroupRequest;
use App\Containers\Filter\UI\WEB\Requests\GetFilterAjaxRequest;
use App\Containers\Filter\UI\WEB\Requests\GetAllFilterGroupsRequest;
use App\Containers\Filter\UI\WEB\Requests\UpdateFilterGroupRequest;
use App\Containers\Filter\UI\WEB\Requests\GetFilterDataByID;
use App\Ship\Parents\Controllers\AdminController;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Http\Request;

class Controller extends AdminController
{
    public function __construct()
    {
        $this->title = 'Bộ lọc sản phẩm';
        parent::__construct();
    }
    /**
     * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(GetAllFilterGroupsRequest $request)
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Filter', $this->form == 'list' ? '' : route('admin_filter_home_page'));
        \View::share('breadcrumb', $this->breadcrumb);

        $filterGroups = Apiato::call('Filter@FilterGroupListingAction', [new DataTransporter($request), $this->perPage]);

        return view('filter::admin.index', [
            'search_data' => $request,
            'data' => $filterGroups,
        ]);
    }

    public function edit(FindFilterGroupRequest $request)
    {
        $this->showEditForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Filter', $this->form == 'list' ? '' : route('admin_filter_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Chỉnh sửa thông tin');
        \View::share('breadcrumb', $this->breadcrumb);

        $filterGroup = Apiato::call('Filter@FindByGroupIdAction', [$request->id]);

        if ($filterGroup) {
            $filters = Apiato::call('Filter@FilterListingAction', [$filterGroup->filter_group_id]);
            return view('filter::admin.edit', [
                'data' => $filterGroup,
                'filters' => $filters
            ]);
        }
        return $this->notfound($request->id);
    }

    public function add(Request $request)
    {
        $this->showAddForm();
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Filter', $this->form == 'list' ? '' : route('admin_filter_home_page'));
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Thêm mới');
        \View::share('breadcrumb', $this->breadcrumb);

        return view('filter::admin.edit', []);
    }

    public function update(UpdateFilterGroupRequest $request)
    {
        try {
            $filterGroup = Apiato::call('Filter@UpdateFilterGroupAction', [$request]);

            if ($filterGroup) {
                return redirect()->route('admin_filter_edit_page', ['id' => $filterGroup->filter_group_id])->with('status', 'Filter đã được cập nhật');
            }
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function create(CreateFilterGroupRequest $request)
    {
        try {
            $filterGroup = Apiato::call('Filter@CreateFilterGroupAction', [$request]);

            if ($filterGroup) {
                return redirect()->route('admin_filter_home_page')->with('status', 'Filter đã được thêm mới');
            }
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }

    public function deleteFilter(FindFilterGroupRequest $request)
    {
        try {
            Apiato::call('Filter@DeleteFilterGroupAction', [$request]);
        } catch (\Exception $e) {
            // throw $e;
            $this->throwExceptionViaMess($e);
        }
    }
    public function ajaxGetListFilter(GetFilterAjaxRequest $request){
        try {
            $dataFilter = [];
            $dataFilterSort = [];
            $filterGroups = Apiato::call('Filter@FilterGroupListingAction', [new DataTransporter($request), 200, true])->toArray();
            foreach ($filterGroups as $key => $value) {
                foreach ($value['filter'] as $items){
                    $dataFilter[$items['filter_group_id']][$items['sort_order']]['text'] = $value['desc']['name']. ' > ' . $items['desc']['name'];
                    $dataFilter[$items['filter_group_id']][$items['sort_order']]['id']= $items['filter_id'];
                    $dataFilter[$items['filter_group_id']][$items['sort_order']]['sort_order']= $items['sort_order'] ?? 0;
                    $dataFilter[$items['filter_group_id']][$items['sort_order']]['short_description']=  $value['desc']['short_description'] ?? '';
                }
                ksort($dataFilter[$items['filter_group_id']]);
            }
            foreach ($dataFilter as $key => $value) {
               foreach ($value as $k => $v) {
                    array_push($dataFilterSort,$v);
               }
            }
            return FunctionLib::ajaxRespond(true, 'Hoàn thành', $dataFilterSort);

        } catch (\Exception $e) {
            // throw $e;
            return FunctionLib::ajaxRespond(false, 'Xảy ra lỗi');

        }
    }
    public function ajaxFilterDataByID(GetFilterDataByID $request){
        try {
            $dataFilter = Apiato::call('Filter@GetFiltersByPrdIdAction',[$request->product_id]);
            $data=[];
            foreach($dataFilter as $key => $item){
                $data[$key] = $item->filter_id;
            }
            return FunctionLib::ajaxRespond(true, 'Hoàn thành', $data);
        } catch (\Exception $e) {
            return FunctionLib::ajaxRespond(false, 'Xảy ra lỗi');
        }
    }
}
