<?php

use Illuminate\Support\Facades\Route;

Route::namespace('\App\Containers\Filter\UI\WEB\Controllers\Admin')->prefix('admin/ajax')->group(function () {
  Route::prefix('filters')->group(function () {
    Route::get('/getListFilter', [
      'as' => 'admin.filter.getListFilter',
      'uses' => 'Controller@ajaxGetListFilter',
      'middleware' => [
      'auth:admin',
      ]
    ]);
    Route::get('/{product_id}/controller/filterData', [
      'as' => 'admin.filter.controller.filterData',
      'uses' => 'Controller@ajaxFilterDataByID',
      'middleware' => [
          'auth:admin',
      ],
    ]);
  });
});
