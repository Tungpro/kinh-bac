<?php

/**
 * @apiGroup           Filter
 * @apiName            getFilterGroups
 * @api                {get} /v1/filter/getFilterGroups Get All FilterGroups
 *
 * @apiVersion         1.0.0
 * @apiPermission      Authenticated User
 *
 * @apiUse             GeneralSuccessMultipleResponse
 */

$router->get('filter/getFilterGroups', [
    'as' => 'api_filtergroup_get_all',
    'uses' => 'Controller@getFilterGroups',
    'middleware' => [
        'auth:admin',
    ],
]);
