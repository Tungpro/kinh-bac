<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:19:58
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\UI\API\Transformers;

use App\Ship\Parents\Transformers\Transformer;
use Illuminate\Support\Facades\Config;

/**
 * Class FiltersSelect2Transformer.
 *
 */
class FiltersSelect2Transformer extends Transformer
{

    /**
     * @param $token
     *
     * @return  array
     */
    public function transform($filter)
    {
        return [
            'filter_id' => $filter->filter_id,
            'name' => strip_tags(html_entity_decode($filter->group . ' &gt; ' . $filter->name, ENT_QUOTES, 'UTF-8'))
        ];
    }
}
