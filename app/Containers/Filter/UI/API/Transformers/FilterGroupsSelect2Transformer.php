<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:19:47
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\UI\API\Transformers;

use App\Ship\Parents\Transformers\Transformer;
use Illuminate\Support\Facades\Config;

/**
 * Class FilterGroupsSelect2Transformer.
 *
 */
class FilterGroupsSelect2Transformer extends Transformer
{

    /**
     * @param $token
     *
     * @return  array
     */
    public function transform($filterGroup)
    {
        $response = [
            'id' => $filterGroup->filter_group_id,
            'text' => html_entity_decode($filterGroup->desc->name)
        ];

        return $response;
    }
}
