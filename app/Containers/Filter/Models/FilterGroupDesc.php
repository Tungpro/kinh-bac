<?php
/**
 * Created by PhpStorm.
 * Filename: FilterGroupDesc.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 8/12/20
 * Time: 14:41
 */

namespace App\Containers\Filter\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class FilterGroupDesc extends Model
{
    protected $table = 'filter_group_description';
    protected $primaryKey = ['filter_group_id', 'language_id'];
    public $incrementing = false;

    protected $fillable = [
        'filter_group_id',
        'language_id',
        'name'
    ];

    public function language()
    {
        return $this->hasOne(Language::class,'language_id','language_id');
    }
}