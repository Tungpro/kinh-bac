<?php
/**
 * Created by PhpStorm.
 * Filename: FilterDesc.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 8/12/20
 * Time: 14:03
 */

namespace App\Containers\Filter\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class FilterDesc extends Model
{
    protected $table = 'filter_description';
    protected $primaryKey = ['filter_id', 'language_id'];
    public $incrementing = false;

    protected $fillable = [
        'filter_id',
        'language_id',
        'filter_group_id',
        'name'
    ];

    public function language()
    {
        return $this->hasOne(Language::class,'language_id','language_id');
    }
}