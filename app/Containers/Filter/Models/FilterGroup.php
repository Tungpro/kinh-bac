<?php
/**
 * Created by PhpStorm.
 * Filename: FilterGroup.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 8/12/20
 * Time: 14:40
 */

namespace App\Containers\Filter\Models;

use App\Models\Category\CategoryFilterGroup;
use App\Ship\Parents\Models\Model;

class FilterGroup extends Model
{
    protected $table = 'filter_group';
    protected $primaryKey = 'filter_group_id';

    protected $fillable = [
        'sort_order',
        'status',
        'min',
        'max'
    ];

    public function desc()
    {
        return $this->hasOne(FilterGroupDesc::class,'filter_group_id','filter_group_id');
    }

    public function all_desc()
    {
        return $this->hasMany(FilterGroupDesc::class,'filter_group_id','filter_group_id');
    }

    public function filter()
    {
        return $this->hasMany(Filter::class,'filter_group_id','filter_group_id');
    }
    public function category_filter()
    {
            return $this->hasMany( CategoryFilterGroup::class, 'filter_group_id', 'filter_group_id');
    }
}