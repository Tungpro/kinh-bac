<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:12:54
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

class FindByGroupIdAction extends Action
{
    public function run($id)
    {
        $data = Apiato::call(
            'Filter@FindByGroupIdTask',
            [
                $id
            ]
        );

        $data->setRelation('all_desc', $data->all_desc->keyBy('language_id'));

        return $data;
    }
}
