<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-12 12:09:48
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

class CreateFilterGroupAction extends Action
{
    public function run($data)
    {
        $filterGroup = Apiato::call(
            'Filter@CreateFilterGroupTask',
            [
                $data
            ]
        );

        if ($filterGroup) {
            Apiato::call('Filter@SaveFilterGroupDescTask', [
                $data,
                [],
                $filterGroup->filter_group_id
            ]);
            Apiato::call('Filter@SaveFilterTask', [
                $data->filter_value,
                $filterGroup->filter_group_id
            ]);
        }

        $this->clearCache();

        return $filterGroup;
    }
}
