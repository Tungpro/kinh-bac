<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-10 13:11:40
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-12 12:09:01
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\Actions;

use App\Containers\Filter\Tasks\GetFiltersGroupByFilterGroupsTask;
use App\Ship\Parents\Actions\Action;

class GetFiltersGroupByFilterGroupsAction extends Action
{
    public $getFiltersGroupByFilterGroupsTask;

    public function __construct(GetFiltersGroupByFilterGroupsTask $getFiltersGroupByFilterGroupsTask)
    {
        $this->getFiltersGroupByFilterGroupsTask = $getFiltersGroupByFilterGroupsTask;
        parent::__construct();
    }

    public function run($filterGroupIds = [], $defaultLanguage = null, $orderBy = ['sort_order' => 'ASC', 'filter_group_id' => 'DESC'])
    {
        return $this->remember(function () use ($filterGroupIds, $defaultLanguage, $orderBy) {
            return $this->getFiltersGroupByFilterGroupsTask->run($filterGroupIds, $defaultLanguage, $orderBy);
        });
    }
}
