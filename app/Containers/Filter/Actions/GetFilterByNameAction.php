<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-10 15:16:16
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-09-10 13:12:31
 * @ Description: Happy Coding!
 */

namespace App\Containers\Filter\Actions;

use App\Ship\Parents\Actions\Action;

class GetFilterByNameAction extends Action
{
    public function run($name,$limit)
    {
        if ($name) {
            $filter['name'] = $name;
        }
        $data = $this->call(
            'Filter@GetFilterForChoosingTask',
            [
                '',
                $filter ?? [],
                [],
                $limit,
                $this->call('Localization@GetDefaultLanguageTask')
            ]
        );

        return $data;
    }
}
