<?php

namespace App\Containers\Nganluong\Tasks;

use App\Containers\Nganluong\Exceptions\NganluongApiErrorException;
use App\Containers\Nganluong\Libs\NganluongLib;
use App\Ship\Parents\Tasks\Task;

/**
 * Class ChargeWithStripeTask.
 *
 * @author Mahmoud Zalt <mahmoud@zalt.me>
 */
class VerifyPaymentWithNganluongTask extends Task
{

  private $gateway;

  /**
   * ChargeWithStripeTask constructor.
   *
   * @param NganluongLib $bizpay
   */
  public function __construct(NganluongLib $gateway)
  {
    $this->gateway = $gateway; //->make(Config::get('services.stripe.secret'), Config::get('services.stripe.version'));
  }

  public function verify(&$message='')
  {
    $response = $this->gateway->verifyUrlCallback($message);
    if (empty($response)) {
      throw new NganluongApiErrorException();
    }

    return $response;
  }

}
