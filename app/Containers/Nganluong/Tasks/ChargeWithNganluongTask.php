<?php

namespace App\Containers\Nganluong\Tasks;

use App\Containers\Nganluong\Exceptions\NganluongApiErrorException;
use App\Containers\Nganluong\Libs\NganluongLib;
use App\Containers\Order\Models\Order;
use App\Containers\Payment\Contracts\PaymentChargerInterface;
use App\Containers\Payment\Models\PaymentTransaction;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Facades\Config;

/**
 * Class ChargeWithNganluongTask.
 *
 * @author Mahmoud Zalt <mahmoud@zalt.me>
 */
class ChargeWithNganluongTask extends Task implements PaymentChargerInterface
{

    private $gateway;

    /**
     * ChargeWithNganluongTask constructor.
     *
     * @param NganluongLib $gateway
     */
    public function __construct()
    {
        $this->gateway = new NganluongLib(Config::get('nganluong-container.MERCHANT_ID'),
                                      Config::get('nganluong-container.MERCHANT_PASS'),
                                      Config::get('nganluong-container.RECEIVER'),
                                      Config::get('nganluong-container.URL_API'));
    }

    /**
     * @param Order $order
     * @return PaymentTransaction
     */
    public function charge(Order $order,$method='VISA')
    {
        $params = array('order_code' => $order->code);
        $params['hash'] = self::getHash('sha256', $order->code, $order->id);
        $return_url = route('web_checkout_payment_complete', $params);
        $description = '';
        $tax_amount = 0;
        $response = $this->gateway->buildUrlCheckout(
          $order->code,
          ($order->total_price + $order->fee_shipping),
          1,
          $description,
          $tax_amount,
          $order->fee_shipping,
          $order->discount,
          $return_url,
          $return_url,
          $order->fullname,
          $order->email,
          $order->phone,
          '',
          $order->orderItems,
          $method,
        );
        if (empty($response)) {
          throw new NganluongApiErrorException();
        }

         // this data will be stored on the pivot table (user credits)

         $transaction = new PaymentTransaction([
          'transaction_id' => $order->id,
          'status' => $response->error_code==00,
          'is_successful' => 0,
          'data' => $response->checkout_url,
        ]);

        return $transaction;
    }


    public static function getHash($algo, $data, $key)
    {
        return hash_hmac($algo, $data, $key);
    }
}
