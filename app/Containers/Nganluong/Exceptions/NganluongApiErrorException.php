<?php

namespace App\Containers\Nganluong\Exceptions;

use App\Ship\Parents\Exceptions\Exception;
use Illuminate\Support\Facades\Config;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

/**
 * Class StripeApiErrorException.
 *
 * @author  Mahmoud Zalt <mahmoud@zalt.me>
 */
class NganluongApiErrorException extends Exception
{
    public $message = 'Nganluong API error.';
}
