<?php

namespace App\Containers\Warehouse\Actions\WarehouseOrder;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetListOrderWarehousesAction extends Action
{
    public function run($filters, $external_data = [], $limit = 10, $skipPagination = false, $orderBy = 'desc')
    {

        $criterias = [];
        if (isset($filters->idWarehouseOrder) && $filters->idWarehouseOrder != '') {
            $criterias[] = ['equalIdWarehouseOrder' => [$filters->idWarehouseOrder]];
        }
        if (isset($filters->idWarehouse) && $filters->idWarehouse != '') {
            $criterias[] = ['equalIdWarehouse' => [$filters->idWarehouse]];
        }
        $criterias[] = ['orderByWarehouse' => [$orderBy]];
        if (isset($filters->tap) && $filters->tap != '') {
            $status=0;
            if($filters->tap =='cancel_order')
                $status= -1;
            elseif($filters->tap =='complete_order')
                $status= 2;
            elseif($filters->tap =='new_order')
                $status= 0;
            elseif($filters->tap =='waitting_order')
                $status= 1;

            $criterias[] = ['equalTab' => [$status]];
        }
        if (isset($filters->type) && $filters->type != '' && $filters->type != 'All') {
          $type = $filters->type ?? '';
          $criterias[] = ['typeTab' => [$type]];
        }
        $warehouses = Apiato::call('Warehouse@WarehouseOrder\GetListOrderWarehousesTask',[$external_data,$skipPagination, $limit],$criterias);

        return $warehouses;
    }
}
