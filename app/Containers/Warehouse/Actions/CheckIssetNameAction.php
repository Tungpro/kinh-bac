<?php

namespace App\Containers\Warehouse\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class CheckIssetNameAction extends Action
{
    public function run(string $input)
    {
        $warehouses = Apiato::call('Warehouse@CheckIssetNameTask', [$input]);

        return $warehouses;
    }
}
