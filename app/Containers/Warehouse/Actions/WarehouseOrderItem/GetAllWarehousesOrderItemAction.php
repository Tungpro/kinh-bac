<?php

namespace App\Containers\Warehouse\Actions\WarehouseOrderItem;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllWarehousesOrderItemAction extends Action
{
    public function run($filters = [], $limit = 10, $skipPagination = false)
    {
        $warehouses = Apiato::call('Warehouse@WarehouseOrderItem\GetAllWarehousesOrderItemTask',[$filters, $skipPagination, $limit]);
        foreach ($warehouses as $key => $value) {
           $warehouses[$key]['quantity_used_by_sale'] =(int) Apiato::call('Warehouse@WarehouseOrderItem\GetCartOrderToWarehouseOrderByPrdVariantsIDTask',[$value->product_variants_id]);
           $warehouses[$key]['quantity_still'] =(int) $warehouses[$key]['total_quantity'] - (int) $warehouses[$key]['quantity_used_by_sale'];
        }
        return $warehouses;
    }
}
