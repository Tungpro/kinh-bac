<?php

namespace App\Containers\Warehouse\Actions\WarehouseOrderItem;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class GetProductWarehouseDetailAction extends Action
{
    public function run($response)
    {
        $warehouses = Apiato::call('Warehouse@WarehouseOrderItem\GetProductWarehouseDetailTask',[$response->id,$response->IdWarehouse,$response->sku,$response->timePicker]);
      
        return $warehouses;
    }
}
