<?php

namespace App\Containers\Warehouse\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;

class SaveWarehouseAction extends Action
{
    public function run(array $data)
    {
        $warehouses = Apiato::call('Warehouse@SaveWarehouseTask', [$data]);
        $this->clearCache();
        return $warehouses;
    }
}
