<?php

namespace App\Containers\Warehouse\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class WarehouseRepository
 */
class WarehouseOrderRepository extends Repository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
