<?php

namespace App\Containers\Warehouse\UI\API\Transformers;

use App\Containers\Warehouse\Models\Warehouse;
use App\Ship\Parents\Transformers\Transformer;

class WarehouseTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected array $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected array $availableIncludes = [

    ];

    /**
     * @param Warehouse $entity
     *
     * @return array
     */
    public function transform(Warehouse $entity)
    {
        $response = [
            'object' => 'Warehouse',
            'id' => $entity->getHashedKey(),
            'created_at' => $entity->created_at,
            'updated_at' => $entity->updated_at,

        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }
}
