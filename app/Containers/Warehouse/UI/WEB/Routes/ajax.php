<?php

use Illuminate\Support\Facades\Route;

Route::namespace('\App\Containers\Warehouse\UI\WEB\Controllers')->prefix('ajax')->group(function () {
  Route::prefix('warehouse')->group(function () {
    Route::get('/controller/warehouseData', [
      'as' => 'warehouse.controller.warehouseData',
      'uses' => 'Controller@ajaxWarehouseData',
      'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::post('/controller/productList', [
      'as' => 'warehouse.controller.productList',
      'uses' => 'Controller@ajaxListProduct',
      'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::get('/controller/{id}/warehouseDelete', [
      'as' => 'warehouse.controller.warehouseDelete',
      'uses' => 'Controller@ajaxWarehouseDelete',
      'middleware' => [
        'auth:admin',
      ]
    ]);
    Route::post('/controller/warehouseList', [
      'as' => 'warehouse.controller.warehouseList',
      'uses' => 'Controller@ajaxWarehouseList',
      'middleware' => [
        'auth:admin',
      ]
    ]);

    Route::post('/controller/searchProductVariants', [
      'as' => 'warehouse.controller.searchProductVariants',
      'uses'  => 'Controller@ajaxSearchProductVariantsAction',
      'middleware' => [
        'auth:admin',
      ]
    ]);

    Route::post('/controller/createWarehouseOrder',[
      'as' => 'warehouse.controller.createWarehouseOrder',
      'uses'  => 'Controller@ajaxCreateWarehouseOrder',
      'middleware' => [
        'auth:admin',
      ]
    ]);

    Route::get('/controller/warehouseItemsDetail',[
      'as' => 'warehouse.controller.warehouseItems',
      'uses' => 'Controller@ajaxWarehouseItemsDetail',
      'middleware' => [
        'auth:admin',
      ]
    ]);

    Route::post('/controller/checkNameIsset',[
      'as' => 'warehouse.controller.checkNameIsset',
      'uses' => 'Controller@ajaxCheckNameIsset',
      'middleware' => [
        'auth:admin',
      ]
    ]);

    Route::post('/controller/updateWarehouseOrder',[
      'as' => 'warehouse.controller.updateWarehouseOrder',
      'uses' => 'Controller@ajaxUpdateWarehouseOrder',
      'middleware' => [
        'auth:admin',
      ]
    ]);

    Route::post('/controller/saveWarehouse',[
      'as' => 'warehouse.controller.saveWarehouse',
      'uses' => 'Controller@ajaxSaveWarehouse',
      'middleware' => [
        'auth:admin',
      ]
    ]);

    Route::post('/controller/ajaxWarehouseOrderItemData',[
      'as' => 'warehouseOrderItem.controller.ajaxWarehouseOrderItemData',
      'uses' => 'WarehouseOrderItemController@ajaxWarehouseOrderItemData',
      'middleware' => [
        'auth:admin',
      ]
    ]);

    Route::post('/controller/productWarehouseDetail',[
      'as' => 'warehouseOrderItem.controller.ajaxProductWarehouseDetail',
      'uses' => 'WarehouseOrderItemController@ajaxProductWarehouseDetail',
      'middleware' => [
        'auth:admin',
      ]
    ]);
  });
});
