<?php

namespace App\Containers\Warehouse\UI\WEB\Controllers;

use App\Containers\Warehouse\UI\WEB\Requests\GetAllWarehousesRequest;
use App\Containers\Warehouse\UI\WEB\Requests\CreateWarehouseOrderRequest;
use App\Containers\Warehouse\UI\WEB\Requests\FindWarehouseByIdRequest;
use App\Containers\Warehouse\UI\WEB\Requests\GetDetailsOrderWarehouseRequest;
use App\Containers\Warehouse\UI\WEB\Requests\CheckNameIssetRequest;
use App\Containers\Warehouse\UI\WEB\Requests\SaveWarehouseRequest;
use App\Containers\Product\UI\WEB\Requests\AjaxFindProductByInput;
use App\Containers\Warehouse\UI\WEB\Requests\DeleteWarehouseRequest;
use App\Containers\Warehouse\Actions\DeleteWarehouseAction;
use Apiato\Core\Foundation\Facades\Apiato;
use Apiato\Core\Foundation\Facades\FunctionLib;
use Apiato\Core\Foundation\Facades\StringLib;
use App\Containers\Product\Models\Product;
use App\Ship\Transporters\DataTransporter;
use App\Ship\Parents\Controllers\AdminController;
/**
 * Class Controller
 *
 * @package App\Containers\Warehouse\UI\WEB\Controllers
 */
class Controller extends AdminController
{
    /**
     * Show all entities
     *
     * @param GetAllWarehousesRequest $request
     */
    public function index()
    {
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Danh sách Kho', $this->form == 'list' ? '' : route('admin_warehouse_home_page'));
        \View::share('breadcrumb', $this->breadcrumb);
        // $warehouses = Apiato::call('Warehouse@GetAllWarehousesAction', [$request]);
        return view('warehouse::index');
    }

    public function warehouseOrder(){
        $this->breadcrumb[] = FunctionLib::addBreadcrumb('Nhập xuất Kho', $this->form == 'list' ? '' : route('admin_warehouse_order_page'));
        \View::share('breadcrumb', $this->breadcrumb);
        // $warehouses = Apiato::call('Warehouse@GetAllWarehousesAction', [$request]);
        return view('warehouse::order');
    }
    /**
     * Show one entity
     *
     * @param FindWarehouseByIdRequest $request
     */
    public function ajaxWarehouseData(GetAllWarehousesRequest $request){
        $warehouses = Apiato::call('Warehouse@GetAllWarehousesAction', [$request]);
        return FunctionLib::ajaxRespond(true, 'Hoàn thành', $warehouses);
    }

    public function ajaxSearchProductVariantsAction(AjaxFindProductByInput $request){
        $limit = 20;
          $data =  Apiato::call('Product@Admin\FindProductByInputAction',[$request->queryString ?? '',$limit]);
           $data->load([
          'product_variants.product_option_values',
          'product_variants.product_option_values.option.desc',
          'product_variants.product_option_values.option_value.desc',
        ]);
        if(!empty($data))
          return FunctionLib::ajaxRespond(true,'Hoàn thành',$data);
        else
          return FunctionLib::ajaxRespond(false,'Không tìm thấy sản phẩm');

      }

    public function ajaxWarehouseList(GetAllWarehousesRequest $request){
        $warehouses = Apiato::call('Warehouse@WarehouseOrder\GetListOrderWarehousesAction', [$request]);
        return FunctionLib::ajaxRespond(true, 'Hoàn thành', $warehouses);
    }
    public function ajaxListProduct(GetAllWarehousesRequest $request){
        $products = Apiato::call('Product@Admin\GetAllProductsAction', [$request->all(), ['with_relationship' => ['categories', 'categories.desc']]]);
        $products->load([
          'product_variants.product_option_values',
          'product_variants.product_option_values.option.desc',
          'product_variants.product_option_values.option_value.desc',
        ]);
        if(count($products) > 0)
            return FunctionLib::ajaxRespond(true, 'Hoàn thành', $products);
        else
            return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    public function ajaxCreateWarehouseOrder(CreateWarehouseOrderRequest $request)
    {
        if(isset($request->data)){
            $order =[];
            foreach ($request->data['choiceVariable'] as $key => $value) {
                if(!empty($value)){
                    $ex=explode('- Mã:', $value);
                    $order[$key]['product_variant'] = $ex[1];
                    $order[$key]['product_count'] = $request->data['inputVariableCount'][$key] ?? 0;
                    $order[$key]['product_price'] = $request->data['inputVariablePrice'][$key] ?? 0;
                    $order[$key]['id_product'] = $request->data['inputVariableProductID'][$key] ?? 0;
                }else{
                    $order[$key]['product_count'] = $request->data['inputVariableCount'][$key] ?? 0;
                    $order[$key]['product_price'] = $request->data['inputVariablePrice'][$key] ?? 0;
                    $order[$key]['id_product'] = $request->data['productData'][$key]['product_id'] ?? 0;
                }
            }
            $warehouse = Apiato::call('Warehouse@WarehouseOrder\CreateWarehouseOrderAction', [$request->data['ruleForm'],$order]);
        }

        if($warehouse)
            return FunctionLib::ajaxRespond(true, 'Hoàn thành', $warehouse);
        else
            return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    public function ajaxWarehouseItemsDetail(GetDetailsOrderWarehouseRequest $request){
        $detail = Apiato::call('Warehouse@WarehouseOrder\GetWarehouseOrderDetailAction',[$request->id]);

        if($detail)
            return FunctionLib::ajaxRespond(true, 'Hoàn thành', $detail);
        else
            return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    public function ajaxUpdateWarehouseOrder(GetDetailsOrderWarehouseRequest $request){
        $dataStatus=[
            'status' => $request->data['status'],
        ];
        $result = Apiato::call('Warehouse@WarehouseOrder\UpdateOrderAction',[$dataStatus,$request->data['id'],$request->data['order_detail']]);
        if($result)
        return FunctionLib::ajaxRespond(true, 'Hoàn thành', $result);
            else
        return FunctionLib::ajaxRespond(false, 'Không tìm thấy dữ liệu');
    }

    public function ajaxCheckNameIsset(CheckNameIssetRequest $request){
        $result = Apiato::call('Warehouse@CheckIssetNameAction',[$request->value]);
        if(!$result)
        return FunctionLib::ajaxRespond(true, 'Hoàn thành', $result);
            else
        return FunctionLib::ajaxRespond(false, 'Tên này đã tồn tại');
    }

    public function ajaxSaveWarehouse(SaveWarehouseRequest $request){
        $result = Apiato::call('Warehouse@SaveWarehouseAction',[$request->all()]);
        if($result)
        return FunctionLib::ajaxRespond(true, 'Hoàn thành', $result);
            else
        return FunctionLib::ajaxRespond(false, 'Lỗi');
    }

    public function ajaxWarehouseDelete(DeleteWarehouseRequest $request)
    {
      try {
          $delete = app(DeleteWarehouseAction::class)->run($request->id);
          return FunctionLib::ajaxRespond(true,'Hoàn thành',$delete);
      } catch (\Throwable $th) {
        //throw $th;
        return FunctionLib::ajaxRespond(false, 'Lỗi', $th);

      }
    }
}
