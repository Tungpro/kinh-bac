<?php

namespace App\Containers\Warehouse\Models;

use App\Ship\Parents\Models\Model;

class Warehouse extends Model
{   protected $table = 'warehouse';

    protected $fillable = [
        'name',
        'address',
        'p_code',
        'd_code',
        'phone',
        'code',
        'status',
        'created',
        'item_quantity'
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'warehouses';
}
