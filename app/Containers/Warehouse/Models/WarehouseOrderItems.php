<?php

namespace App\Containers\Warehouse\Models;

use App\Ship\Parents\Models\Model;
use App\Containers\Product\Models\ProductVariant;
use App\Containers\Product\Models\Product;

class WarehouseOrderItems extends Model
{   protected $table = 'warehouse_order_items';

    protected $fillable = [
        'warehouse_order_id',
        'product_id',
        'price',
        'quantity',
        'quantity_origin',
        'created',
        'product_variants_id'
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'warehouses';

    public function productDetail(){
        return $this->hasMany(ProductVariant::class,'product_id','product_id');
    }
    public function product(){
        return $this->hasOne(Product::class,'id','product_id');
    }
    public function product_variants(){
        return $this->hasMany(ProductVariant::class,'product_variant_id','product_variants_id');
    }
}
