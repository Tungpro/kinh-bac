<?php

namespace App\Containers\Warehouse\Tasks\WarehouseOrderItem;

use App\Containers\Warehouse\Data\Repositories\WarehouseOrderItemsRepository;
use App\Containers\Warehouse\Models\WarehouseOrderItems;
use App\Ship\Parents\Tasks\Task;

/**
 * Class GetAllProductssTask.
 */
class GetCartOrderToWarehouseOrderByPrdVariantsIDTask extends Task
{

    protected $repository;

    public function __construct(WarehouseOrderItemsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run(int $id)
    { 
        $result =  WarehouseOrderItems::selectRaw('SUM(warehouse_order_items.quantity_origin) as total_quantity')
        ->join('warehouse_order','warehouse_order.id','=','warehouse_order_items.warehouse_order_id')
        ->groupByRaw('warehouse_order_items.product_variants_id')
        ->where('warehouse_order_items.product_variants_id', $id)
        ->whereNotNull('warehouse_order.cart_order_id')->first();
        return isset($result) || !empty($result) ? $result->total_quantity : 0;
    }

    // public function equalTab($tab) {
    //     $this->repository->pushCriteria(new ByStatusCriteria($tab));
    // }

    // public function equalIdWarehouseOrder($id) {
    //     $this->repository->pushCriteria(new ByIdCriteria($id));
    // }

    // public function equalIdWarehouse($id){
    //     $this->repository->pushCriteria(new ByIdWarehouseCriteria($id));
    // }
}
