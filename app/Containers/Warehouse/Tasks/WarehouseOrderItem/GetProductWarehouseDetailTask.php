<?php

namespace App\Containers\Warehouse\Tasks\WarehouseOrderItem;

use App\Containers\Warehouse\Data\Repositories\WarehouseOrderItemsRepository;
use App\Containers\Warehouse\Models\WarehouseOrderItems;
use App\Ship\Parents\Tasks\Task;

/**
 * Class GetAllProductssTask.
 */
class GetProductWarehouseDetailTask extends Task
{

    protected $repository;

    public function __construct(WarehouseOrderItemsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run(int $id,  $warehouseID = '', $sku = '', $timePicker='', $limit=20)
    { 
        // dd($timePicker);
        $result =  WarehouseOrderItems::select('warehouse_order.warehouse_id','warehouse_order.code','warehouse_order.status','warehouse_order.created','warehouse_order.type','warehouse_order_items.*','warehouse.name as name_warehouse')
        ->join('warehouse_order','warehouse_order.id','=','warehouse_order_items.warehouse_order_id')
        ->leftJoin('warehouse','warehouse.id','=','warehouse_order.warehouse_id')
        ->where('product_variants_id', $id);
        if(!empty($warehouseID)){
            $result->where('warehouse_order.warehouse_id', $warehouseID);
        }
        if(!empty($sku)){
            $result->where('warehouse_order.code','LIKE', '%'.$sku.'%');
        }
        if(!empty($timePicker)){
            $from = strtotime($timePicker[0].' 00:00:00');
            $to = strtotime($timePicker[1].' 24:00:00');
            $result->whereBetween('warehouse_order.created', [$from, $to]);
        }
        return $result->paginate($limit);
    }

}
