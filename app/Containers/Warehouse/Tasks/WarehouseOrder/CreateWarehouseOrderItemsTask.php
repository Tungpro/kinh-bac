<?php

namespace App\Containers\Warehouse\Tasks\WarehouseOrder;

use App\Containers\Warehouse\Data\Repositories\WarehouseOrderItemsRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateWarehouseOrderItemsTask extends Task
{

    protected $repository;

    public function __construct(WarehouseOrderItemsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data, int $wh_id, $type)
    {
        try {
            $dataFn = [
                'warehouse_order_id' => $wh_id,
                'quantity' => $data['product_count'],
                'quantity_origin' => $type == 'In' ?  $data['product_count'] : -$data['product_count'],
                'price' => $data['product_price'],
                'product_id' => $data['id_product'],
                'product_variants_id' => @$data['product_variant'],
            ];
            return $this->repository->create($dataFn);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
