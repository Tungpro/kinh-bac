<?php

namespace App\Containers\Warehouse\Tasks\WarehouseOrder;

use App\Containers\Warehouse\Data\Repositories\WarehouseOrderRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindWarehouseOrderByIdTask extends Task
{

    protected $repository;

    public function __construct(WarehouseOrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
