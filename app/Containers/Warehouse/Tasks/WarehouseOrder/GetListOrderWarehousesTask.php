<?php

namespace App\Containers\Warehouse\Tasks\WarehouseOrder;

use App\Containers\Warehouse\Data\Repositories\WarehouseOrderRepository;

use App\Ship\Criterias\Eloquent\ByIdCriteria;
use App\Ship\Criterias\Eloquent\ByStatusCriteria;
use App\Ship\Criterias\Eloquent\ByIdWarehouseCriteria;
use App\Ship\Criterias\Eloquent\OrderByFieldCriteria;
use App\Ship\Criterias\Eloquent\WhereColumnCriteria;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;

/**
 * Class GetAllProductssTask.
 */
class GetListOrderWarehousesTask extends Task
{

    protected $repository;

    public function __construct(WarehouseOrderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($external_data, $skipPagination = false, $limit = 10)
    {
        return $skipPagination ? $this->repository->all() : $this->repository->paginate($limit);
    }

    public function equalTab($tab) {
        $this->repository->pushCriteria(new ByStatusCriteria($tab));
    }
    public function typeTab($type){
        $this->repository->pushCriteria(new WhereColumnCriteria(['type' => $type]));
    }

    public function equalIdWarehouseOrder($id) {
        $this->repository->pushCriteria(new ByIdCriteria($id));
    }

    public function equalIdWarehouse($id){
        $this->repository->pushCriteria(new ByIdWarehouseCriteria($id));
    }

    public function orderByWarehouse($sortOrder){
      $this->repository->pushCriteria(new OrderByFieldCriteria('id',$sortOrder));
  }
}
