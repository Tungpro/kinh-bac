<?php

namespace App\Containers\Warehouse\Tasks;

use App\Containers\Warehouse\Data\Repositories\WarehouseRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CheckIssetNameTask extends Task
{

    protected $repository;

    public function __construct(WarehouseRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($input)
    {
        try {
            return $this->repository->where('name',$input)->count();
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
