<?php

namespace App\Containers\Testimonial\Models;

use Apiato\Core\Foundation\ImageURL;
use App\Ship\core\Traits\HelpersTraits\LangTrait;
use App\Ship\Parents\Models\Model;

class Testimonial extends Model
{
    use LangTrait;

    const TABLE_NAME = 'testimonial';
    protected $table = self::TABLE_NAME;

    protected $fillable = [
        'status', 'sort_order', 'image', 'type', 'embed_code', 'created_by', 'updated_by',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $resourceKey = 'testimonial';

    /*relationship*/
    public function all_desc()
    {
        return $this->hasManyKeyBy('language_id', TestimonialDesc::class, 'testimonial_id', 'id');
    }

    public function desc()
    {
        return $this->hasOne(TestimonialDesc::class, 'testimonial_id', 'id');
    }

    /*function */
    public function getImageUrl($size = 'small')
    {
        return ImageURL::getImageUrl($this->image, 'testimonial', $size);
    }

}
