<?php

namespace App\Containers\Testimonial\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class TestimonialDesc extends Model
{
    const TABLE_NAME = 'testimonial_desc';
    protected $table = self::TABLE_NAME;

    protected $fillable = [
        'testimonial_id', 'language_id', 'name', 'short_description', 'description',
    ];

    protected $resourceKey = 'testimonial_desc';

    public function language()
    {
        return $this->hasOne(Language::class, 'language_id', 'language_id');
    }

}
