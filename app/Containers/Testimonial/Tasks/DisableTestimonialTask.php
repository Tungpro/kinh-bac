<?php

namespace App\Containers\Testimonial\Tasks;

use App\Containers\Testimonial\Data\Repositories\TestimonialRepository;
use App\Ship\core\Foundation\BladeHelper;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DisableTestimonialTask extends Task
{
    protected $repository;

    public function __construct(TestimonialRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->getModel()->where('id', $id)->update(['status' => BladeHelper::AN]);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
