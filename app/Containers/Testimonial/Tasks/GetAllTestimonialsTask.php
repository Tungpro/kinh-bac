<?php

namespace App\Containers\Testimonial\Tasks;

use App\Containers\BaseContainer\Traits\ScopeAdminBaseSearchTrait;
use App\Containers\BaseContainer\Traits\ScopeFEAvailableDataTrait;
use App\Containers\Testimonial\Data\Repositories\TestimonialRepository;
use App\Ship\Criterias\Eloquent\WhereColumnCriteria;
use App\Ship\Parents\Tasks\Task;

class GetAllTestimonialsTask extends Task
{
    use ScopeAdminBaseSearchTrait;
    use ScopeFEAvailableDataTrait;

    protected $repository;

    public function __construct(TestimonialRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($request, $limit = 20)
    {

        return $this->returnDataByLimit($limit);
    }

    public function extraSearchCondition($request)
    {
        if (isset($request->type)) {
            $this->repository->pushCriteria(new WhereColumnCriteria(['type' => $request->type]));
        }

    }

}
