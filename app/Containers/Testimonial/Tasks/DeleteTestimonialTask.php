<?php

namespace App\Containers\Testimonial\Tasks;

use App\Containers\Testimonial\Data\Repositories\TestimonialRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteTestimonialTask extends Task
{
    protected $repository;

    public function __construct(TestimonialRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            $examCategory = $this->repository->find($id);

            return $examCategory->delete();
        } catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
