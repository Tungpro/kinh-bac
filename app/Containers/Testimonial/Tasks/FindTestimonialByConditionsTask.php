<?php

namespace App\Containers\Testimonial\Tasks;

use App\Containers\Testimonial\Data\Repositories\TestimonialRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindTestimonialByConditionsTask extends Task
{
    protected $repository;

    public function __construct(TestimonialRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        try {
            return $this->repository->first();
        } catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
