<?php

namespace App\Containers\Testimonial\Tasks;

use App\Containers\Testimonial\Data\Repositories\TestimonialRepository;
use App\Ship\core\Foundation\BladeHelper;
use App\Ship\Parents\Tasks\Task;
use Exception;

class EnableTestimonialTask extends Task
{
    protected $repository;

    public function __construct(TestimonialRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->getModel()->where('id', $id)->update(['status' => BladeHelper::HIEN_THI]);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
