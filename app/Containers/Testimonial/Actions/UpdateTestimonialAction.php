<?php

namespace App\Containers\Testimonial\Actions;

use App\Containers\Testimonial\Data\Repositories\TestimonialDescRepository;
use App\Containers\Testimonial\Models\Testimonial;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;

class UpdateTestimonialAction extends Action
{
    public function run($request)
    {
        $data = Arr::only($request, [
            'id', 'description', 'status', 'sort_order', 'image', 'type', 'embed_code',
        ]);
        $data['updated_by'] = auth()->id();

        $objBefore = Apiato::call('Testimonial@FindTestimonialByIdTask', [$data['id']], [['with' => [['all_desc']]]]);
        $objUpdated = Apiato::call('Testimonial@UpdateTestimonialTask', [$data['id'], Arr::except($data, ['description'])]);

        if ($objUpdated) {
            $originalDesc = Apiato::call('BaseContainer@Admin\GetAllModelDescTask', [app(TestimonialDescRepository::class), $objUpdated->id, 'testimonial_id']);
            Apiato::call('BaseContainer@Admin\SaveModelDescTask', [app(TestimonialDescRepository::class), $data, $originalDesc, $objUpdated->id, 'testimonial_id']);

            // Create Log
            Apiato::call('User@CreateUserLogSubAction', [
                $objUpdated->id,
                $objBefore->toArray(),
                $objUpdated->load('all_desc')->toArray(),
                'Sửa Hỏi đáp cổ đông',
                Testimonial::class
            ]);
        }

        $this->clearCache();

        return $objUpdated;
    }
}
