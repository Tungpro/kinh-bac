<?php

namespace App\Containers\Testimonial\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class FindTestimonialByIdAction extends Action
{
    public function run($id, array $withData = [], array $conditions = [], $selectFields = ['*'])
    {
        return Apiato::call('Testimonial@FindTestimonialByIdTask', [$id], [
            ['with' => [$withData]],
            ['where' => [$conditions]],
            ['selectFields' => [$selectFields]]
        ]);
    }
}
