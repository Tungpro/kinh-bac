<?php

namespace App\Containers\Testimonial\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Testimonial\Models\Testimonial;
use App\Ship\Parents\Actions\Action;

class EnableTestimonialAction extends Action
{
    public function run($data)
    {
        $object = Apiato::call('Testimonial@EnableTestimonialTask', [$data->id]);

        Apiato::call('User@CreateUserLogSubAction', [
            $data->id,
            [],
            [],
            'Hiển thị Hỏi đáp cổ đông',
            Testimonial::class
        ]);

        $this->clearCache();

        return $object;
    }
}
