<?php

namespace App\Containers\Testimonial\Actions;

use App\Containers\Testimonial\Data\Repositories\TestimonialDescRepository;
use App\Containers\Testimonial\Models\Testimonial;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class DeleteTestimonialAction extends Action
{
    public function run($id)
    {
        $deleted = Apiato::call('Testimonial@DeleteTestimonialTask', [$id]);

        if ($deleted) {
            Apiato::call('BaseContainer@Admin\DeleteModelDescTask', [app(TestimonialDescRepository::class), $id, 'testimonial_id']);

            Apiato::call('User@CreateUserLogSubAction', [
                $id,
                [],
                [],
                'Xóa Hỏi đáp cổ đông',
                Testimonial::class
            ]);

            $this->clearCache();

            return $deleted;
        }

        return false;
    }
}
