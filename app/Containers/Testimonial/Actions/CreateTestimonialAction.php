<?php

namespace App\Containers\Testimonial\Actions;

use App\Containers\Testimonial\Data\Repositories\TestimonialDescRepository;
use App\Containers\Testimonial\Models\Testimonial;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;
use Illuminate\Support\Arr;

class CreateTestimonialAction extends Action
{
    public function run($request)
    {
        $data = Arr::only($request, [
            'description', 'status', 'sort_order', 'image', 'type', 'embed_code',
        ]);
        $data['created_by'] = auth()->id();

        $objCreated = Apiato::call('Testimonial@CreateTestimonialTask', [Arr::except($data, ['description'])]);

        if ($objCreated) {
            // Create Desc
            Apiato::call('BaseContainer@Admin\SaveModelDescTask', [app(TestimonialDescRepository::class), $data, [], $objCreated->id, 'testimonial_id']);

            // Create Log
            Apiato::call('User@CreateUserLogSubAction', [
                $objCreated->id,
                [],
                $objCreated->load('all_desc')->toArray(),
                'Tạo Hỏi đáp cổ đông',
                Testimonial::class
            ]);
        }

        $this->clearCache();

        return $objCreated;
    }
}
