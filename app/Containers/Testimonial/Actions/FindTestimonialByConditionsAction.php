<?php

namespace App\Containers\Testimonial\Actions;

use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class FindTestimonialByConditionsAction extends Action
{
    public function run(array $conditions = [], array $withData = [], $selectFields = ['*'])
    {
        return Apiato::call('Testimonial@FindTestimonialByConditionsTask', [], [
            ['where' => [$conditions]],
            ['with' => [$withData]],
            ['selectFields' => [$selectFields]]
        ]);
    }
}
