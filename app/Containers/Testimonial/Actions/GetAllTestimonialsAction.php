<?php

namespace App\Containers\Testimonial\Actions;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;
use Apiato\Core\Foundation\Facades\Apiato;

class GetAllTestimonialsAction extends Action
{
    public function run($request, $limit = 20,
                        ?Language $currentLang = null,
                        $with = [], $conditions = [], $selectFields = ['*'], $orderBy = ['sort_order' => 'ASC', 'id' => 'DESC']
    )
    {
        return $this->remember(function () use ($request, $limit, $currentLang, $with, $conditions, $selectFields, $orderBy) {

            return Apiato::call('Testimonial@GetAllTestimonialsTask', [$request, $limit], [
                ['currentLang' => [$currentLang]],

                ['with' => [$with]],
                ['where' => [$conditions]],
                ['selectFields' => [$selectFields]],
                ['orderBy' => [$orderBy]],

                ['scopeAdminBaseSearch' => [$request]],
                ['scopeFEAvailableData' => [$request]],
            ]);
        }, null, [], 0, $request->skipCache ?? true);
    }
}
