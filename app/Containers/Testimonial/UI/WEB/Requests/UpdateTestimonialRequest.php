<?php

namespace App\Containers\Testimonial\UI\WEB\Requests;

use App\Containers\BaseContainer\Traits\RequestBaseLanguage;
use App\Containers\Testimonial\Models\Testimonial;
use App\Ship\Parents\Requests\Request;

class UpdateTestimonialRequest extends Request
{
    use RequestBaseLanguage;
    protected $access = [
        'permissions' => 'testimonial-edit',
        'roles' => 'admin',
    ];

    protected $decode = [
        // 'id',
    ];

    protected $urlParameters = [
        // 'id',
    ];

    public function rules()
    {
        return [
            'id' => ['required', sprintf('exists:%s,id', Testimonial::TABLE_NAME)],

//            'description.*.name' => 'bail|required|string',
            'description.*.short_description' => 'bail|nullable|string',
            'description.*.description' => 'bail|nullable|string|max:500',

            'status' => 'bail|nullable|numeric|in:1,2',
            'sort_order' => 'bail|nullable|numeric|min:0',

            'type' => ['bail', 'required', 'in:' . implode(',', array_keys(config('testimonial-container.type')))],
            'embed_code' => 'bail|nullable|min:0',

            'image' => 'bail|nullable|mimes:jpeg,jpg,png|max:15360',
        ];
    }

    public function attributes()
    {
        return [
                'description.*.short_description' => 'Chức vụ',
                'description.*.description' => 'Nội dung',

                'status' => 'Trạng thái',
                'sort_order' => 'Sắp xếp',

                'type' => 'Loại hiển thị',
                'embed_code' => 'Mã nhúng video',

                'image' => 'Hình ảnh',
            ] + $this->messagesLang([
//                'description.*.name' => 'Tên language không được bỏ trống',
            ]);
    }

    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
