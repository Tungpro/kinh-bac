<?php

namespace App\Containers\Testimonial\UI\WEB\Requests;

use App\Containers\Testimonial\Models\Testimonial;
use App\Ship\Parents\Requests\Request;

class UpdateStatusTestimonialRequest extends Request
{
    protected $access = [
        'permissions' => 'testimonial-edit',
        'roles' => 'admin',
    ];

    protected $decode = [];

    protected $urlParameters = [
        'id',
    ];

    public function rules()
    {
        return [
            'id' => ['required', sprintf('exists:%s,id', Testimonial::TABLE_NAME)],

        ];
    }

    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
