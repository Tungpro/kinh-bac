<?php

namespace App\Containers\Testimonial\UI\WEB\Controllers\Admin;

use Apiato\Core\Foundation\FunctionLib;
use App\Containers\Testimonial\UI\WEB\Requests\CreateTestimonialRequest;
use App\Containers\Testimonial\UI\WEB\Requests\DeleteTestimonialRequest;
use App\Containers\Testimonial\UI\WEB\Requests\GetAllTestimonialsRequest;
use App\Containers\Testimonial\UI\WEB\Requests\UpdateTestimonialRequest;
use App\Containers\Testimonial\UI\WEB\Requests\StoreTestimonialRequest;
use App\Containers\Testimonial\UI\WEB\Requests\EditTestimonialRequest;
use App\Containers\Testimonial\UI\WEB\Requests\UpdateStatusTestimonialRequest;
use App\Ship\Parents\Controllers\AdminController;
use Apiato\Core\Foundation\Facades\Apiato;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class Controller extends AdminController
{
    protected $containerName = 'testimonial';

    public function __construct()
    {
        $this->title = 'Hỏi đáp cổ đông';
        $this->imageField = 'image';
        $this->imageKey = 'testimonial';
        $this->actions = [
            'list' => 'Testimonial@GetAllTestimonialsAction',
            'create' => 'Testimonial@CreateTestimonialAction',
            'edit' => 'Testimonial@FindTestimonialByIdAction',
            'update' => 'Testimonial@UpdateTestimonialAction',
            'delete' => 'Testimonial@DeleteTestimonialAction',
        ];
        $this->routes = [
            'list' => 'admin.testimonial.index',
            'create' => 'admin.testimonial.create',
            'store' => 'admin.testimonial.store',
            'edit' => 'admin.testimonial.edit',
            'update' => 'admin.testimonial.update',
            'destroy' => 'admin.testimonial.destroy',
            'disable' => "admin.$this->containerName.disable",
            'enable' => "admin.$this->containerName.enable",
        ];
        View::share('routes', $this->routes);
        View::share('containerName', $this->containerName);

        parent::__construct();
    }

    public function index(GetAllTestimonialsRequest $request)
    {
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['list', $this->title]);

        $request->merge(['scopeAdminBaseSearch' => true]);
        $data = Apiato::call($this->actions['list'], [$request, $this->perPage, null, ['desc'], [], ['*'], ['type' => 'ASC', 'sort_order' => 'ASC']]);

        $types = array_merge(['' => '-- Loại hiển thị --'], config('testimonial-container.type'));

        return view("$this->containerName::Admin.index", [
            'request' => $request,
            'data' => $data,
            'types' => $types,
        ]);
    }

    public function create(CreateTestimonialRequest $request)
    {
        $this->showAddForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['add', $this->title, $this->routes['list']]);

        return view("$this->containerName::Admin.form", [
            'types' => config('testimonial-container.type'),
        ]);
    }

    public function store(StoreTestimonialRequest $request)
    {
        $this->editMode = false;
        return $this->save($request);
    }

    public function edit($id, EditTestimonialRequest $request)
    {
        $this->showEditForm();
        Apiato::call('BaseContainer@CreateBreadcrumbAction', ['edit', $this->title, $this->routes['list']]);

        $editedObj = Apiato::call($this->actions['edit'], [$id, ['all_desc']]);

        if ($editedObj) {

            return view("$this->containerName::Admin.form", [
                'data' => $editedObj,
                'types' => config('testimonial-container.type'),
            ]);
        }

        return redirect()->route($this->routes['list']);
    }

    public function update(UpdateTestimonialRequest $request)
    {
        $this->editMode = true;
        return $this->save($request);
    }

    public function destroy($id, DeleteTestimonialRequest $request)
    {
        try {
            Apiato::call($this->actions['delete'], [$id]);

            return FunctionLib::ajaxRespondV2(true, 'Xóa bản ghi thành công!');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình xóa dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function enable(UpdateStatusTestimonialRequest $request)
    {
        try {
            Apiato::call('Testimonial@EnableTestimonialAction', [$request]);

            return FunctionLib::ajaxRespondV2(true, 'Hiện bản ghi thành công!');
        } catch (Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình sửa dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }

    public function disable(UpdateStatusTestimonialRequest $request)
    {
        try {
            Apiato::call('Testimonial@DisableTestimonialAction', [$request]);

            return FunctionLib::ajaxRespondV2(true, 'Ẩn bản ghi thành công!');
        } catch (Exception $e) {
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình sửa dữ liệu! Vui lòng thử lại! ' . $e->getMessage()]);
        }
    }
}
