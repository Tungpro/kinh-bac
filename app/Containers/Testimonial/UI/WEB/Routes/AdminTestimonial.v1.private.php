<?php

/** @var Route $router */
$router->group([
    'middleware' => ['auth:admin'],
    'namespace' => '\App\Containers\Testimonial\UI\WEB\Controllers\Admin',
    'domain' => sprintf('admin.%s', parse_url(config('app.url'))['host'])
], function () use ($router) {
    $router->resource('loi-chung-thuc', 'Controller')
        ->names([
            'index' => 'admin.testimonial.index',
            'create' => 'admin.testimonial.create',
            'store' => 'admin.testimonial.store',
            'edit' => 'admin.testimonial.edit',
            'update' => 'admin.testimonial.update',
            'destroy' => 'admin.testimonial.destroy',
        ])
        ->except(['show']);

    $router->post('/loi-chung-thuc/enable/{id}', ['as' => 'admin.testimonial.enable', 'uses' => 'Controller@enable',]);
    $router->post('/loi-chung-thuc/disable/{id}', ['as' => 'admin.testimonial.disable', 'uses' => 'Controller@disable',]);
});
