<div class="tab-pane active" id="data">
    <div class="tabbable">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="status">Trạng thái <small class="text-danger">(Hiển thị hay không)</small></label>
                    <div class="input-group">
                        <select id="status" name="status"
                                class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}">
                            @if(!empty(\App\Ship\core\Foundation\BladeHelper::STATUS_TEXT))
                                @foreach(\App\Ship\core\Foundation\BladeHelper::STATUS_TEXT as $statusKey => $statusText)
                                    <option value="{{$statusKey}}" {{ old('status', @$data->status) == $statusKey ? 'selected' : '' }}>{{$statusText}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="type">Loại hiển thị <span class="text-danger">*</span></label>
                    <div class="input-group">
                        <select id="type" name="type"
                                class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}">
                            @if(isset($types) && !empty($types))
                                @foreach($types as $typeId => $typeText)
                                    <option value="{{$typeId}}" {{$typeId === @$data->type ? 'selected' : ''}}>{{@$typeText}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
{{--                <div class="form-group">--}}
{{--                    <label for="embed_code">Mã nhúng video cho <span--}}
{{--                                class="text-danger">Hỏi đáp cổ đông video</span></label>--}}
{{--                    <div class="input-group">--}}
{{--                        <textarea name="embed_code" id="embed_code"--}}
{{--                                  class="form-control {{ $errors->has("embed_code") ? 'is-invalid' : '' }}"--}}
{{--                                  placeholder="Mã nhúng video" rows="5"--}}
{{--                        >{!! old("embed_code", @$data['embed_code']) !!}</textarea>--}}
{{--                    </div>--}}
{{--                </div>--}}

                <div class="form-group">
                    <label for="sort_order">Sắp xếp <small class="text-danger">(Vị trí, càng nhỏ thì càng lên
                            đầu)</small></label>
                    <div class="input-group">
                        <input type="number" class="form-control {{ $errors->has('sort_order') ? 'is-invalid' : '' }}"
                               min="0" placeholder="0"
                               name="sort_order" id="sort_order"
                               value="{{ old('sort_order', @$data->sort_order) }}">
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>