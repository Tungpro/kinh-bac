<?php

namespace App\Containers\Testimonial\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class TestimonialDescRepository extends Repository
{
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
