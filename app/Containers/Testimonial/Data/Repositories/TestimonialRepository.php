<?php

namespace App\Containers\Testimonial\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class TestimonialRepository extends Repository
{
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];

}
