<?php
/**
 * Created by PhpStorm.
 * Filename: CategoryFilter.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 8/18/20
 * Time: 23:27
 */

namespace App\Containers\Category\Models;

use App\Models\Filter\FilterGroup;
use App\Ship\Parents\Models\Model;

class CategoryFilterGroup extends Model
{
    protected $table = 'category_filter';

    public function category()
    {
        return $this->hasOne(Category::class,'category_id','category_id');
    }

    public function filter_group()
    {
        return $this->hasOne(FilterGroup::class,'filter_group_id','filter_group_id');
    }


//    public function landing() {
//        return $this->belongsToMany(CustomerLanding::class, 'tbl_exams_landings', 'exam_id', 'landing_id');
//    }
}