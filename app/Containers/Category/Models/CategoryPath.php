<?php
/**
 * Created by PhpStorm.
 * Filename: CategoryPath.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 8/11/20
 * Time: 14:27
 */

namespace App\Containers\Category\Models;

use App\Ship\Parents\Models\Model;

class CategoryPath extends Model
{
    protected $table = 'category_path';
}