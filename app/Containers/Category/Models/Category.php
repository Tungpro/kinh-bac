<?php

namespace App\Containers\Category\Models;

use App\Containers\Category\Enums\CategoryStatus;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\Filter\Models\FilterGroup;
use App\Containers\Product\Models\Product;
use App\Containers\News\Models\News;
use App\Containers\News\Models\NewsCategory;
use App\Containers\Product\Models\ProductCategory;
use App\Ship\Parents\Models\Model;
use Illuminate\Support\Facades\DB;
use Apiato\Core\Foundation\ImageURL;
use Illuminate\Support\Str;

class Category extends Model
{
    //
    use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;
    protected $table = 'category';
    protected $primaryKey = 'category_id';
    protected static $cat = [];
    protected static $splitKey = '_';


    protected $fillable = [
        'image',
        'icon',
        'icon2',
        'parent_id',
        'type',
        'top',
        'hot',
        'is_home',
        'sort_order',
        'status',
        'primary_color',
        'second_color',
        'position'
    ];

    public function desc()
    {
        return $this->hasOne(CategoryDesc::class, 'category_id', 'category_id');
    }

    public function all_desc()
    {
        return $this->hasMany(CategoryDesc::class, 'category_id', 'category_id');
    }

    public function filter_groups()
    {
        return $this->belongsToMany(FilterGroup::class, CategoryFilterGroup::getTableName(), 'category_id', 'filter_group_id');
    }

    public function news()
    {
        return $this->belongsToMany(News::class, NewsCategory::getTableName(), 'category_id', 'news_id');
    }

    public function lang()
    {
        $lang = config('app.locales');
        return isset($lang[$this->lang]) ? $lang[$this->lang] : 'vi';
    }

    public function type()
    {
        return isset(self::$type[$this->type]) ? self::$type[$this->type] : '1';
    }

    public function getImageUrl($size = 'medium')
    {
        return ImageURL::getImageUrl($this->image, 'category', $size);
    }

    public function getImageUrlByImg($img = '', $size = 'medium')
    {
        return ImageURL::getImageUrl($img, 'category', $size);
    }


    public static function getType()
    {
        return self::$type;
    }

    public static function getCateById($id, $type = 1, $with_count = false)
    {
        $wery = self::where('id', $id)->where('status', '>', 0)->where('type', $type);

        if ($with_count) {
            $wery->withCount(['products' => function ($q) {
                $q->where('status', 2);
            }]);
        }

        return $wery->first();
    }

    public static function getCateIds($cate_ids = [], &$output = [])
    {
//        $output = array_merge($cate_ids,$output);
        $wery = DB::table('categories');
        $wery->select('id', 'pid');
        $wery->whereIn('pid', $cate_ids);
        $result = $wery->get();

        if (!empty($result)) {
            foreach ($result as $item) {
                $output[] = $item->id;
                self::getCateIds([$item->id], $output);
            }
        }
    }

    public static function getAllChilds($cate_ids = [], &$output = [])
    {
        $wery = DB::table('categories');
        $wery->select('id', 'pid');
        $wery->whereIn('id', $cate_ids);
        $result = $wery->get();

        if (!empty($result)) {
            $has_pid = [];
            foreach ($result as $item) {
                if ($item->pid != 0) {
                    $has_pid[] = $item->pid;
                }
                $output[] = $item->id;
            }
            if (!empty($has_pid)) {
                self::getCateIds($has_pid, $output);
            }
        }
    }

    public static function getIsHomeCate()
    {
        $data = self::where('status', 1);
        $data->where('is_home', 1);
        $data->where('status', '>', 0);
        $data->orderBy('sort');
        return $data->get();
    }

    public function sixProducts()
    {
        return $this->hasMany(ProductCategory::class, 'category_id', 'category_id')->nPerGroup('product_id', 8);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, ProductCategory::getTableName(), 'category_id', 'product_id');
    }

    public function withProductLimit($currentLang, $paginate, $column = ['*'], $pageParam = 'p_a_g_e')
    {
        return $this->products()
            ->where('products.status', 2)
            ->orderBy('products.sort_order', 'ASC')
            ->orderBy('products.id', 'DESC')
            ->with(['desc' => function ($q1) use ($currentLang) {
                $q1->select('id', 'product_id', 'language_id', 'name', 'slug');
                $q1->activeLang($currentLang ? $currentLang->language_id : 1);
            }])
            ->paginate($paginate, $column, $pageParam);
    }

    /*scope*/
    public function scopeAvailable($query, array $positions = [])
    {
        return $query->where('status', 2)
            ->where(function ($query) use ($positions) {
                foreach ($positions as $position) {
                    $query->orWhereRaw("LOCATE('{$position}', position) > 0");
                }
            });
    }


    public function link()
    {
        if(!empty($this->desc->link)){
            return $this->desc->link ;
        }

        if ($this->type === CategoryType::PRODUCT) {
            return false;
        } else if ($this->type === CategoryType::NEWS) {
            return route('web.news.category-detail', ['slug' => !empty($this->desc->slug) ? $this->desc->slug : Str::slug($this->desc->name), 'id' => $this->category_id]);
        } else if ($this->type === CategoryType::PROJECT) {
            return route('web.project.category-detail', ['slug' => !empty($this->desc->slug) ? $this->desc->slug : Str::slug($this->desc->name), 'id' => $this->category_id]);
        }else if($this->type === CategoryType::HOLDER){
              return route('web.holder.category-detail', ['slug' => !empty($this->desc->slug) ? $this->desc->slug : Str::slug($this->desc->name), 'id' => $this->category_id]);
        }

        return null;
    }


    public static function linkCatByType($type, $slug, $id)
    {
        if(!empty($slug)) {
            if ($type === CategoryType::PRODUCT) {
                return false;
            } else if ($type === CategoryType::NEWS) {
                return route('web.news.category-detail', ['slug' => $slug, 'id' => $id]) . '#category';
            } else if ($type === CategoryType::PROJECT) {
                return route('web.project.category-detail', ['slug' => $slug, 'id' => $id]);
            } else if ($type === CategoryType::RECRUIT) {
                return null;
            } else if ($type === CategoryType::HOLDER) {
                return route('web.holder.category-detail', ['slug' => $slug, 'id' => $id]);
            }
        }

        return null;
    }
}
