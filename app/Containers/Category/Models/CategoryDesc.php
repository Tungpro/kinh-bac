<?php
/**
 * Created by PhpStorm.
 * Filename: CategoryDesc.php
 * User: Oops!Memory - OopsMemory.com
 * Date: 8/10/20
 * Time: 15:59
 */

namespace App\Containers\Category\Models;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Models\Model;

class CategoryDesc extends Model
{
    protected $table = 'category_description';
    protected $primaryKey = ['category_id', 'language_id'];
    public $incrementing = false;

    public function language()
    {
        return $this->hasOne(Language::class,'language_id','language_id');
    }
}