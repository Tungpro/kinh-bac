<?php

namespace App\Containers\Category\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class CategoryDescRepository.
 */
class CategoryDescRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Category';

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name',
        'category_id',
        'language_id',
    ];
}
