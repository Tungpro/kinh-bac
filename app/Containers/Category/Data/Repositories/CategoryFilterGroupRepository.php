<?php

namespace App\Containers\Category\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

/**
 * Class CategoryFilterGroupRepository.
 */
class CategoryFilterGroupRepository extends Repository
{

    /**
     * the container name. Must be set when the model has different name than the container
     *
     * @var  string
     */
    protected $container = 'Category';

    /**
     * @var array
     */
    protected $fieldSearchable = [
       
    ];
}
