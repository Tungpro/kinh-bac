<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-04 16:55:35
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-06 14:06:44
 * @ Description: Happy Coding!
 */

use App\Containers\Localization\Actions\CheckSegmentLanguageAction;

Route::group(
[
    'middleware' => [
        // 'api',
    ],
    'prefix' => app(CheckSegmentLanguageAction::class)->run().'/category',
],
function () use ($router) {
    $router->any('/getAllShippingCategory', [
        'as' => 'api_list_category_shipping_page',
        'uses'       => 'FrontEnd\Controller@getAllShippingCategory'
    ]);

    $router->any('/getAllPromotionCategory', [
        'as' => 'api_list_category_promotion_page',
        'uses'       => 'FrontEnd\Controller@getAllPromotionCategory'
    ]);

    $router->any('/getAllHotCategory', [
        'as' => 'api_list_category_hot_page',
        'uses'       => 'FrontEnd\Controller@getAllHotCategory'
    ]);
});