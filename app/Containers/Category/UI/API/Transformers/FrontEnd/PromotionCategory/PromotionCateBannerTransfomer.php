<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-10-02 11:43:56
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-03 23:59:06
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\UI\API\Transformers\FrontEnd\PromotionCategory;

use Apiato\Core\Foundation\ImageURL;
use App\Ship\Parents\Transformers\Transformer;

class PromotionCateBannerTransfomer extends Transformer
{
    public $bannerSize;
    public function __construct(string $bannerSize = 'large')
    {
        $this->bannerSize = $bannerSize;
        parent::__construct();
    }

    public function transform($banner)
    {
        $data = [
            'id' => $banner->id,
            'category_id' => $banner->category_id,
            'name' => $banner->desc->name,
            'short_description' => $banner->desc->short_description,
            'image' => ImageURL::getImageUrl($banner->desc->image,'banner',$this->bannerSize),
            'link' => $banner->desc->link
        ];
        return $data;
    }
}
