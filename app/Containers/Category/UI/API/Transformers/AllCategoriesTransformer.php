<?php

namespace App\Containers\Category\UI\API\Transformers;

use App\Ship\Parents\Transformers\Transformer;
use Illuminate\Support\Facades\Config;

/**
 * Class AllCategoriesTransformer.
 *
 */
class AllCategoriesTransformer extends Transformer
{

    /**
     * @param $token
     *
     * @return  array
     */
    public function transform($category)
    {
        $response = [
            'category_id' => $category->category_id,
            'self_name'       => $category->self_name,
            'name' =>   $category->name,
            'parent_id' => $category->parent_id
        ];

        return $response;
    }
}
