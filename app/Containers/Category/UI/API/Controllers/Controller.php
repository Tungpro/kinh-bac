<?php

namespace App\Containers\Category\UI\API\Controllers;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Containers\Category\UI\API\Requests\GetAllCategoriesRequest;
use App\Containers\Category\UI\API\Transformers\AllCategoriesTransformer;
use App\Containers\Category\UI\API\Transformers\CategoriesSelect2Transformer;
use App\Ship\Parents\Controllers\ApiController;
use App\Ship\Transporters\DataTransporter;

/**
 * Class Controller.
 */
class Controller extends ApiController
{
    public function getAllCategories(GetAllCategoriesRequest $request)
    {
        $categories = Apiato::call('Category@Admin\GetAllCategoriesAction',[$request->all(),30]);
        $type = $request->type;

        if($type && $type == 'select2') {
            return $this->transform($categories, CategoriesSelect2Transformer::class, [], [], 'category');
        }else {
            return $this->transform($categories, AllCategoriesTransformer::class,[],[],'category');
        }
    }

}
