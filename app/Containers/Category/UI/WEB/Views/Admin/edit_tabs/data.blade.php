<div class="tab-pane" id="data">
    <div class="tabbable">
        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="status">Loại danh mục</label>
            <div class="col-sm-3">
                <select class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type" id="type">
                    @php($type = \App\Containers\Category\Enums\CategoryType::TYPE)
                    @foreach($type as $key => $item)
                    <option value="{{$key}}" {{ old('type', @$data->type) == $key ? 'selected' : '' }}>{{$item}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="status">Trạng thái <span
                        class="d-block small text-danger">(Hiển thị hay không)</span></label>
            <div class="col-sm-3">
                <select class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status"
                        id="status">
                    @if(!empty(\App\Ship\core\Foundation\BladeHelper::STATUS_TEXT))
                        @foreach(\App\Ship\core\Foundation\BladeHelper::STATUS_TEXT as $statusKey => $statusText)
                            <option value="{{$statusKey}}" {{ old('status', @$data->status) == $statusKey ? 'selected' : '' }}>{{$statusText}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="sort_order">Hiển thị tại trang chủ<span
                    class="d-block small text-danger">(Có hay không)</span></label>
            <div class="col-sm-3">
                <select class="form-control{{ $errors->has('is_home') ? ' is-invalid' : '' }}" name="is_home" id="is_home">
                    @if(!empty(\App\Ship\core\Foundation\BladeHelper::STATUS_YES_NO))
                        @foreach(\App\Ship\core\Foundation\BladeHelper::STATUS_YES_NO as $statusKey => $statusText)
                            <option value="{{$statusKey}}" {{ old('is_home', @$data->is_home) == $statusKey ? 'selected' : '' }}>{{$statusText}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="row form-group align-items-center">
            <label class="col-sm-2 control-label text-right mb-0" for="sort_order">Sắp xếp <span
                        class="d-block small text-danger">(Vị trí, càng nhỏ thì càng lên đầu)</span></label>
            <div class="col-sm-3">
                <input type="text" name="sort_order"
                       value="{{ old('sort_order', FunctionLib::numberFormat(@$data->sort_order)) }}"
                       placeholder="Vị trí sắp xếp" id="sort_order" class="form-control"
                       onkeypress="return shop.numberOnly()" onkeyup="mixMoney(this)" onfocus="this.select()">
            </div>
        </div>

        {{--        <div class="row form-group align-items-center">
                    <label class="col-sm-2 control-label text-right mb-0" for="primary_color">Màu chủ đạo <span
                        class="d-block small text-danger">(Màu sắc chủ đạo của danh mục)</span></label>
                    <div class="col-sm-1">
                        <input type="color" class="form-control{{ $errors->has('primary_color') ? ' is-invalid' : '' }}" id="primary_color" name="primary_color" value="{{ old('primary_color', @$data->primary_color) }}">
                    </div>
                </div>
                <div class="row form-group align-items-center">
                    <label class="col-sm-2 control-label text-right mb-0" for="second_color">Màu thứ cấp <span
                        class="d-block small text-danger">(Màu sắc thứ cấp của danh mục)</span></label>
                    <div class="col-sm-1">
                        <input type="color" class="form-control{{ $errors->has('second_color') ? ' is-invalid' : '' }}" id="second_color" name="second_color" value="{{ old('second_color', @$data->second_color) }}">
                    </div>
                </div>--}}
{{--        <div class="row form-group align-items-center">--}}
{{--            <label class="col-sm-2 control-label text-right mb-0" for="input-filter"><span data-toggle="tooltip"--}}
{{--                                                                                           title="Bộ lọc">Nhóm bộ lọc</span></label>--}}
{{--            <div class="col-sm-6">--}}
{{--                <input type="text" placeholder="Nhóm bộ lọc" id="input-filter" class="form-control"/>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-6">
                <div id="category_filter_group" class="list-group flex-row">
                    @if (isset($data->filter_groups))
                        @foreach ($data->filter_groups as $item)
                            <a href="#" id="category-filter{{ $item->filter_group_id }}"
                               class="col-3 list-group-item list-group-item-action">
                                <i class="fa fa-close"></i> {{ $item->name }} <br/><small
                                        class="text-danger">{{$item->short_description}}</small>
                                <input type="hidden" name="category_filter_group[]"
                                       value="{{ $item->filter_group_id }}"/>
                            </a>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>

{{--        <div class="row form-group list-position">--}}
{{--            <label class="col-sm-2 control-label text-right mb-0 align-text-top">Vị trí <span--}}
{{--                        class="d-block small text-danger">(Vị trí được hiển thị)</span></label>--}}
{{--            <div class="col-sm-10">--}}
{{--                @php($choosen = old('position', @$data->position))--}}
{{--                @if(!empty($choosen))--}}
{{--                    @if(is_string($choosen))--}}
{{--                        @php($choosen = explode(',', $choosen))--}}
{{--                    @endif--}}
{{--                @else--}}
{{--                    @php($choosen = [])--}}
{{--                @endif--}}

{{--                @foreach ($positions as $k => $v)--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-sm-1">--}}
{{--                            <label class="c-switch c-switch-label c-switch-outline-primary">--}}
{{--                                {!! Form::checkbox('position[]', $k, in_array($k, $choosen), ['class' => 'c-switch-input']) !!}--}}
{{--                                <span class="c-switch-slider" data-checked="On" data-unchecked="Off"></span>--}}
{{--                            </label>--}}
{{--                        </div>--}}
{{--                        <div class="col-sm-11 mb-2">{{ $v }}</div>--}}
{{--                    </div>--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--        </div>--}}

    </div>
</div>

@push('js_bot_all')
    <script>
        $(document).ready(function () {
            console.log(API_URL);
            $("#parent_id").select2({
                width: '100%',
                tags: true,
                ajax: {
                    url: "/admin/ajax/category/controller/getCategories?type=select2",
                    headers: ENV.headerParams,
                    dataType: 'JSON',
                    delay: 800,
                    data: function (params) {
                        return {
                            name: params.term, // search term
                            page: params.page,
                            _token: ENV.token
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results: data.data,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
            });
        });


        $('input#input-filter').autocomplete({
            minLength: 0,
            classes: {
                "ui-autocomplete": "dropdown-menu"
            },
            'source': function (request, response) {
                $.ajax({
                    global: false,
                    url: "{{ route('api_filtergroup_get_all') }}",
                    dataType: 'json',
                    headers: ENV.headerParams,
                    data: {
                        filter_group_name: request.term,
                        _token: ENV.token
                    },
                    success: function (json) {
                        response($.map(json.data, function (item) {
                            return {
                                label: item.name,
                                desc: item.short_description,
                                value: item.filter_group_id
                            }
                        }));
                    }
                });
            },
            'select': function (event, ui) {
                var item = ui.item;

                $('#category-filter' + item.value).remove();

                var html = '<a href="#" id="category-filter' + item.value +
                    '" class="col-3 list-group-item list-group-item-action">\n' +
                    '                        <i class="fa fa-close"></i> ' + item.label + '<br/><small class="text-danger">' + item.desc + '</small>' +
                    '                        <input type="hidden" name="category_filter_group[]" value="' + item
                        .value + '" />\n' +
                    '                    </a>';

                $('#category_filter_group').append(html);

                $('input#input-filter').val('');
                return false;
            }
        }).autocomplete("instance")._renderItem = function (ul, item) {
            return $("<li>")
                .append("<div>" + item.label + "<br/><small class='text-danger'>" + item.desc + "</small></div>")
                .appendTo(ul);
        };


        admin.system.tinyMCE('.editor_desc', plugins = '', menubar = '', toolbar = '', height = 300, '90%');

        $('#category_filter_group').delegate('.fa-close', 'click', function () {
            $(this).parent().remove();
        });

        @if(@$data->type === 2)
        $('.list-item').show();
        $('.list-position').show();
        @else
        $('.list-item').hide();
        $('.list-position').hide();
        @endif

        $('#type').on('change', function () {
            if ($(this).val() == 2) {
                $('.list-item').show();
                $('.list-position').show();
            } else {
                $('.list-item').hide();
                $('.list-position').hide();
            }
        });

    </script>
@endpush
