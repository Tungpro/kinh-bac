<tr class="tab-item-tr">

    <td>
        <div class="input-group p-1">
            @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
            <input type="file" class="form-control" placeholder="Tiêu đề"
                   name="category_description[{{$it_lang['language_id']}}][item][item_image][]" id="name_{{$it_lang['language_id']}}"
                   value="{{@$item['item_image']}}"
            >
            @if(!empty(@$item['item_image']))
                <input type="hidden" class="form-control"
                       name="category_description[{{$it_lang['language_id']}}][item][check_item_image][]"
                       value="{{@$item['item_image']}}"
                >
            @endif
            <div class="text-center"><img src="{{asset('upload/category/'.@$item['item_image'])}}"   alt="" style="width:35%"></div>
        </div>
    </td>

    <td>
        <div class="input-group p-1">
            @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
            <input type="text" class="form-control" placeholder="Tiêu đề"
                   name="category_description[{{$it_lang['language_id']}}][item][item_title][]" id="name_{{$it_lang['language_id']}}"
                   value="{{@$item['item_title']}}"
            >
        </div>
    </td>

    <td>
        <div class="input-group p-1">
            @include('basecontainer::admin.inc.ensign', ['lang' => $it_lang])
            <input type="text" class="form-control" placeholder="Link video youtube"
                   name="category_description[{{$it_lang['language_id']}}][item][item_iframe][]" id="name_{{$it_lang['language_id']}}"
                   value="{{@$item['item_iframe']}}"
            >
        </div>
    </td>

    <td class="px-2 text-center" style="width: 80px">
        <a href="javascript:void(0)" class="badge badge-danger" onclick="deleteATab(this)">
            Xóa
        </a>
    </td>
</tr>
