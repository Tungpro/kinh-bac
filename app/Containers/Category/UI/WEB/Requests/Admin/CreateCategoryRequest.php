<?php

namespace App\Containers\Category\UI\WEB\Requests\Admin;

use App\Containers\BaseContainer\Traits\RequestBaseLanguage;
use App\Ship\Parents\Requests\Request;

/**
 * Class CreateCategoryRequest.
 *
 */
class CreateCategoryRequest extends Request
{
    use RequestBaseLanguage;
    /**
     * Define which Roles and/or Permissions has access to this request.
     *
     * @var  array
     */
    protected $access = [
        'roles'       => 'admin',
        'permissions' => 'create-categories',
    ];

    /**
     * Id's that needs decoding before applying the validation rules.
     *
     * @var  array
     */
    protected $decode = [

    ];

    /**
     * Defining the URL parameters (`/stores/999/items`) allows applying
     * validation rules on them and allows accessing them like request data.
     *
     * @var  array
     */
    protected $urlParameters = [
        // 'id',
    ];

    /**
     * @return  array
     */
    public function rules()
    {
        return [
//            'category_description.*.name'  => 'required|max:255',
            'image' => 'bail|mimes:jpeg,jpg,png,gif',
        ];
    }

    /**g,gif|max
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {

        return  [
//            'category_description.*.name.max' => 'Độ dài tối đa 255 ký tự cho trường tên',
            'image.mimes' => 'Ảnh đại diện không đúng định dạng (jpeg, jpg, png, gif)',
        ] + $this->messagesLang([
//            'category_description.*.name.required' => 'Tên danh mục language không được bỏ trống',
//            'category_description.*.name.max' => 'Tên danh mục language tối đa 255 ký tự',
        ]);

    }

    /**
     * @return  bool
     */
    public function authorize()
    {
        return $this->check([
            'hasAccess',
        ]);
    }
}
