<?php

use Illuminate\Support\Facades\Route;

Route::namespace('\App\Containers\Category\UI\WEB\Controllers\Admin')->prefix('admin/ajax')->group(function () {
  Route::prefix('category')->group(function () {
    Route::get('/{product_id}/controller/cateData', [
      'as' => 'admin.category.controller.cateData',
      'uses' => 'Controller@ajaxCateDataByID',
      'middleware' => [
          'auth:admin',
      ],
    ]);

    Route::post('/controller/getCategories', [
      'as' => 'api_category_get_all2',
      'uses'       => 'Controller@getAllCategories',
      'middleware' => [
        'auth:admin',
      ]
    ]);

    Route::get('/controller/getCategories', [
      'as' => 'api_category_get_all2',
      'uses'       => 'Controller@getAllCategories',
      'middleware' => [
        'auth:admin',
      ]
    ]);
  });
});
