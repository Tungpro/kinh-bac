<?php

namespace App\Containers\Category\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;

/**
 * Class GetAllProductsAction.
 *
 */
class GetCategoryByIdAction extends Action
{

    /**
     * @return mixed
     */
    public function run($id, $currentLang = null)
    {
        $data = Apiato::call('Category@GetCategoryByIdTask', [$id, $currentLang ?? Apiato::call('Localization@GetDefaultLanguageTask')]);

        return $data;
    }
}
