<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-07-25 23:31:45
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-30 22:55:14
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\Actions\SubActions;

use App\Ship\Parents\Actions\SubAction;

class PresentFrontEndCategoriesSubAction extends SubAction
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run(?array $data): ?array
    {
        return $this->remember(function () use ($data) {
            return $this->buildTree($data);
        }, null, [5]);
    }

    private function buildTree(?array $elements, $parentId = 0)
    {
        $branch = array();

        if (!empty($elements)) {
            foreach ($elements as $element) {
                if ($element['parent_id'] == $parentId) {
                    $children = $this->buildTree($elements, $element['category_id']);
                    $element['sub'] = $children ?: [];
                    $branch[] = $element;
                }
            }
        }

        return $branch;
    }
}
