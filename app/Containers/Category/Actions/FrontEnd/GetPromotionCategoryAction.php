<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 20:43:34
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-11-02 16:12:23
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\Actions\FrontEnd;

use App\Containers\Banner\Actions\SubActions\GetAvailableBannerByPositionSubAction;
use App\Containers\Category\Actions\SubActions\GetAllCategoriesForFrontEndSubAction;
use App\Containers\Category\Enums\CategoryStatus;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetPromotionCategoryAction extends Action
{
    protected $getAvailableBannerByPositionSubAction;
    public $getAllCategoriesForFrontEndSubAction;
    protected $currentLang;
    protected $isMobile;
    protected $limit = 20;
    protected $skipPagination = false;

    public function __construct(
        GetAvailableBannerByPositionSubAction $getAvailableBannerByPositionSubAction,
        GetAllCategoriesForFrontEndSubAction $getAllCategoriesForFrontEndSubAction
    )
    {
        $this->getAllCategoriesForFrontEndSubAction = $getAllCategoriesForFrontEndSubAction;
        $this->getAvailableBannerByPositionSubAction = $getAvailableBannerByPositionSubAction;
        parent::__construct();
    }

    public function run(?Language $currentLang = null, int $parentId = 0, bool $isIndex = true, bool $isMobile = false, $withData = [])
    {
        $this->currentLang = $currentLang;
        $this->isMobile = $isMobile;
        return $this->remember(function () use ($parentId, $isIndex, $withData) {
            $data = $this->getAllCategoriesForFrontEndSubAction->keyBy('category_id')->run(
                [
                    'cate_type' => CategoryType::PRODUCT,
                    'show_sale_page' => CategoryStatus::ACTIVE_PROMOTION_PAGE,
                    'byParentId' => $parentId,
                ],
                $this->currentLang,
                $this->skipPagination,
                $this->limit,
                'category.sort_order,category.category_id desc',
                $withData,
                1,
                'object'
            );

            if (!empty($data) && $isIndex) {
                foreach ($data as &$cate) {
                    $this->getOnTopBanners($cate);
                    $this->getConnerBanners($cate);
                }
            }

            return $data;
        }, null, [], 0, $this->skipCache);
    }

    public function setLimit($limit = 20)
    {
        $this->limit = $limit;

        return $this;
    }

    public function skipPagination()
    {
        $this->skipPagination = true;

        return $this;
    }

    private function getOnTopBanners(&$cate): void
    {
        $onTopBanners = $this->getAvailableBannerByPositionSubAction->run(
            [
                'pos' => ['ads_sale_ontop_category'],
                'categoryIds' => [$cate['category_id']]
            ], // position whithin category ids
            $is_mobile = $this->isMobile,
            ['desc'], // Relationships
            ['sort_order' => 'ASC', 'created_at' => 'DESC'], // order by
            $this->currentLang, // Current lang
            1 // $limit
        );

        $cate['on_top_banners'] = $onTopBanners;
    }

    private function getConnerBanners(&$cate): void
    {
        $connerBanners = $this->getAvailableBannerByPositionSubAction->run(
            [
                'pos' => ['ads_sale_on_leftconner_category'],
                'categoryIds' => [$cate['category_id']]
            ], // position whithin category ids
            $is_mobile = $this->isMobile,
            ['desc'], // Relationships
            ['sort_order' => 'ASC', 'created_at' => 'DESC'], // order by
            $this->currentLang, // Current lang
            10 // $limit
        );

        $cate['conner_banners'] = $connerBanners;
    }
}
