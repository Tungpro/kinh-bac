<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 20:43:34
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-06 13:59:38
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\Actions\FrontEnd;

use App\Containers\Category\Actions\SubActions\GetAllCategoriesForFrontEndSubAction;
use App\Containers\Category\Enums\CategoryStatus;
use App\Containers\Category\Enums\CategoryType;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetHotCategoryAction extends Action
{
    public $getAllCategoriesForFrontEndSubAction;
    
    public function __construct(
        GetAllCategoriesForFrontEndSubAction $getAllCategoriesForFrontEndSubAction
    ) {
        parent::__construct();
        $this->getAllCategoriesForFrontEndSubAction = $getAllCategoriesForFrontEndSubAction;
    }

    public function run(?Language $currentLang = null): ?array
    {
        $data = $this->getAllCategoriesForFrontEndSubAction->run(
            [
                'cate_type' => CategoryType::PRODUCT,
                'hot' => CategoryStatus::HOT
            ],
            $currentLang,
            true,
            'category.sort_order,category.category_id desc',
        );
        
        return $data;
    }
}
