<?php

namespace App\Containers\Category\Actions\FrontEnd;

use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetCategoriesWithProductsAction extends Action
{
    public function run(?Language $currentLang = null, int $limitCategory = 3, int $limitProduct = 12)
    {
        $productCategoryWithProduct = app(GetAllCategoryAction::class)
            ->skipCache()
            ->run($currentLang, 0, false, []);

        return $productCategoryWithProduct->map(function ($item) use ($currentLang, $limitProduct) {
            return $item->setRelation('products', $item->withProductLimit($currentLang, $limitProduct, ['id', 'image', 'price', 'global_price']));
        });
    }
}
