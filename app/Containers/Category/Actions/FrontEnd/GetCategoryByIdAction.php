<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 20:43:34
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-02 14:37:12
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\Actions\FrontEnd;

use App\Containers\Category\Actions\SubActions\GetAllCategoriesForFrontEndSubAction;
use App\Containers\Category\Actions\SubActions\PresentFrontEndCategoriesSubAction;
use App\Containers\Localization\Models\Language;
use App\Ship\Parents\Actions\Action;

class GetCategoryByIdAction extends Action
{
    public $presentFrontEndCategoriesSubAction,$getAllCategoriesForFrontEndSubAction;
    
    public function __construct(
        PresentFrontEndCategoriesSubAction $presentFrontEndCategoriesSubAction,
        GetAllCategoriesForFrontEndSubAction $getAllCategoriesForFrontEndSubAction
    ) {
        parent::__construct();
        $this->presentFrontEndCategoriesSubAction = $presentFrontEndCategoriesSubAction;
        $this->getAllCategoriesForFrontEndSubAction = $getAllCategoriesForFrontEndSubAction;
    }

    public function run(int $categoryType, int $categoryId, Language $currentLang = null, bool $just_arr_ids = false): ?array
    {
        $data = $this->getAllCategoriesForFrontEndSubAction->run(
            ['cate_type' => $categoryType],
            $currentLang,
            true,
            'category.sort_order,category.created_at desc',
        );
        $data =  $this->presentFrontEndCategoriesSubAction->run($data);

        if (!$just_arr_ids) {
            $returnData = $this->returnCateById($data, $categoryId);
        } else {
            $returnData = [];//$this->getChildrenFor($data, $categoryId);
            $this->returnJustIds($data, $categoryId, $returnData);
        }
// dd($returnData,$data);
        return $returnData;
    }

    private function returnCateById(array $elements, int $categoryId = 0): ?array
    {
        $branch = [];

        if (!empty($branch)) {
            return $branch;
        }

        foreach ($elements as $element) {
            if ($element['category_id'] == $categoryId) {
                $branch = $element;
                break;
            } elseif (!empty($element['sub'])) {
                return $this->returnCateById($element['sub'], $categoryId);
            }
        }

        return $branch;
    }

    private function getChildrenFor($ary, $id): ?array
    {
        $results = array();

        foreach ($ary as $el) {
            if ($el['parent_id'] == $id) {
                $results[] = $el['category_id'];
            }
            if (count($el['sub']) > 0 && ($children = $this->getChildrenFor($el['sub'], $el['category_id'])) !== FALSE) {
                $results = array_merge($results, $children);
            }
        }

        return count($results) > 0 ? $results : [];
    }

    private function returnJustIds(array $elements, int $categoryId = 0, &$returnJustIds = []): ?array
    {
        foreach ($elements as $element) {
            if ($element['category_id'] == $categoryId) {
                $returnJustIds = array_merge([$element['category_id']],$this->getChildrenFor($element['sub'],$categoryId));
                break;
            } elseif (!empty($element['sub'])) {
                $this->returnJustIds($element['sub'], $categoryId, $returnJustIds);
            }
        }

        return $returnJustIds;
    }
}
