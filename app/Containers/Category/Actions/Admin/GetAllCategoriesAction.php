<?php

namespace App\Containers\Category\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

class GetAllCategoriesAction extends Action
{
    public function run($filters, $limit = 10, $skipPagination = false,$currentPage = 1)
    {

            $data = Apiato::call('Category@GetAllCategoriesTask', [$filters, Apiato::call('Localization@GetDefaultLanguageTask'), $skipPagination, $limit,$currentPage]);
            return $data;
    }
}
