<?php

namespace App\Containers\Category\Actions\Admin;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

class DeleteCategoryAction extends Action
{
    public function run($id)
    {
        $data = Apiato::call('Category@DeleteCategoryTask', [$id]);

        $this->clearCache();

        return $data;
    }
}
