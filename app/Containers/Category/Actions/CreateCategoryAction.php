<?php

namespace App\Containers\Category\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;
use App\Ship\Transporters\DataTransporter;
use App\Ship\Parents\Requests\Request;

/**
 * Class CreateCategoryAction.
 *
 */
class CreateCategoryAction extends Action
{

    /**
     * @return mixed
     */
    public function run($data,Request $request)
    {
        $category = Apiato::call('Category@CreateCategoryTask', [$data]);
        if($category) {
            Apiato::call('Category@SaveCategoryDescTask', [$data, $original_desc = [], $category->category_id, null, $request]);
            Apiato::call('Category@SavePathsTask', [$category,$category->category_id]);
            Apiato::call('Category@SaveCateFilterGroupTask', [$category,!empty($data->category_filter_group) ?: []]);
        }
        // $data

        $this->clearCache();

        return $category;
    }
}
