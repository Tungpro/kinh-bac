<?php

namespace App\Containers\Category\Tasks;

use App\Containers\Category\Data\Repositories\CategoryPathRepository;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class SavePathsTask.
 */
class SavePathsTask extends Task
{

    protected $repository;

    public function __construct(CategoryPathRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($data,$category_id)
    {
        $parent_id = $data['parent_id'];
        $category_paths = $this->repository->getModel()->where('path_id', $category_id)->orderBy('level')->get();

        if (!$category_paths->isEmpty()) {
            foreach ($category_paths as $category_path) {
                $this->repository->getModel()->where('category_id', $category_path->category_id)->where('level', '<', $category_path->level)->delete();
                $paths = [];
                $temp_paths = $this->repository->getModel()->where('category_id', $parent_id)->orderBy('level')->get()->pluck('path_id')->toArray();

                if (!empty($temp_paths)) $paths = array_merge($paths, $temp_paths);

                $temp_paths = $this->repository->getModel()->where('category_id', $category_path->category_id)->orderBy('level')->get()->pluck('path_id')->toArray();

                if (!empty($temp_paths)) $paths = array_merge($paths, $temp_paths);

                $level = 0;

                $replace_array = [];
                foreach ($paths as $path_id) {
                    $replace_array[] = ['category_id' => (int)$category_path->category_id, 'path_id' => (int)$path_id, 'level' => (int)$level];
                    $level++;
                }

                $this->repository->getModel()->replace($replace_array);
            }
        } else {
            $this->repository->getModel()->where('category_id', $category_id)->delete();
            $level = 0;

            $paths = $this->repository->getModel()->where('category_id', $parent_id)->orderBy('level')->get()->pluck('path_id')->toArray();

            $insert_array = [];
            foreach ($paths as $path_id) {
                $insert_array[] = ['category_id' => $category_id, 'path_id' => (int)$path_id, 'level' => (int)$level];
                $level++;
            }
            if (!empty($insert_array)) $this->repository->getModel()->insert($insert_array);
            $this->repository->getModel()->replace([['category_id' => (int)$category_id, 'path_id' => $category_id, 'level' => (int)$level]]);
        }
    }
}
