<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-09-30 16:49:08
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-20 16:50:58
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\Tasks\FrontEnd;

use App\Containers\Category\Data\Repositories\CategoryRepository;
use App\Containers\Collection\Enums\CollectionStatus;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;

class CategoryProductListingTask extends Task
{
    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(int $limit = 10, array $whereProduct = [])
    {
        $language_id = $this->currentLang ? $this->currentLang->language_id : 1;

        $this->repository->with([
            'desc' => function ($query) use ($language_id) {
                    $query->activeLang($language_id);
            },
            'products' => function ($query) use ($language_id,$whereProduct) {
                $query->where('status',2)->orderBy('sort_order','asc')->orderBy('created_at','desc');
                if (!empty($whereProduct['name'])) {
                        $query->whereHas('desc', function ($q) use ($whereProduct) {
                            $q->where('name', 'like', '%' . $whereProduct['name'] . '%');
                        });
                    }
            },
            'products.desc' => function ($query) use ($language_id) {
                $query->activeLang($language_id);
              },
            ]);

        if (!empty($whereProduct['name'])) {
            $this->repository->whereHas('products' ,function ($query) use ($whereProduct,$language_id){
                $query->whereHas('desc', function ($q) use ($whereProduct,$language_id) {
                    $q->where('name', 'like', '%' . $whereProduct['name'] . '%');
                });
            });

        }

        return $this->skipPagin ? ($limit == 0 ? $this->repository->all() : $this->repository->limit($limit)) : $this->repository->paginate($limit);
    }


    public function isActive(bool $bool = true): self
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('status', $bool ? CollectionStatus::ACTIVE : CollectionStatus::DE_ACTIVE));
        return $this;
    }

    public function inRandomOrder(): self
    {
        $this->repository->inRandomOrder();
        return $this;
    }
}
