<?php

namespace App\Containers\Category\Tasks\FrontEnd;


use App\Containers\Category\Data\Repositories\CategoryRepository;
use App\Containers\Localization\Models\Language;
use App\Containers\BaseContainer\Traits\ScopeAdminBaseSearchTrait;
use App\Containers\BaseContainer\Traits\ScopeFEAvailableDataTrait;
use App\Ship\Parents\Tasks\Task;

class GetAvailableCategoryByPositionTask extends Task
{
    use ScopeAdminBaseSearchTrait;
    use ScopeFEAvailableDataTrait;

    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(Language $currentLang = null, array $filters = [],$limit = 0, int $offset = 0, string $exceptID = '')
    {
        $language_id = $currentLang->language_id ? $currentLang->language_id : 1;

        return  $this->with([
                'desc' => function ($query) use ($language_id,$filters) {
                    $query->where('language_id', $language_id);
                    if(!empty($filters['slug'])){
                        $query->where('slug', $filters['slug']);
                    }
                },
            ])->returnDataByLimit($limit,$offset,$exceptID);
    }
}
