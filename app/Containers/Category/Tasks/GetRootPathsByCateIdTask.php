<?php

namespace App\Containers\Category\Tasks;

use App\Containers\Category\Data\Repositories\CategoryPathRepository;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;

/**
 * Class GetAllPathsByCateIdTask.
 */
class GetRootPathsByCateIdTask extends Task
{

    protected $repository;

    public function __construct(CategoryPathRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($category_id)   {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('category_id',$category_id));
        $this->repository->pushCriteria(new ThisEqualThatCriteria('level',0));
        $wery = $this->repository->first();

        return $wery;
    }
}
