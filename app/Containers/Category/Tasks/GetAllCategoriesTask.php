<?php

namespace App\Containers\Category\Tasks;

use App\Containers\Category\Data\Repositories\CategoryRepository;
use App\Containers\Category\Models\CategoryDesc;
use App\Containers\Category\Models\CategoryPath;
use App\Ship\Parents\Tasks\Task;
use Illuminate\Support\Facades\DB;

/**
 * Class GetAllProductssTask.
 */
class GetAllCategoriesTask extends Task
{

    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($filters = [], $defaultLanguage = null, $skipPagination = false, $limit = 10, $currentPage = 1)
    {

        $language_id = $defaultLanguage ? $defaultLanguage->language_id : 1;
        if (!isset($filters['status'])) {
            $conds[] = ['c1.status', '>', -1];
            $conds[] = ['c2.status', '>', -1];
        } else {
            $conds[] = ['c1.status', '=', $filters['status']];
            $conds[] = ['c2.status', '=', $filters['status']];
        }

        if (isset($filters['parent_id'])) {
            $conds[] = ['c1.parent_id', '=', (int)$filters['parent_id']];
            $conds[] = ['c2.parent_id', '=', (int)$filters['parent_id']];
        }


        $conds[] = ['cd1.language_id', '=', $language_id];
        $conds[] = ['cd2.language_id', '=', $language_id];



        $wery = DB::table(CategoryPath::getTableName(), 'cp');
        $wery->selectRaw("cp.category_id AS category_id,cd2.name as self_name,cd2.description as description,
                GROUP_CONCAT( cd1.NAME ORDER BY cp.LEVEL SEPARATOR '" . $this->repository->separator . "' ) AS name,
                c1.parent_id,
                c1.sort_order, c1.type,c1.is_home,c1.hot, c1.is_good_price,c1.show_freeship_page,c1.show_sale_page,
                c1.position,cd2.slug");
        $wery->leftJoin($this->repository->getModel()->getTable() . ' as c1', 'cp.category_id', '=', 'c1.category_id');
        $wery->leftJoin($this->repository->getModel()->getTable() . ' as c2', 'cp.path_id', '=', 'c2.category_id');
        $wery->leftJoin(CategoryDesc::getTableName() . ' as cd1', 'cp.path_id', '=', 'cd1.category_id');
        $wery->leftJoin(CategoryDesc::getTableName() . ' as cd2', 'cp.category_id', '=', 'cd2.category_id');
        $wery->where($conds);
        $wery->groupBy('cp.category_id');
        if (isset($filters['name']) && $filters['name'] != '') {
            // $detectSearch=explode(">  ",$filters['name']);
            $detectSearch = explode(">", $filters['name']);
            $lastKey = array_key_last($detectSearch);
            if ($lastKey > 0) {
                if (!empty($detectSearch[$lastKey]))
                    $wery->having('name', 'LIKE', '%' . trim(preg_replace('~\x{00a0}~siu', '', $detectSearch[$lastKey])) . '%');
                else
                    $wery->having('name', 'LIKE', '%' . trim(preg_replace('~\x{00a0}~siu', '', $detectSearch[$lastKey - 1])) . '%');
            } else {
                $wery->having('name', 'LIKE', '%' . $filters['name'] . '%');
            }
            // $conds[] = ['name','LIKE','%'.$request->title.'%'];
        }
        if (isset($filters['cate_type'])) {
            if(is_array($filters['cate_type'])){
                $wery->whereIn('c1.type',$filters['cate_type']);
            }else{
                $wery->where('c1.type',$filters['cate_type']);
            }
        }

        $wery->orderByRaw('name,type,sort_order');

        // return $wery->paginate($limit);

        return $skipPagination ? $wery->get() : $wery->paginate($limit);
    }
}
