<?php

namespace App\Containers\Category\Tasks;

use App\Ship\Parents\Tasks\Task;

/**
 * Class SaveCateFilterGroupTask.
 */
class SaveCateFilterGroupTask extends Task
{

    protected $repository;

    public function __construct()
    {
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($category,$category_filter_group)
    {
        if (is_array($category_filter_group)) {
            $category->filter_groups()->sync($category_filter_group);
        }
    }
}
