<?php

namespace App\Containers\Category\Tasks;

use App\Containers\Category\Data\Repositories\CategoryDescRepository;
use App\Containers\Category\Data\Repositories\CategoryRepository;
use App\Containers\Category\Models\CategoryDesc;
use App\Containers\Category\Models\CategoryPath;
use App\Ship\Criterias\Eloquent\ThisEqualThatCriteria;
use App\Ship\Parents\Tasks\Task;
use App\Ship\Transporters\DataTransporter;
use Illuminate\Support\Facades\DB;

/**
 * Class GetAllCategoryDescTask.
 */
class GetAllCategoryDescTask extends Task
{

    protected $repository;

    public function __construct(CategoryDescRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $skipPagination
     *
     * @return  mixed
     */
    public function run($category_id, $defaultLanguage = null)
    {
        $this->repository->pushCriteria(new ThisEqualThatCriteria('category_id', $category_id));
        $wery = $this->repository->all()->keyBy('language_id')->toArray();

        return $wery;
    }
}
