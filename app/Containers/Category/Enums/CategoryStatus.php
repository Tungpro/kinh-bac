<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-10-03 22:17:38
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class CategoryStatus extends BaseEnum
{
    const IS_GOOD_PRICE = 1;
    const SHOW_FREESHIP_PAGE = 1;
    const SHOW_PROMOTION_PAGE = 1;

    const ACTIVE  = 2 ;
    const HIDE = 1 ;
    const DELETE = -1;

    const STATUS = [
        self::ACTIVE => 'Hiển thị',
        self::HIDE => 'Ẩn',
        self::DELETE => 'Xóa',
    ];


}
