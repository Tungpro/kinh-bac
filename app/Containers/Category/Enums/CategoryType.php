<?php

/**
 * @ Created by: VSCode
 * @ Author: Oops!Memory - OopsMemory.com
 * @ Create Time: 2021-08-04 22:37:51
 * @ Modified by: Oops!Memory - OopsMemory.com
 * @ Modified time: 2021-08-04 22:47:58
 * @ Description: Happy Coding!
 */

namespace App\Containers\Category\Enums;

use App\Containers\BaseContainer\Enums\BaseEnum;

final class CategoryType extends BaseEnum
{
    const PRODUCT = 1;
    const NEWS = 2;
    const PROJECT = 3;
    const HOLDER = 4;
    const RECRUIT = 5;

    const TYPE = [
          self::NEWS => "Tin tức",
          self::PROJECT => "Lĩnh vực hoạt động",
          self::HOLDER => "Quan hệ cổ đông",
          self::RECRUIT => "Tuyển dụng",
    ];
}