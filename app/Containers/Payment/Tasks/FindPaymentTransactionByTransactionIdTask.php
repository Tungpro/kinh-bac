<?php

namespace App\Containers\Payment\Tasks;

use App\Containers\Payment\Data\Repositories\PaymentTransactionRepository;
use App\Containers\Payment\Models\PaymentAccount;
use App\Containers\Payment\Models\PaymentTransaction;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

/**
 * Class FindPaymentTransactionByTransactionIdTask
 *
 */
class FindPaymentTransactionByTransactionIdTask extends Task
{

    protected $repository;

    public function __construct(PaymentTransactionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $id
     *
     * @return  mixed
     * @throws  NotFoundException
     */
    public function run($transaction_id): ?PaymentTransaction
    {
        try {
            $paymentTransaction = $this->repository->where('transaction_id',$transaction_id)->first();
        } catch (Exception $exception) {
            throw new NotFoundException();
        }

        return $paymentTransaction;
    }
}
