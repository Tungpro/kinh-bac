<?php

namespace App\Containers\Payment\Gateway;

use App\Containers\Order\Models\Order;
use App\Containers\Payment\Actions\FindPaymentTransactionDetailsAction;
use App\Containers\Payment\Exceptions\NoChargeTaskForPaymentGatewayDefinedException;
use App\Containers\Payment\Models\PaymentTransaction;
use App\Containers\Settings\Actions\PaymentType\FindPaymentTypeByIdAction;
use App\Containers\Settings\Enums\PaymentStatus;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

/**
 * Class PaymentsGateway
 *
 * @author  Johannes Schobel <johannes.schobel@googlemail.com>
 * @author  Mahmoud Zalt  <mahmoud@zalt.me>
 */
class PaymentsGateway
{

  /**
   * @param Order $order
   * @return PaymentTransaction
   */
  public function charge(Order $order)
  {
    // check, if the account is owned by user to be charged
    $paymentType = app(FindPaymentTypeByIdAction::class)->run($order->payment_type);
    //Check payment type
    if (!$paymentType) {
      return false;
    }
    //Check payment gateways online
    if (empty($paymentType->online_method)) {
      return false;
    }

    $chargerTaskTaskName = Config::get('payment-container.gateways.' . $paymentType->online_method . '.charge_task', null);

    if ($chargerTaskTaskName === null) {
      throw new NoChargeTaskForPaymentGatewayDefinedException();
    }

    // create the task
    $chargerTask = App::make($chargerTaskTaskName);
    $method = Config::get('payment-container.gateways.' . $paymentType->online_method . '.method', null);
    $transaction = $chargerTask->charge($order,$method);
    $transaction->user_id = $order->customer_id;
    $transaction->gateway = $paymentType->online_method;

    $transaction->save();

    return $transaction;
  }

  public function verify(Order $order)
  {
    if(!empty($order->payment_time)) {
      return $order;
    }
    // check, if the account is owned by user to be charged
    $transaction = app(FindPaymentTransactionDetailsAction::class)->run($order->id);
    //Check payment type
    if (!$transaction) {
      return false;
    }
    $chargerTaskTaskName = Config::get('payment-container.gateways.' . $transaction->gateway . '.verify_task', null);
    if ($chargerTaskTaskName === null) {
      throw new NoChargeTaskForPaymentGatewayDefinedException();
    }

    $message = '';
    // create the task
    $chargerTask = App::make($chargerTaskTaskName);
    $verify = $chargerTask->verify($message);
    $transaction->is_successful = $verify;
    $order->payment_time = Carbon::now();
    $order->pay_gate_note = $message;
    $order->pay_gate_type = @$_REQUEST['gate']??Config::get('payment-container.gateways.' . $transaction->gateway . '.method', null);;
    if ($verify) {
      $order->payment_status = PaymentStatus::PAID;
    } else {
      $order->payment_status = PaymentStatus::NON_PAID;
    }
    $transaction->save();
    $order->save();

    return $order;
  }
}
