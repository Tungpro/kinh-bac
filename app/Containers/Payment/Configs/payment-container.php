<?php

use App\Containers\Billing\Tasks\ChargeWithBillingTask;
use App\Containers\Billing\Tasks\VerifyPaymentWithBillingTask;
use App\Containers\Nganluong\Tasks\ChargeWithNganluongTask;
use App\Containers\Nganluong\Tasks\VerifyPaymentWithNganluongTask;

return [

  /*
    |--------------------------------------------------------------------------
    | Payment Container
    |--------------------------------------------------------------------------
    */


  /*
     * The default currency if no currency is passed
     */
  'currency' => 'vnd',

  'gateways' => [
    'billing_visa' => [
      'container'   => 'Thanh toán VISA qua Billing',
      'charge_task' => ChargeWithBillingTask::class,
      'method' => 'wepay_visa',
      'verify_task' => VerifyPaymentWithBillingTask::class,
    ],

    'billing_momo' => [
      'container'   => 'Thanh toán MoMo qua Billing',
      'charge_task' => ChargeWithBillingTask::class,
      'method' => 'momo_direct',
      'verify_task' => VerifyPaymentWithBillingTask::class,
    ],

    'billing_vnpay' => [
      'container'   => 'Thanh toán VNPay qua Billing',
      'charge_task' => ChargeWithBillingTask::class,
      'method' => 'vnpay',
      'verify_task' => VerifyPaymentWithBillingTask::class,
    ],

    'billing_viettelpay' => [
      'container'   => 'Thanh toán Viettelpay qua Billing',
      'charge_task' => ChargeWithBillingTask::class,
      'method' => 'viettelpay',
      'verify_task' => VerifyPaymentWithBillingTask::class,
    ],

    'nganluong_visa' => [
      'container'   => 'Thanh toán VISA qua Ngân Lượng',
      'charge_task' => ChargeWithNganluongTask::class,
      'method' => 'VISA',
      'verify_task' => VerifyPaymentWithNganluongTask::class,
    ],

    'nganluong_atm' => [
      'container'   => 'Thanh toán ATM qua Ngân Lượng',
      'charge_task' => ChargeWithNganluongTask::class,
      'method' => 'ATM_ONLINE',
      'verify_task' => VerifyPaymentWithNganluongTask::class,
    ],
  ],
];
