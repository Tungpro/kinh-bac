<?php

namespace App\Containers\Payment\Actions;

use App\Containers\Payment\Models\PaymentTransaction;
use App\Containers\Payment\Tasks\FindPaymentTransactionByTransactionIdTask;
use App\Ship\Parents\Actions\Action;

/**
 * Class FindPaymentTransactionDetailsAction
 *
 */
class FindPaymentTransactionDetailsAction extends Action
{

    /**
     * @param Int $id
     *
     * @return  \App\Containers\Payment\Models\PaymentTransaction
     */
    public function run($transaction_id): ?PaymentTransaction
    {
        $paymentTransaction = app(FindPaymentTransactionByTransactionIdTask::class)->run($transaction_id);
        return $paymentTransaction;
    }
}
