<?php

namespace App\Containers\Payment\Actions;

use Apiato\Core\Foundation\Facades\Apiato;
use App\Ship\Parents\Actions\Action;

/**
 * Class GetAllPaymentAccountsAction
 *
 * @author  Johannes Schobel <johannes.schobel@googlemail.com>
 */
class ProcessPaymentAction extends Action
{

    /**
     * @return  mixed
     */
    public function run($order)
    {
        $paymentType = Apiato::call('Settings@FindPaymentTypeByIdAction',[$order->payment_type]);

        $paymentAccounts = Apiato::call('Payment@ProcessPaymentTask',
            [

            ],
            [
                'addRequestCriteria',
                'ordered',
            ]
        );

        return true;
    }
}
