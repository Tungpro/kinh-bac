<?php

namespace App\Containers\Payment\Contracts;

use App\Containers\Order\Models\Order;
use App\Containers\Payment\Models\AbstractPaymentAccount;
use App\Containers\Payment\Models\PaymentTransaction;

/**
 * Interface  PaymentChargerInterface
 *
 * @author   Johannes Schobel <johannes.schobel@googlemail.com>
 */
interface PaymentChargerInterface
{

    /**
     * Performs the transaction by making a request to the service provider (e.g., paypal, stripe, ...)
     *
     * @param Order $order
     * @return PaymentTransaction
     */
    public function charge(Order $order);

}
